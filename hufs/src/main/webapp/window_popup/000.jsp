<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/web/smart_001/image"/>
<c:set var="_CSS" value="${pageContext.request.contextPath}/template/web/smart_001/css"/>
<c:set var="C_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="CML" value="/template/lms"/>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="imagetoolbar" content="no" />
<title><c:out value='${popupManageVO.popupTitleNm}'/></title>
<link rel="stylesheet" type="text/css" href="${_CSS}/styles.css" />
<script src="/template/common/js/jquery-1.12.4.min.js"></script>

<script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
<script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
<script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>

<script type="text/javascript" src="${C_JS}/common.js"></script>
<script type="text/javaScript">
$(document).ready(function(){
	$("#chkPopup").change(function(){
		if($(this).is(":checked")){
			fnSetCookiePopup( "notice", "done" , 1);
		}else{
			fnSetCookiePopup( "notice", "done" , 0);
		}
	});
});
/* ********************************************************
* 체크버튼 클릭시
******************************************************** */
function fnPopupCheck() {

	var chk = document.getElementById("chkPopup");
	if(chk && chk.checked) {
		fnSetCookiePopup( "notice", "done" , 1);
	}
	window.close();
}

</script>
<style type="text/css"> 
	html{height:100%;width:100%;}
</style>
</head>
<body style="background:none;width:100%;height:100%;overflow:auto;">
	<div>
		<img src="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/images/temp/notice201218.png" alt=""/>
	</div>
	
	<div id="popmainboxdn">
		<label for="chkPopup"><input type="checkbox" name="chkPopup" id="chkPopup"/>24시간동안 이 창 열지 않기</label>
		<a href="#" class="btn_close_pop" onclick="fnPopupCheck()" title="팝업창닫기"><img src="${_IMG}/common/popbox_close.gif" alt="닫기" /></a>
	</div>
</body>
</html>