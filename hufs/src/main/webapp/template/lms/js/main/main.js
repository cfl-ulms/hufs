$(document).ready(function () {
  if (document.querySelector("[data-slick]")) {
    // slick 슬라이드
    $(".visualSlick").slick({
      //메인비주얼
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: true,
      arrows: false,
    });
    $(".bookListSlick").slick({
      //특수외국어 출간 교재
      slidesToShow: 5,
      slidesToScroll: 5,
      infinite: false,
      dots: true,
      arrows: true,
    });
    $(".myClassSlick").slick({
      //나의수업
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      arrows: false,
      dots: true,
    });
    $(".noticeListSlick").slick({
      //나의수업
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      vertical: true,
      verticalSwiping: true,
      autoplay: true,
      autoplaySpeed: 2000,
      pauseOnFocus: true,
      pauseOnHover: true,
      prevArrow: $(".notice-icons-wrap .prev"),
      nextArrow: $(".notice-icons-wrap .next"),
    });
    $("#gallerySlick").slick({
      //배너 있을 때
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: true,
      arrows: false,
    });
    $("#gallerySlick2").slick({
      //배너 없을 때
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: true,
      arrows: false,
    });
  }

  $(".notice-icons.play").on("click", function () {
    var $this = $(this);
    $this.toggleClass("pause");
    if ($this.hasClass("pause")) {
      $(".noticeListSlick").slick("slickPause");
    } else {
      $(".noticeListSlick").slick("slickPlay");
    }
  });

  $('#boardBox .tab-wrap .tab-list').click(function(){
    var idx = $(this).index();
    
    $('#boardBox .tab-wrap .tab-list').removeClass('on').eq(idx).addClass('on');
    $('#boardBox .con-board').hide().eq(idx).fadeIn();
    $('#boardBox .more').hide();
    $('#boardBox .more' + idx).fadeIn();
  });
});
