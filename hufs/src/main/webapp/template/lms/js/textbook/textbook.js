$(document).ready(function () {

  $(".textbookSlide").slick({ //교재/사전 슬라이드
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    arrows: false,
    dots: true,
  });

});