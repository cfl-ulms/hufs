$(document).ready(function () {
  $(document).on("click", ".tab-lists", function () {
    var $this = $(this);
    var $thisContents = $this.data("target");
    $(".tab-lists").removeClass("on");
    $this.addClass("on");
    $(".educourse-con-lists").removeClass("on");
    $($thisContents).addClass("on");
  });

  $(".hotCourse").slick({ //인기 있는 교육과정
    slidesToShow: 3,
    slidesToScroll: 3,
    infinite: true,
    arrows: false,
    dots: true,
  });

  if ($(".checkList").length) {
    var $allChkkBtn = $(".allCheck[type=checkbox]:not(:disabled)"); //전체체크
    var $checkBox = $(".checkList[type=checkbox]:not(:disabled)"); //모든체크목록
    var $parents = $(".common-table-wrap , .table-type-board");

    //체크박스
    $checkBox.on("change click", function () {
      var $this = $(this);
      var chkBoxCnt = $(".checkList[type=checkbox]:not(:disabled)").length; //체크박스 개수
      var checkedCnt = $(".checkList[type=checkbox]:checked").length; //활성화된 체크박스 개수
      
      if ($allChkkBtn) {
        if (checkedCnt < chkBoxCnt) { //모두 체크 시 전체체크 활성화
          $this.parents($parents).find($allChkkBtn).prop("checked", false);
        } else if (checkedCnt == chkBoxCnt) {
          $this.parents($parents).find($allChkkBtn).prop("checked", true);
        }
      }

      if ($this.hasClass("colorChange")) { //배경색변경
        if ($this.prop("checked")) {
          $this.parents("tr").addClass("bg-point-light");
        } else {
          $this.parents("tr").removeClass("bg-point-light");
        }
      }

    });

    //전체체크박스
    if ($allChkkBtn) {
      $allChkkBtn.on("change click", function () {
        var $this = $(this);
        var $allChkLists = $this.parents($parents).find($checkBox);
  
        if ($this.prop("checked")) {
          $allChkLists.prop("checked", true);
          $allChkLists.trigger("change");
        } else {
          $allChkLists.prop("checked", false);
          $allChkLists.trigger("change");
        }
      });
    }
  }
  
});


