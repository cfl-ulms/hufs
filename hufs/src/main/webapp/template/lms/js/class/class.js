$(document).ready(function () {
  //과제 내용 펼치기,접기
  if ($(".subjectToggle").length) {
    var $subjectToggle = $(".subjectToggle");
    var flag = $subjectToggle.data("open"); // flag == true 박스 열린상태 // false 닫힌상태

    var subjectHeaderH = $(".subjectViewContain .subject-title-wrap").outerHeight() + 29; // 헤더 높이값 + 박스 패딩값
    var subjectBoxH = $subjectToggle.siblings(".subject-view-wrap").css('height', 'auto').outerHeight(); // reset value

    // 함수 호출
    subjectTextChange(flag);
    subjectContainer($subjectToggle, false);

    $subjectToggle.on("click",function(){
      var $this = $(this);
      subjectContainer($this, true);
    });
  
    function subjectContainer($this, $isAnimate) {
      var $target = $this.siblings(".subject-view-wrap");
      if (!flag) { //닫힘
        if ($isAnimate) {
          $target.stop().animate({"height": subjectHeaderH}, 600);
        }else {
          $target.css({"height": subjectHeaderH});
        }
        flag = true;
        subjectTextChange(flag);
      } else { //펼치기
        $target.outerHeight(subjectHeaderH); //init & start height
        if ($isAnimate) {
          $target.stop().animate({"height": subjectBoxH}, 600);
        }else {
          $target.css({"height": subjectBoxH});
        }
        
        flag = false;
        subjectTextChange(flag);
      }
    }

    function subjectTextChange(flag) {
      if (!flag) { // 펼쳐진 상태
        $subjectToggle.children(".text").text("과제내용 접기");
        $subjectToggle.removeClass('on'); // 화살표 방향 바꾸기
        
      } else {
        $subjectToggle.children(".text").text("과제내용 펼치기");
        $subjectToggle.addClass('on'); // 화살표 방향 바꾸기
      }
    }
  }
});