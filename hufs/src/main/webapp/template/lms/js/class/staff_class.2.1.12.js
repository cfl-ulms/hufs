$(document).ready(function () {

    var $chkBox = $(".checkbox [type=radio]:not(:disabled)"); //모든체크목록
    
    //체크박스
    $chkBox.on("click", function () {
      var $this = $(this);
      var allChkBoxes = $this.parents("tr").find($chkBox);
      
      allChkBoxes.each(function () {
        var $this = $(this);
        
        if (this.checked) {
          $this.parents("td").addClass($this.data("color"));
        }else {
          $this.parents("td").removeClass($this.data("color"));
        }
      });
      
    });
});