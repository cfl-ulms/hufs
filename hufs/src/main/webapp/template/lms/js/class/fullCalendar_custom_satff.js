document.addEventListener('DOMContentLoaded', function () {
  var initialLocaleCode = 'ko';
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth',
    },
    locale: initialLocaleCode,
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [{
        title: '[학교]',
        start: '2020-04-03T13:00:00',
        constraint: 'schoolSchedule', // defined below
        color: '#b7ca10' // 학교일정 컬러값
      },
      {
        title: '[개인]',
        start: '2020-04-13T11:00:00',
        constraint: 'personalSchedule', // defined below
        color: '#76828e' // 개인일정 컬러값
      },
      {
        title: '[수업] 태국어',
        start: '2020-04-18',
        end: '2020-04-21',
        constraint: 'classSchedule', // defined below
        color: '#068467' // 수업일정
      },
      {
        title: 'Meeting',
        start: '2020-04-18T10:30:00',
        end: '2020-04-18T12:30:00',
        color: '#068467' // 수업일정
      },
      {
        title: 'Lunch',
        start: '2020-04-18T12:00:00',
        color: '#068467' // 수업일정
      },
      {
        title: 'Meeting',
        start: '2020-04-18T14:30:00',
        color: '#068467' // 수업일정
      },
      {
        title: 'Happy Hour',
        start: '2020-04-18T17:30:00',
        color: '#068467' // 수업일정
      },
      {
        title: 'Dinner',
        start: '2020-04-18T20:00:00',
        color: '#068467' // 수업일정
      },

      // areas where "personalSchedule" must be dropped
      {
        groupId: 'personalSchedule',
        title: '[개인]',
        start: '2020-04-11T10:00:00',
        end: '2020-04-11T16:00:00',
        color: '#76828e'
      },

    ],
    eventClick: function (arg) {
      // opens events in a popup window
    },
  }); 
   calendar.render();
});