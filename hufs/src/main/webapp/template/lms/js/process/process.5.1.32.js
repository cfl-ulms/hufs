$(document).ready(function () {

  if (document.getElementsByClassName('boardTypeGraph')) {
    var myChartArr = [];

    var chartGroupBarOption = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#54c374', '#396fd4'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom : 0,
        data: ['조 그룹수', '총 학생수'],
        icon: 'rect',
        itemGap : 20,
        textStyle : {
          fontSize : 13,
          verticalAlign : 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 50, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        },
      },
      yAxis: {
        type: 'category',
        data: ['전체','해당\n교육과정'],
        axisLabel: {
          lineHeight: 17
        },
        axisTick: {
          show: false,
        },
      },
      series: [
        {
          name: '조 그룹수',
          type: 'bar',
          barGap: 0,
          barWidth: 40,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [30, 12], //data 값 변경
        },
        {
          name: '총 학생수',
          type: 'bar',
          barGap: 0,
          barWidth: 40,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [40, 15], //data 값 변경
        },
      ]
    };

    var chartSubjectBarOption = {
      animation:false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      color: ['#396fd4', '#facc32'],
      legend: {
        show: true,
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['과제 완료율', '과제수'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: true,
          data: ['전체', '나'],
          axisTick: {
            show: false,
          },
        },
        {
          type: 'category',
          axisTick: {
            show: false,
          },
        }
      ],
      yAxis: [
        {
          type: 'value',
          scale: true,
          min: 0, // 최소 갯수 (값 확인후 변경가능)
          max: 10, // 최대 갯수 (값 확인후 변경가능)
          axisTick: {
            show: false,
          },
        },
        {
          type: 'value',
          scale: true,
          min: 0, // 최소 갯수 (값 확인후 변경가능)
          max: 100, // 최대 갯수 (값 확인후 변경가능)
          axisLabel: {
            formatter: '{value}%'
          },
          axisTick: {
            show: false,
          },
          axisLine: {
            show: false,
          }
        },
      ],
      series: [
        {
          name: '과제수',
          type: 'bar',
          barWidth: 60,
          xAxisIndex: 0,
          yAxisIndex: 0,
          label: {
            normal: {
              show: true,
              position: 'insideTop',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: ['6', '3'], //data 값 변경
        },
        {
          name: '과제 완료율',
          type: 'line',
          xAxisIndex: 1,
          yAxisIndex: 1,
          symbolSize: 12,
          label: {
            normal: {
              show: true,
              position: [-15,17],
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
                color:'#000',
                backgroundColor: '#facc32',
                padding: 5,
              },
              symbolSize : 30,
              formatter: '{@%}%',
            },
          },
          data: ['45', '85'], //data 값 변경
        },
      ]
    };

    var chartGenderBarOption = {
      color: ['#54c374', '#396fd4'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        show: true,
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정', '전체'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능)
        axisLabel: {
          formatter: '{value}%'
        },
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['남자', '여자'],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          stack: '비율',
          barWidth: 60,
          label: {
            normal: {
              show: true,
              position: 'inside',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [45.5, 50] //data 값 변경
        },
        {
          name: '전체',
          type: 'bar',
          stack: '비율',
          barWidth: 60,
          label: {
            normal: {
              show: true,
              position: 'inside',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [54.5, 50] //data 값 변경
        },
      ]
    };

    var chartStudentBarOption2 = {
      color: ['#396fd4', '#54c374', '#facc32', '#f6845d'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['1학년', '2학년', '3학년', '4학년'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#54c374', '#facc32', '#f6845d'],
          radius: '65%',
          center: ['50%', '43%'],
          hoverAnimation: false,
          data: [
            { value: 19, name: '1학년' }, //data 값 변경
            { value: 7, name: '2학년' }, //data 값 변경
            { value: 4, name: '3학년' }, //data 값 변경
            { value: 5, name: '4학년' }, //data 값 변경
          ],
          labelLine: {
            normal: {
              show: true,
            },
          },
          label: {
            normal: {
              show: true,
              formatter: '{c}',
              textStyle: {
                color: '#333',
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },

        }
      ]
    };

    var chartSatisfyPieOption = {
      color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
      animation: false,
      legend: {
        orient: 'vertical',
        right: 0,
        top:30,
        data: ['매우만족', '만족', '보통', '불만족','매우불만족'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
          radius: '100%',
          center: ['35%', '50%'],
          hoverAnimation: false,
          animation: false,
          data: [
            { value: 15, name: '매우만족' },
            { value: 4, name: '만족' },
            { value: 3, name: '보통' },
            { value: 2, name: '불만족' },
            { value: 1, name: '매우불만족' },
          ],
          labelLine: {
            normal: {
              show: false,
            }
          },
          label: {
            normal: {
              show: false,
            }
          },
        }
      ]
    };

    //echart 셋팅
    $(".boardTypeGraph").each(function (idx, el) {
      var myChart = echarts.init(el);
      var chartType = el.dataset.chartType;

      myChartArr.push(myChart);

      if (chartType == "groupbar") {
        //차트옵션 셋팅
        myChart.setOption(chartGroupBarOption);
      } else if (chartType == "subjectbar") {
        //차트옵션 셋팅
        myChart.setOption(chartSubjectBarOption);
      } else if (chartType == "genderbar") {
        //차트옵션 셋팅
        myChart.setOption(chartGenderBarOption);
      } else if (chartType == "studentPie2") {
        //차트옵션 셋팅
        myChart.setOption(chartStudentBarOption2);
      } else if (chartType == "satisfypie") {
        //차트옵션 셋팅
        myChart.setOption(chartSatisfyPieOption);
      }
    });

    $(window).on('resize', function () {
      myChartArr.forEach(function (el, idx) {
        el.resize();
      });
    });

    $(".btn-expansion").on("click", function () {
      myChartArr.forEach(function (el, idx) {
        setTimeout(function () {
          el.resize();
        }, 310); // transition 끝난후 resize
      });
    });
  }
});