$(document).ready(function(){

  if (document.getElementsByClassName('boardTypeGraph')){
    var myChartArr = [];

    var chartPieOption = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#396fd4', '#54c374', '#ddd'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['공지', '질문', '기타'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#54c374', '#ddd'],
          radius: '80%',
          center: ['50%', '40%'],
          hoverAnimation : false,
          data: [
            { value: 65, name: '공지' }, //data 값 변경
            { value: 20, name: '질문' }, //data 값 변경
            { value: 15, name: '기타' }, //data 값 변경
          ],
          labelLine: {
            normal: {
              show: false,
            }
          },
          label: {
            normal: {
              show: false,
            }
          },
          
        }
      ]
    };

    var chartBarOption = {
      grid: {
        top: '2%',
        left: '3%',
        right: '3%',
        bottom: '14%',
        containLabel: true
      },
      color: ['#396fd4', '#54c374', '#dddddd'],
      animation: false,
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['공지', '질문','기타'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 12, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['오바른', '이바른', '관리자'],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '공지',
          type: 'bar',
          stack: '총갯수',
          barWidth: 20,
          label: {
            normal: {
              show: false,
            }
          },
          data: [0, 0, 9] //data 값 변경
        },
        {
          name: '질문',
          type: 'bar',
          stack: '총갯수',
          barWidth: 20,
          label: {
            normal: {
              show: false,
            }
          },
          data: [2, 3, 0] //data 값 변경
        },
        {
          name: '기타',
          type: 'bar',
          stack: '총갯수',
          barWidth: 20,
          label: {
            normal: {
              show: false,
            }
          },
          data: [4, 3, 0] //data 값 변경
        },
      ]
    };

    //echart 셋팅
    $(".boardTypeGraph").each(function (idx, el) {
      var myChart = echarts.init(el);
      var chartType = el.dataset.chartType;
      
      myChartArr.push(myChart);

      if (chartType == "pie") {
        //차트옵션 셋팅
        myChart.setOption(chartPieOption);
      } else if (chartType == "bar"){
        //차트옵션 셋팅
        myChart.setOption(chartBarOption);
      } 
    });

    $(window).on('resize', function () {
      myChartArr.forEach(function (el, idx) {
        el.resize();
      });
    });

    $(".btn-expansion").on("click",function(){
      myChartArr.forEach(function (el, idx) {
        setTimeout(function(){
          el.resize();
        },310); // transition 끝난후 resize
      });
    });
  }
});