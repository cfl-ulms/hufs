function SurveyWidgetObj(objIndex) {
	var index = objIndex - 1 ;
	 this.widget = $(
			 '<div class="tbset">\
				<div class="tb02wrap">\
			 	<a href="#" class="upMove" title="위로이동"><img src="/template/manage/images/btn/btn_goup.gif"/></a>\
	 			<a href="#" class="downMove" title="아래이동"><img src="/template/manage/images/btn/btn_godown.gif"/></a>\
			    <table class="chart2 ip_tb02">\
			        <tbody>\
			        <tr>\
						<th>설문 번호</th>\
			 			<td class="surveyTd">'+objIndex+'</td>\
						<th>필수여부</th>\
						<td>\
							<input type="radio" id="essenAtY'+objIndex+'" class="essenAt" name="essenAt'+objIndex+'" value="Y"><label for="essenAtY'+objIndex+'">필수</label>\
		                  	<input type="radio" id="essenAtN'+objIndex+'" class="essenAt" name="essenAt'+objIndex+'" value="N" ><label for="essenAtN'+objIndex+'">선택</label>\
						</td>\
			        </tr>\
			        <tr>\
						<th>항목 주제</th>\
						<td colspan="3"><textarea style="width:100%;height:50px;" placeholder="항목 주제를 입력하세요." name="qesitmSj" ></textarea></td>\
			        </tr>\
			        <tr>\
						<th>설명</th>\
						<td colspan="3"><textarea style="width:100%;height:50px;" placeholder="항목에 대한 자세한 설명을 입력하실 수 있습니다." name="qesitmCn" ></textarea></td>\
			        </tr>\
					<tr>\
						<th>유형</th>\
						<td colspan="3">\
							<div class="f_wrap">\
								<select class="uselectbox" name="qesitmTyCode">\
									<option value="multiple">객관식</option>\
									<option value="answer">주관식</option>\
									<option value="table">표형</option>\
								</select>\
								&nbsp;\
								<select class="detailType multiple" name="qesitmTy">\
									<option value="">선택</option>\
									<option value="1">1. 그렇다 유형</option>\
									<option value="2">2. 만족 유형</option>\
									<option value="3">3. (5)~(1) 점수 유형</option>\
									<option value="4">4. 수동입력</option>\
								</select>\
								<select class="detailType table" style="display: none;" name="qesitmTy">\
								<option value="">선택</option>\
									<option value="6">1. (5)~(1) 점수 유형</option>\
									<option value="7">2. 수동입력</option>\
								</select>\
							</div>\
			            </td>\
			        </tr>\
		    	</tbody>\
			</table>\
 			<a href="#" class="set_x" title="삭제하기" style="right:-20px;"><img src="/template/manage/images/btn/del.gif"/></a>\
		</div>\
	</div>\
			 ');
	  this.value = '';
}


//문항 추가
function fnAddQuestion(){
	var widget = $("#questionDiv");
	var surveyIndex = widget.find(".tbset").length;
    var questionObj = new SurveyWidgetObj((surveyIndex + 1));
    widget.append(questionObj.widget);
    $(".tbset:eq("+surveyIndex+")").find("input:radio[class=essenAt]:input[value=Y]").attr("checked", true);


/*  $(".tbset:eq("+surveyIndex+")").find(".uselectbox").val("answer").trigger("change");
   $(".tbset:eq("+surveyIndex+")").find(".uselectbox").change();*/
}

function initOrderNum(){
	$(".tbset").each(function(i){
		$(this).find(".surveyTd").text(i+1);
	});
}


//유형
$(document).on("change",".uselectbox", function(){
	var val = $(this).val();
	var html = "";

	$(this).closest(".tbset").find(".detailType").hide();
	$(this).closest(".tbset").find(".addTr").remove();

	if($(this).closest("table").find("tr.tr-result").length != 0){
		$(this).closest("table").find("tr.tr-result").remove();
	}

	if("answer" == val){
		html += "<tr class=\"tr-result\">";
		html += 	"<td>&nbsp;</td>";
		html += 		"<td colspan=\"3\">";
		html += 		"<ul class=\"type5\">";
		html += 			"<li><textarea class=\"tarea02\" style=\"width:100%;height:45px;\" placeholder=\"주관식 입력\"></textarea></li>";
		html += 		"</ul>";
		html += 	"</td>";
		html += "</tr>";

		$(this).closest(".tbset").find("tbody").append(html);
	}else{
		$(this).closest(".tbset").find("select."+val).show();
		$(this).closest(".tbset").find("select."+val).val("");
	}
});


//항목에 따른 문항 변경
$(document).on("change",".detailType",function(){
	var val = $(this).val();
	var multiple1 = ["매우 그렇다","그렇다","그저 그렇다","그렇지 않다","매우 그렇지 않다"];
	var multiple2 = ["매우 만족","만족","그저 그렇다","불만족","매우 불만족"];
	var multiple3 = ["5","4","3","2","1"];
	var multiObj = {"1" : multiple1, "2" :multiple2, "3":multiple3};

	$(this).closest("table").find("tr.tr-result").remove();
	$(this).closest("table").find("tr.addTr").remove();

	var html = "";
	html += "<tr class=\"tr-result\">";
	html += 	"<td>&nbsp;</td>";
	html += 		"<td colspan=\"3\">";
	if(val == 4){
		html += "<a href=\"#\" class=\"btn_add btn_mngTxt\" style=\"margin:5px auto;\">클릭하면 추가</a>";
	}

	if(val != 6 && val != 7 ){
	html += 			"<ul class=\"type"+val+"\">";
	}else{
		html += "<div class=\"type6\">";
		html += "<a href=\"#\" class=\"btn_mngTxt add_table\" style=\"margin:5px auto;\">클릭하면 추가</a>";
		html += "<div class=\"ip_wrap\">";
		html += "<table class=\"chart2\" style=\"width:450px;\">";
		html += "<tbody>";
	}

	if($(this).closest(".tbset").find(".uselectbox").val() == "multiple"){
		for(var i=0; i < 5; i++){
			html += "<li>";
			html +=		"<label>";
			html +=			"<input type=\"radio\" class=\"rd01\">&nbsp;";
			if(val == 4){
				html +=				"<span><input type=\"text\" class=\"exCn\"/></span>";
			}else{
				html +=				"<span class=\"exCn\">"+multiObj[val][i]+"</span>";
			}
			html +=			"</label>";
			html +=			"<a href=\"#\" class=\"btn_delitem\"><img src=\"/template/manage/images/btn/del.gif\"/></a>";
			html +=		"</li>";
		}
	}else if(val == "7"){
		var html = "";
		html += "<tr class=\"addTr\">";
		html += "<th>표 설정</th>";
		html += "<td colspan=\"3\">";
		html += "가로 : <input type=\"text\" class=\"tableWidth\"/> X 세로 : <input type=\"text\" class=\"tableHeight\"/>";
		html += "<button class=\"fnTable\">표 만들기</button>";
		html += "</td>";
		html += "</tr>";

	}else if(val == "6"){
		for(var w = 1; w < 7; w++ ){
			html += "<tr>";
				for(var h = 1; h < 7; h++ ){
					if(w == 1){
						if(h == 1){
							html += "<td class=\"th_1\" style=\"width:250px;\"></td>";
						}else{
							html += "<td class=\"exCn height\">"+(h-1)+"</td>"
						}
					}else{
						if(h == 1){
							html += "<td class=\"th_1 free_textarea\">";
							html += "<textarea style=\"width:90%;height:30px;\" class=\"exCn width\" title=\"여기에 내용을입하세요.\" placeholder=\"보기주제를 입력하세요.\"></textarea>";
							html += "</td>";
						}else{
							html += "<td><input type=\"radio\"  class=\"rd01\"></td>";
						}
					}
				}
			html += "</tr>";
		}

		html += "</tbody>";
		html += "</table>";
		html += "</div>";
		html += "</div>";

	}

	$(this).parents(".tbset").find("tbody").append(html);
});

$(document).on("click",".fnTable",function(){
	$(this).closest("table").find("tr.tr-result").remove();

	var tempWidth = Number($(this).parents(".tbset").find(".tableWidth").val()) + 1;
	var tempHeight = Number($(this).parents(".tbset").find(".tableHeight").val()) + 1;
	var html = "";
	html += "<tr class=\"tr-result\">";
	html += 	"<td>&nbsp;</td>";
	html += 		"<td colspan=\"3\">";
	html += "<div class=\"type7\">";
	html += "<div class=\"ip_wrap\">";
	html +=	"<table class=\"chart2\" style=\"width:450px;\">";
	html += "<tbody>";

	for(var w = 0; w < tempWidth; w++ ){
		html += "<tr>";
			for(var h = 0; h < tempHeight; h++ ){
				if(w == 0){
					if(h == 0){
						html += "<td><textarea disabled/></td>";
					}else{
						html += "<td><textarea class=\"exCn height\" placeholder=\"보기주제를 입력하세요.\"/></td>"
					}
				}else{
					if(h == 0){
						html += "<td class=\"th_1 free_textarea\">";
						html += "<textarea style=\"width:90%;height:30px;\" class=\"exCn width\" title=\"여기에 내용을입하세요.\" placeholder=\"보기주제를 입력하세요.\"></textarea>";
						html += "</td>";
					}else{
						html += "<td></td>";
					}
				}
			}
		html += "</tr>";
	}
	html += "</tbody>";
	html += "</table>";
	html += "</div>";
	html += "</div>";
	html += "</td>";
	html += "</tr>";
	$(this).parents(".tbset").find("tbody").append(html);
});


//문항 삭제
$(document).on("click",".set_x",function(){
	if($("div.tbset").length > 1){
		$(this).closest("div.tbset").remove();
		initOrderNum();
	}
	return false;
});

//보기 삭제
$(document).on("click",".btn_delitem",function(){
	if($(this).closest("ul").find("li").length > 1){
		$(this).closest("li").remove();
	}
   	return false;
});

//수동입력 항목 추가(표형)
$(document).on("click",".add_table",function(){
var liHtml = "";
	liHtml = "<tr>"+
				"<td class=\"th_1 free_textarea\">" +
					"<textarea style=\"width:90%;height:30px;\" class=\"exCn width\" title=\"여기에 내용을입하세요.\" placeholder=\"보기주제를 입력하세요.\"></textarea>"+
				"</td>"+
	            "<td><input type=\"radio\"  class=\"rd01\" class=\"exCn height\"></td>"+
	            "<td><input type=\"radio\"  class=\"rd01\" class=\"exCn height\"></td>"+
	            "<td><input type=\"radio\"  class=\"rd01\" class=\"exCn height\"></td>"+
	            "<td><input type=\"radio\"  class=\"rd01\" class=\"exCn height\"></td>"+
	            "<td><input type=\"radio\"  class=\"rd01\" class=\"exCn height\"></td>"+
	        "</tr>"

	$(this).closest(".tr-result").find("tbody").append(liHtml);
	return false;
});

//수동입력 항목 추가(객관식)
$(document).on("click",".btn_add",function(){
var liHtml = "";
	liHtml = "<li>" +
				"<label>" +
					"<input type='radio' class='rd01'>&nbsp;" +
					"<span><input type='text' class='exCn' /></span>" +
				"</label>" +
				"<a href='#' class='btn_delitem'><img src='/template/manage/images/btn/del.gif'/></a>" +
			"</li>";

	$(this).closest(".tbset").find("tr.tr-result ul").append(liHtml);
	return false;
});

//문항 위치 이동
$(document).on("click",".upMove, .downMove",function(){
	   var tempClass = $(this).attr("class");
	   if(tempClass == "upMove"){
		   if($(this).closest(".tbset").prev(".tbset").length != 0){
			   $(this).closest(".tbset").prev(".tbset").before($(this).closest(".tbset"));
		   }else{
			   return false;
		   }
	   }else{
		   if($(this).closest(".tbset").next(".tbset").length != 0){
			   $(this).closest(".tbset").next(".tbset").after($(this).closest(".tbset"));
		   }else{
			   return false;
		   }
	   }
	   initOrderNum();
});





function fnSave(action){
	var questionsArray = new Array();
	var submitChk = true
	if($("#surveyTitle").val() == "" || $("#surveyTitle").val() == undefined){
		alert("설문명을 입력해 주세요");
		submitChk = false;
		return false;
	}else{
		$(".tbset").each(function(index){
			var hIndex = $(this).find(".tr-result .height").length;
			var wIndex = $(this).find(".tr-result .width").length;
			var tempIndex = $(this).find(".tr-result .exCn").length;
			var questionObj = new Object();

			questionObj.qesitmSn = (index+1);
			questionObj.essenAt = $(this).find("input[class=essenAt]:checked").val();
			questionObj.qesitmSj = $(this).find("textarea[name=qesitmSj]").val();
			questionObj.qesitmCn = $(this).find("textarea[name=qesitmCn]").val();
			questionObj.qesitmTyCode = $(this).find("select[name=qesitmTyCode]").val();
			var tempQesitmTyCode = $(this).find("select[name=qesitmTyCode]").val();
			if(tempQesitmTyCode != "answer"){
				if($(this).find("."+tempQesitmTyCode+" option:selected").val() == ""){
					alert("유형을 선택해 주세요.");
					submitChk = false;
				}

				questionObj.qesitmTy = $(this).find("."+tempQesitmTyCode).val();
				var examplesArray = new Array();
				$(this).find(".tr-result .exCn").each(function(eIndex,item){
					var examplesObj = new Object();
					if(questionObj.qesitmTy == 6){
						if($(item).hasClass("height")){
							examplesObj.exSn = hIndex--;
							examplesObj.exTy = "H";
							examplesObj.exCn = $(item).text();
						}else{
							examplesObj.exSn = wIndex--;
							examplesObj.exTy = "W";
							examplesObj.exCn = $(item).val();
						}

						if(examplesObj.exCn == "" || examplesObj.exCn == undefined){
							submitChk = false;
							alert("보기를 입력해주세요");
							return false;
						}

					}else if(questionObj.qesitmTy == 7){
						if($(item).hasClass("height")){
							examplesObj.exTy = "H";
							examplesObj.exSn = hIndex--;
						}else{
							examplesObj.exTy = "W";
							examplesObj.exSn = wIndex--;
						}

						if($(item).val() == "" || $(item).val() == undefined){
							submitChk = false;
							alert("보기를 입력해주세요");
							return false;
						}else{
							examplesObj.exCn = $(item).val();
						}
					}else{
						if(questionObj.qesitmTy == 4){
							if($(item).val() == "" || $(item).val() == undefined){
								submitChk = false;
								alert("보기를 입력해주세요");
								return false;
							}else{
								examplesObj.exCn = $(item).val();
							}
						}else{
							examplesObj.exCn = $(item).text();
						}
						examplesObj.exSn = (tempIndex - eIndex);
					}
					examplesArray.push(examplesObj);
				});
				questionObj.examples = examplesArray;
			}
			questionsArray.push(questionObj);
		});

		
		if(!confirm("저장하시겠습니까?")){
			return false;
		}

		if(submitChk){
			$.ajax({
				data : {"schdulNm": $("#surveyTitle").val(), "schdulClCode" : $("#schdulClCode").val(), "questionsArray" : JSON.stringify(questionsArray), "action" : action , "schdulId":$("#schdulId").val()},
				dataType : "json",
				type : "post",
				url : "/mng/lms/sur/insertSurvey.json",
				success:function(data){
					var resultCode = data.resultCode;
					if(resultCode == "Ex001"){
						alert("저장되었습니다.");
						location.href = "/mng/lms/sur/surveyManageList.do?schdulClCode="+$("#schdulClCode").val();
					}else{
						alert("저장에 실패했습니다.");
						return false;
					}
				}

			});
		}
	}

}


function fnCancel(){
	if(confirm("취소 하시겠습니까?")){
		location.href = "/mng/lms/sur/surveyManageList.do?schdulClCode="+$("#schdulClCode").val();
	}
}



