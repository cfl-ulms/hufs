
		function fnImageCheck(fileId) {
			var fileValue = $('#' + fileId).val();
			if(fileValue && fileValue != '') {
		    	if(fileValue.toUpperCase().indexOf('.PNG') == -1 && fileValue.toUpperCase().indexOf('.JPG') == -1 && fileValue.toUpperCase().indexOf('.GIF') == -1 && fileValue.toUpperCase().indexOf('.BMP') == -1) {
		    		alert("이미지만 첨부가 가능합니다.");
		    		$('#' + fileId).focus();
		    		return false;
		    	}
		    }

			return true;
		}
	    function fnGetCookie(name) {
	    	/*
	  	  var prefix = name + "=";

	  	  var cookieStartIndex = document.cookie.indexOf(prefix);
	  	  if (cookieStartIndex == -1) return null;
	  	  var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
	  	  if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;


	  	  return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
	  	  */
	    	var results = document.cookie.match ( '(^|;) ?' + name + '=([^;]*)(;|$)' );
			  if ( results )
				  return ( unescape ( results[2] ) );
			 else
				 return null;
	    }

	    function fnGetCookiePopup(name) {
	    	/*
	  	  var prefix = name + "=";

	  	  var cookieStartIndex = document.cookie.indexOf(prefix);
	  	  if (cookieStartIndex == -1) return null;
	  	  var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
	  	  if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;


	  	  return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
	  	  */
	    	var results = document.cookie.match ( '(^|;) ?' + name + '=([^;]*)(;|$)' );
			  if ( results )
				  return ( unescape ( results[2] ) );
			 else
				 return null;
	    }



	    function fnSetCookieValue(name, value) {
	    	var expireDate = new Date();
	    	expireDate.setFullYear(expireDate.getFullYear() + 1);
	    	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expireDate.toGMTString() + ";";
	    }

	    function fnSetCookiePopup( name, value, expiredays ) {
		  	  var todayDate = new Date();
		  	  todayDate.setDate( todayDate.getDate() + expiredays );
			  if(value != null) {
		  		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";";
			  } else {
				  document.cookie = name + "=; path=/; expires=" + todayDate.toGMTString() + ";";
			  }
		    }

	    function fn_egov_popupOpen_PopupManage(popupId,fileUrl,width,height,top,left,stopVewAt){

	    	var url = "/uss/ion/pwm/openPopupManage.do?";
	    	url = url + "fileUrl=" + fileUrl;
	    	url = url + "&stopVewAt=" + stopVewAt;
	    	url = url + "&popupId=" + popupId;
	    	var name = popupId;
	    	var openWindows = window.open(url,name,"width="+width+",height="+height+",top="+top+",left="+left+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes");

	    	if (openWindows) {openWindows.focus();}
	    }

	    function fnTabListLoading(frmId, frmUrl) {
			var frm = document.getElementById(frmId);
			if(frm) {
				if(frm.src == "about:blank") {
					frm.src = frmUrl;
				}
			}
		}

	    function initTabMenu(tabContainerID) {
	    	var tabContainer = document.getElementById(tabContainerID);
	    	var tabAnchor = tabContainer.getElementsByTagName("a");
	    	var i = 0;

	    	for(i=0; i<tabAnchor.length; i++) {
	    		if (tabAnchor.item(i).className == "tab")
	    			thismenu = tabAnchor.item(i);
	    		else
	    			continue;

	    		thismenu.container = tabContainer;
	    		thismenu.targetEl = document.getElementById(tabAnchor.item(i).href.split("#")[1]);
	    		thismenu.targetEl.style.display = "none";
	    		thismenu.imgEl = thismenu.getElementsByTagName("img").item(0);

	    		thismenu.onclick = function tabMenuClick() {
	    			currentmenu = this.container.current;
	    			if (currentmenu == this)
	    				return false;

	    			if (currentmenu) {
	    				currentmenu.targetEl.style.display = "none";
	    				if (currentmenu.imgEl) {
	    					currentmenu.imgEl.src = currentmenu.imgEl.src.replace("_on.png", ".png");
	    				} else {
	    					currentmenu.className = currentmenu.className.replace(" on", "");
	    				}
	    			}
	    			this.targetEl.style.display = "";
	    			if (this.imgEl) {
	    				this.imgEl.src = this.imgEl.src.replace(".png", "_on.png");
	    			} else {
	    				this.className += " on";
	    			}
	    			this.container.current = this;

	    			return true;
	    		};
	    		thismenu.onfocus = function tabMenuClick() {
	    			currentmenu = this.container.current;
	    			if (currentmenu == this)
	    				return false;

	    			if (currentmenu) {
	    				currentmenu.targetEl.style.display = "none";
	    				if (currentmenu.imgEl) {
	    					currentmenu.imgEl.src = currentmenu.imgEl.src.replace("_on.png", ".png");
	    				} else {
	    					currentmenu.className = currentmenu.className.replace(" on", "");
	    				}
	    			}
	    			this.targetEl.style.display = "";
	    			if (this.imgEl) {
	    				this.imgEl.src = this.imgEl.src.replace(".png", "_on.png");
	    			} else {
	    				this.className += " on";
	    			}
	    			this.container.current = this;

	    			return true;
	    		};
	    		if (!thismenu.container.first)
	    			thismenu.container.first = thismenu;
	    	}
	    	if (tabContainer.first)
	    		tabContainer.first.onclick();

	    }

	    function fnImagePreview(url) {
    		var URL 	=	'';
    		var winNM	=	'Preview';
    		var OPT		=	'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resizable=0,top=30,left=30,copyhistory=0 width=1024 height=859';
    		var imgwin = window.open(URL,winNM,OPT);

    		var what = url;
    		imgwin.focus();

    		 imgwin.document.open();

    		 imgwin.document.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n");
    		 imgwin.document.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n");

    		 imgwin.document.write("<title>크게보기</title>\n");   // 새창으로 페이지 제목

    		 imgwin.document.write("<sc"+"ript>\n");

    		 imgwin.document.write("function resize() {\n");

    		 imgwin.document.write("pic = document.il;\n");

    		 imgwin.document.write("if (eval(pic).height) { var name = navigator.appName\n");

    		 imgwin.document.write("  if (name == 'Microsoft Internet Explorer') { myHeight = eval(pic).height + 60;  myWidth = eval(pic).width + 10;\n");

    		 imgwin.document.write("  } else { myHeight = eval(pic).height + 56; myWidth = eval(pic).width + 8; }\n");

    		 imgwin.document.write("  clearTimeout();\n");

    		 imgwin.document.write("  var height = screen.height;\n");

    		 imgwin.document.write("  var width = screen.width;\n");

    		 imgwin.document.write("  var leftpos = width / 2 - myWidth / 2;\n"); //hjhj

    		 imgwin.document.write("  var toppos = height / 2 - myHeight / 2; \n");

    		 imgwin.document.write("  self.moveTo(leftpos, toppos);\n");

    		 imgwin.document.write("  self.resizeTo(myWidth, myHeight);\n");

    		 imgwin.document.write("}else setTimeOut(resize(), 100);}\n");

    		 imgwin.document.write("</sc"+"ript>\n");

    		 imgwin.document.write("</head><body style='margin:0px;padding:0px'>\n");

    		 imgwin.document.write("<img border=0 src="+what+" xwidth='100' xheight='9' name='il' onload='resize();' alt='이미지를 클릭하시면 창이 닫힙니다.' title='이미지를 클릭하시면 창이 닫힙니다.' onclick='javascript:window.close()' />\n");

    		 imgwin.document.write("</body></html>\n");

    		 imgwin.document.close();
    	}

	    function goUrl(value) {
		  	var obj = document.getElementById("sLink"+value);
		  	if(value != "") {
		  		if(obj.value == "") {
		  			alert("사이트를 선택하세요"); return false;
		  		}
		  		window.open(obj.value);
		  	}
		  }


	    function fn_byte_convert(val) {
	    	var size = "0 KB";

            if (val >= 1073741824)
                size = fn_comma(round(val / 1073741824, 2)) + " GB";
            else if (val >= 1048576)
                size = fn_comma(round(val / 1048576, 2)) + " MB";
            else if (val >= 1024)
                size = fn_comma(round(val / 1024, 2)) + " KB";
            else if (val > 0 && round(val < 1024, 2))
                size = "1 KB";

            return size;
	    }

	    function round(n, digits) {
	    	var factor = Math.pow(10, digits); // 정수부 반올림
	    	var increment = 5/(factor*10);

	    	return Math.floor((n+increment)*factor)/factor;
    	}


		function fn_comma(n) {
			var reg = /(^[+-]?\d+)(\d{3})/;
			n += '';
			while (reg.test(n)) {
				n = n.replace(reg, '$1' + ',' + '$2');
			}

			return n;
		}

	    function fn_egov_editor_file_add(userFile, wireData) {
	    	//확장자 추출 부분 작업
			var fileVal = userFile.orignlFileNm,
				ext = fileVal.split("."),
				splitSize = ext.length - 1,
				fileExt = ext[splitSize].toLowerCase(),
				icon = "icon-file";

			if(fileExt=="bmp" || fileExt=="gif" || fileExt=="jpeg" || fileExt=="jpg" || fileExt=="png"){
				icon = "icon-img";
			}

	    	var pageChk = $("#staffPage").val();
	    	var list = "";

	    	if(pageChk == "staff"){
	    		list += "<a href=\"#none\" class=\"attachment " + icon + " font-gray\" id=\"" + userFile.atchFileId + '_' + userFile.fileSn + "\">";
	    		list += 	"<span class=\"text\">" + userFile.orignlFileNm + "</span>";
	    		list +=		"<span class=\"file-remove\" onclick=\"fn_egov_editor_tmpr_file_del('" + wireData.editorId + "','" + userFile.tmprFileId + "');return false;\">-</span>"
	    		list += "</a>";
	    		//list += "<a href=\"/cmm/fms/deleteStreamLongFile.do?tmprFileId=" + userFile.tmprFileId + "\" class=\"" + userFile.atchFileId + '_' + userFile.fileSn + "\" onclick=\"fn_egov_editor_tmpr_file_del('" + wireData.editorId + "','" + userFile.tmprFileId + "');return false;\"><span class=\"file-remove\">-</span></a>";
	    	}else if(pageChk == "common"){
	    		list += "<a href='#' id='" + userFile.atchFileId + '_' + userFile.fileSn + "' onclick=\"fn_egov_editor_tmpr_file_del('" + wireData.editorId + "','" + userFile.tmprFileId + "');return false;\" class='attachment " + icon + " font-gray'>";
	    		list += 	"<span class=\"text\">" + userFile.orignlFileNm + "</span>";
	    		list += 	"<span class='file-remove'>-</span>";
	    		list += "</a>";
	    	}else{
	    		list += "<tr id='" + userFile.atchFileId + '_' + userFile.fileSn + "'>";
                list += "<td><img src='/template/manage/images/ico_file.gif' alt='파일'/> " + userFile.orignlFileNm + "</td>";
                list += "<td class='size'>" + fn_byte_convert(userFile.fileMg) + "</td>";
                list += "<td class='del'><a href=\"/cmm/fms/deleteStreamLongFile.do?tmprFileId=" + userFile.tmprFileId + "\" onclick=\"fn_egov_editor_tmpr_file_del('" + wireData.editorId + "','" + userFile.tmprFileId + "');return false;\"><img src=\"/template/manage/images/btn_sdelete.gif\"/></a></td>";
                list += "</tr>";
	    	}

	    	 $('#fileGroupId_' + wireData.editorId).val(userFile.atchFileId);
	         $('#fileCurrSize_' + wireData.editorId).val(wireData.totalFileMg);
	         $('#fileCurrCount_' + wireData.editorId).val(wireData.totalFileCount);
	         $('#multiFileList_' + wireData.editorId).append(list);


            /*if(userFile.IsEdit == 'True') {
            	$('#' + userFile.atchFileId + '_' + userFile.fileSn).remove();
            }*/

	         if(tinymce) {
		            var html = "";
		            if (isImage(userFile.orignlFileNm)) {
		            	html = '<img src="' + userFile.fileStreWebCours + '/' + userFile.streFileNm + '" class="' + userFile.atchFileId + '_' + userFile.fileSn + '">';
		            } else if(isVideo(userFile.orignlFileNm)) {
		            	html = '<video src="' + userFile.fileStreWebCours + '/' + userFile.streFileNm + '" controls class="' + userFile.atchFileId + '_' + userFile.fileSn + '"></video>';
		            }

		            if(html != ''){
		            	tinymce.activeEditor.insertContent(html);
		            }
	            }

	            fn_egov_file_clean_action(wireData.editorId);
		    }

		    function fn_egov_editor_tmpr_file_del(editorId, tmprFileId) {

		    	if(confirm('삭제하시면 본문에서도 삭제됩니다. 계속하시겠습니까?')) {
		    		var url = "/cmm/fms/deleteStreamLongFileByAjax.do";
		    		var param = {tmprFileId:tmprFileId};
		    		$.ajax({
						type : "POST",
						url : url,
						data : param,
						dataType : "json",
						success : function(data) {
							if(data.delCount > 0) {
			    				fn_egov_editor_file_del_action(data, editorId);
			    			}
						}, error: function(xhr, textStatus, errorThrown) {
				            alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
						}
					});
		    	}
		    }

		    function fn_egov_editor_file_del(editorId, estnAt, progrmId, atchFileId, fileSn) {

		    	if(confirm('삭제하시면 본문에서도 삭제됩니다. 계속하시겠습니까?')) {
		    		var url = "/cmm/fms/deleteFileInfByAjax.do";
		    		var param = {estnAt:estnAt, progrmId:progrmId, atchFileId:atchFileId, fileSn:fileSn};
		    		$.ajax({
						type : "POST",
						url : url,
						data : param,
						dataType : "json",
						success : function(data) {
							if(data.delCount > 0) {
			    				fn_egov_editor_file_del_action(data, editorId);
			    			}
						}, error: function(xhr, textStatus, errorThrown) {
				            alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
						}
					});
		    	}
		    }

		    function fn_egov_editor_file_del_action(data, editorId) {

		    	$('#fileCurrSize_' + editorId).val(data.totalFileMg);
	            $('#fileCurrCount_' + editorId).val(data.totalFileCount);
		    	$('#' + data.atchFileId + '_' + data.fileSn).remove();
		    	$('.' + data.atchFileId + '_' + data.fileSn).remove();
				tinyMCE.activeEditor.dom.remove(tinyMCE.activeEditor.dom.select('[class=' + data.atchFileId + '_' + data.fileSn + ']'));
				tinyMCE.activeEditor.dom.remove(tinyMCE.activeEditor.dom.select('[data-mce-p-class=' + data.atchFileId + '_' + data.fileSn + ']'));
				fn_egov_file_clean_action(editorId);
		    }

		    function fn_egov_file_clean_action(editorId) {
		    	if($('#multiFileList_' + editorId).children().length == 1) {
		    		$('#tr_file_empty_' + editorId).show();
		    	} else {
		    		$('#tr_file_empty_' + editorId).hide();
		    	}

		    	$('#lblMaxCount_' + editorId).html($('#posblAtchFileNumber_' + editorId).val());
		    	$('#lblMaxSize_' + editorId).html(fn_byte_convert($('#posblAtchFileSize_' + editorId).val()));
		    	$('#lblCurrCount_' + editorId).html($('#fileCurrCount_' + editorId).val());
		    	$('#lblCurrSize_' + editorId).html(fn_byte_convert($('#fileCurrSize_' + editorId).val()));
		    }

	    function fn_egov_downFile(url) {
	    	window.open(url);
	    }

	    var nowZoom = 100;
		function zoomIn() {
			nowZoom = nowZoom + 20;
			if(nowZoom >= 500) nowZoom = 500;
			zooms();
		}


		function zoomOut() {
			nowZoom = nowZoom - 10;
			if(nowZoom <= 70) nowZoom = 70;
			zooms();
		}

		function zoomReset(){
			nowZoom = 100;
			zooms();
		}

		function zooms(){
			document.body.style.zoom = nowZoom + '%';

			if(nowZoom==70){
				alert ("30%축소 되었습니다. 더 이상 축소할 수 없습니다.");
			}

			if(nowZoom==500){
				alert ("500%확대 되었습니다. 더 이상 확대할 수 없습니다.");
			}
		}

		function fnTotalSearch(form) {
	  		var searchCnd = $('#searchCnd option:selected').val();
	  		var searchWrd = $.trim($('#searchWrd').val());

			if(searchWrd == "") {
				alert("검색어를 입력해주세요");
				$('#searchWrd').focus();
				return false;
			}

			if(searchWrd.length < 2) {
				alert("검색어는 2자 이상 입력해주세요");
				$('#searchWrd').focus();
				return false;
			}

			if(searchCnd == 'bbs') {
				form.action = "/sch/bbsSearch.do";
			} else if(searchCnd == 'vod') {
				form.action = "/sch/vodSearch.do";
			} else if(searchCnd == 'cmy') {
				form.action = "/sch/cmySearch.do";
			}
	  	}

		function commonPopShow(id) {
        	$('#wrapper').append("<div id='layer_blind_box' style='position:absolute; top:0; left:0; width:100%; height:100%; background:#000; z-index:50;'></div>");
        	$('#layer_blind_box').css('opacity', 0.3);
        	//jQuery('#' + id).fadeIn(300);
        	$('#' + id).css('z-index', 100);
        	$('#' + id).fadeIn();
        }

        function commonPopHide(id) {
 			$('#layer_blind_box').remove();
 			$('#' + id).fadeOut();
        };

        var token = "";
        var loginYn = "";
        $(document).ready(function(){
        	/*$('a').click(function() {
        		ssoLinkAction(this);
        	});*/

        	$('#btnMyMenuAdd').click(function() {
				if (confirm('마이메뉴에 추가 하시겠습니까?')) {
					location.href = this.href;
				}
				return false;
			});

        	$.datepicker.setDefaults({
        	    dateFormat: 'yy-mm-dd',
        	    prevText: '이전 달',
        	    nextText: '다음 달',
        	    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        	    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        	    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
        	    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        	    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
        	    showMonthAfterYear: true,
        	    yearSuffix: '년'
        	});

        	$('.datepicker').datepicker({
    			dateFormat: 'yy-mm-dd'
    		});


        	$(document).on("keyup", ".numonly", function(key) {
        		if(key.keyCode == 8 || key.keyCode == 46) {
        			return;
        		}
        		var val = $(this).val().replace(/[^0-9]/gi,"");
        		$(this).val(val == "" ? 0 : val);
        	});


        	$('.noteOpen').click(function(){
        		var info = $(this).data('info');
        		if(info.type == 'view') {
        			var param = $.param(info);
        			fn_popup("/uss/ion/ntr/detailNoteRecptn.do?" + param,"NOTE");
        		} else {
        			fn_popup("/uss/ion/ntr/listNoteRecptn.do","NOTE");
        		}
        		return false;
        	});
        });

        /*function ssoLinkAction(aObj) {
	        if(token != "" && loginYn == "Y" && aObj.href.indexOf('javascript') == -1 && aObj.href.indexOf(document.domain) == -1) {
	    		var makeUrl = aObj.href;
	    		if(aObj.href.indexOf('token') == -1) {
					makeUrl = aObj.href + (aObj.href.indexOf('?') == -1 ? '?' : '&') + 'token=' + token;
				}
	    		aObj.href = makeUrl;
	    	}
        }*/


        /*******************************************************************************
         * Tab Button 처리
         * by moon
         */
        (function($) {
        	$.fn.tabToggle = function(obj) {
        		obj = $.extend({
        			tabClass : null,
        			titClass : null,
        			bbsList : null,
        			moreBtn : null,
        			height : 10,
        			top : 0,
        			left: 0
        		}, obj || {});
        		return this.each(function() {
        			var rowCount = $(this).children(obj.tabClass).size();
        			$(this).css({"position":"relative","height":obj.height});

        			$(this).children(obj.tabClass).each(function(index){
        				$(this).children(obj.titClass).css({"position":"absolute"});
        				$(this).children(obj.bbsList).css({
        					"position":"absolute",
        					"top" : obj.top,
        					"left" : obj.left
        				});
        				if(index != 0) {
        					$(this).children(obj.bbsList).css({"visibility":"hidden","height":"0"});
        					$(this).children(obj.moreBtn).css({"visibility":"hidden","height":"0"});
        				}
        				var curTab = this;
        				$(this).children(obj.titClass).children("a").hover(function() {
        					$(curTab).parent().children(obj.tabClass).each(function(index) {
        						$(this).children(obj.bbsList).css({"visibility":"hidden","height":"0"});
        						$(this).children(obj.moreBtn).css({"visibility":"hidden","height":"0"});
        					});
        					$(curTab).children(obj.bbsList).css({"visibility":"visible","height":"auto"});
        					$(curTab).children(obj.moreBtn).css({"visibility":"visible","height":"auto"});
        					$(obj.titClass).removeClass("tabActive");
        					$(this).parent(obj.titClass).addClass("tabActive");
        				});

        				$(this).children(obj.titClass).children("a").focus(function() {
        					$(curTab).parent().children(obj.tabClass).each(function(index) {
        						$(this).children(obj.bbsList).css({"visibility":"hidden","height":"0"});
        						$(this).children(obj.moreBtn).css({"visibility":"hidden","height":"0"});
        					});
        					$(curTab).children(obj.bbsList).css({"visibility":"visible","height":"auto"});
        					$(curTab).children(obj.moreBtn).css({"visibility":"visible","height":"auto"});
        					$(this).parent(obj.titClass).addClass("tabActive");
        				});
        			});
        			if(obj.moreBtn) {
        				$(obj.moreBtn).css({"position":"absolute","top":"0","right":"0"});
        			};
        		});
        	};
        })(jQuery);


        //============================================================================================
        /**
         * 공통 팝업 (by moon)
         */
		fn_popup = function() {
			var argLen = arguments.length;
			var popUrl = null;
			var popSeCode = null;
			var popWidth = 1020;
			var popHeight = 635;
			var popDialogNm = "FNPOPDIALOG";
			var popOptions = null;
			var options = null;
			var fullScreen = false;

			if(argLen < 1) {
				alert("잘못된 호출입니다.");
				return false;
			}

			switch(argLen) {
			case 5:
				popOptions = arguments[4];
			case 4:
				popHeight = arguments[3];
			case 3:
				popWidth = arguments[2];
			case 2:
				popSeCode = arguments[1];

				if(popSeCode != null) {
					switch(popSeCode) {
					case "ONECLICK": // 원클릭
						popWidth = 1020;
						popHeight = 660;
						popOptions = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes";
						break;
					case "GNR": // 수업자료, 수업지도안, 에듀큐레이션
						popWidth = 1020;
						popHeight = 635;
						break;
					case "SIMILARITY": // 유사문서
						popWidth = 1020;
						popHeight = 550;
						break;
					case "ICECREAM": // 아이스크림
						popWidth = 1090;
						popHeight = 665;
						break;
					case "YOUTUBE": // YOUTUBE
						fullScreen = true;
						break;
					case "DOC":
						fullScreen = true;
						break;
					case "MOV": // 동영상
						popWidth = 776;
						popHeight = 589;
						break;
					case "SWF": // 동영상
						popWidth = 776;
						popHeight = 589;
						break;
					case "WAV": // 사운드
						popWidth = 776;
						popHeight = 589;
						break;
					case "IMG": // 이미지
						fullScreen = true;
						break;
					case "TXT":
						fullScreen = true;
						break;
					case "NOTE": // 쪽지
						popWidth = 802;
						popHeight = 622;
						popOptions = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=no";
						break;
					case "MEMO": // 메모
						popWidth = 1060;
						popHeight = 650;
						popOptions = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes";
						break;
					case "MYFRIEND": // 친구
						popWidth = 1020;
						popHeight = 760;
						popOptions = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes";
						break;
					case "SHAREDPAPER": // 공유문서함
						popWidth = 1020;
						popHeight = 900;
						popOptions = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes";
						break;
					case "cal": // 검색결과 만족도 조사
						popWidth = 410;
						popHeight = 520;
						break;
					case "FULL":
						fullScreen = true;
						break;
					default :
						break;
					}
				}
			case 1:
				popUrl = arguments[0];
				popDialogNm += popSeCode;
				break;
			default :
				alert("Error : arguments");
				return false;
				break;
			}

			if(fullScreen === true) {
				window.open(popUrl, popDialogNm, 'height=' + (screen.availHeight-80) + ',width=' + screen.availWidth + ',top=0,left=0,toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes');
			}else {
				if(popOptions != null) options = popOptions;
				else options = "width="+popWidth+",height="+popHeight+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=no";
				window.open(popUrl, popDialogNm, options);
			}
			return false;
		};


		fn_setHomepage = function (homepage) {
			if( navigator.userAgent.indexOf("MSIE") != -1) {
				if(confirm("홈페이지를 시작페이지로 설정하시겠습니까?")) {
					document.body.style.behavior="url(#default#homepage)";
					document.body.setHomepage(homepage);
				}
			}else {
				alert("해당 브라우저는 지원을 하지 않습니다.");
			}
		};


		function getJson(url, param, callback) {
			$.ajax({
				type : "POST",
				url : url,
				data : param,
				dataType : "json",
				success : function(result) {
					if(result.success) {
						callback(result);
					} else {
						alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
					}
				}, error: function(xhr, textStatus, errorThrown) {
		            alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
				}
			});
		}

		function getHtml(url, param, callback) {
			$.ajax({
				type : "POST",
				url : url,
				data : param,
				dataType : "html",
				success : function(result) {
					callback(result);
				}, error: function(xhr, textStatus, errorThrown) {
		            alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
				}
			});
		}

		function zipCodePop(roadZip, roadAddr) {
			new daum.Postcode({
		        oncomplete: function(data) {
		            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

		            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
		            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
		            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
		            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

		            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
		            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
		            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
		                extraRoadAddr += data.bname;
		            }
		            // 건물명이 있고, 공동주택일 경우 추가한다.
		            if(data.buildingName !== '' && data.apartment === 'Y'){
		               extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
		            }
		            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
		            if(extraRoadAddr !== ''){
		                extraRoadAddr = ' (' + extraRoadAddr + ')';
		            }
		            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
		            if(fullRoadAddr !== ''){
		                fullRoadAddr += extraRoadAddr;
		            }

		            $('#' + roadZip).val(data.zonecode); //5자리 새우편번호 사용
		            $('#' + roadAddr).val(data.roadAddress);

		            // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
		            if(data.autoRoadAddress) {
		                //예상되는 도로명 주소에 조합형 주소를 추가한다.
		                var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
		                $('#guide').html('(예상 도로명 주소 : ' + expRoadAddr + ')');

		            } else if(data.autoJibunAddress) {
		                var expJibunAddr = data.autoJibunAddress;
		                $('#guide').html('(예상 지번 주소 : ' + expJibunAddr + ')');

		            } else {
		            	$('#guide').html('');
		            }
		        }
		    }).open();
		}

		function zipCodePop(norZip, norAddr, roadZip, roadAddr) {
			new daum.Postcode({
		        oncomplete: function(data) {
		            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

		            // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
		            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
		            var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
		            var extraRoadAddr = ''; // 도로명 조합형 주소 변수

		            // 법정동명이 있을 경우 추가한다. (법정리는 제외)
		            // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
		            if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
		                extraRoadAddr += data.bname;
		            }
		            // 건물명이 있고, 공동주택일 경우 추가한다.
		            if(data.buildingName !== '' && data.apartment === 'Y'){
		               extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
		            }
		            // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
		            if(extraRoadAddr !== ''){
		                extraRoadAddr = ' (' + extraRoadAddr + ')';
		            }
		            // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
		            if(fullRoadAddr !== ''){
		                fullRoadAddr += extraRoadAddr;
		            }

		            if(roadZip) {
			            // 우편번호와 주소 정보를 해당 필드에 넣는다.
			            $('#' + norZip).val(data.postcode); //6자리 기존우편번호 사용
			            $('#' + norAddr).val(data.jibunAddress); //기존 주소 사용

			            $('#' + roadZip).val(data.zonecode); //5자리 새우편번호 사용
			            $('#' + roadAddr).val(data.roadAddress);
		            } else {
		            	$('#' + norZip).val(data.zonecode); //5자리 새우편번호 사용
		 	            $('#' + norAddr).val(data.roadAddress); //기존 주소 사용
		            }

		            // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
		            if(data.autoRoadAddress) {
		                //예상되는 도로명 주소에 조합형 주소를 추가한다.
		                var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
		                $('#guide').html('(예상 도로명 주소 : ' + expRoadAddr + ')');

		            } else if(data.autoJibunAddress) {
		                var expJibunAddr = data.autoJibunAddress;
		                $('#guide').html('(예상 지번 주소 : ' + expJibunAddr + ')');

		            } else {
		            	$('#guide').html('');
		            }
		        }
		    }).open();
		}

		/**
		 * 앞뒤 공백 제거
		 */
		function trim(str)
		{
		     return str.replace(/(^[ \f\n\r\t]*)|([ \f\n\r\t]*$)/g, "");
		}

		//금액
		function numberWithCommas(x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		//랜덤함수
		function randOnlyString(leng){
			var ALPHA = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'],
				rN = '',
				leng = leng == null || leng == 'undefined' ? 8 : leng;

			for(var i=0; i<leng; i++){
				var randTnum = Math.floor(Math.random()*ALPHA.length);
				rN += ALPHA[randTnum];
			}

			return rN;
		}

$(document).ready(function(){
	//하위 메뉴 삭제
	$(".sub-menu-list-wrap").each(function(){
		if($(this).children("li").length == 0){
			$(this).remove();
		}
	});


	//공유하기
	$('.btn-share').click(function() {
		var href = $(this).data("href"),
			title = $(this).data("title");

		$(".sns-share-url").val(href);
		$("#sns_title").val(title);
		return false;
	});

	$(".btn_sns").click(function(){
		var type = $(this).data("type"),
			title = $("#sns_title").val(),
			strURL = $(".sns-share-url").val();

		if(type == "twitter"){
			snsUrl = "http://twitter.com/share?url=" + encodeURIComponent(strURL) + '&text' + encodeURIComponent(title);
		}else if(type == "facebook"){
			snsUrl = "http://www.facebook.com/share.php?u=" + encodeURIComponent(strURL);
		}

		window.open(snsUrl);
	});

	//좋아요(wishlist)
	$(".btn-wishlist").click(function(){
		var code = $(this).data("code"),
			id = $(this).data("id"),
			ele = $(this),
			url = "/uss/ivp/insertIvp.json";

		if($(this).hasClass("on")){
			url = "/uss/ivp/deleteIvp.json";
		}

		if(code && id){
			$.ajax({
				type : "post"
				, url : url
				, data : {trgetTyCode : code, trgetId : id}
				, dataType : "json"
				, success : function(data){
					if("Y" == data.successYn){
						if(ele.hasClass("on")){
							ele.removeClass("on");
						}else{
							ele.addClass("on");
						}
					}
				}, error : function(){
					alert("데이터 불러 들이는데 실패했습니다.");
				}
			});
		}
	});

	//페이지 이동
	$(".goPage").click(function(){
		location.href = $(this).data("url");
		return false;
	});
});

//숫자만 입력
$(document).on("keyup", ".onlyNum", function(){
	var inpVal = $(this).val().replace(/[^0-9]/g,'');
	$(this).val(inpVal);
});
/*
$(".onlyNum").bind("input",function(event){
	var inpVal = $(this).val().replace(/[^0-9]/g,'');
	$(this).val(inpVal);
});
*/
//금액
$(".money").bind("input",function(event){
	var inpVal = $(this).val().replace(/,/gi,"").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	$(this).val(inpVal);
});

//확장자 추출
function fileExt(fileNm) {
	var ext          = fileNm.split(".");
	var splitSize    = ext.length - 1;
	var fileExt      = ext[splitSize].toLowerCase();

	return fileExt;
}

