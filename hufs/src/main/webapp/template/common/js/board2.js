
ctgryObj = function(upperCtgryId, ctgryId, ctgryNm, ctgryLevel) {
		this.upperCtgryId = upperCtgryId;
		this.ctgryId = ctgryId;
		this.ctgryNm = ctgryNm;
		this.ctgryLevel= ctgryLevel;
}

function fnFindctgryObj(ctgryId) {
	for(var i = 0 ; i < boardCateList.length ; i++){
		var cate = boardCateList[i];
		if(cate.ctgryId == ctgryId) {
			return cate;
		}
	}
	
	return null;
}

function fnCtgryChange(ctgryLevel) {
	var actObj = document.getElementById("ctgry" + ctgryLevel);
	var targetObj = document.getElementById("ctgry" + (ctgryLevel + 1));
	if(targetObj != null) {
		for(var cmIdx = ctgryLevel + 1 ; cmIdx <= boardCateLevel ; cmIdx++){
			var cmObj = document.getElementById("ctgry" + cmIdx);
			if(cmObj != null) {
				for(var i = cmObj.length - 1 ; i > 0 ; i--){
					cmObj.options.remove(i);
				}
			}
		}
		
		var currVal = actObj.options[actObj.selectedIndex].value;
		var currCtgryObj = fnFindctgryObj(currVal);
		if(currCtgryObj != null) {
			var pos = 1;
			for(var i = 0 ; i < boardCateList.length ; i++){
				var cate = boardCateList[i];
				if(cate.upperCtgryId == currCtgryObj.ctgryId && cate.ctgryLevel == ctgryLevel + 1) {
					targetObj.options[pos] = new Option(cate.ctgryNm, cate.ctgryId);
					pos++;
				}
			}
		}
	}
}

function fnCtgryInit(searchCateList) {
	var arr = searchCateList.replace('[','').replace(']','').split(",");
	for(var i = 0; i < arr.length; i++) {
		if(arr[i].trim() != '') {
			var cmObj = document.getElementById("ctgry" + (i+1));
			if(cmObj != null) {
				for(var y = 0; y < cmObj.length; y++){
					if(cmObj[y].value == arr[i].trim()) {
						cmObj[y].selected = true;
						fnCtgryChange(i+1);
						break;
					}
				}
			}
		}
	}
}


String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g, "");
}

function fn_egov_SelectBoxValue(sbName)
{
	var FValue = "";
	for(var i=0; i < document.getElementById(sbName).length; i++)
	{
		if(document.getElementById(sbName).options[i].selected == true){
			
			FValue=document.getElementById(sbName).options[i].value;
		}
	}
	
	return  FValue;
}

function fn_egov_bbs_basic_regist(frm) {

	return validateBoard(frm);
}


function fn_egov_bbs_editor(adfile_config) {
	var editorOption = {
	  selector: '#' + adfile_config.editorId,
	  language : 'ko_KR',
	  //height: 500,
	  menubar: false,
	  statusbar: false,
	  relative_urls : false,
	  plugins: [
	    'advlist autolink lists link image charmap print preview anchor textcolor',
	    'searchreplace visualblocks code fullscreen',
	    'insertdatetime media table contextmenu paste code help wordcount'
	  ],
	  toolbar1: '',
	  toolbar2: 'fullscreen | undo redo | fontselect fontsizeselect bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | code',
	  content_css: [
	    '/template/lms/font/font.css'
	    , '/template/lms/css/common/base.css'
	  ]
	  , setup: function (editor) {
		  editor.addButton('ImageButton', {
			  text: '이미지',
			  icon: 'image',
		      tooltip: "이미지 추가",
		      onclick: function () {
		    	var uploader = $('.fileupload.' + adfile_config.editorId);
				uploadInit(uploader, adfile_config, 'Image');
		      }
		  });
		  /*
		  editor.addButton('VideoButton', {
			  text: '동영상',
			  icon: 'media',
			  tooltip: "동영상 추가",
		      onclick: function () {
		    	  var uploader = $('.fileupload.' + adfile_config.editorId);
					uploadInit(uploader, adfile_config, 'Video');
		      }
		  });
		  */
	  }
	};
	
	
	
	
	if(adfile_config.fileAtchPosblAt == 'Y') {
		editorOption.toolbar1 = 'ImageButton | VideoButton'
		
		$('#btnAddFile_' + adfile_config.editorId).show();
		
		fn_egov_file_clean_action(adfile_config.editorId);
		if(adfile_config.atchFileId == '') {
			adfile_config.atchFileId = getAtchFileId();
		}
		
		
		$('.file_btn').click(function() {
			
			var uploader = $('.fileupload.' + adfile_config.editorId);
			uploadInit(uploader, adfile_config, 'All');
			
			return false;
		});
	}
	
	tinymce.init(editorOption);
	
	$('#nttSj').focus();
}

$(document).ready(function(){
	var view_cont_restrict_length = $('.view_cont').width();
	$('.view_cont img').each(function(index) {
		
		var isResize = false;
		if($(this).attr("width") && $(this).attr("height")) {
			var ratio = $(this).attr("width") / $(this).attr("height"); // 비율
		    
		    if($(this).attr("width") > view_cont_restrict_length) {
		    	var newheight = Math.round(view_cont_restrict_length / ratio);
		    	$(this).attr({width:view_cont_restrict_length, height:newheight});  // 폭과 높이를 새로 지정
		    	isResize = true;
		    }
		} else {
			var img = new Image();
		    img.src = $(this).attr("src");
		    var ratio = img.width / img.height; // 비율
		    
		    if(img.width > view_cont_restrict_length) {
		    	var newheight = Math.round(view_cont_restrict_length / ratio);
		    	$(this).attr({width:view_cont_restrict_length, height:newheight});  // 폭과 높이를 새로 지정
		    	isResize = true;
		    }
		}
		
		if(isResize) {
			//$(this).css("cursor","pointer").click(function () {
			$(this).click(function () {
				//fnOriginImagePreview($(this).attr("src"));
				location.href = $(this).attr("src");
			}).attr("title", "원본보기");
		}
	});
});

function fnOriginImagePreview(url) {
	var URL 	=	'';
	var winNM	=	'Preview';
	var OPT		=	'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1,width=' + $(window).width() + ',height=' + $(window).height();		
	var imgwin = window.open(URL,winNM,OPT);	

	var what = url;
	imgwin.focus(); 

	 imgwin.document.open(); 

	 //imgwin.document.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n");  
	 imgwin.document.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n");

	 imgwin.document.write("<title>원본보기</title>\n");   // 새창으로 페이지 제목

	 imgwin.document.write("</head><body style='margin:0px;padding:0px'>\n"); 

	 imgwin.document.write("<a href='#'><img border=0 src="+what+" alt='이미지를 클릭하시면 창이 닫힙니다.' title='이미지를 클릭하시면 창이 닫힙니다.' onclick='javascript:window.close()' /></a>\n"); 

	 imgwin.document.write("</body></html>\n"); 

	 imgwin.document.close();
}