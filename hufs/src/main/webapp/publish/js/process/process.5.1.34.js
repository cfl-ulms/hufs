$(document).ready(function () {

  //과제 내용 펼치기,접기 (학생화면 수업(class.js)에 사용했던 스크립트 공통으로 이동)
  if ($(".expansionPanel").length) {
    var $openToggle = $(".expansionPanel .openToggle");
    // var flag = $openToggle.data("open"); // flag == true 박스 열린상태 // false 닫힌상태

    $openToggle.on("click", function () {
      var $this = $(this);
      $this.siblings(".panel").slideToggle(200);
      $this.toggleClass('on');
    });
  }
  
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = "<div class='page'>" + printContents + "</div>";

    window.print();

    document.body.innerHTML = originalContents;
  }

  $(".popupPrint").click(function () {
    printDiv("printContent");
  });

  /*=================================================
            top 버튼
  =================================================*/
  var BTNTOP = {
    $btnGoTop: $(".btn-top-wrap"),
    $btnTop: $(".btn-top-wrap .btn-go-top"),
    prevTop: null,
    showPosition: 300,
    init: function () {
      this.btnShowHide();
    },
    onAniTop: function (e) {
      e.preventDefault();
      $("html, body").animate({
        scrollTop: 0
      }, 400);
    },
    btnShowHide: function () {
      $(window).on("scroll", function (e) {
        var $this = $(this);
        if ($this.scrollTop() >= BTNTOP.showPosition) {
          BTNTOP.$btnGoTop.fadeIn();
        } else {
          BTNTOP.$btnGoTop.fadeOut();
        }
        if (document.getElementById("footer")) {
          var footerTop = $("#footer").offset().top;
          var footerHeight = $("#footer").height();

          if (footerTop + 15 <= $this.scrollTop() + $this[0].innerHeight) {
            BTNTOP.$btnGoTop.addClass("bottom-fixed");
            if (BTNTOP.$btnGoTop.hasClass("bottom-fixed")) {
              BTNTOP.$btnGoTop.css("bottom", footerHeight + 15);
            }
          } else {
            BTNTOP.$btnGoTop.removeClass("bottom-fixed");
            BTNTOP.$btnGoTop.css("bottom", "15px");
          }
        }
      }).scroll();
    }
  };

  BTNTOP.init();
  BTNTOP.$btnTop.on("click", BTNTOP.onAniTop);

});