$(document).ready(function () {

  if (document.getElementsByClassName('boardTypeGraph')) {
    var myChartArr = [];

    var chartBarOption2 = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#54c374', '#396fd4'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정', '전체'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        },
      },
      yAxis: {
        type: 'category',
        data: ['총 시간', '주 회', '일 시간',],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [60, 25, 15],
        },
        {
          name: '전체',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [80, 30, 28],
        },
      ]
    };

    var chartBarOption3 = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#54c374', '#396fd4'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정', '전체'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['총\n학생수', '평가\n횟수', '과정\n과제수'],
        axisLabel: {
          lineHeight: 17
        },
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [40, 18, 20],  //data 값 변경
        },
        {
          name: '전체',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [48, 28, 33],  //data 값 변경
        }
      ]
    };

    var chartGenderBarOption = {
      color: ['#54c374', '#396fd4'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        show: true,
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정', '전체'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능) (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능) (값 확인후 변경가능)
        axisLabel: {
          formatter: '{value}%'
        },
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['남자', '여자'],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          stack: '비율',
          barWidth: 60,
          label: {
            normal: {
              show: true,
              position: 'inside',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [45.5, 50] //data 값 변경
        },
        {
          name: '전체',
          type: 'bar',
          stack: '비율',
          barWidth: 60,
          label: {
            normal: {
              show: true,
              position: 'inside',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [54.5, 50] //data 값 변경
        },
      ]
    };

    var chartStudentBarOption = {
      color: ['#396fd4', '#54c374', '#ddd'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['본교', '그 외 대학', '일반'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#54c374', '#ddd'],
          radius: '65%',
          center: ['50%', '43%'],
          hoverAnimation: false,
          data: [
            { value: 19, name: '본교'},  //data 값 변경
            { value: 7, name: '그 외 대학'},  //data 값 변경
            { value: 4, name: '일반'},  //data 값 변경
          ],
          labelLine: {
            normal: {
              show: true,
            },
          },
          label: {
            normal: {
              show: true,
              formatter: '{c}',
              textStyle: {
                color: '#333',
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },

        }
      ]
    };

    var chartSatisfyPieOption = {
      color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
      animation: false,
      legend: {
        orient: 'vertical',
        right: 0,
        top: 30,
        data: ['매우만족', '만족', '보통', '불만족', '매우불만족'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
          radius: '100%',
          center: ['35%', '50%'],
          hoverAnimation: false,
          animation: false,
          data: [
            { value: 15, name: '매우만족' },
            { value: 4, name: '만족' },
            { value: 3, name: '보통' },
            { value: 2, name: '불만족' },
            { value: 1, name: '매우불만족' },
          ],
          labelLine: {
            normal: {
              show: false,
            }
          },
          label: {
            normal: {
              show: false,
            }
          },
        }
      ]
    };

    //echart 셋팅
    $(".boardTypeGraph").each(function (idx, el) {
      var myChart = echarts.init(el);
      var chartType = el.dataset.chartType;

      myChartArr.push(myChart);

      if (chartType == "bar2") {
        //차트옵션 셋팅
        myChart.setOption(chartBarOption2);
      } else if (chartType == "bar3") {
        //차트옵션 셋팅
        myChart.setOption(chartBarOption3);
      } else if (chartType == "genderbar") {
        //차트옵션 셋팅
        myChart.setOption(chartGenderBarOption);
      } else if (chartType == "studentPie") {
        //차트옵션 셋팅
        myChart.setOption(chartStudentBarOption);
      } else if (chartType == "satisfypie") {
        //차트옵션 셋팅
        myChart.setOption(chartSatisfyPieOption);
      }
    });

    $(window).on('resize', function () {
      myChartArr.forEach(function (el, idx) {
        el.resize();
      });
    });

    $(".btn-expansion").on("click", function () {
      myChartArr.forEach(function (el, idx) {
        setTimeout(function () {
          el.resize();
        }, 310); // transition 끝난후 resize
      });
    });
  }
});