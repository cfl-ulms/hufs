$(document).ready(function(){
  // 문제 순서변경
  $(".dd").nestable({
    maxDepth: 1
  });

  $('.dd').on('change', function () {
    var $this = $(this); // 문제 list
    //문제 등록형일 경우
    if ($this.parents().hasClass("quiz-write-wrap")){
      $(".checkboxNum").each(function (idx){
        idx++; // idx 1 부터 시작
        $(this).text(idx); // 체크박스 번호
      });
    }
    if ($this.parents().hasClass("quiz-save-wrap")) {
      $($(".quizSaveNum").get().reverse()).each(function (idx) {
        idx++; // idx 1 부터 시작
        $(this).text(idx+". "); // 번호
      });
    }
  });
  
});