$(document).ready(function () {
  // 아코디언 형태 이벤트
  $(".acoArrow").on("click", function () {
    var $this = $(this);
    var $thisInnerList = $this.parents(".innerList");
    if ($this.hasClass("open")){
      $this.removeClass("open");
    }else {
      $this.addClass("open");
    }
    $thisInnerList.find(".aco-content").slideToggle();
  });

  // 체크박스 선택 이벤트
  var $infoCheck = $(".infoCheck[type='checkbox']"); // each checkbox
  var $allAgree = $(".allAgree"); // 전체동의
  $allAgree.on("change click",function(){
    if ($allAgree.prop("checked")){
      $infoCheck.prop("checked",true); //전체 체크
    }else {
      $infoCheck.prop("checked", false); //전체 해제
    }
  });
    // 각각 체크할 경우 전체동의 체크박스 이벤트
  $infoCheck.on("change click", function(){ 
    var infoCheckLength = $infoCheck.length;
    var checkedLength = $(".infoCheck[type=checkbox]:checked").length;
    if (infoCheckLength > checkedLength) {
      $allAgree.prop("checked", false);
    }else {
      $allAgree.prop("checked", true);
    }
  });
});