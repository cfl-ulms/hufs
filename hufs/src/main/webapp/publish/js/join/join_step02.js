$(document).ready(function () {
  // 소속 선택
  $(".group-sort-lables [type='radio']").on("change", function () {
    var $this = $(this);
    var $target = $this.data("group");
    $(".group-sort-form").removeClass('open');
    $this.parents(".group-sort-wrap").find($target).addClass("open");
  });
  
  // 관심 특수외국어 선택 
  $(".join-contents-wrap .tab-list").on("click",function(){
    $(this).toggleClass("on");
    if ($(".join-contents-wrap .tab-list.on").length > 3){
      $(this).removeClass("on");
      alert("관심 특수외국어는 3개 언어까지만 선택하실 수 있어요!");
    }
  });

});