$(document).ready(function() {
  Lms.common_ui.init();

  //과제 내용 펼치기,접기 (학생화면 수업(class.js)에 사용했던 스크립트 공통으로 이동)
  if ($(".subjectToggle").length) {
    var $subjectToggle = $(".subjectToggle");
    var flag = $subjectToggle.data("open"); // flag == true 박스 열린상태 // false 닫힌상태

    var subjectHeaderH = $(".subjectViewContain .subject-title-wrap").outerHeight() + 29; // 헤더 높이값 + 박스 패딩값
    var subjectBoxH = $subjectToggle.siblings(".subject-view-wrap").css('height', 'auto').outerHeight(); // reset value

    // 함수 호출
    subjectTextChange(flag);
    subjectContainer($subjectToggle, false);

    $subjectToggle.on("click", function () {
      var $this = $(this);
      subjectContainer($this, true);
    });

    function subjectContainer($this, $isAnimate) {
      var $target = $this.siblings(".subject-view-wrap");
      if (!flag) { //닫힘
        if ($isAnimate) {
          $target.stop().animate({ "height": subjectHeaderH }, 600);
        } else {
          $target.css({ "height": subjectHeaderH });
        }
        flag = true;
        subjectTextChange(flag);
      } else { //펼치기
        $target.outerHeight(subjectHeaderH); //init & start height
        if ($isAnimate) {
          $target.stop().animate({ "height": subjectBoxH }, 600);
        } else {
          $target.css({ "height": subjectBoxH });
        }

        flag = false;
        subjectTextChange(flag);
      }
    }

    function subjectTextChange(flag) {
      if (!flag) { // 펼쳐진 상태
        $subjectToggle.children(".text").text("과제내용 접기");
        $subjectToggle.removeClass('on'); // 화살표 방향 바꾸기

      } else {
        $subjectToggle.children(".text").text("과제내용 펼치기");
        $subjectToggle.addClass('on'); // 화살표 방향 바꾸기
      }
    }
  }

  //썸네일뷰 체크 박스 클릭했을 경우
  if ($(".subject-feedback-header .checkbox")) {
    $(".subject-feedback-header .checkbox [type=checkbox]").change(function () {
      var $this = $(this);
      $this.parents('.subject-feedback-card').toggleClass('on');
    });
  }

  //리스트 체크크박스 선택 시
  if ($(".selectedList").length) {
    $(".selectedList [type=checkbox]").change(function () {
      var $this = $(this);
      $this.parents('tr').toggleClass('bg-point-light');
    });
  }
});

var Lms = Lms || {};

Lms.common_ui = {
  slyArr: [], //Sly scroll 사용시
  media_width: 1200,
  init: function() {
    // 상단 네비게이션 호출
    this.gnb_evt_listener();
    // dotdotdot lib 호출
      this.dotdotdot();
    // slick lib 호출
    this.slick_init();
    // 넓은화면
    this.zoom_evt_listener();
    // select box lib 호출
    if ($(".select2").length) {
      this.select2_init($(".select2"));
    };
    if ($(".froala_editor").length) {
      this.froala_init();
      this.froala_readonly();
    }
    // 달력 lib 호출
    this.datepicker();
    // 모달창 호출
    this.modal_open_evt_listener($(".btnModalOpen"));
    this.modal_close_evt_listener($(".btnModalClose"));
    this.modal_close_evt_listener($(".btnModalConfirm"));
    this.modal_close_evt_listener($(".btnModalCancel"));
    // 공유하기 클립보드 호출
    this.modal_share($(".copyUrl"));
    // window popup 호출
    this.window_popup_open_evt_listener($(".btnWindowOpen"));
    this.window_popup_close_evt_listener($(".btnWindowClose"));
    // 아코디언 형태 호출
    this.accordion_common($(".acoArrowCommon"));
    if ($(".slyWrap").length) {
      // setTimeout(function(){
        this.sly_init();
      // },400); //폰트 랜더시간
    }
    this.resize_evt_listener();
    this.text_count_evt_listener();
    this.text_paste_evt_listener(document.querySelectorAll(".onlyText[contenteditable]")); //텍스트편집기 붙여넣기 이벤트
    this.browser_check(); //브라우저체크
    if ($(".table-inside-checkbox").length) {
      this.check_table_evt_listener();
    }
    if ($(".tbl-main").length){
      this.table_wheel_evt_listener();
    }
  },

  resize_evt_listener: function() {
    $(window).resize(function () {
      if ($(".slyWrap").length) {
        Lms.common_ui.sly_evt_reloader();
      }
    });
  },
  gnb_evt_listener: function(){
    var $gnbBg = $(".nav-bg");

    if ($(".sub-menu-list-wrap").length){

      $(".nav-list.depth-1").on('mouseenter', function() {
        if ($(this).find(".sub-menu-list-wrap").length){
          $gnbBg.slideDown(50);
        }else {
          $gnbBg.slideUp(50);
        }
      });

      $(".nav-list-wrap").on('mouseleave', function(e) {
        if ($(this).find(".sub-menu-list-wrap").length) {
          $gnbBg.slideUp(50);
        }
      });
    }

    $(".btn-menu-trigger").on("click", function() {
      // console.log("전체메뉴 오픈");
    })
  },
  zoom_evt_listener: function(){
    var $zoomButton = $(".btn-expansion");
    var $container = $(".page-container");
    var $panel = $(".page-container .expansion-panel");
    $zoomButton.on("click", function () {
      $container.toggleClass("closeSidenav");
      $panel.toggleClass("isWide");
      if ($(".slyWrap").length) {
        setTimeout(function(){
          Lms.common_ui.sly_evt_reloader();
        },310); // sidenav-bar transition 값 끝난 이후 호출
      }
    });
  },
  sly_init : function() {
    var that = this;
    var options = {
      horizontal: 1,
      itemNav: 'centered',
      smart: 1,
      activateOn: 'click',
      mouseDragging: 1,
      touchDragging: 1,
      releaseSwing: 1,
      startAt: 0,
      scrollBar: $('.slyWrap').parent().find('.scrollbar'),
      scrollBy: 0,
      activatePageOn: 'click',
      speed: 300,
      elasticBounds: 1,
      easing: 'easeOutExpo',
      dragHandle: 1,
      dynamicHandle: 1,
      clickBar: 1,
    };
    
    Array.prototype.slice.call(document.querySelectorAll('.slyWrap')).forEach(function (el, idx) {
      that.slyArr[idx] = new Sly('.slyWrap', options).init();
      that.sly_scroll_reloader(that.slyArr[idx]);
    });
  },
  sly_evt_reloader : function(){
    var that = this;
    that.slyArr.forEach(function (constructor, idx) {
      constructor.reload();
      that.sly_scroll_reloader(constructor);
    });
  },
  sly_scroll_reloader : function($sly){
    //가로 스크롤 숨기기
    if ($sly.pos.start == $sly.pos.end) {
      $(".board-tab-wrap .scrollbar").hide();
    } else {
      $(".board-tab-wrap .scrollbar").show();
    }
  },
  select2_init: function($select2_el) {
    $select2_el.each(function() {
      var $this = $(this);
      if ($this.hasClass("search")) {//검색창 활성화
        $this.select2({
          theme: $this.data("select"),
          width: "100%",
          "language": {
            "noResults": function () {
              return "<span style='color: #aaa;'>검색결과가 없습니다.</span>";
            }
          },
          escapeMarkup: function (markup) {
            return markup;
          }
        });
      } else {
        $this.select2({ //검색창 비활성화
          theme: $this.data("select"),
          width: "100%",
          minimumResultsForSearch: Infinity,
        });
      }
      if($this.data("link")){
        //옵션선택 시 페이지 이동
        $this.on("select2:select", function (e) {
          var data = e.params.data;
          var url = data.id;
          location.href = url;
        });
      }
    })
  },
  froala_init: function () {
    $('.froala_editor').froalaEditor({
      toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertTable', '|', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo'],
      fontFamily: {
        'sans-serif': '한자',
        'Arial,Helvetica,sans-serif': 'Arial',
        'Impact,Charcoal,sans-serif': 'Impact',
        'Tahoma,Geneva,sans-serif': 'Tahoma'
      }
    });
  },
  froala_readonly: function() {
    var $froala_read_only = $('.froala-read-only')

    $froala_read_only.froalaEditor('edit.off');
    $froala_read_only.froalaEditor('toolbar.hide');
  },
  dotdotdot : function(){
    var $dotdotdot = $(".dotdotdot");

    if ($dotdotdot.length) {
      $dotdotdot.dotdotdot({
        watch: window
      });
    }
  },
  slick_init: function () {
    if (document.querySelector("[data-slick]")) {
      var slickElementList = document.querySelectorAll("[data-slick]");
      //slick 첫 로딩
      Array.prototype.slice.call(slickElementList).forEach(function (el) {
        if (el.dataset.slick = "true") {
          el.style.visibility = "visible";
        }
      });
    }
  },
  datepicker: function () {
    if ($(".datepicker").length){
      $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
      }).val();
    }
  },
  modal_open_evt_listener: function($trigger_btn) {
    $trigger_btn.on("click", function(e) {
      e.preventDefault();
      var $this = $(this);
      
      var type = $this.data("modal-type");
      var size = $this.data("modal-size");
      var header = $this.data("modal-header");
      var text = $this.data("modal-text");
      var subtext = $this.data("modal-subtext");
      var btntext = $this.data("modal-rightbtn");
      // var btnlink = $this.data("modal-link");
      
      Lms.common_ui.show_alert_fn({
        type: type,
        size: size,
        header: header,
        text: text,
        subtext: subtext,
        btntext: btntext,
        // btnlink: btnlink
      });
    });
  },
  show_alert_fn: function (option) {
    var $alert_modal = $("#" + option.type + "_modal");

    if ($("body").hasClass("modal-open")) { // 모달창 열린 상태에서 또 열리는 경우
      $(".alert-modal").hide();
      $("body").removeClass("modal-open");
    }
    if (option.type === "confirm") {
      $alert_modal.find(".btnModalConfirm").text(option.btntext);
      // if (option.btnlink) {
      //   $alert_modal.find(".btnModalConfirm").attr("onclick", "location.href='" + option.btnlink + "'");
      // }
    }
    $alert_modal.find(".modal-dialog").addClass(option.size)
    $alert_modal.find(".modal-title").text(option.header);
    $alert_modal.find(".modal-text").html(option.text);
    $alert_modal.find(".modal-subtext").html(option.subtext);
    $alert_modal.show();
    $("body").addClass("modal-open");

  },
  modal_close_evt_listener: function($trigger_btn) {
    $trigger_btn.on("click", function() {
      $(this).parents(".alert-modal").hide();
      $("body").removeClass("modal-open");
    });
  },
  modal_share : function($sharebtn){
    $sharebtn.on("click", function () {
      //주소복사(클립보드) 버튼
      var $copyurl = $(this);
      var toolTip = $copyurl.siblings(".tooltip");

      // clipboard
      $copyurl.select();
      document.execCommand("copy");
      // tollTip show
      toolTip.addClass('on');
      setTimeout(function () {
        toolTip.removeClass('on');
      }, 1000);
    });
  },
  window_popup_open_evt_listener: function ($openbtn) {
    $openbtn.on("click", function (e) {
      e.preventDefault();
      var $this = $(this);
      var thisUrl = $this.attr("href");
      var popup_width;
      var popup_hieght;
      var popup_x; // 위치값 x
      var popup_y; // 위치값 y
      var popup_options;

      var thisWidth = $this.data("width");
      var thisHeight = $this.data("height");

      popup_width = thisWidth === "full" ? window.innerWidth : Number(thisWidth);
      popup_hieght = thisHeight === "full" ? window.innerHeight : Number(thisHeight);
      
      popup_x = (window.innerWidth / 2) - (popup_width / 2);
      popup_y = thisHeight === "full" ? "20" : (window.innerHeight / 2) - (popup_hieght / 2);
      
      popup_options = "toolbar=yes,scrollbars=yes,resizable=yes,left=" + popup_x + ", top=" + popup_y;
      popup_options += ", width=" + popup_width;
      popup_options += ", height=" + popup_hieght;
      
      window.open(thisUrl, "_blank", popup_options);
      
    });
  },
  window_popup_close_evt_listener: function ($closebtn) {
    $closebtn.on("click", function () {
      window.close();
    });
  },
  accordion_common : function($accordion){
    $accordion.on("click", function () {
      var $this = $(this);
      var $thisInnerList = $this.parents(".innerList");
      if ($this.hasClass("open")) {
        $this.removeClass("open");
      } else {
        $this.addClass("open");
      }
      $thisInnerList.find(".aco-content").slideToggle();
    });
  },
  text_count_evt_listener : function(){ // 팝업 일정등록 내용입력 글자제한수 체크이벤트
    $(".textTyping").keyup(function () {
      var contentValue = $(this).val();
      if (contentValue.length > 100) return;
      $(".textCount").html(contentValue.length);
    });
  },
  text_paste_evt_listener: function ($contentTextarea) { // Content Editable에 텍스트 붙여넣기 이벤트
    function insertTextAtCursor(text) {
      var sel;
      var range;
      var html;
      if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
          range = sel.getRangeAt(0);
          range.deleteContents();
          range.insertNode(document.createTextNode(text));
        }
      } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().text = text;
      }
    }
    Array.prototype.forEach.call($contentTextarea, function (el) {
      el.addEventListener("paste", function (e) {
        e.preventDefault();
        
        if (e.clipboardData && e.clipboardData.getData) { //Chrome
          var text = e.clipboardData.getData("text/plain");

          document.execCommand("insertHTML", false, text);
        } else if (window.clipboardData && window.clipboardData.getData) { //IE
          var text = window.clipboardData.getData("Text");
          insertTextAtCursor(text);
        }
      });
    });
  },
  browser_check: function () { // Content Editable에 텍스트 붙여넣기 이벤트
    var browserCheck = function(){
    // 브라우저 및 버전을 구하기 위한 변수들.
    'use strict';
    var agent = navigator.userAgent.toLowerCase(),
      name = navigator.appName,
      browser = {
        "name": "",
        "version": "",
        "fullName": "",
      };

    // MS 계열 브라우저를 구분하기 위함.
    if (name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
      browser.name = 'ie';
      if (name === 'Microsoft Internet Explorer') { // IE old version (IE 10 or Lower)
        agent = /msie ([0-9]{1,}[\.0-9]{0,})/.exec(agent);
        browser.version = parseInt(agent[1]);
      } else { // IE 11+
        if (agent.indexOf('trident') > -1) { // IE 11
          browser.version = 11;
        } else if (agent.indexOf('edge/') > -1) { // Edge
          browser.fullName = 'edge';
        }
      }
      browser.fullName = browser.name + "_" + browser.version;

    } else if (agent.indexOf('safari') > -1) { // Chrome or Safari
      if (agent.indexOf('opr') > -1) { // Opera
        browser.fullName = 'opera';
      } else if (agent.indexOf('chrome') > -1 || agent.indexOf('crios') > -1) { // Chrome
        browser.fullName = 'chrome';
      } else { // Safari
        browser.fullName = 'safari';
      }

    } else if (agent.indexOf('firefox') > -1) { // Firefox
      browser.fullName = 'firefox';
    }
    return browser;
      };
      var browser = browserCheck();
      var browserName = browser.fullName;
      browser.fullName && document.getElementsByTagName('html')[0].classList.add(browserName);
  },
  check_table_evt_listener : function(){
    //테이블 내 checkbox 선택시 input 안에 선택갯수 체크
    $(".checkbox.table-inside-checkbox [name=tablecheck]").change(function () {
      var tableChecked = $("input[name=tablecheck]:checked").length;
      $(".table-inside-input").attr('value', tableChecked);
    });
  },
  table_wheel_evt_listener : function(){
    // 테이블 내 마우스 휠 스크롤 lib 호출
    $(".tbl-main").mCustomScrollbar({
      theme: "dark",
      axis: "x",
      mouseWheel: { enable: true },
    });
  }
};
