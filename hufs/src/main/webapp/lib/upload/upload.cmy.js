var filelist = [];
var uploadOpt = {
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: '/cmm/fms/upload.do',
		formData: {},
		//acceptFileTypes: /(\.|\/)(mpg|mpeg|mpeg-1|mpeg-2|mp4|avi|mov|flv|qt|m4v|wmv|asf)$/i,
		autoUpload: true,
		maxNumberOfFiles: 1,
		sequentialUploads: true,
		maxChunkSize: 10000000,
		dataType: 'json',
		dropZone: $('.dropZone'),
		add: function (e, data) {
			console.log('add', data);
    		
    		var opt  = $(this).fileupload('option', 'formData');    		
    		var extCheck = true;
    		if(opt.acceptFileTypes == 'Image') {
    			extCheck = isImage(data.files[0].name);
    		} else if(opt.acceptFileTypes == 'Video') {
    			extCheck = isVideo(data.files[0].name);
    		}
    		
    		if(!extCheck) {
    			alert('파일형식이 잘못되었습니다.');
    			return;
    		}
    		
    		
    		//용량 체크
    		
    		/*var tot = parseFloat($('#fileCurrSize_' + opt.editorId).val()) + data.files[0].size;
    		
    		if(opt.maxMegaFileSize * 1024* 1024 < tot) {
    			alert('첨부가능 용량이 초과되었습니다.');
    			return;
    		}*/
    		
    		var fu = $(this).data('blueimp-fileupload') || $(this).data('fileupload');
    		opt.maxChunkSize = fu.options.maxChunkSize;
    		
    		data.submit();
    		
		},
	    done: function (e, data) {console.log('done', data);
	    	if(data.result.success) {
	    		var data = data.result;
	    		if(data.files.length > 0) {
	    			var opt  = $(this).fileupload('option', 'formData');
	    			opt.appendFileListArea.append($(tmpl(opt.templateJsId, data)));
	    			//$('#fileGroupId').val(data.files[0].atchFileId);
	    			if(opt.appendFileListArea.children().length > 0) {
		    			opt.appendFileListArea.parent().parent().find('.toggleArea').eq(0).show();
		    			opt.appendFileListArea.parent().parent().find('.toggleArea').eq(1).hide();
	    			}
	    		}
	    	}
	    },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
        
        	var opt  = $(this).fileupload('option', 'formData');
        	opt.progressArea.css(
                'width',
                progress + '%'
            ).text(progress + '%');
        },
        submit: function (e, data) {
    		console.log('submit', data);
		},
		started: function (e) {
			console.log('started');
		},
		stopped: function (e) {
			console.log('stopped');
		},		
		added: function (e, data) {
			console.log('added', data);
		},
		completed: function (e, data) {
			console.log('completed', data);	
		},
		success: function (e, data) {
			console.log('success', data);	
		},		
		drop: function (e, data) {
			console.log('drop', e, data);
		},
		finished: function (e, data) {
			console.log('finished', data);	
		},
		fileuploadcompleted: function (e, data) {
			console.log('fileuploadcompleted', data)
		}
};


$(document).bind('dragover', function (e) {
    var dropZones = $('.dropZone'),
        timeout = window.dropZoneTimeout;
    if (timeout) {
        clearTimeout(timeout);
    } else {
        dropZones.addClass('in');
    }
    var hoveredDropZone = $(e.target).closest(dropZones);
    dropZones.not(hoveredDropZone).removeClass('hover');
    hoveredDropZone.addClass('hover');
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZones.removeClass('in hover');
    }, 100);
});

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});

function formatFileSize (bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}

function isImage(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "JPEG" || ext == "JPG" || ext == "PNG" || ext == "GIF") {
		return true;
	}
	return false;
}

function isZip(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "ZIP") {
		return true;
	}
	return false;
}

function isVideo(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	//var extensions = new Array("MPG", "MPE", "MPEG", " MPEG-1", " MPEG-2","MP4", "AVI", "OGV", "FLV", "MOV", "M4V", "QT", "WMV", "ASF", "3GP");
	var extensions = new Array("MP4");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}

function isAudio(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	var extensions = new Array("MP3","OGG","M4A","WMA","AIFF","WAV","AIF","AAC","AC3");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}
function isPDF(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PDF") {
		return true;
	}
	return false;
}

function isPPT(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PPTX" || ext == "PPT" || ext == "ODP") {
		return true;
	}
	return false;
}

function getAtchFileId() {
	var atchFileId = null;
	$.ajax({
		type : "POST",
		url : '/cmm/fms/selectFileIdByAjax.do',
		data : {},
		async: false,
		dataType : "json",
		success : function(result) {
			atchFileId = result.atchFileId;
		}, error: function(xhr, textStatus, errorThrown) {
            alert('An error occurred! ' + ( errorThrown ? errorThrown :xhr.status ));
		}
	});
	
	return atchFileId;
}


function uploadInit(uploader, option) {
	$.cleanData(uploader);
	
	if(option.atchFileId == '') {
		option.atchFileId = getAtchFileId();
		$('#fileGroupId').val(option.atchFileId);
	}
	uploadOpt.formData = option;
	uploadOpt.dropZone = option.dropZone;
	uploader.fileupload(uploadOpt).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	
};