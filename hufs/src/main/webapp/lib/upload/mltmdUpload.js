var filelist = [];

$(function () {
	'use strict';
	
	// Initialize the jQuery File Upload widget:
	$('#mltmdMvpInfoVO').fileupload({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: '/mltmdFileUpload.do?mltmdClCode=' + mltmdClCode + '&fileStreCours=' + fileStreCours,
		//acceptFileTypes: /(\.|\/)(mpg|mpeg|mpeg-1|mpeg-2|mp4|avi|mov|flv|qt|m4v|wmv|asf)$/i,
		autoUpload: true,
		//maxNumberOfFiles: 1,
		sequentialUploads: true,
		dataType: 'json',
		dropZone: $('.dropZone'),
		add: function (e, data) {
    		console.log('add', data);
    		
    		if(mltmdClCode != 'PHOTO') {
				if(filelist.length > 0) {
					alert('1개의 파일만 올리실 수 있습니다. 기존 파일을 삭제하신 후에 시도해주세요');
					return false;
				}
			}
			
    		var existsFile = getExistsFilelist(data.files[0]);
    		if(existsFile) {
    			alert('이미 첨부된 파일 입니다.');
    			return false;
    		}
    		
    		var fileTypeCheckFlag = false;
    		if(mltmdClCode == 'VOD') {
    			fileTypeCheckFlag = isVideo(data.files[0].name);
    		} else if(mltmdClCode == 'PHOTO') {
    			fileTypeCheckFlag = isImage(data.files[0].name);
    		} else if(mltmdClCode == 'VR') {
    			fileTypeCheckFlag = isZip(data.files[0].name);
    		} else if(mltmdClCode == 'AR') {
    			fileTypeCheckFlag = isZip(data.files[0].name);
    		}
    		
    		if(!fileTypeCheckFlag) {
    			alert('파일형식이 잘못되었습니다.');
    		} else {
    			data.submit();
    		}
		},
	    done: function (e, data) {console.log('done', data);
	    	if(data.result.success) {
	    		$('.fileList').append(tmpl("template-upload", data.result));
			    $.each(data.result.data, function (index, file) {
			    	filelist.push(file);
			    	
			    	//$('.fileList').append('<li>' + formatFileDisplay(file) + '</li>');
			    	
			    	//파일변경되었음표시
			    	$('#mvpChangeAt').val('Y');
				});
	    	}
	    	$('#mvpJsonData').val(JSON.stringify(filelist));
	    },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        submit: function (e, data) {
    		console.log('submit', data);
		},
		started: function (e) {
			console.log('started');
		},
		stopped: function (e) {
			console.log('stopped');
		},		
		added: function (e, data) {
			console.log('added', data);
		},
		completed: function (e, data) {
			console.log('completed', data);	
		},
		success: function (e, data) {
			console.log('success', data);	
		},		
		drop: function (e, data) {
			console.log('drop', e, data);	
			if(mltmdClCode != 'PHOTO') {
				if(data.files.length > 1) {
					alert('1개의 파일만 올리실 수 있습니다.');
					return false;
				}
				if(filelist.length > 0) {
					alert('1개의 파일만 올리실 수 있습니다. 기존 파일을 삭제하신 후에 시도해주세요');
					return false;
				}
			}
		},
		finished: function (e, data) {
			console.log('finished', data);	
		},
		fileuploadcompleted: function (e, data) {
			console.log('fileuploadcompleted', data)
		}
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

	$(document).on('click','.fileRemove',function() {	
		var file = {};
		file.name = $(this).data('name');
		file.size = $(this).data('size');
		setDeleteFilelist(file);
		
		$(this).parent().remove();	
		
		
	});
	
	/* 
	// Enable iframe cross-domain access via redirect option:
	$('#fileupload').fileupload(
		'option',
		'redirect',
		window.location.href.replace(
			/\/[^\/]*$/,
			'/cors/result.html?%s'
		)
	);

	$('#fileupload').fileupload('option', {
		dropZone: $('.dropZone'),
		disableImageResize: true,
		previewMaxWidth: 60,
		pasteZone: null,
		disableAudioPreview: true,
		disableVideoPreview: true,
		maxFileSize: null,
		acceptFileTypes: /(\.|\/)(pdf|png|jpg|jpeg|pjpeg|gif|bmp|mpg|mpeg|mpeg-1|mpeg-2|mp4|avi|mov|flv|qt|m4v|wmv|3gp|asf|ogv|3gpp|mp3|ogg|m4a|wav|wma|aiff|aif|aac|ac3|ppt|pptx|odp|pptm|zip)$/i,
		submit: function (e, data) {
		
		},
		stopped: function (e) {
			
		},
		sequentialUploads: true,
		added: function (e, data) {
						
		},
		autoUpload: true,
		completed: function (e, data) {
		
		},
		success: function (e, data) {
		
		},
		maxNumberOfFiles: 999,
		drop: function (e, data) {
		
		},
		done: function (e, data) {
           
        },
		progress: function (e, data) {
        
        },
	});


	$('#fileupload').bind('fileuploadsubmit', function (e, data) {
	    data.formData = $('#fileupload').serializeArray();
	}); */

});

$(document).bind('dragover', function (e) {
    var dropZones = $('.dropZone'),
        timeout = window.dropZoneTimeout;
    if (timeout) {
        clearTimeout(timeout);
    } else {
        dropZones.addClass('in');
    }
    var hoveredDropZone = $(e.target).closest(dropZones);
    dropZones.not(hoveredDropZone).removeClass('hover');
    hoveredDropZone.addClass('hover');
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZones.removeClass('in hover');
    }, 100);
});

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});

function formatFileDisplay(file) {
	var size = '<span style="">'+(file.fileMg/1000).toFixed(2)+'K</span>';
	return file.lgclflNm + ' ('+ size +') <img data-name="' + file.lgclflNm + '" data-size="' + file.fileMg + '" src="/template/manage/images/sms/sms_del.gif" class="fileRemove" style="cursor:pointer;margin-top: 4px;">';
}

function formatFileSize (bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}

function getFilelist() {
	var files = filelist;
	var filenames = '';
	for (var i=0; i<files.length; i<i++) {
		var suffix = (i==files.length-1) ? '' : ',';
		filenames += files[i].name + suffix;
	}
	return filenames;
}

function getExistsFilelist(checkFile) {
	var files = filelist;
	var flag = false;
	for (var i=0; i<files.length; i<i++) {
		if(files[i].lgclflNm == checkFile.name && files[i].fileMg == checkFile.size) {
			flag = true;
			break;
		}
	}
	return flag;
}

function setDeleteFilelist(checkFile) {
	var files = filelist;
	for (var i=0; i<files.length; i<i++) {
		if(files[i].lgclflNm == checkFile.name && files[i].fileMg == checkFile.size) {
			files.splice(i, 1);
			$('#mvpJsonData').val(JSON.stringify(files));
			$('#mvpChangeAt').val('Y');
			break;
		}
	}
	
}

function isImage(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "JPEG" || ext == "JPG" || ext == "PNG") {
		return true;
	}
	return false;
}

function isZip(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "ZIP") {
		return true;
	}
	return false;
}

function isVideo(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	var extensions = new Array("MPG", "MPE", "MPEG", " MPEG-1", " MPEG-2","MP4", "AVI", "OGV", "FLV", "MOV", "M4V", "QT", "WMV", "ASF", "3GP");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}

function isAudio(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	var extensions = new Array("MP3","OGG","M4A","WMA","AIFF","WAV","AIF","AAC","AC3");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}
function isPDF(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PDF") {
		return true;
	}
	return false;
}

function isPPT(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PPTX" || ext == "PPT" || ext == "ODP") {
		return true;
	}
	return false;
}