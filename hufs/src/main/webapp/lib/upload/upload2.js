var filelist = [];
var formData = {UploadType:'Image'};
var uploadModal;
$(function () {
	'use strict';
	
	uploadModal = new jBox('Modal', {
		  width: 450,
		  height: 350,
		  closeButton: 'title',
		  animation: false,
		  title: '<i class="fas fa-sign-in-alt"></i> UPLOAD',
		  content: $('#uploadTemplate'),
		  onOpen: function() {
			  
		  }
		});
	
	// Initialize the jQuery File Upload widget:
	
	
	/*
	$('#fileupload')
	.bind('fileuploaddestroy', function (e, data) {console.log('fileuploaddestroy')})
	 .bind('fileuploadcompleted', function (e, data) {console.log('fileuploadcompleted')})
	 .bind('fileuploadfinished', function (e, data) {console.log('fileuploadfinished')});
*/
	$(document).on('click','.fileRemove',function() {	
		
		var file = {};
		file.name = $(this).data('name');
		file.size = $(this).data('size');
		file.atchFileId = $(this).data('atch_fileId');
		file.fileSn = $(this).data('file_sn');
		setDeleteFilelist(file);
		
		$(this).parent().remove();	
		
		console.log('fileRemove', filelist);
		
		
		tinymce.activeEditor.dom.remove(tinymce.activeEditor.dom.select('[class=' + file.atchFileId + '_' + file.fileSn + ']'));
		//tinymce.activeEditor.dom.remove(tinymce.activeEditor.dom.select('.' + file.atchFileId + '_' + file.fileSn));
	});
	
	/* 
	// Enable iframe cross-domain access via redirect option:
	$('#fileupload').fileupload(
		'option',
		'redirect',
		window.location.href.replace(
			/\/[^\/]*$/,
			'/cors/result.html?%s'
		)
	);

	$('#fileupload').fileupload('option', {
		dropZone: $('.dropZone'),
		disableImageResize: true,
		previewMaxWidth: 60,
		pasteZone: null,
		disableAudioPreview: true,
		disableVideoPreview: true,
		maxFileSize: null,
		acceptFileTypes: /(\.|\/)(pdf|png|jpg|jpeg|pjpeg|gif|bmp|mpg|mpeg|mpeg-1|mpeg-2|mp4|avi|mov|flv|qt|m4v|wmv|3gp|asf|ogv|3gpp|mp3|ogg|m4a|wav|wma|aiff|aif|aac|ac3|ppt|pptx|odp|pptm|zip)$/i,
		submit: function (e, data) {
		
		},
		stopped: function (e) {
			
		},
		sequentialUploads: true,
		added: function (e, data) {
						
		},
		autoUpload: true,
		completed: function (e, data) {
		
		},
		success: function (e, data) {
		
		},
		maxNumberOfFiles: 999,
		drop: function (e, data) {
		
		},
		done: function (e, data) {
           
        },
		progress: function (e, data) {
        
        },
	});


	$('#fileupload').bind('fileuploadsubmit', function (e, data) {
	    data.formData = $('#fileupload').serializeArray();
	}); */

});

function uploadInit(acceptFileTypes) {
	
	$.cleanData( $('#fileupload') );
	
	var opt = {
		url: '/fileUpload.do',
		sequentialUploads: true,
		maxFileSize:100*1024*1024,
		previewMaxWidth: 120,
        previewMaxHeight: 120,
		previewCrop: true,
		dataType: 'json',
		dropZone: $('.dropZone')
		,submit: function (e, data) {
    		console.log('submit', data);
		},
		started: function (e) {
			console.log('started');
		},
		stopped: function (e, data) {
			console.log('stopped');
			//$(this).fileupload('destroy');
			//data.files.length = 0;
			
			
		},		
		added: function (e, data) {
			console.log('added', data);
			
			if(data.files[0].preview.videoWidth) {
				//data.files[0].preview.videoWidth = 120;
				//data.files[0].preview.videoHeight = 120;
				
				$(data.files[0].preview).css('width', '120px');
				
			}
			/*$(this).fileupload(
			        'option',
			        {
			            previewMaxWidth: 120,
			            previewMaxHeight: 120
			        }
			    );*/
		},
		completed: function (e, data) {
			console.log('completed', data);	
		},
		success: function (e, data) {
			console.log('success', data);	
		},		
		drop: function (e, data) {
			console.log('drop', e, data);
		},
		finished: function (e, data) {
			console.log('finished', data);	
		},
		fileuploadcompleted: function (e, data) {
			console.log('fileuploadcompleted', data)
		}
    };
	
	if(acceptFileTypes == 'image') {
		//opt.acceptFileTypes = imageAcceptType();
	} else if(acceptFileTypes == 'video') {
		//opt.acceptFileTypes = videoAcceptType();
	}
	
	$('#fileupload').fileupload(opt)
	.on('fileuploaddone', function (e, data) {console.log('don', data);
    	$.each(data.result.files, function (index, file) {
	    	filelist.push(file);
		});
    	$('.fileList').append(tmpl("template-download", data.result));
    	if(tinymce) {
    		tinymce.activeEditor.insertContent(tmpl("template-editor", data.result));
    	}
    })
    .on('fileuploadprogressall', function (e, data) {console.log('fileuploadprogressall', data);
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if(progress == 100) {
        	
        	uploadModal.close();
        	$('.files').empty();
        	
        }
    })
    .on('fileuploadcompleted', function (e, data) {console.log('fileuploadcompleted', data);
       
    })
    .on('fileuploaddestroy', function (e, data) {console.log('fileuploaddestroy', data);
       
    })
    ;
	
	
}
$(document).bind('dragover', function (e) {
    var dropZones = $('.dropZone'),
        timeout = window.dropZoneTimeout;
    if (timeout) {
        clearTimeout(timeout);
    } else {
        dropZones.addClass('in');
    }
    var hoveredDropZone = $(e.target).closest(dropZones);
    dropZones.not(hoveredDropZone).removeClass('hover');
    hoveredDropZone.addClass('hover');
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZones.removeClass('in hover');
    }, 100);
});

$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});

function formatFileDisplay(file) {
	var size = '<span style="">'+(file.fileMg/1000).toFixed(2)+'K</span>';
	return file.lgclflNm + ' ('+ size +') <img data-name="' + file.lgclflNm + '" data-size="' + file.fileMg + '" src="/template/manage/images/sms/sms_del.gif" class="fileRemove" style="cursor:pointer;margin-top: 4px;">';
}

function formatFileSize (bytes) {
    if (typeof bytes !== 'number') {
        return '';
    }
    if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
    }
    if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
    }
    return (bytes / 1000).toFixed(2) + ' KB';
}

function getFilelist() {
	var files = filelist;
	var filenames = '';
	for (var i=0; i<files.length; i<i++) {
		var suffix = (i==files.length-1) ? '' : ',';
		filenames += files[i].name + suffix;
	}
	return filenames;
}

function getExistsFilelist(checkFile) {
	var files = filelist;
	var flag = false;
	for (var i=0; i<files.length; i<i++) {
		if(files[i].lgclflNm == checkFile.name && files[i].fileMg == checkFile.size) {
			flag = true;
			break;
		}
	}
	return flag;
}

function setDeleteFilelist(checkFile) {
	var files = filelist;
	for (var i=0; i<files.length; i<i++) {
		if(files[i].orignlFileNm == checkFile.name && files[i].fileMg == checkFile.size) {
			files.splice(i, 1);
			$('#mvpJsonData').val(JSON.stringify(files));
			$('#mvpChangeAt').val('Y');
			break;
		}
	}
	
}

function isImage(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "JPEG" || ext == "JPG" || ext == "PNG") {
		return true;
	}
	return false;
}

function isZip(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "ZIP") {
		return true;
	}
	return false;
}

function isVideo(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	var extensions = new Array("MPG", "MPE", "MPEG", " MPEG-1", " MPEG-2","MP4", "AVI", "OGV", "FLV", "MOV", "M4V", "QT", "WMV", "ASF", "3GP");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}

function isAudio(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	var extensions = new Array("MP3","OGG","M4A","WMA","AIFF","WAV","AIF","AAC","AC3");
	if (extensions.indexOf(ext) != -1) {
		return true;
	}
	return false;
}
function isPDF(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PDF") {
		return true;
	}
	return false;
}

function isPPT(name){
	var nameArr = name.split('.');
	var ext = nameArr[nameArr.length - 1].toUpperCase();
	if (ext == "PPTX" || ext == "PPT" || ext == "ODP") {
		return true;
	}
	return false;
}

function imageAcceptType() {
	return /(\.|\/)(gif|jpg|png)$/i
}

function videoAcceptType() {
	return /(\.|\/)(mp4)$/i
}
