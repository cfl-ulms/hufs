<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>
<style>
	#selStChangeBtn, #selStDelBtn{float:right; display:inline-block; border: 1px solid #719fde; border-radius: 3px; margin-right:5px; margin-bottom: 5px; background: #666666; color: #fff; font-size: 13px; font-weight: bold; padding: 3px 10px; vertical-align: middle; right: 0}
	#addstuBtn{float:right; display:inline-block; border: 1px solid #719fde; border-radius: 3px; margin-right:5px; margin-bottom: 5px; background: #93baf7; color: #fff; font-size: 13px; font-weight: bold; padding: 3px 10px; vertical-align: middle; right: 0}
	.th_line{text-align: center !important; padding: 7px 17px !important;}
</style>
<script>
$(document).ready(function(){
	//승인 상태 변경 모달
	$(".btn-open-dialog").click(function () {
		var sttus = $(this).data("sttus");

		//버튼 클릭 체크
		if(sttus != "") {
			var sttusNm = "";

			if     (sttus == "1") sttusNm = "승인";
			else if(sttus == "2") sttusNm = "대기";
			else if(sttus == "3") sttusNm = "취소";
			else if(sttus == "4") sttusNm = "환불";
			
			$(".sts_code").text(sttusNm);
			$(".sttus_" + sttus).prop('checked', true);
			$("#userId").val($(this).data("userid"));
		}
		
		$("#dialog-background, .sttus-update-dialog").show();
	});

	//승인상태 변경
	$(".btn_change").click(function(){
		$("#sttusForm").submit();
	});
	
	//수강신청 조기 종료 모달
	$(document).on("click", "#updateConfirmBtn", function(){
		$("#dialog-background, .process-update-dialog").show();
	});
	
	//수강신청 조기 종료 업데이트
	$(".btn_update").click(function(){
		$("#acceptForm").submit();
	});
	
	//모달 종료
	$(document).on("click", "#dialog-background,.btn-close-dialog, .btn-no", function(){
		$("#dialog-background, .sttus-update-dialog, .process-update-dialog").hide();
	});
	
	//수강자 수동입력
	$("#addstuBtn").click(function(){
		window.open("/mng/usr/EgovStuMberManage.do?crclId=${curriculumVO.crclId}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
		return false;
	});
	
	//수강신청 인원 전체 클릭
	$("#stIdTotal").change(function(){
		
		var paramLen =0;
		paramLen = $("input[name=stMemberId]").size();
		
		if($("#stIdTotal").is(":checked")){
			for(var i = 0 ; i < paramLen ; i++){
				if($("input[name=stMemberId]").eq(i).is(":checked") == false){
					$("input[name=stMemberId]").eq(i).prop("checked",true);
				}
			}
		} else {
			for(var j = 0 ; j < paramLen ; j++){
				if($("input[name=stMemberId]").eq(j).is(":checked") == true){
					$("input[name=stMemberId]").eq(j).prop("checked",false);
				}
			}
		}
	});
	
});


// 선택된 학생 일괄 승인
function selStChange(){
	if(!confirm("선택한 학생을 승인변경 하시겠습니까?")){
		return false;
	}
	
	var crclId = '${curriculumVO.crclId}';
	
	var paramUserIdArr = [];
	var paramUserId = '';
	var paramLen =0;
	var checkedLen = 0;
	paramLen = $("input[name=stMemberId]").size();
	
	for(var i = 0 ; i < paramLen ; i++){
		if($("input[name=stMemberId]").eq(i).is(":checked")){
			paramUserIdArr.push($("input[name=stMemberId]").eq(i).val());	
			checkedLen = checkedLen+1;
		}
	}
	
	if(checkedLen == 0){
		alert("승인할 학생을 선택해 주세요.");
		return false;
	}
	
	paramUserId = paramUserIdArr.join(",");
	
	$.ajax({
		type : "POST",
		url : "/ajax/mng/lms/crm/updateSttusCurriculumMember.json",
		data : {"paramUserId" 	: paramUserId,
				"crclId"		: crclId
				},
		dataType : "json",
		success : function(data){
			if(data.result=='Y'){
				alert("변경 완료하였습니다.");
				location.reload();
			} else {
				alert("관리자에게 문의해주세요.");
			}
		}, error : function(){
			alert("error");
		}
	});
}

// 선택된 학생 삭제
function selStDel(){
	if(!confirm("선택한 학생을 정말 삭제하시겠습니까?")){
		return false;
	}
	
	var crclId = '${curriculumVO.crclId}';
	
	var paramUserIdArr = [];
	var paramUserId = '';
	var paramLen =0;
	var checkedLen = 0;
	paramLen = $("input[name=stMemberId]").size();
	
	for(var i = 0 ; i < paramLen ; i++){
		if($("input[name=stMemberId]").eq(i).is(":checked")){
			paramUserIdArr.push($("input[name=stMemberId]").eq(i).val());	
			checkedLen = checkedLen+1;
		}
	}
	
	if(checkedLen == 0){
		alert("삭제할 학생을 선택해 주세요.");
		return false;
	}
	
	paramUserId = paramUserIdArr.join(",");
	
	$.ajax({
		type : "POST",
		url : "/ajax/mng/lms/crm/delCurriculumMember.json",
		data : {"paramUserId" 	: paramUserId,
				"crclId"		: crclId
				},
		dataType : "json",
		success : function(data){
			if(data.result=='Y'){
				alert("삭제 완료하였습니다.");
				location.reload();
			} else {
				alert("관리자에게 문의해주세요.");
			}
		}, error : function(){
			alert("error");
		}
	});
}

</script>
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>

	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="3"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
	</c:import>
	
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crm/curriculumAccept.do"/>">
	  		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>

			<input type="text" name="searchHostCode" value="${searchVO.searchHostCode }" class="inp_s" id="inp_text" placeholder="소속">
			
			<select id="searchSttus" name="searchSttus">
	  			<option value="">승인 상태 전체</option>
	  			<option value="1" <c:if test="${'1' eq searchVO.searchSttus}">selected="selected"</c:if>>승인</option>
	  			<option value="2" <c:if test="${'2' eq searchVO.searchSttus}">selected="selected"</c:if>>대기</option>
	  			<option value="3" <c:if test="${'3' eq searchVO.searchSttus}">selected="selected"</c:if>>취소</option>
	  			<option value="4" <c:if test="${'4' eq searchVO.searchSttus}">selected="selected"</c:if>>환불</option> 
			</select>

			<input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy }" class="inp_s onlyNum" id="inp_text" placeholder="생년월일 8자리 숫자만입력">
			
			<input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" class="inp_s" id="inp_text" placeholder="학생 이름">
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>

	<div>
		<span class="total">총 ${totCnt }건</span>
		<%-- <p class="total">총 ${totCnt }건</p> --%>
		<c:if test="${curriculumVO.processSttusCodeDate > 5 or curriculumVO.processSttusCodeDate eq 4}">
			<!-- <div class="btn-box"> -->
				<!-- <a href="#" id="btn-addstu" class="btn_mngTxt">[관리자] 수강대상자 수동 등록</a> -->
				<a href="#" id="addstuBtn">[관리자] 수강대상자 수동 등록</a>
				<c:if test="${curriculumVO.processSttusCodeDate eq 4 }">
					<a href="#" id="selStChangeBtn" onclick="selStChange()">선택한 학생 승인 일괄변경</a>
					<a href="#" id="selStDelBtn" onclick="selStDel()">선택한 학생 삭제</a>
				</c:if>
			<!-- </div> -->
		</c:if>
	</div>	
	
	<table class="chart2 mb50">
		<tbody>
			<tr class="alC">
				<th class="th_line"><input type="checkbox" id="stIdTotal"></th>
				<th class="th_line">no</th>
				<th class="th_line">ID</th>
				<th class="th_line">소속</th>
				<th class="th_line">이름</th>
				<th class="th_line">생년월일</th>
				<th class="th_line">학번</th>
				<th class="th_line">학년</th>
				<th class="th_line">전화번호</th>
				<th class="th_line">신청서</th>
				<th class="th_line">계획서</th>
				<th class="th_line">기타파일</th>
				<th class="th_line">신청일</th>
				<th class="th_line">승인 상태</th>
				<th class="th_line">상태 변경</th>
			</tr>
			<c:forEach var="member" items="${curriculumMemberList}" varStatus="status">
				<tr class="alC">
					<td class="line alC"><input type="checkbox" name="stMemberId" value="${member.userId}"></td>
					<td class="line alC">${totCnt - status.index }</td>
					<td class="line alC">${member.userId }</td>
					<td class="line alC">${member.mngDeptNm }</td>
					<td class="line alC">${member.userNm }</td>
					<td class="line alC">${member.brthdy }</td>
					<td class="line alC">${member.stNumber }</td>
					<td class="line alC">${member.stGrade }</td>
					<td class="line alC">(+${member.geocode }) ${member.moblphonNo }</td>
					<c:url var="aplyFileUrl" value="/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${member.aplyFile }"/>
						<c:param name="oriFileNm" value="${member.aplyOriFile }"/>
	 			    </c:url>
	 			    <td class="line alC">
	 			    	<c:if test="${not empty member.aplyOriFile}">
	 			    		<a href="${aplyFileUrl }" title="${member.aplyOriFile }"><img src="/template/manage/images/ico_file.gif" alt=""/></a>
	 			    	</c:if>
	 			    </td>
					<c:url var="planFileUrl" value="/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${member.planFile }"/>
						<c:param name="oriFileNm" value="${member.planOriFile }"/>
	 			    </c:url>
					<td class="line alC">
						<c:if test="${not empty member.planOriFile}">
							<a href="${planFileUrl }" title="${member.planOriFile }"><img src="/template/manage/images/ico_file.gif" alt=""/></a>
						</c:if>
					</td>
					<c:url var="etcFileUrl" value="/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${member.etcFile }"/>
						<c:param name="oriFileNm" value="${member.etcOriFile }"/>
	 			    </c:url>
					<td class="line alC">
						<c:if test="${not empty member.etcOriFile}">
							<a href="${etcFileUrl }" title="${member.etcOriFile }"><img src="/template/manage/images/ico_file.gif" alt=""/></a>
						</c:if>
					</td>
					<td class="line alC">${member.frstRegisterPnttm }</td>
					<td class="line alC">
						<c:choose>
							<c:when test="${member.sttus eq 1}">승인</c:when>
							<c:when test="${member.sttus eq 2}">대기</c:when>
							<c:when test="${member.sttus eq 3}">취소</c:when>
							<c:when test="${member.sttus eq 4}">환불</c:when>
						</c:choose>
					</td>
					<td class="line alC">
						<c:if test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4}">
							<a href="#none" data-sttus="${member.sttus }" data-userid="${member.userId }" class="btn_mngTxt btn-open-dialog">변경</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(curriculumMemberList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="14"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>
	</table>

	<div class="btn_c">
		<c:if test="${curriculumVO.processSttusCodeDate eq '3'}"><a href="#none" class="btn_mngTxt" id="updateConfirmBtn">수강신청 조기 마감</a></c:if>
	    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
	    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
	</div>

<div class="my-dialog sttus-update-dialog" style="height:210px;">
	<div class="modal_header">승인 상태 변경</div>
	<div class="modal_body">
		<h3 class="status_cur">현재 <span class="sts_code"></span> 상태입니다.</h3>
		<form id="sttusForm" action="/mng/lms/crm/updateCurriculumAccept.do" method="post">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
			<input type="hidden" name="userId" id="userId" value=""/>

			<ul class="box_process">
				<li><input type="radio" class="sttus_1" name="sttus" value="1" /> 승인</li>
				<li><input type="radio" class="sttus_2" name="sttus" value="2" /> 대기</li>
				<li><input type="radio" class="sttus_3" name="sttus" value="3" /> 취소</li>
				<li><input type="radio" class="sttus_4" name="sttus" value="4" /> 환불</li>
			</ul>
		</form>
		<div class="box_btn">
			<a href="#" class="btn_change btn_mngTxt">변경</a>
			<button class="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
		</div>
	</div>
</div>

<div class="my-dialog process-update-dialog" style="width:700px;height:250px;">
	<div class="modal_header">알림</div>
	<div class="modal_body" style="padding:10px;">
	    <jsp:useBean id="now" class="java.util.Date" />
        <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />

		<form id="acceptForm" action="/mng/lms/crm/updateProcessSttusCode.do" method="post">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="processSttusCode" value="4"/>
			<input type="hidden" name="applyEndDate" value="${today }"/>
			<input type="hidden" name="forwardUrl" value="/mng/lms/crm/curriculumAccept.do?crclId=${curriculumVO.crclId}"/>

			<div style="padding-bottom:30px;">
				<c:out value="${curriculumVO.crclNm }"/>의 수강신청을 조기 마감 하시겠습니까?<br /><br />
				
				<span style="color:#62A4E0;">
				수강신청을 조기 마감하시려면<br />
				해당 과정의 상태를 <strong>[수강신청 종료]</strong> 상태로 변경해야 합니다.<br /><br />
				</span>
				
				과정의 상태를 [수강신청 종료] 상태로 변경하시겠습니까?<br />
			</div>
			<br />
		</form>
		<div class="box_btn">
			<a href="#none" class="btn_mngTxt btn-no">아니오</a>
			<a href="#none" class="btn_update btn_mngTxt">네, 해당 과정의 상태를 [수강신청 종료] 상태로 변경합니다.</a>
			<button class="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
		</div>
	</div>
</div>
<div id="dialog-background"></div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>