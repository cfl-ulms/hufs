<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
    <c:when test="${searchVO.mngAt eq 'Y'}">
        <c:import url="/mng/template/top.do" charEncoding="utf-8">
            <c:param name="menu" value="CURRICULUM_MANAGE"/>
            <c:param name="depth1" value="BASE_CRCL_MNG"/>
            <c:param name="depth2" value=""/>
            <c:param name="title" value="교육과정개설신청관리"/>
        </c:import>
    </c:when>
    <c:otherwise>
        <c:import url="/mng/template/top.do" charEncoding="utf-8">
            <c:param name="menu" value="CURRICULUM_MANAGE"/>
            <c:param name="depth1" value="BASE_CRCL"/>
            <c:param name="depth2" value=""/>
            <c:param name="title" value="과정등록관리"/>
        </c:import>
    </c:otherwise>
</c:choose>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
    <c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
        <c:set var="processSttusCode" value="2"/>
    </c:when>
    <c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
        <c:set var="processSttusCode" value="3"/>
    </c:when>
    <c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
        <c:set var="processSttusCode" value="4"/>
    </c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
    <c:if test="${result.code eq processSttusCode}">
        <c:set var="codeNm" value="${result.codeNm}"/>
        <c:set var="codeDc" value="${result.codeDc}"/>
    </c:if>
</c:forEach>

<script>
$(document).ready(function(){
    //과정확정
    $(".btn_confirm").click(function(){
        var href = $(this).attr("href");
        
        if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정 하시겠습니까?")){
            return;
        }else{
            return false;
        }
        
        return false;
    });
    
    //과정승인
    $(".btn_apr").click(function(){
        var href = $(this).attr("href");
        
        if(!$("input[name=crclOutcome]:checked").val()){
            alert("과정성과를 선택해 주세요.");
        }else{
            if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 승인 하시겠습니까?")){
                $("#curriculumVO").attr("action", href);
                $("input[name=aprvalAt]").val("Y");
                $("#curriculumVO").submit();
            }
        }
        
        return false;
    });
    
    //과정반려
    $(".btn_rjt").click(function(){
        var href = $(this).attr("href"),
            txt = "교수님, 먼저 교육과정개설 신청을 진행해 주셔서 감사드립니다.\n신청해 주신 교육과정은 개설되지 못했습니다. 이점 양해 부탁드립니다.\n문의사항은 운영팀으로 연락 주시면 안내드리도록 하겠습니다.";
        
        if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 반려 하시겠습니까?")){
            $("#curriculumVO").attr("action", href);
            $("input[name=aprvalAt]").val("N");
            if(!$("#comnt").val()){
                $("#comnt").val(txt);
            }
            $("#curriculumVO").submit();
        }
        
        return false;
    });
    
    //승인취소
    $(".btn_aprCcl").click(function(){
        var href = $(this).attr("href");
        
        if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 승인 취소하시겠습니까?")){
            if(!$("#aprvalDn").val()){
                alert("승인 취소사유를 입력 후 승인취소 버튼을 눌러주세요.");
                $(".box_aprDn").show();
                $("#aprvalDn").focus(); 
            }else{
                $("#curriculumVO").attr("action", href);
                $("input[name=aprvalAt]").val("D");
                $("#curriculumVO").submit();
            }
        }
        
        return false;
    });
    
    //코멘트등록
    $(".btn_cmt").click(function(){
        var href = $(this).attr("href");
        
        $("#curriculumVO").attr("action", href);
        $("#curriculumVO").submit();
        
        return false;
    });
    
    //교원수 - 초기화
    facTotCnt();
});

//교원 수
function facTotCnt(){
    var dupCnt = 0,
        ids = [],
        idList = [];
    
    $(".facIdList").each(function(){
        var id = $(this).val(),
            dupAt = false;
        $.each(idList, function(i){
            if(idList[i] == id){
                dupAt = true;
            }
        });
        
        if(!dupAt){
            idList.push(id);
        }
    });
    
    $(".sumUser").text(idList.length);
}
</script>
    <table class="chart2 mb50">
        <tr>
            <td class="alC">
            	<c:out value="${curriculumVO.crclNm }"/>
            	<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
            </td>
        </tr>
    </table>
    <c:if test="${curriculumVO.processSttusCode > 0}">
        <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
            <c:param name="step" value="2"/>
            <c:param name="crclId" value="${curriculumVO.crclId}"/>
            <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
            <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        </c:import>
    </c:if>

    <c:choose>
        <c:when test="${param.importFlag eq 1 }">
            <c:import url="/mng/lms/crm/curriculumStudent.do" charEncoding="utf-8">
		        <c:param name="menuCode" value="student"/>
		    </c:import>
        </c:when>
        <c:when test="${param.importFlag eq 2 }">
            <c:import url="/mng/lms/crm/curriculumGroupView.do?classCnt=1" charEncoding="utf-8">
                <c:param name="menuCode" value="student"/>
            </c:import>
        </c:when>
        <c:when test="${param.importFlag eq 3 }">
            <c:import url="/mng/lms/crm/curriculumClassView.do" charEncoding="utf-8">
                <c:param name="menuCode" value="student"/>
            </c:import>
        </c:when>
    </c:choose>

    <div class="btn_c">        
        <c:url var="listUrl" value="/mng/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
        <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
    </div>
    
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>