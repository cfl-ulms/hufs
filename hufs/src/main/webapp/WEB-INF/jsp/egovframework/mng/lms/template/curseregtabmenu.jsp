<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${param.crclId}" />
	<c:param name="crclbId" value="${param.crclbId}" />
</c:url>
<% /*URL 정의*/ %>

<table class="tab_table" style="margin-bottom:30px;">
	<tbody>
		<tr>
			<th class="alC <c:if test="${param.step eq '1'}">on</c:if>"><a href="/mng/lms/crm/curriculumManage.do${_BASE_PARAM }">과정계획서</a></th>
			<th class="alC <c:if test="${param.step eq '2'}">on</c:if>"><a href="/mng/lms/crm/curriculumRegister.do${_BASE_PARAM }">수강신청 화면</a></th>
			<th class="alC <c:if test="${param.step eq '3'}">on</c:if>"><a href="/mng/lms/crm/curriculumAccept.do${_BASE_PARAM }">수강신청 승인</a></th>
			<th class="alC <c:if test="${param.step eq '4'}">on</c:if>"><a href="/mng/lms/crm/curriculumStudent.do${_BASE_PARAM }">수강대상자 확정</a></th>
		</tr>
	</tbody>
</table>