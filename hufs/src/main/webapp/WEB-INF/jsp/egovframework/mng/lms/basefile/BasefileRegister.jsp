<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/basefile"/>
<c:set var="_ACTION" value=""/>

<c:choose>
	<c:when test="${empty searchVO.atchFileId}">
		<c:set var="_MODE" value="REG"/>
		<c:set var="_ACTION" value="${_PREFIX}/addBasefile.do"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
		<c:set var="_ACTION" value="${_PREFIX}/updateAcademic.do"/>
	</c:otherwise>
</c:choose>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_FILE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="양식 관리"/>
</c:import>
<script>
$(document).ready(function(){
	var depth2 = [];
	<c:forEach var="result" items="${menuList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			depth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>2Depth 선택</option>";
			
		$("#ctgryId2").html(option);
		for(i = 0; i < depth2.length; i++){
			console.log(depth2[i].upperCtgryId +" : "+ ctgryId1);
			if(depth2[i].upperCtgryId == ctgryId1){
				$("#ctgryId2").append("<option value='"+depth2[i].ctgryId+"'>"+depth2[i].ctgryNm+"</option>");
			}
		}
	});
});
</script>

<div id="cntnts">
<form:form commandName="basefileVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data">
	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col class="10%"/>
			<col class="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><em>*</em> <label>서비스 메뉴 설정</label></th>
				<td>
					<select id="ctgryId1" name="ctgryIdList">
						<c:forEach var="result" items="${menuList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}"><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					<select id="ctgryId2" name="ctgryIdList">
						<option value="">2Depth 선택</option>
					</select>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>파일</label></th>
				<td><input type="file" name="file" class="inp_long"/></td>
			</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }"/>
        <c:url var="listUrl" value="/mng/lms/basefile/BasefileList.do"/>
        <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>
</form:form>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>

