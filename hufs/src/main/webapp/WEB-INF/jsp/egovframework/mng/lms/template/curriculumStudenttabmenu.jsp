<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${param.crclId}" />
	<c:param name="crclbId" value="${param.crclbId}" />
	<c:param name="menuCode" value="${param.menuCode}" />
</c:url>
<% /*URL 정의*/ %>

<table class="tab_table" style="width:30%;">
	<colgroup>
		<col width="33%">
		<col width="33%">
		<col width="33%">
	</colgroup>
	<tbody>
		<tr>
		    <%-- 아래와 같이 url 분기 처리 이유는 "과정관리 > 학생"에서 import를 해오는 이슈로 인하여 처리함. --%>

		    <%-- 전체명단 url --%>
		    <c:choose>
		        <c:when test="${param.menuCode eq 'student' }"><c:set var="allUrl" value="/mng/lms/crcl/selectCurriculumStudent.do${_BASE_PARAM }&importFlag=1"/></c:when>
		        <c:otherwise><c:set var="allUrl" value="/mng/lms/crm/curriculumStudent.do${_BASE_PARAM }"/></c:otherwise>
		    </c:choose>
		    
		    <%-- 조배정 url --%>
            <c:choose>
                <c:when test="${param.menuCode eq 'student' }"><c:set var="groupUrl" value="/mng/lms/crcl/selectCurriculumStudent.do${_BASE_PARAM }&importFlag=2"/></c:when>
                <c:otherwise><c:set var="groupUrl" value="/mng/lms/crm/curriculumGroupView.do${_BASE_PARAM }&classCnt=1"/></c:otherwise>
            </c:choose>
            
            <%-- 분반 url --%>
            <c:choose>
                <c:when test="${param.menuCode eq 'student' }"><c:set var="classUrl" value="/mng/lms/crcl/selectCurriculumStudent.do${_BASE_PARAM }&importFlag=3"/></c:when>
                <c:otherwise><c:set var="classUrl" value="/mng/lms/crm/curriculumClassView.do${_BASE_PARAM }"/></c:otherwise>
            </c:choose>
		    
			<th class="alC <c:if test="${param.step eq '1'}">on</c:if>"><a href="${allUrl }">전체명단</a></th>
			<th class="alC <c:if test="${param.step eq '2'}">on</c:if>"><a href="${groupUrl }">조배정</a></th>
			<th class="alC <c:if test="${param.step eq '3'}">on</c:if>"><a href="${classUrl }">분반</a></th>
		</tr>
	</tbody>
</table>