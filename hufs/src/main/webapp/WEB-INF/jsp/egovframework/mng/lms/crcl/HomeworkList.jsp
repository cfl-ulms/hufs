<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" />

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CURRICULUM_MANAGE"/>
    <c:param name="depth1" value="BASE_CRCL"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과정등록관리"/>
</c:import>

<script>
$(document).ready(function(){
	$(document).on("click", ".submitWaitingList, .testWaitingList", function() {
        $(".modal_header").text("제출 대기 학생");
        $("#waitingMemberList").html("");

        $.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/crcl/homeworkWaitingMemberListAjax.do",
            data:{crclId:"${param.crclId}", hwId:$(this).data("hwid"), hwType:$(this).data("hwtype"), hwWaitingType:$(this).data("hwwaitingtype"), adminPageFlag:"Y"},
            success: function(data) {
                $("#waitingMemberList").html(data);
            }.bind(this),
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });
    });

	//평가대기 학생
    $(document).on("click", ".testWaitingList", function() {
        $(".modal_header").text("평가 대기 학생");
    });

	//모달 열기
    $(".btn-open-dialog").click(function () {        
        $("#dialog-background, .my-dialog").show();
    });
	
    //모달 종료
    $(document).on("click", "#dialog-background,.btn-close-dialog, .btn-no", function(){
        $("#dialog-background, .my-dialog").hide();
    });
});

//검색 초기화
function fnReset(){
    $("#bbs_search").find("input").not("#resetBtn").val("");
    $("#bbs_search select").find("option:first").attr('selected', 'selected');
}
</script>
<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm}"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
<c:if test="${curriculumVO.processSttusCode > 0}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        <c:param name="step" value="8"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
    <ul class="group_list">
        <li class="act"><a href="/mng/lms/crcl/homeworkList.do?crclId=${param.crclId }">과제</a></li>
        <li class="partition"> | </li>
        <li><a href="/mng/lms/crcl/homeworkTestList.do?crclId=${param.crclId }">과제평가</a></li>
    </ul>
    <table class="chart_board">
        <thead>
          <tr>
            <th>번호</th>
            <th colspan='2'>구분</th>
            <th>수업주제</th>
            <th>과제명</th>
            <th>교원명</th>
            <th>제출기간</th>
            <th>마감일</th>
            <th>과제상태</th>
            <th>학생공개</th>
            <th>제출현황</th>
            <th>평가현황</th>
          </tr> 
        </thead>
        <tbody>
            <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                <c:url var="viewUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
                    <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                    <c:param name="hwId" value="${result.hwId }" />
                    <c:param name="plId" value="${result.plId }" />
                    <c:param name="menu" value="CURRICULUM_MANAGE" />
                </c:url>
                <tr class="">
                  <td scope='row' onclick="location.href='${viewUrl}'"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
                  <td onclick="location.href='${viewUrl}'">${result.hwCodeNm}</td>
                  <td onclick="location.href='${viewUrl}'">${result.hwTypeNm}</td>
                  <td onclick="location.href='${viewUrl}'" class='left-align'>${result.studySubject}</td>
                  <td onclick="location.href='${viewUrl}'" class='left-align keep-all'>${result.nttSj}</td>
                  <td onclick="location.href='${viewUrl}'">${result.ntcrNm}</td>
                  <td onclick="location.href='${viewUrl}'">${result.openDate}<br>~<br>${result.closeDate}</td>
                  <td onclick="location.href='${viewUrl}'" class='keep-all'>${result.closeDate}</td>
                  
                  <%-- 날짜 비교를 위한 데이터 컨버팅 --%>
                  <c:set var="openDate" value="${fn:replace(result.openDate, '-', '')}${result.openTime }"/>
                  <fmt:parseDate value="${openDate}" var="openParseDate" pattern="yyyyMMddHHmm"/>
                  <fmt:formatDate value="${openParseDate}" pattern="yyyyMMddHHmm" var="openDateForm"/>
                  
                  <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
                  <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                  <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateForm"/>

                  <td onclick="location.href='${viewUrl}'">
                      <c:choose>
                          <c:when test="${openDateForm > nowDate }">등록대기</c:when>
                          <c:when test="${closeDateForm < nowDate or result.stuOpenAt eq 'Y'}">마감</c:when>
                          <c:otherwise>제출중</c:otherwise>
                      </c:choose>
                  </td>
                  <td onclick="location.href='${viewUrl}'">
                      <c:choose>
                          <c:when test="${result.stuOpenAt eq 'Y' }">공개</c:when>
                          <c:otherwise>대기</c:otherwise>
                      </c:choose>
                  </td>
                  <td>
                      <c:choose>
                          <c:when test="${result.homeworkSubmitCnt eq result.memberCnt }">전원제출</c:when>
                          <c:otherwise>
                              <a href="#" class="underline btn-open-dialog submitWaitingList"  data-modal-type="waiting_list" data-hwid="${result.hwId }" data-hwtype="${result.hwType }" data-hwwaitingtype="1">
                                  ${result.homeworkSubmitCnt}/${result.memberCnt }
                              </a>
                          </c:otherwise>
                      </c:choose>
                  </td>
                  <td>
                      <c:choose>
                          <c:when test="${result.homeworkScrCnt eq result.memberCnt }">평가완료</c:when>
                          <c:otherwise>
                              <a href="#" class="underline btn-open-dialog testWaitingList"  data-modal-type="waiting_list" data-hwid="${result.hwId }" data-hwtype="${result.hwType }" data-hwwaitingtype="2">
                                  ${result.homeworkScrCnt}/${result.memberCnt }
                              </a>
                          </c:otherwise>
                      </c:choose>
                  </td>
                </tr>
            </c:forEach>
            <c:if test="${fn:length(selectHomeworkList) == 0}">
                <tr>
                    <td class="listCenter" colspan="12"><spring:message code="common.nodata.msg" /></td>
                </tr>
            </c:if>
          </tbody>
    </table>
    
    <div class="btn_r">
        <a href="/mng/lms/crcl/homeworkRegister.do?registAction=regist&mode=curriculum&crclId=${param.crclId }&menu=CURRICULUM_MANAGE"><img src="/template/manage/images/btn/btn_regist.gif"></a>
    </div>
    
    <div id="paging">
        <c:url var="pageUrl" value="/mng/lms/crcl/homeworkList.do?${_BASE_PARAM}">
        </c:url>
    
        <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
    </div>
</div>

<div class="my-dialog" style="overflow-y:auto;height:400px;">
    <div class="modal_header"></div>
    <div class="modal_body ">
        <div>
            <ul id="waitingMemberList">
            
            </ul>
        </div>
        <div class="box_btn">
            <button class="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
        </div>
    </div>
</div>
<div id="dialog-background"></div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>