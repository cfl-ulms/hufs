<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crclb"/>
<c:set var="_ACTION" value=""/>
<%--과정체계코드 --%>
<c:set var="sysCode" value="${fn:split(curriculumbaseVO.sysCodePath,'>')}"/>
<script>
$(document).ready(function(){
	<c:choose>
		<c:when test="${curriculumbaseVO.totalTimeAt ne 'Y'}">
			$(".timeAt").hide();
			$("#totalTime").val("0");
			$("#weekNum").val("0");
			$("#dayTime").val("0");
			$("#totalTimeAt").val("N");
		</c:when>
		<c:otherwise>
			$(".timeAt").show();
			$("#totalTimeAt").val("Y");
		</c:otherwise>
	</c:choose>
});
</script>

<table class="chart2">
	<caption>등록폼</caption>
	<colgroup>
		<col width="15%"/>
		<col width="*"/>
		<col width="15%"/>
		<col width="*"/>
	</colgroup>
	<tbody>
		<tr>
			<th><label>이수구분</label></th>
			<td><c:out value="${curriculumbaseVO.divisionNm }"/></td>
			<th><label>학점인정여부</label></th>
			<td>
				<c:choose>
					<c:when test="${curriculumbaseVO.gradeAt ne 'N'}">예(${curriculumbaseVO.gradeNum} 학점)</c:when>
					<c:otherwise>아니오</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<th><label>관리구분</label></th>
			<td colspan="3">
				<c:out value="${curriculumbaseVO.controlNm }"/>
				<c:if test="${curriculumbaseVO.projectAt eq 'Y'}"> / 프로젝트 과정</c:if>
			</td>
		</tr>
		<tr>
			<th><label>대상</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${curriculumbaseVO.targetType eq 'Y'}">본교생</c:when>
					<c:otherwise>일반</c:otherwise>
				</c:choose>
				> <c:out value="${curriculumbaseVO.targetDetailNm}"/>
				
				<c:if test="${curriculumbaseVO.totalTimeAt eq 'Y'}">총 시간 등록과정</c:if>
			</td>
		</tr>
		<tr>
			<th><label>성적처리기준</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${curriculumbaseVO.evaluationAt eq 'Y'}">절대평가  > <c:out value="${curriculumbaseVO.gradeTypeNm}"/></c:when>
					<c:otherwise>상대평가</c:otherwise>
				</c:choose>
			</td>	
		</tr>
		<tr>
			<th><label>운영보고서 유형</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${'NOR' eq curriculumbaseVO.reportType}">일반과정</c:when>
					<c:otherwise>프로젝트과정</c:otherwise>
				</c:choose>
			</td>	
		</tr>
		<tr>
			<th><label>과정만족도설문유형</label></th>
			<td>
				<c:forEach var="result" items="${surveyList}" varStatus="status">
					<c:if test="${result.schdulId eq curriculumbaseVO.surveySatisfyType}">${result.schdulNm}</c:if>
				</c:forEach>
			</td>
			<th><label>교원대상 과정만족도 설문 유형</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${empty curriculumbaseVO.professorSatisfyType}">
						선택안함
					</c:when>
					<c:otherwise>
						<c:forEach var="result" items="${surveyList3}" varStatus="status">
							<c:if test="${result.schdulId eq curriculumbaseVO.professorSatisfyType}">${result.schdulNm}</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<th><label>개별 수업만족도조사 실시 여부</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${curriculumbaseVO.surveyIndAt ne 'N'}">
						예
						<c:forEach var="result" items="${surveyList2}" varStatus="status">
							<c:if test="${result.schdulId eq curriculumbaseVO.surveyInd}">(${result.schdulNm})</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>아니오</c:otherwise>
				</c:choose>
			</td>	
		</tr>
		<tr>
			<th><label>학생 수강신청 제출서류 여부</label></th>
			<td colspan="3">
				<c:choose>
					<c:when test="${curriculumbaseVO.stdntAplyAt eq 'Y'}">예</c:when>
					<c:otherwise>아니오</c:otherwise>
				</c:choose>
			</td>	
		</tr>
		<tr class="stdntAply" <c:if test="${curriculumbaseVO.stdntAplyAt ne 'Y'}">style="display:none;"</c:if>>
			<th><label>신청서</label></th>
			<td colspan="3">
				<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.aplyFile}&amp;fileSn=0">
					<img src='/template/manage/images/ico_file.gif' alt='파일'/><c:out value="${curriculumbaseVO.aplyFileNm}"/>
				</a>
			</td>	
		</tr>
		<tr class="stdntAply" <c:if test="${curriculumbaseVO.stdntAplyAt ne 'Y'}">style="display:none;"</c:if>>
			<th><label>계획서</label></th>
			<td colspan="3">
				<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.planFile}&amp;fileSn=0">
					<img src='/template/manage/images/ico_file.gif' alt='파일'/><c:out value="${curriculumbaseVO.planFileNm}"/>
				</a>
			</td>	
		</tr>
		<tr class="stdntAply" <c:if test="${curriculumbaseVO.stdntAplyAt ne 'Y'}">style="display:none;"</c:if>>
			<th><label>기타</label></th>
			<td colspan="3">
				<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.etcFile}&amp;fileSn=0">
					<img src='/template/manage/images/ico_file.gif' alt='파일'/><c:out value="${curriculumbaseVO.etcFileNm}"/>
				</a>
			</td>	
		</tr>
		<c:if test="${fn:length(refFileList) > 0}">
			<tr>
				<th><label>(책임교원대상) 과정 학습 참고자료</label></th>
				<td colspan="3">
					<ul id="refeFile_box">
					<c:forEach var="result" items="${refFileList}" varStatus="status">
						<li class='refFileList'>
							<a href="/cmm/fms/FileDown.do?atchFileId=${result.atchFileId}&amp;fileSn=0">
								<img src='/template/manage/images/ico_file.gif' alt='파일'/><c:out value="${result.orignlFileNm}"/>
							</a>
						</li>
					</c:forEach>
					</ul>
				</td>	
			</tr>
		</c:if>
	</tbody>
</table>