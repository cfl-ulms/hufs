<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js" />

<table class="tab_table">
	<tbody>
		<c:choose>
			<c:when test="${param.menu eq 'CLASS_MANAGE'}">
				<tr>
					<c:choose>
						<c:when test="${param.depth1 eq 'CRCL_BOARD'}">
							<th class="alC <c:if test="${param.step eq '7'}">on</c:if>"><a href="/mng/lms/manage/lmsBbsList.do?crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}">게시판</a></th>
						</c:when>
						<c:when test="${param.depth1 eq 'CRCL_REPORT'}">
							<th class="alC <c:if test="${param.step eq '8'}">on</c:if>"><a href="/mng/lms/manage/manageReport.do?crclId=<c:out value="${param.crclId}&menu=CLASS_MANAGE&depth1=${param.depth1}"/>">운영보고서</a></th>
						</c:when>
						<c:otherwise>
							<th class="alC <c:if test="${param.step eq '1'}">on</c:if>"><a href="/mng/lms/crcl/selectCurriculum.do?plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}&step=1">과정계획</a></th>
							<c:if test="${param.processSttusCodeDate ne '5'}">
								<c:if test="${param.totalTimeAt eq 'Y'}">
									<th class="alC <c:if test="${param.step eq '2'}">on</c:if>"><a href="/mng/lms/manage/schedule.do?plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}&step=2">시간표</a></th>
									<th class="alC <c:if test="${param.step eq '3'}">on</c:if>"><a href="/mng/lms/manage/studyPlanView.do?plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}&step=3">수업계획</a></th>
									<c:if test="${evtId eq 'CTG_0000000000000058'}">
			    						<li class="tab-list <c:if test="${param.step eq '4'}">on</c:if>"><a href="/lms/quiz/QuizList.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=4&tabType=T">평가</a></li>
							      	</c:if>
								</c:if>
								<th class="alC <c:if test="${param.step eq '5'}">on</c:if>"><a href="/mng/lms/manage/homeworkList.do?plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}&searchHwCode=2">과제</a></th>
								<th class="alC <c:if test="${param.step eq '6'}">on</c:if>"><a href="/mng/lms/atd/selectAttendList.do?plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}">출석</a></th>
								<th class="alC <c:if test="${param.step eq '7'}">on</c:if>"><a href="/mng/lms/manage/lmsBbsList.do?crclId=<c:out value="${param.crclId}"/>&menu=CLASS_MANAGE&depth1=${param.depth1}">게시판</a></th>
								<th class="alC <c:if test="${param.step eq '8'}">on</c:if>"><a href="/mng/lms/manage/manageReport.do?crclId=<c:out value="${param.crclId}&menu=CLASS_MANAGE&depth1=${param.depth1}"/>">운영보고서</a></th>
							</c:if>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<th class="alC <c:if test="${param.step eq '1'}">on</c:if>"><a href="/mng/lms/manage/lmsControl.do?crclId=<c:out value="${param.crclId}"/>&menu=${param.menu }&depth1=${param.depth1}">관리</a></th>
					<c:if test="${param.processSttusCodeDate ne '5'}">
						<th class="alC <c:if test="${param.step eq '2'}">on</c:if>"><a href="/mng/lms/crcl/selectCurriculumStudent.do?crclId=<c:out value="${param.crclId}"/>&importFlag=1">학생</a></th>
					</c:if>
					<th class="alC <c:if test="${param.step eq '3'}">on</c:if>"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}"/>">과정</a></th>
					<c:if test="${param.processSttusCodeDate ne '5'}">
						<c:if test="${param.totalTimeAt eq 'Y'}">
							<th class="alC <c:if test="${param.step eq '4'}">on</c:if>"><a href="/mng/lms/manage/schedule.do?crclId=<c:out value="${param.crclId}"/>">시간표</a></th>
							<th class="alC <c:if test="${param.step eq '5'}">on</c:if>"><a href="/mng/lms/manage/studyPlan.do?crclId=<c:out value="${param.crclId}"/>">수업</a></th>
						</c:if>
						<th class="alC <c:if test="${param.step eq '6'}">on</c:if>"><a href="/mng/lms/manage/lmsBbsList.do?crclId=<c:out value="${param.crclId}"/>">게시판</a></th>
						<th class="alC <c:if test="${param.step eq '7'}">on</c:if>"><a href="/mng/lms/manage/gradeTotal.do?crclId=<c:out value="${param.crclId}"/>">성적</a></th>
						<th class="alC <c:if test="${param.step eq '8'}">on</c:if>"><a href="/mng/lms/crcl/homeworkList.do?crclId=<c:out value="${param.crclId}"/>">과제</a></th>
						<th class="alC <c:if test="${param.step eq '9'}">on</c:if>"><a href="/mng/lms/manage/curriculumSurveyList.do?crclId=<c:out value="${param.crclId}"/>">설문</a></th>
						<th class="alC <c:if test="${param.step eq '10'}">on</c:if>"><a href="/mng/lms/manage/manageReport.do?crclId=<c:out value="${param.crclId}"/>&step=10">운영보고서</a></th>
					</c:if>
				</tr>
			</c:otherwise>

		</c:choose>
	</tbody>
</table>
