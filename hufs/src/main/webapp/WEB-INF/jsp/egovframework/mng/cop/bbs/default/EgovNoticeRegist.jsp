<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/cop/bbs"/>
<c:set var="_ACTION" value=""/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>

<%-- nttNo = 0은 취소/환불 규정 때문에 처리함 --%>
<c:if test="${searchVO.nttNo ne 0 }">
	<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
		<c:param name="title" value="${brdMstrVO.bbsNm}"/>
	</c:import>
</c:if>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board2.js?v=1" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<%-- K-MOOC --%>
<c:if test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000007'}">
<script>
$(function() {
  $( "#tmp01" ).datepicker({
    dateFormat: "yy-mm-dd"
  });
  $( "#tmp02" ).datepicker({
    dateFormat: "yy-mm-dd"
  });
});
</script>
</c:if>

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">
	function fn_egov_regist() {

		tinyMCE.triggerSave();
		
		if (!fn_egov_bbs_basic_regist(document.board)){
			return false;
		}
		
		$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());
		
		<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
			for(var cmIdx = 1 ; cmIdx <= boardCateLevel ; cmIdx++){
				var cmObj = document.getElementById("ctgry" + cmIdx);
				if(cmObj != null) {
					if(fn_egov_SelectBoxValue("ctgry" + cmIdx) != '') {
						document.board.ctgryId.value = fn_egov_SelectBoxValue("ctgry" + cmIdx);
					}
				}
			}
	    </c:if>

	    <c:choose>
	    	<c:when test="${searchVO.registAction eq 'updt'}">
				if (!confirm('<spring:message code="common.update.msg" />')) {
					 return false
				}
			</c:when>
			<c:otherwise>
				if (!confirm('<spring:message code="common.regist.msg" />')) {
					return false;
				}
			</c:otherwise>
		</c:choose>
	}
	
	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
	
		var boardCateLevel = ${boardCateLevel};
		var boardCateList = new Array(${fn:length(boardCateList)});
		<c:forEach var="cate" items="${boardCateList}" varStatus="status">
			boardCateList[${status.index}] = new ctgryObj('${cate.upperCtgryId}', '${cate.ctgryId}', '${cate.ctgryNm}', ${cate.ctgryLevel});
		</c:forEach>
	</c:if>
	
	
	$(document).ready(function(){
		var adfile_config = {
				siteId:"<c:out value='${brdMstrVO.siteId}'/>",
				pathKey:"Board",
				appendPath:"<c:out value='${brdMstrVO.bbsId}'/>",
				editorId:"${_EDITOR_ID}",
				fileAtchPosblAt:"${brdMstrVO.fileAtchPosblAt}",
				maxMegaFileSize:${brdMstrVO.posblAtchFileSize},
				atchFileId:"${board.atchFileId}"
			};
			
		fnCtgryInit('<c:out value='${board.ctgryPathById}'/>');
		fn_egov_bbs_editor(adfile_config);
		
		$("#egovComFileUploader").change(function(){
			var fileVal = $(this).val(),
				ext = fileVal.split("."),
				splitSize = ext.length - 1,
				fileExt = ext[splitSize].toLowerCase();

			
			if($("#box_images > li").length > 0){
				alert("기존 책표지를 삭제 후 등록해주세요.")
				$(this).val("");
			}else if(fileExt=="bmp" || fileExt=="gif" || fileExt=="jpeg" || fileExt=="jpg" || fileExt=="png" || fileExt=="BMP" || fileExt=="GIF" || fileExt=="JPEG" || fileExt=="JPG" || fileExt=="PNG"){ 
				
			}else{
				alert("이미지 확장자만 저장할 수 있습니다.");
				$(this).val("");
			}
		});
	});
</script>


<div id="cntnts">
	<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
        <input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
		<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>" />
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
	    <input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
	    <input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
		<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>" />
		<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
		<input type="hidden" name="registAction" value="<c:out value='${searchVO.registAction}'/>"/>
		<input type="hidden" name="tmplatImportAt" value="<c:out value='${searchVO.tmplatImportAt}'/>"/>
		<input type="hidden" name="siteId" value="<c:out value='${searchVO.siteId}'/>"/>
        
        <form:hidden path="nttNo"/>
        <form:hidden path="ctgryId"/>
        <form:hidden path="atchFileId"/>
                    
		<table class="chart2" summary="작성인, 제목, 내용, 파일첨부를 입력하는 표입니다." >
			<caption> </caption>
			<colgroup>
				<col width="20%" />
				<col width="80%" />
			</colgroup>
			<tbody>
				<c:choose>
					<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
						<input type="hidden" name="nttSj" value="dummy"/>
						<tr>
							<th><spring:message code="cop.processSttus" /></th>
							<td>
								<%-- 
								<c:choose>
									<c:when test="${not empty board.processSttusCode}"><c:set var="processSttusCode" value="${board.processSttusCode}"/></c:when>
									<c:otherwise><c:set var="processSttusCode" value="QA03"/></c:otherwise>
								</c:choose>
								 --%>
								<select name="processSttusCode" class="select">
									<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
										<option value='<c:out value="${resultState.code}"/>' <c:if test="${resultState.code eq 'QA03'}">selected="selected"</c:if>><c:out value="${resultState.codeNm}"/></option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>
								<label for="nttSj">
									<c:choose>
										<%-- 교재/사전 --%>
										<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">교재명</c:when>
										<%-- K-MOOC --%>
										<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000007'}">강좌명</c:when>
										<c:otherwise><spring:message code="cop.nttSj" /></c:otherwise>
									</c:choose>
								</label>
							</th>
							<td><form:input path="nttSj" cssClass="inp_long"/><br/><form:errors path="nttSj" /></td>
						</tr>
						
						<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
						<tr>
							<th>
								<label for="ctgry1">
									<c:choose>
										<%-- 교재/사전 --%>
										<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">언어</c:when>
										<c:otherwise><spring:message code="cop.category.view" /></c:otherwise>
									</c:choose>
								</label>
							</th>
							<td>
								<c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
									<c:choose>
										<c:when test="${status.first}">
											<select name="regCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})">
												<option value=""><spring:message code="cop.select" /></option>
												<c:forEach var="cate" items="${boardCateList}">
													<c:if test="${cate.ctgryLevel eq 1 }">
														<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
													</c:if>
												</c:forEach>
											</select>
										</c:when>
										<c:otherwise><select name="regCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})"><option value=""><spring:message code="cop.select" /></option></select></c:otherwise>
									</c:choose>
								</c:forEach>
								<script type="text/javascript">
									fnCtgryInit('${board.ctgryPathById}');
								</script>
							</td>
						</tr>
						</c:if>	
						
						 <c:choose>
						 	<%-- 교재/사전 --%>
						 	<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">
						 		<tr>
									<th><label>출판사</label></th>
									<td><form:input path="tmp01" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>저자</label></th>
									<td><form:input path="tmp02" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>책표지</label></th>
									<td>
										<input name="file_1" id="egovComFileUploader" type="file" class="inp" /><br/>
										<span>권장사이즈 220 * 310</span>
										<c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
									    	<c:param name="atchFileId" value="${board.atchFileId}" />
									    	<c:param name="mngAt" value="Y"/>
									    	<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
									    	<c:param name="siteId" value="${brdMstrVO.siteId}"/>
									    	<c:param name="width" value="220"/>
									    	<c:param name="height" value="310"/>
										</c:import>
									</td>
								</tr>
								<tr>
									<th><label>구매URL</label></th>
									<td><form:input path="tmp05" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>키워드</label></th>
									<td><form:input path="tmp04" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>E-Book 여부</label></th>
									<td>
										<label>예 : <input type="radio" name="tmp03" value="Y" <c:if test="${board.tmp03 eq 'Y'}">checked="checked"</c:if>/></label>&nbsp;
										<label>아니오 : <input type="radio" name="tmp03" value="N" <c:if test="${board.tmp03 ne 'Y'}">checked="checked"</c:if>/></label>
									</td>
								</tr>
								<tr>
									<th><label>학습자료로 공개</label></th>
									<td>
										<label>공개 : <input type="radio" name="tmp06" value="Y" <c:if test="${board.tmp06 eq 'Y'}">checked="checked"</c:if>/></label>&nbsp;
										<label>비공개 : <input type="radio" name="tmp06" value="N" <c:if test="${board.tmp06 ne 'Y'}">checked="checked"</c:if>/></label>
						            </td>
								</tr>
								<tr>
									<th><label for="noticeAt">메인노출 여부</label></th>
									<td>
										<label><spring:message code="button.yes" /> : <form:radiobutton path="noticeAt"  value="Y"/>&nbsp;</label>
						                <label><spring:message code="button.no" /> : <form:radiobutton path="noticeAt"  value="N"/></label>
						                <br/><form:errors path="noticeAt" />
									</td>
								</tr>
						 	</c:when>
						 	<%-- K-MOOC --%>
						 	<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000007'}">
						 		<tr>
									<th><label>강좌표지</label></th>
									<td>
										<input name="file_1" id="egovComFileUploader" type="file" class="inp" /><br/>
										<span>권장사이즈 395 * 240</span>
										<c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
									    	<c:param name="atchFileId" value="${board.atchFileId}" />
									    	<c:param name="mngAt" value="Y"/>
									    	<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
									    	<c:param name="siteId" value="${brdMstrVO.siteId}"/>
									    	<c:param name="width" value="395"/>
									    	<c:param name="height" value="240"/>
										</c:import>
									</td>
								</tr>
						 		<tr>
									<th><label>강좌기간</label></th>
									<td><form:input path="tmp01" readonly="true"/> ~ <form:input path="tmp02" readonly="true"/></td>
								</tr>
								<tr>
									<th><label>강좌운영기관</label></th>
									<td><form:input path="tmp03" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>K-MOOC URL</label></th>
									<td><form:input path="tmp05" cssClass="inp_long"/></td>
								</tr>
								<tr>
									<th><label>학습자료로 공개</label></th>
									<td>
										<label>공개 : <input type="radio" name="tmp06" value="Y" <c:if test="${board.tmp06 eq 'Y'}">checked="checked"</c:if>/></label>&nbsp;
										<label>비공개 : <input type="radio" name="tmp06" value="N" <c:if test="${board.tmp06 ne 'Y'}">checked="checked"</c:if>/></label>
						            </td>
								</tr>
						 	</c:when>
						 	<c:otherwise>
						 		<c:if test="${brdMstrVO.othbcUseAt eq 'Y'}">
									<tr>
										<th><label for="othbcAt"><spring:message code="cop.publicAt" /></label></th>
										<td>
											<spring:message code="cop.public" /> : <form:radiobutton path="othbcAt"  value="Y"/>&nbsp;
							                <spring:message code="cop.private" /> : <form:radiobutton path="othbcAt"  value="N"/>
							                <br/><form:errors path="othbcAt" />
										</td>
									</tr>
								</c:if>
						 	</c:otherwise>
						 </c:choose>
					</c:otherwise>
				</c:choose>
				
				<tr>
					<td colspan="2">
						<form:textarea path="nttCn" rows="30" cssStyle="width:100%"/><br/><form:errors path="nttCn" />
					</td>
				</tr>
				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<tr>
						<th><spring:message code="cop.atchFile" /></th>
						<td>
							<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
								<c:param name="editorId" value="${_EDITOR_ID}"/>
								<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
						    	<c:param name="param_atchFileId" value="${board.atchFileId}" />
						    	<c:param name="imagePath" value="${_IMG }"/>
						    	<c:param name="mngAt" value="Y"/>
							</c:import>
							<noscript>
								<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
							</noscript>
						</td>
					</tr>
				</c:if>
			</tbody>
		</table>
		
		<c:if test="${board.bbsId eq 'BBSMSTR_000000000025'}">
			<strong style="color:red;">※ 첨부파일에 첫번째가 "취소 신청서", 두번째까 "환불 신청서"로 사용자 페이지에 노출됩니다. 순서에 맞게 등록 부탁드립니다.</strong>
		</c:if>
 
		<div class="btn_r">
			<c:choose>
				<c:when test="${searchVO.registAction eq 'regist' }">
					<input type="image" src="${_IMG}/btn/btn_regist.gif" alt="등록"/>
				</c:when>
				<c:when test="${searchVO.registAction eq 'updt' }">
					<input type="image" src="${_IMG}/btn/btn_modify.gif" alt="수정"/>
				</c:when>
				<c:when test="${searchVO.registAction eq 'reply' }">
					<input type="image" src="${_IMG}/btn/btn_reply.gif" alt="답변"/>
				</c:when>
			</c:choose>			   
			  <c:url var="selectBoardListUrl" value="${_PREFIX}/selectBoardList.do">
			  	<c:param name="siteId" value="${searchVO.siteId}"/>
				<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
		        <c:param name="bbsId" value="${brdMstrVO.bbsId}" />
		        <c:param name="trgetId" value="${searchVO.trgetId}" />
		        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
				<c:param name="searchCnd" value="${searchVO.searchCnd}" />
				<c:param name="searchWrd" value="${searchVO.searchWrd}" />
				<c:param name="searchCate" value="${searchVO.searchCate}" />
				<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		  			<c:if test="${not empty searchCate}">
		  				<c:param name="searchCateList" value="${searchCate}" />
		  			</c:if>
		  		</c:forEach>
		      </c:url>
		      <%-- nttNo = 0은 취소/환불 규정 때문에 처리함 --%>
			  <c:choose>
	      		<c:when test="${searchVO.nttNo ne 0 }"><a href="${selectBoardListUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록" /></a></c:when>
	      		<c:otherwise>
	      			<a href="/mng/cop/bbs/selectBoardArticle.do?nttNo=0&siteId=${searchVO.siteId}&bbsId=${brdMstrVO.bbsId}&trgetId=SYSTEM_DEFAULT_BOARD">
	      				<img src="${_IMG}/btn/btn_list.gif" alt="목록" />
	      			</a>
	      		</c:otherwise>
	      	</c:choose>
		</div>
	</form:form>
      
</div>        

<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8"/>	