<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CURRICULUM_MANAGE"/>
    <c:param name="depth1" value="BASE_CRCL"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과정등록관리"/>
</c:import>

<script>
$(document).ready(function(){
	//초기 값 설정
    $(".table-inside-input").val($("input[name=hwIdList]:checked").length);
    
    //반 선택
    $(".selClass").change(function(){
        var clas = $(this).val();
        
        $("input[name=searchClassCnt]").val(clas);
        $("form[name=searchFrm]").submit();
    });
    
    //저장
    $("#btn-reg").click(function(){
        $("#frm").submit();
    });
});
</script>
<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm}"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
<c:if test="${curriculumVO.processSttusCode > 0}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        <c:param name="step" value="8"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
    <ul class="group_list">
        <li><a href="/mng/lms/crcl/homeworkList.do?crclId=${param.crclId }">과제</a></li>
        <li class="partition"> | </li>
        <li class="act"><a href="/mng/lms/crcl/homeworkTestList.do?crclId=${param.crclId }">과제평가</a></li>
    </ul>
    <div id="bbs_search">
        <form name="searchFrm" method="post" action="<c:url value="/mng/lms/crcl/homeworkTestList.do"/>">
            <input type="hidden" name="crclId" value="${param.crclId }"/>
            <input type="hidden" name="searchClassCnt" value="${searchVO.searchClassCnt}"/>

            <input type="text" name="searchStudentUserNm" value="${searchVO.searchStudentUserNm}" id="searchStudentUserNm" class="searchStudentUserNm" placeholder="학생 이름"/>
            <br/>
            <input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
        </form>
    </div>
    <form name="frm" id="frm" method="post" action="<c:url value="/mng/lms/crcl/homeworkTestUpdate.do"/>">
        <input type="hidden" name="crclId" value="${searchVO.crclId}"/>
        <input type="hidden" name="searchClassCnt" value="${searchVO.searchClassCnt}"/>

        <select class="selClass">
            <option value="0">전체</option>
            <c:forEach var="result" items="${selectGroupList}">
                <option value="<c:out value="${result.classCnt}"/>" <c:if test="${result.classCnt eq searchVO.searchClassCnt}">selected="selected"</c:if>><c:out value="${result.classCnt}"/>반</option>
            </c:forEach>
        </select>
        <br />
        <br />

        <div class="tbl">
            <div class="tbl-left" style="width:609px;margin-right:1px;float:left;">
			    <table class="chart_board2">
			        <colgroup>
                        <col>
                        <col>
                        <col>
                        <col>
                        <col>
                        <col>
                        <col>
                    </colgroup>
			        <thead>
			            <tr style="height: 60px;">
			                <th scope='colgroup'  rowspan='3' colspan='4'>학생정보</th>
			                <th scope='colgroup'  colspan='3'>과제마감일</th>
			            </tr>
			            <tr style="height: 60px;">
		                    <th scope='colgroup'  colspan='3'>과제명</th>
		                </tr>
		                <tr style="height: 60px;">
		                    <th scope='colgroup'  colspan='3'>성적반영<input type='text' class='table-inside-input' value='0' /></th>
		                </tr>
		                <tr style="height: 60px;">
		                    <th scope='col'>소속</th>
		                    <th scope='col'>생년월일</th>
		                    <th scope='col'>이름</th>
		                    <th scope='col'>조</th>
		                    <th scope='col'>변환점수</th>
		                    <th scope='col'>총점</th>
		                    <th scope='col'>석차</th>
		                </tr>
		            </thead>
			        <tbody>
		                <c:forEach var="result" items="${selectStudentList}" varStatus="stauts">
		                    <tr class="">
		                        <td scope='row' class='title'>
		                          <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.mngDeptNm}"/></span></div>
		                        </td>
		                        <td><c:out value="${result.brthdy}"/></td>
		                        <td><c:out value="${result.userNm}"/></td>
		                        <td><c:out value="${result.groupCnt}"/>조</td>
		                        <td><c:out value="${fn:replace(result.chScr,'.0','')}"/></td>
		                        <td><c:out value="${fn:replace(result.totScr,'.0','')}"/></td>
		                        <td><c:out value="${result.rk}"/></td>
		                     </tr>
		                </c:forEach>
		            </tbody>
			    </table>
			</div>
			<div class="tbl-main-wrap" style="overflow-y:auto;">
			    <div class="tbl-main">
			        <table class="chart_board2">
	                    <thead>
	                        <tr style="height: 60px;">
                                <c:forEach var="result" items="${homeworkList}" varStatus="status">
                                    <th scope='colgroup' colspan='2' style="min-width: 65px;"><c:out value="${result.closeDate}"/></th>
                                </c:forEach>
                            </tr>
                            <tr style="height: 60px;">
                                <c:forEach var="result" items="${homeworkList}" varStatus="status">
                                    <th scope='colgroup' class='title' colspan='2' style="min-width: 65px;">
                                        <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.nttSj}"/></span></div>
                                    </th>
                                </c:forEach>
                            </tr>
                            <tr style="height: 60px;">
                                <c:forEach var="result" items="${homeworkList}" varStatus="status">
                                    <th scope='colgroup' colspan='2' style="min-width: 65px;">
                                        <label class='checkbox table-inside-checkbox'>
                                            <input type='checkbox' name='hwIdList' value="${result.hwId}" <c:if test="${result.scoreApplyAt eq 'Y'}">checked="checked"</c:if>><span class='custom-checked'></span>
                                        </label>
                                    </th>
                                </c:forEach>
                            </tr>
                            <tr style="height: 60px;">
                                <c:forEach var="result" items="${homeworkList}" varStatus="status">
                                    <th scope='colgroup' style="min-width: 65px;">총점</th>
                                    <th scope='colgroup' style="min-width: 65px;">석차</th>
                                </c:forEach>
                            </tr>
	                    </thead>
	                    <tbody>
	                        <c:forEach var="stuList" items="${selectStudentList}">
                                <tr>
                                    <c:forEach var="result" items="${scoreList}" varStatus="status">
                                        <c:if test="${stuList.userId eq result.userId}">
                                            <td><c:out value="${result.scr}"/></td>
                                            <td><c:out value="${result.rk}"/></td>
                                        </c:if>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
	                    </tbody>
	                </table>
			    </div>
			</div>
		</div>
    </form>
    <div class="btn_c">
        <a href="#none" id="btn-reg" class="btn_mngTxt">저장</a>
    </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>