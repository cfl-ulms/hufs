<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchGroupCode}"><c:param name="searchGroupCode" value="${searchVO.searchGroupCode}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchBrthdy}"><c:param name="searchBrthdy" value="${searchVO.searchBrthdy}" /></c:if>
	<c:if test="${not empty searchVO.searchMoblphonNo}"><c:param name="searchMoblphonNo" value="${searchVO.searchMoblphonNo}" /></c:if>
	<c:if test="${not empty searchVO.searchUserId}"><c:param name="searchUserId" value="${searchVO.searchUserId}" /></c:if>
	<c:if test="${not empty searchVO.searchEmailAdres}"><c:param name="searchEmailAdres" value="${searchVO.searchEmailAdres}" /></c:if>
	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
</c:url>
<% /*URL 정의*/ %>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CERT_MANAGE"/>
	<c:param name="depth1" value=""/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="학생별조회"/>
</c:import>

<script>
//달력
$(function() {
  $("#searchStartDate, #searchEndDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	
	//삭제
	$(".btn_del").click(function(){
		if(confirm("해당 과정을 정말 삭제하시겠습니까?")){
			return;
		}else{
			return false;
		}
	});
});
</script>
<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/selectCertificate.do"/>">
	  		<select id="searchCrclYear" name="searchCrclYear">
	  			<option value="">선택</option>
				<c:forEach var="result" items="${yearList}" varStatus="status">
	  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
	  			</c:forEach>
			</select>

			<select id="searchCrclTerm" name="searchCrclTerm">
				<option value="">학기 전체</option>
				<c:forEach var="result" items="${crclTermList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~
			<input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<label><strong>과정명 : </strong> <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요."/></label>
			
			<select id="searchCondition" name="searchCondition">
				<option value="ALL">전체</option>
				<option value="Y" <c:if test="${searchVO.searchCondition eq 'Y'}">selected="selected"</c:if>>유효한 수료증</option>
				<option value="X" <c:if test="${searchVO.searchCondition eq 'X'}">selected="selected"</c:if>>폐기된 수료증</option>
			</select>
			<br/>
	  		<label><strong>소속 : </strong> <input type="text" name="searchGroupCode" value="${searchVO.searchGroupCode}" class="inp_s" id="inp_text" placeholder="소속을 입력해주세요."/></label>
			<label><strong>이름 : </strong> <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" class="inp_s" id="inp_text" placeholder="이름을 입력해주세요."/></label>
			<br/>
			<label><strong>생년월일 : </strong> <input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy}" class="inp_s" id="inp_text" placeholder="숫자만 입력해주세요."/></label>
			<label><strong>연락처 : </strong> <input type="text" name="searchMoblphonNo" value="${searchVO.searchMoblphonNo}" class="inp_s" id="inp_text" placeholder="'-'를 제외하고 입력해주세요 예)01012345678"/></label>
			<br/>
			<label><strong>아이디 : </strong> <input type="text" name="searchUserId" value="${searchVO.searchUserId}" class="inp_s" id="inp_text" placeholder="아이디를 입력해주세요."/></label>
			<label><strong>이메일 : </strong> <input type="text" name="searchEmailAdres" value="${searchVO.searchEmailAdres}" class="inp_s" id="inp_text" placeholder="이메일을 입력해주세요."/></label>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	
	<p>검색결과 : <c:out value="${paginationInfo.totalRecordCount}" />건</p>
	<table class="chart_board">
	    <colgroup>
			<col width="120px"/>
			<col width="60px"/>
			<col width="60px"/>
			<col width="80px"/>
			<col width="500px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="150px"/>
			<col width="200px"/>
			<col width="50px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>수료증번호</th>
	      	<th>년도</th>
	      	<th>학기</th>
	      	<th>기간</th>
	        <th>과정명</th>
	        <th>소속</th>
	        <th>이름</th>
	        <th>생년월일</th>
	        <th>연락처</th>
	        <th>아이디</th>
	        <th>이메일</th>
	        <th>조회</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:url var="viewUrl" value="/lms/selectCertificate.do">
					<c:param name="searchCertificateId" value="${result.certificateId}"/>
				</c:url>
	    		<tr <c:if test="${result.reissueAt eq 'Y'}"> style="background:#eee;"</c:if>>
	    			<td><c:out value="${result.certificateId}"/></td>
	    			<td><c:out value="${result.crclYear}"/></td>
	    			<td><c:out value="${result.crclTermNm}"/></td>
	    			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	    			<td class="tit"><c:out value="${result.crclNm}"/></td>
	    			<td><c:out value="${result.groupCode }"/></td>
	    			<td><c:out value="${result.userNm}"/></td>
	    			<td><c:out value="${fn:substring(result.brthdy,0,4)}"/>-<c:out value="${fn:substring(result.brthdy,4,6)}"/>-<c:out value="${fn:substring(result.brthdy,6,8)}"/></td>
	    			<td>(+<c:out value="${result.geocode}"/>) <c:out value="${result.moblphonNo}"/></td>
	    			<td><c:out value="${result.userId}"/></td>
	    			<td><c:out value="${result.emailAdres}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.reissueAt eq 'Y'}">
	    						폐기됨
	    					</c:when>
	    					<c:otherwise>
	    						<a href="${viewUrl}" target="_blank">조회</a>
	    					</c:otherwise>
	    				</c:choose>
	    			</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="12"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>
    </table>

    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/selectCertificate.do${_BASE_PARAM}">
	    </c:url>

	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>