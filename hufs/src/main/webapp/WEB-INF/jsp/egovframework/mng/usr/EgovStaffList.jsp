<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
		<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="STAF_MANAGE"/>
	<c:param name="title" value="직원관리"/>
</c:import>	
<c:set var="_ACTION" value="/mng/usr/EgovStaffManage.do" />

<div id="cntnts">
		<form:form name="listForm" action="${_ACTION }" method="post">
		<%-- <input type="hidden" name="siteId" value="${searchVO.siteId}"/> --%>
		<div id="bbs_search">
			<label for="useYn" class="hdn">계정사용여부</label>
			<select name="useYn" id="useYn">
				<option value="">전체</option>
				<option value="Y" <c:if test="${searchVO.useYn eq 'Y'}">selected="selected"</c:if>>사용중</option>
				<option value="N" <c:if test="${searchVO.useYn eq 'N'}">selected="selected"</c:if>>사용안함</option>
	  	  	</select>
			<select name="searchCondition">
				<option value="">전체</option>
				<option value="1" <c:if test="${searchVO.searchCondition == '1'}">selected</c:if>>소속</option>
				<option value="2" <c:if test="${searchVO.searchCondition == '2'}">selected</c:if>>사번</option>
				<option value="3" <c:if test="${searchVO.searchCondition == '3'}">selected</c:if>>이름</option>
	  	  	</select>
			<label><input type="text" name="searchKeyword" value="${searchVO.searchKeyword}" id="searchKeyword" /></label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</div>
		<p class="total">검색결과 : ${paginationInfo.totalRecordCount}명</p>

		<table class="chart_board">
		<colgroup>
			<col width="70"/>				
			<col width="120"/>
			<col width="70"/>
			<col width="70"/>
			<col width="70"/>
		</colgroup>
		<thead>
			<tr>
				<th>번호</th>
				<th>소속</th>
				<th>사번</th>
				<th>이름</th>
				<th>계정사용여부</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
				<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
				<td class="listtd"><c:out value="${result.groupCode}" /></td>
				<td class="listtd"><c:out value="${result.userSchNo}" /></td>
				<td class="listtd">
					<a href="/mng/usr/EgovStaffAddView.do?userSeCode=10&userId=${result.userId}">
						<c:out value="${result.userNm}" />
					</a>
				</td>
				<td class="listtd"><c:out value="${result.useYn}" /></td>
				
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="10">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>

		</tbody>
		</table>
	
	<div class="btn_r">
       	<a href="<c:url value='/mng/usr/EgovStaffAddView.do'/>"><img src="${_IMG}/btn/btn_regist.gif" /></a>
	</div>

	<div id="paging">
		<c:url var="pageUrl" value="/mng/usr/EgovStaffManage.do${_BASE_PARAM}">
		</c:url>

		<c:if test="${not empty paginationInfo}">
			<ul>
				<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			</ul>
		</c:if>
	</div>
	</form:form>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	