<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<script>
$(document).ready(function(){
	//반 수량 저장
	$(document).on("click", ".class_set_btn", function(){
		var setClassTr      = 0;
		var classCnt        = parseInt($(".class_cnt").val());
		var html            = "";
		var forFirstCnt     = 1;
		var arrRemoveUserId = new Array();

		if(fn_class_check() == false) {
			return false;
		}

		//기존에 있는 tr 개수 설정
		$(".tr_student_list").each(function(){
			setClassTr++;
		});

		//설정 된 조개수와 수정할 조 개수가 같으면 false 처리
		if(setClassTr == classCnt) {
			return false;
		}

		//설정 된 조개수 보다 수정할 조 개수가 많으면 기존 학생을 유지하고 append 처리함
		if(setClassTr < classCnt) {
			forFirstCnt = setClassTr + 1;
		
		//설정 된 조개수 보다 수정할 조 개수가 적으면 기존 학생을 제거
		} else if(setClassTr > classCnt) {
			$(".tbl_student_pick tr").each(function(){
				if($(this).find(".searchUserNm").data("classcnt") > classCnt) {
					//tr 삭제
					$(this).remove();
				}
			});
			return;
		}
		
		//담당교수를 리스트에 첫 row에서 선택된 교수를 가져온다.
		var htmlSelectOption = $("select[name='manageIdList']:first").html();
		
		for(var i=forFirstCnt;i<=$(".class_cnt").val();i++) {
			html += "<tr class='tr_student_list'>";
			html += "	<td>" + i + "반</td>";
			html += "	<td class='td_manage'>";
			html += "		<input type='hidden' class='manageClassCntList' name='manageClassCntList' value='" + i + "' />";
			//html += "       <input type='hidden' class='manageIdList' name='manageIdList' value='" + managerId + "' />";
			html += "        <select name='manageIdList'>";
			html +=          htmlSelectOption;
			html += "        </select>";			
			html += "	</td>";
			html += "	<td><input type='text' class='searchUserNm' placeholder='학생 명을 입력.' data-classcnt='" + i + "' /></td>";
			html += "	<td class='td_student_list'></td>";
			html += "</tr>";
		}
		$(".tbl_student_pick tbody").append(html);
	});

	//학생검색
	$(document).on("focus", ".searchUserNm", function(){
		var option = fn_student_search_opion();
		$(this).autocomplete(option);
	});

	//반배정 확정
	$(document).on("click", ".class_complate_btn", function(){
		if(confirm("이미 조가 배정되어 있을 경우, 분반 확정을 하면 초기화가 됩니다.\n반을 수정하시겠습까?")) {
			var classCnt = $(".tr_student_list").length;
	        
	        if(classCnt <= 0) {
	            alert("최소 한개의 반이 필요합니다.");
	            return false;
	        }
	        
	        $("#listForm").submit();
        } else {
            return false;
        }
	});
	
	//학생 삭제
	$(document).on("click", ".student_delete_btn", function(){
		var userId = $(this).parent().data("userid");
		
		$(this).parent().remove();

		$(".tr_student").each(function(){
			if($(this).data("userid") == userId) {
				$(this).removeClass("pick_complate");
				$(this).data("pickflag", "false");
			}
		});
	});
});

//조 수량 체크 함수
function fn_class_check() {
	var classCnt = $(".class_cnt").val();
	var flag = true;
	
	if(classCnt =="" || classCnt == "0") {
		alert("반 수량을 설정해 주세요.")
		flag = false;
	}
	return flag;
}

//담당교수 검색 autocomplate option
function fn_student_search_opion() {
	var option = {
		source : function(request, response){
			$.ajax({
				type:"post",
		          	dataType:"json",
		          	url:"/mng/lms/crm/studentJson.json",
		          	data:{crclId : "${curriculumVO.crclId}", searchUserNm : $(this.element).val()},
		          	success:function(result){
		          		response($.map(result.items, function(item){
		          			var userName = "";

                            if(item.mngDeptNm == null) {
                                userName = item.userNm;
                            } else {
                                userName = item.userNm + "("+item.mngDeptNm+")";
                            }
                            return{
                            label:userName,
		       	            value:item.userId,
		       	            userNm:item.userNm
		            		}
		           		}));
		          	},
		          	error: function(){
		          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
		            }
			})
		},
	    matchContains:true,
	    selectFirst:false,
	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
	    delay:100,                 //milliseconds
	    select:function(event,ui){ //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
	    	var html        = "";
	    	var alreadyFlag = true;
	    	
	    	html += "<div class='div_student_box' data-userid='" + ui.item.value + "'>";
			html += "    <input type='hidden' name='userIdList' value='" + ui.item.value + "'>";
			html += "    <input type='hidden' name='classCntList' value='" + $(this).data("classcnt") + "'>";
			html += "    <span>" + ui.item.userNm + " | </span>";
			html += "    <a href='#none' class='student_delete_btn'>X</a>";
			html += "</div>";

	    	$(".div_student_box").each(function(){
	    		if($(this).data("userid") == ui.item.value){
	    			alert("이미 등록 된 학생 입니다.");
	    			alreadyFlag = false;

	    			return false;
	    		} 
	    	});
	    	
	    	if(alreadyFlag) {
	    		$(this).parent().next().append(html);
	    	}

	    	return false;
	    },
	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	};

	return option;
}
</script>
<table class="chart2 mb50">
	<tr>
		<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
	</tr>
	<tr>
		<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
		<td class="alC">
			<ul>
				<c:forEach var="user" items="${subUserList}" varStatus="status">
					<li>
						<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
					</li>
				</c:forEach>
			</ul>
		</td>
		<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
	</tr>
</table>

<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="4"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<c:import url="/mng/lms/curriculumStudenttabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="3"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<div>
	<div style="height:40px;">
		<c:choose>
			<c:when test="${classCnt eq 0 }"><c:set var="pickClassCnt" value=""/></c:when>
			<c:otherwise><c:set var="pickClassCnt" value="${classCnt }"/></c:otherwise>
		</c:choose>
		<input type="text" class="inp_s onlyNum class_cnt" id="inp_text" placeholder="반 수량" value="${pickClassCnt }">
		<a href="#none" class="btn_mngTxt class_set_btn">반 수량 저장</a>
	</div>
	<div class="student_pick_list">
		<form id="listForm" method="post" action="/mng/lms/crm/updateCurriculumClass.do">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>

			<table class="tbl_student_pick">
				<colgroup>
					<col width='80px' />
					<col width='200px' />
					<col width='200px' />
					<col width='*' />
				</colgroup>
				<tbody>
					<tr class="pick_table_header">
						<th>분반</th>
						<th>담당교수</th>
						<th>학생배정</th>
						<th>배정된 학생 명단</th>
					</tr>
					
					<c:forEach var="curriculumClass" items="${curriculumClassList}" varStatus="status">
						<tr class="tr_student_list">
							<td>${curriculumClass.classCnt }반</td>
							<td class='td_manage'>
								<input type="hidden" class="manageClassCntList" name="manageClassCntList" value="${curriculumClass.classCnt }" />
								<select name="manageIdList">
									<c:forEach var="fac" items="${facList}" varStatus="status">
										<option value="${fac.facId}" <c:if test="${curriculumClass.manageId eq fac.facId }">selected</c:if>>${fac.userNm}</option>
									</c:forEach>
								</select>
							</td>
							<td><input type="text" class="searchUserNm" placeholder="학생 명을 입력." data-classcnt="${curriculumClass.classCnt }" /></td>
							<td class="td_student_list">
								<c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status2">
									<c:if test="${pickStudent.classCnt eq curriculumClass.classCnt }">
										<div class="div_student_box" data-userid="${pickStudent.userId }">
											<input type="hidden" name="userIdList" value="${pickStudent.userId }">
											<input type="hidden" name="classCntList" value="${pickStudent.classCnt }">
											<span>${pickStudent.userNm } | </span>
											<a href="#none" class="student_delete_btn">X</a>
										</div>
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</div>

<div class="btn_c">
<c:choose>
	 	<c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
	 	</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4}">
			<a href="#none" class="btn_mngTxt class_complate_btn">반배정 확정</a>
		</c:when>
	</c:choose>

    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>