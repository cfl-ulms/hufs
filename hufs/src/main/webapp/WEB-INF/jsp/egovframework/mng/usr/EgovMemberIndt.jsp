<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="PROF_MANAGE"/>
	<c:param name="validator" value="userManageVO"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<c:set var="registerFlag" value="${empty userManageVO.userId ? '등록' : '수정'}"/>

<script type="text/javascript">

$(document).ready(function(){
	
});

<c:if test='${not empty message}'>
alert("${message}");
</c:if>
		function checkForm(form) {	
			/* if(!validateUserManageVO(form)) {				
				return false;
			} */
			if($('#userNm').val() == "" || $('#userNm').val() == null){
				alert("이름을 입력하세요.");
				$('#userNm').focus();
				return false;
			}else if($("input:radio[name=sexdstn]:checked").val() == "" || $("input:radio[name=sexdstn]:checked").val() == null){
				alert("성별을 선택하세요.");
				$('#sexdstn').focus();
				return false;
			}/* else if($('#groupCode').val() == "" || $('#groupCode').val() == null){
				alert("소속을 선택하세요.");
				$('#groupCode').focus();
				return false;
			} */
			else if($('#mngDeptCode').val() == "" || $('#mngDeptCode').val() == null){
				alert("주관기관을 선택하세요.");
				$('#mngDeptCode').focus();
				return false;
			}else if($('#major').val() == "" || $('#major').val() == null){
				alert("교육과정언어를 선택하세요.");
				$('#major').focus();
				return false;
			}else if($('#email1').val() == "" || $('#email1').val() == null){
				alert("이메일을 입력하세요.");
				$('#email1').focus();
				return false;
			}else if($('#email2').val() == "" || $('#email2').val() == null){
				alert("이메일 주소를 선택하세요.");
				$('#email2').focus();
				return false;
			}else{
				var regFlag = "${registerFlag}";

				if(regFlag == "수정"){
					var mail = $('#email1').data("id") + "@" + $('#email2').data("id");
					var txtMail = $('#email1').val() + "@" + $('#email2').val();
					
					if(mail == txtMail){
						if(confirm('<spring:message code="common.update.msg" />')) {
							//return true;
							form.submit();
						}else {
							return false;
						}
					}else{
						$.ajax({
							url : "/mng/usr/EgovMberEmailDuplChk.json",
							dataType : "json",
							type : "GET",
							data : {emailAdres : $('#email1').val()+'@'+$('#email2').val()},
							success : function(data){
								if(data.successYn == "N"){
									alert("중복된 이메일 계정입니다. 이메일 정보를 다시 확인해 주세요.");
									return false;
								}else{
									<c:choose>
										<c:when test="${registerFlag eq '수정'}">
											if(confirm('<spring:message code="common.update.msg" />')) {
												//return true;
												form.submit();
											}else {
												return false;
											}
										</c:when>
										<c:otherwise>
											if(confirm('<spring:message code="common.regist.msg" />')) {
												form.submit();
											}else {
												return false;
											}
										</c:otherwise>
									</c:choose>
								}	
							}, error : function(){
								alert("error");
							}
						});
						
						return false;
					}
				}else{
					$.ajax({
						url : "/mng/usr/EgovMberEmailDuplChk.json",
						dataType : "json",
						type : "GET",
						data : {emailAdres : $('#email1').val()+'@'+$('#email2').val()},
						success : function(data){
							if(data.successYn == "N"){
								alert("중복된 이메일 계정입니다. 이메일 정보를 다시 확인해 주세요.");
								return false;
							}else{
								<c:choose>
									<c:when test="${registerFlag eq '수정'}">
										if(confirm('<spring:message code="common.update.msg" />')) {
											//return true;
											form.submit();
										}else {
											return false;
										}
									</c:when>
									<c:otherwise>
										if(confirm('<spring:message code="common.regist.msg" />')) {
											form.submit();
										}else {
											return false;
										}
									</c:otherwise>
								</c:choose>
							}	
						}, error : function(){
							alert("error");
						}
					});
					
					return false;
				}
			}
			
		}
		
		
		
	    function fnIdRcomd(){
	    	var frm = document.userManageVO;
	        var url = "<c:url value='/uss/umt/cmm/EgovIdRecomendCnfirmView.do'/>";
	        window.open(url, 'RcomdIdCheck', 'menu=no, scrollbars=yes, width=420,height=300');
	    }
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}

		function fn_egov_return_IdCheck(userId) {
	    	var frm = document.userManageVO;
			frm.userId.value = userId;
	    }

		function fn_egov_return_RcomdCheck(rcomdId) {
	    	var frm = document.userManageVO;
			frm.recomendId.value = rcomdId;
	    }

		function inputDirectEmailDns(val){
		 	document.getElementById('email2').value = val;
		}

		function sendPassword() {
		    if(confirm("비밀번호를 재발급하고 "+document.userManageVO.moblphonNo.value+"번호로 전송 하시겠습니까?")) {
		        document.userManageVO.action = "${pageContext.request.contextPath}/mng/usr/SendPassword.do";
		        document.userManageVO.target = "passSand";
		        return true;
		    }else{
		    	return false;
			}
		}
		
		function fnIdCheck(){
	    	var frm = document.userManageVO;
	    	var userId = frm.userId.value;
	    	console.log("userId : "+userId);
	    	if(!fnCheckUserIdLength(userId) || !fnCheckSpace(userId) || !fnCheckNotKorean(userId) || !(fnCheckEnglish(userId) && fnCheckDigit(userId) && !fnCheckTksu(userId))){
		    	alert("아이디는 띄어쓰기 없는 영문+숫자 조합 8~50자 내에서 입력해야 합니다.");
		    	
		    	console.log("1 : "+(!fnCheckUserIdLength(userId)));
		    	console.log("2 : "+(!fnCheckSpace(userId)));
		    	console.log("3 : "+(!fnCheckNotKorean(userId)));
		    	console.log("4 : "+(fnCheckEnglish(userId)));
		    	console.log("5 : "+(fnCheckDigit(userId)));
		    	console.log("6 : "+(!fnCheckTksu(userId)));
		    	console.log("7 : "+(!(fnCheckEnglish(userId) && fnCheckDigit(userId) && !fnCheckTksu(userId))));
		    	console.log("8 : "+(!fnCheckUserIdLength(userId) || !fnCheckSpace(userId) || !fnCheckNotKorean(userId) || !(fnCheckEnglish(userId) && fnCheckDigit(userId) && !fnCheckTksu(userId))));
		    	
		    } else {
		        var url = "<c:url value='/uss/umt/cmm/EgovIdDplctCnfirm.do?checkId=" + encodeURIComponent(userId) + "'/>";
		        window.open(url, 'IdCheck', 'menu=no, scrollbars=yes, width=500,height=350');
		    }
	    }
		
		/* 문자열 길이 */
		function fnCheckUserIdLength(str) {
			
            if (str.length < 8 || str.length > 50 ){
                    return false;
            }
            return true;
        }
		
		/* 공백 체크 */
		function fnCheckSpace(str){
				for (var i=0; i < str .length; i++) {
				    ch_char = str .charAt(i);
				    ch = ch_char.charCodeAt();
				        if(ch == 32) {
				            return false;
				        }
				}
            	console.log("여기기기기기기기2222");
	    	    return true;
	    	}
		
		/* 한글 페크 */
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
           	console.log("여기기기기기기기3333");
    	    return true;
    	}
		
		/* 특수문자 체크 */
		function fnCheckTksu(str) { 
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    ch = ch_char.charCodeAt();
			        if( !((ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64) || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126)) ) {
			            
			        } else {
			           	console.log("여기기기기기기기4444");
			        	return true;
			        }
			}
            return false;
            
        }
        
		/* 영어체크 */
        function fnCheckEnglish(str){
       			console.log("영어체크 길이 : "+str.length);
       		for(var i=0;i<str.length;i++){
       			var EnglishChar = str.charCodeAt(i);
       			console.log("영어체크  : "+EnglishChar);
       			console.log("영어체크 2 : "+(0x61));
       			console.log("영어체크 i : "+i);
       			if( !( 0x61 <= EnglishChar && EnglishChar <= 0x7A ) && !( 0x41 <= EnglishChar && EnglishChar <= 0x5A ) ) {
       				console.log("걸리는 영단어 : "+EnglishChar);
       				
       			} else {
       				console.log("걸리지 않는 영단어 : "+EnglishChar);
		        	return true;
		        }
       		}
           	console.log("여기기기기기기기5555");
       		return false;
       }
       
       function fnCheckDigit(str) {  
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    iValue = parseInt(ch_char);
		        if(isNaN(iValue)) {
		           
		        } else {
		        	return true;
		        }
			}
           	console.log("여기기기기기기기6666");
            return false;
            
        }
		
	</script>
<div id="cntnts">
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<c:set var="actionUrl" value="EgovUserSelectIndt.do"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<c:set var="actionUrl" value="EgovUserSelectUpdt.do?targetId=${userManageVO.userId}"/>
			</c:when>
		</c:choose> 

	<form:form commandName="userManageVO" id="userManageVO" name="userManageVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/mng/usr/${actionUrl }"> 
	
	<form:hidden path="moblphonNo"/>
	<form:hidden path="userSeCode" value="08"/>
		
	<table class="chart2">
		<caption>회원관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
			<col width="300"/>
		</colgroup>
		<tbody>
		<tr>
			<th><em>*</em><label for="userNm"> 이름 </label><span style="color:red;">(필수)</span></th>
			<td>
				<form:input path="userNm" id="userNm" cssClass="inp" />
				
				<form:select path="workStatusCode">
					<c:forEach var="result" items="${workStatusList}" varStatus="status">
						<option value="${result.code}" <c:if test="${userManageVO.workStatusCode eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</form:select>
				<div><form:errors path="userNm" /></div>
			</td>
			<td rowspan="11" align="center">
				<c:choose>
					<c:when test="${empty userManageVO.photoStreFileNm}">사진없음</c:when>
					<c:otherwise><img src="${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>" style="max-width:300px"/></c:otherwise>
				</c:choose>
				<br><input type="file" name="userPicFile" class="inp" style="width:300px"/>
			</td>
		</tr>
		
		<tr>
			<th><em>*</em><label for="sexdstn"> 성별 </label><span style="color:red;">(필수)</span></th>
			<td>
				<form:radiobutton path="sexdstn" value="M" label="남자"/>
				<form:radiobutton path="sexdstn" value="F" label="여자"/>
	
				&nbsp;&nbsp;<form:checkbox path="confmAt" value="Y"/><abbr title="특수외국어진흥사업 LMS 사용권한을 부여하는 경우 체크하세요"> LMS 계정 사용</abbr>
			</td>
		</tr>
		
		<tr>
			<th><label for="userSchNo"> 사번 </label>(선택)</th>
			<td>
				<form:input path="userSchNo" id="userSchNo" cssClass="inp" maxlength="20"/>
			</td>
		</tr>
		
		<tr>
			<th><label for="positionCode"> 직위 </label>(선택)</th>
			<td>
			
			<label for="positionCode" class="hdn">직위</label>
		  	  	<form:select path="positionCode">
					<option value="">선택</option>
					<c:forEach var="result" items="${positionList}" varStatus="status">
						<option value="${result.code}" <c:if test="${userManageVO.positionCodeNm eq result.codeNm}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</form:select>
	  	  	</td>
		</tr>
		
		<tr>
			<th>
				<label for="groupCode"> 소속 </label>
				(선택)
				<!-- <span style="color:red;">(필수)</span> -->
			</th>
			<td>
			
			<label for="groupCode" class="hdn">소속</label>
		  	  	<form:select path="groupCode">
					<option value="">선택</option>
					<c:forEach var="result" items="${groupList}" varStatus="status">
						<option value="${result.codeNm}" <c:if test="${userManageVO.groupCode eq result.codeNm}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</form:select>
	  	  	</td>
		</tr>
		
		<tr>
			<th>
				<em>*</em>
				<label for="mngDeptCode"> 주관기관 </label>
				<span style="color:red;">(필수)</span>
			</th>
			<td>
			
			<label for="mngDeptCode" class="hdn">주관기관</label>
		  	  	<form:select path="mngDeptCode">
					<option value="">선택</option>
					<c:forEach var="result" items="${deptList}" varStatus="status">
						<c:if test="${result.ctgryLevel ne '0' }">
							<option value="${result.ctgryId}" <c:if test="${userManageVO.mngDeptCodeNm eq result.ctgryNm}">selected="selected"</c:if>>${result.ctgryNm }</option>
						</c:if>
					</c:forEach>
				</form:select>
	  	  	</td>
		</tr>
		
		<tr>
			<th>
				<em>*</em>
				<label for="major"> 교육과정 언어 </label><br/>
				<span style="color:red;">(필수)</span>
			</th>
			<td>
			
			<label for="major" class="hdn">교육과정 언어</label>
		  	  	<form:select path="major">
					<option value="">선택</option>
					<c:forEach var="result" items="${langList}" varStatus="status">
						<c:if test="${result.ctgryLevel ne '0' }">
							<option value="${result.ctgryId}" <c:if test="${userManageVO.majorNm eq result.ctgryNm}">selected="selected"</c:if>>${result.ctgryNm }</option>
						</c:if>
					</c:forEach>
				</form:select>
	  	  	</td>
		</tr>
		
		<tr>
			<th><label for="moblphonNo">연락처</label> (선택)</th>
			<td>
				<c:set var="moblArr" value="${fn:split(userManageVO.moblphonNo, '-')}"/>
				<c:forEach items="${moblArr}" var="arr" varStatus="status">
					<c:if test="${status.count eq 1}"><c:set var="phone1" value="${fn:trim(arr)}"/></c:if>
					<c:if test="${status.count eq 2}"><c:set var="phone2" value="${fn:trim(arr)}"/></c:if>
					<c:if test="${status.count eq 3}"><c:set var="phone3" value="${fn:trim(arr)}"/></c:if>
				</c:forEach>
				<select id="phone1" name="phone1" title="휴대전화번호 선택">
					<option value="010" <c:if test="${phone1 eq '010'}"> selected="selected"</c:if>>010</option>
					<option value="011" <c:if test="${phone1 eq '011'}"> selected="selected"</c:if>>011</option>
					<option value="016" <c:if test="${phone1 eq '016'}"> selected="selected"</c:if>>016</option>
					<option value="017" <c:if test="${phone1 eq '017'}"> selected="selected"</c:if>>017</option>
					<option value="018" <c:if test="${phone1 eq '018'}"> selected="selected"</c:if>>018</option>
					<option value="019" <c:if test="${phone1 eq '019'}"> selected="selected"</c:if>>019</option>
				</select>
				<input type="text" id="phone2" name="phone2" value="${phone2}" maxlength="4" class="inp tel" title="휴대전화번호 가운데자리 입력" />
				<input type="text" id="phone3" name="phone3" value="${phone3}" maxlength="4" class="inp tel" title="휴대전화번호 뒷자리 입력" />
			</td>
		</tr>
		<tr>
			<th><em>*</em><label for="emailHead"> E-mail </label><span style="color:red;">(필수)</span></th>
			<td>
			<c:set var="emailArr" value="${fn:split(userManageVO.emailAdres, '@')}"/>
				<c:forEach items="${emailArr}" var="arr" varStatus="status">
					<c:if test="${status.count eq 1}">
						<c:set var="emailHead" value="${fn:trim(arr)}"/>
					</c:if>
					<c:if test="${status.count eq 2}">
						<c:set var="emailBody" value="${fn:trim(arr)}"/>
					</c:if>
				</c:forEach>
				<input type="text" name="email1" id="email1" value="${emailHead}" class="inp" data-id="${emailHead}" /> @ <input type="text" name="email2" value="${emailBody}" id="email2" class="inp" data-id="${emailBody}" />
				<select id="email_choice" name="email_choice" onchange='inputDirectEmailDns(this.value);'>
						<option value="">직접입력</option>
						<option value="hanmail.net"	<c:if test="${emailBody eq 'hanmail.net'}"> selected="selected"</c:if>>다음</option>
						<option value="naver.com"	<c:if test="${emailBody eq 'naver.com'}"> selected="selected"</c:if>>네이버(naver.com)</option>
						<option value="nate.com"	<c:if test="${emailBody eq 'nate.com'}"> selected="selected"</c:if>>네이트(nate.com)</option>
						<option value="gmail.com"	<c:if test="${emailBody eq 'gmail.com'}"> selected="selected"</c:if>>G메일(gmail.com)</option>
				</select>
				
			</td>
		</tr>
		
		<tr>
			<th><label for="userId"> 아이디</label> (선택)</th>
			<td>
			<form:input path="userId" id="userId" cssClass="inp" maxlength="100"/> 
			<a href="javascript:void(0);" onclick="fnIdCheck();return false;" class="btn_s"><span>중복확인</span></a>
			<p>등록하지 않을 시 E-mail 정보로 자동 등록됩니다.</p>
			</td>
		</tr>
		
		<tr>
			<th><label for="password"> 패스워드</label> (자동)</th>
			<td>등록 시 자동으로 1234로 지정됩니다.</td>
		</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="./EgovMberManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<%-- <a href="/mng/usr/EgovMberAddView.do"><img src="${_IMG}/btn/btn_regist.gif" alt="등록하기" /></a> --%>
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<input type="image" src="${MNG_IMG}/btn/btn_regist.gif" alt="등록하기" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<input type="image" src="<c:url value='${MNG_IMG}/btn/btn_modify.gif'/>" alt="수정" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
		</c:choose>

		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소"/></a>
						
	</div>
</form:form>
<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	