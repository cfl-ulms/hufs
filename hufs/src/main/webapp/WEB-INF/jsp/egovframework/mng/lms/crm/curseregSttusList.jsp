<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclNm" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchCrclNm" value="${searchVO.searchApplyStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchCrclNm" value="${searchVO.searchApplyEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclNm" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchCrclNm" value="${searchVO.searchHostCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG_LIST"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청 승인/취소 내역"/>
</c:import>

<script>
//달력
$(function() {
    $("#searchApplyStartDate, #searchApplyEndDate").datepicker({
        dateFormat: "yy-mm-dd"
    });
});

$(document).ready(function(){
});

//검색 초기화
function fnReset(){
	$("#bbs_search").find("input").not("#resetBtn").val("");
	$("#bbs_search select").find("option:first").attr('selected', 'selected');
}
</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crm/curseregSttusList.do"/>">
	  		<select id="searchCrclYear" name="searchCrclYear">
	  			<option value="">선택</option>
	  			<c:forEach var="result" items="${yearList}" varStatus="status">
	  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
	  			</c:forEach>
			</select>
			
			<select id="searchCrclTerm" name="searchCrclTerm">
				<option value="">학기 전체</option>
				<c:forEach var="result" items="${crclTermList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<input type="text" name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" id="searchApplyStartDate" class="btn_calendar" placeholder="수강신청시작기간" readonly="readonly"/> ~ 
			<input type="text" name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" id="searchApplyEndDate" class="btn_calendar" placeholder="수강신청종료기간" readonly="readonly"/>
			<br/>

			<label><input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요."/></label>

			<select id="control" name="searchCrclLang">
				<option value="">언어 전체</option>
				<c:forEach var="result" items="${langList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclLang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<select id="control" name="searchHostCode">
				<option value="">주관기관 전체</option>
				<c:forEach var="result" items="${insList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchHostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;">
		</form>
	</div>
	
	<table class="chart_board">
	    <colgroup>
	    	<col width="50px"/>
			<col width="80px"/>
			<col width="80px"/>
			<col width="130px"/>
			<col width="160px"/>
			<col width="80px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="50px"/>
			<col width="50px"/>
			<col width="50px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>년도</th>
	      	<th>학기</th>
	      	<th>과정상태</th>
	      	<th>과정명</th>
	      	<th>언어</th>
	      	<th>주관기관</th>
	      	<th>책임, 강의책임교원</th>
	      	<th>수강신청기간</th>
	      	<th>수업료</th>
	      	<th>등록비</th>
	      	<th>신청승인</th>
	      	<th>취소/환불</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:url var="viewUrl" value="/mng/lms/crm/curseregSttusView.do?${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:param name="crclbId" value="${result.crclbId}"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
	    		<tr onclick="location.href='${viewUrl}'">
	    			<td><c:out value="${result.crclYear}"/></td>
	    			<td><c:out value="${result.crclTermNm}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.processSttusCodeDate eq '1' }">대기</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '3' }">접수중</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '4' }">종료</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '5' }">과정개설취소</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '6' }">수강대상자 확정</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '7' }">과정 중</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '8' }">과정종료</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '9' }">과정종료(성적산정)</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '10' }">과정종료(성적발표)</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '11' }">성적발표 종료</c:when>
	    					<c:when test="${result.processSttusCodeDate eq '12' }">최종종료(확정)</c:when>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.crclNm}"/></td>
	    			<td><c:out value="${result.crclLangNm}"/></td>
	    			<td><c:out value="${result.hostCodeNm}"/></td>
	    			<td><c:out value="${result.userNm}"/></td>
	    			<td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.tuitionFees eq 0 }">무료</c:when>
	    					<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.tuitionFees}" />원</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.registrationFees eq 0 }">없음</c:when>
	    					<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.registrationFees}" />원</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.acceptStudentCnt}"/></td>
	    			<td><c:out value="${result.cancleStudentCnt}"/></td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="12"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    
    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crm/curseregSttusList.do?${_BASE_PARAM}">
	    </c:url>
	
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>