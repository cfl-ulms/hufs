<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${param.crclId }"/>
   <c:param name="bbsId" value="${param.bbsId }"/>
   <c:param name="frstRegisterId" value="${param.frstRegisterId }"/>
</c:url>
<% /*URL 정의*/ %>


<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<script>
</script>
</head>
<body>
<br/>
<div id="cntnts">
	<div style="text-align:center;width:100%;height:100px;">
		<h1 style="font-size:20px;text-align:center;">${masterVo.sysTyCode eq 'CLASS' ? '(반 별)' : masterVo.sysTyCode eq 'GROUP' ? '(조 별)' : ''} ${masterVo.bbsNm} 게시판 통계</h1>

	</div>
	<div>
		<c:if test="${not empty attendCollect and  paginationInfo.totalRecordCount ge attendCollect}">
		<div style="float:left;border:1px solid blue; width:100px;font-size:14pt;text-align:center;height:20px;">
			${paginationInfo.totalRecordCount ge attendCollect ? 'PASS' : 'FAIL'}
		</div>
		</c:if>
		<div style="float:right;">
			<h3>${resultList[0].department } ${resultList[0].userNm }</h3> <br/>
			<p>게시물 총 ${paginationInfo.totalRecordCount } 건 <c:if test="${not empty attendCollect}"> / ${attendCollect } 이상 Pass </c:if>

		</div>
	</div>
	<table class="chart_board" >
				<colgroup>
					<col width="65px" />
					<col width="70px" />
					<col width="*" />
					<col width="85px" />
					<col width="85px" />
				</colgroup>
				<thead>
					<tr>
						<th><spring:message code="cop.nttNo"/></th>
						<th>글 구분</th>
						<th>제목</th>
						<th><spring:message code="cop.listAtchFile" /></th>
						<th class="last">등록일</th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
					<c:when test="${not empty resultList }">
						<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="/mng/lms/manage/lmsMemBbsView.do">
							  	<c:param name="nttNo" value="${result.nttNo}" />
							  	<c:param name="crclId" value="${param.crclId}" />
							  	<c:param name="bbsId" value="${result.bbsId}" />
						    </c:url>
							<tr>
								<td>${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}</td>
								<td>${result.ctgryNm}</td>
								<td>${result.nttSj }</td>
								<c:choose>
						          <c:when test="${not empty result.atchFileId}">
						          	<td><img src="${_C_IMG}/sub/board/icon_file.gif" alt="첨부파일" /></td>
						          </c:when>
						          <c:otherwise>
						           	<td>-</td>
						          </c:otherwise>
						        </c:choose>
							  	<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
						</c:forEach>
					</c:when>
					<c:otherwise>
						 <tr>
					        <td colspan="5">해당 자료가 없습니다.</td>
					      </tr>
					</c:otherwise>
				</c:choose>
				</tbody>
			</table>
		<c:if test="${not empty resultList }">
			<div id="paging">
				<c:url var="pageUrl" value="/mng/lms/manage/selectLmsBbsList.do${_BASE_PARAM}">
				</c:url>
			    <ul>
			      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
			    </ul>
			</div>
		</c:if>
</div>