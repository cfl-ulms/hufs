<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>게시판 선택</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = $(".chk:checked").val(),
			name = $(".chk:checked").parents("tr").find(".box_bbsnm").text(),
			colect = $(".chk:checked").parents("tr").find(".colect").val(),
			size = $(".chk:checked").length;
		
		if(size == 0){
			alert("게시판을 선택해주세요.");
		}else if(!colect || colect == 0){
			alert("횟 수를 확인해주세요.");
		}else{
			name = colect + "회 - " + name
			
			window.opener.selBbs(id, name, colect);
			window.close();
		}
		return false;
	});
	
	
	$(".btn_cancel").click(function(){
		window.close();
	});
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
<form name="listForm" id="listForm" method="post" method="post" action="">
	<p>
		게시판 수에 따라 100점 환산 점수로 자동 표기됩니다.<br/>
		(단, 책임, 강의책임교원 필요시 수동으로 점수 수정도 가능합니다 과정관리 > 성적 메뉴에서 점수 수정도 가능.)<br/><br/>
		예시) 게시판 3개를 선택한 경우, Pass 기준으로 학생이 글쓰기 완료 시 각 게시판 1 점씩  3점 만점을 획득하고, 100점 환산점 계산으로 수업참여도는 100점으로 표기됩니다.
	</p>
	<br/>
	<table class="chart_board">
		<colgroup>
			<col width="10%"/>
			<col width="*"/>
			<col width="40%"/>
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>게시판 명</th>
				<th>Pass 기준</th>
		  	</tr> 
		</thead>
		<tbody class="box_bbs">
			<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
				<tr class="list">
					<td>
						<input type="radio" class="chk" name="bbsIdList" value="${result.bbsId}"/>
					</td>
					<td class="box_bbsnm">
						(
						<c:choose>
							<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
							<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
							<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
						</c:choose>
						)
						<c:out value="${result.bbsNm}"/>
					</td>
					<td>
						<input type="number" value="0" class="onlyNum wid80p colect" name="colect"/>회
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
    <div class="btn_c">
		<a href="#" class="btn_ok btn_mngTxt">선택</a>&nbsp;
		<a href="#" class="btn_cancel btn_mngTxt">취소</a>
	</div>
</form>
</div>