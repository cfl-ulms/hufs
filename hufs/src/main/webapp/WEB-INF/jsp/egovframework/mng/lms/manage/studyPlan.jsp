<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
<link rel="stylesheet" href="${CML}/css/common/modal_staff.css?v=2">

<script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
<script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>

<script src="${CML}/js/common.js?v=1"></script>

<script>
$(document).ready(function(){
	//출석
	$(".atd").click(function(){
		var id = $(this).data("id"),
			sbj = $(this).data("nm"),
			crclId = $(this).data("crclid"),
			attTCnt = $(this).data("tot"),
			attYCnt = $(this).data("ycnt"),
			attLCnt = $(this).data("lcnt"),
			attNCnt = $(this).data("ncnt");
		
		$.ajax({
			url : "/lms/atd/selectAttendList.do"
			, type : "post"
			, dataType : "html"
			, data : {plId : id, crclId : crclId, modalAt : "Y"}
			, success : function(data){
				$("#sbj").text(sbj);
				$("#plId").val(id);
				$("#atdlist").html(data);
				
				$("#goAtd").attr("href", "/mng/lms/atd/selectAttendList.do?menu=CLASS_MANAGE&crclId=" + crclId + "&plId=" + id + "&depth1=CURRICULUM_STUDY");
			}, error : function(){
				alert("error");
			}
		});
		
		return false;
	});
});

function search(){
	var searchFacNm = $("#inp_text").val(),
		resultCnt = 0;
	
	if(searchFacNm){
		$(".schTr").hide();
		$(".facNm").each(function(){
			if($(this).val() == searchFacNm){
				$(this).parents(".schTr").show();
				resultCnt++;
			}
		});
		
		if(resultCnt == 0){
			$(".empty").show();
		}else{
			$(".empty").hide();
		}
		
	}else{
		$(".schTr").show();
	}
	
	return false;
}

//출석 검색
function searchAtd(){
	var params = $("#atdFrom").serialize();
	
	$.ajax({
		url : "/lms/atd/selectAttendList.do"
		, type : "post"
		, dataType : "html"
		, data : params
		, success : function(data){
			$("#atdlist").html(data);
		}, error : function(){
			alert("error");
		}
	});
	
	return false;
}
</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm}"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="5"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<br/>
	
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crcl/CurriculumList.do"/>" onsubmit="return search();">
			<input type="text" name="searchFacNm" value="${searchVO.searchFacNm}" class="inp_s" id="inp_text" placeholder="교원명 검색"/></label>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	
	<form class="scheduleForm" name="schedule">
	<table class="chart2 mt50">
		<colgroup>
			<col width="5%"/>
			<col width="10%"/>
			<col width="5%"/>
			<col width="*"/>
			<col width="10%"/>
			<col width="8%"/>
			<col width="10%"/>
			<col width="15%"/>
		</colgroup>
		<tbody class="box_timetable">
			<tr>
			  	<th class="alC">차시</th>
			  	<th class="alC">수업일</th>
			 	<th class="alC">시수</th>
				<th class="alC">수업주제</th>
				<th class="alC">교원명</th>
				<th class="alC">수업자료</th>
				<th class="alC">수업방법</th>
				<th class="alC">출석</th>
		  	</tr> 
		  	<c:choose>
		  		<c:when test="${curriculumVO.totalTime > 0}">
		  			<c:forEach var="scheduleMngVO" items="${resultList}" varStatus="status">
			  			<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
						<tr class="schTr">
							<td class="alC line"><c:out value="${status.count}"/></td>
							<td class="alC line">
								<c:out value="${scheduleMngVO.startDt}"/>
								<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
							</td>
							<td class="alC line"><c:out value="${scheduleMngVO.sisu}"/></td>
							<td class="alL line">
								<c:url var="viewUrl" value="/mng/lms/manage/studyPlanView.do${_BASE_PARAM}">
									<c:param name="plId" value="${scheduleMngVO.plId}" />
								</c:url>
								<a href="${viewUrl}"><c:out value="${scheduleMngVO.studySubject}"/></a>
							</td>
							<td class="alC line">
								<ul>
									<c:forEach var="result" items="${facPlList}" varStatus="status">
										<c:if test="${result.plId eq scheduleMngVO.plId}">
											<li>
												<span>
													<c:out value="${result.userNm}"/>
													<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
													<input type="hidden" class="facNm" value="${result.userNm}"/>
												</span> 
											</li>
										</c:if>
									</c:forEach>
								</ul>
							</td>
							<td class="alC line box_period">
								<c:choose>
									<c:when test="${not empty scheduleMngVO.atchFileId}"><img src="/template/manage/images/ico_file.gif" alt="파일 다운로드"/></c:when>
									<c:otherwise>-</c:otherwise>
								</c:choose>
							</td>
							<td class="alC line">
								<c:if test="${not empty scheduleMngVO.courseId}">
									<c:choose>
						  				<c:when test="${scheduleMngVO.spType ne 'N' }">
						  					강의식
						  				</c:when>
						  				<c:otherwise>
						  					비강의식
						  				</c:otherwise>
						  			</c:choose>
						  			- <c:out value="${scheduleMngVO.courseNm}"/>
					  			</c:if>
							</td>
							<td class="alC line">
								<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
								<c:choose>
									<c:when test="${today >= startDt}">
										<c:choose>
				                      		<c:when test="${not empty scheduleMngVO.attTCnt and scheduleMngVO.attTCnt == scheduleMngVO.attYCnt}">전원출석</c:when>
				                      		<c:otherwise>
				                      			<a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${scheduleMngVO.plId}" data-nm="${scheduleMngVO.studySubject}" data-crclid="${curriculumVO.crclId}" data-lcnt="${scheduleMngVO.attLCnt}" data-ncnt="${scheduleMngVO.attNCnt}" data-tot="${scheduleMngVO.attTCnt}" data-ycnt="${scheduleMngVO.attYCnt}">
					                      			<c:if test="${scheduleMngVO.attLCnt > 0}">지각 <c:out value="${scheduleMngVO.attLCnt}"/></c:if>
					                      			<c:if test="${scheduleMngVO.attLCnt > 0 and scheduleMngVO.attNCnt > 0}">,</c:if>
					                      			<c:if test="${scheduleMngVO.attNCnt > 0}">결석 <c:out value="${scheduleMngVO.attNCnt}"/></c:if>
				                      			</a>
				                      		</c:otherwise>
				                      	</c:choose>
									</c:when>
									<c:otherwise>
										대기
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
					<tr class="empty" style="display:none;">
						<td colspan="8" class="alC">검색 결과가 없습니다.</td>
					</tr>
		  		</c:when>
		  		<c:when test="${curriculumVO.totalTime > 0 and today < curriculumStartDate}">
		  			<tr>
		  				<td colspan="8" class="alC">과정 기간이 아닙니다.</td>
		  			</tr>
		  		</c:when>
		  		<c:otherwise>
		  			<tr>
		  				<td colspan="8" class="alC">시수 없음</td>
		  			</tr>
		  		</c:otherwise>
		  	</c:choose>
		</tbody>
	</table>
	</form>
	
<div id="attend_view_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">출석확인</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">[<span id="sbj"></span>]</div>
        <form name="atdFrom" id="atdFrom" onsubmit="return searchAtd();">
        	<input type="hidden" name="crclId" value="${curriculumVO.crclId }"/>
        	<input id="plId" type="hidden" name="plId" value=""/>
        	<input id="modalAt" type="hidden" name="modalAt" value="Y"/>
            <select name="attentionType" style="height:40px;">
                  <option value="">전체</option>
                  <option value="Y">출석</option>
                  <option value="NY">지각 및 결석자</option>
            </select>
            <input type="text" name="userNm" value="" placeholder="이름을 입력해 주세요." style="height:35px;">
            <button class="btn_mngTxt" style="height:40px;">검색</button>
        </form>
        <div style="display:block;width:100%;height:300px;overflow-y:auto;">
	        <table class="modal-table-wrap size-sm center-align">
	          <colgroup>
	            <col width="10%">
	            <col width="30%">
	            <col width="*">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray">
	              <th class="font-700">No</th>
	              <th class="font-700">상태</th>
	              <th class="font-700">이름</th>
	            </tr>
	          </thead>
	          <tbody id="atdlist"></tbody>
	        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn_mngTxt btnModalCancel">닫기</button>
        <a href="#" id="goAtd" class="btn_mngTxt btnModalConfirm" target="_blank">출석 수정</a>
      </div>
    </div>
  </div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>