<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="searchCrclNm" value="${searchVO.searchCrclNm	}" />
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplySttusCode}"><c:param name="searchApplySttusCode" value="${searchVO.searchApplySttusCode}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchHostCode" value="${searchVO.searchHostCode}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchPlanStartDateBeforeAt}"><c:param name="searchPlanStartDateBeforeAt" value="${searchVO.searchPlanStartDateBeforeAt }" /></c:if>
	<c:if test="${not empty searchVO.searchManageNm}"><c:param name="searchManageNm" value="${searchVO.searchManageNm }" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<script>
//달력
$(function() {
  $("#searchStartDate, #searchEndDate, #searchApplyStartDate, #searchApplyEndDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
});
</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crm/curseregManage.do"/>">
	  		<select id="searchCrclYear" name="searchCrclYear">
	  			<option value="">선택</option>
	  			<c:forEach var="result" items="${yearList}" varStatus="status">
	  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
	  			</c:forEach>
			</select>
			
			<select id="searchCrclTerm" name="searchCrclTerm">
				<option value="">학기 전체</option>
				<c:forEach var="result" items="${crclTermList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~ 
			<input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<br/>
			<input type="text" name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" id="searchApplyStartDate" class="btn_calendar" placeholder="수강신청시작기간" readonly="readonly"/> ~ 
			<input type="text" name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" id="searchApplyEndDate" class="btn_calendar" placeholder="수강신청종료기간" readonly="readonly"/>

			<label><input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요."/></label>

			<select id="searchApplySttusCode" name="searchApplySttusCode">
				<option value="">신청현황 전체</option>
				<option <c:if test="${param.searchApplySttusCode eq  1}">selected</c:if> value="1">대기</option>
				<option <c:if test="${param.searchApplySttusCode eq  3}">selected</c:if> value="3">접수중</option>
				<option <c:if test="${param.searchApplySttusCode eq  4}">selected</c:if> value="4">종료</option>
				<option <c:if test="${param.searchApplySttusCode eq  5}">selected</c:if> value="5">과정개설취소</option>
			</select>
			<br/>
			<select id="control" name="searchHostCode">
				<option value="">주관기관 전체</option>
				<c:forEach var="result" items="${insList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchHostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<select id="control" name="searchCrclLang">
				<option value="">언어 전체</option>
				<c:forEach var="result" items="${langList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclLang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>

			<label><input type="text" name="searchManageNm" value="${searchVO.searchManageNm}" class="inp_s" id="inp_text" placeholder="교수명을 입력해주세요."/></label>

			<label><input type="checkbox" name="searchPlanStartDateBeforeAt" value="Y" id="searchPlanStartDateBeforeAt" <c:if test="${searchVO.searchPlanStartDateBeforeAt eq 'Y'}">checked</c:if> /> <strong>과정계획 대기 </strong></label>
			
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	
	<div style="padding-bottom:10px;">
		<a href="/mng/lms/crm/curseregManageCancelRule.do" class="btn_mngTxt">환불처리 기준 안내</a>
	</div>
	
	<table class="chart_board">
	    <colgroup>
	    	<col width="50px"/>
			<col width="80px"/>
			<col width="80px"/>
			<col width="130px"/>
			<col width="160px"/>
			<col width="80px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="50px"/>
			<col width="50px"/>
			<col width="50px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>번호</th>
	      	<th>년도</th>
	      	<th>학기</th>
	      	<th>과정상태</th>
	      	<th>과정명</th>
	      	<th>언어</th>
	      	<th>주관기관</th>
	      	<th>책임, 강의책임교원</th>
	      	<th>과정계획서</th>
	      	<th>과정기간</th>
	      	<th>수강신청기간</th>
	      	<th>수강정원</th>
	      	<th>신청현황</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:url var="viewUrl" value="/mng/lms/crm/curriculumManage.do?${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:param name="crclbId" value="${result.crclbId}"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
	    		<tr onclick="location.href='${viewUrl}'">
	    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	    			<td><c:out value="${result.crclYear}"/></td>
	    			<td><c:out value="${result.crclTermNm}"/></td>
	    			<td>
	    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
	    					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
						</c:forEach>
	    			</td>
	    			<td><c:out value="${result.crclNm}"/></td>
	    			<td><c:out value="${result.crclLangNm}"/></td>
	    			<td><c:out value="${result.hostCodeNm}"/></td>
	    			<td><c:out value="${result.userNm}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.planSttusCode eq 1 }">대기</c:when>
	    					<c:when test="${result.planSttusCode eq 2 }">진행중</c:when>
	    					<c:when test="${result.planSttusCode eq 3 }">완료</c:when>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	    			<td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
	    			<td>
	    				<c:choose>
							<c:when test="${result.applyMaxCnt eq '0' or empty result.applyMaxCnt}">제한없음</c:when>
							<c:otherwise>
								<c:out value="${result.applyMaxCnt}"/>명
							</c:otherwise>
						</c:choose>
	    			</td>
	    			<td>
						<c:choose>
	    					<c:when test="${result.applySttusCode eq '0' }">
	    						과정계획 미등록<br />
	    						<span class="ready">대기</span>
	    					</c:when>
	    					<c:when test="${result.applySttusCode eq '1' }">
                                <span class="ready">대기</span>
                            </c:when>
	    					<c:when test="${result.applySttusCode eq '3' }">
	    						<c:if test="${result.applyMaxCnt ne 0 }"><c:out value="${result.acceptStudentCnt}"/>/<c:out value="${result.applyMaxCnt}"/></c:if>
	    						<c:if test="${result.applyMaxCnt eq 0 }"><c:out value="${result.acceptStudentCnt}"/></c:if>
	    						<br />
	    						<span class="ing">접수중</span>
	    					</c:when>
	    					<c:when test="${result.applySttusCode eq '4' }"><span class="ready">종료</span></c:when>
	    					<c:when test="${result.applySttusCode eq '5' }"><span class="ready">과정개설취소</span></c:when>
	    					<c:otherwise><span class="ready">종료</span></c:otherwise>
	    				</c:choose>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="13"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    
    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}">
	    </c:url>
	
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>        

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>