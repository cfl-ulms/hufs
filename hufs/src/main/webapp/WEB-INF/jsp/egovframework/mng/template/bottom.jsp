<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
			</div>
		</div>
		<!-- rightPage -->
	</div>
	<!-- container -->

	<div id="footer">
		<div class="copy">
			<span>Copyright ⓒ <c:out value="${curYear}"/> by <strong><c:out value="${siteInfo.siteNm}"/></strong> All Rights Reserved. </span>
		</div>
	</div>

</div> 
<!-- wrap end -->

</body>
</html>