<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="schdulId" value="${surveyVO.schdulId}"/>
    <c:param name="schdulClCode" value="${surveyVO.schdulClCode}"/>
    <c:param name="crclId" value="${surveyVO.crclId}"/>
    <c:param name="plId" value="${surveyVO.plId}"/>
</c:url>
<% /*URL 정의*/ %>

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>만족도 제출현황</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		window.close();
		return false;
	});
});
</script>
</head>
<body>
<div id="navi">
	<h2 class="naviTit">
		<a href="/mng/lms/manage/curriculumSurveyMember.do${_BASE_PARAM}">미제출자 <c:out value="${surveyMemberSummary.submitNCnt}"/>명</a> | 
		<a href="/mng/lms/manage/curriculumSurveyMember.do${_BASE_PARAM}&searchSubmitAt=Y">제출자  <c:out value="${surveyMemberSummary.submitYCnt}"/>명</a>
	</h2>
</div>
<br/>
<div id="cntnts">
	<table class="chart_board">
		<colgroup>
			<col width="10%"/>
			<col width="*"/>
		</colgroup>
		<thead>
			<tr>
				<th>No</th>
				<th>성명</th>
		  	</tr>
		</thead>
		<tbody class="box_bbs">
			<c:forEach var="result" items="${memberList}" varStatus="status">
				<tr>
					<td>${status.count }</td>
					<td>${result.userNm }
				</tr>
			</c:forEach>
		</tbody>
	</table>

    <div class="btn_c">
		<a href="#" class="btn_ok btn_mngTxt">확인</a>&nbsp;
	</div>
</div>