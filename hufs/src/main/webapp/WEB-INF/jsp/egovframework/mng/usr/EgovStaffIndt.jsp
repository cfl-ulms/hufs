<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="STAF_MANAGE"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<c:set var="registerFlag" value="${empty userManageVO.userId ? '등록' : '수정'}"/>

<script type="text/javascript">

$(document).ready(function(){
	
});

<c:if test='${not empty message}'>
alert("${message}");
</c:if>
		function checkForm(form) {	
		
			if($('#userNm').val() == "" || $('#userNm').val() == null){
				alert("이름을 입력하세요.");
				$('#userNm').focus();
				return false;
			}else if($('#email1').val() == "" || $('#email1').val() == null){
				alert("이메일을 입력하세요.");
				$('#email1').focus();
				return false;
			}else if($('#email_choice').val() == "" || $('#email_choice').val() == null){
				alert("이메일 주소를 선택하세요.");
				$('#email_choice').focus();
				return false;
			}else{
				<c:choose>
					<c:when test="${registerFlag eq '수정'}">
						if(confirm('<spring:message code="common.update.msg" />')) {
					</c:when>
					<c:otherwise>
						if(confirm('<spring:message code="common.regist.msg" />')) {
					</c:otherwise>
				</c:choose>
					return true;
				}else {
					return false;
				}
			}
		
			
		}
			
	    function fnIdRcomd(){
	    	var frm = document.userManageVO;
	        var url = "<c:url value='/uss/umt/cmm/EgovIdRecomendCnfirmView.do'/>";
	        window.open(url, 'RcomdIdCheck', 'menu=no, scrollbars=yes, width=420,height=300');
	    }
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}

		function fn_egov_return_IdCheck(userId) {
	    	var frm = document.userManageVO;
			frm.userId.value = userId;
	    }

		function fn_egov_return_RcomdCheck(rcomdId) {
	    	var frm = document.userManageVO;
			frm.recomendId.value = rcomdId;
	    }

		function inputDirectEmailDns(val){
		 	document.getElementById('email2').value = val;
		}

		function sendPassword() {
		    if(confirm("비밀번호를 재발급하고 "+document.userManageVO.moblphonNo.value+"번호로 전송 하시겠습니까?")) {
		        document.userManageVO.action = "${pageContext.request.contextPath}/mng/usr/SendPassword.do";
		        document.userManageVO.target = "passSand";
		        return true;
		    }else{
		    	return false;
			}
		}
		
		function fnIdCheck(){
	    	var frm = document.userManageVO;
	    	var userId = frm.userId.value;
	    	if(!fnCheckUserIdLength(userId) || !fnCheckSpace(userId) || !fnCheckNotKorean(userId) || !(fnCheckEnglish(userId) && fnCheckDigit(userId) && !fnCheckTksu(userId))){
		    	alert("아이디는 띄어쓰기 없는 영문+숫자 조합 8~20자 내에서 입력해야 합니다.");
		    } else {
		        var url = "<c:url value='/uss/umt/cmm/EgovIdDplctCnfirm.do?checkId=" + encodeURIComponent(userId) + "'/>";
		        window.open(url, 'IdCheck', 'menu=no, scrollbars=yes, width=500,height=350');
		    }
	    }
		
		function fnCheckUserIdLength(str) {
            if (str.length < 8 || str.length > 20 ){
                    return false;
            }
            return true;
        }
		
		function fnCheckSpace(str){
				for (var i=0; i < str .length; i++) {
				    ch_char = str .charAt(i);
				    ch = ch_char.charCodeAt();
				        if(ch == 32) {
				            return false;
				        }
				}
	    	    return true;
	    	}
		
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}
		
		function fnCheckTksu(str) { 
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    ch = ch_char.charCodeAt();
			        if( !(ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64) || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126) ) {
			            
			        } else {
			        	return true;
			        }
			}
            return false;
            
        }
        
        function fnCheckEnglish(str){
       		for(var i=0;i<str.length;i++){
       			var EnglishChar = str.charCodeAt(i);
       			if( !( 0x61 <= EnglishChar && EnglishChar <= 0x7A ) && !( 0x41 <= EnglishChar && EnglishChar <= 0x5A ) ) {
       				
       			} else {
		        	return true;
		        }
       		}
       		return false;
       }
       
       function fnCheckDigit(str) {  
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    iValue = parseInt(ch_char);
		        if(isNaN(iValue)) {
		           
		        } else {
		        	return true;
		        }
			}
            return false;
            
        }
		
	</script>
<div id="cntnts">
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<c:set var="actionUrl" value="EgovStaffSelectIndt.do"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<c:set var="actionUrl" value="EgovStaffSelectUpdt.do?targetId=${userManageVO.userId}"/>
			</c:when>
		</c:choose> 
	
	<form:form commandName="userManageVO" name="userManageVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/mng/usr/${actionUrl }"> 
	
	<form:hidden path="moblphonNo"/>
	<form:hidden path="userSeCode" value="10"/>
		
	<table class="chart2">
		<caption>회원관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
			<col width="300"/>
		</colgroup>
		<tbody>
		<tr>
			<th><em>*</em><label for="userNm"> 이름 </label><span style="color:red;">(필수)</span></th>
			<td>
				<form:input path="userNm" id="userNm" cssClass="inp" />
				<div><form:errors path="userNm" /></div>
			</td>
			<td rowspan="11" align="center">
				<c:choose>
					<c:when test="${empty userManageVO.photoStreFileNm}">사진없음</c:when>
					<c:otherwise><img src="${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>" style="max-width:300px"/></c:otherwise>
				</c:choose>
				<br><input type="file" name="userPicFile" class="inp" style="width:300px"/>
			</td>
		</tr>

		<tr>
			<th><label for="userSchNo"> 사번 </label>(선택)</th>
			<td>
				<form:input path="userSchNo" id="userSchNo" cssClass="inp" maxlength="20"/>
				&nbsp;&nbsp;
		  	  	<form:select path="workStatusCode">
					<c:forEach var="result" items="${workStatusList}" varStatus="status">
						<option value="${result.code}" <c:if test="${userManageVO.workStatusCode eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</form:select>
			</td>
		</tr>
		
		<tr>
			<th><label for="groupCode"> 소속 </label>(선택)</th>
			<td>
		  	  	<form:select path="groupCode">
					<option value="">선택</option>
					<c:forEach var="result" items="${groupList}" varStatus="status">
						<option value="${result.codeNm}" <c:if test="${userManageVO.groupCode eq result.codeNm}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</form:select>
	  	  	</td>
		</tr>
		
		<tr>
			<th><label for="moblphonNo">연락처</label> (선택)</th>
			<td>
				<c:set var="moblArr" value="${fn:split(userManageVO.moblphonNo, '-')}"/>
				<c:forEach items="${moblArr}" var="arr" varStatus="status">
					<c:if test="${status.count eq 1}"><c:set var="phone1" value="${fn:trim(arr)}"/></c:if>
					<c:if test="${status.count eq 2}"><c:set var="phone2" value="${fn:trim(arr)}"/></c:if>
					<c:if test="${status.count eq 3}"><c:set var="phone3" value="${fn:trim(arr)}"/></c:if>
				</c:forEach>
				<select id="phone1" name="phone1" title="휴대전화번호 선택">
					<option value="010" <c:if test="${phone1 eq '010'}"> selected="selected"</c:if>>010</option>
					<option value="011" <c:if test="${phone1 eq '011'}"> selected="selected"</c:if>>011</option>
					<option value="016" <c:if test="${phone1 eq '016'}"> selected="selected"</c:if>>016</option>
					<option value="017" <c:if test="${phone1 eq '017'}"> selected="selected"</c:if>>017</option>
					<option value="018" <c:if test="${phone1 eq '018'}"> selected="selected"</c:if>>018</option>
					<option value="019" <c:if test="${phone1 eq '019'}"> selected="selected"</c:if>>019</option>
				</select>
				<input type="text" id="phone2" name="phone2" value="${phone2}" maxlength="4" class="inp tel" title="휴대전화번호 가운데자리 입력" />
				<input type="text" id="phone3" name="phone3" value="${phone3}" maxlength="4" class="inp tel" title="휴대전화번호 뒷자리 입력" />
			</td>
		</tr>
		<tr>
			<th><label for="emailHead"> E-mail </label><span style="color:red;">(필수)</span></th>
			<td>
			<c:set var="emailArr" value="${fn:split(userManageVO.emailAdres, '@')}"/>
				<c:forEach items="${emailArr}" var="arr" varStatus="status">
					<c:if test="${status.count eq 1}">
						<c:set var="emailHead" value="${fn:trim(arr)}"/>
					</c:if>
					<c:if test="${status.count eq 2}">
						<c:set var="emailBody" value="${fn:trim(arr)}"/>
					</c:if>
				</c:forEach>
				<input type="text" name="email1" id="email1" value="${emailHead}" class="inp" /> @ <input type="text" name="email2" value="${emailBody}" id="email2" class="inp"/>
				<select id="email_choice" name="email_choice" onchange='inputDirectEmailDns(this.value);'>
						<option value="">직접입력</option>
						<option value="hanmail.net"	<c:if test="${emailBody eq 'hanmail.net'}"> selected="selected"</c:if>>다음</option>
						<option value="naver.com"	<c:if test="${emailBody eq 'naver.com'}"> selected="selected"</c:if>>네이버(naver.com)</option>
						<option value="nate.com"	<c:if test="${emailBody eq 'nate.com'}"> selected="selected"</c:if>>네이트(nate.com)</option>
						<option value="empal.com"	<c:if test="${emailBody eq 'empal.com'}"> selected="selected"</c:if>>엠파스</option>
						<option value="paran.com"	<c:if test="${emailBody eq 'paran.com'}"> selected="selected"</c:if>>파란(paran.com)</option>
						<option value="hanafos.com"	<c:if test="${emailBody eq 'hanafos.com'}"> selected="selected"</c:if>>하나포스(hanafos.com)</option>
						<option value="gmail.com"	<c:if test="${emailBody eq 'gmail.com'}"> selected="selected"</c:if>>G메일(gmail.com)</option>
						<option value="kornet.net"	<c:if test="${emailBody eq 'kornet.net'}"> selected="selected"</c:if>>코넷</option>
						<option value="korea.com"	<c:if test="${emailBody eq 'korea.com'}"> selected="selected"</c:if>>코리아닷컴(korea.com)</option>
						<option value="dreamwiz.com"	<c:if test="${emailBody eq 'dreamwiz.com'}"> selected="selected"</c:if>>드림위즈(dreamwiz.com)</option>
						<option value="lycos.co.kr"	<c:if test="${emailBody eq 'lycos.co.kr'}"> selected="selected"</c:if>>라이코스(lycos.co.kr)</option>
						<option value="chollian.net"	<c:if test="${emailBody eq 'chollian.net'}"> selected="selected"</c:if>>천리안(chollian.net)</option>
						<option value="yahoo.co.kr"	<c:if test="${emailBody eq 'yahoo.co.kr'}"> selected="selected"</c:if>>야후(yahoo.co.kr)</option>
						<option value="hotmail.com"	<c:if test="${emailBody eq 'hotmail.com'}"> selected="selected"</c:if>>핫메일(hotmail.com)</option>
				</select>
				
			</td>
		</tr>
		
		<tr>
			<th><label for="userId"> 아이디</label> (선택)</th>
			<td>
			<form:input path="userId" id="userId" cssClass="inp" maxlength="20"/> 
			<%-- <a href="<c:url value='/uss/umt/cmm/EgovIdDplctCnfirm.do?checkId='/>" onclick="fnIdCheck();return false;" class="btn_s"><span>중복확인</span></a> --%>
			<p>등록하지 않을 시 E-mail 정보로 자동 등록됩니다.</p>
			</td>
		</tr>
		
		<tr>
			<th><em>*</em><label for="password"> 패스워드</label> (자동)</th>
			<td>등록 시 자동으로 1234로 지정됩니다.</td>
		</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="./EgovMberManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<%-- <a href="/mng/usr/EgovMberAddView.do"><img src="${_IMG}/btn/btn_regist.gif" alt="등록하기" /></a> --%>
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<input type="image" src="${MNG_IMG}/btn/btn_regist.gif" alt="등록하기" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<input type="image" src="<c:url value='${MNG_IMG}/btn/btn_modify.gif'/>" alt="수정" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
		</c:choose>

		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소"/></a>
						
	</div>
</form:form>
<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	