<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/cop/bbs"/>
<c:set var="serverName" value="<%=request.getServerName()%>"/>
<c:set var="serverPort" value="<%=request.getServerPort()%>"/>
<c:choose>
	<c:when test="${serverPort == 80}">
		<c:set var="serverPort" value=""/>
	</c:when>
	<c:otherwise>
		<c:set var="serverPort" value=":${serverPort}"/>
	</c:otherwise>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="siteId" value="${searchVO.siteId}"/>
	<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:param name="trgetId" value="${searchVO.trgetId}" />
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="searchCnd" value="${searchVO.searchCnd}" />
	<c:param name="searchWrd" value="${searchVO.searchWrd}" />
	<c:param name="searchCate" value="${searchVO.searchCate}" />
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<% /*URL 정의*/ %>

<%-- nttNo = 0은 취소/환불 규정 때문에 처리함 --%>
<c:if test="${searchVO.nttNo ne 0 }">
	<c:import url="/EgovPageLink.do?link=/mng/template/popTop" charEncoding="utf-8">
		<c:param name="title" value="${brdMstrVO.bbsNm}"/>
	</c:import>
</c:if>

	<script type="text/javascript">
		function onloading() {
			if ("<c:out value='${msg}'/>" != "") {
				alert("<c:out value='${msg}'/>");
			}
		}
			
		function fn_egov_delete_notice(url) {
			
			if (confirm('<spring:message code="common.delete.msg" />')) {
				document.location.href = url;
			}	
		}
		
		function urlCopy() {
			var addr = "http://${serverName}${serverPort}/hpg/bbs/selectBoardArticle.do?nttNo=${searchVO.nttNo}&menuId=${menuId}&bbsId=${searchVO.bbsId}";
			window.clipboardData.setData('Text', addr);
			alert("페이지 주소가 복사되었습니다.\n" + addr);
		}

	</script>
<div id="cntnts">

	<div class="view_wrap">
		<dl class="view_tit">
			<dt>
				<c:choose>
					<%-- 교재/사전 --%>
					<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">교재명</c:when>
					<%-- K-MOOC --%>
					<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000007'}">강좌명</c:when>
					<c:otherwise><spring:message code="cop.nttSj" /></c:otherwise>
				</c:choose>
			</dt>
			<dd><strong><c:out value="${board.nttSj}" /></strong> <a href="#" onclick="urlCopy();return false;"><img src="${_IMG}/btn/add_copy.gif" style="vertical-align:middle;margin-left:10px;"/></a></dd>
		</dl>
		
		<table  class="view_writer_chart">
			<caption></caption>
			<colgroup>
				<col width="110px" />
				<col width="*" />
				<col width="110px" />
				<col width="*" />
			</colgroup>
			<tbody>
                <c:if test="${!empty brdMstrVO.ctgrymasterId}">
	                <tr>
		                <th>
		                	<c:choose>
								<%-- 교재/사전 --%>
								<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">언어</c:when>
								<c:otherwise><spring:message code="cop.category.view" /></c:otherwise>
							</c:choose>
		                </th>
		                <td colspan="3" class="last"><c:out value="${board.ctgryNm}" /></td>
	                </tr>  
                </c:if>
                <%-- 교재/사전 --%>
			 	<c:if test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000005'}">
			 		<tr>
						<th><label>메인노출 여부</label></th>
						<td colspan="3" class="last">
							<c:choose>
								<c:when test="${board.noticeAt eq 'Y'}">예</c:when>
								<c:otherwise>아니오</c:otherwise>
							</c:choose>
						</td>
					</tr>
			 		<tr>
						<th><label>출판사</label></th>
						<td colspan="3" class="last"><c:out value="${board.tmp01}" /></td>
					</tr>
					<tr>
						<th><label>저자</label></th>
						<td colspan="3" class="last"><c:out value="${board.tmp02}" /></td>
					</tr>
					<tr>
						<th><label>책표지</label></th>
						<td colspan="3" class="last">
							<c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
						    	<c:param name="atchFileId" value="${board.atchFileId}" />
						    	<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
						    	<c:param name="siteId" value="${brdMstrVO.siteId}"/>
						    	<c:param name="width" value="220"/>
						    	<c:param name="height" value="310"/>
							</c:import>
						</td>
					</tr>
					<tr>
						<th><label>구매URL</label></th>
						<td colspan="3" class="last"><a href="<c:out value="${board.tmp05}" />" target="_blank"><c:out value="${board.tmp05}" /></a></td>
					</tr>
					<tr>
						<th><label>키워드</label></th>
						<td colspan="3" class="last">
							<c:set var="keyword" value="${fn:split(board.tmp04,',')}"/>
							<c:forEach var="result" items="${keyword}" varStatus="status">
								<span class="keyword">${result}</span>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<th><label>E-Book 여부</label></th>
						<td>
							<c:choose>
								<c:when test="${board.tmp03 eq 'Y'}">예</c:when>
								<c:otherwise>아니오</c:otherwise>
							</c:choose>
						</td>
						<th><label>학습자료로 공개</label></th>
						<td class="last">
							<c:choose>
								<c:when test="${board.tmp06 eq 'Y'}">공개</c:when>
								<c:otherwise>비공개</c:otherwise>
							</c:choose>
			            </td>
					</tr>
			 	</c:if>
			 	<%-- K-MOOC --%>
			 	<c:if test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000007'}">
			 		<tr>
						<th><label>강좌표지</label></th>
						<td colspan="3" class="last">
							<c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
						    	<c:param name="atchFileId" value="${board.atchFileId}" />
						    	<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
						    	<c:param name="siteId" value="${brdMstrVO.siteId}"/>
						    	<c:param name="width" value="220"/>
						    	<c:param name="height" value="310"/>
							</c:import>
						</td>
					</tr>
			 		<tr>
						<th><label>강좌기간</label></th>
						<td colspan="3" class="last">
							<c:out value="${board.tmp01}" /> ~ <c:out value="${board.tmp02}" />
						</td>
					</tr>
					<tr>
						<th><label>강좌운영기관</label></th>
						<td colspan="3" class="last"><c:out value="${board.tmp03}" /></td>
					</tr>
					<tr>
						<th><label>K-MOOC URL</label></th>
						<td colspan="3" class="last"><a href="<c:out value="${board.tmp05}" />" target="_blank"><c:out value="${board.tmp05}" /></a></td>
					</tr>
					<tr>
						<th><label>학습자료로 공개</label></th>
						<td colspan="3" class="last">
							<c:choose>
								<c:when test="${board.tmp06 eq 'Y'}">공개</c:when>
								<c:otherwise>비공개</c:otherwise>
							</c:choose>
			            </td>
					</tr>
			 	</c:if>
                <tr>
	                <th><spring:message code="cop.ntcrNm" /></th>
	                <td colspan="3" class="last"><c:out value="${board.ntcrNm}" /> (<c:out value="${board.frstRegisterId}" />)</td>
                </tr>
                <tr>
                	<th><spring:message code="cop.frstRegisterPnttm" /></th>
	                <td><fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                <th><spring:message code="cop.inqireCo" /></th>
	                <td class="last"><c:out value="${board.inqireCo}" /></td>
                </tr>
 			</tbody>
		</table>
         <div class="view_cont">
			<c:out value="${board.nttCn}" escapeXml="false" />
		</div>
		
		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
		<dl class="view_tit02">
			<dt><spring:message code="cop.atchFileList" /></dt>
			<dd>
				<ul class="list">
					<c:if test="${not empty board.atchFileId}">
						<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
							<c:param name="param_atchFileId" value="${board.atchFileId}" />
							<c:param name="imagePath" value="${_IMG }"/>
							<c:param name="mngAt" value="Y"/>
						</c:import>
					</c:if>
				</ul>
			</dd>
		</dl>
		</c:if>
		
		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and board.processSttusCode eq 'QA03'}">
			<c:if test="${not empty board.estnData}">
				<div class="view_cont">
					<c:out value="${board.estnParseData.cn}" escapeXml="false" />
				</div>
			</c:if>
			<c:if test="${not empty board.estnAtchFileId}">
				<dl class="view_tit02">
					<dt><spring:message code="cop.atchFileList" /></dt>
					<dd>
						<ul class="list">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.estnAtchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
							</c:import>
						</ul>
					</dd>
				</dl>
			</c:if>
		</c:if>
		
		<c:if test="${board.bbsId eq 'BBSMSTR_000000000025'}">
			<strong style="color:red;">※ 첨부파일에 첫번째가 "취소 신청서", 두번째까 "환불 신청서"로 사용자 페이지에 노출됩니다. 순서에 맞게 등록 부탁드립니다.</strong>
		</c:if>
	</div>
	
	<div class="btn_r">
		<%-- nttNo = 0은 취소/환불 규정 때문에 처리함 --%>
		<c:if test="${searchVO.nttNo ne 0 }">
			<a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
		</c:if>
      	<c:if test="${brdMstrVO.replyPosblAt eq 'Y' and searchVO.nttNo ne 0}">
      		<c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
      			<c:param name="nttNo" value="${board.nttNo}" />
	  			<c:param name="registAction" value="reply" />
			</c:url>
        	<a href="<c:out value="${addReplyBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_reply.gif" alt="답글"/></a>
      	</c:if>
      	
		<c:choose>
			<c:when test="${searchVO.nttNo ne 0 }">
				<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
		      		<c:param name="nttNo" value="${board.nttNo}" />
			  		<c:param name="registAction" value="updt" />
				</c:url>
			</c:when>
			<c:otherwise>
				<c:url var="forUpdateBoardArticleUrl" value="/mng/lms/crm/curseregManageCancelRuleReg.do">
				</c:url>
			</c:otherwise>
		</c:choose>
		<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>
      	<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
      		<c:param name="nttNo" value="${board.nttNo}" />
		</c:url>
		<c:if test="${searchVO.nttNo ne 0 }">
      		<a href="<c:out value="${deleteBoardArticleUrl}"/>" onclick="fn_egov_delete_notice(this.href);return false;"><img src="${_IMG}/btn/btn_del.gif" alt="삭제"/></a>
      	</c:if>
	</div>
	
	<c:if test="${brdMstrVO.commentUseAt eq 'Y'}"> 
    		<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
    			<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
    		</c:import> 
    </c:if>
    
</div>        

<c:import url="/EgovPageLink.do?link=/mng/template/popBottom" charEncoding="utf-8"/>	