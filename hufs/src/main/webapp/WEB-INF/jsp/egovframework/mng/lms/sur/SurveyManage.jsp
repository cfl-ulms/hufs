<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="schdulClCode" value="${param.schdulClCode}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="SURVEY_MANAGE"/>
	<c:param name="depth2" value="${param.schdulClCode}"/>
	<c:param name="title" value="${param.schdulClCode eq 'TYPE_1' ? '과정만족도 조사' : '수업만족도 조사' }"/>
</c:import>

<script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
<script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
<script src="/template/lms/js/sur/surveyObj.js?v=1"></script>
<link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
<style>
table {width: 678px;
    margin: 0;
    float: left;
    position: relative;
    top: 0;
    padding: 0;
    border: 1px solid #444444;
   }
tr, td {
border: 1px solid #444444;
}
div.survey_div{
border: 1px solid #444444;
overflow-y: scroll;
height : 500px;
}
div.survey_question{
float:left
}

c2_tb tr,c2_tb tb{
	width:100px;
	height : 50px;

}

textarea {
	resize : none;
}

.box_btn{clear:both;}
</style>

<script type="text/javascript">

$(function(){

	if($(".tbset").length == 0){
		fnAddQuestion();

	}
});




</script>

<div id="surveyContent">
<input type="hidden" id="schdulId" value="${param.schdulId }"/>
<input type="hidden" id="schdulClCode" value="${param.schdulClCode }"/>
		<div >
			<input type="text" id="surveyTitle" placeholder="설문명을 입력하세요." style="width:100%;height:60px;font-size:26px;" value="${actionKey eq 'update' ? surveyInfo.schdulNm : '' }"/>
		</div>
		<div id="questionDiv">
			<c:if test="${actionKey eq 'update'}" >
				<c:if test="${not empty surveyInfo.questionsArray }">
					<c:forEach items="${surveyInfo.questionsArray}" var="question" varStatus="status">

						<div class="tbset">
							<div class="tb02wrap">
						 	<a href="#" class="upMove" title="위로이동"><img src="/template/manage/images/btn/btn_goup.gif"/></a>
				 			<a href="#" class="downMove" title="아래이동"><img src="/template/manage/images/btn/btn_godown.gif"/></a>
						    <table class="chart2 ip_tb02">
						        <tbody>
						        <tr>
									<th>설문 번호</th>
						 			<td class="surveyTd">${question.qesitmSn}</td>
									<th>필수여부</th>
									<td>
										<input type="radio" id="essenAtY${status.count }" class="essenAt" name="essenAt${status.count }" value="Y" ${empty question.essenAt or question.essenAt eq 'Y' ? 'checked' : '' }>
										<label for="essenAtY${status.count }">필수</label>
					                  	<input type="radio" id="essenAtN${status.count }" class="essenAt" name="essenAt${status.count }" value="N" ${question.essenAt eq 'N' ? 'checked' : '' }></label>
					                  	<label for="essenAtN${status.count }">선택</label>
									</td>
						        </tr>
						        <tr>
									<th>항목 주제</th>
									<td colspan="3"><textarea style="width:100%;height:50px;" placeholder="항목 주제를 입력하세요." name="qesitmSj" >${question.qesitmSj}</textarea></td>
						        </tr>
						        <tr>
									<th>설명</th>
									<td colspan="3"><textarea style="width:100%;height:50px;" placeholder="항목에 대한 자세한 설명을 입력하실 수 있습니다." name="qesitmCn" >${question.qesitmCn}</textarea></td>
						        </tr>
								<tr>
									<th>유형</th>
									<td colspan="3">
										<div class="f_wrap">

											<select class="uselectbox" name="qesitmTyCode">
												<option value="multiple" ${question.qesitmTyCode eq 'multiple' ? 'selected' : ''}>객관식</option>
												<option value="answer" ${question.qesitmTyCode eq 'answer' ? 'selected' : ''}>주관식</option>
												<option value="table" ${question.qesitmTyCode eq 'table' ? 'selected' : ''}>표형</option>
											</select>
											&nbsp;
											<select class="detailType multiple" name="qesitmTy" style="${question.qesitmTyCode eq 'multiple' ? '' : 'display:none'}">
												<option value="" ${empty question.qesitmTy ? 'selected' : ''}>선택</option>
												<option value="1" ${question.qesitmTy eq '1' ? 'selected' : ''}>1. 그렇다 유형</option>
												<option value="2" ${question.qesitmTy eq '2' ? 'selected' : ''}>2. 만족 유형</option>
												<option value="3" ${question.qesitmTy eq '3' ? 'selected' : ''}>3. (5)~(1) 점수 유형</option>
												<option value="4" ${question.qesitmTy eq '4' ? 'selected' : ''}>4. 수동입력</option>
											</select>
						                    <select class="detailType table" style="${question.qesitmTyCode eq 'table' ? '' : 'display:none'}" name="qesitmTy">
												<option value="" ${empty question.qesitmTy ? 'selected' : ''}>선택</option>
												<option value="6" ${question.qesitmTy eq '6' ? 'selected' : ''}>1. (5)~(1) 점수 유형</option>
												<option value="7" ${question.qesitmTy eq '7' ? 'selected' : ''}>2. 수동입력</option>
											</select>
										</div>
						            </td>
						        </tr>
						        <c:if test="${question.qesitmTyCode eq 'table' and question.qesitmTy eq '7' }">
						        	<tr class="addTr">
										<th>표 설정</th>
										<td colspan="3">
											가로 : <input type="text" class="tableWidth" value="${fn:length(question.widthExamples)}"/> X 세로 : <input type="text" class="tableHeight" value="${fn:length(question.heightExamples)}"/>
											<button class="fnTable">표 만들기</button>
										</td>
									</tr>
						        </c:if>
						        <tr class="tr-result">
						            <td>&nbsp;</td>
						            <td colspan="3">

						             	<c:choose>
						             		<c:when test="${question.qesitmTyCode eq 'multiple'}">
					             				<ul class="type${question.qesitmTy}">

					             					<c:if test="${question.qesitmTy eq '4'}">
					             						<a href="#" class="btn_add btn_mngTxt" style="margin:5px auto;">클릭하면 추가</a>
					             					</c:if>

							             			<c:forEach items="${question.examples}" var="examples" varStatus="st">
							             				<li>
									            			<label>
														    	<input type="radio" class="rd01">&nbsp;

														    	<c:choose>
														    		<c:when test="${question.qesitmTy eq '4'}">
														    			<span><input type="text" class="exCn" value="${fn:trim(examples.exCn)}"/></span>
														    		</c:when>
														    		<c:otherwise>
														    			<span class="exCn">${fn:trim(examples.exCn)}</span>
														    		</c:otherwise>
														    	</c:choose>

														    </label>
															<a href="#" class="btn_delitem"><img src="/template/manage/images/btn/del.gif"/></a>
									            		</li>
							             			</c:forEach>
						             			</ul>
						             		</c:when>
						             		<c:when test="${question.qesitmTyCode eq 'table'}">

							             		<div class="type${question.qesitmTyCode}">

							             			<c:if test="${question.qesitmTy eq '6' }">
														<a href="#" class="btn_mngTxt add_table" style="margin:5px auto;">클릭하면 추가</a>
													</c:if>
													<div class="ip_wrap">
														<table class="chart2" style="width:450px;">
									                        <tbody>

								                        		<c:set var="wIndex" value="0"/>
									                        	<c:forEach begin="0" end="${fn:length(question.widthExamples) }"  step="1" varStatus="wStatus" var="wResult">
								                        			<c:set var="hIndex" value="0"/>
									                        		<tr>
									                        		<c:forEach begin="0" end="${fn:length(question.heightExamples)}" step="1" varStatus="hStatus" var="hResult" >
									                        			<c:choose>
									                        				<c:when test="${wStatus.first}">
									                        					<c:choose>
										                        					<c:when test="${hStatus.first}">
										                        						<c:choose>
										                        							<c:when test="${question.qesitmTy eq '6' }">
										                        								<td class="th_1" style="width:250px;"></td>
										                        							</c:when>
										                        							<c:otherwise>
													                        					<td><textarea disabled></textarea></td>
										                        							</c:otherwise>
										                        						</c:choose>
									                        						</c:when>
									                        						<c:otherwise>
								                        								<c:choose>
										                        							<c:when test="${question.qesitmTy eq '6' }">
										                        								<td class="exCn height">${fn:trim(question.heightExamples[hIndex].exCn)}</td>
										                        							</c:when>
										                        							<c:otherwise>
													                        					<td><textarea class="exCn height">${fn:trim(question.heightExamples[hIndex].exCn) }</textarea></td>
										                        							</c:otherwise>
										                        						</c:choose>
											                        					<c:set var="hIndex" value="${hIndex + 1}"/>
									                        						</c:otherwise>
										                        				</c:choose>
									                        				</c:when>
									                        				<c:otherwise>
									                        					<c:choose>
										                        					<c:when test="${hStatus.first }">
							                        									<td class="th_1 free_textarea">
																							<textarea class="exCn width" title="여기에 내용을입하세요." placeholder="보기주제를 입력하세요.">
																								${fn:trim(question.widthExamples[wIndex].exCn) }
																							</textarea>
																						</td>
																						<c:set var="wIndex" value="${wIndex + 1}"/>
										                        					</c:when>
										                        					<c:otherwise>
										                        						<c:choose>
										                        							<c:when test="${question.qesitmTy eq '6' }">
										                        								<td><input type="radio"  class="rd01"></td>
										                        							</c:when>
										                        							<c:otherwise>
													                        					<td></td>
										                        							</c:otherwise>
										                        						</c:choose>
										                        					</c:otherwise>
										                        				</c:choose>

									                        				</c:otherwise>
									                        			</c:choose>
									                        		</c:forEach>
								                        			</tr>
																</c:forEach>
															</tbody>
														</table>
													</div>
												</div>
						             		</c:when>
						             		<c:otherwise>
						             			<ul class="type5">
													<li><textarea class="tarea02" style="width:100%;height:45px;" placeholder="주관식 입력"></textarea></li>
												</ul>
						             		</c:otherwise>
						             	</c:choose>


						            </td>
								</tr>
						    	</tbody>
							</table>
				 			<a href="#" class="set_x" title="삭제하기" style="right:-20px;"><img src="/template/manage/images/btn/del.gif"/></a>
						</div>
					</div>


					</c:forEach>
				</c:if>
			</c:if>

		</div>
		
	<div class="bt_wrap" style="display: table;">
	    <button class="bt_2" onclick="fnSave('${actionKey eq 'update' ? 'update' : 'insert'}');">저장</button>
		<button class="bt_0" onclick="fnAddQuestion();">항목추가</button>
		<button class="bt_2" onclick="fnCancel();">취소</button>
	</div>
	
	<div class="box_btn">
	    <button class="bt_2" onclick="fnSave('${actionKey eq 'update' ? 'update' : 'insert'}');">저장</button>
		<button class="bt_0" onclick="fnAddQuestion();">항목추가</button>
		<button class="bt_2" onclick="fnCancel();">취소</button>
	</div>
</div>



<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>