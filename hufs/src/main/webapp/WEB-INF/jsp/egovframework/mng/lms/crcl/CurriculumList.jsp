<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchProjectAt}"><c:param name="searchProjectAt" value="${searchVO.searchProjectAt}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchHostCode" value="${searchVO.searchHostCode}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<script>
//달력
$(function() {
  $("#searchStartDate, #searchEndDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];

	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>

	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";

		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${searchVO.searchSysCode2}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}

			}
		}
		$("#ctgryId2").change();
	});

	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";

		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${searchVO.searchSysCode3}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
		$("#ctgryId3").change();
	});

	$("#ctgryId3").change(function(){
		var sysCode3 = $(this).val(),
			optionHtml = "<option value=''>기본과정명</option>";

		$.ajax({
			url : "/mng/lms/crclb/CurriculumbaseList.json"
			, type : "post"
			, dataType : "json"
			, data : {searchSysCode3 : sysCode3}
			, success : function(data){
				var searchcrclbId = "${searchVO.searchcrclbId}";
				if(data.successYn == "Y"){
					$.each(data.items, function(i){
						if(data.items[i].crclbId == searchcrclbId){
							optionHtml += "<option value='"+data.items[i].crclbId+"' selected='selected'>"+data.items[i].crclbNm+"</option>"
						}else{
							optionHtml += "<option value='"+data.items[i].crclbId+"'>"+data.items[i].crclbNm+"</option>"
						}

					});

					$("#searchcrclbId").html(optionHtml);
				}
			}, error : function(){
				alert("error");
			}
		});

	});

	$("#ctgryId1").change();
	
	//삭제
	$(".btn_del").click(function(){
		if(confirm("해당 과정을 정말 삭제하시겠습니까?")){
			return;
		}else{
			return false;
		}
	});
});
</script>
<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crcl/CurriculumList.do"/>">
	  		<select id="searchCrclYear" name="searchCrclYear">
	  			<option value="">선택</option>
				<c:forEach var="result" items="${yearList}" varStatus="status">
	  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
	  			</c:forEach>
			</select>

			<select id="searchCrclTerm" name="searchCrclTerm">
				<option value="">학기 전체</option>
				<c:forEach var="result" items="${crclTermList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~
			<input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<br/>
	  		<select id="ctgryId1" name="searchSysCode1">
				<option value="">대분류 전체</option>
				<c:forEach var="result" items="${sysCodeList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchSysCode1}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<select id="ctgryId2" name="searchSysCode2">
				<option value="">중분류 전체</option>
			</select>
			<select id="ctgryId3" name="searchSysCode3">
				<option value="">소분류 전체</option>
			</select>
			<select id="searchcrclbId" name="searchcrclbId">
				<option value="">기본과정명 전체</option>
			</select>
			<br/>
			<select id="control" name="searchHostCode">
				<option value="">주관기관 전체</option>
				<c:forEach var="result" items="${insList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchHostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<select id="control" name="searchCrclLang">
				<option value="">언어 전체</option>
				<c:forEach var="result" items="${langList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclLang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<select id="targetType" name="searchTargetType">
				<option value="">대상 선택</option>
				<option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
				<option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
			</select>
			<select id="searchTargetDetail" name="searchTargetDetail">
				<option value="">선택</option>
				<c:forEach var="result" items="${targetList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchTargetDetail}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>

			<select id="searchProcessSttusCode" name="searchProcessSttusCodeDate">
				<option value="">과정상태 전체</option>
				<c:forEach var="result" items="${statusComCode}" varStatus="status">
					<option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCode}">selected="selected"</c:if>>${result.codeNm}</option>
				</c:forEach>
			</select>

			<label><strong>과정명 : </strong> <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요."/></label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>

	<table class="chart_board">
	    <colgroup>
			<col width="50px"/>
			<col width="80px"/>
			<col width="60px"/>
			<col width="80px"/>
			<%-- 
			<col width="110px"/>
			<col width="150px"/>
			<col width="100px"/>
			 --%>
			<col width="130px"/>
			<col width="130px"/>
			<col width="130px"/>
			<col width="500px"/>
			<col width="80px"/>
			<col width="80px"/>
			<col width="100px"/>
			<col width="100px"/>
			<%-- <col width="150px"/> --%>
			<col width="200px"/>
			<col width="100px"/>
			<col width="50px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>번호</th>
	      	<th>등록구분</th>
	      	<th>년도</th>
	      	<th>학기</th>
	      	<th>기간</th>
	      	<!-- 
	        <th>대분류</th>
	        <th>중분류</th>
	        <th>소분류</th>
	         -->
	        <th>기본과정명</th>
	        <th>과정상태</th>
	        <th>과정명</th>
	        <th colspan="2">대상</th>
	        <th>등록인원</th>
	        <th>언어</th>
	        <!-- <th>주관기관</th> -->
	        <th>책임교원</th>
	        <th>사업비</th>
	        <th>관리</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:url var="viewUrl" value="/mng/lms/crcl/selectCurriculum.do${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
	    		<tr onclick="location.href='${viewUrl}'">
	    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.userSeCode eq '08'}">주관기관</c:when>
	    					<c:when test="${result.userSeCode eq '10' or result.userSeCode eq '99'}">운영</c:when>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.crclYear}"/></td>
	    			<td><c:out value="${result.crclTermNm}"/></td>
	    			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	    			<c:set var="sysPath" value="${fn:split(result.sysCodeNmPath,'>')}"/>
	    			<%-- 
	    			<td><c:out value="${sysPath[1]}"/></td>
	    			<td><c:out value="${sysPath[2]}"/></td>
	    			<td><c:out value="${sysPath[3]}"/></td>
	    			 --%>
	    			<td><c:out value="${result.crclbNm}"/></td>
	    			<td>
	    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
	    					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
						</c:forEach>
	    			</td>
	    			<td class="tit"><c:out value="${result.crclNm}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
	    					<c:otherwise>일반</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.targetDetailNm}"/></td>
	    			<td>
	    				<c:out value="${result.classStudentCnt}"/> /
	    				<c:choose>
	    					<c:when test="${result.applyMaxCnt > 0}"><c:out value="${result.applyMaxCnt}"/></c:when>
	    					<c:otherwise>제한없음</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.crclLangNm}"/></td>
	    			<%-- <td><c:out value="${result.hostCodeNm}"/></td> --%>
	    			<td>
	    				<c:out value="${result.hostCodeNm}"/><br/>
	    				<c:out value="${result.userNm}"/>
	    			</td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.totAmount eq '0' or empty result.totAmount}">무료</c:when>
	    					<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.totAmount}" />원</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td>
	    				<c:url var="delUrl" value="/mng/lms/crcl/deleteCurriculum.do${_BASE_PARAM}">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>
			        	<a href="${delUrl}" class="btn_del"><img src="${_IMG}/btn/del.gif"/></a>
	    			</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="14"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>
    </table>

    <div class="btn_r">
       	<a href="/mng/lms/crcl/addCurriculumView.do"><img src="/template/manage/images/btn/btn_regist.gif"></a>
	</div>

    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	    </c:url>

	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>