<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclNm" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchCrclNm" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchCrclNm" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchCrclNm" value="${searchVO.searchApplyStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchCrclNm" value="${searchVO.searchApplyEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclNm" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchCrclNm" value="${searchVO.searchHostCode}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCode}"><c:param name="searchProcessSttusCode" value="${searchVO.searchProcessSttusCode}" /></c:if>
	<c:if test="${not empty searchVO.searchRequestAt}"><c:param name="searchRequestAt" value="${searchVO.searchRequestAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG_CONFIRM"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강대상자 확정 승인"/>
</c:import>

<script>
//달력
$(function() {
    $("#searchApplyStartDate, #searchApplyEndDate").datepicker({
        dateFormat: "yy-mm-dd"
    });

    $("#searchStartDate, #searchEndDate").datepicker({
      dateFormat: "yy-mm-dd"
    });
});

$(document).ready(function(){
});

//검색 초기화
function fnReset(){
	$("#bbs_search").find("input").not("#resetBtn").val("");
	$("#bbs_search select").find("option:first").attr('selected', 'selected');
}
</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crm/curseregManageConfirm.do"/>">
	  		<select id="searchCrclYear" name="searchCrclYear">
	  			<option value="">선택</option>
	  			<c:forEach var="result" items="${yearList}" varStatus="status">
	  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
	  			</c:forEach>
			</select>
			
			<select id="searchCrclTerm" name="searchCrclTerm">
				<option value="">학기 전체</option>
				<c:forEach var="result" items="${crclTermList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~ 
			<input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<br/>
			
			<input type="text" name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" id="searchApplyStartDate" class="btn_calendar" placeholder="수강신청시작기간" readonly="readonly"/> ~ 
			<input type="text" name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" id="searchApplyEndDate" class="btn_calendar" placeholder="수강신청종료기간" readonly="readonly"/>

			<label><input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요."/></label>
			<br/>

			<select id="searchProcessSttusCode" name="searchProcessSttusCode">
				<option value="">과정상태 전체</option>
				<c:forEach var="result" items="${statusComCode}" varStatus="status">
					<option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCode}">selected="selected"</c:if>>${result.codeNm}</option>
				</c:forEach>
			</select>

			<select id="control" name="searchCrclLang">
				<option value="">언어 전체</option>
				<c:forEach var="result" items="${langList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclLang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<select id="control" name="searchHostCode">
				<option value="">주관기관 전체</option>
				<c:forEach var="result" items="${insList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchHostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>

			<label><input type="checkbox" name="searchRequestAt" value="Y" id=searchRequestAt <c:if test="${searchVO.searchRequestAt eq 'Y'}">checked</c:if> /> <strong>요청 대기만 조회 </strong></label>

			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;">
		</form>
	</div>

	<div style="color:red;"><strong>※ 관리자 확정 후 [과정관리] 메뉴에서 과정상태를 [수강대상자 확정] 으로 변경하셔야 학생에게 공지됩니다.</strong></div>
	
	<table class="chart_board">
	    <colgroup>
	    	<col width="50px"/>
			<col width="80px"/>
			<col width="100px"/>
			<col width="80px"/>
			<col width="100px"/>
			<col width="130px"/>
			<col width="160px"/>
			<col width="80px"/>
			<col width="50px"/>
			<col width="50px"/>
			<col width="80px"/>
			<col width="80px"/>
			<col width="80px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>년도</th>
	      	<th>학기</th>
	      	<th>수강신청기간</th>
	      	<th>과정상태</th>
	      	<th>기간</th>
	      	<th>과정명</th>
	      	<th>언어</th>
	      	<th>주관기관</th>
	      	<th>총학생</th>
	      	<th>분반수</th>
	      	<th>분반진행</th>
	      	<th>조배정</th>
	      	<th>수강대상자 확정</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:url var="viewUrl" value="/mng/lms/crm/curriculumStudent.do?${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:param name="crclbId" value="${result.crclbId}"/>
					<c:param name="menuCode" value="confm"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
	    		<tr onclick="location.href='${viewUrl}'">
	    			<td><c:out value="${result.crclYear}"/></td>
	    			<td><c:out value="${result.crclTermNm}"/></td>
	    			<td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
	    			<td>
	    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
	    					<c:if test="${statusCode.code eq result.processSttusCodeDate}">
	    						<c:if test="${statusCode.code eq 4 and result.confmSttusCode eq 3 }"><strong style="color:red;" title="과정상태를 [수강대상자 확정] 으로 변경하셔야 학생에게 공지됩니다."></c:if>
	    							<c:out value="${statusCode.codeNm}"/>
	    						<c:if test="${statusCode.code eq 4 and result.confmSttusCode eq 3 }"></strong></c:if>
	    					</c:if>
						</c:forEach>
	    			</td>
	    			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	    			<td><c:out value="${result.crclNm}"/></td>
	    			<td><c:out value="${result.crclLangNm}"/></td>
	    			<td><c:out value="${result.hostCodeNm}"/></td>
	    			<td><c:out value="${result.classStudentCnt}"/>명</td>
	    			<td><c:out value="${result.classCnt}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.confmSttusCode eq 0 }">진행중</c:when>
	    					<c:when test="${result.confmSttusCode > 1 and result.classCnt eq 0 }">안함</c:when>
	    					<c:when test="${result.confmSttusCode > 1 }">확정</c:when>
	    					<c:otherwise>-</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td>
						<c:choose>
	    					<c:when test="${result.confmSttusCode eq 0 }">진행중</c:when>
	    					<c:when test="${result.confmSttusCode > 1 and result.groupCnt eq 0 }">안함</c:when>
	    					<c:when test="${result.confmSttusCode > 1 }">확정</c:when>
	    					<c:otherwise>-</c:otherwise>
	    				</c:choose>
					</td>
	    			<td>
	    				<c:choose>
	    					<%-- <c:when test="${result.confmSttusCode eq 2 and result.processSttusCode <= 4 }">요청대기</c:when> --%>
	    					<c:when test="${result.confmSttusCode eq 2 and (result.processSttusCode eq 4 or result.processSttusCode > 5)}">요청대기</c:when>
	    					<c:when test="${result.confmSttusCode eq 3 and result.processSttusCode > 5 }">관리자확정</c:when>
	    					<c:otherwise>-</c:otherwise>
	    				</c:choose>
	    			</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="13"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    
    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crm/curseregManageConfirm.do?${_BASE_PARAM}">
	    </c:url>
	
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>