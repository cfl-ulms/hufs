<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="?">
		<c:if test="${!empty searchVO.searchGroup}"><c:param name="searchGroup" value="${searchVO.searchGroup}" /></c:if>
		<c:if test="${!empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
<c:set var="_ACTION" value="/mng/usr/EgovStudentManage.do" />
	<c:import url="/mng/template/top.do" charEncoding="utf-8">
		<c:param name="menu" value="MBER_MANAGE"/>
		<c:param name="depth1" value="MBER_MANAGE"/>
		<c:param name="depth2" value="STDT_MANAGE"/>
		<c:param name="title" value="학생조회"/>
	</c:import>	

<style>
tr td{cursor:pointer;}
</style>

	<div id="cntnts">	
			<form:form name="listForm" action="${_ACTION }" method="post">
			<div id="bbs_search">
			
				<%-- <label for="selectGroupCode" class="hdn">소속</label>
				<select name="searchGroup" id="selectGroupCode">
					<option value="">전체</option>
					<c:forEach var="result" items="${groupList}" varStatus="status">
						<option value="${result.code}" <c:if test="${searchVO.searchGroup eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</select> --%>
		  	  	<label for="selectGroupCode" class="hdn">소속</label>
				<select name="searchGroup" id="selectGroupCode">
					<option value="">전체</option>
					<option value="06" <c:if test="${searchVO.searchGroup eq '06'}">selected="selected"</c:if>>한국외국어대학교</option>
					<option value="04" <c:if test="${searchVO.searchGroup eq '04'}">selected="selected"</c:if>>기타대학</option>
					<option value="02" <c:if test="${searchVO.searchGroup eq '02'}">selected="selected"</c:if>>일반</option>
		  	  	</select>
				<label><strong>이름: </strong> <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" id="searchUserNm" /></label>
				<br/>
				<label><strong>생년월일: </strong> <input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy}" id="searchBrthdy" placeholder="숫자만 입력해주세요."/></label>
				<label><strong>연락처: </strong> <input type="text" name="searchMoblphonNo" value="${searchVO.searchMoblphonNo}" id="searchMoblphonNo" placeholder="-를 제외하고 입력해주세요.예)01012345678)" style="width:280px;"/></label>
				<br/>
				<label><strong>아이디: </strong> <input type="text" name="searchUserId" value="${searchVO.searchUserId}" id="searchUserId" style="width:200px;"/></label>&nbsp;&nbsp;&nbsp;
				<label><strong>이메일: </strong> <input type="text" name="searchEmailAdres" value="${searchVO.searchEmailAdres}" id="searchEmailAdres" style="width:280px;"/></label>
				<br/>
				<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			</div>
			<p class="total">검색결과 : ${paginationInfo.totalRecordCount}명</p>
	
			<table class="chart_board">
			<colgroup>
				<col width="70"/>				
				<col width="120"/>
				<col width="120"/>
				<col width="70"/>
				<col width="120"/>
				<col width="70"/>
				<col width="70"/>
				<col width="70"/>
				<col width="70"/>
				<col width="180"/>
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>소속</th>
					<th>아이디</th>
					<th>이름</th>
					<th>이메일</th>
					<th>생년월일</th>
					<th>학번</th>
					<th>연락처</th>
					<th>가입일</th>
					<th>수강이력</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr onclick="location.href='/mng/usr/EgovStudentView.do?userId=${result.userId }'">
					<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
					<td class="listtd">
						<c:choose>
							<c:when test="${result.userSeCode eq '02'}">
								<c:out value="${result.groupCode }"/>
							</c:when>
							<c:when test="${result.userSeCode eq '04'}">
								<c:out value="${result.groupCode }"/>
							</c:when>
							<c:when test="${result.userSeCode eq '06'}">
								<c:out value="한국외국어대학교"/>
							</c:when>
						</c:choose>
					</td>
					<td class="listtd"><c:out value="${result.userId}" /></td>
					<td class="listtd"><c:out value="${result.userNm}" /></td>
					<td class="listtd"><c:out value="${result.emailAdres}" /></td>
					<td class="listtd"><c:out value="${fn:substring(result.brthdy, 0, 4)}-${fn:substring(result.brthdy, 4, 6)}-${fn:substring(result.brthdy, 6, 8)}"/></td>
					<td class="listtd"><c:out value="${result.stNumber}" /></td>
					<td class="listtd">(+<c:out value="${result.geocode}"/>)<c:out value="${result.moblphonNo}" /></td>
					<td class="listtd"><fmt:formatDate value="${result.frstRegistPnttm}"  pattern="yyyy-MM-dd"/></td>
					<td class="listtd">
						<c:choose>
							<c:when test="${result.currCnt eq 0 }">
								없음
							</c:when>
							<c:when test="${result.currCnt eq 1 }">
								<c:out value="${result.currNm }"/>
							</c:when>
							<c:when test="${result.currCnt > 1 }">
								<c:out value="${result.currNm } 외 ${result.currCnt - 1}건 "/>
							</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
		      <tr>
		      	<td class="listtd" colspan="10">
		        	<spring:message code="common.nodata.msg" />
		        </td>
		      </tr>
		    </c:if>
	
			</tbody>
			</table>
		
		<%-- <div class="btn_r">
	       	<a href="<c:url value='/mng/usr/EgovMberAddView.do'/>"><img src="${_IMG}/btn/btn_regist.gif" /></a>
		</div> --%>
	
		<div id="paging">
			<c:url var="pageUrl" value="/mng/usr/EgovStudentManage.do${_BASE_PARAM}">
			</c:url>
	
			<c:if test="${not empty paginationInfo}">
				<ul>
					<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				</ul>
			</c:if>
		</div>
		</form:form>
	</div>
	<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>

