<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_JS" value="/template/manage/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value="/mng/lms/manage/studyPlanUpt.do?menu=${param.menu }"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${param.menu }"/>
	<c:param name="depth1" value="${param.menu eq 'CURRICULUM_MANAGE' ? 'BASE_CRCL' : 'CURRICULUM_STUDY'}"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.menu eq 'CURRICULUM_MANAGE' ? '과정등록관리' : ''}"/>
</c:import>

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_EDITOR_ID" value="spCn"/>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
$(document).ready(function(){
	var adfile_config = {
			siteId:"<c:out value='${siteInfo.siteId}'/>",
			pathKey:"Study",
			appendPath:"<c:out value='${scheduleMngVO.plId}'/>",
			editorId:"${_EDITOR_ID}",
			fileAtchPosblAt:"Y",
			maxMegaFileSize:"1024",
			atchFileId:"${scheduleMngVO.atchFileId}"
		};

	fn_egov_bbs_editor(adfile_config);

	//수업방법 값 초기에 있는지 확인
	<c:if test="${not empty scheduleMngVO.courseId}">
		$("#online, #offline").hide();
		$(".spType").attr("disabled", true);
	</c:if>
	
	//평가 값 초기에 있는지 확인
	<c:if test="${fn:length(evtResultList) > 0}">
		$(".evtAt").attr("disabled", true);
	</c:if>

	//수업방법
	$(".spType").click(function(){
		var val = $(this).val();

		if(val == "Y"){
			$("#offline").hide();
			$("#online").show();
		}else{
			$("#offline").show();
			$("#online").hide();
		}
	});

	//수업방법 선택
	$("#offline, #online").change(function(){
		var val = $(this).val(),
			name = $(this).children("option:selected").text();

		if(val){
			$("#offline, #online").hide();
			$(".spType").attr("disabled", true);
			$(".box_spType").html("<span>"+name+"</span><input type='hidden' name='courseId' value='"+val+"'/><a href='#' class='btn_deltype'><img src='/template/manage/images/btn/del.gif'/></a>");
		}

		//평가선택 시
		if(val == "CTG_0000000000000054"){
			$(".evtTr").show();
		}else{
			$(".evtTr").hide();
		}
	});

	//평가 선택
	$("#evtId").change(function(){
		var val = $(this).val(),
			name = $(this).children("option:selected").text();

		if($(".evtAt:checked").val() == "Y"){
			$(".box_evt").append("<li><span>"+name+"</span><input type='hidden' name='evtIdList' value='"+val+"'/><a href='#' class='btn_delevt'><img src='/template/manage/images/btn/del.gif'/></a></li>");
			$(".evtAt").attr("disabled", true);
		}else{
			$(".box_evt").html("");
		}
	});

	//평가 아니오 선택
	$(".evtAt").click(function(){
		if($(this).val() != "Y"){
			$(".box_evt").html("");
		}
	});

	//수업자료
	$(".btn_file1").click(function(){
		var tempHostCode = '${curriculumVO.hostCode }';
		window.open("/cmm/fms/studyFileList.do?hostCode="+tempHostCode, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=900");
		return false;
	});

	//교원변경
	$(".btn_chfc").click(function(){
		 var facAt = $("input[name=changeFacAt]").val();

		 if(facAt == "Y"){
			 $(".view_mode").show();
			 $(".edit_mode").hide();
			 $("input[name=changeFacAt]").val("N");
		 }else{
			 $(".view_mode").hide();
			 $(".edit_mode").show();
			 $("input[name=changeFacAt]").val("Y");
		 }
	});
});

//교원변경
$(document).on("change", ".fac", function(){
	var userId = $(this).val(),
		userNm = $(this).find("option:selected").data("nm"),
		cnt = 0;

	if(userId){
		$(this).siblings(".box_fac").find("input[name=facIdList]").each(function(){
			if($(this).val() == userId){
				cnt++;
				return false;
			}
		});

		if(cnt == 0){
			html = "<li><span>"+userNm+"</span> <input type='hidden' name='facIdList' value='"+userId+"'/> <a href='#' class='btn_delfac'><img src='/template/manage/images/btn/del.gif'/></a></li>";
			$(this).parents("tr").find(".box_fac").append(html);
		}else{
			alert("이미 등록된 교원입니다.");
		}
	}
});

//교수명 삭제
$(document).on("click", ".btn_delfac", function(){
	$(this).parents("li").remove();
	return false;
});

//달력
$(document).on("focus", ".btn_calendar", function(){
	$(this).datepicker({
	    dateFormat: "yy-mm-dd"
	});
});

//수업방법 삭제
$(document).on("click", ".btn_deltype", function(){
	$(this).parents(".box_spType").html("");
	$(".spType").attr("disabled", false);

	var val = $(".spType:checked").val();

	if(val == "Y"){
		$("#offline").hide();
		$("#online").show();
	}else{
		$("#offline").show();
		$("#online").hide();
	}

	return false;
});

//평가 삭제
$(document).on("click", ".btn_delevt", function(){
	$(this).parents("li").remove();
	$(".evtAt").attr("disabled", false);
	return false;
});

function vali(){
	$(".spType").attr("disabled", false);

	//파일
	$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

	return;
}

//양식관리 파일
function file(id, name, strName){
		var inp = "",
			dpChk= "";

		for(i = 0; i < id.length; i++){
			dpChk = 0;
			$("input[class=refeFileList]").each(function(){
				if($(this).val() == id[i]){
					alert("이미 "+name[i]+"파일은 참고자료로 등록하셨습니다.");
					dpChk++;
				}
			});

			if(dpChk == 0){
				inp = "<li class='refFileList'><input type='hidden' value='"+id[i]+"' class='refeFileList' name='refeFileList'/><img src='/template/manage/images/ico_file.gif' alt='파일'/> "+name[i]+" <a href='#' class='refFile'><img src='/template/manage/images/btn_sdelete.gif'/></a></li>";
				$("#refeFile_box").append(inp);
				$("#fileDelChk").val("Y");
			}
		}
}

//참고자료 삭제
$(document).on("click",".refFile",function(){
	$(this).parents("li").remove();
	$("#fileDelChk").val("Y");
	return false;
});

</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm}"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.menu eq 'CLASS_MANAGE' ? '3' : '5' }"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<br/>

	<form:form commandName="scheduleMngVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return vali();">
	<input type="hidden" name=plId value="${scheduleMngVO.plId}"/>
	<input type="hidden" name="crclId" value="${scheduleMngVO.crclId}"/>
	<input type="hidden" id="fileGroupId" name="fileGroupId" value="${scheduleMngVO.atchFileId}"/>
	<input type="hidden" id="fileDelChk" name="fileDelChk"/> <!-- 수업자료 삭제 체크 -->
	<form:hidden path="atchFileId"/>

	<table class="chart2">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody class="box_timetable">
			<tr>
			  	<th class="alC">수업주제</th>
			  	<td colspan="5"><input type="text" class="wid100" name="studySubject" value="${scheduleMngVO.studySubject}"/></td>
		  	</tr>
		  	<tr>
			  	<th class="alC">교원</th>
			  	<td colspan="5">
			  		<div class="view_mode">
			  			<ul>
					  		<c:forEach var="result" items="${facPlList}" varStatus="status">
								<li>
									<span>
										<c:out value="${result.userNm}"/>
										<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
									</span>
								</li>
							</c:forEach>
						</ul>
						<a href="#" class="btn_mngTxt btn_chfc">교원변경</a>
			  		</div>
			  		<div class="edit_mode" style="display:none;">
				  		<select class="fac">
							<option value="">선택</option>
							<c:forEach var="result" items="${facList}">
								<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
							</c:forEach>
						</select>
				  		<ul class="box_fac">
					  		<c:forEach var="result" items="${facPlList}" varStatus="status">
								<li>
									<span><c:out value="${result.userNm}"/><c:if test="${result.manageCode eq '04'}">(부교원)</c:if></span>
									<input type='hidden' name='facIdList' value='${result.facId}'/>
									<a href='#' class='btn_delfac'><img src='/template/manage/images/btn/del.gif'/></a>
								</li>
							</c:forEach>
						</ul>
						<a href="#" class="btn_mngTxt btn_chfc">교원변경취소</a>
					</div>
					<input type="hidden" name="changeFacAt" value="N"/>
			  	</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업일</th>
		  		<td><input type="text" name="startDt" class="btn_calendar" value="${scheduleMngVO.startDt}" autocomplete="off" readonly="readonly"/></td>

		  		<th class="alC">시작시간</th>
		  		<td>
		  			<c:choose>
						<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
							<select class="startTimeHH" name="startTimeHH">
								<option value="">선택</option>
								<c:forEach var="result" begin="6" end="24" step="1">
									<c:set var="hour">
										<c:choose>
											<c:when test="${result < 10}">0${result}</c:when>
											<c:otherwise>${result}</c:otherwise>
										</c:choose>
									</c:set>
									<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
								</c:forEach>
							</select>
                          	:
                          	<select class="startTimeMM" name="startTimeMM">
								<option value="">선택</option>
								<c:forEach var="result" begin="0" end="55" step="5">
									<c:set var="minute">
										<c:choose>
											<c:when test="${result < 10}">0${result}</c:when>
											<c:otherwise>${result}</c:otherwise>
										</c:choose>
									</c:set>
									<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
								</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<select class="startTime" name="startTime">
								<option value="">선택</option>
								<c:forEach var="result" items="${camSchList}">
									<option value="${result.startTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.startTime eq result.startTime}">selected="selected"</c:if>>
										<c:out value="${result.period}"/>교시
										<c:out value="${fn:substring(result.startTime,0,2)}"/>:<c:out value="${fn:substring(result.startTime,2,4)}"/>
									</option>
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
		  		</td>

		  		<th class="alC">종료시간</th>
		  		<td>
		  			<c:choose>
						<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
							<select class="endTimeHH" name="endTimeHH">
								<option value="">선택</option>
								<c:forEach var="result" begin="6" end="24" step="1">
									<c:set var="hour">
										<c:choose>
											<c:when test="${result < 10}">0${result}</c:when>
											<c:otherwise>${result}</c:otherwise>
										</c:choose>
									</c:set>
									<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.endTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
								</c:forEach>
							</select>
                          	:
							<select class="endTimeMM" name="endTimeMM">
								<option value="">선택</option>
								<c:forEach var="result" begin="0" end="55" step="5">
									<c:set var="minute">
										<c:choose>
											<c:when test="${result < 10}">0${result}</c:when>
											<c:otherwise>${result}</c:otherwise>
										</c:choose>
									</c:set>
									<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.endTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
								</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<select class="endTime" name="endTime">
								<option value="">선택</option>
								<c:forEach var="result" items="${camSchList}">
									<option value="${result.endTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.endTime eq result.endTime}">selected="selected"</c:if>>
										<c:out value="${result.period}"/>교시
										<c:out value="${fn:substring(result.endTime,0,2)}"/>:<c:out value="${fn:substring(result.endTime,2,4)}"/>
									</option>
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
		  		</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">장소</th>
		  		<td colspan="5">
		  			<select id="campusId" name="campusId" required>
						<option value="">캠퍼스 선택</option>
						<c:forEach var="result" items="${campusList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.campusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<input type="text" class="inp_long" name="placeDetail" value="${empty scheduleMngVO.placeDetail ? curriculumVO.campusPlace : scheduleMngVO.placeDetail}" placeholder="상세장소 입력"/>
		  		</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업영역</th>
		  		<td colspan="5">
		  			<c:set var="lang" value="${empty scheduleMngVO.crclLang ? curriculumVO.crclLang : scheduleMngVO.crclLang}"/>
		  			<select id="crclLang" name="crclLang" class="autoTxt" required>
						<option value="">선택</option>
						<c:forEach var="result" items="${langList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq lang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<select id="spLevel" name="spLevel" required>
						<c:forEach var="result" items="${levelList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.spLevel}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
		  		</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업방법</th>
		  		<td colspan="5">
		  			<label><input type="radio" class="spType" name="spType" value="Y" <c:if test="${scheduleMngVO.spType ne 'N'}">checked="checked"</c:if>/> 강의식</label>
		  			<label><input type="radio" class="spType" name="spType" value="N" <c:if test="${scheduleMngVO.spType eq 'N'}">checked="checked"</c:if>/> 비강의식</label>
		  			&nbsp;
	  				<select id="online" name="online" <c:if test="${scheduleMngVO.spType eq 'N'}">style="display:none;"</c:if>>
	  					<option value="">선택</option>
	  					<c:forEach var="result" items="${onList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.courseId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
	  				</select>

	  				<select id="offline" name="offline" <c:if test="${scheduleMngVO.spType ne 'N'}">style="display:none;"</c:if>>
	  					<option value="">선택</option>
	  					<c:forEach var="result" items="${offList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.courseId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
	  				</select>

	  				<div class="box_spType">
	  					<c:if test="${not empty scheduleMngVO.courseId}">
		  					<span><c:out value="${scheduleMngVO.courseNm}"/></span>
							<input type='hidden' name='courseId' value='${scheduleMngVO.courseId}'/>
							<a href='#' class='btn_deltype'><img src='/template/manage/images/btn/del.gif'/></a>
						</c:if>
	  				</div>
		  		</td>
		  	</tr>
		  	<tr class="evtTr" <c:if test="${scheduleMngVO.courseId ne 'CTG_0000000000000054'}">style="display:none;"</c:if>>
		  		<th class="alC">평가</th>
		  		<td colspan="5">
		  			<label><input type="radio" class="evtAt" name="evtAt" value="Y" <c:if test="${scheduleMngVO.evtAt ne 'N'}">checked="checked"</c:if>/> 예</label>
		  			&nbsp;
		  			<select id="evtId" name="evtId">
	  					<option value="">선택</option>
	  					<c:forEach var="result" items="${evtList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}"><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
	  				</select>

		  			<label><input type="radio" class="evtAt" name="evtAt" value="N" <c:if test="${scheduleMngVO.evtAt eq 'N'}">checked="checked"</c:if>/> 아니오(진행안함)</label>
		  			<ul class="box_evt">
		  				<c:forEach var="result" items="${evtResultList}">
		  					<li>
			  					<span><c:out value="${result.evtNm}"/></span>
								<input type='hidden' name='evtIdList' value='${result.evtId}'/>
								<a href='#' class='btn_delevt'><img src='/template/manage/images/btn/del.gif'/></a>
							</li>
						</c:forEach>
	  				</ul>
		  		</td>
		  	</tr>
		  	<tr>
		  		<td colspan="6"><textarea id="spCn" name="spCn" rows="30" style="width:100%"><c:out value="${scheduleMngVO.spCn}"/></textarea></td>
		  	</tr>
		  	<tr>
				<th><spring:message code="cop.atchFile" /></th>
				<td colspan="5">
					<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
						<c:param name="editorId" value="${_EDITOR_ID}"/>
						<c:param name="estnAt" value="N" />
				    	<c:param name="param_atchFileId" value="${scheduleMngVO.atchFileId}" />
				    	<c:param name="imagePath" value="${_IMG }"/>
				    	<c:param name="mngAt" value="Y"/>
				    	<c:param name="crclId" value="${scheduleMngVO.crclId}"/>
					</c:import>
					<noscript>
						<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
					</noscript>
				</td>
			</tr>
			<tr>
				<th>수업자료</th>
				<td colspan="3">
					<input type="text" id="refe" class="inp_long btn_file1" data-target="refe" placeholder="수업자료(기존 등록된 수업자료 추가)" readonly="readonly"/>
					<a href="#" class="btn_file1" data-target="refe"><img src="/template/manage/images/search_btn.gif" alt="파일선택"/></a>
					<ul id="refeFile_box">
						<c:forEach var="result" items="${fileList}" varStatus="status">
							<li class='refFileList'>
								<input type="hidden" value="${result.streFileNm}" class="refeFileList">
								<img src='/template/manage/images/ico_file.gif' alt='파일'/>
								${result.orignlFileNm}
								<a href='#' class='refFile'><img src='/template/manage/images/btn_sdelete.gif'/></a>
							</li>
						</c:forEach>
					</ul>
				</td>
			</tr>
			<tr class="stdntAply">
				<th>학습성과</th>
				<td colspan="5">
					<textarea id="spGoal" name="spGoal" rows="5" style="width:100%"><c:out value="${scheduleMngVO.spGoal}"/></textarea>
				</td>
			</tr>
		</tbody>
	</table>

	<div class="btn_c">
		<c:set var="listUrlTemp" value="${param.menu eq 'CURRICULUM_MANAGE' ? '/mng/lms/manage/studyPlan.do' : '/mng/lms/cla/curriculumStudyList.do'}"/>

		<input type="image" src="${_IMG}/btn/btn_modify.gif"/>
	    <c:url var="listUrl" value="${listUrlTemp }${_BASE_PARAM}"/>
	    <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>
	</form:form>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>