<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId }"/>
   <c:param name="bbsId" value="${searchVO.bbsId }"/>
   <c:param name="searchType" value="${searchVO.searchType }"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? param.menu : 'CURRICULUM_MANAGE' }"/>
	<c:param name="depth1" value="${not empty param.depth1 ? param.depth1 : 'BASE_CRCL' }"/>
   <c:param name="depth2" value=""/>
   <c:param name="title" value="과정등록관리"/>
</c:import>
<style>
table#resultTable th {
   padding : 0px 0px;
}
</style>
<script>
$(document).ready(function(){
   var targetDiv = document.getElementById('chart_pie')
   var myChart = echarts.init(targetDiv);
   var dataMapArr = [];
   var dataArr = [];

   <c:forEach var="result" items="${pieList}" varStatus="status">
      var tempObject = new Object();
      if('${result.cnt}' != '0'){
         tempObject.value = '${result.cnt}';
         tempObject.name = '${result.ctgryNm}';
         dataMapArr.push(tempObject);
         dataArr.push(tempObject.name);
      }
   </c:forEach>

   var option = {
           title: {
                 text: '글 구분별',
                 left: 'center'
             },
            animation: false,
            legend: {
             bottom: 10,
              data: dataArr,
              icon: 'rect',
              itemGap: 20,
              textStyle: {
                fontSize: 13,
                verticalAlign: 'middle',
              },
            },
            series: [
              {
                   type: 'pie',
                   radius: ['50%', '70%'],
                   label:false,
                   center: ['50%', '50%'],
                   hoverAnimation: false,
                   animation: false,
                   data: dataMapArr
              }
            ]
          };

   myChart.setOption(option);


   dataMapArr = [];
   <c:forEach var="result" items="${barList}" varStatus="status">
   var tempBarObject = new Object();
      tempBarObject.name = '${result.ctgryNm}';
      tempBarObject.type = 'bar';
      tempBarObject.stack = 'stack';
      var tempCnt = '${result.ctgCnt}';
      tempBarObject.data = tempCnt.split(",");
      dataMapArr.push(tempBarObject);
   </c:forEach>

   dataArr = [];
   <c:forEach var="result" items="${barMemberList}" varStatus="status">
      dataArr.push('${result.userNm}');
   </c:forEach>

   fnBarChart(dataMapArr,dataArr );

   $("#searchMemType").on("change", function(){
	   var searchMemType = $(this).val();
	   var tempCntType = '${masterVo.sysTyCode}'
	   var tempCrclId = '${param.crclId}';
	   var tempBbsId = '${param.bbsId}';

	   $.ajax({
			url : "/mng/lms/manage/lmsBbsStatisticsBar.json",
			dataType : "json",
			type : "POST",
			data : {"searchMemType" : searchMemType, "crclId" : tempCrclId, "bbsId" : tempBbsId, "cntType" : tempCntType.toLowerCase()},
			success : function(data){
				var memberList = data.barMemberList;
				var valueList = data.barList;
				var dataMapArr = [];
				var dataArr = [];

				if(memberList.length != 0){
					for(var i=0; i<memberList.length; i++){
						 dataArr.push(memberList[i].userNm);
					}

					for(var i=0; i<valueList.length; i++){
						var tempBarObject = new Object();
					      tempBarObject.name = valueList[i].ctgryNm;
					      tempBarObject.type = 'bar';
					      tempBarObject.stack = 'stack';
					      var tempCnt = valueList[i].ctgCnt;
					      tempBarObject.data = tempCnt.split(",");
					      dataMapArr.push(tempBarObject);
					}

				}

				fnBarChart(dataMapArr, dataArr);
			}, error : function(){
				alert("error");
			}
		});
   });

});

function fnBarChart(valueArr, memberArr){
   var targetDiv = document.getElementById('chart_bar')
   var barChart = echarts.init(targetDiv);

   if(memberArr.length != 0){
	    option = {

	          grid: {
	              left: '3%',
	              right: '4%',
	              bottom: '3%',
	              containLabel: true
	          },
	          xAxis: {
	              type: 'value'
	          },
	          yAxis: {
	              type: 'category',
	              data: memberArr
	          },
	          series: valueArr
	      };

	   barChart.setOption(option);
   }else{
	   barChart.clear();
   }
}

function fnBoardList(userId){
	var tempBbsId = '${searchVO.bbsId }';
	var tempCrclId = '${searchVO.crclId }';
	window.open("/mng/lms/manage/selectLmsBbsList.do?crclId="+tempCrclId+"&bbsId="+tempBbsId+"&frstRegisterId="+userId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
	return false;
}
</script>

   <table class="chart2">
      <tr>
         <td class="alC">
         	<c:out value="${curriculumVO.crclNm }"/>
         	<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
         </td>
      </tr>
   </table>
   <br/>
   <c:if test="${curriculumVO.processSttusCode > 0}">
      <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
         <c:param name="step" value="6"/>
         <c:param name="crclId" value="${curriculumVO.crclId}"/>
         <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
         <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
      </c:import>
   </c:if>


   <div id="chart_div" style="width:100%;float:left;margin-left:30px;">
   <h1>${masterVo.sysTyCode eq 'CLASS' ? '(반 별)' : masterVo.sysTyCode eq 'GROUP' ? '(조 별)' : ''} ${masterVo.bbsNm} (총 ${totalBbsCnt}개의 게시글 등록 )</h1>
   	<div style="text-align:right;margin-right:70px;">
   		<select id="searchMemType"  style="position:absolute;right:45px;z-index:10;">
            <option value="">전체 사용자</option>
            <option value="T">교원</option>
            <option value="S">학생</option>
            <option value="A">관리자</option>
         </select>
      </div>
      <div id="chart_pie" style="width:48%;height:300px;border:1px solid #ddd;float:left"></div>
      <div id="chart_bar" style="width:48%;height:300px;border:1px solid #ddd;float:left;"> </div>

   </div>

   <div>
      <div style="float:left;padding-top:34px;width:100%;">
	      <ul class="box_bbsmst">
				<c:url var="searchUrl1" value="/mng/lms/manage/lmsBbsStatistics.do">
					<c:param name="crclId" value="${param.crclId}"/>
					<c:param name="bbsId" value="${param.bbsId}" />
					<c:param name="searchType" value="student" />
				</c:url>

				<c:url var="searchUrl2" value="/mng/lms/manage/lmsBbsStatistics.do">
					<c:param name="crclId" value="${param.crclId}"/>
					<c:param name="bbsId" value="${param.bbsId}" />
					<c:param name="searchType" value="teacher" />
				</c:url>

				<li <c:if test="${searchVO.searchType eq 'student'}">class="on"</c:if>>
					<a href="${searchUrl1}">학생</a>
				</li>
				<li <c:if test="${searchVO.searchType eq 'teacher'}">class="on"</c:if>>
					<a href="${searchUrl2}">교원</a>
				</li>
		</ul>
         <div style="text-align:right;margin-bottom:11px;">
			<form name="frm" method="post" action="/mng/lms/manage/lmsBbsStatistics.do">
				<input type="hidden" name="crclId" value="${param.crclId }"/>
			  	<input type="hidden" name="bbsId" value="${param.bbsId }"/>
			  	<input type="hidden" name="searchType" value="${searchVO.searchType }"/>

	            <select name="sortType" id="sortType">
	               <option value="sum" ${searchVO.sortType eq 'sum' ? 'selected' : '' }>합계순</option>
	               <option value="name" ${searchVO.sortType eq 'name' ? 'selected' : '' }>이름순</option>
	               <c:if test="${searchVO.searchType eq 'student'}">
		               <option value="birth" ${searchVO.sortType eq 'birth' ? 'selected' : '' }>생년월일순</option>
		               <option value="class" ${searchVO.sortType eq 'class' ? 'selected' : '' }>소속순</option>
	               </c:if>
	            </select>
	            <input type="text" id="searchWrd" name="searchWrd" class="inp_s" value="${searchVO.searchWrd}" placeholder="${searchVO.searchType eq 'student' ? '학생 이름' : '교원명' }"/>
	            <input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
            </form>
      	</div>
      		<table id="resultTable" class="chart_board">
      		<c:choose>
      			<c:when test="${searchVO.searchType eq 'teacher'}">
      				<colgroup>
	                  <col width="5%" />
	                  <col width="15%" />
	                  <c:forEach items="${pieList}" var="result">
	                       <col width="*" />
	                   </c:forEach>
	                  <col width="*" />
	               </colgroup>
	               <thead>
	                  <tr>
	                      <th rowspan="2">no</th>
	                      <th rowspan="2">교원</th>
	                      <th colspan="${fn:length(pieList)}">글구분</th>
	                      <th rowspan="2">합계</th>
	                    </tr>
	                    <tr>
	                       <c:forEach items="${pieList}" var="result">
	                          <th>${result.ctgryNm }</th>
	                       </c:forEach>
	                    </tr>
	               </thead>
	               <tbody>
	               <c:if test="${not empty resultList }">
		               <c:forEach var="result" items="${resultList}" varStatus="status">
		               	<tr>
		               		<td>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
		               		<td>${result.userNm }</td>
		               		<c:set var="groupCnt" value="${fn:split(result.groupCnt,',') }"/>
		               		<c:forEach items="${groupCnt }" var="group">
		               			<td>${group }</td>
		               		</c:forEach>
		               		<td>${result.totCnt }</td>
		               	</tr>
		               </c:forEach>
	               </c:if>
	               </tbody>
      			</c:when>
      			<c:otherwise>
      				<colgroup>
	                  <col width="5%" />
	                  <col width="15%" />
	                  <col width="10%" />
	                  <col width="10%" />
	                  <col width="*" />
	                  <col width="*" />
	                  <c:forEach items="${pieList}" var="result">
	                       <col width="*" />
	                   </c:forEach>
	                  <col width="*" />
	                  <col width="10%" />
	                  <c:if test="${not empty attendCollect }">
	                   <col width="10%" />
	                  </c:if>
	               </colgroup>
	               <thead>
	                  <tr>
	                      <th rowspan="2">no</th>
	                      <th rowspan="2">소속</th>
	                      <th rowspan="2">이름</th>
	                      <th rowspan="2">생년월일</th>
	                      <th rowspan="2">학번</th>
	                      <th rowspan="2">학년</th>
	                      <th colspan="${fn:length(pieList)}">글구분</th>
	                      <th rowspan="2">합계</th>
	                      <c:if test="${not empty attendCollect }">
	                      	<th rowspan="2">총괄평가 기준 P/F</th>
	                      </c:if>
	                    </tr>
	                    <tr>
	                       <c:forEach items="${pieList}" var="result">
	                          <th>${result.ctgryNm }</th>
	                       </c:forEach>
	                    </tr>
	               </thead>
	               <tbody>
	               <c:if test="${not empty resultList }">
		               <c:forEach var="result" items="${resultList}" varStatus="status">
		               	<tr>
		               		<td>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
		               		<td>${result.stClass }</td>
		               		<td>${result.userNm }</td>
		               		<td>${result.birth }</td>
		               		<td>${result.stNumber }</td>
		               		<td>${result.stGrade }</td>
		               		<c:set var="groupCnt" value="${fn:split(result.groupCnt,',') }"/>
		               		<c:forEach items="${groupCnt }" var="group">
		               			<td>${group }</td>
		               		</c:forEach>
		               		<td><a href="javascript:fnBoardList('${result.userId }');">${result.totCnt }</a></td>
		               		<c:if test="${not empty attendCollect}">
		               			<td>${result.totCnt ge attendCollect ? 'PASS' : 'FAIL' }</td>
		               		</c:if>
		               	</tr>

		               </c:forEach>
	               </c:if>
	               </tbody>
      			</c:otherwise>
      		</c:choose>


            </table>

 			<c:if test="${not empty resultList }">
	         <div id="paging">
	            <c:url var="pageUrl" value="/mng/lms/manage/lmsBbsStatistics.do${_BASE_PARAM}">
	            </c:url>
	             <ul>
	               <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
	             </ul>
	         </div>
         </c:if>
         </div>
      </div>

   <br/><br/><br/><br/>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>