<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
	<%-- <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if> --%>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

	<link rel="stylesheet" href="/template/lms/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="/template/lms/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <!-- <link rel="stylesheet" href="/template/lms/css/common/base.css?v=1"> -->
  <link rel="stylesheet" href="/template/lms/css/common/common.css?v=1">
  <link rel="stylesheet" href="/template/lms/css/common/board.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/table.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/modal.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/class/class.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
  <script src="/template/lms/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="/template/lms/js/common.js?v=1"></script>

  <!--=================================================
          페이지별 스크립트
  ==================================================-->
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/locales-all.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/interaction/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.js?v=1"></script>

	<script>
	$(document).ready(function(){
		
		$('#btnRegist').click(function(){
			$('#layerPopup').show();
		});

		var calendarEl = document.getElementById('calendar');
		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
			header: {
			      left: 'prev,next today',
			      center: 'title',
			      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth',
			    },
			    locale: 'ko',
			    navLinks: true, // can click day/week names to navigate views
			    editable: true,
			    eventLimit: true, // allow "more" link when too many events
			    events: [
			    	<c:forEach var="result" items="${resultList}" varStatus="status">
			    		<c:set var="mySchYn" value="N"/>
			    		<c:forEach var="list" items="${facPlList}" varStatus="status">
				    		<c:if test="${list.crclId eq result.crclId}">
				    			<c:set var="mySchYn" value="Y"/>
				    		</c:if>
			    		</c:forEach>
				    		{
				            title: '${result.studySubject}',
				            start: new Date(<c:out value='${fn:substring(result.startDt, 0,4)}'/>, <c:out value='${fn:substring(result.startDt, 5, 7)}'/>-1, <c:out value='${fn:substring(result.startDt, 8,10)}'/>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">,<c:out value="${fn:substring(result.startTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">, <c:out value="${fn:substring(result.startTime, 2, 4)}"/></c:if>
				            		),
				            end: new Date(<c:out value='${fn:substring(result.endDt, 0,4)}'/>, <c:out value='${fn:substring(result.endDt, 5,7)}'/>-1, <c:out value='${fn:substring(result.endDt, 8,10)}'/>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">,<c:out value="${fn:substring(result.endTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">, <c:out value="${fn:substring(result.endTime, 2, 4)}"/></c:if>
				            		),
				            constraint: 'crclSchedule', // defined below
				            color: <c:choose><c:when test="${mySchYn eq 'Y'}">'#b7ca10'</c:when><c:otherwise>'#ff0000'</c:otherwise></c:choose>
				            , textColor: '#ffffff'
				            , url: '/mng/lms/manage/scheduleCalendarView.do?crclId=${result.crclId}&plId=${result.plId}&plType=crcl&crclLang=${curriculumVO.crclLang }'
				            <c:if test="${result.allDayAt eq 'Y'}">, allDay : true</c:if>
				          },
			    	</c:forEach>

			    ],
			    eventClick: function (arg) {
			      // opens events in a popup window
			    },
			}); 
		   
		calendar.render();

	});
	
	function checkForm(form) {	
		if($('#studySubject').val() == "" || $('#studySubject').val() == null){
			alert("일정명을 입력하세요.");
			$('#studySubject').focus();
			$('#layerPopup').show();
			return false;
			
		}else{
			<c:choose>
				<c:when test="${registerFlag eq '수정'}">
					if(confirm('<spring:message code="common.update.msg" />')) {
				</c:when>
				<c:otherwise>
					if(confirm('<spring:message code="common.regist.msg" />')) {
						$('#schduleForm').submit();
				</c:otherwise>
			</c:choose>
				return true;
			}else {
				return false;
			}
		}
	}
	</script>


	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="4"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<ul class="tab_menu mb50">
		<li><a href="/mng/lms/manage/schedule.do${_BASE_PARAM}&type=list">수업관리</a></li>
		<li><a href="/mng/lms/manage/scheduleCalendar.do${_BASE_PARAM}&type=cal" class="active">달력관리</a></li>
	</ul>
	<br/><br/>
	
	<button id="btnRegist" class="btn-sm font-400 btn-point mt-20" style="margin-bottom:20px;">${plTypeNm }일정 등록</button>
	
	<div class="page-content-wrap pt-0">
       <!-- 콘텐츠바디 -->
       <section class="page-content-body">
         <article class="content-wrap">
           <div id='calendar'></div>
         </article>
       </section>
     </div>
     
     
     	<c:set var="now" value="<%=new java.util.Date() %>"/>
		<c:set var="today"><fmt:formatDate value="${now }" pattern="yyyy-MM-dd"/></c:set>
        <!-- 팝업 -->
        <div id="layerPopup" class="alert-modal" style="display:none;">
		    <div class="modal-dialog modal-top">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">일정 등록</h4>
		          <button type="button" class="btn-modal-close btnModalClose"></button>
		        </div>
		        <form:form commandName="scheduleMngVO" id="schduleForm" name="scheduleMngVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/mng/lms/manage/insertCalendarSchedule.do">
		        <input type="hidden" id="crclId" name="crclId" value="${crclId }"/>
		        <input type="hidden" id="crclLang" name="crclLang" value="${curriculumVO.crclLang }"/>
		        <input type="hidden" id="plType" name="plType" value="crcl"/>
		        <div class="modal-body">
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정명 <span class="terms-necessary">*</span></dt>
		            <dd>
		              <input id="studySubject" name="studySubject" placeholder="일정명을 입력하세요."/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정 <span class="terms-necessary">*</span></dt>
		            <dd class="form-schedule-wrap">
		              <%-- <form:input path="startDt" value=""  class="ell date datepicker type2"/>
		              <i>~</i>
		              <form:input path="endDt" value="" class="ell date datepicker type2"/> --%>
		              <input   id="startDt" name="startDt" value="${today }"  class="ell date datepicker type2"/>
		              <!-- <i>~</i> -->
		              <input type="hidden" id="endDt" name="endDt" value="${today }" class="ell date datepicker type2"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">시간</dt>
		            <dd class="form-schedule-wrap schedule-time">
		              <select id="startTimeHH" name="startTimeHH" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              :
		               <select id="startTimeMM" name="startTimeMM" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              <i>~</i>
		              <select id="endTimeHH" name="endTimeHH" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              :
		               <select id="endTimeMM" name="endTimeMM" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              <label class="checkbox-img">
		                <input type="checkbox" id="allDayAt" name="allDayAt" value="Y"/>
		                <span class="custom-checked"></span>
		                <span class="text">종일</span>
		              </label>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">장소</dt>
		            <dd class="form-schedule-wrap schedule-place">
		              <select id="campusId" name="campusId" class="select2" data-select="style1" data-placeholder="캠퍼스 선택">
		                <option value="">전체</option>
						<c:forEach var="result" items="${sysCodeList}" varStatus="status">
							<option value="${result.ctgryId}" <c:if test="${'' eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
						</c:forEach>
		              </select>
		              <input id="placeDetail" name="placeDetail" class="mt-10" placeholder="상세위치정보"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap schedule-contents">
		            <dt class="form-title">
		              내용
		              <div class="text-typing"><span class="font-point textCount">0</span>/100</div>
		            </dt>
		            <dd class="form-schedule-wrap">
		              <textarea id="plCn" name="plCn" class="textTyping" maxlength="100" placeholder="내용을 입력해주세요."></textarea>
		            </dd>
		          </dl>
		        </div>
		        
		        <div class="modal-footer">
		          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
		          <button type="button" class="btn-xl btn-point btnModalConfirm" onclick="return checkForm(document.scheduleMngVO);">확인</button>
		        </div>
		        </form:form>
		      </div>
		    </div>
		  </div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>