<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
	<%-- <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if> --%>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

	<link rel="stylesheet" href="/template/lms/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="/template/lms/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <!-- <link rel="stylesheet" href="/template/lms/css/common/base.css?v=1"> -->
  <link rel="stylesheet" href="/template/lms/css/common/common.css?v=1">
  <link rel="stylesheet" href="/template/lms/css/common/board.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/table.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/modal.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/class/class.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
  <script src="/template/lms/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="/template/lms/js/common.js?v=1"></script>

  <!--=================================================
          페이지별 스크립트
  ==================================================-->
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/locales-all.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/interaction/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.js?v=1"></script>

<style>
body,input,button{font-size:12px;line-height:1.5em;font-family:"돋움",Dotum,Helvetica,AppleGothic,Sans-serif;color:#888888;}
</style>


<script>
$(document).ready(function(){
	var calendarEl = document.getElementById('calendar');
	var calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
		header: {
		      left: 'prev,next today',
		      center: 'title',
		      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth',
		    },
		    locale: 'ko',
		    navLinks: true, // can click day/week names to navigate views
		    editable: true,
		    eventLimit: true, // allow "more" link when too many events
		    events: [
		    	/* <c:forEach var="result" items="${resultList}" varStatus="status">
		    		{
		            title: '${result.title}',
		            start: new Date(<c:out value='${fn:substring(result.startDt, 0,4)}'/>, <c:out value='${fn:substring(result.startDt, 5, 7)}'/>-1, <c:out value='${fn:substring(result.startDt, 8,10)}'/>
		            		<c:if test="${result.startTime != null and result.startTime != ''}">,<c:out value="${fn:substring(result.startTime, 0, 2)}"/></c:if>
		            		<c:if test="${result.startTime != null and result.startTime != ''}">, <c:out value="${fn:substring(result.startTime, 2, 4)}"/></c:if>
		            		),
		            end: new Date(<c:out value='${fn:substring(result.endDt, 0,4)}'/>, <c:out value='${fn:substring(result.endDt, 5,7)}'/>-1, <c:out value='${fn:substring(result.endDt, 8,10)}'/>
		            		<c:if test="${result.endTime != null and result.endTime != ''}">,<c:out value="${fn:substring(result.endTime, 0, 2)}"/></c:if>
		            		<c:if test="${result.endTime != null and result.endTime != ''}">, <c:out value="${fn:substring(result.endTime, 2, 4)}"/></c:if>
		            		),
		            		constraint: <c:choose><c:when test="${plType eq 'all'}">'schoolSchedule'</c:when><c:when test="${plType eq 'ind'}">'personalSchedule'</c:when></c:choose>, // defined below
		            color: <c:choose><c:when test="${plType eq 'all'}">'#b7ca10'</c:when><c:when test="${plType eq 'ind'}">'#76828e'</c:when></c:choose> // 학교일정 컬러값
		            //, url: '/mng/sch/selectClassScheduleView.do?plId=${result.plId}'
		            <c:if test="${result.allDayAt eq 'Y'}">, allDay : true</c:if>
		          },
		    	</c:forEach> */

		    ],
		    eventClick: function (arg) {
		      // opens events in a popup window
		    }
		}); 
	   
	calendar.render();
	
	$('.btnModalClose').click(function(){
		$('#schduleForm').attr("action", "/mng/lms/manage/scheduleCalendar.do?crclId=${crclId}&type=cal");
		$('#schduleForm').submit();
	});
	
	$('.btnModalDelete').click(function(){
		$('#schduleForm').attr("action", "/mng/sch/deleteClassSchedule.do");
		$('#schduleForm').submit();
	});
	
	/* $('.btnModalCancel').click(function(){
		$('#schduleForm').attr("action", "/mng/sch/selectClassSchedule.do");
		$('#schduleForm').submit();
	}); */
});

function checkForm(form) {	
	if(confirm('<spring:message code="common.update.msg" />')) {
	$('#schduleForm').submit();
	return true;
}
}
</script>
<button id="btnRegist" class="btn-sm font-400 btn-point mt-20" style="margin-bottom:20px;">일정 등록</button>
	
		<div class="page-content-wrap pt-0">
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <article class="content-wrap">
              <div id='calendar'></div>
            </article>
          </section>
        </div>
 
        <c:set var="now" value="<%=new java.util.Date() %>"/>
		<c:set var="today"><fmt:formatDate value="${now }" pattern="yyyy-MM-dd"/></c:set>
        <!-- 팝업 -->
        <div id="layerPopup" class="alert-modal" style="display:block;">
		    <div class="modal-dialog modal-top">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">일정 수정</h4>
		          <button type="button" class="btn-modal-close btnModalClose"></button>
		        </div>
		        <form:form commandName="scheduleMngVO" id="schduleForm" name="scheduleMngVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/mng/lms/manage/updateCalendarSchedule.do">
		        <form:hidden path="crclId" value="${scheduleMngVO.crclId }"/>
		        <form:hidden path="crclLang" value="${crclLang }"/>
		        <form:hidden path="plId" value="${scheduleMngVO.plId }"/>
		        <form:hidden path="plType" value="crcl"/>
		        <div class="modal-body">
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정명 <span class="terms-necessary">*</span></dt>
		            <dd>
		             <form:input path="studySubject" placeholder="일정명을 입력하세요."/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정 <span class="terms-necessary">*</span></dt>
		            <dd class="form-schedule-wrap">
		              <%-- <form:input path="startDt" value=""  class="ell date datepicker type2"/>
		              <i>~</i>
		              <form:input path="endDt" value="" class="ell date datepicker type2"/> --%>
		              <form:input  path="startDt"   class="ell date datepicker type2"/>
		              <!-- <i>~</i> -->
		              <form:input type="hidden" path="endDt" class="ell date datepicker type2"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">시간</dt>
		            <dd class="form-schedule-wrap schedule-time">
		              <form:select path="startTimeHH" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(scheduleMngVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              :
		               <form:select path="startTimeMM" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="50" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(scheduleMngVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              <i>~</i>
		              <form:select path="endTimeHH" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(scheduleMngVO.endTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              :
		               <form:select path="endTimeMM" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="50" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(scheduleMngVO.endTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              <label class="checkbox-img">
		                <form:checkbox path="allDayAt" value="Y"/>
		                <span class="custom-checked"></span>
		                <span class="text">종일</span>
		              </label>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">장소</dt>
		            <dd class="form-schedule-wrap schedule-place">
		              <form:select path="campusId" class="select2" data-select="style1" data-placeholder="캠퍼스 선택">
		                <option value="">전체</option>
						<c:forEach var="result" items="${sysCodeList}" varStatus="status">
							<option value="${result.ctgryId}" <c:if test="${scheduleMngVO.campusId eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
						</c:forEach>
		              </form:select>
		              <form:input path="placeDetail" class="mt-10" placeholder="상세위치정보"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap schedule-contents">
		            <dt class="form-title">
		              내용
		              <div class="text-typing"><span class="font-point textCount">0</span>/100</div>
		            </dt>
		            <dd class="form-schedule-wrap">
		              <form:textarea path="plCn" class="textTyping" maxlength="100" placeholder="내용을 입력해주세요."></form:textarea>
		            </dd>
		          </dl>
		        </div>
		        
		        <div class="modal-footer">
		          <button type="button" class="btn-xl btn-outline-gray btnModalDelete">삭제</button>
		          <button type="button" class="btn-xl btn-point btnModalConfirm" onclick="return checkForm(document.scheduleMngVO);">확인</button>
		        </div>
		        </form:form>
		      </div>
		    </div>
		  </div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	
