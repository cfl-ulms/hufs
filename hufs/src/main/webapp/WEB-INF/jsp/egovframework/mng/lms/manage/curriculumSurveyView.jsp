<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="crclId" value="${param.crclId}"/>
</c:url>

<c:choose>
	<c:when test="${searchVO.mngAt eq 'Y'}">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CURRICULUM_MANAGE"/>
			<c:param name="depth1" value="BASE_CRCL_MNG"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="교육과정개설신청관리"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="${param.depth1 eq 'CRCL_SURVEY' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE'}"/>
			<c:param name="depth1" value="${param.depth1 eq 'CRCL_SURVEY' ? 'CRCL_SURVEY' : 'BASE_CRCL' }"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="${param.depth1 eq 'CRCL_SURVEY' ? '설문' : '과정등록관리'}"/>
		</c:import>
	</c:otherwise>
</c:choose>

<script>
$(document).ready(function(){
	$(".chartDiv").each(function (idx, el) {
	var totalCnt = 0;
	var myChart = echarts.init(el);

	var tempArr = $(this).closest(".queDiv").find("tr.answerClass");
	var dataMapArr = $.makeArray(tempArr.map(function(){
		if($(this).data("excnt") != 0){
	 		var tempObject = new Object();
	 		tempObject.value = $(this).data("excnt");
	  	 	tempObject.name = $(this).data("excn");
	  	 	totalCnt += $(this).data("excnt");
	  	 	return tempObject;
		}
	}));

	var dataArr = $.makeArray(tempArr.map(function(){
	 	return  $(this).data("excn");
	}));


	var option = {
			 color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		      animation: false,
		      legend: {
		        orient: 'vertical',
		        right: 0,
		        top: 30,
		        data: dataArr,
		        icon: 'rect',
		        itemGap: 20,
		        textStyle: {
		          fontSize: 13,
		          verticalAlign: 'middle',
		        },
		      },
		      series: [
		        {
	        		color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		          	type: 'pie',
		          	radius: '85%',
		          	label:false,
		          	center: ['35%', '50%'],
		          	hoverAnimation: false,
		          	animation: false,
		          	data: dataMapArr
		        }
		      ]
		    };

	myChart.setOption(option);
	$(this).closest(".queDiv").find(".totalCnt").text("응답("+totalCnt+")명");
	});
});
</script>

	<table class="chart2 mb50">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="9"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
			<c:param name="menu" value="${param.depth1 eq 'CRCL_SURVEY' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE'}"/>
			<c:param name="depth1" value="${param.depth1 eq 'CRCL_SURVEY' ? 'CRCL_SURVEY' : 'BASE_CRCL' }"/>
		</c:import>
	</c:if>

	<div style="border:1px solid #000;padding:21px;font-size:15px;color:#000;letter-spacing: 0.04em;text-align:center;">
		<c:choose>
			<c:when test="${not empty param.plId}">
				수업만족도 설문 | 수업주제 : ${curriculumAddInfo.studySubject }
			</c:when>
			<c:otherwise>
				<c:choose>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_1'}">
							<label>과정만족도 설문</label>
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_2'}">
							<label>수업만족도 설문</label>
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_3'}">
							<label>교원대상 과정만족도 설문</label>
						</c:when>
						<c:otherwise>
							<label>과정만족도 설문</label>
						</c:otherwise>
					</c:choose>
			</c:otherwise>
		</c:choose>
	</div>
	<br/>
	<strong>*교육과정 개요</strong>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>년도</label></th>
				<td><c:out value="${curriculumVO.crclYear }"/></td>
				<th><label>학기</label></th>
				<td><c:out value="${curriculumVO.crclTermNm}"/></td>
				<th><label>언어</label></th>
				<td><c:out value="${curriculumVO.crclLangNm}"/></td>
			</tr>
			<tr>
				<th><label>과정기간</label></th>
				<td colspan="3">
					<c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/>
				</td>
				<th><label>학점인정여부</label></th>
				<td colspan="2">
					<c:out value="${curriculumVO.gradeAt}"/>
				</td>
			</tr>
			<tr>
				<th><label>총 시간</label></th>
				<td><c:out value="${curriculumVO.totalTime}"/>시간</td>
				<th><label>주</label></th>
				<td><c:out value="${curriculumVO.weekNum}"/>회</td>
				<th><label>일</label></th>
				<td><c:out value="${curriculumVO.dayTime}"/>시간</td>
			</tr>
			<tr>
				<th><label>과정 진행 캠퍼스</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.campusNm}"/>
					(<c:out value="${curriculumVO.campusPlace}"/>)
				</td>
			</tr>
			<tr>
				<th><label>책임교수</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.hostCodeNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>과정 개요 및 목표</label></th>
				<td colspan="5">
					<c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/>
				</td>
			</tr>
			<tr>
				<th>
					<c:choose>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_1'}">
							<label>과정만족도<br/> 진행률</label>
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_2'}">
							<label>수업만족도<br/> 진행률</label>
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_3'}">
							<label>교원대상 과정만족도<br/> 진행률</label>
						</c:when>
						<c:otherwise>
							<label>과정만족도<br/> 진행률</label>
						</c:otherwise>
					</c:choose>
				</th>
				<td colspan="5">
				<fmt:formatNumber value="${curriculumAddInfo.joinCnt}" type="number" var="joinCnt" />
				<fmt:formatNumber value="${curriculumAddInfo.memberCnt}" type="number" var="memberCnt" />
					${ joinCnt} / ${memberCnt}
					<c:if test="${curriculumAddInfo.memberCnt ne '0' and joinCnt lt memberCnt}">
						(${memberCnt -  joinCnt}명 미제출)
					</c:if>
				</td>
			</tr>
		</tbody>
	</table>

	<br/>
	<div>
	<div style="border:1px solid #000;padding:21px;font-size:15px;color:#000;letter-spacing: 0.04em;text-align:center;">
		<c:choose>
			<c:when test="${not empty param.plId}">
				수업만족도
			</c:when>
			<c:otherwise>
				<c:choose>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_1'}">
							과정만족도
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_2'}">
							수업만족도
						</c:when>
						<c:when test="${curriculumAddInfo.schdulClCode eq 'TYPE_3'}">
							교원대상 과정만족도
						</c:when>
						<c:otherwise>
							과정만족도
						</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
			<c:if test="${curriculumAddInfo.sum ne 0 }">
             	<%-- <fmt:formatNumber var="resultPoint"  pattern="0" value="${(curriculumAddInfo.sum / curriculumAddInfo.joinCnt)*100}"/> --%>
             	<fmt:formatNumber var="resultPoint"  pattern="0" value="${curriculumAddInfo.sum}"/>
           	</c:if>
			:  ${curriculumAddInfo.sum eq 0  ? 0 : resultPoint}
	</div>
	<br>
		<c:if test="${not empty surveyAnswer }">
			<c:forEach items="${surveyAnswer}" var="result" varStatus="status">
				<div class="queDiv" style="float:left;width:100%">
					<div style="border-top:1px solid #000;padding:21px;background-color:#f9f9f9;font-size:15px;color:#000;letter-spacing: 0.04em;margin-bottom:15px">
						${status.count }. ${result.qesitmSj }
					</div>

					<c:choose>
						<c:when test="${result.qesitmTyCode eq 'multiple'}">
							<div>
		                    	<div class="chartDiv" style="width:49%;height:240px;float:left;border:1px solid #ddd;margin-right:10.5px;"></div>
		                  	</div>
							<div style="width:50%;float:left;">
								<table class="answerTable chart2 mb50" style="height:240px;">
									<colgroup>
										<col width="15%"/>
										<col width="*"/>
										<col width="15%"/>
									</colgroup>
									<tbody>
										<tr>
											<th>점수</th>
											<th>항목</th>
											<th class="totalCnt">응답()</th>
										</tr>
										<c:forEach items="${result.answerList}" var="answer"  varStatus="aStatus">
											<tr class="answerClass" data-excn="${answer.exCn }" data-excnt="${answer.cnt }">
												<td>${fn:length(result.answerList) - aStatus.index }</td>
												<td>${answer.exCn }</td>
												<td>${answer.cnt }</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:when>
						<c:when test="${result.qesitmTyCode eq 'answer'}">
							<div>
								<table class="chart2 mb50"">
									<colgroup>
				                      <col style='width:10%'>
				                      <col style='width:18%'>
				                      <col style='width:18%'>
				                      <col>
				                    </colgroup>
									<tbody>
										<tr>
											<th scope='col'>번호</th>
					                        <th scope='col'>응답자</th>
					                        <th scope='col'>전공</th>
					                        <th scope='col'>응답 내용</th>
										</tr>
										<c:forEach items="${result.essayList}" var="essay"  varStatus="eStatus">
											<tr>
						                        <td scope='row'>${eStatus.count}</td>
						                        <td>${essay.userNm }</td>
												<td>${essay.crclNm }</td>
												<td>${essay.cnsr }</td>
						                    </tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:when>
					</c:choose>

				</div>
			</c:forEach>
		</c:if>
	</div>
	<div class="btn_r">
		<c:choose>
			<c:when test="${param.depth1 eq 'CRCL_SURVEY'}">
				<c:url var="listUrl" value="/mng/lms/cla/classSurveyList.do">
					<c:if test="${not empty param.searchCtgryId}"><c:param name="searchCtgryId" value="${param.searchCtgryId}" /></c:if>
					<c:if test="${not empty param.searchCrclId}"><c:param name="searchCrclId" value="${param.searchCrclId}" /></c:if>
					<c:if test="${not empty param.searchSubmitN}"><c:param name="searchSubmitN" value="${param.searchSubmitN}" /></c:if>
					<c:if test="${not empty param.searchSubmitI}"><c:param name="searchSubmitI" value="${param.searchSubmitI}" /></c:if>
					<c:if test="${not empty param.searchSubmitF}"><c:param name="searchSubmitF" value="${param.searchSubmitF}" /></c:if>
					<c:if test="${not empty param.searchSchdulClCode}">
						<c:forEach items="${param.searchSchdulClCode}" var="result">
							<c:param name="searchSchdulClCode" value="${result}" />
						</c:forEach>
					</c:if>
					<c:if test="${not empty param.searchKeyWord}"><c:param name="searchKeyWord" value="${param.searchKeyWord}" /></c:if>
				</c:url>
			</c:when>
			<c:otherwise>
				<c:url var="listUrl" value="/mng/lms/manage/curriculumSurveyList.do${_BASE_PARAM}"/>
			</c:otherwise>
		</c:choose>
	    <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>


<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>