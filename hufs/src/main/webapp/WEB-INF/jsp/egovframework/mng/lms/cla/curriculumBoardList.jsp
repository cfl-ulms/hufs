<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCtgryId}"><c:param name="searchCtgryId" value="${searchVO.searchCtgryId}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclbId}"><c:param name="searchCrclbId" value="${searchVO.searchCrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCode}"><c:param name="searchProcessSttusCode" value="${searchVO.searchProcessSttusCode}" /></c:if>
	<c:if test="${not empty searchVO.searchSysType}">
		<c:forEach items="${searchVO.searchSysType}" var="result">
			<c:param name="searchSysType" value="${result}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty searchVO.searchKeyWord}"><c:param name="searchKeyWord" value="${searchVO.searchKeyWord}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CLASS_MANAGE"/>
	<c:param name="depth1" value="CRCL_BOARD"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정 게시판"/>
</c:import>
<style>
.backGray{
	background : #ccc;
}

</style>
<script>
$(document).ready(function(){

	/* $("#searchCtgryId, #searchCrclbId").change(function(){
		var tempCtgryId = $("select[id=searchCtgryId]").val();
		var tempCrclbId = $("select[id=searchCrclbId]").val();
		initCurriculum(tempCtgryId,tempCrclbId );
	}); */


	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
      	$("#searchStartDate").val("");
      	$("#searchEndDate").val("");
		$("input:checkbox").attr("checked", false);
		$("#searchKeyWord").val("");
		/* initCurriculum("",""); */
	});

	$("#searchStartDate, #searchEndDate").datepicker({
		dateFormat: "yy-mm-dd"
	 });

	$("#searchTodayClass").on("click",function(){
		if($(this).prop("checked")){
			var now = new Date();
		    var year = now.getFullYear();
	      	var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
	      	var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
	      	var chan_val = year + '-' + mon + '-' + day;
	      	$("#searchStartDate").val(chan_val);
	      	$("#searchEndDate").val(chan_val);
		}
	});
});

</script>

<div id="cntnts">
<c:set var="now" value="<%=new java.util.Date()%>" />
	<div id="bbs_search">
	  	<form id="frm" method="post" action="<c:url value="/mng/lms/cla/curriculumBoardList.do"/>">
			<strong>주관기관 : </strong>
	  		<select id="searchCtgryId" name="searchCtgryId">
				<option value="">전체</option>
				<c:forEach var="result" items="${ctgryList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1' }">
						<option value="${result.ctgryId}" ${searchVO.searchCtgryId eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>
	  		<strong>전체기본과정 : </strong>
	  		<select id="searchCrclbId" name="searchCrclbId">
				<option value="">전체</option>
				<c:forEach var="result" items="${curriculumBaseList}" varStatus="status">
					<option value="${result.crclbId}" ${searchVO.searchCrclbId eq result.crclbId ? 'selected' : '' }><c:out value="${result.crclbNm}"/></option>
				</c:forEach>
			</select>
			&nbsp;&nbsp;
			<strong>과정기간 : </strong>
			<%-- <c:set var="nowDate"><fmt:formatDate value="${now}" pattern="yyyy-MM-dd" /></c:set> --%>
			<input type="text" name="searchStartDate" value="${empty searchVO.searchStartDate ? '' : searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~
			<input type="text" name="searchEndDate" value="${empty searchVO.searchEndDate ? '' : searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<br/>
			<strong>과정상태 : </strong>
	  		<select id="searchProcessSttusCode" name="searchProcessSttusCode">
				<option value="">전체</option>
				<c:forEach var="result" items="${statusComCode}" varStatus="status">
					<option value="${result.code}" ${searchVO.searchProcessSttusCode eq result.code ? 'selected' : '' }>${result.codeNm}</option>
				</c:forEach>
			</select>
			&nbsp;&nbsp;
			<strong>과정명: </strong>
			<input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm }" class="inp_s" id="searchCrclNm" />
			<br/>
			<strong>게시판 사용자 그룹 : </strong>
			<c:forEach items="${searchVO.searchSysType}" var="result">
				<c:choose>
					<c:when test="${result eq 'ALL' }">
						<c:set var="searchSysType1" value="ALL"/>
					</c:when>
					<c:when test="${result eq 'GROUP' }">
						<c:set var="searchSysType2" value="GROUP"/>
					</c:when>
					<c:otherwise>
						<c:set var="searchSysType3" value="CLASS"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
	  		<label><input type="checkbox" name="searchSysType" value="ALL" ${searchSysType1 eq 'ALL' ? 'checked' : '' }/>전체</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="searchSysType" value="GROUP" ${searchSysType2 eq 'GROUP' ? 'checked' : '' }/>조별</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="searchSysType" value="CLASS" ${searchSysType3 eq 'CLASS' ? 'checked' : '' }/>반별</label>
			<br/>
	  		<label><strong>게시판 명 : </strong> <input type="text" name="searchKeyWord" value="${searchVO.searchKeyWord }" class="inp_s" id="searchKeyWord" /></label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;"/>
		</form>
	</div>
	<div>
		<h2>총 ${boardListCnt}개</h2>
	</div>
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co4"/>
		</colgroup>
	    <thead>
	      <tr>
	        <th>No</th>
	        <th>기본과정</th>
	        <th>주관기관</th>
	        <th>과정상태</th>
	        <th>과정명</th>
	        <th>과정기간</th>
	        <th>사용자 그룹</th>
	        <th>게시판 명</th>
	        <th>최근 게시물</th>
	        <th>등록일</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<c:choose>
	    		<c:when test="${not empty boardList}">
			    	<c:forEach var="result" items="${boardList}" varStatus="status">
				    	<c:url var="viewUrl" value="/mng/lms/manage/lmsBbsView.do?">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:param name="bbsId" value="${result.bbsId}"/>
							<c:param name="nttNo" value="${result.nttNo}"/>
							<c:param name="depth1" value="CRCL_BOARD"/>
							<c:param name="menu" value="CLASS_MANAGE"/>
							<c:param name="pageIndex" value="1"/>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'">
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<td>${result.crclbNm}</td>
			    			<td>${result.ctgryNm}</td>
			    			<td>${result.codeNm }</td>
			    			<td>${result.crclNm}</td>
			    			<td>${result.startDate} ~ ${result.endDate}</td>
			    			<td>${result.sysTyCode eq 'ALL' ? '전체' : result.sysTyCode eq 'GROUP' ? '조별' : '반별'}</td>
			    			<td>(${result.sysTyCode eq 'ALL' ? '전체' : result.sysTyCode eq 'GROUP' ? '조별' : '반별'}) ${result.bbsNm}</td>
			    			<td>${result.nttSj}</td>
			    			<td>${result.frstRegisterPnttm}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</tbody>
    </table>

 	 <div id="paging">
		<div id="paging">
	    	<c:url var="pageUrl" value="/mng/lms/cla/curriculumBoardList.do${_BASE_PARAM}">
	    </c:url>
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl }" /></ul>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
