<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
</c:url>
<% /*URL 정의*/ %>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${empty param.menu ? 'CURRICULUM_MANAGE' : param.menu }"/>
	<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? 'CURRICULUM_STUDY' : 'BASE_CRCL'  }"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.menu eq 'CLASS_MANAGE' ? '' : '평가'}"/>
</c:import>
<script>
$(document).ready(function(){

});
</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm}"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.menu eq 'CLASS_MANAGE' ? '3' : '5'}"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<br/>

<article class="content-wrap">
              <div class="no-content-wrap">
                <p class="sub-title">
                  온라인 퀴즈를 신규로 등록해주세요.<br>
                  온라인 퀴즈를 기존에 등록하여 수업하신 적이 있으시다면 [나의 지난 퀴즈 불러오기] 버튼을 누르시면<br>
                  과정별로 등록되었던 퀴즈 이력을 확인하여 다시 사용하실 수 있습니다.
                </p>
                <div class="mt-30">
                  <a href="#" class="btn-xl btn-point btnModalOpen" data-modal-type="no_contents_quiz">온라인 퀴즈 신규 등록하기</a>
                  <a href="#" class="btn-xl btn-outline btnModalOpen" data-modal-type="before_quiz">나의 지난 퀴즈 불러오기</a>
                </div>
              </div>
            </article>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>