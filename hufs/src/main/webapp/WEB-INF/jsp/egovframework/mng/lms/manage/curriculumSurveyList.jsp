<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="crclId" value="${param.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.menu eq 'CLASS_MANAGE' ? '설문' : '과정등록관리' }"/>
</c:import>

<script>
function fnSelectMember(schdulId, schdulClCode, plId, crclId){
	window.open("/mng/lms/manage/curriculumSurveyMember.do?schdulId="+schdulId+"&schdulClCode="+schdulClCode+"&plId="+plId+"&crclId="+crclId,"selectMember","height=500, width=485, top=50, left=20, scrollbars=yes, resizable=no");
}

</script>

<style>
tr.success{
	background:#bfbfbf
}

</style>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="9"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>

	<table class="chart_board">
		<thead>
			<tr>
			  	<th>번호</th>
			  	<th>설문구분</th>
			 	<th>수업주제</th>
				<th>설문 제출 시작일</th>
				<th>제출현황</th>
				<th>만족도</th>
		  	</tr>
		</thead>
		<tbody>
		<c:choose>
			<c:when test="${not empty surveyCurriculumList }">
				<c:forEach var="result" items="${surveyCurriculumList}" varStatus="status">
					<tr class="${result.memberCnt ne 0 and result.joinCnt eq result.memberCnt ? 'success' : ''}">
						<td>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
						<c:url var="viewUrl" value="/mng/lms/manage/curriculumSurveyView.do">
							<c:param name="schdulId" value="${result.schdulId}"/>
							<c:param name="schdulClCode" value="${result.schdulClCode}"/>
							<c:param name="crclId" value="${param.crclId}"/>
							<c:if test="${result.schdulClCode eq 'TYPE_2' }">
								<c:param name="plId" value="${result.id}"/>
							</c:if>
						</c:url>

						<td><a href="${viewUrl}">${result.type }</a></td>
						<td>${result.studySubject }</td>
						<td>${result.endDate }</td>

						<fmt:formatNumber value="${result.joinCnt}" type="number" var="joinCnt" />
						<fmt:formatNumber value="${result.memberCnt}" type="number" var="memberCnt" />
						<c:choose>
							<c:when test="${ memberCnt ne 0 and result.joinCnt lt result.memberCnt}">
								<c:if test="${result.schdulClCode eq 'TYPE_2' }">
									<c:set var="plId" value="${result.id}"/>
								</c:if>
								<td><a href="#none" onclick="fnSelectMember('${result.schdulId}', '${result.schdulClCode }', '${plId }' , '${param.crclId}')">${result.joinCnt}/${result.memberCnt }</a></td>
							</c:when>
							<c:otherwise>
								<td>${result.joinCnt}/${result.memberCnt}</td>
							</c:otherwise>
						</c:choose>
						
						<c:if test="${result.sum ne 0 }">
							<c:choose>
								<c:when test="${result.schdulClCode eq 'TYPE_1' or result.schdulClCode eq 'TYPE_3'}">
									<fmt:formatNumber var="resultPoint"  pattern="0.00" value="${result.sum}"/>
								</c:when>
								<c:otherwise>
									<fmt:formatNumber var="resultPoint"  pattern="0" value=" ${(result.sum / result.joinCnt)*100} " />
								</c:otherwise>
							</c:choose>
						</c:if>
						<td> ${result.sum eq 0 ? '-' :  resultPoint}</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="6" class="alC">검색 결과가 없습니다.</td>
				</tr>
			</c:otherwise>
		</c:choose>
		</tbody>
	</table>

	 <c:if test="${not empty surveyCurriculumList }">
		<div id="paging">
		    <c:url var="pageUrl" value="/mng/lms/manage/curriculumSurveyList.do${_BASE_PARAM}"/>
		    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
		</div>
	</c:if>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>