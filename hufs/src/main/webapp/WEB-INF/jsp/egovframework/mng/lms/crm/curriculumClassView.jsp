<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<%--수강대상자 확정 승인 --%>
	<c:when test="${param.menuCode eq 'confm' }">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG_CONFIRM"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강대상자 확정 승인"/>
			<c:param name="menuCode" value="${param.confm }"/>
		</c:import>
	</c:when>
	<c:when test="${param.menuCode eq 'student' }">
       <%-- 과정등록관리 > 학생 --%>
    </c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강신청"/>
		</c:import>
	</c:otherwise>
</c:choose>

<script>
alert("준비 중입니다.");
history.back();

$(document).ready(function(){
	//반배정 취소
	$(document).on("click", ".class_cancle_btn", function(){
		location.href="/mng/lms/crm/curriculumClassRegister.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}";
	});
});

//조 수량 체크 함수
function fn_class_check() {
	var classCnt = $(".class_cnt").val();
	var flag = true;
	
	if(classCnt =="" || classCnt == "0") {
		alert("조 수량을 설정해 주세요.")
		flag = false;
	}
	return flag;
}

//담당교수 검색 autocomplate option
function fn_manage_search_opion() {
	var option = {
		source : function(request, response){
			$.ajax({
				type:"post",
   	          	dataType:"json",
   	          	url:"/mng/usr/EgovMberManage.json",
   	          	data:{userSeCode : "08", searchUserNm : $(this.element).val()},
   	          	success:function(result){
   	          		response($.map(result.items, function(item){     //function의 item에 data가 바인딩된다.
   	            		return{
	       	             label:item.userNm + "("+item.userId+")",
	       	             value:item.userId,
	       	             userNm:item.userNm
   	            		}
   	           		}));
   	          	},
   	          	error: function(){
   	          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
   	          }
			})
		},
   	    matchContains:true,
   	    selectFirst:false,
   	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
   	    delay:100,                 //milliseconds
   	    select:function(event,ui){ //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
   	    	$(this).prevAll(".manageIdList").val(ui.item.value);
   	    	$(this).val(ui.item.userNm);
   	    	return false;
   	    },
   	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	};

	return option;
}

//담당교수 검색 autocomplate option
function fn_student_search_opion() {
	var option = {
		source : function(request, response){
			$.ajax({
				type:"post",
		          	dataType:"json",
		          	url:"/mng/lms/crm/studentJson.json",
		          	data:{crclId : "${curriculumVO.crclId}", searchUserNm : $(this.element).val()},
		          	success:function(result){
		          		response($.map(result.items, function(item){
		          			var userName = "";

                            if(item.mngDeptNm == null) {
                                userName = item.userNm;
                            } else {
                                userName = item.userNm + "("+item.mngDeptNm+")";
                            }
                            return{
                            label:userName,
		       	            value:item.userId,
		       	            userNm:item.userNm
		            		}
		           		}));
		          	},
		          	error: function(){
		          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
		            }
			})
		},
	    matchContains:true,
	    selectFirst:false,
	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
	    delay:100,                 //milliseconds
	    select:function(event,ui){ //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
	    	var html        = "";
	    	var alreadyFlag = true;
	    	
	    	html += "<div class='div_student_box' data-userid='" + ui.item.value + "'>";
			html += "    <input type='hidden' name='userIdList' value='" + ui.item.value + "'>";
			html += "    <input type='hidden' name='classCntList' value='" + $(this).data("classcnt") + "'>";
			html += "    <span>" + ui.item.userNm + " | </span>";
			html += "    <a href='#none' class='student_delete_btn'>X</a>";
			html += "</div>";

	    	$(".div_student_box").each(function(){
	    		if($(this).data("userid") == ui.item.value){
	    			alert("이미 등록 된 학생 입니다.");
	    			alreadyFlag = false;

	    			return false;
	    		} 
	    	});
	    	
	    	if(alreadyFlag) {
	    		$(this).parent().next().append(html);
	    	}

	    	return false;
	    },
	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	};

	return option;
}
</script>
<%-- 과정등록관리 > 학생에서 import할 때 상단 table 안나오도록 처리 --%>
<c:if test="${param.menuCode ne 'student' }">
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>
</c:if>

<c:if test="${empty param.menuCode}">
	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="4"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
	</c:import>
</c:if>

<c:import url="/mng/lms/curriculumStudenttabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="3"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<div>
	<div class="student_pick_list">
		<form id="listForm" method="post" action="/mng/lms/crm/updateCurriculumClass.do">
			<table class="tbl_student_pick">
				<colgroup>
					<col width='80px' />
					<col width='200px' />
					<col width='200px' />
					<col width='*' />
				</colgroup>
				<tbody>
					<tr class="pick_table_header">
						<th>분반</th>
						<th>담당교수</th>
						<th>학생수</th>
						<th>배정된 학생 명단</th>
					</tr>
					
					<c:forEach var="curriculumClass" items="${curriculumClassList}" varStatus="status">
						<tr class="tr_student_list">
							<td>${curriculumClass.classCnt }반</td>
							<td class='td_manage'>${curriculumClass.manageNm }</td>
							<td>${curriculumClass.classUserCnt }명</td>
							<td class="td_student_list">
								<c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status2">
									<c:if test="${pickStudent.classCnt eq curriculumClass.classCnt }">
										<div class="div_student_box">
											<span>${pickStudent.userNm }</span>
										</div>
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</div>

<div class="btn_c">
	<c:choose>
	 	<c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
	 	</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<a href="#none" class="btn_mngTxt class_cancle_btn">반배정 취소</a>
		</c:when>
	</c:choose>

    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>