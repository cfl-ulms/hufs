<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="schdulClCode" value="${param.schdulClCode}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="SURVEY_MANAGE"/>
	<c:param name="depth2" value="${param.schdulClCode}"/>
	<c:param name="title" value="${param.schdulClCode eq 'TYPE_1' ? '과정만족도 조사' : '수업만족도 조사' }"/>
</c:import>

<script type="text/javascript">

function fnDel(schdulId){
	if(!confirm("해당 설문을 삭제 하시겠습니까? ")){
		return false;
	}
		
	var tempSchdulClCode = "${param.schdulClCode}";
	$.ajax({
		data : {"schdulId": schdulId},
		dataType : "json",
		type : "post",
		url : "/mng/lms/sur/deleteSurvey.json",
		success:function(data){
			var resultCode = data.resultCode;
			if(resultCode == "Ex001"){
				alert("삭제 완료하였습니다.");
				location.href = "/mng/lms/sur/surveyManageList.do?schdulClCode="+tempSchdulClCode;
			}else{
				alert("변경에 실패했습니다. 관리자에게 문의해주세요.");
				return false;
			}
		}
	});
}

function fnView(){
	location.href=""
}
</script>

<div id="cntnts">
	<table class="chart_board">
	    <colgroup>
			<col width="15%"/>
			<col width="30%"/>
			<col width="20%"/>
			<col width="20%"/>
			<col width="15%"/>
		</colgroup>
	    <caption class="hdn">${param.schdulClCode eq 'TYPE_1' ? '과정만족도 조사' : '수업만족도 조사' }</caption>
	    <thead>
	      <tr>
	        <th>No.</th>
	        <th>${param.schdulClCode eq 'TYPE_1' ? '과정만족도' : '수업만족도' } 설문명</th>
	        <th>설문 문항 수</th>
	        <th>사용여부</th>
	        <th>관리</th>
	      </tr>
	    </thead>
	    <tbody>
			<c:choose>
				<c:when test="${not empty surveyList }">
					<c:forEach items="${surveyList}" var="result" varStatus="status">
						<tr>
							<td>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
							<c:url var="viewUrl" value="/mng/lms/sur/surveyManage.do">
								<c:param name="schdulId" value="${result.schdulId}"/>
								<c:param name="actionKey" value="view"/>
								<c:param name="schdulClCode" value="${param.schdulClCode}"/>
							</c:url>
							<c:url var="updateUrl" value="/mng/lms/sur/surveyManage.do">
								<c:param name="schdulId" value="${result.schdulId}"/>
								<c:param name="actionKey" value="update"/>
								<c:param name="schdulClCode" value="${param.schdulClCode}"/>
							</c:url>
							<td>
								<c:choose>
									<c:when test="${result.currUseCnt eq 0}">
										<a href="${updateUrl}">${result.schdulNm}</a>
									</c:when>
									<c:otherwise>
										<a href="${viewUrl}">${result.schdulNm}</a>
									</c:otherwise>
								</c:choose>
							</td>
							<td>${result.questionCnt }</td>
							<td>${result.currUseCnt > 0 ? '사용중':'사용안함' }</td>
							<td>
								<c:choose>
									<c:when test="${result.currUseCnt eq 0}">
										<a href="#none" onclick="fnDel('${result.schdulId}')"><img src="${_IMG}/btn/del.gif"/></a>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
		    	</c:when>
		    	<c:otherwise>
					<tr>
			        	<td class="listCenter" colspan="5"><spring:message code="common.nodata.msg" /></td>
			      	</tr>
			    </c:otherwise>
			</c:choose>
		</tbody>
    </table>

	<div class="btn_r">
    	<c:url var="addUrl" value="/mng/lms/sur/surveyManage.do">
			<c:param name="schdulClCode" value="${param.schdulClCode}"/>
			<c:param name="actionKey" value="insert"/>
		</c:url>
		<a href="${addUrl}"><img src="${_IMG}/btn/btn_creat.gif" alt="생성"/></a>
	</div>

    <c:if test="${not empty surveyList }">
		<div id="paging">
		    <c:url var="pageUrl" value="/mng/lms/sur/surveyManageList.do${_BASE_PARAM}">
		    </c:url>

		    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
		</div>
	</c:if>

</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>