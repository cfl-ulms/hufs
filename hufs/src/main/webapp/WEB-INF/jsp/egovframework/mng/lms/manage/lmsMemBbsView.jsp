<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/lms/manage"/>
<c:set var="_ACTION" value=""/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/default.css'/>"/>
<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/page.css?v=1'/>"/>
<link type="text/css" rel="stylesheet" href="<c:url value='/template/manage/css/com.css'/>"/>
<link type="text/css" href="<c:url value='/template/common/js/jquery/themes/base/jquery.ui.all.css'/>" rel="stylesheet" />
<link type="text/css" href="<c:url value='/template/common/js/jquery/css/jquery.timepicker.css'/>" rel="stylesheet" />
<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery-1.9.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery-ui.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery.timepicker.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/template/common/js/jquery/ui/i18n/jquery.ui.datepicker-ko.js'/>" charset="utf-8"></script>
<script type="text/javascript" src="<c:url value='/template/common/js/common.js'/>"></script>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty searchVO.bbsId}"><c:param name="bbsId" value="${searchVO.bbsId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>


<script type="text/javascript">
function fn_egov_delete_notice(url) {

	if (confirm('<spring:message code="common.delete.msg" />')) {
		document.location.href = url;
	}
}

	$(document).ready(function(){

	});
</script>

<br/>

<div id="cntnts">
	<div style="text-align:center;width:100%;height:100px;">
		<h1 style="font-size:20px;text-align:center;">${masterVo.sysTyCode eq 'CLASS' ? '(반 별)' : masterVo.sysTyCode eq 'GROUP' ? '(조 별)' : ''} ${masterVo.bbsNm} 게시판 통계</h1>

	</div>
	<div>
		<c:if test="${not empty attendCollect and  paginationInfo.totalRecordCount ge attendCollect}">
		<div style="float:left;border:1px solid blue; width:100px;font-size:14pt;text-align:center;height:20px;">
			${board.totCnt ge attendCollect ? 'PASS' : 'FAIL'}
		</div>
		</c:if>
		<div style="float:right;">
			<h3>${board.department } ${board.ntcrNm}</h3> <br/>
			<p>게시물 총 ${board.totCnt } 건 <c:if test="${not empty attendCollect}"> / ${attendCollect } 이상 Pass </c:if>

		</div>
	</div>

	<div class="view_wrap">
		<dl class="view_tit">
			<dt><spring:message code="cop.nttSj" /></dt>
			<dd><strong><c:out value="${board.nttSj}" /></strong></dd>
		</dl>

		<table  class="view_writer_chart">
			<caption></caption>
			<colgroup>
				<col width="110px" />
				<col width="*" />
				<col width="110px" />
				<col width="*" />
			</colgroup>
			<tbody>
                <c:if test="${!empty brdMstrVO.ctgrymasterId}">
	                <tr>
		                <th>게시글 구분</th>
		                <td colspan="3" class="last"><c:out value="${board.ctgryNm}" /></td>
	                </tr>
                </c:if>
                <tr>
	                <th><spring:message code="cop.ntcrNm" /></th>
	                <td colspan="3" class="last"><c:out value="${board.ntcrNm}" /> (<c:out value="${board.frstRegisterId}" />)</td>
                </tr>
                <tr>
                	<th><spring:message code="cop.frstRegisterPnttm" /></th>
	                <td><fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                <th><spring:message code="cop.inqireCo" /></th>
	                <td class="last"><c:out value="${board.inqireCo}" /></td>
                </tr>
 			</tbody>
		</table>
         <div class="view_cont">
			<c:out value="${board.nttCn}" escapeXml="false" />
		</div>

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="view_tit02">
				<dt><spring:message code="cop.atchFileList" /></dt>
				<dd>
					<ul class="list">
						<c:if test="${not empty board.atchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
								<c:param name="mngAt" value="Y"/>
							</c:import>
						</c:if>
					</ul>
				</dd>
			</dl>
		</c:if>
	</div>

	<div class="btn_r">
		<c:url var="prevUrl" value="/mng/lms/manage/lmsMemBbsView.do">
      		<c:param name="crclId" value="${param.crclId}" />
      		<c:param name="bbsId" value="${board.bbsId}" />
      		<c:param name="frstRegisterId" value="${board.frstRegisterId}" />
		</c:url>
		<a href="/mng/lms/manage/lmsBbsList.do${_BASE_PARAM}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
		<c:if test="${not empty board.prevNo}">

			<c:url var="prevUrl" value="/mng/lms/manage/lmsMemBbsView.do">
	      		<c:param name="nttNo" value="${board.prevNo}" />
	      		<c:param name="bbsId" value="${board.bbsId}" />
			</c:url>
			<input type="button" id="resetBtn" onclick="location.href='${prevUrl}'" value="이전글" style="width:73px;height:26px;">
		</c:if>
		<c:if test="${not empty board.nextNo}">
			<c:url var="nextUrl" value="/mng/lms/manage/lmsMemBbsView.do">
	      		<c:param name="nttNo" value="${board.nextNo}" />
		  		<c:param name="bbsId" value="${board.bbsId}" />
			</c:url>
			<input type="button" id="resetBtn" onclick="location.href='${nextUrl}'" value="다음글" style="width:73px;height:26px;">
		</c:if>

	</div>
</div>
