<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:set var="menu" value="SCHD_MANAGE"/>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${menu}"/>
	<c:param name="depth1" value="CAMPUS_LIST"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="일정관리"/>
</c:import>

<script>
$(document).ready(function(){
	/*
	fnChangeTime(); // 수정하기 진입 시 첫 화면에 시간 계산해서 뿌려줌
	
	// 1교시 시작시간 selectbox 변경 시 나머지 교시 시간 계산해서 뿌려줌
	$(".firstTime").change( function() {
		fnChangeTime();
	});
	*/
});

// 시간 계산
function fnChangeTime(){
 	var hour = $('#selectHour').val(); 
	var min = $('#selectMin').val();
	var stHour = hour;
	var stMin = min;
	var endHour = hour;
	var endMin = min;
	
	// 종료 시간 계산 (50분)
	endMin = parseInt(endMin, 10) + 50;
	if(endMin > 59){
		endMin = parseInt(endMin, 10) - 60;
		if(endMin == 0){
			endMin = "00";
		}
		endHour = parseInt(hour, 10) + 1;
	}

	for(var i=1; i<=12; i++){
		$('#stTime_'+i).text(stHour + ":" + stMin);
		// 시작 시간 계산 (1시간)
		stHour = parseInt(stHour, 10) + 1;
		
		
		$('#endTime_'+i).text(endHour + ":" + endMin);
		
		endHour = parseInt(endHour, 10) + 1;
		if(endHour < 10){
			endHour = "0"+endHour;
		}
		if(stHour < 10){
			stHour = "0"+stHour;
		}
	}
}
 
function fnModBtn(){ // 수정하기 버튼 클릭
	if(${fn:length(campusVO.campusId) eq 0}){
		alert("캠퍼스 구분을 선택해주세요.");
		$("#campusId").foucus();
		return false;
	}else{
		$('#main').hide();
		$('#btn_modify').hide();
		
		$('#main_text').show();
		$('#btn_save').show();
	}
}

function fnSaveBtn(){ // 저장하기 버튼 클릭
	$("#form").submit();
	/*
	var endHour = $('#selectHour').val();
	var endMin = $('#selectMin').val();
	var startTime = $('#selectHour').val() + "" + $('#selectMin').val();
	var endTime = "";
	
	// 종료 시간 계산 (50분)
	endMin = parseInt(endMin, 10) + 50;
	if(endMin > 59){
		endMin = parseInt(endMin, 10) - 60;
		if(endMin == 0){
			endMin = "00";
		}
		endHour = parseInt(endHour, 10) + 1;
	}
	endTime = endHour + "" + endMin;
	
	$.ajax({
		type:"post",
		dataType:"json",
		url:"/mng/sch/insertCampusSchedule.json",
		data:"campusId="+$("#campusId").val()+"&startTime="+startTime+"&endTime="+endTime,
		success: function(data) {
			$('#frm').attr("action","/mng/sch/selectCampusSchedule.do");
			$('#frm').submit();
		},
		error:function() {
			//alert("error");
		}
	});
	*/
}
</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form id="frm"name="frm" method="gets" action="<c:url value="/mng/sch/selectCampusSchedule.do"/>">
		<select id="campusId" name="campusId">
			<option value="">선택</option>s
			<c:forEach var="result" items="${sysCodeList}" varStatus="status">
				<c:if test="${result.ctgryLevel eq '1'}">
					<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq campusVO.campusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
				</c:if>
			</c:forEach>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" style="margin-left:20px;" />
		</select>
	</form>
	</div>
</div> 

<div class="btn_r">
	<a href="javascript:fnModBtn();"><input id="btn_modify" type="image" src="/template/manage/images/btn/btn_modify.gif"></a>
	<a href="javascript:fnSaveBtn();"><input id="btn_save" type="image" src="/template/manage/images/btn/btn_save.gif" style="display:none;"></a>
</div>

<form id="form" name="form" method="post" action="/mng/sch/insertCampusSchedule.do">
<input type="hidden" name="campusId" value="${campusVO.campusId}"/>
<table class="chart_board">
    <colgroup>
    	<col class="co6"/>
    	<col class="co6"/>
		<col class="co6"/>
	</colgroup>
    <thead>
      <tr>
      	<th>교시</th>
        <th>시작시간</th>
        <th>종료시간</th>
      </tr> 
    </thead>
    <tbody id="main">
    	<c:forEach var="result" items="${resultList}" varStatus="status">
    		<tr>
    			<td><c:out value="${result.period}" /></td>
    			<td><c:out value="${fn:substring(result.startTime, 0, 2)}"/>:<c:out value="${fn:substring(result.startTime, 2, 4)}"/></td>
    			<td><c:out value="${fn:substring(result.endTime, 0, 2)}"/>:<c:out value="${fn:substring(result.endTime, 2, 4)}"/></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) eq 0}">
			<tr>
	        	<td class="listCenter" colspan="3"><spring:message code="common.nodata.msg" /></td>
	      	</tr>
	    </c:if>
	</tbody>
	<!-- 수정하기 -->
	<tbody id="main_text" style="display:none;">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
	   			<td><c:out value="${result.period}" /></td>
	   			<td>
	   				<select class="firstTime" name="startTimeHHList">
				      <c:forEach var="item" varStatus="i" begin="8" end="24" step="1">
				       <option value="<c:if test="${item < 10}">0</c:if>${item}" <c:if test="${item eq fn:substring(result.startTime, 0, 2)}">selected="selected"</c:if>>
				        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
				       </option>
				      </c:forEach>
				    </select>
				    :
				    <select class="firstTime" name="startTimeMMList">
				      <option value="00">00</option>
				      <c:forEach var="item" varStatus="i" begin="10" end="50" step="10">
				       <option value="<c:if test="${item < 10}">0</c:if>${item}" <c:if test="${item eq fn:substring(result.startTime, 2, 4)}">selected="selected"</c:if>>
				        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
				       </option>
				      </c:forEach>
				     </select>
	   			</td>
	   			<td>
	   				<select class="endTime" name="endTimeHHList">
				      <c:forEach var="item" varStatus="i" begin="8" end="24" step="1">
				       <option value="<c:if test="${item < 10}">0</c:if>${item}" <c:if test="${item eq fn:substring(result.endTime, 0, 2)}">selected="selected"</c:if>>
				        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
				       </option>
				      </c:forEach>
				    </select>
				    :
				    <select class="endTime" name="endTimeMMList">
				      <option value="00">00</option>
				      <c:forEach var="item" varStatus="i" begin="10" end="50" step="10">
				       <option value="<c:if test="${item < 10}">0</c:if>${item}" <c:if test="${item eq fn:substring(result.endTime, 2, 4)}">selected="selected"</c:if>>
				        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
				       </option>
				      </c:forEach>
				     </select>
	   			</td>
			</tr>
		</c:forEach>
		<%-- 
		<tr>
   			<td>1</td>
   			<td>
				<select id="selectHour" class="firstTime" >
			      <option value="08">08</option>
			      <c:forEach var="item" varStatus="i" begin="9" end="24" step="1">
			       <option value="${item}" <c:if test="${item eq fn:substring(resultList[0].startTime, 0, 2)}">selected="selected"</c:if>>
			        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
			       </option>
			      </c:forEach>
			     </select>
			:
				<select id="selectMin" class="firstTime">
			      <option value="00">00</option>
			      <c:forEach var="item" varStatus="i" begin="10" end="50" step="10">
			       <option value="${item}" <c:if test="${item eq fn:substring(resultList[0].startTime, 2, 4)}">selected="selected"</c:if>>
			        <c:if test="${item < 10}">0</c:if><c:out value="${item}" />
			       </option>
			      </c:forEach>
			     </select>
			</td>
   			<td><span id="endTime_1"></span></td>
		</tr>
		<c:forEach var="result" varStatus="status" begin="2" end="12" step="1">
			<tr>
	   			<td><c:out value="${status.current}" /></td>
	   			<td><span id="stTime_${status.current}"></span></td>
	   			<td><span id="endTime_${status.current}"></span></td>
			</tr>
		</c:forEach>
		 --%>
	</tbody>    
   </table>      
</form>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>