<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="STDT_MANAGE"/>
	<c:param name="validator" value="userManageVO"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("#btn_resent").click(function(){
		var userId = $(this).data("id"),
			userNm = $(this).data("nm"),
			emailAdres = $(this).data("email");
		
		$.ajax({
			url: '/uss/umt/user/authMail.json',
			async: true,
			type: 'POST',
			data: {
				userId: userId
				, userNm: userNm
				, emailAdres: emailAdres
			},
			dataType: 'json',
			success: function(data){
				alert(userNm + "\n" + emailAdres + "\n로 인증메일이 발송되었습니다.");
			}, error : function(){
				alert("error");
			}
		});
	});
	
	$("#btn_auth").click(function(){
		var userId = $(this).data("id"),
			userNm = $(this).data("nm"),
			emailAdres = $(this).data("email");
		
		if(confirm(userNm + "\n" + emailAdres + "\n메일인증을 수동으로 완료 처리하시겠습니까?")){
			$.ajax({
				url: '/mng/usr/mailAuth.do',
				async: true,
				type: 'POST',
				data: {
					userId: userId
				},
				dataType: 'json',
				success: function(data){
					alert("인증처리 되었습니다.");
					location.reload();
				}, error : function(){
					alert("error");
				}
			});
		}
		
	});
});

function fnResetPwd(){
	if(confirm("${userManageVO.userNm} 님의 패스워드를 초기화 하시겠습니까?\n(초기 비밀번호 : 1234)로 변경됨")){
		$(document).ready(function(){
			$.ajax({
				url: '/mng/usr/EgovResetPwd.do',
				async: true,
				type: 'POST',
				data: {
					userId: '${userManageVO.userId}'
				},
				dataType: 'text',
				success: function(){
					alert("패스워드 초기화가 완료되었습니다.\n1234로 초기화되었습니다.");
				}
			});
		});
	}
}

</script>
		
<div id="cntnts">
	<table class="chart2">
		<caption>회원관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
			<col width="300"/>
		</colgroup>
		<tbody>
		<p>회원가입일 : <fmt:formatDate value="${userManageVO.frstRegistPnttm}"  pattern="yyyy-MM-dd hh:mm:ss"/></p>
		<tr>
			<th><span style="color:red;">구분</span></th>
			<td>
				<c:choose>
					<c:when test="${userManageVO.userSeCode eq '02'}">
						<c:out value="일반"/>
					</c:when>
					<c:when test="${userManageVO.userSeCode eq '04'}">
						<c:out value="기타 대학"/>
					</c:when>
					<c:when test="${userManageVO.userSeCode eq '06'}">
						<c:out value="한국외국어대학교"/>
					</c:when>
				</c:choose>
			</td>
		</tr>
		
		<tr>
			<th>소속</th>
			<td>
				<c:choose>
					<c:when test="${userManageVO.userSeCode eq '02'}">
						<c:out value="${userManageVO.groupCode }"/>
					</c:when>
					<c:when test="${userManageVO.userSeCode eq '04'}">
						<c:out value="${userManageVO.groupCode }"/>
					</c:when>
					<c:when test="${userManageVO.userSeCode eq '06'}">
						<c:out value="한국외국어대학교"/>
					</c:when>
				</c:choose>
			</td>
		</tr>
		<c:if test="${userManageVO.userSeCode eq '06' or userManageVO.userSeCode eq '04'}">
		<tr>
			<th>세부전공</th>
			<td>
				<c:out value="${userManageVO.major}"/>
			</td>
		</tr>
		</c:if>
		
		<tr>
			<th>이름 <span style="color:red;">*</span></th>
			<td>
				<c:out value="${userManageVO.userNm}"/>
			</td>
			<td rowspan="11" align="center">
				<c:choose>
					<c:when test="${empty userManageVO.photoStreFileNm}">사진없음</c:when>
					<c:otherwise><img src="${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>" width="300"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
		
		<tr>
			<th> 생년월일 <span style="color:red;">*</span></th>
			<td>
				<c:out value="${fn:substring(userManageVO.brthdy, 0, 4)}-${fn:substring(userManageVO.brthdy, 4, 6)}-${fn:substring(userManageVO.brthdy, 6, 8)}"/>
			</td>
		</tr>
		
		<tr>
			<th> 성별 <span style="color:red;">*</span></th>
			<td>
				<c:out value="${userManageVO.sexdstn eq 'M' ? '남자' : '여자'}"/>
			</td>
		</tr>
		
		<c:if test="${userManageVO.userSeCode eq '06'}">
			<tr>
				<th> 학번 <br/>(한국외국어대학교)</th>
				<td>
					<c:out value="${userManageVO.stNumber}"/>
				</td>
			</tr>
		</c:if>

		<tr>
			<th><label for="moblphonNo">휴대전화</label> <span style="color:red;">*</span></th>
			<td>
				(+<c:out value="${userManageVO.geocode}"/>)
				<c:out value="${userManageVO.moblphonNo}"/>
			</td>
		</tr>
		
		<tr>
			<th> 아이디 <span style="color:red;">*</span></th>
			<td>
				<c:out value="${userManageVO.userId}"/>
			</td>
		</tr>
		
		<tr>
			<th> 본인확인 이메일 <span style="color:red;">*</span></th>
			<td>
				<c:out value="${userManageVO.emailAdres}"/>
			</td>
		</tr>
		
		<tr>
			<th> 메일 인증여부</th>
			<td>
				<c:choose>
					<c:when test="${userManageVO.confmAt eq 'Y'}">
						인증완료
						<c:if test="${not empty userManageVO.confmPnttm }">
							(<fmt:formatDate value="${userManageVO.confmPnttm}"  pattern="yyyy-MM-dd hh:mm"/>)
						</c:if>
					</c:when>
					<c:otherwise>
						인증 대기중
						<button type="button" id="btn_resent" data-id="${userManageVO.userId}" data-nm="${userManageVO.userNm}" data-email="${userManageVO.emailAdres}"> 인증메일 재발송  </button>
						<button type="button" id="btn_auth" data-id="${userManageVO.userId}" data-nm="${userManageVO.userNm}" data-email="${userManageVO.emailAdres}"> 관리자 수동 인증하기  </button>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		
		<tr>
			<th><em>*</em> 패스워드 <span style="color:red;">*</span></th>
			<td>***** &nbsp;&nbsp;&nbsp;&nbsp;
			<button onclick="fnResetPwd();return false;" > 패스워드 초기화  </button>
			</td>
		</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="./EgovStudentManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<%-- <a href="/mng/usr/EgovMberAddView.do"><img src="${_IMG}/btn/btn_regist.gif" alt="등록하기" /></a> --%>
		<input type="image" src="${MNG_IMG}/btn/btn_modify.gif" alt="수정하기" onclick="location.href='/mng/usr/EgovStudentAddView.do?userId=${userManageVO.userId}'"/>
		<%-- <input type="image" src="<c:url value='${MNG_IMG}/btn/btn_modify.gif'/>" alt="수정" onclick="return checkForm(document.userManageVO);"/> --%>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록"/></a>				
	</div>
	
	<h2>교육과정 이력</h2>
	<table class="chart2">
	          <colgroup>
	            <col style='width:5%'>
	            <col style='width:5%'>
	            <col style='width:150px'>
	            <col style='width:130px'>
	            <col style='width:5%'>
	            <col style='width:5%'>
	          </colgroup>
	          <thead>
	            <tr class='bg-light-gray font-700' style='text-align:center;'>
	              <th scope='col'>년도</th>
	              <th scope='col'>학기</th>
	              <th scope='col'>기본과정분류</th>
	              <th scope='col'>교육과정명</th>
	              <th scope='col'>성적표</th>
	              <th scope='col'>수료증</th>
	            </tr>
	          </thead>
	          <tbody>
	            <c:forEach var="result" items="${resultList}" varStatus="status">
				<c:url var="viewUrl" value="/lms/crm/CurriculumAllView.do${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:param name="crclbId" value="${result.crclbId}"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
			<td><c:out value="${result.crclYear}"/></td>
			<td><c:out value="${result.crclTermNm}"/></td>
			<td><c:out value="${result.sysCodeNmPath}"/></td>
			<td><c:out value="${result.crclNm}"/></td>
			<td></td>
			<td></td>
	</tr>
	</c:forEach>
	<c:if test="${fn:length(resultList) == 0}">
	<tr>
	      	<td class="listCenter" colspan="7"><spring:message code="common.nodata.msg" /></td>
	  	</tr>
	</c:if>
	          </tbody>
	        </table>
<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	