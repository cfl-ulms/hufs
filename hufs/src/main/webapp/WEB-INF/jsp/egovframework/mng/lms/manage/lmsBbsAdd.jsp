<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/lms/manage"/>
<c:set var="_ACTION" value=""/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty param.depth1}"><c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/></c:if>
	<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
</c:url>
<% /*URL 정의*/ %>


<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
	<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.depth1 eq 'CRCL_BOARD' ? '과정 게시판' : '과정등록관리'}"/>
</c:import>


<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<style>
ul#chkGroup {
    list-style:none;
    margin:0;
    padding:0;
}

ul#chkGroup li {
    margin: 0 10px 0 0;
    padding: 0 0 0 0;
    border : 0;
    float: left;
}

</style>

<script type="text/javascript">
	function fn_egov_regist() {
		$("#openScope").val($("#openScope").val().substring(1,$("#openScope").val().length));

		tinyMCE.triggerSave();

		$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

		<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
			for(var cmIdx = 1 ; cmIdx <= boardCateLevel ; cmIdx++){
				var cmObj = document.getElementById("ctgry" + cmIdx);
				if(cmObj != null) {
					if(fn_egov_SelectBoxValue("ctgry" + cmIdx) != '') {
						document.board.ctgryId.value = fn_egov_SelectBoxValue("ctgry" + cmIdx);
					}
				}
			}
	    </c:if>

	    <c:choose>
	    	<c:when test="${searchVO.registAction eq 'updt'}">
				if (!confirm('<spring:message code="common.update.msg" />')) {
					 return false;
				}
			</c:when>
			<c:otherwise>
				if (!confirm('<spring:message code="common.regist.msg" />')) {
					return false;
				}
			</c:otherwise>
		</c:choose>
	}

	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">

		var boardCateLevel = ${boardCateLevel};
		var boardCateList = new Array(${fn:length(boardCateList)});
		<c:forEach var="cate" items="${boardCateList}" varStatus="status">
			boardCateList[${status.index}] = new ctgryObj('${cate.upperCtgryId}', '${cate.ctgryId}', '${cate.ctgryNm}', ${cate.ctgryLevel});
		</c:forEach>
	</c:if>


	$(document).ready(function(){

		if($(".selectScope:checked").val() == 'T'){
			$('#selectGroup option').attr("disabled",true);
			$("#selectGroup").css("background-color", "#ababab");
		}else{
			$('#selectGroup option').attr("disabled",false);
			$("#selectGroup").css("background-color", "");
		}


		var adfile_config = {
				siteId:"<c:out value='${brdMstrVO.siteId}'/>",
				pathKey:"Board",
				appendPath:"<c:out value='${brdMstrVO.bbsId}'/>",
				editorId:"${_EDITOR_ID}",
				fileAtchPosblAt:"${brdMstrVO.fileAtchPosblAt}",
				maxMegaFileSize:1024,
				atchFileId:"${board.atchFileId}"
			};

		fnCtgryInit('<c:out value='${board.ctgryPathById}'/>');
		fn_egov_bbs_editor(adfile_config);

		$("#egovComFileUploader").change(function(){
			var fileVal = $(this).val(),
				ext = fileVal.split("."),
				splitSize = ext.length - 1,
				fileExt = ext[splitSize].toLowerCase();


			if($("#box_images > li").length > 0){
				alert("기존 책표지를 삭제 후 등록해주세요.")
				$(this).val("");
			}else if(fileExt=="bmp" || fileExt=="gif" || fileExt=="jpeg" || fileExt=="jpg" || fileExt=="png" || fileExt=="BMP" || fileExt=="GIF" || fileExt=="JPEG" || fileExt=="JPG" || fileExt=="PNG"){

			}else{
				alert("이미지 확장자만 저장할 수 있습니다.");
				$(this).val("");
			}
		});


		$(".btn_ok").click(function(){
			if($(".selectScope:checked").val() == "G"){
				var selectText = $('#selectGroup option:selected').text();
				var selectVal = $('#selectGroup option:selected').val();
				var checkResult = true;
				$("#chkGroup li").each(function(){
					if($(this).text() == selectText){
						alert("이미 선택된 그룹입니다.");
						checkResult = false;
						return false;
					}
				});

				if(checkResult){
					$("#chkGroup").append("<li>"+selectText+"<a href=\"#\" class=\"btn_delli\"><img src=\"/template/manage/images/btn/del.gif\"></a></li>");
					$("#openScope").val($("#openScope").val()+","+selectVal);
				}
			}
		});


		$(".btn_delli").click(function(){
			$(this).closest("li").remove();
		});

		$(".selectScope").click(function(){
			if($(this).val() == 'T'){
				$('#selectGroup option').attr("disabled",true);
				$("#selectGroup").css("background-color", "#ababab");
			}else{
				$('#selectGroup option').attr("disabled",false);
				$("#selectGroup").css("background-color", "");
			}
		});
});
</script>

<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm }"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
<c:if test="${curriculumVO.processSttusCode > 0}">
	<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="6"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
		<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	</c:import>
</c:if>

<ul class="box_bbsmst">
	<c:forEach var="result" items="${masterList}">
		<c:url var="masterUrl" value="/mng/lms/manage/lmsBbsList.do${_BASE_PARAM}">
			<c:param name="bbsId" value="${result.bbsId}" />
		</c:url>
		<li <c:if test="${result.bbsId eq searchVO.bbsId}">class="on"</c:if>>
			<a href="${masterUrl}">
				<c:choose>
					<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
					<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
				</c:choose>
				<c:out value="${result.bbsNm}"/>
			</a>
		</li>
	</c:forEach>
</ul>

<div id="cntnts">
	<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
        <input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
	    <input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
	    <input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
		<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
		<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
		<input type="hidden" name="registAction" value="<c:out value='${searchVO.registAction}'/>"/>
		<input type="hidden" name="tmplatImportAt" value="<c:out value='${searchVO.tmplatImportAt}'/>"/>

        <input type="hidden" name="nttNo" value="<c:out value='${board.nttNo}'/>"/>
        <input type="hidden" name="atchFileId" value="<c:out value='${board.atchFileId}'/>"/>
        <input type="hidden" name="crclId" value="${searchVO.crclId}"/>
        <input type="hidden" id="openScope" name="openGroup" value="${board.openGroup}"/>
        <c:if test="${param.depth1 eq 'CRCL_BOARD' }">
        	<input type="hidden"  name="depth1" value="CRCL_BOARD"/>
        </c:if>


        <%--
        <form:hidden path="nttNo"/>
        <form:hidden path="ctgryId"/>
        <form:hidden path="atchFileId"/>
         --%>
		<table class="chart2" summary="작성인, 제목, 내용, 파일첨부를 입력하는 표입니다." >
			<colgroup>
				<col width="20%" />
				<col width="80%" />
			</colgroup>
			<tbody>
				<c:choose>
					<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
						<input type="hidden" name="nttSj" value="dummy"/>
						<tr>
							<th><spring:message code="cop.processSttus" /></th>
							<td>
								<select name="processSttusCode" class="select">
									<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
										<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}"/></option>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th>
								<label for="nttSj"><spring:message code="cop.nttSj" /></label>
							</th>
							<td>
								<input type="text" id="nttSj" class="inp_long" name="nttSj" value="${board.nttSj}"/>
							</td>
						</tr>

						<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
							<tr>
								<th>
									<label for="ctgry1">게시글 구분</label>
								</th>
								<td>
									<c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
										<c:choose>
											<c:when test="${status.first}">
												<%-- <select name="regCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})"> --%>
												<select name="ctgryId" id="ctgry${ctgryLevel}">
													<option value=""><spring:message code="cop.select" /></option>
													<c:forEach var="cate" items="${boardCateList}">
														<c:if test="${cate.ctgryLevel eq 1 }">
															<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
														</c:if>
													</c:forEach>
												</select>
											</c:when>
											<c:otherwise><select name="regCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})"><option value=""><spring:message code="cop.select" /></option></select></c:otherwise>
										</c:choose>
									</c:forEach>
									<script type="text/javascript">
										fnCtgryInit('${board.ctgryPathById}');
									</script>
								</td>
							</tr>
							<tr>
								<th>
									<label for="ctgry1">공개 범위</label>
								</th>
								<c:choose>
									<c:when test="${brdMstrVO.sysTyCode eq 'ALL' }">
										<td>
											<label><input type="radio" class="selectScope" name="openScope" value="T" ${empty board.openScope or board.openScope eq 'T' ? 'checked' : '' }/>전체공개</label>&nbsp;&nbsp;
											<label><input type="radio" class="selectScope" name="openScope" value="P" ${board.openScope eq 'P' ? 'checked' : '' } ${empty selecteResultList ? 'readonly' : ''}/>교원공개</label>
										</td>
									</c:when>
									<c:otherwise>
										<td>
											<label><input type="radio" class="selectScope" name="openScope" value="T" ${empty board.openScope or board.openScope eq 'T' ? 'checked' : '' }/>전체공개</label>&nbsp;&nbsp;
											<label><input type="radio" class="selectScope" name="openScope" value="G" ${board.openScope eq 'G' ? 'checked' : '' } ${empty selecteResultList ? 'readonly' : ''}/>그룹선택</label>

												<select id="selectGroup" ${empty selecteResultList ? 'readonly' : ''}>
													<c:if test="${not empty selecteResultList }">
														<c:forEach items="${selecteResultList}" var="result">
															<option value="${brdMstrVO.sysTyCode eq 'CLASS' ? result.classCnt : result.groupCnt }" >${brdMstrVO.sysTyCode eq 'CLASS' ? result.classCnt  : result.groupCnt }${brdMstrVO.sysTyCode eq 'CLASS' ? '반' : '조'}</option>
														</c:forEach>
													</c:if>
												</select>
												<a href="#" class="btn_ok btn_mngTxt">선택</a>
												<br/>
												<ul id="chkGroup">
													<c:if test="${not empty  board.openGroup}">
														<c:set var="groupList" value="${fn:split(board.openGroup,',') }"/>
														<c:forEach items="${groupList}" var="tempCnt">
															<li>${tempCnt }${brdMstrVO.sysTyCode eq 'CLASS' ? '반' : '조'}<a href="#" class="btn_delli"><img src="/template/manage/images/btn/del.gif"></a></li>
														</c:forEach>
													</c:if>
												</ul>
										</td>
									</c:otherwise>
								</c:choose>

							</tr>
						</c:if>
						<%--
						<tr>
							<th><label for="othbcAt"><spring:message code="cop.publicAt" /></label></th>
							<td>
								<spring:message code="cop.public" /> : <form:radiobutton path="othbcAt"  value="Y"/>&nbsp;
				                <spring:message code="cop.private" /> : <form:radiobutton path="othbcAt"  value="N"/>
				                <br/><form:errors path="othbcAt" />
							</td>
						</tr>
				 		--%>
				 	</c:otherwise>
				 </c:choose>
				<tr>
					<td colspan="2">
						<textarea id="nttCn" class="width:100%" name="nttCn" rows="30" ><c:out value="${board.nttCn}" escapeXml="false"/></textarea>
					</td>
				</tr>
				<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
					<tr>
						<th><spring:message code="cop.atchFile" /></th>
						<td>
							<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
								<c:param name="editorId" value="${_EDITOR_ID}"/>
								<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
						    	<c:param name="param_atchFileId" value="${board.atchFileId}" />
						    	<c:param name="imagePath" value="${_IMG }"/>
						    	<c:param name="mngAt" value="Y"/>
							</c:import>
							<noscript>
								<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
							</noscript>
						</td>
					</tr>
				</c:if>
			</tbody>
		</table>
		
		<div style="margin-top: 20px;">
			<ul style="overflow: hidden;">
				<li style="float: left;">
					<c:choose>
						<c:when test="${param.depth1 eq 'CRCL_BOARD'}">
							<c:url var="selectBoardListUrl" value="/mng/lms/cla/curriculumBoardList.do"/>
						</c:when>
						<c:otherwise>
							<c:url var="selectBoardListUrl" value="/mng/lms/manage/lmsBbsList.do">
							  	<c:param name="crclId" value="${searchVO.crclId}"/>
								<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
						        <c:param name="bbsId" value="${brdMstrVO.bbsId}" />
						        <c:param name="trgetId" value="${searchVO.trgetId}" />
						        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
								<c:param name="searchCnd" value="${searchVO.searchCnd}" />
								<c:param name="searchWrd" value="${searchVO.searchWrd}" />
								<c:param name="searchCate" value="${searchVO.searchCate}" />
								<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
						  			<c:if test="${not empty searchCate}">
						  				<c:param name="searchCateList" value="${searchCate}" />
						  			</c:if>
						  		</c:forEach>
						  		<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
								<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
						      </c:url>
						</c:otherwise>
					</c:choose>

					<a href="${selectBoardListUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록" /></a>
				</li>
				<li style="float: right;">
			      	<c:choose>
						<c:when test="${searchVO.registAction eq 'regist' }">
							<input type="image" src="${_IMG}/btn/btn_regist.gif" alt="등록"/>
						</c:when>
						<c:when test="${searchVO.registAction eq 'updt' }">
							<input type="image" src="${_IMG}/btn/btn_modify.gif" alt="수정"/>
						</c:when>
						<c:when test="${searchVO.registAction eq 'reply' }">
							<input type="image" src="${_IMG}/btn/btn_reply.gif" alt="답변"/>
						</c:when>
					</c:choose>
				</li>
			</ul>
		</div>
	</form:form>

</div>

	<br/><br/><br/><br/>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>