<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<%--수강대상자 확정 승인 --%>
	<c:when test="${param.menuCode eq 'confm' }">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG_CONFIRM"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강대상자 확정 승인"/>
			<c:param name="menuCode" value="${param.confm }"/>
		</c:import>
	</c:when>
	<c:when test="${param.menuCode eq 'student' }">
	   <%-- 과정등록관리 > 학생 --%>
	</c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강신청"/>
		</c:import>
	</c:otherwise>
</c:choose>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
$(document).ready(function(){
	//------------------------------
	//수강신청 start
	//------------------------------

	//전체명단 확정 승인
	$(document).on("click", "#confmAcceptBtn", function(){
		var studentFixFlag = true;

		$.ajax({
            url : "/lms/crm/selectMemberCntList.json"
                , type : "post"
                , dataType : "json"
                , data : {"crclId" : "${param.crclId}", "sttus" : "1"}
                , success : function(data){
                	if(0 < data.memberCntList[0].classNullCnt) {
                		alert("학생중에 반이 선택이 안되었습니다. 선택 후 확정이 가능합니다.")
                		return false;
                	}
                	
                	if(0 < data.memberCntList[0].groupNotNullCnt && 0 < data.memberCntList[0].groupNullCnt) {
                		alert("학생중에 조가 선택이 안되었습니다. 선택 후 확정이 가능합니다.")
                        return false;
                	}

                	confirmProcess();
                }, error : function(){
                    alert("error");
                }
        });
		return false;
	});
	
	function confirmProcess() {
		//수강신청 정원 확인
        var applyMaxCnt     = "${curriculumVO.applyMaxCnt}";
        var classStudentCnt = "${curriculumVO.classStudentCnt}";

        /* if(applyMaxCnt != 0 & applyMaxCnt < classStudentCnt) {
            alert("수강신청 승인 인원이 초과하였습니다.")
            return false;
        } */
        
        //가상폼 생성 start------------------------------
        var $form = $('<form></form>');
        $form.attr('id', 'acceptForm');
        $form.attr('method', 'post');
        //가상폼 생성 end--------------------------------
        
        var confmSttusCode = $("<input type=\"hidden\" id=\"confmSttusCode\" name=\"confmSttusCode\" value=\"1\"/>");
        var crclId = $("<input type=\"hidden\" id=\"crclId\" name=\"crclId\" value=\"${curriculumVO.crclId}\"/>");
        var crclbId = $("<input type=\"hidden\" name=\"crclbId\" value=\"${curriculumVO.crclbId}\"/>");
        var forwardUrl = $("<input type=\"hidden\" id=\"forwardUrl\" name=\"forwardUrl\" value=\"/mng/lms/crm/curriculumStudent.do?crclId=${param.crclId}&crclbId=${param.crclbId}\"/>");

        $form.attr('action', '/mng/lms/crm/updateProcessSttusCode.do');
        $form.attr('target', '_self');
        $form.append(confmSttusCode).append(crclId).append(crclbId).append(forwardUrl);
        
        $("#header").append($form);
        $form.submit();
	}
	
	//전체명단 확정 취소
	$(document).on("click", "#confmCancelBtn", function(){
		//가상폼 생성 start------------------------------
		var $form = $('<form></form>');
		$form.attr('id', 'acceptForm');
		$form.attr('method', 'post');
		//가상폼 생성 end--------------------------------
		
		var confmSttusCode = $("<input type=\"hidden\" id=\"confmSttusCode\" name=\"confmSttusCode\" value=\"0\"/>");
		var crclId = $("<input type=\"hidden\" id=\"crclId\" name=\"crclId\" value=\"${curriculumVO.crclId}\"/>");
		var crclbId = $("<input type=\"hidden\" name=\"crclbId\" value=\"${curriculumVO.crclbId}\"/>");
		var forwardUrl = $("<input type=\"hidden\" id=\"forwardUrl\" name=\"forwardUrl\" value=\"/mng/lms/crm/curriculumStudent.do?crclId=${param.crclId}&crclbId=${param.crclbId}\"/>");

		$form.attr('action', '/mng/lms/crm/updateProcessSttusCode.do');
		$form.attr('target', '_self');
		$form.append(confmSttusCode).append(crclId).append(crclbId).append(forwardUrl);
		
		$("#header").append($form);
		$form.submit();
	});
	
	//수강명단 최종 확정 모달
	$(document).on("click", "#updateConfirmBtn", function(){
		$.ajax({
			type:"post",
			dataType:"json",
			url:"/mng/lms/crm/selectMemberGroupCnt.json",
			data:{"crclId":"${curriculumVO.crclId}"},
			success: function(data) {
				var resultList = data.selectMemberGroupCnt[0];
				var textHtml   = "";
				
				//분반과, 조배정을 진행하지 않고
				if(1 >= resultList["classCnt"]) {
					textHtml = "분반";
				}
				
				if(0 >= resultList["groupCnt"]) {
					textHtml = "조배정";
				}
				
				if(1 >= resultList["classCnt"] && 0 >= resultList["groupCnt"]) {
					textHtml = "분반과, 조배정";
				}
				
				if(textHtml != "") {
					$('#updateContent').html(textHtml + "을 진행하지 않고, ");
				}
			},
			error:function() {
				alert("관리자에게 문의 바랍니다.");
			}
		});

		$("#dialog-background, .process-update-dialog").show();
	});
	
	//수강명단 최종 확정 업데이트
	$(".btn_update").click(function(){
		$("#earlyAcceptForm").submit();
	});
	//------------------------------
	//수강신청 end
	//------------------------------



	//------------------------------
	//수강대상자 확정 승인 start
	//------------------------------
	$(document).on("click", "#confmAdminAcceptBtn", function(){
		$("#dialog-background, .admin-process-update-dialog").show();
	}); 

	//관리자 최종 확정 승인 취소
	$(document).on("click", "#confmAdminCancleBtn", function(){
		//가상폼 생성 start------------------------------
		var $form = $('<form></form>');
		$form.attr('id', 'acceptForm');
		$form.attr('method', 'post');
		//가상폼 생성 end--------------------------------
		
		var confmSttusCode = $("<input type=\"hidden\" id=\"confmSttusCode\" name=\"confmSttusCode\" value=\"2\"/>");
		var processSttusCode = $("<input type=\"hidden\" id=\"processSttusCode\" name=\"processSttusCode\" value=\"4\"/>");
		var crclId = $("<input type=\"hidden\" id=\"crclId\" name=\"crclId\" value=\"${curriculumVO.crclId}\"/>");
		var crclbId = $("<input type=\"hidden\" name=\"crclbId\" value=\"${curriculumVO.crclbId}\"/>");
		var forwardUrl = $("<input type=\"hidden\" id=\"forwardUrl\" name=\"forwardUrl\" value=\"/mng/lms/crm/curriculumStudent.do?crclId=${param.crclId}&crclbId=${param.crclbId}&menuCode=${param.menuCode}\"/>");

		$form.attr('action', '/mng/lms/crm/updateProcessSttusCode.do');
		$form.attr('target', '_self');
		$form.append(processSttusCode).append(confmSttusCode).append(crclId).append(crclbId).append(forwardUrl);
		
		$("#header").append($form);
		$form.submit();
	});
	
	//관리자 최종 확정 승인("예" 버튼 클릭)
	$(document).on("click", ".btn_admin_accept", function(){
		$("#adminAcceptForm input[name=processSttusCode]").val("6");
		$("#adminAcceptForm").submit();
	});
	
	//관리자 최종 확정 승인("아니오" 버튼 클릭)
	$(document).on("click", ".btn_admin_cancle", function(){
		/* $("#adminAcceptForm input[name=processSttusCode]").val("");
		$("#adminAcceptForm").submit(); */
		$("#dialog-background, .process-update-dialog").hide();
        $("#dialog-background, .admin-process-update-dialog").hide();
	});
	//------------------------------
	//수강대상자 확정 승인 end
	//------------------------------
	
	//모달 종료
	$(document).on("click", "#dialog-background,.btn-close-dialog, .btn-no", function(){
		$("#dialog-background, .process-update-dialog").hide();
		$("#dialog-background, .admin-process-update-dialog").hide();
	});
	
	//엑셀파일 다운로드
	$(".btn_excel").click(function(){
		var actionUrl = $("#frm").attr("action");
		
		$("#excelAt").val("Y");
		$("#frm").attr("action", "/mng/lms/crm/curriculumStudent.do");
		$("#frm").submit();
		
		$("#excelAt").val("");
		$("#frm").attr("action", actionUrl);
		return false;
	});

	//일괄파일 다운로드
	$(".btn_zipFile").click(function(){
		var actionUrl = $("#frm").attr("action");
		
		$("#zipFileAt").val("Y");
		$("#frm").attr("action", "/mng/lms/crm/curriculumStudent.do");
		$("#frm").submit();
		
		$("#zipFileAt").val("");
		$("#frm").attr("action", actionUrl);
		return false;
	});
});

</script>
<%-- 과정등록관리 > 학생에서 import할 때 상단 table 안나오도록 처리 --%>
<c:if test="${param.menuCode ne 'student' }">
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>
</c:if>

<c:if test="${empty param.menuCode}">
	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="4"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
	</c:import>
</c:if>

<c:import url="/mng/lms/curriculumStudenttabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="1"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<%-- 과정등록관리 > 학생에서 import할 때 상단 table 안나오도록 처리 --%>
<c:if test="${param.menuCode ne 'student' }">
	<table class="chart2 mb50">
		<tbody>
			<tr>
				<th class="alC">소속</th>
				<th class="alC">통계지표</th>
				<th class="alC">총 인원 : ${totCnt }명</th>
			</tr>
			<c:forEach var="stats" items="${curriculumMemberStatsList}" varStatus="status">
				<tr class="alC">
					<td class="line alC">${stats.mngDeptNm }</td>
					<td class="line alC">${stats.ageNm }</td>
					<td class="line alC">${stats.ageCnt }명</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<div id="bbs_search">
    <c:choose>
        <%-- 과정등록관리 > 학생 --%>
        <c:when test="${param.menuCode eq 'student' }"><c:set var="searchUrl" value="/mng/lms/crcl/selectCurriculumStudent.do?importFlag=1"/></c:when>
        <c:otherwise><c:set var="searchUrl" value="/mng/lms/crm/curriculumStudent.do"/></c:otherwise>
    </c:choose>
  	<form id="frm" name="frm" method="post" action="<c:url value="${searchUrl }"/>">
  		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
		<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
		<input type="hidden" name="menuCode" value="${param.menuCode}"/>
		<input type="hidden" id="excelAt" name="excelAt" value=""/>
		<input type="hidden" id="zipFileAt" name="zipFileAt" value=""/>
		<input type="text" name="searchHostCode" value="${searchVO.searchHostCode }" class="inp_s" id="inp_text" placeholder="소속">
		
		<input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" class="inp_s" id="inp_text" placeholder="학생 이름">
		<br/>
		<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
	</form>
</div>
<div class="alR mb20">
	<a href="#" class="btn_excel">엑셀파일 다운로드</a>
	<c:if test="${curriculumVO.stdntAplyAt eq 'Y'}">
		<a href="#" class="btn_zipFile btn_mngTxt">파일일괄다운로드</a>
	</c:if>
</div>

<table class="chart2 mb50">
	<tbody>
		<tr>
			<th class="alC">no</th>
			<th class="alC">소속</th>
			<th class="alC">이름</th>
			<th class="alC">생년월일</th>
			<th class="alC">학번</th>
			<th class="alC">학년</th>
			<th class="alC">전화번호</th>
			<th class="alC">이메일</th>
			<th class="alC">신청서</th>
			<th class="alC">계획서</th>
			<th class="alC">기타파일</th>
			<th class="alC">반배정</th>
			<th class="alC">조배정</th>
			<th class="alC">담당교수</th>
		</tr>
		<c:forEach var="student" items="${selectStudentList}" varStatus="status">
			<tr class="alC">
				<td class="line alC">${status.count}</td>
				<td class="line alC">${student.mngDeptNm }</td>
				<td class="line alC">${student.userNm }</td>
				<td class="line alC">${student.brthdy }</td>
				<td class="line alC">${student.stNumber }</td>
				<td class="line alC">${student.stGrade }</td>
				<td class="line alC">(+${student.geocode})${student.moblphonNo}</td>
				<td class="line alC">${student.emailAdres}</td>
				<c:url var="aplyFileUrl" value="/cmm/fms/absolutePathFileDown.do">
					<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
					<c:param name="fileNm" value="${student.aplyFile }"/>
					<c:param name="oriFileNm" value="${student.aplyOriFile }"/>
 			    </c:url>
 			    <td class="line alC"><a href="${aplyFileUrl }">${student.aplyOriFile }</a></td>
				<c:url var="planFileUrl" value="/cmm/fms/absolutePathFileDown.do">
					<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
					<c:param name="fileNm" value="${student.planFile }"/>
					<c:param name="oriFileNm" value="${student.planOriFile }"/>
 			    </c:url>
				<td class="line alC"><a href="${planFileUrl }">${student.planOriFile }</a></td>
				<c:url var="etcFileUrl" value="/cmm/fms/absolutePathFileDown.do">
					<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
					<c:param name="fileNm" value="${student.etcFile }"/>
					<c:param name="oriFileNm" value="${student.etcOriFile }"/>
 			    </c:url>
				<td class="line alC"><a href="${etcFileUrl }">${student.etcOriFile }</a></td>
				<td class="line alC">${student.classCnt }</td>
				<td class="line alC">${student.groupCnt }</td>
				<td class="line alC">${student.manageNm }</td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(selectStudentList) == 0}">
			<tr>
	        	<td class="listCenter" colspan="11"><spring:message code="common.nodata.msg" /></td>
	      	</tr>
	    </c:if>
	</tbody>
</table>

<div class="btn_c">
	<c:choose>
	 	<%-- 수강대상자 확정 승인 start --%>
	 	<c:when test="${curriculumVO.confmSttusCode eq 2 and curriculumVO.processSttusCodeDate eq 4 and param.menuCode eq 'confm'}">
			<a href="#none" class="btn_mngTxt" id="confmAdminAcceptBtn">관리자 최종 확정 승인</a>
		</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 3 and (curriculumVO.processSttusCodeDate eq 4 or curriculumVO.processSttusCodeDate eq 6) and param.menuCode eq 'confm'}">
			<a href="#none" class="btn_mngTxt" id="confmAdminCancleBtn">관리자 최종 확정 승인 취소</a>
		</c:when>
		<%-- 수강대상자 확정 승인 end --%>
		
		<%-- 수강신청 start --%>
		<c:when test="${curriculumVO.confmSttusCode >= 3 or curriculumVO.processSttusCodeDate >= 5}">
	 	</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<a href="#none" class="btn_mngTxt" id="confmAcceptBtn">전체 명단 확정</a>
		</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 1 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<a href="#none" class="btn_mngTxt" id="confmCancelBtn">전체명단 확정 취소</a>
			<a href="#none" class="btn_mngTxt" id="updateConfirmBtn">수강명단 최종 확정</a>
		</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 2 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
		  <a href="#none" class="btn_mngTxt" id="confmCancelBtn">수강명단 최종 확정 취소</a>
        </c:when>
		<%-- 수강신청 end --%>
	</c:choose>
    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
	<c:if test="${empty param.menuCode }">
		<div style="color:red;padding-top:10px;"><strong>수강명단을 최종 확정하시면 운영팀에 통보되고 더 이상 수정할 수 없게 됩니다.</strong></div>
	</c:if>
</div>

<div class="my-dialog process-update-dialog" style="width:400px;height:200px;">
<div class="modal_header">알림</div>
<div class="modal_body" style="padding:10px;">
	<form id="earlyAcceptForm" action="/mng/lms/crm/updateProcessSttusCode.do" method="post">
		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
		<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
		<input type="hidden" name="confmSttusCode" value="2"/>
		<input type="hidden" name="forwardUrl" value="/mng/lms/crm/curriculumStudent.do?crclId=${param.crclId}&crclbId=${param.crclbId}"/>

		<div style="padding-bottom:30px;">
			<span id="updateContent"></span>
			수강명단을 최종 확정하시겠습니까?<br /><br />
			<div style="text-align:center;">(총 ${totCnt}명)</div>
		</div>
		<br />
	</form>
		<div class="box_btn">
			<a href="#none" class="btn_update btn_mngTxt">예</a>
			<a href="#none" class="btn_mngTxt btn-no">아니오</a>
			<button class="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
		</div>
	</div>
</div>

<div class="my-dialog admin-process-update-dialog" style="width:680px;height:auto;">
<div class="modal_header">알림</div>
<div class="modal_body" style="padding:10px;">
	<form id="adminAcceptForm" action="/mng/lms/crm/updateProcessSttusCode.do" method="post">
		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
		<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
		<input type="hidden" name="confmSttusCode" value="3"/>
		<input type="hidden" name="processSttusCode" value=""/>
		<input type="hidden" name="forwardUrl" value="/mng/lms/crm/curriculumStudent.do?crclId=${param.crclId}&crclbId=${param.crclbId}&menuCode=${param.menuCode}"/>

		<div style="padding-bottom:30px;">
			<c:out value="${curriculumVO.crclNm }"/>이 관리자 승인으로 수강대상자가 최종 확정되었습니다.<br /><br />
			
			<span style="color:#63A4E0;">
				수강신청 학생에게 수강대상자 여부 안내를 진행하시려면,<br />
				해당 과정의 상태를 <strong>[수강대상자 확정]</strong> 상태로 변경해야 합니다.
			</span>

			<br /><br />
			과정의 상태를 [수강대상자 확정] 상태로 변경하시겠습니까?
		</div>
		<br />
	</form>
		<div class="box_btn">
			<a href="#none" class="btn_admin_accept btn_mngTxt">예</a>
			<a href="#none" class="btn_admin_cancle btn_mngTxt">아니오</a>
			<button class="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
		</div>
	</div>
</div>

<div id="dialog-background"></div>
<%-- <c:import url="/mng/template/bottom.do" charEncoding="utf-8"/> --%>