<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
	<%-- <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if> --%>
</c:url>
<% /*URL 정의*/ %>
<c:choose>
	<c:when test="${param.menu eq 'CLASS_MANAGE'}">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CLASS_MANAGE"/>
			<c:param name="depth1" value="${param.depth1}"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수업관리"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CURRICULUM_MANAGE"/>
			<c:param name="depth1" value="BASE_CRCL"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="과정등록관리"/>
		</c:import>
	</c:otherwise>
</c:choose>

<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
<link rel="stylesheet" href="${CML}/css/common/modal_staff.css?v=2">

<script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
<script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>

<script src="${CML}/js/common.js?v=1"></script>

<script>
$(document).ready(function(){
	
	//시작시간 & 종료시간 & 과정교수명
	var startTimeA = [],
		startTimeHH = [],
		startTimeMM = [],
		endTimeA = [],
		endTimeHH = [],
		endTimeMM = [],
		facA = [];
	
	<c:forEach var="result" items="${camSchList}">
		startTimeA.push({startTime : '${result.startTime}', period : '${result.period}', startTimeNm : '${result.period}교시 ${fn:substring(result.startTime,0,2)}:${fn:substring(result.startTime,2,4)}'});
	</c:forEach>
	
	<c:forEach var="result" items="${camSchList}">
		endTimeA.push({endTime : '${result.endTime}', period : '${result.period}', endTimeNm : '${result.period}교시 ${fn:substring(result.endTime,0,2)}:${fn:substring(result.endTime,2,4)}'});
	</c:forEach>
	
	<c:forEach var="result" begin="6" end="24" step="1">
		<c:set var="hour">
			<c:choose>
				<c:when test="${result < 10}">0${result}</c:when>
				<c:otherwise>${result}</c:otherwise>
			</c:choose>
		</c:set>
		startTimeHH.push({startTimeHH : '${hour}'});
		endTimeHH.push({endTimeHH : '${hour}'});
	</c:forEach>
	
	<c:forEach var="result" begin="0" end="55" step="5">
		<c:set var="minute">
			<c:choose>
				<c:when test="${result < 10}">0${result}</c:when>
				<c:otherwise>${result}</c:otherwise>
			</c:choose>
		</c:set>
		startTimeMM.push({startTimeMM : '${minute}'});
		endTimeMM.push({endTimeMM : '${minute}'});
	</c:forEach>
	
	<c:forEach var="result" items="${facList}">
		facA.push({facId : '${result.facId}', userNm : '${result.userNm}'});
	</c:forEach>
	
	//시수(시간표) 추가
	$(".btn_addTime").click(function(){
		var html = "",
			startTimeHtml = "<option value=''>선택</option>",
			endTimeHtml = "<option value=''>선택</option>",
			startTimeHHHtml = "<option value=''>선택</option>",
			startTimeMMHtml = "<option value=''>선택</option>",
			endTimeHHHtml = "<option value=''>선택</option>",
			endTimeMMHtml = "<option value=''>선택</option>",
			facHtml = "<option value=''>선택</option>",
			curTime = parseInt($(".curTime").text()),
			totalTime = parseInt($(".totalTime").text());
		
		if(curTime <= totalTime){
			for(i = 0; i < startTimeA.length; i++){
				startTimeHtml += "<option value='"+ startTimeA[i].startTime +"' data-period='"+ startTimeA[i].period +"'>" + startTimeA[i].startTimeNm + "</option>";
			}
			
			for(i = 0; i < endTimeA.length; i++){
				endTimeHtml += "<option value='"+ endTimeA[i].endTime +"' data-period='"+ endTimeA[i].period +"'>" + endTimeA[i].endTimeNm + "</option>";
			}
			
			for(i = 0; i < startTimeHH.length; i++){
				startTimeHHHtml += "<option value='"+ startTimeHH[i].startTimeHH +"'>" + startTimeHH[i].startTimeHH + "</option>";
			}
			
			for(i = 0; i < startTimeMM.length; i++){
				startTimeMMHtml += "<option value='"+ startTimeMM[i].startTimeMM +"'>" + startTimeMM[i].startTimeMM + "</option>";
			}
			
			for(i = 0; i < endTimeHH.length; i++){
				endTimeHHHtml += "<option value='"+ endTimeHH[i].endTimeHH +"'>" + endTimeHH[i].endTimeHH + "</option>";
			}
			
			for(i = 0; i < endTimeMM.length; i++){
				endTimeMMHtml += "<option value='"+ endTimeMM[i].endTimeMM +"'>" + endTimeMM[i].endTimeMM + "</option>";
			}
			
			for(i = 0; i < facA.length; i++){
				facHtml += "<option value='"+ facA[i].facId +"' data-nm='"+ facA[i].userNm +"'>"+ facA[i].userNm +"</option>";
			}
			
			<c:choose>
				<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
					html = "<tr class='scheduleTr'>" +
								"<td class='alC line'>" +
									"<input type='text' name='sisu' class='wid50p sisu'/>" +
									"<input type='hidden' name='crclId' value='${curriculumVO.crclId}'/>" +
								"</td>" +
								"<td class='alC line'>" +
									"<input type='text' name='startDt' class='btn_calendar edit_mode' autocomplete='off' readonly='readonly'/>" +
								"</td>" +
								"<td class='alC line'>" +
									"<select class='startTimeHH' name='startTimeHH'>" +
										startTimeHHHtml +
									"</select>" +
									" : " +
									"<select class='startTimeMM' name='startTimeMM'>" +
										startTimeMMHtml +
									"</select>" +
								"</td>" +
								"<td class='alC line'>" +
									"<select class='endTimeHH' name='endTimeHH'>" +
										endTimeHHHtml +
									"</select>" +
									" : " +
									"<select class='endTimeMM' name='endTimeMM'>" +
										endTimeMMHtml +
									"</select>" +
								"</td>" +
								"<td class='alC line box_period'>" +
									"<input type='text' name='periodTxt'/>" +
								"</td>" +
								"<td class='alC line'>" +
									"<select class='fac edit_mode' name='facList'>" +
									facHtml +
									"</select>" +
									"<ul class='box_fac'></ul>" +
								"</td>" +
								"<td class='alC line'>" +
									"<textarea class='wid80 edit_mode' name='studySubject'></textarea>" +
								"</td>" +
								"<td class='alC line'></td>" +
								"<td class='alC line'>" +
									"<a href='#' class='btn_mngTxt btn_save'>저장</a>" +
									"<a href='#' class='btn_mngDelTxt btn_rowDel' data-id='${scheduleMngVO.plId}' data-crclid='${curriculumVO.crclId}'>삭제</a>" +
								"</td>" +
							"</tr>";
				</c:when>
				<c:otherwise>
					html = "<tr class='scheduleTr'>" +
								"<td class='alC line'>" +
									"<input type='text' name='sisu' class='wid50p sisu'/>" +
									"<input type='hidden' name='crclId' value='${curriculumVO.crclId}'/>" +
								"</td>" +
								"<td class='alC line'>" +
									"<input type='text' name='startDt' class='btn_calendar edit_mode' autocomplete='off' readonly='readonly'/>" +
								"</td>" +
								"<td class='alC line'>" +
									"<select class='startTime edit_mode' name='startTime'>" +
										startTimeHtml +
									"</select>" +
								"</td>" +
								"<td class='alC line'>" +
									"<select class='endTime edit_mode' name='endTime'>" +
										endTimeHtml +
									"</select>" +
								"</td>" +
								"<td class='alC line box_period'></td>" +
								"<td class='alC line'>" +
									"<select class='fac edit_mode'>" +
									facHtml +
									"</select>" +
									"<ul class='box_fac'></ul>" +
								"</td>" +
								"<td class='alC line'>" +
									"<textarea class='wid80 edit_mode' name='studySubject'></textarea>" +
								"</td>" +
								"<td class='alC line'></td>" +
								"<td class='alC line'>" +
									"<a href='#' class='btn_mngTxt btn_save'>저장</a>" +
									"<a href='#' class='btn_mngDelTxt btn_rowDel' data-id='${scheduleMngVO.plId}' data-crclid='${curriculumVO.crclId}'>삭제</a>" +
								"</td>" +
							"</tr>";
				</c:otherwise>
			</c:choose>
			
			
					
			$(".box_timetable").append(html);
		}else{
			alert("총 시수 " + totalTime + "시간까지 시간표 등록이 가능합니다.");
		}
		
		return false;
	});
	
	//출석
	$(".atd").click(function(){
		var id = $(this).data("id"),
			sbj = $(this).data("nm"),
			crclId = $(this).data("crclid"),
			attTCnt = $(this).data("tot"),
			attYCnt = $(this).data("ycnt"),
			attLCnt = $(this).data("lcnt"),
			attNCnt = $(this).data("ncnt");
		
		$.ajax({
			url : "/lms/atd/selectAttendList.do"
			, type : "post"
			, dataType : "html"
			, data : {plId : id, crclId : crclId, modalAt : "Y"}
			, success : function(data){
				$("#sbj").text(sbj);
				$("#plId").val(id);
				$("#atdlist").html(data);
				
				$("#goAtd").attr("href", "/mng/lms/atd/selectAttendList.do?menu=CLASS_MANAGE&crclId=" + crclId + "&plId=" + id + "&depth1=CURRICULUM_STUDY");
			}, error : function(){
				alert("error");
			}
		});
		
		return false;
	});
	
});

//시작시간
$(document).on("change", ".startTime", function(){
	var sisu = $(this).parents("tr").find(".sisu").val(),
		startTime = $(this).val(),
		startP = $(this).find("option:selected").data("period"),
		selEq = 0,
		sumSisu = 0,
		cnt = 0,
		period = "";
	
	if(!sisu){
		sisu = 1;
	}
	
	sumSisu = parseInt(startP) + parseInt(sisu);
	selEq = sumSisu - 1;
	
	$(this).parents("tr").find(".endTime option:eq("+selEq+")").prop("selected", true);
	for(i = startP; i < sumSisu; i++){
		if(cnt == 0){
			period = i;
		}else{
			period += "," + i;
		}
		cnt++;
	}
	
	period += "<input type='hidden' name='periodTxt' value='"+period+"'/>";
	$(this).parents("tr").find(".box_period").html(period);
	
	sisuTotCnt();
	return false;
});

//종료시간
$(document).on("change", ".endTime", function(){
	var startTime = $(this).parents("tr").find(".startTime").val(),
		startP = $(this).parents("tr").find(".startTime option:selected").data("period"),
		endP = $(this).find("option:selected").data("period"),
		selEq = 0,
		sumSisu = 0,
		cnt = 0,
		period = "";
	
	sumSisu = parseInt(endP) - parseInt(startP) + 1;
	$(this).parents("tr").find(".sisu").val(sumSisu);
	for(i = startP; i < (endP + 1); i++){
		if(cnt == 0){
			period = i;
		}else{
			period += "," + i;
		}
		cnt++;
	}
	
	period += "<input type='hidden' name='periodTxt' value='"+period+"'/>";
	$(this).parents("tr").find(".box_period").html(period);
	
	sisuTotCnt();
	return false;
});

//교수명
$(document).on("change", ".fac", function(){
	var userId = $(this).val(),
		userNm = $(this).find("option:selected").data("nm"),
		cnt = 0;
	
	if(userId){
		$(this).siblings(".box_fac").find("input[name=facIdList]").each(function(){
			if($(this).val() == userId){
				cnt++;
				return false;
			}
		});
		
		if(cnt == 0){
			html = "<li><span>"+userNm+"</span> <input type='hidden' name='facIdList' value='"+userId+"'/> <a href='#' class='btn_delfac'><img src='/template/manage/images/btn/del.gif'/></a></li>";
			$(this).parents("tr").find(".box_fac").append(html);
		}else{
			alert("이미 등록된 교원입니다.");
		}
	}
});

//교수명 삭제
$(document).on("click", ".btn_delfac", function(){
	$(this).parents("li").remove();
	return false;
});

//수정
$(document).on("click", ".btn_edit", function(){
	var editMode = false;
	if($(this).hasClass("past")){
		if(confirm("이미 완료된 수업입니다. 그래도 수정하시겠습니까?")){
			editMode = true;
		}else{
			editMode = false;
		}
	}else{
		editMode = true;
	}
	
	if(editMode){
		$(this).parents(".box_btn").hide().next().show();
		
		$(this).parents("tr").find(".view_mode").hide();
		$(this).parents("tr").find(".edit_mode").show();
	}
	
	return false;
});

//삭제
$(document).on("click", ".btn_del", function(){
	
	if(!confirm("삭제하시겠습니까?"))
		return false;
	
	var id = $(this).data("id"),
		crclId = $(this).data("crclid");
	
	var target_row = $(this);
	
	$.ajax({
		url : "/mng/lms/manage/dltSchedule.json"
		, type : "post"
		, dataType : "json"
		, data : {plId : id, crclId : crclId}
		, success : function(data){
			alert("삭제하였습니다.");
			target_row.parents("tr").remove();
			console.log(data.successYn);
		}, error : function(){
			alert("error");
		}
	}).done(function(){
		sisuTotCnt();
	});
	
	return false;
});


//삭제
$(document).on("click", ".btn_rowDel", function(){

	var id = $(this).data("id");
	var crclId = $(this).data("crclid");
	var dataDelFlag = 'N';
	
	if(id != null && id != '' && id != 'undefined')
		dataDelFlag ='Y';
		
	var target_row = $(this);
	
	if(dataDelFlag == 'Y'){
		if(!confirm("저장되어 있는 데이터입니다.\n 삭제하시겠습니까?"))
			return false;
		
		$.ajax({
			url : "/mng/lms/manage/dltSchedule.json"
			, type : "post"
			, dataType : "json"
			, data : {plId : id, crclId : crclId}
			, success : function(data){
				alert("삭제하였습니다.");
				target_row.parents("tr").remove();
			}, error : function(){
				alert("error");
			}
		}).done(function(){
			sisuTotCnt();
		});
		
	} else {
		target_row.parents("tr").remove();
	}
	
	return false;
});

//시수저장&수정
$(document).on("click", ".btn_save", function(){
	var id = $(this).data("id"),
		params = $(this).parents(".scheduleTr").find(":input").serialize(),
		tr = $(this).parents(".scheduleTr"),
		url = "/mng/lms/manage/scheduleReg.do?crclLang=${curriculumVO.crclLang}",
		curTime = parseInt($(".curTime").text()),
		totalTime = parseInt($(".totalTime").text()),
		sisu = $(this).parents(".scheduleTr").find("input[name=sisu]").val(),
		startDate = $(this).parents(".scheduleTr").find("input[name=startDt]").val(),
		
		<c:choose>
			<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
				startTimeHH = $(this).parents(".scheduleTr").find("select[name=startTimeHH]").val(),
				startTimeMM = $(this).parents(".scheduleTr").find("select[name=startTimeMM]").val(),
				endTimeHH = $(this).parents(".scheduleTr").find("select[name=endTimeHH]").val(),
				endTimeMM = $(this).parents(".scheduleTr").find("select[name=endTimeMM]").val(),
				periodTxt = $(this).parents(".scheduleTr").find("input[name=periodTxt]").val();
			</c:when>
			<c:otherwise>
				startTime = $(this).parents(".scheduleTr").find("select[name=startTime]").val(),
				endTime = $(this).parents(".scheduleTr").find("select[name=endTime]").val();
			</c:otherwise>
		</c:choose>
	
	if($(this).parents(".scheduleTr").find("select[name=facList]").val() == ''){
		alert("교수를 선택해 주새요.");
		return false;
	}
		
	if(curTime <= totalTime){
		if(!startDate){
			alert("수업일을 선택해 주세요.");
		<c:choose>
			<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
				}else if(!startTimeHH){
					alert("시작시간을 선택해 주세요.");
				}else if(!startTimeMM){
					alert("시작시간 분을 선택해 주세요.");
				}else if(!endTimeHH){
					alert("종료시간을 선택해 주세요.");
				}else if(!endTimeMM){
					alert("종료시간 분을 선택해 주세요.");
				}else if((startTimeHH + "" + startTimeMM) >= (endTimeHH + "" + endTimeMM)){
					alert("종료시간이 시작시간보다 앞 시간입니다.");
				}else if(!periodTxt){
					alert("교시를 입력해 주세요.");
			</c:when>
			<c:otherwise>
				}else if(!startTime){
					alert("시작시간을 선택해 주세요.");
				}else if(!endTime){
					alert("종료시간을 선택해 주세요.");
			</c:otherwise>
		</c:choose>
		
		}else if(!sisu || sisu == 0){
			alert("시수를 입력해 주세요.");
		}else{
			$.ajax({
				url : url
				, type : "post"
				, dataType : "html"
				, data : params
				, success : function(data){
					tr.html(data);
				}, error : function(){
					alert("error");
				}
			});
			
			$(this).parents(".box_save").hide().prev().show();
		}
	}else{
		alert("총 시수 " + totalTime + "시간까지 시간표 등록이 가능합니다.");
	}
	
	return false;
});

//시수
$(document).on("change", ".sisu", function(){
	var _pattern2 = /^\d*[.]\d{2}$/;
	if(_pattern2.test($(this).val())){
		alert("소수점 첫번째 자리까지만 입력가능합니다.");
		return false;
	}
	/* $(this).val(); */
	sisuTotCnt();
});

//달력
var today = new Date(),
	startString = "${curriculumVO.startDate}",
	startDateArray = startString.split("-"),
	startDateObj = new Date(startDateArray[0], Number(startDateArray[1])-1, startDateArray[2]),
	minCnt = (startDateObj.getTime() - today.getTime())/1000/60/60/24 + 1,
	
	endString = "${curriculumVO.endDate}",
	endDateArray = endString.split("-"),
	endDateObj = new Date(endDateArray[0], Number(endDateArray[1])-1, endDateArray[2]),
	maxCnt = (endDateObj.getTime() - today.getTime())/1000/60/60/24 + 1;

$(document).on("focus", ".btn_calendar", function(){
	$(this).datepicker({
	    dateFormat: "yy-mm-dd",
	    minDate : minCnt,
	    maxDate : maxCnt
	});
});

//시수 계산
function sisuTotCnt(){
	var sisuCnt = 0;
	
	//총 시수
	$(".sisu").each(function(){
		sisuCnt += parseFloat($(this).val());
/* 		sisuCnt += Number($(this).val()); */
	});
	
	$(".curTime").text(sisuCnt);
}

//출석 검색
function searchAtd(){
	var params = $("#atdFrom").serialize();
	
	$.ajax({
		url : "/lms/atd/selectAttendList.do"
		, type : "post"
		, dataType : "html"
		, data : params
		, success : function(data){
			$("#atdlist").html(data);
		}, error : function(){
			alert("error");
		}
	});
	
	return false;
}

</script>
<c:set var="curriculumStartDate" value="${fn:replace(curriculumVO.startDate,'-','')}"/>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.step eq '2' ? '2' : '4'}"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<%-- 
	<c:if test="${curriculumVO.totalTime > 0}">
		<ul class="tab_menu mb50">
			<li><a href="/mng/lms/manage/schedule.do${_BASE_PARAM}&type=list" class="active">수업관리</a></li>
			<li><a href="/mng/lms/manage/scheduleCalendar.do${_BASE_PARAM}&type=cal">달력관리</a></li>
		</ul>
		<br/>
	</c:if>
	 --%>
	<form class="scheduleForm" name="schedule">
	<table class="chart2 mt50">
		<colgroup>
			<col width="5%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="10%"/>
			<col width="*"/>
			<col width="10%"/>
			<col width="10%"/>
		</colgroup>
		<tbody class="box_timetable">
			<tr>
			  	<th class="alC">시수</th>
			  	<th class="alC">수업일</th>
			 	<th class="alC">시작시간</th>
				<th class="alC">종료시간</th>
				<th class="alC">교시</th>
				<th class="alC">교수명</th>
				<th class="alC">수업주제</th>
				<th class="alC">출석</th>
				<th class="alC">관리</th>
		  	</tr> 
		  	<c:set var="curTime" value="0"/>
		  	<c:choose>
		  		<c:when test="${curriculumVO.totalTime > 0}">
		  			<c:forEach var="scheduleMngVO" items="${resultList}">
			  			<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
						<tr class="scheduleTr <c:if test="${today >= startDt}">backGray</c:if>">
							<td class="alC line">
								<div class="edit_mode" style="display:none;">
									<input type="text" name="sisu" class="wid50p sisu" value="${scheduleMngVO.sisu}" style="text-align: center;"/>
									<%-- <input type="number" name="sisu" class="wid50p onlyNum sisu" value="${scheduleMngVO.sisu}"/> --%>
									<input type="hidden" name="crclId" value="<c:out value="${curriculumVO.crclId}"/>"/>
									<input type="hidden" name="plId" value="<c:out value="${scheduleMngVO.plId}"/>"/>
								</div>
								<div class="view_mode"><c:out value="${scheduleMngVO.sisu}"/></div>
							</td>
							<td class="alC line">
								<div class="edit_mode" style="display:none;">
									<input type="text" name="startDt" class="btn_calendar edit_mode" value="${scheduleMngVO.startDt}" autocomplete="off" readonly="readonly"/>
								</div>
								<div class="view_mode"><c:out value="${scheduleMngVO.startDt}"/></div>
							</td>
							<td class="alC line">
								<c:choose>
									<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
										<div class="edit_mode" style="display:none;">
											<select class="startTimeHH" name="startTimeHH">
												<option value="">선택</option>
												<c:forEach var="result" begin="6" end="24" step="1">
													<c:set var="hour">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
												</c:forEach>
											</select>
											:
											<select class="startTimeMM" name="startTimeMM">
												<option value="">선택</option>
												<c:forEach var="result" begin="0" end="55" step="5">
													<c:set var="minute">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
												</c:forEach>
											</select>
										</div>
									</c:when>
									<c:otherwise>
										<select class="startTime edit_mode" name="startTime" style="display:none;">
											<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.startTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.startTime eq result.startTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.startTime,0,2)}"/>:<c:out value="${fn:substring(result.startTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</c:otherwise>
								</c:choose>
								<div class="view_mode">
									<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
								</div>
							</td>
							<td class="alC line">
								<c:choose>
									<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
										<div class="edit_mode" style="display:none;">
											<select class="endTimeHH" name="endTimeHH">
												<option value="">선택</option>
												<c:forEach var="result" begin="6" end="24" step="1">
													<c:set var="hour">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.endTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
												</c:forEach>
											</select>
											:
											<select class="endTimeMM" name="endTimeMM">
												<option value="">선택</option>
												<c:forEach var="result" begin="0" end="55" step="5">
													<c:set var="minute">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.endTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
												</c:forEach>
											</select>
										</div>
									</c:when>
									<c:otherwise>
										<select class="endTime edit_mode" name="endTime" style="display:none;">
											<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.endTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.endTime eq result.endTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.endTime,0,2)}"/>:<c:out value="${fn:substring(result.endTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</c:otherwise>
								</c:choose>
								<div class="view_mode">
									<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
								</div>
							</td>
							<td class="alC line box_period">
								<c:choose>
									<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
										<div class="view_mode">
											<c:out value="${scheduleMngVO.periodTxt}"/>
										</div>
										<input type='text' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>' class="edit_mode" style="display:none;"/>
									</c:when>
									<c:otherwise>
										<c:out value="${scheduleMngVO.periodTxt}"/>
										<input type='hidden' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>'/>
									</c:otherwise>
								</c:choose>
							</td>
							<td class="alC line">
								<input type="hidden" name="changeFacAt" value="Y"/>
								<select class="fac edit_mode" name="facList" style="display:none;">
									<option value="">선택</option>
									<c:forEach var="result" items="${facList}">
										<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
									</c:forEach>
								</select>
								<ul class="box_fac">
									<c:set var="facCnt" value="1"/>
									<c:forEach var="result" items="${facPlList}" varStatus="status">
										<c:if test="${result.plId eq scheduleMngVO.plId}">
											<li>
												<span>
													<c:out value="${result.userNm}"/>
													<c:if test="${facCnt ne 1}">(부교원)</c:if>
												</span> 
												<input type='hidden' name='facIdList' value='${result.facId}'/> 
												<a href='#' class='btn_delfac edit_mode' style="display:none;"><img src='/template/manage/images/btn/del.gif'/></a>
											</li>
											<c:set var="facCnt" value="${facCnt + 1}"/>
										</c:if>
									</c:forEach>
								</ul>
							</td>
							<td class="alC line">
								<textarea class="wid80 edit_mode" name="studySubject" style="display:none;"><c:out value="${scheduleMngVO.studySubject}"/></textarea>
								<div class="view_mode"><c:out value="${scheduleMngVO.studySubject}"/></div>
							</td>
							<td class="alC line">
								<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
								<c:choose>
									<c:when test="${today >= startDt}">
										<c:choose>
				                      		<c:when test="${not empty scheduleMngVO.attTCnt and scheduleMngVO.attTCnt == scheduleMngVO.attYCnt}">전원출석</c:when>
				                      		<c:otherwise>
				                      			<a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${scheduleMngVO.plId}" data-nm="${scheduleMngVO.studySubject}" data-crclid="${curriculumVO.crclId}" data-lcnt="${scheduleMngVO.attLCnt}" data-ncnt="${scheduleMngVO.attNCnt}" data-tot="${scheduleMngVO.attTCnt}" data-ycnt="${scheduleMngVO.attYCnt}">
					                      			<c:if test="${scheduleMngVO.attLCnt > 0}">지각 <c:out value="${scheduleMngVO.attLCnt}"/></c:if>
					                      			<c:if test="${scheduleMngVO.attLCnt > 0 and scheduleMngVO.attNCnt > 0}">,</c:if>
					                      			<c:if test="${scheduleMngVO.attNCnt > 0}">결석 <c:out value="${scheduleMngVO.attNCnt}"/></c:if>
				                      			</a>
				                      		</c:otherwise>
				                      	</c:choose>
									</c:when>
									<c:otherwise>
										대기
									</c:otherwise>
								</c:choose>
							</td>
							<td class="alC line">
								<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
								<div class="box_btn">
									<a href="#" class="btn_edit <c:if test="${today >= startDt}">past</c:if>"><img src="/template/manage/images/btn/edit.gif"></a>
									<a href="#" class="btn_del" data-id="${scheduleMngVO.plId}" data-crclid="${curriculumVO.crclId}"><img src="/template/manage/images/btn/del.gif"></a>
								</div>
								<div class="box_save" style="display:none;">
									<a href="#" class="btn_mngTxt btn_save" data-id="${scheduleMngVO.plId}">저장</a>
									<a href="#" class="btn_mngDelTxt btn_rowDel" data-id="${scheduleMngVO.plId}" data-crclid="${curriculumVO.crclId}">삭제</a>
								</div>
							</td>
						</tr>
						<c:set var="curTime" value="${curTime + scheduleMngVO.sisu}"/>
					</c:forEach>
		  		</c:when>
		  		<c:when test="${curriculumVO.totalTime > 0 and today < curriculumStartDate}">
		  			<tr>
		  				<td colspan="9" class="alC">과정 기간이 아닙니다.</td>
		  			</tr>
		  		</c:when>
		  		<c:otherwise>
		  			<tr>
		  				<td colspan="9" class="alC">시수 없음</td>
		  			</tr>
		  		</c:otherwise>
		  	</c:choose>
		</tbody>
		
		<%-- 과정기간부터 등록 가능 --%>
		<c:if test="${curriculumVO.totalTime > 0}">
			<tfoot>
				<tr>
					<td colspan="9"><a href="#" class="btn_addTime"><img src="/template/manage/images/btn/btn_plus.gif"> 시수 추가</a></td>
				</tr>
				<tr class="box_sum">
					<td colspan="9" class="alC"><span class="curTime"><c:out value="${curTime}"/></span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span></td>
				</tr>
			</tfoot>
		</c:if>
	</table>
	</form>
	
<div id="attend_view_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">출석확인</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">[<span id="sbj"></span>]</div>
        <form name="atdFrom" id="atdFrom" onsubmit="return searchAtd();">
        	<input type="hidden" name="crclId" value="${curriculumVO.crclId }"/>
        	<input id="plId" type="hidden" name="plId" value=""/>
        	<input id="modalAt" type="hidden" name="modalAt" value="Y"/>
            <select name="attentionType" style="height:40px;">
                  <option value="">전체</option>
                  <option value="Y">출석</option>
                  <option value="NY">지각 및 결석자</option>
            </select>
            <input type="text" name="userNm" value="" placeholder="이름을 입력해 주세요." style="height:35px;">
            <button class="btn_mngTxt" style="height:40px;">검색</button>
        </form>
        <div style="display:block;width:100%;height:300px;overflow-y:auto;">
	        <table class="modal-table-wrap size-sm center-align">
	          <colgroup>
	            <col width="10%">
	            <col width="30%">
	            <col width="*">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray">
	              <th class="font-700">No</th>
	              <th class="font-700">상태</th>
	              <th class="font-700">이름</th>
	            </tr>
	          </thead>
	          <tbody id="atdlist"></tbody>
	        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn_mngTxt btnModalCancel">닫기</button>
        <a href="#" id="goAtd" class="btn_mngTxt btnModalConfirm" target="_blank">출석 수정</a>
      </div>
    </div>
  </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>