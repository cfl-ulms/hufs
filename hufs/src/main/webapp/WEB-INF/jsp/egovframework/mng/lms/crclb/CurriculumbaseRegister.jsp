<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crclb"/>
<c:set var="_ACTION" value=""/>
<%--과정체계코드 --%>
<c:set var="sysCode" value="${fn:split(curriculumbaseVO.sysCodePath,'>')}"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.searchcrclbNm}"><c:param name="searchcrclbNm" value="${searchVO.searchcrclbNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${empty searchVO.crclbId}">
		<c:set var="_MODE" value="REG"/>
		<c:set var="_ACTION" value="${_PREFIX}/addCurriculumbase.do${_BASE_PARAM }"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
		<c:set var="_ACTION" value="${_PREFIX}/updateCurriculumbase.do${_BASE_PARAM}"/>
	</c:otherwise>
</c:choose>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCLB"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="기본과정관리"/>
</c:import>

<script>
$(document).ready(function(){
	//학생수강신청 제출 서류여부
	if("Y" == $("input[name=stdntAplyAt]:checked").val()){
		$(".stdntAply").show();
	}else{
		$(".stdntAply").hide();
	}
	
	$("input[name=stdntAplyAt]").click(function(){
		if("Y" == $(this).val()){
			$(".stdntAply").show();
		}else{
			$(".stdntAply").hide();
		}
	});
	
	//과정코드 생성
	$("input[name=autoIdAt]").click(function(){
		if("Y" == $(this).val()){
			$("#crclbId").prop("readonly", true);
		}else{
			$("#crclbId").prop("readonly", false);
		}
	});
	
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${sysCode[2]}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${sysCode[3]}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
	});
	
	$("#ctgryId1").change();
	
	//학점인정여부
	$("input[name=gradeAt]").click(function(){
		if("Y" == $(this).val()){
			$("#gradeNum").prop("readonly", false);
		}else{
			$("#gradeNum").val("");
			$("#gradeNum").prop("readonly", true);
		}
	});
	
	//성적처리기준
	$("input[name=evaluation]").click(function(){
		if("Y" == $(this).val()){
			$("#gradeType").prop("disabled", false);
		}else{
			$("#gradeType").find("option:eq(0)").prop("selected", true);
			$("#gradeType").prop("disabled", true);
		}
	});
	
	//수강신청제출서류
	$("input[name=surveyIndAt]").click(function(){
		if("Y" == $(this).val()){
			$("#surveyInd").prop("disabled", false);
		}else{
			$("#surveyInd").find("option:eq(0)").prop("selected", true);
			$("#surveyInd").prop("disabled", true);
		}
	});
	
	//프로젝트과정관련
	$("#control").change(function(){
		var type = $(this).val();
		if(type == "CTG_0000000000000042"){
			$("input[name=projectAt]").prop("checked", true);
			$("#reportType").val("PRJ").attr("selected", true);
		}
	})
	
	$("input[name=projectAt]").click(function(){
		if(!$(this).is(":checked") && $("#control").val() == "CTG_0000000000000042"){
			alert("프로젝트과정은 체크 해제할 수 없습니다.");
			$(this).prop("checked", true);
		}
		
		if($(this).is(":checked")){
			$("#reportType").val("PRJ").attr("selected", true);
		}
	});
	
	$("#reportType").change(function(){
		var type = $(this).val();
		if($("input[name=projectAt]").is(":checked") && type != "PRJ"){
			alert("프로젝트 과정 선택 시 운영보고서는 프로젝트과정만 선택할 수 있습니다.");
			$(this).val("PRJ").attr("selected", true);
		}
	});
	
	//양식자료 선택
	$(".btn_file1").click(function(){
		<%-- CTG_0000000000000172 : 기본과정관리 --%>
		var target = $(this).data("target");
		window.open("/mng/lms/basefile/BasefileList.do?tmplatImportAt=N&searchTarget=CTG_0000000000000172&target="+target, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");

		return false;
	});
	
	//파일자료 삭제
	$(".btn_deletefile").click(function(){
		var target = $(this).data("target");
		
		$("#" + target).val("");
		$("#" + target + "File").val("");
		$(this).hide();
		return false;
	});
});

//양식관리 파일
function file(id, name, target){
	if(target == "refe"){
		var inp = "",
			dpChk = 0;
		
		for(i = 0; i < id.length; i++){
			dpChk = 0;
			$("input[name=refeFileList]").each(function(){
				if($(this).val() == id[i]){
					alert("이미 "+name[i]+"파일은 참고자료로 등록하셨습니다.");
					dpChk++;
				}
			});
			
			if(dpChk == 0){
				inp = "<li class='refFileList'><input type='hidden' value='"+id[i]+"' name='refeFileList'/><img src='/template/manage/images/ico_file.gif' alt='파일'/> "+name[i]+" <a href='#' class='refFile'><img src='/template/manage/images/btn_sdelete.gif'/></a></li>";
				$("#refeFile_box").append(inp);	
			}
		}
	}else{
		$("#" + target).val(name);
		$("#" + target + "File").val(id);
		$("#" + target + "DelBtn").show();
	}
}

//참고자료 삭제
$(document).on("click",".refFile",function(){
	$(this).parents("li").remove();
});


//폼 validator
function vali(){
	if($("input[name=gradeAt]:checked").val() == "Y" && (!$("#gradeNum").val() || $("#gradeNum").val() == 0)){
		alert("학점을 입력해주세요.");
		return false;
	}
	
	if($("input[name=evaluationAt]:checked").val() == "Y" && !$("#gradeType").val()){
		alert("절대평가 세부항목을 선택해주세요.");
		return false;
	}
	
	if($("input[name=surveyIndAt]:checked").val() == "Y" && !$("#surveyInd").val()){
		alert("개별 수업만족도조사 실시 여부를 선택해주세요.");
		return false;
	}
	
	return;
}
</script>

<form:form commandName="curriculumbaseVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return vali();">
	<strong>*필수정보</strong>
	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><em>*</em> <label>기본과정명</label></th>
				<td><form:input path="crclbNm" cssClass="inp_long" required="true"/></td>
			</tr>
			<tr>
				<th><em>*</em> <label>기본과정코드</label></th>
				<td>
					<c:choose>
						<c:when test="${_MODE eq 'REG'}">
							<label><input type="radio" name="autoIdAt" value="Y" checked="checked"/>자동</label>&nbsp;&nbsp; 
							<label><input type="radio" name="autoIdAt" value="N"/>수동</label>
							&nbsp;<form:input path="crclbId" readonly="true"/>
						</c:when>
						<c:otherwise>
							<c:out value="${curriculumbaseVO.crclbId}"/>
							<form:hidden path="crclbId"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>과정체계</label></th>
				<td>
					<select id="ctgryId1" name="sysCodeList" required>
						<option value="">대분류</option>
						<c:forEach var="result" items="${sysCodeList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq sysCode[1]}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					<select id="ctgryId2" name="sysCodeList" required>
						<option value="">중분류</option>
					</select>
					<select id="ctgryId3" name="sysCodeList" required>
						<option value="">소분류</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<strong>*기본정보</strong>
	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><em>*</em> <label>이수구분</label></th>
				<td>
					<select id="division" name="division" required>
						<option value="">선택</option>
						<c:forEach var="result" items="${divisionList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumbaseVO.division}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
				</td>
				<th><em>*</em> <label>학점인정여부</label></th>
				<td>
					<label><input type="radio" name="gradeAt" value="Y" <c:if test="${curriculumbaseVO.gradeAt ne 'N'}">checked="checked"</c:if>/>예</label>
					&nbsp;<form:input path="gradeNum"/>&nbsp;&nbsp;
					<label><input type="radio" name="gradeAt" value="N" <c:if test="${curriculumbaseVO.gradeAt eq 'N'}">checked="checked"</c:if>/>아니오</label>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>관리구분</label></th>
				<td colspan="3">
					<select id="control" name="control" required>
						<option value="">선택</option>
						<c:forEach var="result" items="${controlList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumbaseVO.control}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<label><input type="checkbox" name="projectAt" value="Y" <c:if test="${curriculumbaseVO.projectAt eq 'Y'}">checked="checked"</c:if>/> 프로젝트 과정</label>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>대상</label></th>
				<td colspan="3">
					<select id="targetType" name="targetType" required>
						<option value="">선택</option>
						<option value="Y" <c:if test="${'Y' eq curriculumbaseVO.targetType}">selected="selected"</c:if>>본교생</option>
						<option value="N" <c:if test="${'N' eq curriculumbaseVO.targetType}">selected="selected"</c:if>>일반</option>
					</select>
					<select id="targetDetail" name="targetDetail" required>
						<option value="">선택</option>
						<c:forEach var="result" items="${targetList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumbaseVO.targetDetail}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<label><input type="checkbox" name="totalTimeAt" value="Y" <c:if test="${curriculumbaseVO.totalTimeAt eq 'Y'}">checked="checked"</c:if>/> 총 시간 등록필수</label>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>성적처리기준</label></th>
				<td colspan="3">
					<label><input type="radio" name="evaluationAt" value="Y" <c:if test="${curriculumbaseVO.evaluationAt ne 'N'}">checked="checked"</c:if>/>절대평가</label>
					&nbsp;
					<select id="gradeType" name="gradeType">
						<option value="">선택</option>
						<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '1'}">
								<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumbaseVO.gradeType}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
							</c:if>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<label><input type="radio" name="evaluationAt" value="N" <c:if test="${curriculumbaseVO.evaluationAt eq 'N'}">checked="checked"</c:if>/>상대평가</label>
				</td>	
			</tr>
			<tr>
				<th><em>*</em> <label>운영보고서 유형</label></th>
				<td colspan="3">
					<select id="reportType" name="reportType" required>
						<option value="">선택</option>
						<option value="NOR" <c:if test="${'NOR' eq curriculumbaseVO.reportType}">selected="selected"</c:if>>일반과정</option>
						<option value="PRJ" <c:if test="${'PRJ' eq curriculumbaseVO.reportType}">selected="selected"</c:if>>프로젝트과정</option>
					</select>
				</td>	
				
			</tr>
			<tr>
				<th><em>*</em> <label>과정만족도설문유형</label></th>
				<td>
					<select id="surveySatisfyType" name="surveySatisfyType" required>
						<option value="">선택</option>
						<c:forEach var="result" items="${surveyList}" varStatus="status">
							<option value="${result.schdulId }" <c:if test="${result.schdulId eq curriculumbaseVO.surveySatisfyType}">selected="selected"</c:if>>${result.schdulNm }</option>
						</c:forEach>
					</select>
				</td>	
				<th><em>*</em> <label>교원대상 과정만족도 설문유형</label></th>
				<td>
					<select id="professorSatisfyType" name="professorSatisfyType">
						<option value="">선택</option>
						<option value="" <c:if test="${empty curriculumbaseVO.professorSatisfyType and _MODE eq 'UPT'}">selected="selected"</c:if>>선택안함</option>
						<c:forEach var="result" items="${surveyList3}" varStatus="status">
							<option value="${result.schdulId }" <c:if test="${result.schdulId eq curriculumbaseVO.professorSatisfyType}">selected="selected"</c:if>>${result.schdulNm }</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th><em>*</em> <label>개별 수업만족도조사 실시 여부</label></th>
				<td colspan="3">
				
					<label><input type="radio" name="surveyIndAt" value="Y" <c:if test="${curriculumbaseVO.surveyIndAt ne 'N'}">checked="checked"</c:if>/>예</label>
					&nbsp;
					<select id="surveyInd" name="surveyInd">
						<option value="">선택</option>
						<c:forEach var="result" items="${surveyList2}" varStatus="status">
							<option value="${result.schdulId }" <c:if test="${result.schdulId eq curriculumbaseVO.surveyInd}">selected="selected"</c:if>>${result.schdulNm }</option>
						</c:forEach>
					</select>
					&nbsp;&nbsp;
					<label><input type="radio" name="surveyIndAt" value="N" <c:if test="${curriculumbaseVO.surveyIndAt eq 'N'}">checked="checked"</c:if>/>아니오</label>
				</td>	
			</tr>
			<tr>
				<th><em>*</em> <label>학생 수강신청 제출서류 여부</label></th>
				<td colspan="3">
					<label><input type="radio" name="stdntAplyAt" value="Y" <c:if test="${curriculumbaseVO.stdntAplyAt eq 'Y'}">checked="checked"</c:if>/>예</label>
					&nbsp;&nbsp;
					<label><input type="radio" name="stdntAplyAt" value="N" <c:if test="${curriculumbaseVO.stdntAplyAt ne 'Y'}">checked="checked"</c:if>/>아니오</label>
				</td>	
			</tr>
			<tr class="stdntAply">
				<th><label>신청서</label></th>
				<td colspan="3">
					<input type="text" id="aply" class="inp_long btn_file1" data-target="aply" value="<c:out value="${curriculumbaseVO.aplyFileNm}"/>" placeholder="양식자료선택" readonly="readonly"/>
					<input type="hidden" id="aplyFile" name="aplyFile" value="<c:out value="${curriculumbaseVO.aplyFile}"/>"/>
					<a href="#" class="btn_file1" data-target="aply"><img src="/template/manage/images/search_btn.gif" alt="파일선택"/></a>
					<a href="#" id="aplyDelBtn" class="btn_deletefile" data-target="aply" <c:if test="${empty curriculumbaseVO.aplyFile}">style="display:none"</c:if>><img src="/template/manage/images/btn/del.gif"> 제출양식제거</a>
				</td>	
			</tr>
			<tr class="stdntAply">
				<th><label>계획서</label></th>
				<td colspan="3">
					<input type="text" id="plan" class="inp_long btn_file1" data-target="plan" value="<c:out value="${curriculumbaseVO.planFileNm}"/>"  placeholder="양식자료선택" readonly="readonly"/>
					<input type="hidden" id="planFile" name="planFile" value="<c:out value="${curriculumbaseVO.planFile}"/>"/>
					<a href="#" class="btn_file1" data-target="plan"><img src="/template/manage/images/search_btn.gif" alt="파일선택"/></a>
					<a href="#" id="planDelBtn" class="btn_deletefile" data-target="plan" <c:if test="${empty curriculumbaseVO.planFile}">style="display:none"</c:if>><img src="/template/manage/images/btn/del.gif"> 제출양식제거</a>
				</td>	
			</tr>
			<tr class="stdntAply">
				<th><label>기타</label></th>
				<td colspan="3">
					<input type="text" id="etc" class="inp_long btn_file1" data-target="etc" value="<c:out value="${curriculumbaseVO.etcFileNm}"/>"  placeholder="양식자료선택" readonly="readonly"/>
					<input type="hidden" id="etcFile" name="etcFile" value="<c:out value="${curriculumbaseVO.etcFile}"/>"/>
					<a href="#" class="btn_file1" data-target="etc"><img src="/template/manage/images/search_btn.gif" alt="파일선택"/></a>
					<a href="#" id="etcDelBtn" class="btn_deletefile" data-target="etc" <c:if test="${empty curriculumbaseVO.etcFile}">style="display:none"</c:if>><img src="/template/manage/images/btn/del.gif"> 제출양식제거</a>
				</td>	
			</tr>
		</tbody>
	</table>
	<br/>
	<strong>*(책임교원대상) 과정 학습 참고자료(선택)</strong>
	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>참고자료</label></th>
				<td colspan="3">
					<input type="text" id="refe" class="inp_long btn_file1" data-target="refe" placeholder="양식자료선택" readonly="readonly"/>
					<a href="#" class="btn_file1" data-target="refe"><img src="/template/manage/images/search_btn.gif" alt="파일선택"/></a>
					<ul id="refeFile_box">
						<c:forEach var="result" items="${refFileList}" varStatus="status">
							<li class='refFileList'>
								<input type='hidden' value='${result.atchFileId}' name='refeFileList'/>
								<img src='/template/manage/images/ico_file.gif' alt='파일'/>
								${result.orignlFileNm}
								<a href='#' class='refFile'><img src='/template/manage/images/btn_sdelete.gif'/></a>
							</li>
						</c:forEach>
					</ul>
				</td>	
			</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }"/>
	    <c:url var="listUrl" value="/mng/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}"/>
	    <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>
</form:form>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>