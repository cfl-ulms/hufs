<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG_CANCEL_RULE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="취소/환불 규정"/>
</c:import>

<div id="cntnts">
	<c:import url="/mng/cop/bbs/forUpdateBoardArticle.do" charEncoding="utf-8">
		<c:param name="nttNo" value="0"/>
		<c:param name="siteId" value="SITE_000000000000000"/>
		<c:param name="bbsId" value="BBSMSTR_000000000025"/>
		<c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/>
		<c:param name="registAction" value="updt"/>
	</c:import>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>