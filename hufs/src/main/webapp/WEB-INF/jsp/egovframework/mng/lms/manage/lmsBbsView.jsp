<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_PREFIX" value="/mng/lms/manage"/>
<c:set var="_ACTION" value=""/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:param name="menuId" value="${param.menuId}" />
	<c:param name="bbsId" value="${param.bbsId}" />
	<c:if test="${not empty param.searchWrd}"><c:param name="searchWrd" value="${param.searchWrd}" /></c:if>
	<c:if test="${not empty param.searchClass}"><c:param name="searchClass" value="${param.searchClass}" /></c:if>
	<c:if test="${not empty param.searchGroup}"><c:param name="searchGroup" value="${param.searchGroup}" /></c:if>
	<c:if test="${not empty param.searchCnd}"><c:param name="searchCnd" value="${param.searchCnd}" /></c:if>
</c:url>
<% /*URL 정의*/ %>




<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${param.depth1 eq 'CRCL_BOARD' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
	<c:param name="depth1" value="${param.depth1 eq 'CRCL_BOARD' ? 'CRCL_BOARD' : 'BASE_CRCL'}"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.depth1 eq 'CRCL_BOARD' ? '과정 게시판' : '과정등록관리'}"/>
</c:import>

<script type="text/javascript">
function fn_egov_delete_notice(url) {

	if (confirm('<spring:message code="common.delete.msg" />')) {
		document.location.href = url;
	}
}

	$(document).ready(function(){
		
		//인쇄하기
		$("#btnPrint").click(function(){
			window.open("/lms/crcl/CurriculumBoardPrintStaff.do${_BASE_PARAM}&nttNo=${board.nttNo}&popupAt=Y&printAt=Y","popup","top=1,left=1,width=900,height=" + screen.availHeight);
		});

	});
</script>

<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm }"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
<c:if test="${curriculumVO.processSttusCode > 0}">
	<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="6"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
		<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	</c:import>
</c:if>
<ul class="box_bbsmst">
	<c:forEach var="result" items="${masterList}">
		<c:url var="masterUrl" value="/mng/lms/manage/lmsBbsList.do">
			<c:param name="bbsId" value="${result.bbsId}" />
			<c:param name="crclId" value="${searchVO.crclId}" />
			<c:param name="menu" value="${param.depth1 eq 'CRCL_BOARD' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
			<c:param name="depth1" value="${param.depth1 eq 'CRCL_BOARD' ? 'CRCL_BOARD' : 'BASE_CRCL'}"/>
		</c:url>
		<li <c:if test="${result.bbsId eq searchVO.bbsId}">class="on"</c:if>>
			<a href="${masterUrl}">
				<c:choose>
					<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
					<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
				</c:choose>
				<c:out value="${result.bbsNm}"/>
			</a>
		</li>
	</c:forEach>

</ul>

<div id="cntnts">

	<div class="view_wrap">
		<dl class="view_tit">
			<dt><spring:message code="cop.nttSj" /></dt>
			<dd><strong><c:out value="${board.nttSj}" /></strong></dd>
		</dl>

		<table  class="view_writer_chart">
			<caption></caption>
			<colgroup>
				<col width="110px" />
				<col width="*" />
				<col width="110px" />
				<col width="*" />
			</colgroup>
			<tbody>
                <c:if test="${!empty brdMstrVO.ctgrymasterId}">
	                <tr>
		                <th>게시글 구분</th>
		                <td colspan="3" class="last"><c:out value="${board.ctgryNm}" /></td>
	                </tr>
                </c:if>
                <tr>
	                <th><spring:message code="cop.ntcrNm" /></th>
	                <td colspan="3" class="last"><c:out value="${board.ntcrNm}" /> (<c:out value="${board.frstRegisterId}" />)</td>
                </tr>
                <tr>
                	<th><spring:message code="cop.frstRegisterPnttm" /></th>
	                <td><fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                <th><spring:message code="cop.inqireCo" /></th>
	                <td class="last"><c:out value="${board.inqireCo}" /></td>
                </tr>
 			</tbody>
		</table>
         <div class="view_cont">
			<c:out value="${board.nttCn}" escapeXml="false" />
		</div>

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
			<dl class="view_tit02">
				<dt><spring:message code="cop.atchFileList" /></dt>
				<dd>
					<ul class="list">
						<c:if test="${not empty board.atchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
								<c:param name="mngAt" value="Y"/>
							</c:import>
						</c:if>
					</ul>
				</dd>
			</dl>
		</c:if>
		<%--
		<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and board.processSttusCode eq 'QA03'}">
			<c:if test="${not empty board.estnData}">
				<div class="view_cont">
					<c:out value="${board.estnParseData.cn}" escapeXml="false" />
				</div>
			</c:if>
			<c:if test="${not empty board.estnAtchFileId}">
				<dl class="view_tit02">
					<dt><spring:message code="cop.atchFileList" /></dt>
					<dd>
						<ul class="list">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${board.estnAtchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
							</c:import>
						</ul>
					</dd>
				</dl>
			</c:if>
		</c:if>
		 --%>
	</div>

	<div style="margin-top: 20px;">
		<ul style="overflow: hidden;">
			<li style="float: left;">
				<c:choose>
					<c:when test="${param.depth1 eq 'CRCL_BOARD'}">
						<c:url var="listUrl" value="/mng/lms/cla/curriculumBoardList.do"/>
					</c:when>
					<c:otherwise>
						<c:url var="listUrl" value="/mng/lms/manage/lmsBbsList.do">
							  	<c:param name="crclId" value="${searchVO.crclId}"/>
								<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
						        <c:param name="bbsId" value="${brdMstrVO.bbsId}" />
						        <c:param name="trgetId" value="${searchVO.trgetId}" />
						        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
								<c:param name="searchCnd" value="${searchVO.searchCnd}" />
								<c:param name="searchWrd" value="${searchVO.searchWrd}" />
								<c:param name="searchCate" value="${searchVO.searchCate}" />
								<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
						  			<c:if test="${not empty searchCate}">
						  				<c:param name="searchCateList" value="${searchCate}" />
						  			</c:if>
						  		</c:forEach>
						  		<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
								<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
				       </c:url>
						<%-- <c:url var="listUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}"/> --%>
					</c:otherwise>
				</c:choose>
				<a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
				<a id="btnPrint"><img src="${_IMG}/btn/btn_print1.gif" alt="인쇄"/></a>
			</li>
			<li style="float: right;">
		      	<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
		      		<c:param name="nttNo" value="${board.nttNo}" />
			  		<c:param name="registAction" value="updt" />
			  		<c:param name="depth1" value="CRCL_BOARD" />
				</c:url>
		      	<a href="<c:out value="${forUpdateBoardArticleUrl}"/>"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>
		      	<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
		      		<c:param name="nttNo" value="${board.nttNo}" />
				</c:url>
		      	<a href="<c:out value="${deleteBoardArticleUrl}"/>" onclick="fn_egov_delete_notice(this.href);return false;"><img src="${_IMG}/btn/btn_del.gif" alt="삭제"/></a>
			</li>
		</ul>
	</div>
	

	<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
    		<c:import url="${_PREFIX}/selectCommentList.do" charEncoding="utf-8">
    			<c:param name="tmplatId" value="${brdMstrVO.tmplatId}" />
    		</c:import>
    </c:if>

</div>

	<br/><br/><br/><br/>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>