<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCtgryId}"><c:param name="searchCtgryId" value="${searchVO.searchCtgryId}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchSubmitN}"><c:param name="searchSubmitN" value="${searchVO.searchSubmitN}" /></c:if>
	<c:if test="${not empty searchVO.searchSubmitI}"><c:param name="searchSubmitI" value="${searchVO.searchSubmitI}" /></c:if>
	<c:if test="${not empty searchVO.searchSubmitF}"><c:param name="searchSubmitF" value="${searchVO.searchSubmitF}" /></c:if>
	<c:if test="${not empty searchVO.searchSchdulClCode}">
		<c:forEach items="${searchVO.searchSchdulClCode}" var="result">
			<c:param name="searchSchdulClCode" value="${result}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty searchVO.searchKeyWord}"><c:param name="searchKeyWord" value="${searchVO.searchKeyWord}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CLASS_MANAGE"/>
	<c:param name="depth1" value="CRCL_SURVEY"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="설문"/>
</c:import>
<style>
.backGray{
	background : #ccc;
}

</style>
<script>
$(document).ready(function(){

	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
		$("input:checkbox").attr("checked", false);
		$("#searchKeyWord").val("");
	});

	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
		$("input:checkbox").attr("checked", false);
		$("#searchKeyWord").val("");
	});
});

</script>

<div id="cntnts">
<c:set var="now" value="<%=new java.util.Date()%>" />
	<div id="bbs_search">
	  	<form id="frm" method="post" action="<c:url value="/mng/lms/cla/classSurveyList.do"/>">
			<strong>주관기관 : </strong>
	  		<select id="searchCtgryId" name="searchCtgryId">
				<option value="">전체</option>
				<c:forEach var="result" items="${ctgryList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1' }">
						<option value="${result.ctgryId}" ${searchVO.searchCtgryId eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>
			<strong>과정명: </strong>
	  		<input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm }" class="inp_s" id="inp_text" />
			<br/>
				<c:forEach items="${searchVO.searchSchdulClCode}" var="result">
					<c:choose>
						<c:when test="${result eq 'TYPE_1' }">
							<c:set var="searchSchdulClCode1" value="TYPE_1"/>
						</c:when>
						<c:otherwise>
							<c:set var="searchSchdulClCode2" value="TYPE_2"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			<strong>설문그룹 : </strong>
	  		<label><input type="checkbox" name="searchSchdulClCode" value="TYPE_1" ${searchSchdulClCode1 eq 'TYPE_1' ? 'checked' : '' }/>과정만족도</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="searchSchdulClCode" value="TYPE_2" ${searchSchdulClCode2 eq 'TYPE_2' ? 'checked' : '' }/>수업만족도</label>&nbsp;&nbsp;
			<br/>
			<strong>제출현황 : </strong>
	  		<label><input type="checkbox" name="searchSubmitN" value="Y" ${searchVO.searchSubmitN eq 'Y' ? 'checked' : '' }/>미제출</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="searchSubmitI" value="Y" ${searchVO.searchSubmitI eq 'Y' ? 'checked' : '' }/>제출 진행중</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="searchSubmitF" value="Y" ${searchVO.searchSubmitF eq 'Y' ? 'checked' : '' }/>제출 완료</label>&nbsp;&nbsp;
	  		<label><strong>만족도 : </strong> <input type="number" name="searchKeyWord" value="${searchVO.searchKeyWord }" class="inp_s" id="searchKeyWord" maxlength="3"/>이상</label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;"/>
		</form>
	</div>
	<div>
		<h2>총 ${surveyListCnt}개</h2>
	</div>
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
			<col width="300px"/>
			<col/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
		</colgroup>
	    <thead>
	      <tr>
	        <th>No</th>
	        <th>주관기관</th>
	        <th>과정명</th>
	        <th>설문구분</th>
	        <th>설문 제출 시작일</th>
	        <th>설문 제출 마감일</th>
	        <th>제출현황</th>
	        <th>만족도</th>
	      </tr>
	    </thead>
	    <tbody>
	    <c:set var="now" value="<%=new java.util.Date()%>" />
		<c:set var="nowDate"><fmt:formatDate value="${now}" pattern="yyyy-MM-dd" /></c:set>
	    	<c:choose>
	    		<c:when test="${not empty surveyList}">
			    	<c:forEach var="result" items="${surveyList}" varStatus="status">
				    	<c:url var="viewUrl" value="/mng/lms/manage/curriculumSurveyView.do${_BASE_PARAM}">
							<c:param name="schdulId" value="${result.schdulId}"/>
							<c:param name="crclId" value="${result.crclId}"/>
							<c:if test="${result.schdulClCode eq 'TYPE_2' }">
								<c:param name="plId" value="${result.id}"/>
							</c:if>
							<c:param name="depth1" value="CRCL_SURVEY"/>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'">
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<td>${result.ctgryNm}</td>
			    			<td class="alL">${result.crclNm}</td>
			    			<td>${result.type }</td>
			    			<td>${result.endDate }
								<%-- <c:choose>
									<c:when test="${fn:indexOf(result.dDay,'-') != -1 }">
										( D${fn:replace(result.dDay,'-', '+')} )
									</c:when>
									<c:otherwise>
										( D-${result.dDay} )
									</c:otherwise>
								</c:choose> --%>
							</td>
							<td>${result.resultStartDate }</td>
			    			<td>
			    			<fmt:formatNumber value="${result.joinCnt}" type="number" var="joinCnt" />
							<fmt:formatNumber value="${result.memberCnt}" type="number" var="memberCnt" />
				    			<c:choose>
				    				<c:when test="${memberCnt ne 0 and  memberCnt eq joinCnt}">
				    					제출완료
				    				</c:when>
				    				<c:when test="${result.schdulClCode eq 'TYPE_1' }">
				    					<fmt:parseDate var="tempDate" value="${result.surEndDate }" pattern="yyyy-MM-dd"/>
				    					<fmt:formatDate value="${tempDate}" pattern="yyyy-MM-dd" var="maxDate"/>
				    					<c:choose>
				    						<c:when test="${nowDate < maxDate  }">
				    							대기
				    						</c:when>
				    						<c:otherwise>
				    							${result.joinCnt } / ${ result.memberCnt}
				    						</c:otherwise>
				    					</c:choose>
				    				</c:when>
				    				<c:when test="${result.schdulClCode eq 'TYPE_2' }">
				    				<fmt:parseDate var="tempDate" value="${result.surEndDate }" pattern="yyyy-MM-dd"/>
				    				<fmt:formatDate value="${tempDate}" pattern="yyyy-MM-dd" var="endDate"/>

										<c:choose>
				    						<c:when test="${nowDate < maxDate  }">
				    							대기
				    						</c:when>
				    						<c:otherwise>
				    							${result.joinCnt } / ${ result.memberCnt}
				    						</c:otherwise>
				    					</c:choose>
				    				</c:when>
				    			</c:choose>
			    			</td>
			    			<td>
			    				<c:if test="${result.sum ne 0 }">
									<fmt:formatNumber var="resultPoint"  pattern="0" value=" ${(result.sum / result.joinCnt)*100} " />
								</c:if>
								 ${result.sum eq 0 ? '-' :  resultPoint}
			    			</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</tbody>
    </table>

 	 <div id="paging">
		<div id="paging">
	    	<c:url var="pageUrl" value="/mng/lms/cla/classSurveyList.do${_BASE_PARAM}">
	    </c:url>
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
