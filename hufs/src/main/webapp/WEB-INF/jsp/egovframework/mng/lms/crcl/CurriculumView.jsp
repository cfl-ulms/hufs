<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${param.menu eq 'CLASS_MANAGE'}">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CLASS_MANAGE"/>
			<c:param name="depth1" value="${param.depth1}"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value=""/>
		</c:import>
	</c:when>
	<c:when test="${searchVO.mngAt eq 'Y'}">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CURRICULUM_MANAGE"/>
			<c:param name="depth1" value="BASE_CRCL_MNG"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="교육과정개설신청관리"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CURRICULUM_MANAGE"/>
			<c:param name="depth1" value="BASE_CRCL"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="과정등록관리"/>
		</c:import>
	</c:otherwise>
</c:choose>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCodeDate}"/>
<c:choose>
	<%-- 
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
	 --%>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
$(document).ready(function(){
	//과정확정
	$(".btn_confirm").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//과정승인
	$(".btn_apr").click(function(){
		var href = $(this).attr("href");
		
		if(!$("input[name=crclOutcome]:checked").val()){
			alert("과정성과를 선택해 주세요.");
		}else{
			if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 승인 하시겠습니까?")){
				$("#curriculumVO").attr("action", href);
 				$("input[name=aprvalAt]").val("Y");
				$("#curriculumVO").submit();
			}
		}
		
		return false;
	});
	
	//과정반려
	$(".btn_rjt").click(function(){
		var href = $(this).attr("href"),
			txt = "교수님, 먼저 교육과정개설 신청을 진행해 주셔서 감사드립니다.\n신청해 주신 교육과정은 개설되지 못했습니다. 이점 양해 부탁드립니다.\n문의사항은 운영팀으로 연락 주시면 안내드리도록 하겠습니다.";
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 반려 하시겠습니까?")){
			$("#curriculumVO").attr("action", href);
			$("input[name=aprvalAt]").val("N");
			if(!$("#comnt").val()){
				$("#comnt").val(txt);
			}
			$("#curriculumVO").submit();
		}
		
		return false;
	});
	
	//승인취소
	$(".btn_aprCcl").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 승인 취소하시겠습니까?")){
			if(!$("#aprvalDn").val()){
				alert("승인 취소사유를 입력 후 승인취소 버튼을 눌러주세요.");
				$(".box_aprDn").show();
				$("#aprvalDn").focus();	
			}else{
				$("#curriculumVO").attr("action", href);
				$("input[name=aprvalAt]").val("D");
				$("#curriculumVO").submit();
			}
		}
		
		return false;
	});
	
	//반려취소
	$(".btn_aprRcl").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 반려 취소하시겠습니까?")){
			if(!$("#aprvalDn").val()){
				alert("반려 취소사유를 입력 후 반려취소 버튼을 눌러주세요.");
				$(".box_aprDn").show();
				$("#aprvalDn").focus();	
			}else{
				$("#curriculumVO").attr("action", href);
				$("input[name=aprvalAt]").val("R");
				$("#curriculumVO").submit();
			}
		}
		
		return false;
	});
	
	//코멘트등록
	$(".btn_cmt").click(function(){
		var href = $(this).attr("href");
		
		$("input[name=aprvalAt]").val("");
		$("#curriculumVO").attr("action", href);
		$("#curriculumVO").submit();
		
		return false;
	});
	
	//교원수 - 초기화
	facTotCnt();
	
	//인쇄하기
	$("#btnPrint").click(function(){
		window.open("/lms/crcl/selectCurriculum.do?crclId=${curriculumVO.crclId}&popupAt=Y&printAt=Y","popup","top=1,left=1,width=1024,height=" + screen.availHeight);
	});
	
	box_lesson();
});

//교원 수
function facTotCnt(){
	var dupCnt = 0,
		ids = [],
		idList = [];
	
	$(".facIdList").each(function(){
		var id = $(this).val(),
			dupAt = false;
		$.each(idList, function(i){
			if(idList[i] == id){
				dupAt = true;
			}
		});
		
		if(!dupAt){
			idList.push(id);
		}
	});
	
	$(".sumUser").text(idList.length);
}

function box_lesson(){
	var htmls = '';
	var crclId = '${curriculumVO.crclId}';
	var preLessonOrder = 0;
	var lessonOrderCnt = 0;
	var totLen =0;
	
	$.ajax({
           type: "POST",
           url: "/ajax/mng/lms/crcl/selectCurriculumLesson.json",
           data: {                
               "crclId" : crclId
               },
           dataType: "json",
           async: false,
           success: function(data, status) {
           	console.log(data);
           	console.log(data.lessonList);
           	$(".box_lesson > tr").remove();
           	if(data.status == "200"){
           		totLen=data.lessonList.length;
           		
           		$.each(data.lessonList, function(index){
           			
           			if(lessonOrderCnt == 0){
           				lessonOrderCnt = this.lessonOrderCnt;
           				preLessonOrder = this.lessonOrder;
    					htmls += '<tbody class="box_lesson">';
    					htmls += '<tr class="first">';
   					} else {
    					htmls += '<tr class="contentBody">';
   						
   					}
                   	 
   					if((lessonOrderCnt == this.lessonOrderCnt) && (preLessonOrder == this.lessonOrder)){
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line alC num">';
   						htmls += this.lessonOrder;
   						htmls += '</td>';
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line lesnm">'+this.lessonNm+'</td>';
   					}
           			
					htmls += '<td class="line">';
/* 					htmls += '<td class="line">'+this.chasiNm+'</td>'; */
					htmls += '<textarea class="chasiList wid100 lessonNmList inp_long" name="chasiList" readonly="readonly" style="height:50px;">'+this.chasiNm+'</textarea>';
					htmls += '</td>';
					htmls += '</tr>';
					if((lessonOrderCnt-1) == 0){
    					htmls += '</tbody">';
					}
					preLessonOrder = this.lessonOrder;
   					--lessonOrderCnt;
           		});
           		
				$("#box_addLes").before(htmls);
           	}
           },
           error: function(xhr, textStatus) {
               alert("문제가 발생하였습니다. \n 관리자에게 문의하세요");
               document.write(xhr.responseText);
               return false;
           },beforeSend:function() {
           },
           complete:function() {
           }
       }); 
}


</script>
	<table class="chart2 mb50">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.step eq '1' ? '1' : '3'}"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<a href="#" id="btnPrint" class="btn_mngTxt fR">인쇄하기</a>
	<br/><br/>
	<strong>*기본과정 </strong>
	<table class="chart2">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>년도</label></th>
				<td>
					<c:out value="${curriculumVO.crclYear }"/>
				</td>
				<th><label>학기</label></th>
				<td>
					<c:out value="${curriculumVO.crclTermNm}"/>
				</td>
				<th><label>언어</label></th>
				<td>
					<c:out value="${curriculumVO.crclLangNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>과정체계</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.sysCodeNmPath}"/>
				</td>
			</tr>
			<tr>
				<th><label>기본과정명</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.crclbNm}"/>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>이수구분</label></th>
				<td>
					<c:out value="${curriculumVO.divisionNm }"/>
				</td>
				<th><label>학점인정여부</label></th>
				<td>
					<c:choose>
						<c:when test="${curriculumVO.gradeAt eq 'Y'}">예</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
				<th><label>관리구분</label></th>
				<td colspan="3">
					<c:out value="${curriculumVO.controlNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>대상</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.targetType eq 'Y'}">본교생</c:when>
    					<c:otherwise>일반</c:otherwise>
    				</c:choose>
					> <c:out value="${curriculumVO.targetDetailNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>성적처리기준</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.evaluationAt eq 'Y'}">절대평가</c:when>
    					<c:otherwise>상대평가</c:otherwise>
    				</c:choose>
    				<c:if test="${not empty curriculumVO.gradeType}">
    					> <c:out value="${curriculumVO.gradeTypeNm}"/>
    				</c:if>
				</td>
			</tr>
			<tr>
				<th><label>운영보고서 유형</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.reportType eq 'NOR'}">일반형</c:when>
    					<c:when test="${curriculumVO.reportType eq 'PRJ'}">프로젝트형</c:when>
    				</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>과정만족도 설문 유형</label></th>
				<td>
					<c:forEach var="result" items="${surveyList}" varStatus="status">
						<c:if test="${result.schdulId eq curriculumVO.surveySatisfyType}">${result.schdulNm}</c:if>
					</c:forEach>
				</td>
				<th><label>교원대상 과정만족도 설문 유형</label></th>
				<td colspan="3">
					<c:choose>
						<c:when test="${empty curriculumVO.professorSatisfyType}">
							선택안함
						</c:when>
						<c:otherwise>
							<c:forEach var="result" items="${surveyList3}" varStatus="status">
								<c:if test="${result.schdulId eq curriculumVO.professorSatisfyType}">${result.schdulNm}</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>개별 수업만족도조사 실시여부</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.surveyIndAt eq 'Y'}">예</c:when>
    					<c:otherwise>아니오</c:otherwise>
    				</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>학생 수강신청 제출서류 여부</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.stdntAplyAt eq 'Y'}">예</c:when>
    					<c:otherwise>아니오</c:otherwise>
    				</c:choose>
				</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<strong>*교육과정 내용</strong>&nbsp;&nbsp;
	<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>과정기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/>
				</td>
			</tr>
			<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
				<tr class="timeAt">
					<th><label>총 시간</label></th>
					<td><c:out value="${curriculumVO.totalTime}"/>시간</td>
					<th><label>주 횟수</label></th>
					<td><c:out value="${curriculumVO.weekNum}"/>회</td>
					<%-- 
					<th><label>일 시간</label></th>
					<td><c:out value="${curriculumVO.dayTime}"/>시간</td>
					 --%>
					<th><label>강의 시간</label></th>
					<td><c:out value="${curriculumVO.startTime}"/> (일일 <c:out value="${curriculumVO.dayTime}"/>시간)</td>
				</tr>
				<tr>
					<th><label>강의요일</label></th>
					<td colspan="5">
						<c:out value="${curriculumVO.lectureDay}"/>
					</td>
				</tr>
			</c:if>
			<tr>
				<th><label>과정 진행 캠퍼스</label></th>
				<td colspan="3">
					<c:out value="${curriculumVO.campusNm}"/>
					(<c:out value="${curriculumVO.campusPlace}"/>)
				</td>
				<th><label>수업 시간표 설정</label></th>
				<td>
					<c:choose>
						<c:when test="${curriculumVO.campustimeUseAt eq 'Y'}">캠퍼스 시간표</c:when>
						<c:otherwise>시간표 수기입력</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>주관기관/학과</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.hostCodeNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>책임교원 </label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.userNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>과정 개요 및 목표</label></th>
				<td colspan="5"><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
			</tr>
			<tr>
				<th><label>과정의 기대효과</label></th>
				<td colspan="5"><c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/></td>
			</tr>
			
			<tr>
				<th><label>과정 계획서 작성기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.planStartDate}"/> ~ <c:out value="${curriculumVO.planEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>수강신청기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>등록기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.tuitionStartDate}"/> ~ <c:out value="${curriculumVO.tuitionEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>수강정원</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
						<c:otherwise>
							<c:out value="${curriculumVO.applyMaxCnt}"/>명
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>주관학과 배정여부</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.assignAt eq 'Y'}">예</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>등록금 납부대상과정 여부</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.tuitionAt eq 'Y'}">
							예(
								<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">수업료</c:if>
								<c:if test="${not empty curriculumVO.registrationFees or curriculumVO.registrationFees != 0}">
									<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">,</c:if>
									등록비
								</c:if>
							)
						</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>등록금액</label></th>
				<td colspan="5">
					수업료 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.tuitionFees}" />원
					등록비 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.registrationFees}" />원
				</td>
			</tr>
			
		</tbody>
	</table>
	
	<strong>* 사업비(소요예산)</strong>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="15%"/>
			<col width="10%"/>
			<col width="*"/>
			<col width="25%"/>
		</colgroup>
		<caption>등록폼</caption>
		<tbody>
			<tr>
				<th class="alC">지원비 비목</th>
				<th class="alC">계정 목</th>
				<th class="alC">금액(원)</th>
				<th class="alC">산출근거</th>
				<th class="alC">비고</th>
			</tr>
			<%-- 지원비 비목 묶어주는 역할 --%>	
			<c:set var="rowspan" value="0"/>
			<c:set var="rowspanList" value=""/>
			<c:set var="prevCode" value="${expense[0].typeCode}"/>
			<c:forEach var="list" items="${expense}" varStatus="status">
				<c:choose>
					<c:when test="${prevCode eq list.typeCode}">
						<c:set var="rowspan" value="${rowspan + 1}"/>
					</c:when>
					<c:otherwise>
						<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
						<c:set var="rowspan" value="1"/>
						<c:set var="prevCode" value="${list.typeCode}"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
			<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
			<c:set var="listCnt" value="${rowspan[0]}"/>
			<c:set var="rowspanCnt" value="0"/>
			
			<c:set var="prevCode" value=""/>
			<c:forEach var="result" items="${expense}" varStatus="status">
				<tr>
					<c:if test="${prevCode ne result.typeCode}">
						<td class="line" rowspan="${rowspan[rowspanCnt]}"><c:out value="${result.typeCodeNm}"/></td>
						<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
					</c:if>
					<c:set var="prevCode" value="${result.typeCode}"/>
					<td class="line"><c:out value="${result.accountCodeNm}"/></td>
					<td class="line"><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.amount}" />원</td>
					<td class="line"><c:out value="${result.reason}"/></td>
					<td><c:out value="${result.etc}"/></td>
				</tr>
			</c:forEach>
			<tr>
				<c:choose>
					<c:when test="${empty curriculumVO.totAmount or curriculumVO.totAmount eq '0' }"><td class="alC" colspan="5">무료</td></c:when>
					<c:otherwise>
						<td class="alC" colspan="2">계</td>
						<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.totAmount}" />원</td>
						<td></td>
						<td></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</tbody>
	</table>
	
	<c:choose>
		<c:when test="${searchVO.mngAt eq 'Y'}">
			<strong>*기타사항</strong>
			<table class="chart2 mb50">
				<tbody>
					<tr>
						<td>
							<c:choose>
								<c:when test="${empty curriculumVO.etcCn}">기타사항 없음</c:when>
								<c:otherwise><c:out value="${curriculumVO.etcCn}"/></c:otherwise>
							</c:choose>
						</td>	
					</tr>
				</tbody>
			</table>
			<hr/>
			<p class="btn_c"><span>신청서 끝.</span></p>
			<div class="btn_c">
				<c:url var="uptUrl" value="/mng/lms/crcl/updateCurriculumView.do${_BASE_PARAM}">
					<c:param name="crclId" value="${curriculumVO.crclId}" />
				</c:url>
				<a href="${uptUrl}" class="btn_mngTxt">신청서 수정</a>
				<br/>
				<span>신청서를 운영팀에서 수정하고 승인하실 수 있습니다.</span>
			</div>
			<br/>
			<br/>
			
			<form action="" method="post" id="curriculumVO" name="curriculumVO">
				<input type="hidden" name="aprvalAt" value="${curriculumVO.aprvalAt}"/>
				<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
				
				<c:choose>
					<c:when test="${curriculumVO.aprvalAt ne 'Y'}">
						<strong>*과정 성과</strong>
						<table class="chart2 mb50">
							<tbody>
								<tr>
									<td>
										<ul class="outList">
											<c:forEach var="result" items="${listComCode}" varStatus="status">
												<li><label><input type="radio" name="crclOutcome" value="${result.code}" <c:if test="${result.code eq curriculumVO.crclOutcome}">checked="checked"</c:if>/>&nbsp;${result.codeNm}</label></li>
											</c:forEach>
										</ul>
									</td>	
								</tr>
							</tbody>
						</table>
						<br/>
						<strong>*운영팀 코멘트</strong><c:if test="${!empty curriculumVO.comnt}">(<fmt:formatDate value="${curriculumVO.comntPnttm}"  pattern="yyyy-MM-dd"/>)</c:if>
						<table class="chart2 mb50">
							<colgroup>
								<col width="*"/>
								<%-- <col width="15%"/> --%>
							</colgroup>
							<tbody>
								<tr>
									<td>
										<textarea class="wid100 hgt50" id="comnt" name="comnt" placeholder="신청 교수님께 남길 코멘트는 여기에 등록하시면 교수님께 안내됩니다."><c:out value="${curriculumVO.comnt}"/></textarea>
									</td>
									<%-- 
									<td>
										<c:url var="uptUrl" value="/mng/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}"/>
										<a href="${uptUrl}" class="btn_mngTxt btn_cmt alC">코멘트<br/>등록</a>
									</td>
									 --%>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${curriculumVO.aprvalAt eq 'Y'}">
						<strong>*과정 성과</strong>
						<table class="chart2 mb50">
							<tbody>
								<tr>
									<th class="alC">과정성과</th>
								</tr>
								<tr>
									<td><c:out value="${curriculumVO.crclOutcomeNm}"/></td>	
								</tr>
							</tbody>
						</table>
						<br/>
						<strong>*운영팀 코멘트</strong><c:if test="${!empty curriculumVO.comnt}">(<fmt:formatDate value="${curriculumVO.comntPnttm}"  pattern="yyyy-MM-dd"/>)</c:if>
						<table class="chart2 mb50">
							<tbody>
								<tr>
									<td>
										<c:choose>
											<c:when test="${empty curriculumVO.comnt}">없음</c:when>
											<c:otherwise><c:out value="${curriculumVO.comnt}"/></c:otherwise>
										</c:choose>
									</td>
								</tr>
							</tbody>
						</table>
						<br/>
					</c:when>
				</c:choose>
				
				<c:if test="${curriculumVO.aprvalAt eq 'Y' or curriculumVO.aprvalAt eq 'N'}">
					<div class="box_aprDn">
						<strong>*승인/반려 취소사유</strong>
						<table class="chart2 mb50">
							<tbody>
								<tr>
									<td><textarea class="wid100 hgt50" id="aprvalDn" name="aprvalDn" placeholder="간단히 취소사유를 입력해 주셔야 취소가 가능합니다. (교수님께 안내됩니다.)&#13;&#10;※ 과정등록관리 메뉴에서 추가로 등록된 내용이 있을 경우 모두 삭제됩니다."><c:out value="${curriculumVO.aprvalDn}"/></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:if>
				
				<div class="btn_c">
					<c:url var="uptUrl" value="/mng/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}"/>
					<c:choose>
						<c:when test="${curriculumVO.aprvalAt eq 'Y'}">
						    <a href="${uptUrl}" class="btn_mngTxt btn_aprCcl">승인취소</a>
						</c:when>
						<c:when test="${curriculumVO.aprvalAt eq 'N'}">
						    <a href="${uptUrl}" class="btn_mngTxt btn_aprRcl">반려취소</a>
						</c:when>
						<c:otherwise>
							<a href="${uptUrl}" class="btn_mngTxt btn_apr">승인</a>
						    <a href="${uptUrl}" class="btn_mngTxt btn_rjt">반려</a>
						</c:otherwise>
					</c:choose>
				    
				    <c:url var="listUrl" value="/mng/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
				    <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
				</div>
			</form>
		</c:when>
		<c:otherwise>
			<strong>*과정 성과</strong>
			<table class="chart2 mb50">
				<caption>등록폼</caption>
				<tbody>
					<tr>
						<td>
							<c:out value="${curriculumVO.crclOutcomeNm}"/>
						</td>	
					</tr>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	
	<c:if test="${processSttusCode > 0}">
		<strong>*강의책임교원 배정</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="15%"/>
				<col width="*"/>
			</colgroup>
			<tbody>
				<tr>
					<td>
						<ul id="box_user2">
							<c:forEach var="user" items="${subUserList}" varStatus="status">
								<li>
									<c:out value="${user.userNm}"/>(${user.userId})
								</li>
							</c:forEach>
						</ul>
					</td>	
				</tr>
			</tbody>
		</table>
		
		<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
			<strong>*교원 배정</strong>&nbsp;&nbsp;<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
			<table class="chart2 mb50">
				<caption>등록폼</caption>
				<colgroup>
					<col width="15%"/>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th class="alC">배정시수</th>
						<th class="alC">실제시간표시수</th>
						<th class="alC" colspan="3">교원배정</th>	
					</tr>
				</tbody>
				 <c:set var="minTimeTot" value="0"/>
				 <c:set var="realTimeTot" value="0"/>
				 <c:forEach var="result" items="${facList}" varStatus="status">
					<tbody class='box_staff'>
						<tr>
							<td class="line alC num"><c:out value="${result.sisu}"/></td>
							<td class="line alC realNum">
								<c:set var="realNum" value="0"/>
								<c:forEach var="facPl" items="${facPlList}">
									<c:if test="${facPl.facId eq result.facId}">
										<c:set var="realNum" value="${realNum + facPl.sisu}"/>
									</c:if>
								</c:forEach>
								<c:out value="${realNum}"/>
								<c:set var="realTimeTot" value="${realTimeTot + realNum}"/>
								<c:set var="minTime" value="${result.sisu - realNum}"/>
								<c:set var="minTimeTot" value="${minTimeTot + minTime}"/>
								<c:if test="${minTime > 0}">
									(<span class="red minTime"><c:out value="${minTime}"/>시간 부족</span>)
								</c:if>
							</td>
							<td class="line"><c:out value="${result.facNm}"/></td>
						</tr>
					</tbody>
					<c:set var="curTime" value="${curTime + result.sisu}"/>
				</c:forEach>
				 
				<tfoot id="box_addTime">
					<tr class="box_sum">
						<td class="alC"><span class="curTime">${curTime}</span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span></td>
						<td class="alC">
							<span class="realTime"><c:out value="${realTimeTot}"/></span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span>
							<%-- 시간표 작업 되면 해당 부분 작업해야됨 --%>
							<c:set var="lackTime" value="${minTimeTot}"/>
							<c:if test="${lackTime > 0}">
								(<span class="red">${lackTime}시간 부족</span>)
							</c:if>
						</td>
						<td colspan="3">
							교원 <span class="sumUser">0</span>명
							<c:forEach var="result" items="${facIdList}" varStatus="status">
								<input type="hidden" class="facIdList" name="facIdList" value="<c:out value="${result}"/>">
							</c:forEach>
						</td>
					</tr>
				</tfoot>
			</table>
		</c:if>
		
		<strong>* 과정내용</strong>&nbsp;&nbsp;<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
		<table class="chart2 mb50">
			<colgroup>
				<col width="15%"/>
				<col width="15%"/>
				<col width="*"/>
			</colgroup>
			<caption>등록폼</caption>
			<tbody>
				<tr>
					<th class="alC">번호</th>
					<th class="alC">내용범주</th>
					<th class="alC">내용</th>
				</tr>
			</tbody>
			
			<%-- 과정내용 번호 및 단원 묶어주는 역할 --%>	
		<%-- 	<c:set var="rowspan" value="0"/>
			<c:set var="rowspanList" value=""/>
			<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
			<c:forEach var="list" items="${lessonList}" varStatus="status">
				<c:choose>
					<c:when test="${prevCode eq list.lessonNm}">
						<c:set var="rowspan" value="${rowspan + 1}"/>
					</c:when>
					<c:otherwise>
						<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
						<c:set var="rowspan" value="1"/>
						<c:set var="prevCode" value="${list.lessonNm}"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
			<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
			<c:set var="listCnt" value="${rowspan[0]}"/>
			<c:set var="rowspanCnt" value="0"/>
			
			<c:set var="prevCode" value=""/>
			<c:set var="number" value="1"/>
			
			<c:forEach var="result" items="${lessonList}" varStatus="status">
				<c:if test="${prevCode ne result.lessonNm}">
					<tbody class='box_lesson'>
				</c:if>
				<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
					<c:if test="${prevCode ne result.lessonNm}">
						<td class="line alC num" rowspan="${rowspan[rowspanCnt] + 1}">
							<c:out value="${number}"/>
							<c:set var="number" value="${number + 1}"/>
						</td>
						<td rowspan='${rowspan[rowspanCnt] + 1}' class='line lesnm'><c:out value="${result.lessonNm}"/></td>
						<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
					</c:if>
					<td class='line'><c:out value="${result.chasiNm}"/></td>
				</tr>
				<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
					</tbody>
				</c:if>
				
				<c:set var="prevCode" value="${result.lessonNm}"/>
			</c:forEach> --%>
			
			<tfoot id="box_addLes"></tfoot>
		</table>
		
		<strong>*성적</strong>
		<%-- 
		<c:choose>
		<c:when test="${curriculumVO.evaluationAt eq 'Y' and not empty curriculumVO.gradeType}">
			절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
		</c:when>
		<c:otherwise>상대평가</c:otherwise>
		</c:choose>
		--%>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<tbody>
				<c:choose>
					<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
						<tr>
							<th class="alC">Grade</th>
							<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
									<th class="alC"><c:out value="${result.ctgryNm}"/></th>
								</c:if>
							</c:forEach>
						</tr>
						<tr class="alC">
							<c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
							<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
							<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
							<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
							<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
							<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
							<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
							<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
							<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
							<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
							<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
							
							<td class="line alC">점수</td>
							<td class="line alC">${aplus}</td>
							<td class="line alC">${a}</td>
							<td class="line alC">${bplus}</td>
							<td class="line alC">${b}</td>
							<td class="line alC">${cplus}</td>
							<td class="line alC">${c}</td>
							<td class="line alC">${dplus}</td>
							<td class="line alC">${d}</td>
							<td class="line alC">${f}</td>
						</tr>
					</c:when>
					<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
						<tr>
							<th class="alC">P/F</th>
							<th class="alC" colspan="2">점수구간</th>
						</tr>
						<tr class="alC">
							<td class="line">Pass</td>
							<td class="alC">${curriculumVO.pass}</td>
							<td class="alC">~ 100</td>
						</tr>
						<tr class="alC">
							<td class="line">Fail</td>
							<td class="alC">${curriculumVO.fail}</td>
							<td class="alC">~ 0</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th class="alC">Grade</th>
							<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
									<th class="alC"><c:out value="${result.ctgryNm}"/></th>
								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<td class="line alC">비중</td>
							<td colspan="2" class="line alC">상위 30%</td>
							<td colspan="2" class="line alC">상위 65%이내</td>
							<td colspan="5" class="line alC">66% ~ 100%</td>
						</tr>
						<%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
						<tr>
							<td class="line alC">가이드 학생 수</td>
							<td colspan="2" class="line alC">30</td>
							<td colspan="2" class="line alC">35</td>
							<td colspan="5" class="line alC">35</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<strong>*총괄평가 기준</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="40%"/>
				<col width="*"/>
			</colgroup>
			<tbody>
				<tr>
					<th class="alC">총괄평가 기준</th>
					<th class="alC">성적 반영률(%)</th>
				</tr>
			</tbody>
			<tbody class='box_evt'>
				<%-- 총괄평가 등록되어 있는지 확인 및 반영률 체크--%>
				<c:set var="totper" value="0"/>
				<c:choose>
					<c:when test="${not empty evaluationList or fn:length(evaluationList) > 0}">
						<c:forEach var="result" items="${evaluationList}" varStatus="status">
							<tr class="<c:out value="${result.evtId}"/>">
								<td class="line alC" <c:if test="${result.evtId eq 'CTG_0000000000000117'}">rowspan="${fn:length(attendBbsList) + 1}"</c:if>><c:out value="${result.evtNm}"/></td>
								<td class="line alR"><c:out value="${result.evtVal}"/>%</td>
							</tr>
							
							<%-- 수업참여게시판 --%>
							<c:if test="${result.evtId eq 'CTG_0000000000000117'}">
								<c:forEach var="bbsList" items="${attendBbsList}">
									<tr>
										<td class="alR">
											(
											<c:choose>
												<c:when test="${bbsList.sysTyCode eq 'ALL'}">전체</c:when>
												<c:when test="${bbsList.sysTyCode eq 'GROUP'}">조별</c:when>
												<c:when test="${bbsList.sysTyCode eq 'CLASS'}">반별</c:when>
											</c:choose>
											)
											<c:out value="${bbsList.bbsNm}"/>
											|
											<strong><c:out value="${bbsList.colect}"/></strong>회 이상
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr><td colspan="3" class="alC">총괄평가 기준 데이터가 없습니다.</td></tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<strong>*수료 기준(선택 입력)</strong>&nbsp;&nbsp;
		<span class="red">별도 수료 기준이 없다면 등록하지 않으셔도 됩니다.</span>
		<table class="chart2">
			<caption>등록폼</caption>
			<colgroup>
				<col width="20%"/>
				<col width="40%"/>
				<col width="40%"/>
			</colgroup>
			<tbody>
				<tr>
					<th class="alC">수료 기준</th>
					<th class="alC">수료 기준 선택및 점수, %</th>
					<th class="alC">설명</th>	
				</tr>
				<c:forEach var="result" items="${finishList}">
					<c:if test="${result.ctgryLevel eq 1}">
						<tr>
							<td class="alC"><c:out value="${result.ctgryNm}"/></td>
							<td class="alC">
								<c:choose>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000084'}">
										<c:choose>
											<c:when test="${empty curriculumVO.fnTot}">해당없음</c:when>
											<c:otherwise><strong>대상</strong> | ${curriculumVO.fnTot}점 이상</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000085'}">
										<c:choose>
											<c:when test="${empty curriculumVO.fnAttend}">해당없음</c:when>
											<c:otherwise><strong>대상</strong> | ${curriculumVO.fnAttend}% 이상</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000086'}">
										<c:choose>
											<c:when test="${empty curriculumVO.fnGradeTot}">해당없음</c:when>
											<c:otherwise><strong>대상</strong> | ${curriculumVO.fnGradeTot}점 이상</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000087'}">
										<c:choose>
											<c:when test="${empty curriculumVO.fnGradeFail}">해당없음</c:when>
											<c:otherwise><strong>대상</strong> | ${curriculumVO.fnGradeFail}점 이상</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000088'}">
										<c:choose>
											<c:when test="${empty curriculumVO.fnHomework}">해당없음</c:when>
											<c:otherwise><strong>대상</strong> | ${curriculumVO.fnHomework}점 이상</c:otherwise>
										</c:choose>
									</c:when>
								</c:choose>
							</td>
							<td><c:out value="${result.ctgryCn}"/></td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<span class="red">성적이 Letter Grade로 설정된 경우, F인 사람은 수료 기준을 만족해도 수료로 체크되지 않음</span>
		<br/><br/><br/>
		<strong>*교재 및 부교재</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="10%"/>
				<col width="*"/>
				<col width="35%"/>
			</colgroup>
			<tbody class="list_book">
				<tr>
					<th class="alC">NO</th>
					<th class="alC">교재명</th>
					<th class="alC">출판사</th>
				</tr>
				<c:forEach var="result" items="${bookList}" varStatus="status">
					<tr>
						<td class="num alC"><c:out value="${status.count}"/></td>
						<td class="alC">
							<c:out value="${result.nttSj}"/>
							<input type="hidden" class="nttNoList" name="nttNoList" value="${result.nttNo}"/>
						</td>
						<td class="alC"><c:out value="${result.tmp01}"/></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(bookList) == 0}">
					<tr><td colspan="3" class="alC">등록된 교재 및 부교재가 없습니다.</td></tr>
				</c:if>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" class="border1"><c:out value="${fn:replace(curriculumVO.bookEtcText, LF, '<br>')}" escapeXml="false"/></td>
				</tr>
			</tfoot>
		</table>
		
		<strong>*자체 운영 계획 및 규정</strong>
		<div class="border1"><c:out value="${fn:replace(curriculumVO.managePlan, LF, '<br>')}" escapeXml="false"/></div>
	</c:if>
	
	<c:if test="${searchVO.mngAt ne 'Y' and param.menu ne 'CLASS_MANAGE'}">
		<div class="btn_c">
			<c:if test="${curriculumVO.processSttusCode eq '0'}">
				<c:url var="confirmUrl" value="/mng/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}">
					<c:param name="crclId" value="${curriculumVO.crclId}" />
					<c:param name="crclNm" value="${curriculumVO.crclNm}" />
					<c:param name="processSttusCode" value="1" />
				</c:url>
			    <a href="${confirmUrl}" class="btn_mngTxt btn_confirm">확정등록</a>
		    </c:if>
			<c:url var="uptUrl" value="/mng/lms/crcl/updateCurriculumView.do${_BASE_PARAM}">
				<c:param name="crclId" value="${curriculumVO.crclId}" />
			</c:url>
		    <a href="${uptUrl}" class="alR"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>
		    
		    <c:url var="listUrl" value="/mng/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
		    <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
		</div>
	</c:if>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>