<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchcrclbNm}"><c:param name="searchcrclbNm" value="${searchVO.searchcrclbNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty param.tmplatImportAt}"><c:param name="tmplatImportAt" value="${param.tmplatImportAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
<c:when test="${param.tmplatImportAt ne 'N'}">

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCLB"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="기본과정관리"/>
</c:import>

<script>
$(document).ready(function(){
	//과정코드 생성
	$("input[name=autoIdAt]").click(function(){
		if("Y" == $(this).val()){
			$("#crclbId").prop("readonly", true);
		}else{
			$("#crclbId").prop("readonly", false);
		}
	});
	
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${searchVO.searchSysCode2}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${searchVO.searchSysCode3}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
	});
	
	$("#ctgryId1").change();
});
</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crclb/CurriculumbaseList.do"/>">
	  		<label><strong>기본과정명 : </strong> <input type="text" name="searchcrclbNm" value="${searchVO.searchcrclbNm}" class="inp_s" id="inp_text" /></label>
	  		<label><strong>기본과정코드 : </strong> <input type="text" name="searchcrclbId" value="${searchVO.searchcrclbId}" class="inp_s" id="inp_text" /></label>
	  		<br/>
	  		<strong>과정체계관리 : </strong>
	  		<select id="ctgryId1" name="searchSysCode1">
				<option value="">대분류</option>
				<c:forEach var="result" items="${sysCodeList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchSysCode1}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<select id="ctgryId2" name="searchSysCode2">
				<option value="">중분류</option>
			</select>
			<select id="ctgryId3" name="searchSysCode3">
				<option value="">소분류</option>
			</select>
			<br/>
			<select id="division" name="searchDivision">
				<option value="">이수구분 선택</option>
				<c:forEach var="result" items="${divisionList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchDivision}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
					
			<select id="control" name="searchControl">
				<option value="">관리구분 선택</option>
				<c:forEach var="result" items="${controlList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchControl}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			
			<select id="targetType" name="searchTargetType">
				<option value="">대상 선택</option>
				<option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
				<option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
			</select>
			<select id="targetDetail" name="searchTargetDetail">
				<option value="">선택</option>
				<c:forEach var="result" items="${targetList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1'}">
						<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.targetDetail}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	  
	<table class="chart_board">
	    <colgroup>
	    	<col width="50px"/>
	    	<%-- <col width="100px"/> --%>
			<col width="400px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="100px"/>
			<col width="80px"/>
			<col width="80px"/>
			<col width="120px"/>
			<col width="100px"/>
			<col width="100px"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>번호</th>
	        <!-- <th>기본과정코드</th> -->
	        <th>기본과정명</th>
	        <th>대분류</th>
	        <th>중분류</th>
	        <th>소분류</th>
	        <th>이수구분</th>
	        <th>관리구분</th>
	        <th colspan="2">대상</th>
	        <th>관리</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<tr>
	    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	    			<%-- <td><c:out value="${result.crclbId}"/></td> --%>
	    			<td><c:out value="${result.crclbNm}"/></td>
	    			<c:set var="sysPath" value="${fn:split(result.sysCodeNmPath,'>')}"/>
	    			<td><c:out value="${sysPath[1]}"/></td>
	    			<td><c:out value="${sysPath[2]}"/></td>
	    			<td><c:out value="${sysPath[3]}"/></td>
	    			<td><c:out value="${result.divisionNm}"/></td>
	    			<td><c:out value="${result.controlNm}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
	    					<c:otherwise>일반</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.targetDetailNm}"/></td>
	    			<c:url var="viewUrl" value="/mng/lms/crclb/updateCurriculumbaseView.do${_BASE_PARAM}">
						<c:param name="crclbId" value="${result.crclbId}"/>
						<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
					</c:url>
	    			<c:url var="delUrl" value="/mng/lms/crclb/deleteCurriculumbase.do">
						<c:param name="crclbId" value="${result.crclbId}"/>
					</c:url>
	    			<td>
	    				<a href="${viewUrl}"><img src="${_IMG}/btn/edit.gif"/></a>
	    				<a href="${delUrl}"><img src="${_IMG}/btn/del.gif"/></a>
	    			</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="11"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    
    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
	    </c:url>
	
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
    
    <div class="btn_r">
    	<c:url var="addUrl" value="/mng/lms/crclb/addCurriculumbaseView.do"/>
		<a href="${addUrl}"><img src="${_IMG}/btn/btn_creat.gif" alt="생성"/></a>
	</div>
	
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
</c:when>
<c:otherwise>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>양식자료 선택</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = "",
			name = "".
			code1 = "",
			code2 = "",
			code3 = "";
		
		if($(".chk:checked").length > 1){
			alert("기본과정을 한개만 선택해 주세요.");
		}else{
			$(".chk").each(function(){
				if($(this).is(":checked")){
					id = $(this).val();
					name = $(this).data("name");
					code1 = $(this).data("code1");
					code2 = $(this).data("code2");
					code3 = $(this).data("code3");
				}
			});
			
			window.opener.selcrclb(id, name, code1, code2, code3);
			window.close();
		}
		
		return false;
	});
	
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
<form name="listForm" id="listForm" method="post" method="post" action="/mng/lms/crclb/CurriculumbaseList.do?tmplatImportAt=N">
	<input type="hidden" name="searchTarget"/>
	<div id="bbs_search">
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchcrclbNm" value="${searchVO.searchcrclbNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해 주세요.">
		<input type="image" src="/template/manage/images/btn/btn_search.gif" alt="검색">
  	</div>
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
	    	<col class="co6"/>
			<col class="*"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co4"/>
			<col class="co6"/>
			<col class="co6"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<th>선택</th>
	        <th>기본과정코드</th>
	        <th>기본과정명</th>
	        <th>대분류</th>
	        <th>중분류</th>
	        <th>소분류</th>
	        <th>이수구분</th>
	        <th>관리구분</th>
	        <th colspan="2">대상</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<tr>
	    			<c:set var="sysCodePath" value="${fn:split(result.sysCodePath,'>')}"/>
	    			<c:set var="sysPath" value="${fn:split(result.sysCodeNmPath,'>')}"/>
	    			<td><input type="checkbox" class="chk" value="${result.crclbId}" data-name="${result.crclbNm}" data-code1="${sysCodePath[1]}" data-code2="${sysCodePath[2]}" data-code3="${sysCodePath[3]}"/></td>
	    			<td><c:out value="${result.crclbId}"/></td>
	    			<td><c:out value="${result.crclbNm}"/></td>
	    			<td><c:out value="${sysPath[1]}"/></td>
	    			<td><c:out value="${sysPath[2]}"/></td>
	    			<td><c:out value="${sysPath[3]}"/></td>
	    			<td><c:out value="${result.divisionNm}"/></td>
	    			<td><c:out value="${result.controlNm}"/></td>
	    			<td>
	    				<c:choose>
	    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
	    					<c:otherwise>일반</c:otherwise>
	    				</c:choose>
	    			</td>
	    			<td><c:out value="${result.targetDetailNm}"/></td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="11"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    
    <div id="paging">
	    <c:url var="pageUrl" value="/mng/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
	    </c:url>
	
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
    <div class="btn_c">
		<a href="#" class="btn_ok" data-target="${param.target}"><img src="/template/manage/images/board/btn_confirm.gif"/></a>
	</div>
</form>
</div>
</c:otherwise>
</c:choose>