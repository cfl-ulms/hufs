<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="PROF_MANAGE"/>
	<c:param name="validator" value="userManageVO"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<script type="text/javascript">

	

	function fnResetPwd(){
		if(confirm("${userManageVO.userNm} 교수의 패스워드를 초기화 하시겠습니까?\n(초기 비밀번호 : 1234)로 변경됨")){
			$(document).ready(function(){
				$.ajax({
					url: '/mng/usr/EgovResetPwd.do',
					async: true,
					type: 'POST',
					data: {
						userId: '${userManageVO.userId}'
					},
					dataType: 'text',
					success: function(){
						alert("패스우드 초기화가 완료되었습니다.\n(교수:1234)로 초기화되었습니다.");
					}
				});
			});
		}
	}

</script>
		
<div id="cntnts">
	<table class="chart2">
		<caption>회원관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
			<col width="300"/>
		</colgroup>
		<tbody>
		<tr>
			<th><em>*</em> 이름 <span style="color:red;">(필수)</span></th>
			<td>
				<c:out value="${userManageVO.userNm}"/> &nbsp;&nbsp;&nbsp;&nbsp;
				<c:choose>
					<c:when test="${userManageVO.confmAt eq 'Y'}">
						LMS 사용중
					</c:when>
					<c:otherwise>
						<span style="font-color:red;">LMS 사용안함</span>
					</c:otherwise>
				</c:choose>&nbsp;&nbsp;&nbsp;&nbsp;
				<c:out value="${userManageVO.workStatusCodeNm}"/>
			</td>
			<td rowspan="11" align="center">
				<c:choose>
					<c:when test="${empty userManageVO.photoStreFileNm}">사진없음</c:when>
					<c:otherwise><img src="${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>" style="max-width:300px"/></c:otherwise>
				</c:choose>
			</td>
		</tr>
		
		<tr>
			<th><em>*</em> 성별 <span style="color:red;">(필수)</span></th>
			<td>
				<c:out value="${userManageVO.sexdstn eq 'M' ? '남자' : '여자'}"/>
			</td>
		</tr>
		
		<tr>
			<th> 사번  (선택)</th>
			<td>
				<c:out value="${userManageVO.userSchNo}"/>
			</td>
		</tr>
		
		<tr>
			<th> 직위 (선택)</th>
			<td>
				<c:out value="${userManageVO.positionCodeNm}"/>
	  	  	</td>
		</tr>
		
		<tr>
			<th> 소속 
				(선택)
				<!-- <span style="color:red;">(필수)</span> -->
			</th>
			<td>
				<c:out value="${userManageVO.groupCode}"/>
	  	  	</td>
		</tr>
		
		<tr>
			<th><label for="mngDeptCode"> 주관기관 </label><span style="color:red;">(필수)</span></th>
			<td>
				<c:out value="${userManageVO.mngDeptCodeNm}"/>
	  	  	</td>
		</tr>
		
		<tr>
			<th> 교육과정 언어  <span style="color:red;">(필수)</span></th>
			<td>
				<c:out value="${userManageVO.majorNm}"/>
	  	  	</td>
		</tr>
		
		<tr>
			<th><label for="moblphonNo">연락처</label> (선택)</th>
			<td>
				<c:out value="${userManageVO.moblphonNo}"/>
			</td>
		</tr>
		<tr>
			<th> E-mail <span style="color:red;">(필수)</span></th>
			<td>
				<c:out value="${userManageVO.emailAdres}"/>
			</td>
		</tr>
		
		<tr>
			<th> 아이디 (선택)</th>
			<td>
				<c:out value="${userManageVO.userId}"/>
			</td>
		</tr>
		
		<tr>
			<th><em>*</em> 패스워드</th>
			<td>***** &nbsp;&nbsp;&nbsp;&nbsp;
			<button onclick="fnResetPwd();return false;" > 패스워드 초기화  </button>
			</td>
		</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="./EgovMberManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<%-- <a href="/mng/usr/EgovMberAddView.do"><img src="${_IMG}/btn/btn_regist.gif" alt="등록하기" /></a> --%>
		<input type="image" src="${MNG_IMG}/btn/btn_modify.gif" alt="수정하기" onclick="location.href='/mng/usr/EgovMberAddView.do?userId=${userManageVO.userId}&userSeCode=08'"/>
		<input type="image" src="${MNG_IMG}/btn/btn_del.gif" alt="삭제하기" onclick="location.href='/mng/usr/EgovUserDelete.do?userId=${userManageVO.userId}&userSeCode=08'"/>
		<%-- <input type="image" src="<c:url value='${MNG_IMG}/btn/btn_modify.gif'/>" alt="수정" onclick="return checkForm(document.userManageVO);"/> --%>
		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_list.gif" alt="목록"/></a>
						
	</div>
<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>



<c:if test="${resultCnt > 0}">
	<h2>교육과정 이력</h2>
	(총 교육과정  ${fn:length(resultListCount)}건 / 수업진행 ${resultCnt }건)
	<div>
	<table class="chart2">
			<caption>교육과정 이력</caption>
			<colgroup>
	          <col style='width:7%'>
	          <col style='width:10%'>
	          <col style='width:9%'>
	          <col style='width:14%'>
	          <col style='width:10%'>
	          <col>
	          <col style='width:10%'>
	          <col>
	        </colgroup>
			<thead>
	          <tr class='bg-light-gray font-700'>
	            <th scope='col'>No</th>
	            <th scope='col'>년도</th>
	            <th scope='col'>학기</th>
	            <th scope='col'>과정기간</th>
	            <th scope='col'>과정상태</th>
	            <th scope='col'>교육과정명</th>
	            <th scope='col'>담당직위</th>
	            <th scope='col'>수업명</th>
	          </tr>
	        </thead>
			<tbody>
				<c:set var="no" value="1"/>      
                 	<c:forEach var="result" items="${resultList}" varStatus="status">
                 		<c:forEach var="list" items="${resultListCount}" varStatus="status2">
                 			<c:if test="${result.crclId eq list.crclId}">
                 				<c:set var="crclCnt" value="${list.groupCnt}"/>
                 			</c:if>
                    </c:forEach>
                    <c:choose>
                  		<c:when test="${result.crclId eq crclId}">  		
		                    <tr class="">
		                      <td class='left-align keep-all'>${result.studySubject}</td>
		                    </tr>	
	                    </c:when>
	                    <c:otherwise>
	                    	<c:choose>
                                <c:when test="${result.processSttusCode eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
                                <c:when test="${result.processSttusCode eq 3 or result.processSttusCode eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
                                <c:when test="${result.processSttusCode eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
                                <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
                            </c:choose>
                            
                            <c:url var="viewUrl" value="/lms/crm/myCurriculumView.do">
								<c:param name="crclId" value="${result.crclId}"/>
								<c:param name="crclbId" value="${result.crclbId}"/>
								<c:param name="subtabstep" value="1"/>
								<c:param name="tabStep" value="1"/>
								<c:param name="menuId" value="MNU_0000000000000068"/>
							</c:url>
					
	                    	<c:choose>
		                    		<c:when test="${crclCnt > 1}">
		                  				<tr class="">
					                      <td rowspan='${crclCnt}'>${no}</td>
					                      <td rowspan='${crclCnt}'>${result.crclYear}</td>
					                      <td rowspan='${crclCnt}'>${result.crclTermNm}</td>
					                      <td rowspan='${crclCnt}'>${result.startDate}<br>~<br>${result.endDate}</td>
					                      	
				                            <td rowspan='${crclCnt}' class='${processSttusColor }'>
							    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status3">
							    					<c:if test="${statusCode.code eq result.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
												</c:forEach>
							    			</td>
					                      
					                      <td class='left-align keep-all' rowspan='${crclCnt}'><a href='${viewUrl}' class='underline keep-all' target="_blank">${result.crclNm}</a></td>
					                      <td rowspan='${crclCnt}'>${result.manageCodeNm}</td>
					                      <td class='left-align keep-all'>${result.studySubject}</td>
					                    </tr>
				                    </c:when>
				                    <c:otherwise>
				                    	<tr class="">
					                      <td>${no}</td>
					                      <td>${result.crclYear}</td>
					                      <td>${result.crclTermNm}</td>
					                      <td>${result.startDate}<br>~<br>${result.endDate}</td>
				                            <td class='${processSttusColor }'>
							    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status3">
							    					<c:if test="${statusCode.code eq result.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
												</c:forEach>
							    			</td>
					                      <td class='left-align keep-all'><a href='${viewUrl}' class='underline keep-all' target="_blank">${result.crclNm}</a></td>
					                      <td>${result.manageCodeNm}</td>
					                      <td class='left-align keep-all'>${result.studySubject}</td>
					                    </tr>
				                    </c:otherwise>
				                 </c:choose>
				                 <c:set var="no" value="${no+1}"/>   
	                    </c:otherwise>
                    </c:choose>
                    
                    <c:set var="crclId" value="${result.crclId}"/>
                   </c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	