<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="CMMN_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CMMN_JS" value="${pageContext.request.contextPath}/template/common/js"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="title" value="배너관리"/>
<c:set var="_MODE" value=""/>

<c:choose>
	<c:when test="${empty searchVO.bannerId}">
		<c:set var="_MODE" value="REG"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${searchVO.bannerTyCode eq 'BAN001'}"><c:set var="title" value="팝업존관리"/></c:when>
	<c:when test="${searchVO.bannerTyCode eq 'BAN002'}"><c:set var="title" value="배너존관리"/></c:when>
	<c:when test="${searchVO.bannerTyCode eq 'BAN003'}"><c:set var="title" value="퀵메뉴관리"/></c:when>
	<c:when test="${searchVO.bannerTyCode eq 'BAN004'}"><c:set var="title" value="메인배너관리(Type1)"/></c:when>
	<c:when test="${searchVO.bannerTyCode eq 'BAN005'}"><c:set var="title" value="메인배너관리(Type2)"/></c:when>
	<c:when test="${searchVO.bannerTyCode eq 'BAN006'}"><c:set var="title" value="서브배너관리"/></c:when>
</c:choose>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="ETC_MANAGE"/>
	<c:param name="depth1" value="ETC_SHORTCUT"/>
	<c:param name="title" value="${title}"/>
</c:import>

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="banner" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">

function fn_egov_regist() {
    
       if(!validateBanner(document.banner)){           
           return false;
       }
       <%-- 
       <c:if test="${_MODE eq 'REG'}">
       if($.trim($('#imageFile').val()) == ""){
    	   alert("배너이미지를 첨부하세요");
           return false;
       }
       </c:if>
 		--%>
       <c:if test="${banner.bannerTyCode eq 'BAN001'}">
       	var ntceBgndeYYYMMDD = $.trim($('#ntceBgndeYYYMMDD').val().replace(/-/g, ''));
		var ntceEnddeYYYMMDD = $.trim($('#ntceEnddeYYYMMDD').val().replace(/-/g, ''));

		if(ntceBgndeYYYMMDD == "") {
			alert("게시시작일자를 선택하세요");
			return false;
		}

		if(ntceEnddeYYYMMDD == "") {
			alert("게시종료일자를  선택하세요");
			return false;
		}
		
		var iChkBeginDe = Number( ntceBgndeYYYMMDD );
		var iChkEndDe = Number( ntceEnddeYYYMMDD );

		if(iChkBeginDe > iChkEndDe || iChkEndDe < iChkBeginDe ){
			alert("게시시작일자는 게시종료일자 보다 클수 없고,\n게시종료일자는 게시시작일자 보다 작을수 없습니다. ");
			return false;
		}

		$('#ntceBgnde').val(ntceBgndeYYYMMDD + $('#ntceBgndeHH').val() +  $('#ntceBgndeMM').val());
		$('#ntceEndde').val(ntceEnddeYYYMMDD + $('#ntceEnddeHH').val() +  $('#ntceEnddeMM').val());
		
	</c:if>	
       
    if(!confirm("<spring:message code="${_MODE eq 'REG' ? 'common.regist.msg' : 'common.update.msg'}" />")){
		return false;					
	}
}

$(function() {
	var Today = new Date();
	var setYear = parseInt(Today.getFullYear()-10)+":"+parseInt(Today.getFullYear()+10);
		
	$('#ntceBgndeYYYMMDD').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: setYear
	});	
	$('#ntceEnddeYYYMMDD').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: setYear
	});	
});

$(document).ready(function(){
	//배너 사진 삭제
	$("#btn-del").click(function(){
		$(".banner-img").remove();
		$(this).remove();
		$("#fileDeleteAt").val("Y");
	});
});
</script>



<div id="cntnts">

	<c:choose>
		<c:when test="${_MODE eq 'REG' }">
			<c:set var="actionUrl" value="${pageContext.request.contextPath}/mng/uss/ion/bnr/addBanner.do"/>
		</c:when>
		<c:otherwise>
			<c:set var="actionUrl" value="${pageContext.request.contextPath}/mng/uss/ion/bnr/updtBanner.do"/>
		</c:otherwise>
	</c:choose>
	<form:form commandName="banner" name="banner" method="post" action="${actionUrl}" enctype="multipart/form-data" onsubmit="return fn_egov_regist();"> 
        
        <form:hidden path="siteId"/>
      	<form:hidden path="sysTyCode"/>
      	
        <form:hidden path="bannerId"/>
        <form:hidden path="ntceBgnde"/>
		<form:hidden path="ntceEndde"/>		
		<form:hidden path="bannerTyCode"/>
		
		<input type="hidden" name="searchCondition" value="<c:out value="${searchVO.searchCondition}"/>"/>
        <input type="hidden" name="searchKeyword" value="<c:out value='${searchVO.searchKeyword}'/>" />
        <input type="hidden" name="searchCate" value="<c:out value='${searchVO.searchCate}'/>" />
        <input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>" />
  		
        <fieldset>
          <legend class="hdn">배너 입력 폼</legend>
        
        <table class="chart2">
			<caption> </caption>
			<colgroup>
				<col width="150px" />
				<col width="*" />
			</colgroup>
          <tbody>
          	<tr>
              <th><em>*</em> <label for="bannerTitle">배너 타이틀<br/>(10자 내외)</label></th>
              <td>
              	<form:input path="bannerTitle" size="15" maxlength="15" cssClass="inp_long" required="required"/> 
                <br/><form:errors path="bannerTitle" />
              </td>         
            </tr>
            <tr>
              <th><em>*</em> <label for="bannerNm">배너 제목<br/>(10자 내외)</label></th>
              <td>
              	<form:input path="bannerNm" size="15" maxlength="15" cssClass="inp_long" required="required"/> 
                <br/><form:errors path="bannerNm" />
              </td>         
            </tr>
            <tr>
              <th><em>*</em> <label for="bannerDc">배너 홍보 상세</label></th>
              <td>
              	<form:textarea path="bannerDc" rows="3" cols="100" required="required"/> 
                <br/><form:errors path="bannerDc" />
              </td>         
            </tr>
            <tr>
              <th><label for="imageFile">배너 사진</label></th>
              <td>
                <input type="file" name="imageFile" id="imageFile" class="input300 inp" title="배너이미지"/>
                <c:if test="${not empty banner.bannerImageFile}">
                	<input type="hidden" id="fileDeleteAt" name="fileDeleteAt" value="N"/>
                	<br/><img src="${pageContext.request.contextPath}${BannerFileStoreWebPath}${searchVO.siteId}/${banner.bannerImageFile}" class="banner-img"/>
                	<a href="#" id="btn-del"><img src="/template/manage/images/btn/del.gif"></a>
                </c:if>
              </td>         
            </tr>
            <tr>
              <th><em>*</em> <label for="linkUrl">링크URL</label></th>
              <td>
              	<form:input path="linkUrl" size="60" maxlength="255" cssClass="inp_long"/>  
                <br/><form:errors path="linkUrl" />
              </td>         
            </tr>
            <tr>
              <th><em>*</em> <label for="popupTrgetAt">새창여부</label></th>
              <td>
              	<spring:message code="button.yes" /> : <form:radiobutton path="popupTrgetAt"  value="Y" />&nbsp;
          	    <spring:message code="button.no" /> : <form:radiobutton path="popupTrgetAt"  value="N"  />
              </td>         
            </tr>
            <c:if test="${searchVO.bannerTyCode eq 'BAN002'}">
            <tr>
              <th><em>*</em> <label id="idBannerThemaClCode" for="bannerThemaClCode">배너주제분류</label>
              </th>
              <td>
                <form:select path="bannerThemaClCode">
                  <form:option value="" label="-- 선택 --"/>
                  <form:options items="${codeList}" itemValue="code" itemLabel="codeNm"/>
                </form:select>
                <div><form:errors path="bannerThemaClCode"/></div>
              </td>         
            </tr>
            </c:if>
            <tr>
              <th><em>*</em> <label for="reflctAt">반영여부</label></th>
              <td>
              	<spring:message code="button.yes" /> : <form:radiobutton path="reflctAt"  value="Y" />&nbsp;
          	    <spring:message code="button.no" /> : <form:radiobutton path="reflctAt"  value="N"  />
              </td>         
            </tr>
			<tr>
			<c:if test="${banner.bannerTyCode eq 'BAN001'}">
				<th>
					<em>*</em> <label id="IdNtceEnddeHH">게시 기간</label>
				</th>
				<td width="80%">
					<input type="text" name="ntceBgndeYYYMMDD" id="ntceBgndeYYYMMDD" size="10" maxlength="10"  class="inp" value="<c:out value="${fn:substring(banner.ntceBgnde, 0, 4)}"/>-<c:out value="${fn:substring(banner.ntceBgnde, 4, 6)}"/>-<c:out value="${fn:substring(banner.ntceBgnde, 6, 8)}"/>" readonly="readonly" />
					<form:select path="ntceBgndeHH">
						<form:options items="${ntceBgndeHH}" itemValue="code" itemLabel="codeNm"/>
					</form:select>H
					<form:select path="ntceBgndeMM">
						<form:options items="${ntceBgndeMM}" itemValue="code" itemLabel="codeNm"/>
					</form:select>M
					<span>&nbsp;&nbsp;~&nbsp;&nbsp;</span>
					<input type="text" name="ntceEnddeYYYMMDD" id="ntceEnddeYYYMMDD" size="10" maxlength="10" class="inp" value="<c:out value="${fn:substring(banner.ntceEndde, 0, 4)}"/>-<c:out value="${fn:substring(banner.ntceEndde, 4, 6)}"/>-<c:out value="${fn:substring(banner.ntceEndde, 6, 8)}"/>" readonly="readonly" />
					<form:select path="ntceEnddeHH">
						<form:options items="${ntceEnddeHH}" itemValue="code" itemLabel="codeNm"/>
					</form:select>H
					<form:select path="ntceEnddeMM">
						<form:options items="${ntceEnddeMM}" itemValue="code" itemLabel="codeNm"/>
					</form:select>M
				</td>
			</c:if>
			</tr>
			<tr>
              <th><em>*</em> <label for="sortOrdr">정렬순서</label></th>
              <td>
              	<form:input path="sortOrdr" size="2" cssClass="inp"/> 
                <br/><form:errors path="sortOrdr" />
              </td>         
            </tr>
          </tbody>
          <tfoot>
          </tfoot>
          </table>
  
          <div class="btn_r">
            <input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }" class="vMid" alt="등록" />
            <c:url var="listUrl" value="/mng/uss/ion/bnr/selectBannerList.do">
            	<c:param name="siteId" value="${searchVO.siteId}"/>
				<c:param name="sysTyCode" value="${searchVO.sysTyCode}"/>
				<c:param name="bannerTyCode" value="${searchVO.bannerTyCode}"/>
				<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
            	<c:if test="${not empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
				<c:if test="${not empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
	      		<c:if test="${not empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		      </c:url>
            <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
          </div>
            
        </fieldset>
	</form:form>
      
</div>      


<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	