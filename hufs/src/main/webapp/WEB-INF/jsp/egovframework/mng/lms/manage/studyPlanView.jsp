<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="crclId" value="${searchVO.crclId}" />
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${empty param.menu ? 'CURRICULUM_MANAGE' : param.menu }"/>
	<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="${param.menu eq 'CLASS_MANAGE' ? '' : '과정등록관리'}"/>
</c:import>

<script>
$(document).ready(function(){

});
</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm}"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.menu eq 'CLASS_MANAGE' ? '3' : '5'}"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<br/>

	<table class="chart2 mt50">
		<colgroup>
			<col width="20%"/>
			<col width="*"/>
			<col width="20%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
			  	<th class="alC">수업주제</th>
			  	<td colspan="3"><c:out value="${scheduleMngVO.studySubject}"/></td>
		  	</tr>
		  	<tr>
			  	<th class="alC">교원</th>
			  	<td colspan="3">
			  		<div class="view_mode">
			  			<ul>
					  		<c:forEach var="result" items="${facPlList}" varStatus="status">
								<li>
									<span>
										<c:out value="${result.userNm}"/>
										<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
									</span>
								</li>
							</c:forEach>
						</ul>
			  		</div>
			  	</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업일</th>
		  		<td>
		  			<c:out value="${scheduleMngVO.startDt}"/>
		  			&nbsp;
		  			<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
		  			~
		  			<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
		  		</td>
		  		<th class="alC">시수</th>
		  		<td><c:out value="${scheduleMngVO.sisu}"/></td>
		  	</tr>
		  	<tr>
		  		<th class="alC">장소</th>
		  		<td colspan="3">
		  			<c:out value="${scheduleMngVO.campusNm}"/>
		  			&nbsp;
		  			<c:out value="${empty scheduleMngVO.placeDetail ? curriculumVO.campusPlace : scheduleMngVO.placeDetail}"/>
		  		</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업영역</th>
		  		<td colspan="3">
		  			<c:set var="lang" value="${empty scheduleMngVO.crclLang ? curriculumVO.crclLang : scheduleMngVO.crclLang}"/>
		  			<c:out value="${empty scheduleMngVO.crclLangNm ? curriculumVO.crclLangNm : scheduleMngVO.crclLangNm}"/>
		  			<c:if test="${not empty scheduleMngVO.spLevel}">
		  				> <c:out value="${scheduleMngVO.spLevelNm}"/>
		  			</c:if>
		  		</td>
		  	</tr>
		  	<tr>
		  		<th class="alC">수업방법</th>
		  		<td colspan="3">
		  			<c:choose>
		  				<c:when test="${scheduleMngVO.spType ne 'N' }">
		  					강의식
		  				</c:when>
		  				<c:otherwise>
		  					비강의식
		  				</c:otherwise>
		  			</c:choose>
		  			- <c:out value="${scheduleMngVO.courseNm}"/>
		  		</td>
		  	</tr>
		  	<tr class="evtTr" <c:if test="${scheduleMngVO.courseId ne 'CTG_0000000000000054'}">style="display:none;"</c:if>>
		  		<th class="alC">평가</th>
		  		<td colspan="3">
		  			<ul class="box_evt">
		  				<c:forEach var="result" items="${evtResultList}">
		  					<li>
			  					<span><c:out value="${result.evtNm}"/></span>
							</li>
						</c:forEach>
	  				</ul>
		  		</td>
		  	</tr>
		  	<tr>
		  		<td colspan="4"><c:out value="${scheduleMngVO.spCn}" escapeXml="false"/></td>
		  	</tr>
		  	<tr>
				<th><spring:message code="cop.atchFile" /></th>
				<td colspan="3">
					<ul class="list">
						<c:if test="${not empty scheduleMngVO.atchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${scheduleMngVO.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
								<c:param name="mngAt" value="Y"/>
							</c:import>
						</c:if>
					</ul>
				</td>
			</tr>
			<tr>
				<th>수업자료</th>
				<td colspan="3">
					<ul id="refeFile_box">
						<c:forEach var="result" items="${fileList}" varStatus="status">
							 <c:url var="downLoad" value="/cmm/fms/FileDown.do">
								<c:param name="atchFileId" value="${result.atchFileId}"/>
								<c:param name="fileSn" value="${result.fileSn}"/>
							</c:url>
							<li class='refFileList'>
								<a href="${downLoad }">
									<img src='/template/manage/images/ico_file.gif' alt='파일'/>
									${result.orignlFileNm}
								</a>
							</li>
						</c:forEach>
					</ul>
				</td>
			</tr>
			<tr class="stdntAply">
				<th>학습성과</th>
				<td colspan="3"><c:out value="${scheduleMngVO.spGoal}"/></td>
			</tr>
		</tbody>
	</table>

	<div class="btn_c">
		<c:set var="listUrlTemp" value="${param.menu eq 'CURRICULUM_MANAGE' ? '/mng/lms/manage/studyPlan.do' : '/mng/lms/cla/curriculumStudyList.do'}"/>

		<c:url var="uptUrl" value="/mng/lms/manage/studyPlanReg.do${_BASE_PARAM}">
			<c:param name="plId" value="${scheduleMngVO.plId}" />
			<c:param name="menu" value="${empty param.menu ? 'CURRICULUM_MANAGE' : param.menu }"/>
		</c:url>
	    <a href="${uptUrl}" class="alR"><img src="${_IMG}/btn/btn_modify.gif" alt="수정"/></a>

	    <c:url var="listUrl" value="${listUrlTemp}${_BASE_PARAM}"/>
	    <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>