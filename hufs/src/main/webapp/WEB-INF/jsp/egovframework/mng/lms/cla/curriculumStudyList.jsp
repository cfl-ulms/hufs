<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchCtgryId}"><c:param name="searchCtgryId" value="${searchVO.searchCtgryId}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclbId}"><c:param name="searchCrclbId" value="${searchVO.searchCrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclId}"><c:param name="searchCrclId" value="${searchVO.searchCrclId}" /></c:if>
	<c:if test="${not empty searchVO.searchStudySubject}"><c:param name="searchStudySubject" value="${searchVO.searchStudySubject}" /></c:if>
	<c:if test="${not empty searchVO.searchClassManage}"><c:param name="searchClassManage" value="${searchVO.searchClassManage}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CLASS_MANAGE"/>
	<c:param name="depth1" value="CURRICULUM_STUDY"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정별 수업"/>
</c:import>
<style>
.backGray{
	background : #ccc;
}

</style>
<script>
$(document).ready(function(){

	$("#searchCtgryId, #searchCrclbId").change(function(){
		var tempCtgryId = $("select[id=searchCtgryId]").val();
		var tempCrclbId = $("select[id=searchCrclbId]").val();
		initCurriculum(tempCtgryId,tempCrclbId );
	});


	$("#resetBtn").on("click", function(){
		var now = new Date();
	    var year = now.getFullYear();
      	var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
      	var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
      	var chan_val = year + '-' + mon + '-' + day;
      	$("#searchStartDate").val(chan_val);
      	$("#searchEndDate").val(chan_val);
		$("input:checkbox").attr("checked", false);

		$("#searchStudySubject").val("");
		$('#searchClassManage').val("");
		initCurriculum("","");
	});

	$("#searchStartDate, #searchEndDate").datepicker({
		dateFormat: "yy-mm-dd"
	 });

	$("#searchTodayClass").on("click",function(){
		if($(this).prop("checked")){
			var now = new Date();
		    var year = now.getFullYear();
	      	var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
	      	var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
	      	var chan_val = year + '-' + mon + '-' + day;
	      	$("#searchStartDate").val(chan_val);
	      	$("#searchEndDate").val(chan_val);
		}
	});
});

function initCurriculum(tempCtgryId,tempCrclbId ){
	$.ajax({
		type:"post",
		dataType:"json",
		url:"/mng/lms/cla/curriculumList.json",
		data:{"searchCtgryId":tempCtgryId, "searchCrclbId" : tempCrclbId } ,
		success: function(data) {
			var curriculumBaseList = data.curriculumBaseList;
			var curriculumList = data.curriculumList;
			var html = "";

				html = "<option value=\"\">전체</option>";
			if(curriculumBaseList.length != 0){
				for(var i=0; i<curriculumBaseList.length; i++){
					if(tempCrclbId == curriculumBaseList[i].crclbId){
						html += "<option value=\""+curriculumBaseList[i].crclbId+"\" selected>"+curriculumBaseList[i].crclbNm+"</option>";
					}else{
						html += "<option value=\""+curriculumBaseList[i].crclbId+"\" >"+curriculumBaseList[i].crclbNm+"</option>";
					}
				}
			}
			$('#searchCrclbId').html(html);

				html = "<option value=\"\">전체</option>";
			if(curriculumList.length != 0){
				for(var i=0; i<curriculumList.length; i++){
					html += "<option value=\""+curriculumList[i].crclId+"\">"+curriculumList[i].crclNm+"</option>";
				}
			}
			$('#searchCrclId').html(html);
		},
		error:function() {
			//alert("error");
		}
	});
}
</script>

<div id="cntnts">
<c:set var="now" value="<%=new java.util.Date()%>" />
	<div id="bbs_search">
	  	<form id="frm" method="post" action="<c:url value="/mng/lms/cla/curriculumStudyList.do"/>">
	  		<strong>주관기관 : </strong>
	  		<select id="searchCtgryId" name="searchCtgryId">
				<option value="">전체</option>
				<c:forEach var="result" items="${ctgryList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1' }">
						<option value="${result.ctgryId}" ${searchVO.searchCtgryId eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			&nbsp;&nbsp;
			<strong>수업일 : </strong>

			<c:set var="nowDate"><fmt:formatDate value="${now}" pattern="yyyy-MM-dd" /></c:set>
			<input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~
			<input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			<br/>
			<strong>전체기본과정 : </strong>
	  		<select id="searchCrclbId" name="searchCrclbId">
				<option value="">전체</option>
				<c:forEach var="result" items="${curriculumBaseList}" varStatus="status">
					<option value="${result.crclbId}" ${searchVO.searchCrclbId eq result.crclbId ? 'selected' : '' }><c:out value="${result.crclbNm}"/></option>
				</c:forEach>
			</select>
			&nbsp;&nbsp;
			<strong>과정명: </strong>
			<input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="inp_text" placeholder="과정명을 입력해주세요.">
			<%--
	  		<select id="searchCrclId" name="searchCrclId">
				<option value="">전체</option>
				<c:forEach var="result" items="${curriculumList}" varStatus="status">
					<option value="${result.crclId}" ${searchVO.searchCrclId eq result.crclId ? 'selected' : '' }><c:out value="${result.crclNm}"/></option>
				</c:forEach>
			</select>
			 --%>
			<br/>
	  		<label><strong>수업주제 : </strong> <input type="text" name="searchStudySubject" value="${searchVO.searchStudySubject }" class="inp_s" id="searchStudySubject" /></label>
	  		<label><strong>교원명 : </strong> <input type="text" name="searchClassManage" value="${searchVO.searchClassManage }" class="inp_s" id="searchClassManage" /></label>
	  		<br/>
	  		<label><input type="checkbox" id="searchTodayClass" name="searchTodayClass" value="Y" ${searchVO.searchTodayClass eq 'Y' ? 'checked' : '' }/>오늘수업만 보기</label>&nbsp;&nbsp;
			<!-- <label><input type="checkbox" name="fileExtImg" value="Y" />출석대기 수업만 보기</label>&nbsp;&nbsp; -->
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;"/>
			<!-- <button value="초기화" style="width:73px;height:26px;"/> -->
		</form>
	</div>
	<div>
		<h2>총 ${studyListCnt}개</h2>
	</div>
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co4"/>
			<%-- <col class="co6"/> --%>
		</colgroup>
	    <thead>
	      <tr>
	        <th>No</th>
	        <th>수업일</th>
	        <th>기본과정</th>
	        <th>주관기관</th>
	        <th>과정명</th>
	        <th>수업주제</th>
	        <th>교원</th>
	       <!--  <th>출석 현황</th> -->
	      </tr>
	    </thead>
	    <tbody>
	    	<c:choose>
	    		<c:when test="${not empty studyList}">
			    	<c:forEach var="result" items="${studyList}" varStatus="status">
				    	<c:url var="viewUrl" value="/mng/lms/manage/studyPlanView.do?">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:param name="plId" value="${result.plId}"/>
							<c:param name="menu" value="CLASS_MANAGE"/>
							<c:param name="depth1" value="CURRICULUM_STUDY"/>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'" class="${result.dateChk eq 'N' ? 'backGray' : '' }" >
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<c:choose>
			    				<c:when test="${not empty result.sisu }">
			    					<td>${result.startDt} <br/>(${result.periodTxt}교시)<br/> ${result.startTime } ~ ${result.endTime } </td>
			    				</c:when>
			    				<c:otherwise>
									<td>${result.startDt} ~ ${result.endDt }</td>
			    				</c:otherwise>
			    			</c:choose>
			    			<td>${result.crclbNm}</td>
			    			<td>${result.ctgryNm }</td>
			    			<td>${result.crclNm}</td>
			    			<td>${result.studySubject}</td>
			    			<td>${result.userNm}</td>
			    			<!-- <td>출석현황</td> -->
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>

				</c:otherwise>
			</c:choose>
		</tbody>
    </table>

 	<div id="paging">
		<div id="paging">
	    	<c:url var="pageUrl" value="/mng/lms/cla/curriculumStudyList.do${_BASE_PARAM}">
	    </c:url>
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
