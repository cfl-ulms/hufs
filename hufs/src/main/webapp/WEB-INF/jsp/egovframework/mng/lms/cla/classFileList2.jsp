<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchcrclbNm}"><c:param name="searchcrclbNm" value="${searchVO.searchcrclbNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty param.tmplatImportAt}"><c:param name="tmplatImportAt" value="${param.tmplatImportAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CLASS_MANAGE"/>
	<c:param name="depth1" value="CLASS_FILE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수업자료"/>
</c:import>

<script>
$(document).ready(function(){

	$("#searchCtgryId").change(function(){
		var tempCtgryId = $(this).val();

		$.ajax({
			type:"post",
			dataType:"json",
			url:"/mng/lms/classCurriculumList.json",
			data:{"searchCtgryId":tempCtgryId} ,
			success: function(data) {
				var html = "";
				var resultList = data.curriculumbaseList;

				if(resultList.length != 0){
					for(var i=0; i<resultList.length; i++){
						html += "<option value=\""+resultList[i].crclId+"\">"+resultList[i].crclNm+"</option>";
					}
					$('#searchCrclId').html(html);
				}
			},
			error:function() {
				//alert("error");
			}
		});
	});
});

function fnPop(url){
	window.open(url, '', 'width=400px,height=500px');
}

</script>

<div id="cntnts">
	<div id="bbs_search">
	  	<form id="frm" method="post" action="<c:url value="/mng/lms/cla/classFileList.do"/>">
	  		<strong>주관기관 : </strong>
	  		<select id="searchCtgryId" name="searchCtgryId">
				<option value="">전체</option>
				<c:forEach var="result" items="${ctgryList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1' }">
						<option value="${result.ctgryId}" ${fileVo.searchCtgryId eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<label><strong>과정명 : </strong><input type="text" name="searchCrclNm" value="${fileVo.searchCrclNm }" class="inp_s" id="inp_text" /></label>
			<br/>
			<label><strong>자료파일명 : </strong> <input type="text" name="searchFileNm" value="${fileVo.searchFileNm }" class="inp_s" id="inp_text" /></label>
	  		<label><strong>파일 등록자 : </strong> <input type="text" name="searchRegisterNm" value="${fileVo.searchRegisterNm }" class="inp_s" id="inp_text" /></label>
	  		<br/>
	  		<label><strong>활용 횟수 : </strong> <input type="number" name="searchUseCnt" value="${fileVo.searchUseCnt }" class="inp_s" id="inp_text" placeholder="이상"/></label>
	  		<strong>자료 유형 : </strong>
	  		<label><input type="checkbox" name="fileExtOther" value="Y" ${param.fileExtOther eq 'Y' ? 'checked' : '' }/>파일</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="fileExtImg" value="Y" ${param.fileExtImg eq 'Y' ? 'checked' : '' }/>이미지</label>&nbsp;&nbsp;
			<label><input type="checkbox" name="fileExtMov" value="Y" ${param.fileExtMov eq 'Y' ? 'checked' : '' }/>영상</label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	<div>
		<h2>총 ${fileListCnt}개</h2>
	</div>
	<table class="chart_board">
	    <colgroup>
	    	<%-- <col class="co6"/> --%>
	    	<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co4"/>
			<col class="co6"/>
		</colgroup>
	    <thead>
	      <tr>
	      	<!-- <th>선택</th> -->
	        <th>등록일</th>
	        <th>유형</th>
	        <th>미리보기</th>
	        <th>자료파일명</th>
	        <th>주관기관</th>
	        <th>과정명</th>
	        <th>파일등록자</th>
	        <th>활용 횟수</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<c:choose>
	    		<c:when test="${not empty fileList}">
			    	<c:forEach var="result" items="${fileList}" varStatus="status">
			    		<c:url var="viewUrl" value="/lms/cla/classFileView.do">
							<c:param name="atchFileId" value="${result.atchFileId}" />
						  	<c:param name="fileSn" value="${result.fileSn}" />
						  	<c:param name="plId" value="${result.plId}" />
						</c:url>
			    		<tr onclick="fnPop('${viewUrl}');">
			    			<!-- <td><input type="checkbox" value=""/></td> -->
			    			<td>${result.frstRegisterPnttm}</td>
			    			<td>${result.fileExtsn}</td>
			    			<td><img src='/template/manage/images/ico_file.gif' alt='파일'/></td>
			    			<td>${result.orignlFileNm }</td>
			    			<td>${result.ctgryNm}</td>
			    			<td>${result.crclNm}</td>
			    			<td>${result.userNm}</td>
			    			<td>${result.attachCnt}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>

				</c:otherwise>
			</c:choose>
		</tbody>
    </table>

  <div id="paging">
  	<c:url var="pageUrl" value="/mng/lms/cla/classFileList.do${_BASE_PARAM}"></c:url>
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
