<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CURRICULUM_MANAGE"/>
    <c:param name="depth1" value="BASE_CRCL"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과정등록관리"/>
</c:import>
<style>
#datatable input{width:70px;text-align:center;}
.chart_board2 tbody tr{height:34px;}
.chart_board2 tbody .datarow{text-align:center;}
.box-btn-top{position:relative;}
#btn-cert{position:absolute;top:-30px;right:15px;}
#btn-certview{position:absolute;top:-30px;right:140px;}
#btn-recert{position:absolute;top:-30px;right:15px;}
</style>
<script>
$(document).ready(function(){
	//수료증 발급
	$("#btn-cert").click(function(){
		var crclId = "<c:out value="${curriculumVO.crclId}"/>",
			finishYCnt = $("#finishYCnt").val();
		
		if(finishYCnt > 0){
			$.ajax({
				url : "/lms/insertCertificate.json"
				, type : "post"
				, dataType : "json"
				, data : {crclId : crclId}
				, success : function(data){
					if(data.successYn == "Y"){
						alert("수료증이 정상 발급되었습니다.");
						location.reload(); 
					}
				}, error : function(){
					alert("error");
				}
			});
			return false;
		}else{
			alert("수료증 발급 대상자가 없습니다.")
		}
		
	});
	
	//수료증 재발급
	$("#btn-recert").click(function(){
		var crclId = "<c:out value="${curriculumVO.crclId}"/>",
			finishYCnt = $("#finishYCnt").val();
		
		if(finishYCnt > 0 && finishYCnt != $(".certBtn").length){
			if(confirm("수료증을 재발급하시면 기존 발행된 수료증은 유지되며 \n미발급 받은 학생만 수료증이 발급됩니다. \n수료증을 재발급 하시겠습니까?")){
				$.ajax({
					url : "/lms/reCertificate.json"
					, type : "post"
					, dataType : "json"
					, data : {crclId : crclId}
					, success : function(data){
						if(data.successYn == "Y"){
							alert("수료증이 재발급되었습니다.");
							location.reload(); 
						}
					}, error : function(){
						alert("error");
					}
				});
			}
		}else{
			alert("수료증 발급 대상자가 없습니다.")
		}
		return false;
	});
});
</script>
<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm}"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
<c:if test="${curriculumVO.processSttusCode > 0}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        <c:param name="step" value="7"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
	<%-- <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }"> --%>
	    <ul class="group_list">
	        <li><a href="/mng/lms/manage/gradeTotal.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title">총괄평가</a></li>
	        <li class="partition"> | </li>
	        <li class="act"><a href="/mng/lms/manage/completeStand.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title">수료기준</a></li>
	    </ul>
    <%-- </c:if> --%>
    
    <div id="bbs_search">
        <form name="frm" id="searchForm" method="post" action="<c:url value="/mng/lms/manage/completeStand.do"/>">
            <input type="hidden" name="crclId" value="${param.crclId }"/>
            <input type="hidden" name="step" value="${searchVO.step}"/>
			
			<c:if test="${not empty gradeSummary}">
				<select name="searchFinishAt">
					<option value="">전체</option>
					<option value="Y" <c:if test="${searchVO.searchFinishAt eq 'Y'}">selected="selected"</c:if>>수료</option>
					<option value="N" <c:if test="${searchVO.searchFinishAt eq 'N'}">selected="selected"</c:if>>미수료</option>
				</select>
			</c:if>
            <input type="text" name="searchUserNm" value="${param.searchUserNm}" id="searchUserNm" class="searchUserNm" placeholder="학생 이름"/>
            <br/>
            <input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
        </form>
    </div>
    
    <c:choose>
		<c:when test="${empty gradeSummary}">
			<c:set var="studentList" value="${selectStudentList}"/>
		</c:when>
		<c:otherwise>
			<c:set var="studentList" value="${gradeList}"/>
		</c:otherwise>
	</c:choose>
	<c:if test="${not empty gradeSummary}">
	    <p>
	    	전체 : <c:out value="${gradeSummary.stuCnt}"/>명 | 수료 : <c:out value="${gradeSummary.finishYCnt}"/>명  | 미수료 : <c:out value="${gradeSummary.finishNCnt}"/>명
	    </p>
	    
	    <div class="box-btn-top">
	    	<c:choose>
	    		<c:when test="${certCnt > 0}">
	    			<a href="/lms/selectCertificate.do?searchCrclId=${curriculumVO.crclId}" id="btn-certview" class="btn_mngTxt" target="_blank">수료증 조회</a>
	    			<a href="#" id="btn-recert" class="btn_mngTxt">수료증 재발급</a>
	    		</c:when>
	    		<c:otherwise>
	    			<a href="#" id="btn-cert" class="btn_mngTxt">수료증 발급</a>
	    		</c:otherwise>
	    	</c:choose>
	    </div>
    </c:if>
    <form name="frm" id="frm" method="post" action="<c:url value="/lms/manage/gradeTotalUpdate.do"/>">
		<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
		<input type="hidden" name="step" value="${searchVO.step}"/>

        <div class="tbl">
            <div class="tbl-left" style="width:609px;margin-right:1px;float:left;">
			    <table class="chart_board2">
			        <colgroup>
                      <col>
                      <col>
                      <col>
                      <col>
                      <col>
                    </colgroup>
                    <thead>
                      <tr class='bg-gray font-700'>
                        <th scope='colgroup' colspan='5'>학생정보</th>
                      </tr>
                      <tr class='bg-gray font-700'>
                        <th scope='col'>소속</th>
                        <th scope='col'>생년월일</th>
                        <th scope='col'>이름</th>
                        <th scope='col'>조</th>
                        <th scope='col'>수료여부</th>
                      </tr>
                    </thead>
			        <tbody>
			        	<c:set var="finishYCnt" value="0"/>
	                  	<c:forEach var="result" items="${studentList}" varStatus="stauts">
	                  		<tr class="" style="height: 40px;">
	                        <td scope='row' class='title'>
	                          <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.mngDeptNm}"/></span></div>
	                        </td>
	                        <td><c:out value="${result.brthdy}"/></td>
	                        <td><c:out value="${result.userNm}"/></td>
	                        <td>
	                        	<c:if test="${not empty result.groupCnt}">
	                        		<c:out value="${result.groupCnt}"/>조
	                        	</c:if>
	                        </td>
	                        <td>
	                        	<c:out value="${result.finishAt}"/>
	                        	<c:if test="${not empty result.certificateId}">
	                        		<a href="/lms/selectCertificate.do?searchCertificateId=${result.certificateId}" class="btn_mngTxt certBtn" data-id="${result.certificateId}" target="_blank">수료증</a>
	                        	</c:if>
	                        	<c:if test="${result.finishAt eq 'Y'}">
	                        		<c:set var="finishYCnt" value="${finishYCnt + 1}"/>
	                        	</c:if>
	                        </td>
	                     </tr>
	                  	</c:forEach>
		            </tbody>
			    </table>
			</div>
			<div class="tbl-main-wrap" style="overflow:auto;">
			    <div class="tbl-main">
			        <table id="datatable" class="chart_board2">
	                    <thead>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${finishList}">
									<c:if test="${result.ctgryLevel eq 1}">
										<c:choose>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
												<th scope='col'><c:out value="${result.ctgryNm}"/></th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
												<th scope='col'>출결</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
												<th scope='col'><c:out value="${result.ctgryNm}"/></th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
												<th scope='col'><c:out value="${result.ctgryNm}"/></th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
												<th scope='col'><c:out value="${result.ctgryNm}"/></th>
											</c:when>
										</c:choose>
									</c:if>
								</c:forEach>
	                        </tr>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${finishList}">
									<c:if test="${result.ctgryLevel eq 1}">
										<c:choose>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
												<th scope='col'><c:out value="${curriculumVO.fnTot}"/>점 이상</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
												<th scope='col'><c:out value="${curriculumVO.fnAttend}"/>% 이상</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
												<th scope='col'><c:out value="${curriculumVO.fnGradeTot}"/>점 이상</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
												<th scope='col'><c:out value="${curriculumVO.fnGradeFail}"/>점 이상</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
												<th scope='col'><c:out value="${curriculumVO.fnHomework}"/>점 이상</th>
											</c:when>
										</c:choose>
									</c:if>
								</c:forEach>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<c:choose>
	               				<c:when test="${empty gradeSummary}">
	               					<c:set var="studentList" value="${selectStudentList}"/>
	               				</c:when>
	               				<c:otherwise>
	               					<c:set var="studentList" value="${gradeList}"/>
	               				</c:otherwise>
	               			</c:choose>
	                    	<c:forEach var="stResult" items="${studentList}" varStatus="stauts">
	                    		<tr class="datarow" style="height: 40px;">
	                    			<c:forEach var="result" items="${finishList}">
										<c:if test="${result.ctgryLevel eq 1}">
											<c:choose>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
													<td scope='col'>
														<span <c:if test="${stResult.chScr < curriculumVO.fnTot}">class='font-red'</c:if>>
															<c:out value="${fn:replace(stResult.chScr,'.0','')}"/>
														</span>
													</td>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
													<td scope='col'>
														<span <c:if test="${stResult.attPer < curriculumVO.fnAttend}">class='font-red'</c:if>>
															<c:out value="${stResult.attPer}"/>
														</span>
													</td>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
													<td scope='col'>
														<span <c:if test="${stResult.examScr < curriculumVO.fnGradeTot}">class='font-red'</c:if>>
															<c:out value="${fn:replace(stResult.examScr,'.00','')}"/>
														</span>
													</td>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
													<td scope='col'>
														<span <c:if test="${stResult.examMin < curriculumVO.fnGradeFail}">class='font-red'</c:if>>
															<c:out value="${stResult.examMin}"/>
														</span>
													</td>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
													<td scope='col'>
														<span <c:if test="${stResult.hChScr < curriculumVO.fnHomework}">class='font-red'</c:if>>
															<c:out value="${stResult.hChScr}"/>
														</span>
													</td>
												</c:when>
											</c:choose>
										</c:if>
									</c:forEach>
			                     </tr>
	                    	</c:forEach>
	                    </tbody>
	                </table>
			    </div>
			</div>
		</div>
		
		<input type="hidden" id="finishYCnt" value="${finishYCnt}"/>
    </form>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>