<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchPositionCode}"><c:param name="searchPositionCode" value="${searchVO.searchPositionCode}" /></c:if>
		<c:if test="${!empty searchVO.searchDept}"><c:param name="searchDept" value="${searchVO.searchDept}" /></c:if>
		<c:if test="${!empty searchVO.searchGender}"><c:param name="searchGender" value="${searchVO.searchGender}" /></c:if>
		<c:if test="${!empty searchVO.searchGroup}"><c:param name="searchGroup" value="${searchVO.searchGroup}" /></c:if>
		<c:if test="${!empty searchVO.searchWork}"><c:param name="searchWork" value="${searchVO.searchWork}" /></c:if>
		<c:if test="${!empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
		<c:if test="${!empty searchVO.templateAt}"><c:param name="templateAt" value="${searchVO.templateAt}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
<c:set var="_ACTION" value="/mng/usr/EgovMberManage.do" />
<c:choose>
<c:when test="${searchVO.templateAt ne 'N'}">
	<c:import url="/mng/template/top.do" charEncoding="utf-8">
		<c:param name="menu" value="MBER_MANAGE"/>
		<c:param name="depth1" value="MBER_MANAGE"/>
		<c:param name="depth2" value="PROF_MANAGE"/>
		<c:param name="title" value="교원관리"/>
	</c:import>	

<script>
$(document).ready(function(){
});
</script>
<style>
tr td{cursor:pointer;}
</style>
	<div id="cntnts">

		<%--  사이트 선택 주석처리
		<c:if test="${USER_INFO.userSe > 10}">
			<form id="SiteListForm" name="SiteListForm" action="${_ACTION }" method="post">
				<div id="bbs_search">
					<c:import url="/mng/sym/sit/selectCommonSiteList.do"/>
				</div>
			</form>
		</c:if> --%>	
		
			<form:form name="listForm" action="${_ACTION }" method="post">
			<%-- <input type="hidden" name="siteId" value="${searchVO.siteId}"/> --%>
			<div id="bbs_search">
			
				<label for="selectGroupCode"><strong>소속 : </strong></label>
				<select name="searchGroup" id="selectGroupCode">
					<option value="">전체</option>
					<c:forEach var="result" items="${groupList}" varStatus="status">
						<option value="${result.code}" <c:if test="${searchVO.searchGroup eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</select>
		  	  	<label for="selectDeptCode"><strong>주관기관 : </strong></label>
				<select name="searchDept" id="selectDeptCode">
					<option value="">전체</option>
					<c:forEach var="result" items="${deptList}" varStatus="status">
						<option value="${result.ctgryId}" <c:if test="${searchVO.searchDept eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
					</c:forEach>
		  	  	</select>
				<label for="selectGender"><strong>성별 : </strong></label>
				<select name="searchGender" id="selectGender">
					<option value="">전체</option>
					<option value="M" <c:if test="${searchVO.searchCondition eq 'M'}">selected="selected"</c:if>>남</option>
					<option value="F" <c:if test="${searchVO.searchCondition eq 'F'}">selected="selected"</c:if>>여</option>
		  	  	</select>
		  	  	<br/>
				<label for="selectSeCode"><strong>직위 : </strong></label>
				<select name="searchPositionCode" id="selectPositionCode">
					<option value="">전체</option>
					<c:forEach var="result" items="${positionList}" varStatus="status">
						<option value="${result.code}" <c:if test="${searchVO.searchPositionCode eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</select>
				<label for="selectWorkStatusCode"><strong>재직상태 : </strong></label>
				<select name="searchWork" id="selectWorkStatusCode">
					<option value="">전체</option>
					<c:forEach var="result" items="${workStatusList}" varStatus="status">
						<option value="${result.code}" <c:if test="${searchVO.searchWork eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
					</c:forEach>
		  	  	</select>
				<label><strong>성명 : </strong> <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" id="inp_text" /></label>
				<br/>
				<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			</div>
			<p class="total">총  회원 ${paginationInfo.totalRecordCount}명ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p>
	
			<table class="chart_board">
			<colgroup>
				<col width="70"/>				
				<col width="120"/>
				<col width="70"/>
				<col width="70"/>
				<col width="70"/>
				<col width="70"/>
				<col width="120"/>
				<col width="120"/>
				<col width="80"/>
				<col width="70"/>
				<col width="180"/>
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>아이디</th>
					<th>이름</th>
					<th>사번</th>
					<th>재직상태</th>
					<th>직위</th>
					<th>소속</th>
					<th>주관기관</th>
					<th>핸드폰</th>
					<th>계정사용여부</th>
					<th>교육과정이력</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="result" items="${resultList}" varStatus="status">
				<tr class="data-row" onclick="location.href='/mng/usr/EgovMberView.do?userId=${result.userId }&userSeCode=08'">
					<td class="listtd"><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
					<td class="listtd"><c:out value="${result.userId}" /></td>
					<td class="listtd"><c:out value="${result.userNm}" /></td>
					<td class="listtd"><c:out value="${result.userSchNo}" /></td>
					<td class="listtd"><c:out value="${result.workStatusCodeNm}" /></td>
					<td class="listtd"><c:out value="${result.positionCodeNm}" /></td>
					<td class="listtd"><c:out value="${result.groupCode}" /></td>
					<td class="listtd"><c:out value="${result.mngDeptCodeNm}" /></td>
					<td class="listtd"><c:out value="${result.moblphonNo}" /></td>
					<td class="listtd" <c:if test="${result.useYn eq 'N'}">style="color:red;"</c:if>>
						<c:choose>
							<c:when test="${result.useYn eq 'N'}">사용 안함</c:when>
							<c:otherwise>사용함</c:otherwise>
						</c:choose>
					</td>
					<td class="listtd">
						<a href="/mng/usr/EgovMberView.do?userId=${result.userId }&userSeCode=08">
							<c:choose>
								<c:when test="${result.currCnt eq 0 }">
									없음
								</c:when>
								<c:when test="${result.currCnt eq 1 }">
									<c:out value="${result.currNm }"/>
								</c:when>
								<c:when test="${result.currCnt > 1 }">
									<c:out value="${result.currNm } 외 ${result.currCnt - 1}건 "/>
								</c:when>
							</c:choose>
						</a>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
		      <tr>
		      	<td class="listtd" colspan="10">
		        	<spring:message code="common.nodata.msg" />
		        </td>
		      </tr>
		    </c:if>
	
			</tbody>
			</table>
		
		<div class="btn_r">
	       	<a href="<c:url value='/mng/usr/EgovMberAddView.do'/>"><img src="${_IMG}/btn/btn_regist.gif" /></a>
		</div>
	
		<div id="paging">
			<c:url var="pageUrl" value="/mng/usr/EgovMberManage.do${_BASE_PARAM}">
			</c:url>
	
			<c:if test="${not empty paginationInfo}">
				<ul>
					<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				</ul>
			</c:if>
		</div>
		</form:form>
	</div>
	<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
</c:when>
<c:otherwise>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>교원검색</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = [],
			name = [],
			mngdept = [],
			mngdeptnm = [];
		
		
		$(".chk").each(function(){
			if($(this).is(":checked")){
				id.push($(this).val());
				name.push($(this).data("name"));
				mngdept.push($(this).data("mngdept"));
				mngdeptnm.push($(this).data("mngdeptnm"));
			}
		});
		
		window.opener.selUser(id, name, mngdept, mngdeptnm, "${searchVO.position}");
		window.close();
		
		return false;
	});
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
<form name="listForm" id="listForm" method="post" method="post" action="${_ACTION }">
	<input type="hidden" name="templateAt" value="${searchVO.templateAt}"/>
	<input type="hidden" name="position" value="${searchVO.position}"/>
	<input type="hidden" name="searchDept" value="${searchVO.searchDept}"/>
	<div id="bbs_search">
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" class="inp_s" id="inp_text" placeholder="이름을 입력해 주세요.">
		<input type="image" src="/template/manage/images/btn/btn_search.gif" alt="검색">
  	</div>
	<table class="chart_board">
		<colgroup>
			<col width="70"/>				
			<col width="120"/>
			<col width="70"/>
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>이름</th>
				<th>이메일</th>
				<th>주관기관/학과</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr>
				<td class="listtd"><input type="checkbox" class="chk" value="${result.userId}" data-name="${result.userNm}" data-mngdept="${result.mngDeptCode}" data-mngdeptnm="${result.mngDeptCodeNm}"/></td>
				<td class="listtd"><c:out value="${result.userNm}" /></td>
				<td class="listtd"><c:out value="${result.emailAdres}" /></td>
				<td class="listtd"><c:out value="${result.mngDeptCodeNm}" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(resultList) == 0}">
	      <tr>
	      	<td class="listtd" colspan="4">
	        	<spring:message code="common.nodata.msg" />
	        </td>
	      </tr>
	    </c:if>
	</tbody>
	</table>
	
    <div class="btn_c">
		<a href="#" class="btn_ok" data-target="${param.target}"><img src="/template/manage/images/board/btn_confirm.gif"/></a>
	</div>
</form>
</div>
</c:otherwise>
</c:choose>

