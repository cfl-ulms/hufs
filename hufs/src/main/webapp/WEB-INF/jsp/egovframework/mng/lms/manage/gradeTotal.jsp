<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CURRICULUM_MANAGE"/>
    <c:param name="depth1" value="BASE_CRCL"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과정등록관리"/>
</c:import>
<style>
#datatable input{width:70px;text-align:center;}
.chart_board2 tbody tr{height:34px;}
</style>
<script>
$(document).ready(function(){
	//저장
	$("#btn-reg").click(function(){
		var leng = $("#datatable").find("input").length - 1;
		
		$("#datatable").find("input").each(function(i){
			if(!$(this).val()){
				alert("변환점수 및 총점을 입력해주세요.");
				$(this).focus();
				return false;
			}
			
			if(leng == i){
				$("#frm").submit();
			}
		});
		
		return false;
	});
});
</script>
<table class="chart2">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm}"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
<br/>
	
<c:if test="${curriculumVO.processSttusCode > 0}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        <c:param name="step" value="7"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
	<%-- <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }"> --%>
	    <ul class="group_list">
	        <li class="act"><a href="/mng/lms/manage/gradeTotal.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title">총괄평가</a></li>
	        <li class="partition"> | </li>
	        <li><a href="/mng/lms/manage/completeStand.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title">수료기준</a></li>
	    </ul>
    <%-- </c:if> --%>
    
     <!-- 테이블영역-->
    <table class="chart_board2">
      	<c:choose>
			<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
				<thead>
		             <tr class='bg-gray font-700'>
						<th class="alC">P/F</th>
						<th class="alC">Pass</th>
						<th class="alC">Fail</th>
					</tr>
				</thead>
				
				<tbody>
	                <tr class="font-700">
						<td class="line">점수</td>
						<td class="alC">${curriculumVO.pass} ~ 100</td>
						<td class="alC">0 ~ ${curriculumVO.fail}</td>
					</tr>
				
					<c:set var="passCnt" value="0"/>
					<c:set var="failCnt" value="0"/>
	           		<c:forEach var="result" items="${gradeTotList}">
	           			<%-- 
	           			<c:if test="${result.chScr >= curriculumVO.pass}"><c:set var="passCnt" value="${passCnt + 1}"/></c:if>
	           			<c:if test="${result.chScr <= curriculumVO.fail}"><c:set var="failCnt" value="${failCnt + 1}"/></c:if>
	           			 --%>
	           			<c:if test="${result.confirmGrade eq 'P'}"><c:set var="passCnt" value="${passCnt + 1}"/></c:if>
               			<c:if test="${result.confirmGrade eq 'F'}"><c:set var="failCnt" value="${failCnt + 1}"/></c:if>
	           		</c:forEach>
				
					<tr class="alC">
						<td class="line">실제 학생 수</td>
						<td class="alC"><c:out value="${passCnt}"/></td>
						<td class="alC"><c:out value="${failCnt}"/></td>
					</tr>
				</tbody>
			</c:when>
			<c:otherwise>
				<colgroup>
		           <col style='width:11%'>
		         </colgroup>
			           <thead>
			             <tr class='bg-gray font-700'>
			               	<th scope='col'>Grade</th>
				               <c:forEach var="result" items="${gradeTypeList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
										<th scope='col'><c:out value="${result.ctgryNm}"/></th>
									</c:if>
								</c:forEach>
								<th scope='col'>계</th>
				         </tr>
				       </thead>
				       <tbody>
				         <tr class="font-700">
				            <c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
							<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
							<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
							<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
							<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
							<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
							<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
							<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
							<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
							<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
							<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
				
				            <td scope='row'>점수</td>
				            <td scope='row'>${aplus}</td>
							<td scope='row'>${a}</td>
							<td scope='row'>${bplus}</td>
							<td scope='row'>${b}</td>
							<td scope='row'>${cplus}</td>
							<td scope='row'>${c}</td>
							<td scope='row'>${dplus}</td>
							<td scope='row'>${d}</td>
							<td scope='row'>${f}</td>
							<td scope='row'></td>
				          </tr>
				              
				          <c:set var="aplusCnt" value="0"/>
						  <c:set var="aCnt" value="0"/>
						  <c:set var="bplusCnt" value="0"/>
						  <c:set var="bCnt" value="0"/>
						  <c:set var="cplusCnt" value="0"/>
						  <c:set var="cCnt" value="0"/>
						  <c:set var="dplusCnt" value="0"/>
						  <c:set var="dCnt" value="0"/>
						  <c:set var="fCnt" value="0"/>
						  <c:set var="sumCnt" value="0"/>
			           		<c:forEach var="result" items="${gradeTotList}">
			           			<c:if test="${result.confirmGrade eq 'A+'}"><c:set var="aplusCnt" value="${aplusCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'A'}"><c:set var="aCnt" value="${aCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'B+'}"><c:set var="bplusCnt" value="${bplusCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'B'}"><c:set var="bCnt" value="${bCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'C+'}"><c:set var="cplusCnt" value="${cplusCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'C'}"><c:set var="cCnt" value="${cCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'D+'}"><c:set var="dplusCnt" value="${dplusCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'D'}"><c:set var="dCnt" value="${dCnt + 1}"/></c:if>
			           			<c:if test="${result.confirmGrade eq 'F'}"><c:set var="fCnt" value="${fCnt + 1}"/></c:if>
			           			<c:set var="sumCnt" value="${sumCnt + 1}"/>
			           		</c:forEach>
				             
			             <tr class="">
				              <td scope='row'>실제 학생 수</td>
				              <td><c:out value="${aplusCnt}"/></td>
				              <td><c:out value="${aCnt}"/></td>
				              <td><c:out value="${bplusCnt}"/></td>
				              <td><c:out value="${bCnt}"/></td>
				              <td><c:out value="${cplusCnt}"/></td>
				              <td><c:out value="${cCnt}"/></td>
				              <td><c:out value="${dplusCnt}"/></td>
				              <td><c:out value="${dCnt}"/></td>
				              <td><c:out value="${fCnt}"/></td>
				              <td class='font-red'><c:out value="${sumCnt}"/></td>
			            </tr>
	           	</tbody>
			</c:otherwise>
		</c:choose>
    </table>
    <br/><br/>
    <div id="bbs_search">
        <form name="frm" id="searchForm" method="post" action="<c:url value="/mng/lms/manage/gradeTotal.do"/>">
            <input type="hidden" name="crclId" value="${param.crclId }"/>
            <input type="hidden" name="step" value="${searchVO.step}"/>

            <input type="text" name="searchUserNm" value="${param.searchUserNm}" id="searchUserNm" class="searchUserNm" placeholder="학생 이름"/>
            <br/>
            <input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
        </form>
    </div>
    
    <form name="frm" id="frm" method="post" action="<c:url value="/mng/lms/manage/gradeTotalUpdate.do"/>">
		<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
		<input type="hidden" name="step" value="${searchVO.step}"/>

        <div class="tbl">
            <div class="tbl-left" style="width:609px;margin-right:1px;float:left;">
			    <table class="chart_board2">
			        <colgroup>
                      <col>
                      <col>
                      <col>
                      <col>
                      <col>
                      <col width="90px;">
                      <col class='bg-point-light'>
                      <col class='bg-point-light'>
                    </colgroup>
			        <thead>
                      <tr class='bg-gray font-700'>
                        <th scope='colgroup' colspan='4' style="height:58px;">학생정보</th>
                        <th scope='col' rowspan="2">변환점수</th>
                        <th scope='col' rowspan="2">확정등급</th>
                        <th scope='col' rowspan="2">총점</th>
                        <th scope='col' rowspan="2">석차</th>
                      </tr>
                      <tr class='bg-gray font-700'>
                        <th scope='col'>소속</th>
                        <th scope='col'>생년월일</th>
                        <th scope='col'>이름</th>
                        <th scope='col'>조</th>
                      </tr>
                    </thead>
			        <tbody>
			        	<c:choose>
               				<c:when test="${fn:length(gradeList) == 0}">
               					<c:set var="studentList" value="${selectStudentList}"/>
               				</c:when>
               				<c:otherwise>
               					<c:set var="studentList" value="${gradeList}"/>
               				</c:otherwise>
               			</c:choose>
		                <c:forEach var="result" items="${studentList}" varStatus="stauts">
		                    <tr class="">
		                        <td scope='row' class='title'>
		                          <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.mngDeptNm}"/></span></div>
		                        </td>
		                        <td><c:out value="${result.brthdy}"/></td>
		                        <td>
		                        	<c:out value="${result.userNm}"/>
		                        	<input type="hidden" name="userIdList" value="${result.userId}"/>
		                        </td>
		                        <td>
		                        	<c:if test="${not empty result.groupCnt}">
		                        		<c:out value="${result.groupCnt}"/>조
		                        	</c:if>
		                        </td>
		                        <td><c:out value="${fn:replace(result.chScr,'.0','')}"/></td>
		                        <td>
		                        	<select name="confirmGradeList" class="select2" data-select="style3" data-placeholder="등급">
		                        		<option value="">선택</option>
		                        		<c:choose>
		                        			<c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
		                        				<c:choose>
		                        					<c:when test="${empty curriculumVO.fnTot && empty curriculumVO.fnAttend && empty curriculumVO.fnGradeTot && empty curriculumVO.fnGradeFail && empty curriculumVO.fnHomework}">
		                        						
		                        						<c:choose>
		                        							<c:when test="${empty result.confirmGrade}">
						                        				<option value="P" selected="selected">Pass</option>
						                        				<option value="F">Fail</option>
		                        							</c:when>
		                        							<c:otherwise>
						                        				<option value="P" <c:if test="${result.confirmGrade eq 'P'}">selected="selected"</c:if>>Pass</option>
						                        				<option value="F" <c:if test="${result.confirmGrade eq 'F'}">selected="selected"</c:if>>Fail</option>
		                        							</c:otherwise>
		                        						</c:choose>
		                        					
		                        					</c:when>
		                        					<c:otherwise>
				                        				<option value="P" <c:if test="${result.confirmGrade eq 'P'}">selected="selected"</c:if>>Pass</option>
				                        				<option value="F" <c:if test="${result.confirmGrade eq 'F'}">selected="selected"</c:if>>Fail</option>
		                        					</c:otherwise>
		                        				</c:choose>
		                        			</c:when>
		                        			<c:otherwise>
		                        				<option value="A+" <c:if test="${result.confirmGrade eq 'A+'}">selected="selected"</c:if>>A+</option>
				                        		<option value="A" <c:if test="${result.confirmGrade eq 'A'}">selected="selected"</c:if>>A</option>
				                        		<option value="B+" <c:if test="${result.confirmGrade eq 'B+'}">selected="selected"</c:if>>B+</option>
				                        		<option value="B" <c:if test="${result.confirmGrade eq 'B'}">selected="selected"</c:if>>B</option>
				                        		<option value="C+" <c:if test="${result.confirmGrade eq 'C+'}">selected="selected"</c:if>>C+</option>
				                        		<option value="C" <c:if test="${result.confirmGrade eq 'C'}">selected="selected"</c:if>>C</option>
				                        		<option value="D+" <c:if test="${result.confirmGrade eq 'D+'}">selected="selected"</c:if>>D+</option>
				                        		<option value="D" <c:if test="${result.confirmGrade eq 'D'}">selected="selected"</c:if>>D</option>
				                        		<option value="F" <c:if test="${result.confirmGrade eq 'F'}">selected="selected"</c:if>>F</option>
		                        			</c:otherwise>
		                        		</c:choose>
		                        	</select>
		                        </td>
		                        <td><c:out value="${fn:replace(result.totScr,'.0','')}"/></td>
		                        <td><c:out value="${result.rk}"/></td>
		                     </tr>
		                </c:forEach>
		            </tbody>
			    </table>
			</div>
			<div class="tbl-main-wrap" style="overflow:auto;">
			    <div class="tbl-main">
			        <table id="datatable" class="chart_board2">
	                    <thead>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${evaluationList}" varStatus="status">
		                        	<c:choose>
	                        			<c:when test="${result.evtId eq 'CTG_0000000000000119'}">
	                        				<c:set var="colspan" value="1"/>
	                        			</c:when>
	                        			<c:when test="${result.evtId eq 'CTG_0000000000000109'}">
	                        				<c:set var="colspan" value="4"/>
	                        			</c:when>
	                        			<c:otherwise>
	                        				<c:set var="colspan" value="2"/>
	                        			</c:otherwise>
	                        		</c:choose>
									<th colspan="${colspan}" style="height:40px;">
										<c:out value="${result.evtNm}"/>
										(<c:out value="${result.evtVal}"/>%)
										<c:choose>
		                        			<c:when test="${result.evtId eq 'CTG_0000000000000111'}">
		                        				<br/><input type="number" name="midtermTopScore" value="${curriculumVO.midtermTopScore eq 0 ? '' : curriculumVO.midtermTopScore}" class="top-score" placeholder="만점 점수 입력"/>
		                        			</c:when>
		                        			<c:when test="${result.evtId eq 'CTG_0000000000000112'}">
		                        				<br/><input type="number" name="finalTopScore" value="${curriculumVO.finalTopScore eq 0 ? '' : curriculumVO.finalTopScore}" class="top-score" placeholder="만점 점수 입력"/>
		                        			</c:when>
		                        			<c:when test="${result.evtId eq 'CTG_0000000000000113'}">
		                        				<br/><input type="number" name="evaluationTopScore" value="${curriculumVO.evaluationTopScore eq 0 ? '' : curriculumVO.evaluationTopScore}" class="top-score" placeholder="만점 점수 입력"/>
		                        			</c:when>
		                        		</c:choose>
									</th>
								</c:forEach>
	                        </tr>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${evaluationList}" varStatus="status">
									<th>100점<br/>만점 기준</th>
									<%-- 출석, 기타 --%>
									<c:if test="${result.evtId ne 'CTG_0000000000000119' and result.evtId ne 'CTG_0000000000000109'}">
										<th>총점</th>
									</c:if>
									<c:if test="${result.evtId eq 'CTG_0000000000000109'}">
										<th>출석</th>
										<th>지각</th>
										<th>결석</th>
									</c:if>
								</c:forEach>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<c:choose>
                  				<c:when test="${fn:length(gradeList) == 0}">
                  					<c:set var="studentList" value="${selectStudentList}"/>
                  				</c:when>
                  				<c:otherwise>
                  					<c:set var="studentList" value="${gradeList}"/>
                  				</c:otherwise>
                  			</c:choose>
	                        <c:forEach var="stResult" items="${studentList}" varStatus="stauts">
           						<c:set var="etcCnt" value="0"/>
	                      		<tr class="gradeTr">
	                      			<c:forEach var="result" items="${evaluationList}">
	                      				<c:choose>
	                      					<%-- 중간시험 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000111'}">
	                      						<td>
	                      							<%-- <input type="number" class="gradenum" name="midtermScoreList" step="any" value="${fn:replace(stResult.midtermScore, '.00', '')}"/> --%>
	                      							<c:out value="${fn:replace(stResult.midtermScore, '.00', '')}"/>
	                      						</td>
	                      						<td><input type="number" class="gradenum" name="midtermTotalScoreList" value="${stResult.midtermTotalScore}"/></td>
	                      					</c:when>
	                      					<%-- 기말고사 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000112'}">
	                      						<td>
	                      							<%-- <input type="number" class="gradenum" name="finalScoreList" step="any" value="${fn:replace(stResult.finalScore, '.00', '')}"/> --%>
	                      							<c:out value="${fn:replace(stResult.finalScore, '.00', '')}"/>
	                      						</td>
	                      						<td><input type="number" class="gradenum" name="finalTotalScoreList" value="${stResult.finalTotalScore}"/></td>
	                      					</c:when>
	                      					<%-- 수시시험 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000113'}">
	                      						<td>
	                      							<%-- <input type="number" class="gradenum" name="evaluationScoreList" step="any" value="${fn:replace(stResult.evaluationScore, '.00', '')}"/> --%>
	                      							<c:out value="${fn:replace(stResult.evaluationScore, '.00', '')}"/>
	                      						</td>
	                      						<td><input type="number" class="gradenum" name="evaluationTotalScoreList" value="${stResult.evaluationTotalScore}"/></td>
	                      					</c:when>
	                      					<%-- 출석 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000109'}">
	                      						<td><input type="number" class="gradenum" name="attendScoreList" step="any" value="${fn:replace(stResult.attendScore, '.00', '')}"/></td>
	                      						<td><c:out value="${stResult.attYCnt}"/></td>
												<td><c:out value="${stResult.attLCnt}"/></td>
												<td><c:out value="${stResult.attNCnt}"/></td>
	                      					</c:when>
	                      					<%-- 과제 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000115'}">
	                      						<td><c:out value="${fn:replace(stResult.hChScr,'.0','')}"/></td>
	                      						<td><c:out value="${fn:replace(stResult.hTotScr,'.0','')}"/></td>
	                      					</c:when>
	                      					<%-- 수업참여도 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000117'}">
	                      						<td><c:out value="${stResult.bChScr}"/></td>
	                      						<td><c:out value="${stResult.bTotScr}"/></td>
	                      					</c:when>
	                      					<%-- 기타 --%>
	                      					<c:when test="${result.evtId eq 'CTG_0000000000000119'}">
	                      						<c:set var="etcCnt" value="${etcCnt + 1}"/>
	                      						<td>
	                      							<c:choose>
	                      								<c:when test="${etcCnt eq '1'}"><input type="number" class="gradenum" name="etcScoreList1" step="any" value="${fn:replace(stResult.etcScore1, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '2'}"><input type="number" class="gradenum" name="etcScoreList2" step="any" value="${fn:replace(stResult.etcScore2, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '3'}"><input type="number" class="gradenum" name="etcScoreList3" step="any" value="${fn:replace(stResult.etcScore3, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '4'}"><input type="number" class="gradenum" name="etcScoreList4" step="any" value="${fn:replace(stResult.etcScore4, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '5'}"><input type="number" class="gradenum" name="etcScoreList5" step="any" value="${fn:replace(stResult.etcScore5, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '6'}"><input type="number" class="gradenum" name="etcScoreList6" step="any" value="${fn:replace(stResult.etcScore6, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '7'}"><input type="number" class="gradenum" name="etcScoreList7" step="any" value="${fn:replace(stResult.etcScore7, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '8'}"><input type="number" class="gradenum" name="etcScoreList8" step="any" value="${fn:replace(stResult.etcScore8, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '9'}"><input type="number" class="gradenum" name="etcScoreList9" step="any" value="${fn:replace(stResult.etcScore9, '.00', '')}"/></c:when>
	                      								<c:when test="${etcCnt eq '10'}"><input type="number" class="gradenum" name="etcScoreList10" step="any" value="${fn:replace(stResult.etcScore10, '.00', '')}"/></c:when>
	                      							</c:choose>
	                      						</td>
	                      					</c:when>
	                      				</c:choose>
									</c:forEach>
	                      		</tr>
	                      	</c:forEach>
	                    </tbody>
	                </table>
			    </div>
			</div>
		</div>
    </form>
    
    <div class="btn_c">
        <a href="#none" id="btn-reg" class="btn_mngTxt">저장</a>
    </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>