<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<%--수강대상자 확정 승인 --%>
	<c:when test="${param.menuCode eq 'confm' }">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG_CONFIRM"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강대상자 확정 승인"/>
			<c:param name="menuCode" value="${param.confm }"/>
		</c:import>
	</c:when>
	<c:when test="${param.menuCode eq 'student' }">
       <%-- 과정등록관리 > 학생 --%>
    </c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="COURSEREG_MANAGE"/>
			<c:param name="depth1" value="CURSEREG"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="수강신청"/>
		</c:import>
	</c:otherwise>
</c:choose>

<script>
$(document).ready(function(){
	//반배정 취소
	$(document).on("click", ".group_cancle_btn", function(){
		location.href="/mng/lms/crm/curriculumGroupRegister.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}&classCnt=${param.classCnt}";
	});
});
</script>
<%-- 과정등록관리 > 학생에서 import할 때 상단 table 안나오도록 처리 --%>
<c:if test="${param.menuCode ne 'student' }">
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>
</c:if>

<c:if test="${empty param.menuCode}">
	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="4"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
	</c:import>
</c:if>

<c:import url="/mng/lms/curriculumStudenttabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="2"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<ul class="group_list">
	<c:forEach var="group" items="${selectGroupList}" varStatus="status">
		<c:choose>
			<c:when test="${group.groupCnt eq 0}"><c:set var="groupCntTab" value="조없음"/></c:when>
			<c:otherwise><c:set var="groupCntTab" value="${group.groupCnt}개조"/></c:otherwise>
		</c:choose>

		<li class="<c:if test="${group.classCnt eq param.classCnt }">act</c:if>">
			<a href="/mng/lms/crm/curriculumGroupView.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}&crclbId=${curriculumVO.crclbId}&classCnt=${group.classCnt }">${group.classCnt }반(${groupCntTab })</a>
		</li>

		<c:if test="${!status.last }">
			<li class="partition"> | </li>
		</c:if>
	</c:forEach>
</ul>

<div class="student_list" style="margin-left:0px;">
	<div class="student_pick_list">
		<form id="listForm" method="post" action="/mng/lms/crm/updateCurriculumGroup.do">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
			<input type="hidden" name="classCnt" value="${param.classCnt}"/>

			<table class="tbl_student_pick">
				<colgroup>
					<col width='80px' />
					<col width='80px' />
					<col width='*' />
				</colgroup>
				<tbody>
					<tr class="pick_table_header">
						<th>조</th>
						<th>학생수</th>
						<th>배정된 학생 명단</th>
					</tr>
					<c:forEach var="groupCnt" items="${groupCntList}" varStatus="status">
						<tr class="tr_student_list">
							<td>${groupCnt.groupCnt }조</td>
							<td>${groupCnt.userCnt }명</td>
							<td class="td_student_list">
								<c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">								
									<c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
										<div class="div_student_box">
											<c:if test="${pickStudent.groupLeaderAt eq 'Y' }">조장 : </c:if>
											${pickStudent.userNm }
										</div>
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</div>
<div class="btn_c">
	<c:choose>
	 	<c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
	 	</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<a href="#none" class="btn_mngTxt group_cancle_btn">조배정 취소</a>
		</c:when>
	</c:choose>

    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>