<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<script>
$(document).ready(function(){

});
</script>
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>

	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="1"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
	</c:import>
	
	<strong>*기본과정 </strong>
	<table class="chart2">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>년도</label></th>
				<td>
					<c:out value="${curriculumVO.crclYear }"/>
				</td>
				<th><label>학기</label></th>
				<td>
					<c:out value="${curriculumVO.crclTermNm}"/>
				</td>
				<th><label>언어</label></th>
				<td>
					<c:out value="${curriculumVO.crclLangNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>과정체계</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.sysCodeNmPath}"/>
				</td>
			</tr>
			<tr>
				<th><label>기본과정명</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.crclbNm}"/>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>이수구분</label></th>
				<td>
					<c:out value="${curriculumVO.divisionNm }"/>
				</td>
				<th><label>학점인정여부</label></th>
				<td>
					<c:choose>
						<c:when test="${curriculumVO.gradeAt eq 'Y'}">예</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
				<th><label>관리구분</label></th>
				<td colspan="3">
					<c:out value="${curriculumVO.controlNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>대상</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.targetType eq 'Y'}">본교생</c:when>
    					<c:otherwise>일반</c:otherwise>
    				</c:choose>
					> <c:out value="${curriculumVO.targetDetailNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>성적처리기준</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.evaluationAt eq 'Y'}">절대평가</c:when>
    					<c:otherwise>상대평가</c:otherwise>
    				</c:choose>
    				<c:if test="${not empty curriculumVO.gradeType}">
    					> <c:out value="${curriculumVO.gradeTypeNm}"/>
    				</c:if>
				</td>
			</tr>
			<tr>
				<th><label>운영보고서 유형</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.reportType eq 'NOR'}">일반형</c:when>
    					<c:when test="${curriculumVO.reportType eq 'PRJ'}">프로젝트형</c:when>
    				</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>과정만족도 설문 유형</label></th>
				<td colspan="5">
				    <c:forEach var="result" items="${surveyList}" varStatus="status">
					   <c:if test="${result.schdulId eq curriculumVO.surveySatisfyType}">${result.schdulNm}</c:if>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<th><label>개별 수업만족도조사 실시여부</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.surveyIndAt eq 'Y'}">예</c:when>
    					<c:otherwise>아니오</c:otherwise>
    				</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>학생 수강신청 제출서류 여부</label></th>
				<td colspan="5">
					<c:choose>
    					<c:when test="${curriculumVO.stdntAplyAt eq 'Y'}">예</c:when>
    					<c:otherwise>아니오</c:otherwise>
    				</c:choose>
				</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<strong>*교육과정 내용</strong>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>과정기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/>
				</td>
			</tr>
			<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
				<tr class="timeAt">
					<th><label>총 시간</label></th>
					<td><c:out value="${curriculumVO.totalTime}"/>시간</td>
					<th><label>주 횟수</label></th>
					<td><c:out value="${curriculumVO.weekNum}"/>회</td>
					<th><label>일 시간</label></th>
					<td><c:out value="${curriculumVO.dayTime}"/>시간</td>
				</tr>
			</c:if>
			<tr>
				<th><label>과정 진행 캠퍼스</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.campusNm}"/>
					(<c:out value="${curriculumVO.campusPlace}"/>)
				</td>
			</tr>
			<tr>
				<th><label>주관기관/학과</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.hostCodeNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>책임교원 </label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.userNm}"/>
				</td>
			</tr>
			<tr>
				<th><label>과정 개요 및 목표</label></th>
				<td colspan="5">
					<c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/>
				</td>
			</tr>
			<tr>
				<th><label>과정의 기대효과</label></th>
				<td colspan="5">
					<c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/>
				</td>
			</tr>
			
			<tr>
				<th><label>과정 계획서 작성기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.planStartDate}"/> ~ <c:out value="${curriculumVO.planEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>수강신청기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>등록기간</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.tuitionStartDate}"/> ~ <c:out value="${curriculumVO.tuitionEndDate}"/>
				</td>
			</tr>
			<tr>
				<th><label>수강정원</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
						<c:otherwise>
							<c:out value="${curriculumVO.applyMaxCnt}"/>명
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>주관학과 배정여부</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.assignAt eq 'Y'}">예</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>등록금 납부대상과정 여부</label></th>
				<td colspan="5">
					<c:choose>
						<c:when test="${curriculumVO.tuitionAt eq 'Y'}">
							예(
								<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">수업료</c:if>
								<c:if test="${not empty curriculumVO.registrationFees or curriculumVO.registrationFees != 0}">
									<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">,</c:if>
									등록비
								</c:if>
							)
						</c:when>
						<c:otherwise>아니오</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th><label>등록금액</label></th>
				<td colspan="5">
					수업료 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.tuitionFees}" />원
					등록비 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.registrationFees}" />원
				</td>
			</tr>
			
		</tbody>
	</table>
		
	<div class="btn_c">
	    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
	    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
	</div>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>