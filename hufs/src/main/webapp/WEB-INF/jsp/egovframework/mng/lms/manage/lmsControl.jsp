<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCodeDate}"/>
<c:choose>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
//달력
$(function() {
  $(".btn_calendar").datepicker({
    dateFormat: "yy-mm-dd"
  });
  
	//timepicker
	$("#resultStartTime, #resultEndTime, #surveyStartTime").timepicker({
		step: 1,          //시간간격 : 5분
		timeFormat: "H:i" //시간:분 으로표시
	});
	
	$("#btn-open-dialog,#dialog-background,#btn-close-dialog").click(function () {
		$("#my-dialog,#dialog-background").toggle();
	});
});

$(document).ready(function(){
	//과정확정취소
	$(".btn_cancel").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정을 취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//과정폐기
	$(".btn_del").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정개설취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//수강신청기간
	$(".btn_apl").click(function(){
		var href = $(this).attr("href"),
			applyStartDate = $("#applyStartDate").val(),
			applyEndDate = $("#applyEndDate").val(),
			type = $(this).data("type"),
			btn = $(this);
		
		if(type != 1){
			$.ajax({
				url : href
				, type : "post"
				, dataType : "json"
				, data : {applyStartDate : applyStartDate, applyEndDate : applyEndDate}
				, success : function(data){
					console.log(data);
					if(data.successYn == "Y"){
						$(".box_apl_text").text(applyStartDate + " ~ " + applyEndDate);
						$(".box_apl_inp").hide();
						$(".box_apl_text").show();
						
						btn.data("type", "1");
						btn.text("기간 수정");
					}
				}, error : function(){
					alert("error");
				}
			});
		}else{
			$(".box_apl_inp").show();
			$(".box_apl_text").hide();
			$(this).data("type", "0");
			$(this).text("기간 등록");
		}
		return false;
	});
	
	//성적발표
	$(".btn_result").click(function(){
		var href = $(this).attr("href"),
			resultStartDate = $("#resultStartDate").val(),
			resultStartTime = $("#resultStartTime").val(),
			resultEndDate = $("#resultEndDate").val(),
			resultEndTime = $("#resultEndTime").val(),
			type = $(this).data("type");
		if(type != 1){
			$.ajax({
				url : href
				, type : "post"
				, dataType : "json"
				, data : {resultStartDate : resultStartDate, resultStartTime : resultStartTime, resultEndDate : resultEndDate, resultEndTime : resultEndTime}
				, success : function(data){
					if(data.successYn == "Y"){
						$(".box_result_text").text(resultStartDate + " " + resultStartTime + " ~ " + resultEndDate + " " + resultEndTime);
						$(".box_result_inp").hide();
						$(".box_result_text").show();
						$(".btn_result").data("type", "1");
						$(".btn_result").text("기간 수정");
					}
				}, error : function(){
					alert("error");
				}
			});
		}else{
			$(".box_result_inp").show();
			$(".box_result_text").hide();
			$(this).data("type", "0");
			$(this).text("기간 등록");
		}
		return false;
	});
	
	//만족도 설문제출
	$(".btn_survey").click(function(){
		var href = $(this).attr("href"),
			surveyStartDate = $("#surveyStartDate").val(),
			surveyStartTime = $("#surveyStartTime").val(),
			type = $(this).data("type");
		
		
		if(type != 1){
			if(surveyStartDate && surveyStartTime){
				$.ajax({
					url : href
					, type : "post"
					, dataType : "json"
					, data : {surveyStartDate : surveyStartDate, surveyStartTime : surveyStartTime}
					, success : function(data){
						if(data.successYn == "Y"){
							$(".box_survey_text").text(surveyStartDate + " " + surveyStartTime + " ~ 과정상태가 과정종료(성적발표)까지 제출가능");
							$(".box_survey_inp").hide();
							$(".box_survey_text").show();
							$(".btn_survey").data("type", "1");
							$(".btn_survey").text("기간 수정");
						}
					}, error : function(){
						alert("error");
					}
				});
			}else{
				alert("시작일 및 시간을 확인해주세요.");
			}
		}else{
			$(".box_survey_inp").show();
			$(".box_survey_text").hide();
			$(this).data("type", "0");
			$(this).text("기간 등록");
		}
		return false;
	});
	
	//과정상태 선택
	$(".processSttusCode").click(function(){
		var desc = $(this).val(),
			expHtml = "";
		
		expHtml = "<strong>[ " + $(this).next().val() + " ]</strong>"
				+ "<br/>"
				+ $(this).next().next().val();
		
		$(".box_exp").html(expHtml);
	});
	
	//과정상태 변경
	$(".btn_change").click(function(){
		$("#prcsForm").submit();
	});
});
</script>
	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="1"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<strong>과정게시판</strong>&nbsp;&nbsp;<span>! 해당 과정에 필요한 만큼 과정게시판을 생성할 수 있습니다.</span>
	<c:url var="bbsUrl" value="/mng/lms/manage/lmsBbsControl.do">
		<c:param name="crclId" value="${curriculumVO.crclId}" />
	</c:url>
	<a href="${bbsUrl}" class="btn_addbbs btn_mngTxt fR">게시판 추가(수정)</a>
	<br/><br/>
	<table class="chart_board">
		<thead>
			<tr>
			  	<th>번호</th>
			  	<th>사용자그룹</th>
			 	<th>게시글 구분 태그</th>
				<th>게시판 명</th>
				<th>등록일</th>
		  	</tr> 
		</thead>
		<tbody>
			<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
				<tr>
					<td><c:out value="${status.count}"/></td>
					<td>
						<c:choose>
							<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
							<c:when test="${result.sysTyCode eq 'INDIV'}">개별</c:when>
							<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
							<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
						</c:choose>
					</td>
					<td>
						<select>
							<option>전체</option>
							
							<c:forEach var="ctgry" items="${ctgryList}" varStatus="sts">
								<c:if test="${ctgry.ctgrymasterId eq result.ctgrymasterId and not empty ctgry.upperCtgryId}">
									<option value=""><c:out value="${ctgry.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
					</td>
					<td><c:out value="${result.bbsNm}"/></td>
					<td><c:out value="${result.frstRegisterPnttm}"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br/><br/><br/><br/>
	<table class="chart2">
		<colgroup>
			<col width="20%"/>
			<col width="60%"/>
			<col width="20%"/>
		</colgroup>
		<tbody>
			<tr>
				<th>수강신청기간</th>
				<td>
					<div class="box_apl_inp" style="display:none;">
						<c:choose>
							<c:when test="${processSttusCode < 4}">
								<input type="text" id="applyStartDate" class="btn_calendar" value="<c:out value="${curriculumVO.applyStartDate}"/>" placeholder="시작일" readonly="readonly"/>
								~
								<input type="text" id="applyEndDate" class="btn_calendar" value="<c:out value="${curriculumVO.applyEndDate}"/>" placeholder="시작일" readonly="readonly"/>
							</c:when>
							<c:when test="${today < fn:replace(curriculumVO.applyEndDate,'-','') and today > fn:replace(curriculumVO.applyStartDate,'-','')}">
								<c:out value="${curriculumVO.applyStartDate}"/>
								<input type="hidden" id="applyStartDate" value="<c:out value="${curriculumVO.applyStartDate}"/>"/>
								~
								<input type="text" id="applyEndDate" class="btn_calendar" value="<c:out value="${curriculumVO.applyEndDate}"/>" placeholder="시작일" readonly="readonly"/>
							</c:when>
						</c:choose>
					</div>
					<div class="box_apl_text">
						<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
					</div>
				</td>
				<td>
					<c:choose>
						<c:when test="${processSttusCode > 3}">
							<c:url var="aplViewwUrl" value="/mng/lms/crm/curriculumStudent.do">
								<c:param name="crclId" value="${curriculumVO.crclId}" />
								<c:param name="crclbId" value="${curriculumVO.crclbId}" />
							</c:url>
							<a href="${aplViewwUrl}" class="btn_mngTxt">수강신청 조회</a>
						</c:when>
						<c:otherwise>
							<c:url var="uptUrl" value="/mng/lms/crcl/updateCurriculumPart.do">
								<c:param name="crclId" value="${curriculumVO.crclId}" />
							</c:url>
							<a href="${uptUrl}" class="btn_apl btn_mngTxt" data-type="1">기간 수정</a>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th>성적발표기간</th>
				<td>
					<div class="box_result_inp" <c:if test="${not empty curriculumVO.resultStartDate }">style="display:none;"</c:if>>
						<input type="text" id="resultStartDate" class="btn_calendar" value="<c:out value="${curriculumVO.resultStartDate}"/>" placeholder="시작일" readonly="readonly" autocomplete="off"/> 
						<input type="text" id="resultStartTime" class="wid80p alC readonly" value="<c:out value="${curriculumVO.resultStartTime}"/>" placeholder="시작 시간" autocomplete="off"/>
						~
						<input type="text" id="resultEndDate" class="btn_calendar" value="<c:out value="${curriculumVO.resultEndDate}"/>" placeholder="종료일"  readonly="readonly" autocomplete="off"/> 
						<input type="text" id="resultEndTime" class="wid80p alC readonly" value="<c:out value="${curriculumVO.resultEndTime}"/>" placeholder="종료 시간" autocomplete="off"/>
					</div>
					<div class="box_result_text" <c:if test="${empty curriculumVO.resultStartDate }">style="display:none;"</c:if>>
						<c:out value="${curriculumVO.resultStartDate}"/>
						<c:out value="${curriculumVO.resultStartTime}"/>
						~
						<c:out value="${curriculumVO.resultEndDate}"/>
						<c:out value="${curriculumVO.resultEndTime}"/>
					</div>
				</td>
				<td>
					<c:url var="resultUrl" value="/mng/lms/crcl/updateCurriculumPart.do">
						<c:param name="crclId" value="${curriculumVO.crclId}" />
					</c:url>
					<c:choose>
						<c:when test="${not empty curriculumVO.resultStartDate}">
							<a href="${resultUrl}" class="btn_result btn_mngTxt" data-type="1">기간 수정</a>
						</c:when>
						<c:otherwise>
							<a href="${resultUrl}" class="btn_result btn_mngTxt">기간 등록</a>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th>과정만족도 설문제출 시작일</th>
				<td>
					<div class="box_survey_inp" <c:if test="${not empty curriculumVO.surveyStartDate }">style="display:none;"</c:if>>
						<input type="text" id="surveyStartDate" class="btn_calendar" value="<c:out value="${curriculumVO.surveyStartDate}"/>" placeholder="시작일" readonly="readonly" autocomplete="off"/> 
						<input type="text" id="surveyStartTime" class="wid80p alC readonly" value="<c:out value="${curriculumVO.surveyStartTime}"/>" placeholder="시작 시간" autocomplete="off"/>
						~
						과정상태가 과정종료(성적발표)까지 제출가능
					</div>
					<div class="box_survey_text" <c:if test="${empty curriculumVO.surveyStartDate }">style="display:none;"</c:if>>
						<c:out value="${curriculumVO.surveyStartDate}"/>
						<c:out value="${curriculumVO.surveyStartTime}"/>
						~
						과정상태가 과정종료(성적발표)까지 제출가능
					</div>
				</td>
				<td>
					<c:url var="resultUrl" value="/mng/lms/crcl/updateCurriculumPart.do">
						<c:param name="crclId" value="${curriculumVO.crclId}" />
					</c:url>
					<c:choose>
						<c:when test="${not empty curriculumVO.surveyStartDate}">
							<a href="${resultUrl}" class="btn_survey btn_mngTxt" data-type="1">기간 수정</a>
						</c:when>
						<c:otherwise>
							<a href="${resultUrl}" class="btn_survey btn_mngTxt">기간 등록</a>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<th>과정상태</th>
				<td>
					<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
    					<c:if test="${statusCode.code eq curriculumVO.processSttusCode}">
    						<c:out value="${statusCode.codeNm}"/>
    						<c:set var="codeNm" value="${statusCode.codeNm}"/>
							<c:set var="codeDc" value="${statusCode.codeDc}"/>
    					</c:if>
					</c:forEach>
				</td>
				
				<c:choose>
					<c:when test="${processSttusCode eq '1'}">
						<c:url var="cancelUrl" value="/mng/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}">
							<c:param name="crclId" value="${curriculumVO.crclId}" />
							<c:param name="processSttusCode" value="0" />
						</c:url>
						<td><a href="${cancelUrl}" class="btn_cancel btn_mngTxt">과정 확정 취소</a></td>
					</c:when>
					<c:otherwise>
						<td><a href="#" id="btn-open-dialog" class="btn_mngTxt">과정 상태 변경</a></td>
					</c:otherwise>
				</c:choose>
			</tr>
		</tbody>
	</table>
	<br/><br/><br/><br/>
	

<div id="my-dialog">
	<div class="modal_header">과정 상태 변경</div>
	<div class="modal_body">
		<h3 class="status_cur">현재 <span class="sts_code"><c:out value="${codeNm}"/></span> 상태입니다.</h3>
		<form id="prcsForm" action="/mng/lms/crcl/updatePsCodeCurriculum.do" method="post">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<ul class="box_process">
				<c:forEach var="result" items="${statusComCode}">
					<li>
						<label>
							<input type="radio" class="processSttusCode" name="processSttusCode" value="<c:out value="${result.code}"/>" <c:if test="${result.code eq curriculumVO.processSttusCode}">checked="checked"</c:if>/> <c:out value="${result.codeNm}"/>
							<input type="hidden" class="codeNm" name="codeNm" value="<c:out value="${result.codeNm}"/>"/>
							<input type="hidden" class="codeDc" name="codeDc" value="<c:out value="${result.codeDc}"/>"/>
						</label>
					</li>
				</c:forEach>
			</ul>
		</form>
		<div class="box_exp">
			<strong>[ <c:out value="${codeNm}"/> ]</strong>
			<br/>
			<c:out value="${codeDc}"/>
		</div>
		<div class="box_btn">
			<a href="#" class="btn_change btn_mngTxt">변경</a>
			<button id="btn-close-dialog"><img src="/template/manage/images/btn/del.gif"></button>
		</div>
	</div>
</div>
<div id="dialog-background"></div>


<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>