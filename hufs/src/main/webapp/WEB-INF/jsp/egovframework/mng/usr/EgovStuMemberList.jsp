<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchPositionCode}"><c:param name="searchPositionCode" value="${searchVO.searchPositionCode}" /></c:if>
		<c:if test="${!empty searchVO.searchDept}"><c:param name="searchDept" value="${searchVO.searchDept}" /></c:if>
		<c:if test="${!empty searchVO.searchGender}"><c:param name="searchGender" value="${searchVO.searchGender}" /></c:if>
		<c:if test="${!empty searchVO.searchGroup}"><c:param name="searchGroup" value="${searchVO.searchGroup}" /></c:if>
		<c:if test="${!empty searchVO.searchWork}"><c:param name="searchWork" value="${searchVO.searchWork}" /></c:if>
		<c:if test="${!empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
		<c:if test="${!empty searchVO.templateAt}"><c:param name="templateAt" value="${searchVO.templateAt}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
<c:set var="_ACTION" value="/mng/usr/EgovStuMberManage.do" />

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>학생검색</title>

<script>
<c:if test="${not empty requestMessage}">
alert("${requestMessage}");
if("수강 신청이 완료되었습니다." == "${requestMessage}"){
	opener.parent.location.reload();
	window.close();
}
</c:if>

$(document).ready(function(){
	
	// SEARCH 버튼 클릭 시 
	$("#search_cheeck").click(function(){
		
		var reg_phone =/^[0-9]+$/;
		
		var phone =$("#inp_phone").val();
		
		if(reg_phone.test(phone) === false && phone !=''){
			alert("전화번호는 숫자만 입력 가능합니다.");
			return false;
		}
		
	});
	
	//학생 선택
	$("#btn-ok").click(function(){
		var leng = $(".chk:checked").length,
			id = "",
			name = "",
			mngdeptnm = "",
			brthdy = "",
			stNumber = "",
		  	stGrade = "";
		
		if(leng == 0){
			alert("학생을 선택해주세요.");
		}else{
			$(".chk").each(function(){
				
				if($(this).is(":checked") == true){
					id = $(this).val(),
					name = $(this).data("name"),
					mngdeptnm = $(this).data("mngdeptnm"),
					brthdy = $(this).data("brthdy"),
					stNumber = $(this).data("stnumber"),
				  	stGrade = $(this).data("stgrade");
					
					$("#userId").val(id);
					$(".userId").text(id);
					$(".mngDeptNm").text(mngdeptnm);
					$(".userNm").text(name);
					$(".brthdy").text(brthdy);
					$(".stNumber").text(stNumber);
					$(".stGrade").text(stGrade);
					$(".findStu").hide();
					$(".box_add").show();
				}
				
				
			});
		}
		
/* 		if(leng == 0){
			alert("학생을 선택해주세요.");
		}else{
			$(".chk").each(function(){
				id = $(this).val(),
				name = $(this).data("name"),
				mngdeptnm = $(this).data("mngdeptnm"),
				brthdy = $(this).data("brthdy"),
				stNumber = $(this).data("stnumber"),
			  	stGrade = $(this).data("stgrade");
				
				$("#userId").val(id);
				$(".userId").text(id);
				$(".mngDeptNm").text(mngdeptnm);
				$(".userNm").text(name);
				$(".brthdy").text(brthdy);
				$(".stNumber").text(stNumber);
				$(".stGrade").text(stGrade);
				
				$(".findStu").hide();
				$(".box_add").show();
			});
		} */
		
		return false;
	});
	
	//팝업닫기
	$(".btn-close").click(function(){
		window.close();
		return false;
	});
	
	//조
	$(".groupCnt").keyup(function(){
		if(!$(this).val()){
			$(".btn-add").text("조배정 없이 수강대상자 등록");
		}else{
			$(".btn-add").text($(this).val() + "조로 배정하여 수강대상자 등록");
		}
	});
	
	//등록
	$(".btn-add").click(function(){
		if(confirm($(".userId").text() + "(" + $(".userNm").text() + ") 학생을 최종 등록하시겠습니까?")){
			$("#frm").submit();
		}
	});
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
	<div class="findStu">
		<form name="listForm" id="listForm" method="post" method="post" action="${_ACTION }">
			<div id="bbs_search">
				<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
				<label for="inp_text" class="hdn">검색어입력</label>
				<div>
					<label>이름 :</label> <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" class="inp_s" id="inp_name" placeholder="이름을 입력해주세요.">
				</div>
				<div>
					<label>전화번호 :</label><input type="text" name="searchMoblphonNo" value="${searchVO.searchMoblphonNo}" class="inp_s" id="inp_phone" placeholder="-를 제외하고 입력해주세요.(예:01012345678)">
				</div>
				<div>
					<label>아이디 :</label> <input type="text" name="searchUserId" value="${searchVO.searchUserId}" class="inp_s" id="inp_text" placeholder="아이디를 입력해 주세요.">
				</div>
				<input type="image" src="/template/manage/images/btn/btn_search.gif" alt="검색" id="search_cheeck">
		  	</div>
			<table class="chart_board">
				<colgroup>
					<col width="70"/>				
					<col width="120"/>
					<col width="70"/>
				</colgroup>
				<thead>
					<tr>
						<th>선택</th>
						<th>ID</th>
						<th>소속</th>
						<th>이름</th>
						<th>생년월일</th>
						<th>학번</th>
						<th>학년</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<td class="listtd"><input type="radio" name="nameChk" class="chk" value="${result.userId}" data-name="${result.userNm}" data-mngdeptnm="${result.mngDeptNm}" data-brthdy="${result.brthdy}" data-stnumber="${result.stNumber}" data-stgrade="${result.stGrade}"/></td> 
						<td class="listtd"><c:out value="${result.userId}" /></td>
						<td class="listtd"><c:out value="${result.mngDeptNm}" /></td>
						<td class="listtd"><c:out value="${result.userNm}" /></td>
						<td class="listtd"><c:out value="${result.brthdy}" /></td>
						<td class="listtd"><c:out value="${result.stNumber}" /></td>
						<td class="listtd"><c:out value="${result.stGrade}" /></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(resultList) == 0}">
			      <tr>
			      	<td class="listtd" colspan="10">
			        	<spring:message code="common.nodata.msg" />
			        </td>
			      </tr>
			    </c:if>
			</tbody>
			</table>
			
		    <div class="btn_c">
		    	<a href="#" class="btn_mngTxt btn-close">닫기</a>
		    	&nbsp;&nbsp;
				<a href="#" id="btn-ok" class="btn_mngTxt">선택</a>
			</div>
		</form>
	</div>
	
	<div class="box_add" style="display:none;">
		<form name="frm" id="frm" method="post" action="/mng/lms/crm/insertCurriculumMember.do" enctype="multipart/form-data">
			<input type="hidden" id="userId" name="userId"/>
			<input type="hidden" id="crclId" name="crclId" value="${searchVO.crclId}"/>
			<input type="hidden" name="stdntAplyAt" value="Y"/>
			
			<table class="chart_board">
				<thead>
					<tr>
						<th>ID</th>
						<th>소속</th>
						<th>이름</th>
						<th>생년월일</th>
						<th>학번</th>
						<th>학년</th>
						<th>조배정</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="userId"></td>
						<td class="mngDeptNm"></td>
						<td class="userNm"></td>
						<td class="brthdy"></td>
						<td class="stNumber"></td>
						<td class="stGrade"></td>
						<td><input type="text" class="groupCnt" name="groupCnt"/>조</td>
					</tr>
				</tbody>
			</table>
			
			<table class="chart_board" style="margin-top:50px;">
				<colgroup>
					<col width="70"/>				
					<col width="*"/>
					<col width="100"/>
				</colgroup>
				<thead>
					<tr>
						<th>구분</th>
						<th>파일명</th>
						<th>파일선택</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>신청서</td>
						<td><input type="text" title="첨부파일등록" id="file_route_1" readonly="readonly" style="width:90%;"></td>
						<td><input id="fileupload_1" class="fileupload" type="file" name="aplyFileInput" onchange="javascript:document.getElementById('file_route_1').value=this.value"></td>
					</tr>
					<tr>
						<td>계획서</td>
						<td><input type="text" title="첨부파일등록" id="file_route_2" readonly="readonly" style="width:90%;"></td>
						<td><input id="fileupload_2" class="fileupload" type="file" name="planFileInput" onchange="javascript:document.getElementById('file_route_2').value=this.value"></td>
					</tr>
					<tr>
						<td>기타</td>
						<td><input type="text" title="첨부파일등록" id="file_route_3" readonly="readonly" style="width:90%;"></td>
						<td><input id="fileupload_3" class="fileupload" type="file" name="etfFileInput" onchange="javascript:document.getElementById('file_route_3').value=this.value"></td>
					</tr>
				</tbody>
			</table>
			<%-- 
			<table class="modal-table-wrap size-sm file-table">
		                <colgroup>
		                  <col width="20%" class="bg-light-blue">
		                  <col width="*">
		                </colgroup>
		                <tbody>
		                  <tr>
		                    <td scope="row" class="table-title font-700">신청서</td>
		                    <td class="ell">
		                      <input type="text" class="tit_wid_330" title="첨부파일등록" id="file_route_1" readonly="readonly">
		                      <a href="#" class="btn_gray file_btn"><input id="fileupload_1" class="fileupload" type="file" name="aplyFileInput" onchange="javascript:document.getElementById('file_route_1').value=this.value">파일선택</a>
		                    </td>
		                  </tr>
		                  <tr>
		                    <td scope="row" class="table-title font-700">계획서</td>
		                    <td class="ell">
		                      <input type="text" class="tit_wid_330" title="첨부파일등록" id="file_route_2" readonly="readonly">
		                      <a href="#" class="btn_gray file_btn"><input id="fileupload_2" class="fileupload" type="file" name="planFileInput" onchange="javascript:document.getElementById('file_route_2').value=this.value">파일선택</a>
		                    </td>
		                  </tr>
		                  <tr>
		                    <td scope="row" class="table-title font-700">기타</td>
		                    <td class="ell">
		                      <input type="text" class="tit_wid_330" title="첨부파일등록" id="file_route_3" readonly="readonly">
		                      <a href="#" class="btn_gray file_btn"><input id="fileupload_3" class="fileupload" type="file" name="etfFileInput" onchange="javascript:document.getElementById('file_route_3').value=this.value">파일선택</a>
		                    </td>
		                  </tr>
		                </tbody>
		              </table>
			 --%>
			
			<div class="btn_c">
		    	<a href="#" class="btn_mngTxt btn-close">닫기</a>
		    	&nbsp;&nbsp;
				<a href="#" class="btn_mngTxt btn-add">조배정 없이 수강대상자 등록</a>
			</div>
		</form>
	</div>

</div>

