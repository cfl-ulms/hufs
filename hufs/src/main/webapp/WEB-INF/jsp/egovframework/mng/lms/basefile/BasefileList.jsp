<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<c:choose>
<c:when test="${param.tmplatImportAt ne 'N'}">
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_FILE"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="양식 관리"/>
</c:import>
<script>
$(document).ready(function(){
	$("#btn-agree").click(function(){
		window.open('/msi/terms.do?popupAt=Y','_blank','toolbar=yes,scrollbars=yes,resizable=yes,left=495, top=58.5, width=1024, height=700');
	});
});
</script>
<div id="cntnts">
<form name="listForm" id="listForm" method="post">
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
	    	<col class="co6"/>
			<col class="*"/>
			<col class="co6"/>
			<col class="co6"/>
			<col class="co6"/>
		</colgroup>
	    <caption class="hdn">양식파일업로드</caption>
	    <thead>
	      <tr>
	        <th>1Depth</th>
	        <th>2Depth</th>
	        <th>양식지명</th>
	        <th>구분</th>
	        <th>등록일</th>
	        <th>관리</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:set var="depthNm" value="${fn:split(result.ctgryPathByName,'>')}"/>
				<tr>
	 				<td><c:out value="${depthNm[1]}"/></td>
					<td><c:out value="${depthNm[2]}"/></td>
					<td>
						<a href="/cmm/fms/FileDown.do?atchFileId=${result.atchFileId}&amp;fileSn=0">
							<img src="/template/manage/images/ico_file.gif" alt="파일">
							<c:out value="${result.orignlFileNm}"/>
						</a>
					</td>
					<c:set var="type">
						<c:choose>
							<c:when test="${result.fileExtsn eq 'pdf'}">PDF</c:when>
							<c:when test="${result.fileExtsn eq 'hwp'}">한글</c:when>
							<c:when test="${result.fileExtsn eq 'bmp' or result.fileExtsn eq 'gif' or result.fileExtsn eq 'jpeg' or result.fileExtsn eq 'jpg' or result.fileExtsn eq 'png'}">사진</c:when>
							<c:when test="${result.fileExtsn eq 'xls' or result.fileExtsn eq 'xlsx'}">엑셀</c:when>
							<c:when test="${result.fileExtsn eq 'ppt' or result.fileExtsn eq 'pptx'}">파워포인트</c:when>
							<c:when test="${result.fileExtsn eq 'doc' or result.fileExtsn eq 'docx'}">워드</c:when>
							<c:otherwise>${fn:toUpperCase(result.fileExtsn)}</c:otherwise>
						</c:choose>
					</c:set>
					<td><c:out value="${type}"/></td>
					<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
					<c:url var="delUrl" value="/mng/lms/basefile/deleteBasefile.do">
						<c:param name="atchFileId" value="${result.atchFileId}"/>
					</c:url>
					<td><a href="${delUrl}"><img src="${_IMG}/btn/del.gif"/></a></td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    <a href="#" id="btn-agree" class="btn_mngTxt fL mt20">수강신청 시 약관 및 개인정보취급방침 동의 안내화면</a>
    <div class="btn_r">
    	<c:url var="addUrl" value="/mng/lms/basefile/addBasefileView.do"/>	
		<a href="${addUrl}"><img src="${_IMG}/btn/btn_creat.gif" alt="생성"/></a>
	</div>
</form>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>

</c:when>
<c:otherwise>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>양식자료 선택</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var target = $(this).data("target"),
			id = "",
			name = "",
			idList = [],
			nameList = [];
		
		if(target == "refe"){
			$(".chk").each(function(){
				if($(this).is(":checked")){
					idList.push($(this).val());
					nameList.push($(this).data("nm"));
				}
			});
			
			window.opener.file(idList, nameList, target);
			window.close();
		}else{
			if($(".chk:checked").length > 1){
				alert("파일을 한개만 선택해 주세요.");
			}else{
				$(".chk").each(function(){
					if($(this).is(":checked")){
						id = $(this).val();
						name = $(this).data("nm");
					}
				});
				
				window.opener.file(id, name, target);
				window.close();
			}
		}
		
		return false;
	});
	
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
<form name="listForm" id="listForm" method="post" method="post" action="/mng/lms/basefile/BasefileList.do?tmplatImportAt=N">
	<input type="hidden" name="searchTarget"/>
	<div id="bbs_search">
		<label for="inp_text" class="hdn">검색어입력</label>
		<input type="text" name="searchKeyword" value="${searchVO.searchKeyword}" class="inp_s" id="inp_text" placeholder="검색어를 입력해 주세요.">
		<input type="image" src="/template/manage/images/btn/btn_search.gif" alt="검색">
  	</div>
	<table class="chart_board">
	    <colgroup>
	    	<col class="co6"/>
			<col class="*"/>
			<col class="co6"/>
		</colgroup>
	    <thead>
	      <tr>
	        <th>선택</th>
	        <th>양식지명</th>
	        <th>구분</th>
	      </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="result" items="${resultList}" varStatus="status">
	    		<c:set var="depthNm" value="${fn:split(result.ctgryPathByName,'>')}"/>
				<tr>
	 				<td><input type="checkbox" class="chk" value="${result.atchFileId}" data-nm="<c:out value="${result.orignlFileNm}"/>"/></td>
					<td>
						<a href="/cmm/fms/FileDown.do?atchFileId=${result.atchFileId}&amp;fileSn=0">
							<img src="/template/manage/images/ico_file.gif" alt="파일">
							<c:out value="${result.orignlFileNm}"/>
						</a>
					</td>
					<c:set var="type">
						<c:choose>
							<c:when test="${result.fileExtsn eq 'pdf'}">PDF</c:when>
							<c:when test="${result.fileExtsn eq 'hwp'}">한글</c:when>
							<c:when test="${result.fileExtsn eq 'bmp' or result.fileExtsn eq 'gif' or result.fileExtsn eq 'jpeg' or result.fileExtsn eq 'jpg' or result.fileExtsn eq 'png'}">사진</c:when>
							<c:when test="${result.fileExtsn eq 'xls' or result.fileExtsn eq 'xlsx'}">엑셀</c:when>
							<c:when test="${result.fileExtsn eq 'ppt' or result.fileExtsn eq 'pptx'}">파워포인트</c:when>
							<c:when test="${result.fileExtsn eq 'doc' or result.fileExtsn eq 'docx'}">워드</c:when>
							<c:otherwise>${fn:toUpperCase(result.fileExtsn)}</c:otherwise>
						</c:choose>
					</c:set>
					<td><c:out value="${type}"/></td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
    <div class="btn_c">
		<a href="#" class="btn_ok" data-target="${param.target}"><img src="/template/manage/images/board/btn_confirm.gif"/></a>
	</div>
</form>
</div>
</c:otherwise>
</c:choose>

