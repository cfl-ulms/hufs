<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<%-- <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclNm" value="${searchVO.searchCrclYear}" /></c:if> --%>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG_LIST"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청 승인/취소 내역"/>
</c:import>

<script>
$(document).ready(function(){
});
</script>

<div id="cntnts">
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>

	<div id="bbs_search">
	  	<form name="frm" method="post" action="<c:url value="/mng/lms/crm/curseregSttusView.do"/>">
	  		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
	  		<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>

			<input type="text" name="searchHostCode" value="${searchVO.searchHostCode }" class="inp_s" id="inp_text" placeholder="소속">
			
			<select id="searchSttus" name="searchSttus">
	  			<option value="">승인 상태 전체</option>
	  			<option value="1" <c:if test="${'1' eq searchVO.searchSttus}">selected="selected"</c:if>>승인</option>
	  			<option value="2" <c:if test="${'2' eq searchVO.searchSttus}">selected="selected"</c:if>>대기</option>
	  			<option value="3" <c:if test="${'3' eq searchVO.searchSttus}">selected="selected"</c:if>>취소</option>
	  			<option value="4" <c:if test="${'4' eq searchVO.searchSttus}">selected="selected"</c:if>>환불</option>
	  			<option value="5" <c:if test="${'5' eq searchVO.searchSttus}">selected="selected"</c:if>>승인/대기</option>
	  			<option value="6" <c:if test="${'6' eq searchVO.searchSttus}">selected="selected"</c:if>>취소/환불</option>
			</select>

			<input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy }" class="inp_s onlyNum" id="inp_text" placeholder="생년월일 8자리 숫자만입력">
			
			<input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" class="inp_s" id="inp_text" placeholder="학생 이름">
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
		</form>
	</div>
	
	<div style="text-align:right;margin-bottom:10px;">
		<span style="float:left;font-weight:bold;">
			<c:forEach var="memberSttusCnt" items="${memberSttusCntList}" varStatus="status">
				<c:choose>
					<c:when test="${param.searchSttus eq 1 }">승인 : ${memberSttusCnt.acceptCnt }</c:when>
					<c:when test="${param.searchSttus eq 2 }">대기 : ${memberSttusCnt.readyCnt }</c:when>
					<c:when test="${param.searchSttus eq 3 }">취소 : ${memberSttusCnt.cancleCnt }</c:when>
					<c:when test="${param.searchSttus eq 4 }">환불 : ${memberSttusCnt.refundCnt }</c:when>
					<c:when test="${param.searchSttus eq 5 }">승인 : ${memberSttusCnt.acceptCnt } | 대기 : ${memberSttusCnt.readyCnt }</c:when>
					<c:when test="${param.searchSttus eq 6 }">취소 : ${memberSttusCnt.cancleCnt } | 환불 : ${memberSttusCnt.refundCnt }</c:when>
					<c:otherwise>총 : ${memberSttusCnt.totalCnt }</c:otherwise>
				</c:choose>
			</c:forEach>
		</span>
		<a href="/mng/lms/crm/curriculumAccept.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}" class="btn_mngTxt">수강신청 메뉴에서 승인상태 변경 / 조회 </a>
	</div>
	
	<table class="chart_board mb50">
	    <colgroup>
	    	<col width="50px"/>
	    </colgroup>
	    <thead>
	        <tr>
	      	    <th>no</th>
	      	    <th>소속</th>
	      	    <th>이름</th>
	      	    <th>생년월일</th>
	      	    <th>학번</th>
	      	    <th>학년</th>
	      	    <th>수업료</th>
	      	    <th>등록비</th>
	      	    <th>수강신청 접수일</th>
	      	    <th>승인상태</th>
	        </tr> 
	    </thead>
	    <tbody>
	    	<c:forEach var="member" items="${curriculumMemberList}" varStatus="status">
				<tr class="alC">
					<td class="line alC">${totCnt - status.index }</td>
					<td class="line alC">${member.mngDeptNm }</td>
					<td class="line alC">${member.userNm }</td>
					<td class="line alC">${member.brthdy }</td>
					<td class="line alC">${member.stNumber }</td>
					<td class="line alC">${member.stGrade }</td>
					<td class="line alC">
					   <c:choose>
                            <c:when test="${curriculumVO.tuitionFees ne 0}">
                                <fmt:formatNumber type="number" maxFractionDigits="3" value="${member.tuitionFees}" />원
                            </c:when>
                            <c:otherwise>무료</c:otherwise>
                        </c:choose>
					</td>
					<td class="line alC">
					    <c:choose>
                            <c:when test="${curriculumVO.registrationFees ne 0}">
                                <fmt:formatNumber type="number" maxFractionDigits="3" value="${member.registrationFees}" />원
                            </c:when>
                            <c:otherwise>없음</c:otherwise>
                        </c:choose>
					</td>
					<td class="line alC">${member.frstRegisterPnttm }</td>
					<td class="line alC">
						<c:choose>
							<c:when test="${member.sttus eq 1}">승인</c:when>
							<c:when test="${member.sttus eq 2}">대기</c:when>
							<c:when test="${member.sttus eq 3}">취소</c:when>
							<c:when test="${member.sttus eq 4}">환불</c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(curriculumMemberList) == 0}">
				<tr>
		        	<td class="listCenter" colspan="10"><spring:message code="common.nodata.msg" /></td>
		      	</tr>
		    </c:if>
		</tbody>    
    </table>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>