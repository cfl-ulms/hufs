<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="crclId" value="${searchVO.crclId}" />
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CLASS_MANAGE"/>
    <c:param name="depth1" value="${param.depth1 }"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value=""/>
</c:import>

<script>
$(document).ready(function(){
	//저장
    $("#btn_save").click(function(){
        var leng = $(".atdType:checked").length - 1;

        $("#atdTypeBox").html("");
        $(".atdType:checked").each(function(i){
            $("#atdTypeBox").append("<input type='hidden' name='attentionTypeList' value='" + $(this).val() + "'/>");

            if(leng == i){
                $("#detailForm").submit();
            }
        });

        return false;
    });
});
</script>
    <table class="chart2">
        <tr>
            <td class="alC">
                <c:out value="${curriculumVO.crclNm}"/>
                <p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
            </td>
        </tr>
    </table>
    <br/>
    <c:if test="${curriculumVO.processSttusCode > 0}">
        <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
            <c:param name="step" value="6"/>
            <c:param name="menu" value="CLASS_MANAGE"/>
            <c:param name="crclId" value="${curriculumVO.crclId}"/>
            <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
            <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        </c:import>
    </c:if>
    <br/>

    <form name="detailForm" id="detailForm" method="post" action="/mng/lms/atd/attendUpt.do">
        <input type="hidden" name="plId" value="${searchVO.plId}"/>
        <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>

	    <table class="chart_board">
	        <colgroup>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
            </colgroup>

	        <thead>
	          <tr>
	            <th>번호</th>
	            <th>소속</th>
	            <th>이름</th>
	            <th>생년월일</th>
	            <th>학번</th>
	            <th>학년</th>
	            <th>조</th>
	            <th>출석</th>
	            <th>지각</th>
	            <th>결석</th>
	          </tr>
	        </thead>
	        <tbody>
	            <c:forEach var="result" items="${resultList}" varStatus="status">
	                <tr class="">
	                  <td scope='row'><c:out value="${status.count}"/></td>
	                  <td>${result.mngDeptNm}</td>
	                  <td>
	                      ${result.userNm}
	                      <input type="hidden" name="userIdList" value="${result.userId}"/>
	                  </td>
	                  <td>${result.brthdy}</td>
	                  <td>${result.stNumber}</td>
	                  <td>${result.stGrade}</td>
	                  <td>${result.classCnt}</td>
	                  <td>
	                      <input type='radio' name='name${status.count}' class="atdType" value="Y" <c:if test="${result.attentionType eq 'Y'}">checked</c:if>>
                      </td>
	                  <td>
	                      <input type='radio' name='name${status.count}' class="atdType" value="L" <c:if test="${result.attentionType eq 'L'}">checked</c:if>>
                      </td>
	                  <td>
	                      <input type='radio' name='name${status.count}' class="atdType" value="N" <c:if test="${result.attentionType eq 'N'}">checked</c:if>>
                      </td>
	                </tr>
	                <c:choose>
                      <c:when test="${result.attentionType eq 'Y'}">
                          <c:set var="atdY" value="${atdY + 1}"/>
                      </c:when>
                      <c:when test="${result.attentionType eq 'L'}">
                          <c:set var="atdL" value="${atdL + 1}"/>
                      </c:when>
                      <c:when test="${result.attentionType eq 'N'}">
                          <c:set var="atdN" value="${atdN + 1}"/>
                      </c:when>
                    </c:choose>
	            </c:forEach>
	            <tr class="">
                  <td scope='row' class='' colspan='7'>총 <c:out value="${fn:length(resultList)}"/>명</td>
                  <td scope='row' class=''>
                    <div class='input-group'><input type='number' class='table-input' value='${atdY}' readonly="readonly"><span class='unit font-gray-light'>명</span></div>
                  </td>
                  <td scope='row' class=''>
                    <div class='input-group'><input type='number' class='table-input' value='${atdL}' readonly="readonly"><span class='unit font-gray-light'>명</span></div>
                  </td>
                  <td scope='row' class=''>
                    <div class='input-group'><input type='number' class='table-input' value='${atdN}' readonly="readonly"><span class='unit font-gray-light'>명</span></div>
                  </td>
                </tr>
	          </tbody>
	    </table>
	    <div id="atdTypeBox"></div>
	</form>

    <div class="btn_c">
        <a href="#none" id="btn_save"><img src="/template/manage/images/btn/btn_regist.gif"></a>
    </div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>