<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<%--과정체계코드 --%>
<c:set var="sysCode" value="${fn:split(curriculumVO.sysCodePath,'>')}"/>
<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${empty searchVO.crclId}">
		<c:set var="_MODE" value="REG"/>
		<c:set var="_ACTION" value="${_PREFIX}/addCurriculum.do${_BASE_PARAM }"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
		<c:set var="_ACTION" value="${_PREFIX}/updateCurriculum.do${_BASE_PARAM}"/>
	</c:otherwise>
</c:choose>
<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
//달력
$(function() {
  $("#startDate, #endDate, #planStartDate, #planEndDate, #applyStartDate, #applyEndDate, #tuitionEndDate, #tuitionStartDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	//초기화 시작
	<c:if test="${curriculumVO.applyMaxCnt == 0}">
		$("#applyMaxCnt").prop("readonly", true);
	</c:if>
	
	if($("input[name=tuitionAt]:checked").val() == "Y"){
		$("input[name=tuitionFeesAt]").prop("disabled", false);
		$("input[name=registrationFeesAt]").prop("disabled", false);
	}else{
		$("input[name=tuitionFeesAt]").prop("checked", false);
		$("input[name=tuitionFeesAt]").prop("disabled", true);
		$("input[name=registrationFeesAt]").prop("checked", false);
		$("input[name=registrationFeesAt]").prop("disabled", true);
		$("#tuitionFees").prop("readonly", true);
		$("#registrationFees").prop("readonly", true);
	}
	
	sisuTotCnt();
	facTotCnt();
	//초기화 끝
	
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${sysCode[2]}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
		autoTxt();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${sysCode[3]}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
		$("#ctgryId3").change();
		autoTxt();
	});
	
	$("#ctgryId3").change(function(){
		var sysCode3 = $(this).val(),
			optionHtml = "<option value=''>기본과정명</option>";
		
		$.ajax({
			url : "/mng/lms/crclb/CurriculumbaseList.json"
			, type : "post"
			, dataType : "json"
			, data : {searchSysCode3 : sysCode3}
			, success : function(data){
				var searchcrclbId = "${curriculumVO.crclbId}";
				if(data.successYn == "Y"){
					$.each(data.items, function(i){
						if(data.items[i].crclbId == searchcrclbId){
							optionHtml += "<option value='"+data.items[i].crclbId+"' selected='selected'>"+data.items[i].crclbNm+"</option>"
						}else{
							optionHtml += "<option value='"+data.items[i].crclbId+"'>"+data.items[i].crclbNm+"</option>"
						}
						
					});
					
					$("#crclbId").html(optionHtml);
				}
			}, error : function(){
				alert("error");
			}
		});
	});
	
	$("#ctgryId1").change();
	
	//기본과정 선택
	$("#crclbId").change(function(){
		var id = $(this).val();
		
		selectcrclb(id);
	});
	
	//과정명 Auto
	$(".autoTxt").change(function(){
		autoTxt();
	});
	
	//수강정원
	$("input[name=applyMaxCntAt]").click(function(){
		if("Y" == $(this).val()){
			$("#applyMaxCnt").prop("readonly", false);
		}else{
			$("#applyMaxCnt").prop("readonly", true);
			$("#applyMaxCnt").val("0");
		}
	});
	
	//등록금
	$("input[name=tuitionAt]").click(function(){
		if("Y" == $(this).val()){
			$("input[name=tuitionFeesAt]").prop("disabled", false);
			$("input[name=registrationFeesAt]").prop("disabled", false);
		}else{
			if(confirm("등록 금액과 기간이 삭제됩니다.")){
				$("input[name=tuitionFeesAt]").prop("checked", false);
				$("input[name=tuitionFeesAt]").prop("disabled", true);
				$("input[name=registrationFeesAt]").prop("checked", false);
				$("input[name=registrationFeesAt]").prop("disabled", true);
				$("#tuitionFees").val('').prop("readonly", true);
				$("#registrationFees").val('').prop("readonly", true);
				$("#tuitionStartDate").val('').prop("readonly", true);
				$("#tuitionEndDate").val('').prop("readonly", true);
			}else{
				return false;
			}
		}
	});
	
	$("input[name=tuitionFeesAt]").click(function(){
		if($(this).is(":checked")){
			$("#tuitionFees").prop("readonly", false);
		}else{
			$("#tuitionFees").prop("readonly", true);
		}
	});
	
	$("input[name=registrationFeesAt]").click(function(){
		if($(this).is(":checked")){
			$("#registrationFees").prop("readonly", false);
		}else{
			$("#registrationFees").prop("readonly", true);
		}
 	});
	
	//교원검색
	$("#searchUser").autocomplete({
		source : function(request, response){
			$.ajax({
				type:"post",
   	          	dataType:"json",
   	          	url:"/mng/usr/EgovMberManage.json",
   	          	data:{userSeCode : "08", searchUserNm : $("#searchUser").val()}, //, searchDept : $("#hostCode").val()
   	          	success:function(result){
   	          		response($.map(result.items, function(item){     //function의 item에 data가 바인딩된다.
   	            		return{
	       	             label:item.userNm + "("+item.userId+")",
	       	             value:item.userId,
	       	             userNm:item.userNm
   	            		}
   	           		}));
   	          	},
   	          	error: function(){
   	          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
   	          }
			})
		},
   	    //autoFocus:true,             //첫번째 값을 자동 focus한다.
   	    matchContains:true,
   	    selectFirst:false,
   	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
   	    delay:100,                 //milliseconds
   	    select:function(event,ui){         //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
   	    	var selHtml = "<li><input type='hidden' name='userIdList' value='"+ui.item.value+"'/>"+ui.item.label+" <a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'/></a></li>",
   	    		cnt = 0;
   	    	
   	    	$("#box_user input[name='userIdList']").each(function(){
   	    		if($(this).val() == ui.item.value){
   	    			alert("이미 등록 된 교원 입니다.");
   	    			cnt++;
   	    			return false;
   	    		} 
   	    	});
   	    	
   	    	if(cnt == 0){
   	    		$("#box_user").append(selHtml);
   	    	}
   	    	
   	    	$("#searchUser").val(ui.item.userNm);
   	    	return false;
   	    },
   	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	});
	
	//강의책임교원검색
	$("#searchUser2").autocomplete({
		source : function(request, response){
			$.ajax({
				type:"post",
   	          	dataType:"json",
   	          	url:"/mng/usr/EgovMberManage.json",
   	          	data:{userSeCode : "08", searchUserNm : $("#searchUser2").val()}, // , searchDept : $("#hostCode").val()
   	          	success:function(result){
   	          		response($.map(result.items, function(item){     //function의 item에 data가 바인딩된다.
   	            		return{
   	            		 label:item.userNm + "("+item.mngDeptCodeNm+"_"+item.emailAdres+")",
	       	             value:item.userId,
	       	             userNm:item.userNm
   	            		}
   	           		}));
   	          	},
   	          	error: function(){
   	          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
   	          }
			})
		},
   	    //autoFocus:true,             //첫번째 값을 자동 focus한다.
   	    matchContains:true,
   	    selectFirst:false,
   	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
   	    delay:100,                 //milliseconds
   	    select:function(event,ui){         //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
   	    	var selHtml = "<li><input type='hidden' name='userIdList2' value='"+ui.item.value+"'/>"+ui.item.label+" <a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'/></a></li>",
   	    		cnt = 0;
   	    	
   	    	$("#box_user2 input[name='userIdList2']").each(function(){
   	    		if($(this).val() == ui.item.value){
   	    			alert("이미 등록 된 교원 입니다.");
   	    			cnt++;
   	    			return false;
   	    		} 
   	    	});
   	    	
   	    	if(cnt == 0){
   	    		$("#box_user2").append(selHtml);
   	    	}
   	    	
   	    	$("#searchUser2").val(ui.item.userNm);
   	    	return false;
   	    },
   	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	});
	
	//사업비
	var expDepth1 = [],
		expDepth2 = [];
	
	<c:forEach var="result" items="${feeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '1'}">
			expDepth1.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '2'}">
			expDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	//지원비 비목 추가
	$(".btn_addExp").click(function(){
		var selectHtml = "",
			actSelHtml = "<select class='accountCodeList' name='accountCodeList'><option value=''>선택</option></select>",
			tbodyHtml = "";
		
		//지원비 비목
		for(i = 0; i < expDepth1.length; i++){
			if(i == 0){
				selectHtml = "<select class='typeCodeList'><option value=''>선택</option>";
			}
			selectHtml += "<option value='"+expDepth1[i].ctgryId+"'>"+expDepth1[i].ctgryNm+"</option>";
			
			if(i == expDepth1.length - 1){
				selectHtml += "</select>";
			}
		}
		
		tbodyHtml = "<tbody class='box_exp'>" +
						 "<tr class='firstExp'>" +
							"<td class='box_selExp' rowspan='2'>" +
								selectHtml+
								"&nbsp;<a href='#' class='btn_delExp'><img src='/template/manage/images/btn/del.gif'></a>" +
							"</td>" +
							"<td class='line'>" + actSelHtml + "</td>" +
							"<td class='line amount'></td>" +
							"<td class='line reason'></td>" +
							"<td class='line etc'></td>" +
							"<td class='contr'></td>" +
						"</tr>" +
						"<tr class='foot'><td colspan='5'><a href='#' class='btn_addExpMore'><img src='/template/manage/images/btn/btn_plus.gif'/> 계정 목 추가</a></td></tr>" +
					"</tbody>"
		
		$("#box_addExp").before(tbodyHtml);
		
		return false;
	});
	
	//지원비 비목 선택
	$(document).on("change", ".typeCodeList", function(){
		var code = $(this).val(),
			actSelHtml = "<option value=''>선택</option>",
			expIdHtml = "<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='"+code+"'/>";
			
		//계정 목
		for(i = 0; i < expDepth2.length; i++){
			if(code == expDepth2[i].upperCtgryId){
				actSelHtml += "<option value='"+expDepth2[i].ctgryId+"'>"+expDepth2[i].ctgryNm+"</option>";
			}
		}
		//계정목초기화
		$(this).parents("tbody.box_exp").find(".typeCodeListInp").remove();
		$(this).parents("tbody.box_exp").find(".accountCodeList").html(actSelHtml);
		$(this).parents("tbody.box_exp").find(".accountCodeList").after(expIdHtml);
		$(this).parents("tbody.box_exp").find(".amount").html("");
		$(this).parents("tbody.box_exp").find(".reason").html("");
		$(this).parents("tbody.box_exp").find(".etc").html("");
		$(this).parents("tbody.box_exp").find(".contr").html("");
	});
	
	//계정목 선택
	$(document).on("change", ".accountCodeList", function(){
		var code = $(this).val(),
			/* amoutHtml = "<input type='number' class='onlyNum wid80 money' name='amountList'/>", */
			/* amoutHtml = "<input type='text' class='onlyNum wid80 money' name='amountList' onkeyup='inputNumberFormat(this)' value=''/>", */
			amoutHtml = "<input type='text' class='onlyNum wid80 money' name='amountList'/>",
			reasonHtml = "<textarea class='wid80' name='reasonList'></textarea>",
			etcHtml = "<textarea class='wid80' name='etcList'></textarea>",
			btnHtml = "<a href='#' class='btn_delExpMore'><img src='/template/manage/images/btn/del.gif'></a>";
		
		$(this).parents("tr").find(".amount").html(amoutHtml);
		$(this).parents("tr").find(".reason").html(reasonHtml);
		$(this).parents("tr").find(".etc").html(etcHtml);
		$(this).parents("tr").find(".contr").html(btnHtml);
	});
	
	//계정 목 추가
	$(document).on("click", ".btn_addExpMore", function(){
		var trHtml = "",
			code = $(this).parents("tbody.box_exp").find(".typeCodeList").val();
			rowspan = parseInt($(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan")) + 1,
			actSelHtml = "",
			expIdHtml = "<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='"+code+"'/>";
		
		for(i = 0; i < expDepth2.length; i++){
			if(i == 0){
				actSelHtml = "<select class='accountCodeList' name='accountCodeList'><option value=''>선택</option>";
			}
			
			if(code == expDepth2[i].upperCtgryId){
				actSelHtml += "<option value='"+expDepth2[i].ctgryId+"'>"+expDepth2[i].ctgryNm+"</option>";
			}
			
			if(i == expDepth2.length - 1){
				actSelHtml += "</select>" + expIdHtml;
			}
		}
		
		trHtml = "<tr>" +
					"<td>" + actSelHtml + "</td>" +
					"<td class='amount'></td>" +
					"<td class='reason'></td>" +
					"<td class='etc'></td>" +
					"<td class='contr'></td>" +
				"</tr>";
		
		$(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan", rowspan);
		$(this).parents("tr.foot").before(trHtml);
		return false;
	});
	
	//단원추가
	$(".btn_addLes").click(function(){
		var tbodyHtml = "";
		
		tbodyHtml = "<tbody class='box_lesson'>" +
						 "<tr class='first'>" +
							"<td rowspan='2' class='line alC num'>" +
							"<input type='hidden' name='lessonOrderList' class='lessonOrder' value='"+($(".box_lesson").length + 1)+"'>"+
							($(".box_lesson").length + 1) +"</td>" +
							"<td rowspan='2' class='line lesnm'><textarea class='wid100 hei100 lessonNmList'></textarea></td>" +
							"<td class='line'><textarea class='chasiList wid100 inp_long' name='chasiList' style='height:50px;'></textarea>" +
							"<td class='line alC'><a href='#' class='btn_delCha'><img src='/template/manage/images/btn/del.gif'></a></td>" +
						"</tr>" +
						"<tr class='foot'><td colspan='2'><a href='#' class='btn_addChaMore'><img src='/template/manage/images/btn/btn_plus.gif'/> 과정내용 추가</a></td></tr>" +
					"</tbody>"
		$("#box_addLes").before(tbodyHtml);
		
		return false;
	});
	
	//과정내용 추가
	$(document).on("click", ".btn_addChaMore", function(){
		var trHtml = "",
			rowspan = parseInt($(this).parents("tbody.box_lesson").find(".num").attr("rowspan")) + 1;
		trHtml = "<tr class='contentBody'>" +
					"<td class='line'>" +
						"<textarea class='chasiList wid100 inp_long' name='chasiList' style='height:50px;'></textarea>" +
					"</td>" +
					"<td class='line alC'><a href='#' class='btn_delCha'><img src='/template/manage/images/btn/del.gif'></a></td>" +
				"</tr>";
		
		$(this).parents("tbody.box_lesson").find(".num").attr("rowspan", rowspan);
		$(this).parents("tbody.box_lesson").find(".lesnm").attr("rowspan", rowspan);
		$(this).parents("tr.foot").before(trHtml);
		return false;
	});
	
	//시수추가
	$(".btn_addTime").click(function(){
		var tbodyHtml = "",
			rand = randOnlyString(8);
		
		tbodyHtml = "<tbody class='box_staff'>" +
						 "<tr class='first'>" +
						 	"<td rowspan='2' class='line sisu alC'><textarea class='onlyNum timeList' name='timeList'></textarea></td>" +
						 	//"<td class='line alC num'>1</td>" +
							//"<td class='line'></td>" +
							//"<td class='line alC'><a href='#' class='btn_delStf'><img src='/template/manage/images/btn/del.gif'></a></td>" +
						"</tr>" +
						"<tr class='foot "+rand+"'><td colspan='3'><a href='#' class='btn_addStfMore' data-rand='"+rand+"'><img src='/template/manage/images/btn/btn_plus.gif'/> 교원 검색 및 추가</a></td></tr>" +
					"</tbody>";
		
		$("#box_addTime").before(tbodyHtml);
		
		return false;
	});
	
	//교원검색 추가 팝업
	$(document).on("click", ".btn_addStfMore", function(){
		var rand = $(this).data("rand"),
			searchDept = "${curriculumVO.hostCode}";
		//window.open("/mng/usr/EgovMberManage.do?templateAt=N&position="+rand+"&searchDept="+searchDept, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
		window.open("/mng/usr/EgovMberManage.do?templateAt=N&position="+rand, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");

		return false;
	});
	
	//수료기준
	$(".radiocheck").change(function(){
		var val = $(this).val();
		if(val == "Y"){
			$(this).parents("td").find(".inpTxt").attr("readonly", false);
		}else{
			$(this).parents("td").find(".inpTxt").attr("readonly", true);
		}
	});
	
	//총괄평가 기준 추가
	$(".btn_addEvt").click(function(){
		window.open("/mng/lms/manage/lmsEvtPopList.do", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=520,height=600");
		return false;
	});
	
	//교재검색
	$(".btn_addBook").click(function(){
		var lang = $(this).data("lang");
		window.open("/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000005&tmplatImportAt=N&searchCateList="+lang, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=1050,height=600");
		return false;
	});
	
	
	box_lesson();
	
	
	// 강의책임교원 배정 입력 칸에 엔터키 이벤트 제거
/* 	document.detailForm.addEventListener("keydown", evt => {
	    if ((evt.keyCode || evt.which) === 13) {
	        evt.preventDefault();
	    }
	}); */
	$('input[type="text"]').keydown(function() {
		if ((event.keyCode || event.which) === 13) {
		  event.preventDefault();
		}
	});
	
});


//교원삭제
$(document).on("click", ".btn_del", function(){
	$(this).parent().remove();
	return false;
});

//지원비삭제
$(document).on("click", ".btn_delExp", function(){
	$(this).parents("tbody.box_exp").remove();
	return false;
});

//계정목삭제
$(document).on("click", ".btn_delExpMore", function(){
	var rowspan = $(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan") - 1;
	
	if(trCnt = $(this).parents("tr").hasClass("firstExp")){
		alert("계정 목 최상단은 삭제 하실 수 없습니다.");
	}else{
		$(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	return false;
});

//과정내용삭제
$(document).on("click", ".btn_delCha", function(){
	var rowspan = $(this).parents("tbody.box_lesson").find(".num").attr("rowspan") - 1;
	var innerHtml = '';
	if($(this).parents("tr").hasClass("first")){
		if(confirm("과정내용 최상단 삭제 시 해당 단원명 및 과정내용이 같이 삭제 됩니다. 삭제하시겠습니까?")){
			$(this).parents("tbody.box_lesson").remove();
			$("tbody.box_lesson .num").each(function(i){
				innerHtml = (i+1)+'<input type="hidden" value='+(i+1)+' name="lessonOrderList">';
				$(this).html(innerHtml);
			});
		}
	}else{
		$(this).parents("tbody.box_lesson").find(".num").attr("rowspan", rowspan);
		$(this).parents("tbody.box_lesson").find(".lesnm").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	
	return false;
});

//시수 입력 시
$(document).on("change", ".timeList", function(){
	sisuTotCnt();
});

//교원삭제
$(document).on("click", ".btn_delStf", function(){
	var rowspan = $(this).parents("tbody.box_staff").find(".sisu").attr("rowspan") - 1;
	
	if($(this).parents("tr").hasClass("first")){
		if(confirm("과정내용 최상단 삭제 시 해당 시수 및 교원이 같이 삭제 됩니다. 삭제하시겠습니까?")){
			$(this).parents("tbody.box_staff").remove();
		}
	}else{
		$(this).parents("tbody.box_staff").find(".sisu").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	
	sisuTotCnt();
	facTotCnt();
	
	return false;
});

//총괄평과 입력 시
$(document).on("change", ".evtper", function(){
	totperCnt();
});

//총괄평가삭제
$(document).on("click", ".btn_delEvt", function(){
	$(this).parents("tr").remove();
	totperCnt();
	return false;
});

//게시판선택
$(document).on("click", ".btn_bbs", function(){
	var id = $(this).data("id");
	
	window.open("/lms/manage/lmsBbsPopList.do?crclId="+id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=650,height=770");
	return false;
});

//게시판삭제
$(document).on("click", ".btn_delbbs", function(){
	$(this).parents("li").remove();
	return false;
});

//교재삭제
$(document).on("click", ".btn_delBook", function(){
	$(this).parents("tr").remove();
	bookNumSort();
	return false;
});


function selcrclb(id, name, code1, code2, code3){
	$.ajax({
		url : "/mng/lms/crclb/selectCurriculumbaseAjax.do"
		, type : "post"
		, dataType : "html"
		, data : {crclbId : id}
		, success : function(data){
			$("#box_base").html(data);
			
			$(".base_name").text(name);
			$("#crclbId").val(id);
			
			$("#ctgryId1").val(code1).attr("selected", true);
			$("#ctgryId1").change();
			$("#ctgryId2").val(code2).attr("selected", true);
			$("#ctgryId2").change();
			$("#ctgryId3").val(code3).attr("selected", true);
			$("#ctgryId3").change();
		}, error : function(){
			alert("기본과정을 불러오는데 실패했습니다.");
		}
	});
}

function selectcrclb(id){
	$.ajax({
		url : "/mng/lms/crclb/selectCurriculumbaseAjax.do"
		, type : "post"
		, dataType : "html"
		, data : {crclbId : id}
		, success : function(data){
			$("#box_base").html(data);
		}, error : function(){
			alert("기본과정을 불러오는데 실패했습니다.");
		}
	});
}

function autoTxt(){
	var yearTxt = $("#crclYear").val() + "년도",
		crclTermTxt = "",
		crclLangTxt = "",
		ctgryId1Txt = "",
		ctgryId2Txt = "",
		ctgryId3Txt = "";
	
	if($("#crclTerm").val()){
		crclTermTxt = $("#crclTerm > option:selected").text();
	}
	if($("#crclLang").val()){
		crclLangTxt = $("#crclLang > option:selected").text();
	}
	if($("#ctgryId1").val()){
		ctgryId1Txt = $("#ctgryId1 > option:selected").text();
	}
	if($("#ctgryId2").val()){
		ctgryId2Txt = $("#ctgryId2 > option:selected").text();
	}
	if($("#ctgryId3").val()){
		ctgryId3Txt = $("#ctgryId3 > option:selected").text();
	}
	
	var txt = yearTxt + " " + crclTermTxt + " " + crclLangTxt + " " + ctgryId1Txt + " " + ctgryId2Txt + " " + ctgryId3Txt;
	//$("#crclNm").val(txt);
}

function selUser(id, name, mngdept, mngdeptnm, position){
	var trHtml = "",
		nameStr = "",
		idStr = "",
		facCnt = $(".sumUser").text();
	
	for(i = 0; i < id.length; i++){
		var rowspan = parseInt($("."+position).parents("tbody.box_staff").find(".sisu").attr("rowspan")) + 1,
			prfNum = $("."+position).parents("tbody.box_staff").find(".box_prf").length + 1;
		
		nameStr = name[i] + "(" + mngdeptnm[i] + ")";
		
		trHtml = "<tr class='box_prf'>" +
				 	"<td class='line alC num'>"+prfNum+"</td>" +
					"<td class='line'>" +
						nameStr +
						"<input type='hidden' class='facIdList' name='facIdList' value='"+id[i]+"'/>" +
						"<input type='hidden' class='facNmList' name='facNmList' value='"+nameStr+"'/>" +
						"<input type='hidden' name='positionList' value='"+position+"'/>" +
					"</td>" +
					"<td class='line alC'><a href='#' class='btn_delStf'><img src='/template/manage/images/btn/del.gif'></a></td>" +
				 "</tr>";
		
		$("."+position).parents("tbody.box_staff").find(".sisu").attr("rowspan", rowspan);
		$("tr."+position).before(trHtml);
	}
	
	//교원 수
	facTotCnt();
}

//교원 수
function facTotCnt(){
	var dupCnt = 0,
		ids = [],
		idList = [];
	
	$(".facIdList").each(function(){
		var id = $(this).val(),
			dupAt = false;
		$.each(idList, function(i){
			if(idList[i] == id){
				dupAt = true;
			}
		});
		
		if(!dupAt){
			idList.push(id);
		}
	});
	
	$(".sumUser").text(idList.length);
}

//시수 계산
function sisuTotCnt(){
	var sisuCnt = 0;
	
	//총 시수
	$(".timeList").each(function(){
		sisuCnt += Number($(this).val());
	});
	
	$(".curTime").text(sisuCnt);
}

//총괄평과 계산
function totperCnt(){
	var totper = 0,
		emptyHtml = "";
	
	if($(".evtper").length > 0){
		$(".evtper").each(function(){
			totper += Number($(this).val());
		});
	}else{
		emptyHtml = "<tr class='box_empty'><td class='alC' colspan='3'>아래 [ + 총괄평가 기준 추가하기] 버튼을 눌러 성적 대상을 선택하세요.</td></tr>";
		$(".box_evt").html(emptyHtml);
	}
	
	$(".totper").text(totper);
}

//게시판
function selBbs(id, name, colect){
	var html = "",
		bbsCnt = 0;
	
	$("input[name=bbsIdList]").each(function(){
		if($(this).val() == id){
			bbsCnt++;
			return false;
		}
	});
	
	if(bbsCnt == 0){
		html = "<li>" +
					name +
					"<input type='hidden' name='bbsIdList' value='"+id+"'/>" +
					"<input type='hidden' name='colectList' value='"+colect+"'/>" +
					"<a href='#' class='btn_delbbs'><img src='/template/manage/images/btn/del.gif'></a>" +
				"</li>";
		
		$("#box_bbsmaster").append(html);	
	}else{
		alert("이미 등록된 게시판입니다.");
	}
}

//총괄평가기준추가
function selEvt(id, name, stand){
	var idCnt = $(".box_evt").find("."+id).length,
		html = "",
		evtSel = "<c:forEach var="result" items="${homeworkComCode}"><option value='${result.code}'>${result.codeNm}</option></c:forEach>";
	
	if(idCnt == 0 || id == "CTG_0000000000000119"){
		if(id == "CTG_0000000000000119"){//기타
			html = "<tr class='"+id+"'>" +
						"<td class='alC'>" +
							"<input type='text' class='wid80' name='evtNmList' value='"+name+"' placeholder='기타'/>" +
						"</td>";
		}else{
			html = "<tr class='"+id+"'>" +
						"<td class='alC'>" +
							name +
							"<input type='hidden' name='evtNmList' value='"+name+"'/>" +
						"</td>";
		}
		
		html += "<input type='hidden' name='evtIdList' value='"+id+"'/>" +
				"<input type='hidden' name='evtStandList' value='"+stand+"'/>";
		
		if(id == "CTG_0000000000000115"){//과제
			html += "<td>" +
						"<input type='number' class='evtper onlyNum' name='evtValList'/>%" +
						"<select name='evtType'>" +
							evtSel +
						"</select>" +
					"</td>";
		}else if(id == "CTG_0000000000000117"){//수업참여도
			html += "<td>" +
						"<input type='number' class='evtper onlyNum' name='evtValList'/>%" +
						"<a href='#' class='btn_mngTxt btn_bbs' data-id='${curriculumVO.crclId}'>게시판 선택</a>" +
						"<ul id='box_bbsmaster'></ul>" +
					"</td>";
		}else{
			html += "<td>" +
						"<input type='number' class='wid80 evtper onlyNum' name='evtValList'/>%" +
					"</td>";
		}
		
		html += "<td class='line alC'><a href='#' class='btn_delEvt'><img src='/template/manage/images/btn/del.gif'></a></td>" +
				"</tr>";
				
		$(".box_evt").append(html);
		$(".box_evt .box_empty").remove();
	}else{
		alert("["+ name +"]은(는) 이미 추가된 총괄평가 기준입니다.");
	}
}

//교재 및 부교재 추가
function addbook(id, sj, publish){
	var html = "",
		bookCnt = 0;
	
	$(".nttNoList").each(function(){
		if($(this).val() == id){
			bookCnt++;
			return false;
		}
	});
	
	if(bookCnt == 0){
		$(".list_book .empty").remove();
		
		html = "<tr>" +
					"<td class='num alC'></td>" +
					"<td class='alC'>" +
						sj +
						"<input type='hidden' class='nttNoList' name='nttNoList' value='"+id+"'/>" +
					"</td>" +
					"<td class='alC'>"+publish+"</td>" +
					"<td class='alC'><a href='#' class='btn_delBook'><img src='/template/manage/images/btn/del.gif'></a></td>" +
				"</tr>";
					
		$(".list_book").append(html);
		bookNumSort();
	}else{
		alert("이미 등록된 교재입니다.");
	}
}

//교재번호 재정렬
function bookNumSort(){
	$(".list_book tr").each(function(i){
		$(this).find(".num").text(i);
	});
}

//폼 validator
function vali(){
	
	if(${totalTimeAt eq 'Y'} || ${processSttusCode} < 3 ){
		if($("#totalTimeAt").val() == "Y"){
			if(!$("#totalTime").val()){
				alert("총 시간을 입력해주세요.");
				return false;
			}else if(!$("#weekNum").val()){
				alert("주 횟수를 입력해주세요.");
				return false;
			}else if(!$("#dayTime").val()){
				alert("일 시간을 입력해주세요.");
				return false;
			}else if(!$("#lectureDay").val()){
				alert("강의요일을 입력해주세요.");
				return false;
			}
		}else if(!$("#applyStartDate").val()){
			alert("수강신청 시작기간을 입력해주세요.");
			return false;
		}else if(!$("#applyEndDate").val()){
			alert("수강신청 종료기간을 입력해주세요.");
			return false;
		}
	}
	
	<%-- 수강신청 전 --%>
	if(${processSttusCode} < 3){
		if($("#box_user li").length == 0){
			alert("책임교원을 등록해주세요.");
			return false;
		}
	}
	if(${processSttusCode} > 0){
		//총괄평가
		totperCnt();
		
		//과정내용
		$("textarea[name=lessonNmList]").remove();
		$(".lesnm").each(function(){
			var rownum = $(this).attr("rowspan"),
				text = $(this).children("textarea").val();
			for(i = 1; i < rownum; i++){
				textarea = "<textarea class='wid100 hei100' name='lessonNmList'>"+text+"</textarea>";
				$("#box_lesnm").append(textarea);
			}
		});
		
		//시수-교원배정
		$("input[name=sisuList]").remove();
		$(".facIdList").each(function(){
			var sisu = $(this).parents("tbody.box_staff").find(".timeList").val();
			
			$(this).parents("td").append("<input type='hidden' name='sisuList' value='"+sisu+"'/>");
		});
		
		//총괄평가기준
		if($(".totper").text().trim() != "100"){
			alert("총괄평가기준 성적 반영률이 100%인지 확인해주세요.");
			return false;
		}
	}
	
	// 과정내용 
	var totalContentList = '';  	// 전체 문자열
	var totalContentArr = [];  		// 전체 문자배열
	var totalContentLen = 0;  		// 전체 과정내용 길이
	
	var contentList = '';  	  		
	var contentArr = [];  	  
	var contentBodyLen = 0;  		// 과정내용 길이(첫번재 row 제외)
	var contentRow  = 0 ;			// 과정내용 번호
	var contentLessonNm = ''		// 내용범주
	var lessonNmCnt = 0			// 내용범주
	var contentChasiNm ='';			// 과정내용의 내용
	var contentChasiNmList ='';		// 과정내용의 문자열
	var contentChasiNmArr =[];		// 과정내용의 내용
	
	totalContentLen = $(".box_lesson").length;
	for(var i = 0 ; i < totalContentLen ; i++){
		contentList = '';
		contentArr = [];
		contentChasiNmArr = [];
		contentChasiNmList = '';
		
		
		contentRow = 0;
		contentRow = $(".box_lesson").eq(i).find(".first > td > input[name=lessonOrderList]").val();
		
		contentLessonNm  = '';
		if($(".box_lesson").eq(i).find(".first > .lesnm").find("textarea").val() == ''){
			lessonNmCnt = lessonNmCnt+1;
		}
		contentLessonNm = $(".box_lesson").eq(i).find(".first > .lesnm").find("textarea").val();
		
		contentChasiNm = '';
		
		if($(".box_lesson").eq(i).find(".first > td > textarea[name=chasiList]").val() == '')
			contentChasiNm =  'emptyContentChasiNm';
		else
			contentChasiNm =  $(".box_lesson").eq(i).find(".first > td > textarea[name=chasiList]").val();
		
		contentChasiNmArr.push(contentChasiNm);

		contentBodyLen = $(".box_lesson").eq(i).find(".contentBody").length;
	
		if(contentBodyLen > 0 ){
			contentChasiNm = '';
			
			for(var j = 0;  j < contentBodyLen ;  j++){
				
				contentChasiNm = $(".box_lesson").eq(i).find(".contentBody .chasiList").eq(j).val();
				if(contentChasiNm != '')
					contentChasiNmArr.push(contentChasiNm);
				else 
					contentChasiNmArr.push("emptyContentChasiNm");
			}
			
		}
		
		contentChasiNmList = contentChasiNmArr.join("#third#");
		
		contentArr.push(contentRow);
		contentArr.push(contentLessonNm);
		contentArr.push(contentChasiNmList);
		contentList = contentArr.join("#second#");
		
		totalContentArr.push(contentList);
	}
	
	totalContentList = totalContentArr.join("#first#");
	$("#lessonContentList").val(totalContentList);
	
	if(lessonNmCnt > 0){
		alert("입력하지 않은 내용범주가 있습니다.\n내용범주를 입력해 주세요.");
		return false;
	}
	
	
	return;
}



function box_lesson(){
	var htmls = '';
	var crclId = '${curriculumVO.crclId}';
	var preLessonOrder = 0;
	var lessonOrderCnt = 0;
	var totLen =0;
	
	$.ajax({
           type: "POST",
           url: "/ajax/mng/lms/crcl/selectCurriculumLesson.json",
           data: {                
               "crclId" : crclId
               },
           dataType: "json",
           async: false,
           success: function(data, status) {
           	console.log(data);
           	console.log(data.lessonList);
           	$(".box_lesson > tr").remove();
           	if(data.status == "200"){
           		totLen=data.lessonList.length;
           		
           		$.each(data.lessonList, function(index){
           			
           			if(lessonOrderCnt == 0){
           				lessonOrderCnt = this.lessonOrderCnt;
           				preLessonOrder = this.lessonOrder;
    					htmls += '<tbody class="box_lesson">';
    					htmls += '<tr class="first">';
   					} else {
    					htmls += '<tr class="contentBody">';
   						
   					}
                   	 
   					if((lessonOrderCnt == this.lessonOrderCnt) && (preLessonOrder == this.lessonOrder)){
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line alC num">';
   						htmls += this.lessonOrder;
   						htmls += '<input type="hidden" name="lessonOrderList" class="lessonOrder" value="'+this.lessonOrder+'"></td>';
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line lesnm"><textarea class="wid100 hei100">'+this.lessonNm+'</textarea></td>';
   					}
           			
					htmls += '<td class="line"><textarea class="chasiList wid100 lessonNmList inp_long" name="chasiList" style="height:50px;">'+this.chasiNm+'</textarea></td>';
					htmls += '<td class="line alC"><a href="#" class="btn_delCha"><img src="/template/manage/images/btn/del.gif"></a></td>';
					htmls += '</tr>';
					if((lessonOrderCnt-1) == 0){
						htmls += '<tr class="foot"><td colspan="2"><a href="#" class="btn_addChaMore"><img src="/template/manage/images/btn/btn_plus.gif"/> 과정내용 추가</a></td></tr>';
    					htmls += '</tbody">';
						
					}
					preLessonOrder = this.lessonOrder;
   					--lessonOrderCnt;
           		});
           		
				$("#box_addLes").before(htmls);
           	}
           },
           error: function(xhr, textStatus) {
               alert("문제가 발생하였습니다. \n 관리자에게 문의하세요");
               document.write(xhr.responseText);
               return false;
           },beforeSend:function() {
           },
           complete:function() {
           }
       }); 
}

// 사업비 금액 , 표기
$(document).on("keyup", "input[name=amountList]", function(){
	console.log("키업");
	console.log($(this).val());
	var param_amount = 0;
	param_amount = comma(uncomma($(this).val()));
	$(this).val(param_amount);
	
	
});

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}

</script>

<c:if test="${processSttusCode > 0}">
<table class="chart2 mb50">
	<tr>
		<td class="alC">
			<c:out value="${curriculumVO.crclNm }"/>
			<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
		</td>
	</tr>
</table>
</c:if>

<c:if test="${curriculumVO.processSttusCode > 0}">
	<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
		<c:param name="processSttusCode" value="${processSttusCode}"/>
		<c:param name="step" value="3"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
	</c:import>
</c:if>

<form:form commandName="curriculumVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return vali();">
	<input type="hidden" id="totalTimeAt" value="<c:out value="${curriculumVO.totalTimeAt eq 'Y' ? 'Y' : 'N'}"/>"/>
	<input type="hidden" id="lessonContentList" name="lessonContentList" value=""/>
	<c:choose>
		<c:when test="${processSttusCode < 3}">
			<strong>*기본과정 선택 </strong> <!-- <span class="noti">년도, 학기, 언어, 과정체계를 입력하시면 과정명이 자동으로 표기됩니다.(수정 가능)</span> -->
			<table class="chart2">
				<caption>등록폼</caption>
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th><em>*</em> <label>년도</label></th>
						<td>
							<select id="crclYear" name="crclYear" class="autoTxt">
								<option value="">선택</option>
								<c:forEach var="result" items="${yearList}" varStatus="status">
					  				<option value="${yearList[status.index]}" <c:if test="${curriculumVO.crclYear eq yearList[status.index]}">selected="selected"</c:if>>${yearList[status.index]}</option>
					  			</c:forEach>
							</select>
						</td>
						<th><em>*</em> <label>학기</label></th>
						<td>
							<select id="crclTerm" name="crclTerm" class="autoTxt" required>
								<option value="">선택</option>
								<c:forEach var="result" items="${crclTermList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.crclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
						</td>
						<th><em>*</em> <label>언어</label></th>
						<td>
							<select id="crclLang" name="crclLang" class="autoTxt" required>
								<option value="">선택</option>
								<c:forEach var="result" items="${langList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.crclLang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>과정체계</label></th>
						<td colspan="5">
							<select id="ctgryId1" name="sysCodeList" required>
								<option value="">대분류</option>
								<c:forEach var="result" items="${sysCodeList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq sysCode[1]}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
							<select id="ctgryId2" name="sysCodeList" required>
								<option value="">중분류</option>
							</select>
							<select id="ctgryId3" name="sysCodeList" class="autoTxt" required>
								<option value="">소분류</option>
							</select>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>기본과정 선택</label></th>
						<td>
							<select id="crclbId" name="crclbId" required>
								<option value=''>기본과정명</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<strong>*기본과정 </strong>
			<table class="chart2">
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th><label>년도</label></th>
						<td>
							<c:out value="${curriculumVO.crclYear }"/>
						</td>
						<th><label>학기</label></th>
						<td>
							<c:out value="${curriculumVO.crclTermNm}"/>
						</td>
						<th><label>언어</label></th>
						<td>
							<c:out value="${curriculumVO.crclLangNm}"/>
						</td>
					</tr>
					<tr>
						<th><label>과정체계</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.sysCodeNmPath}"/>
						</td>
					</tr>
					<tr>
						<th><label>기본과정명</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.crclbNm}"/>
						</td>
					</tr>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	
	<div id="box_base">
		<c:if test="${not empty curriculumVO.crclbId}">
			<table class="chart2 mb50">
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th><label>이수구분</label></th>
						<td>
							<c:out value="${curriculumVO.divisionNm }"/>
						</td>
						<th><label>학점인정여부</label></th>
						<td>
							<c:choose>
								<c:when test="${curriculumVO.gradeAt eq 'Y'}">예</c:when>
								<c:otherwise>아니오</c:otherwise>
							</c:choose>
						</td>
						<th><label>관리구분</label></th>
						<td colspan="3">
							<c:out value="${curriculumVO.controlNm}"/>
						</td>
					</tr>
					<tr>
						<th><label>대상</label></th>
						<td colspan="5">
							<c:choose>
		    					<c:when test="${curriculumVO.targetType eq 'Y'}">본교생</c:when>
		    					<c:otherwise>일반</c:otherwise>
		    				</c:choose>
							> <c:out value="${curriculumVO.targetDetailNm}"/>
						</td>
					</tr>
					<tr>
						<th><label>성적처리기준</label></th>
						<td colspan="5">
							<c:choose>
		    					<c:when test="${curriculumVO.evaluationAt eq 'Y'}">절대평가</c:when>
		    					<c:otherwise>상대평가</c:otherwise>
		    				</c:choose>
		    				<c:if test="${not empty curriculumVO.gradeType}">
		    					> <c:out value="${curriculumVO.gradeTypeNm}"/>
		    				</c:if>
						</td>
					</tr>
					<tr>
						<th><label>운영보고서 유형</label></th>
						<td colspan="5">
							<c:choose>
		    					<c:when test="${curriculumVO.reportType eq 'NOR'}">일반형</c:when>
		    					<c:when test="${curriculumVO.reportType eq 'PRJ'}">프로젝트형</c:when>
		    				</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>과정만족도 설문 유형</label></th>
						<td>
							<c:forEach var="result" items="${surveyList}" varStatus="status">
								<c:if test="${result.schdulId eq curriculumVO.surveySatisfyType}">${result.schdulNm}</c:if>
							</c:forEach>
						</td>
						<th><label>교원대상 과정만족도 설문 유형</label></th>
						<td colspan="3">
							<c:choose>
								<c:when test="${empty curriculumbaseVO.professorSatisfyType}">
									선택안함
								</c:when>
								<c:otherwise>
									<c:forEach var="result" items="${surveyList3}" varStatus="status">
										<c:if test="${result.schdulId eq curriculumVO.professorSatisfyType}">${result.schdulNm}</c:if>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>개별 수업만족도조사 실시여부</label></th>
						<td colspan="5">
							<c:choose>
		    					<c:when test="${curriculumVO.surveyIndAt eq 'Y'}">예</c:when>
		    					<c:otherwise>아니오</c:otherwise>
		    				</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>학생 수강신청 제출서류 여부</label></th>
						<td colspan="5">
							<c:choose>
		    					<c:when test="${curriculumVO.stdntAplyAt eq 'Y'}">예</c:when>
		    					<c:otherwise>아니오</c:otherwise>
		    				</c:choose>
						</td>
					</tr>
				</tbody>
			</table>
		</c:if>
	</div>
	<c:choose>
		<c:when test="${processSttusCode < 3}">
			<strong>*교육과정등록</strong>&nbsp;&nbsp;
			<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
			<table class="chart2 mb50">
				<caption>등록폼</caption>
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th><em>*</em> <label>과정명</label></th>
						<td colspan="5"><form:input path="crclNm" cssClass="inp_long" required="true"/></td>
					</tr>
					<tr>
						<th><em>*</em> <label>과정기간</label></th>
						<td colspan="5"><form:input path="startDate" required="true" cssClass="btn_calendar" placeholder="시작일" readonly="true"/> ~ <form:input path="endDate" required="true" cssClass="btn_calendar" placeholder="종료일" readonly="true"/></td>
					</tr>
					<c:if test="${curriculumVO.totalTimeAt eq 'Y' or _MODE eq 'REG'}">
						<tr class="timeAt">
							<th><em>*</em> <label>총 시간</label></th>
							<td><input type="number" id="totalTime" name="totalTime" value="${curriculumVO.totalTime}" class="onlyNum"/>시간</td>
							<th><em>*</em> <label>주 횟수</label></th>
							<td><input type="number" id="weekNum" name="weekNum" value="${curriculumVO.weekNum}" class="onlyNum"/>회</td>
							<th><em>*</em> <label>일 시간</label></th>
							<td>
								<input type="number" id="dayTime" name="dayTime" value="${curriculumVO.dayTime}" class="onlyNum"/>시간<br/>
								<input type="text" id="startTime" name="startTime" value="${curriculumVO.startTime}" placeholder="시작시간(ex.오후 3:00)" style="margin-top:5px;"/>
							</td>
						</tr>
						<tr class="timeAt">
							<th><em>*</em> <label>강의요일</label></th>
							<td colspan="5"><form:input path="lectureDay" cssClass="inp_long" placeholder="강의요일 입력(ex.월,화(주 2회))"/></td>
						</tr>
					</c:if>
					<tr>
						<th><em>*</em> <label>과정 진행 캠퍼스</label></th>
						<td colspan="3">
							<select id="campusId" name="campusId" required>
								<option value="">캠퍼스 선택</option>
								<c:forEach var="result" items="${campusList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.campusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
							&nbsp;&nbsp;
							<form:input path="campusPlace" cssClass="inp_long" required="true" placeholder="상세장소 입력"/>
						</td>
						<th><label>수업 시간표 설정</label></th>
						<td>
							<label><input type="radio" name="campustimeUseAt" value="Y" <c:if test="${curriculumVO.campustimeUseAt ne 'N'}">checked="checked"</c:if>/>캠퍼스 시간표</label>
							&nbsp;&nbsp;&nbsp;
							<label><input type="radio" name="campustimeUseAt" value="N" <c:if test="${curriculumVO.campustimeUseAt eq 'N'}">checked="checked"</c:if>/>시간표 수기입력</label>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>주관기관/학과</label></th>
						<td colspan="5">
							<select id="hostCode" name="hostCode" required>
								<option value="">선택</option>
								<c:forEach var="result" items="${insList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.hostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>책임교원 </label></th>
						<td colspan="5">
							<input type="text" id="searchUser" placeholder="교원 명을 입력해주세요."/>
							<ul id="box_user">
								<c:if test="${not empty curriculumVO.userId}">
									<c:set var="userId" value="${fn:split(curriculumVO.userId,',')}"/>
									<c:set var="userNm" value="${fn:split(curriculumVO.userNm,',')}"/>
									
									<c:forEach var="user" items="${userId}" varStatus="status">
										<li>
											<input type='hidden' name='userIdList' value='${userId[status.count-1]}'/>
											<c:out value="${userNm[status.count-1]}"/>(${userId[status.count-1]})
											<a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'/></a>
										</li>
									</c:forEach>
								</c:if>
							</ul>
						</td>
					</tr>
					<tr>
						<th><label>과정 개요 및 목표</label></th>
						<td colspan="5">
							<form:textarea path="crclGoal" cssClass="inp_long" cssStyle="height:100px;"/>
						</td>
					</tr>
					<tr>
						<th><label>과정의 기대효과</label></th>
						<td colspan="5">
							<form:textarea path="crclEffect" cssClass="inp_long" cssStyle="height:100px;"/>
						</td>
					</tr>
					<tr>
						<th><label>과정 계획서 작성기간</label></th>
						<td colspan="5"><form:input path="planStartDate" cssClass="btn_calendar" placeholder="시작일" readonly="true"/> ~ <form:input path="planEndDate" cssClass="btn_calendar" placeholder="종료일" readonly="true"/></td>
					</tr>
					<tr>
						<th><em>*</em> <label>수강신청기간</label></th>
						<td colspan="5"><form:input path="applyStartDate" required="true" cssClass="btn_calendar" placeholder="시작일" readonly="true"/> ~ <form:input path="applyEndDate" required="true" cssClass="btn_calendar" placeholder="종료일" readonly="true"/></td>
					</tr>
					<tr>
						<th><em>*</em> <label>수강정원</label></th>
						<td colspan="5">
							<label><input type="radio" name="applyMaxCntAt" value="Y" <c:if test="${curriculumVO.applyMaxCnt != 0}">checked="checked"</c:if>/>인원제한</label>
							&nbsp;<input type="number" id="applyMaxCnt" name="applyMaxCnt" value="<c:out value="${curriculumVO.applyMaxCnt}" default="0"/>" class="onlyNum money num"/> 명&nbsp;&nbsp;
							<label><input type="radio" name="applyMaxCntAt" value="N" <c:if test="${curriculumVO.applyMaxCnt == 0}">checked="checked"</c:if>/>제한없음</label>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>주관학과 배정여부</label></th>
						<td colspan="5">
							<label><input type="radio" name="assignAt" value="Y" <c:if test="${curriculumVO.assignAt eq 'Y'}">checked="checked"</c:if>/>예</label>
							&nbsp;&nbsp;&nbsp;
							<label><input type="radio" name="assignAt" value="N" <c:if test="${curriculumVO.assignAt ne 'Y'}">checked="checked"</c:if>/>아니오</label>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>등록금 납부대상과정 여부</label></th>
						<td colspan="5">
							<label><input type="radio" name="tuitionAt" value="Y" <c:if test="${curriculumVO.tuitionAt eq 'Y'}">checked="checked"</c:if>/>예</label>&nbsp;
							(
								<label><input type="checkbox" name="tuitionFeesAt" value="Y" <c:if test="${not empty curriculumVO.tuitionFees and curriculumVO.tuitionFees != 0}">checked="checked"</c:if> <c:if test="${curriculumVO.tuitionAt ne 'Y'}">disabled</c:if>/> 수업료</label>&nbsp;&nbsp;
								<label><input type="checkbox" name="registrationFeesAt" value="Y" <c:if test="${not empty curriculumVO.registrationFees and curriculumVO.registrationFees != 0}">checked="checked"</c:if> <c:if test="${curriculumVO.tuitionAt ne 'Y'}">disabled</c:if>/> 등록비</label>
							)&nbsp;&nbsp;
							<label><input type="radio" name="tuitionAt" value="N" <c:if test="${curriculumVO.tuitionAt ne 'Y'}">checked="checked"</c:if>/>아니오</label>
						</td>
					</tr>
					<tr>
						<th><label>등록금액</label></th>
						<td colspan="5">
							<label>수업료 : <input type="number" id="tuitionFees" name="tuitionFees" value="${curriculumVO.tuitionFees}" class="onlyNum money"/>원</label>&nbsp;&nbsp;
							<label>등록비 : <input type="number" id="registrationFees" name="registrationFees"  value="${curriculumVO.registrationFees}" class="onlyNum money"/>원</label>
						</td>
					</tr>
					<tr>
						<th><em>*</em> <label>등록기간</label></th>
						<td colspan="5"><form:input path="tuitionStartDate" cssClass="btn_calendar" placeholder="시작일" readonly="true"/> ~ <form:input path="tuitionEndDate" cssClass="btn_calendar" placeholder="종료일" readonly="true"/></td>
					</tr>
				</tbody>
			</table>
			<strong>* 사업비(소요예산)</strong>
			<table class="chart2 mb50">
				<colgroup>
					<col width="15%"/>
					<col width="15%"/>
					<col width="10%"/>
					<col width="25%"/>
					<col width="25%"/>
					<col width="10%"/>
				</colgroup>
				<caption>등록폼</caption>
				<tbody>
					<tr>
						<th class="alC">지원비 비목</th>
						<th class="alC">계정 목</th>
						<th class="alC">금액(원)</th>
						<th class="alC">산출근거</th>
						<th class="alC">비고</th>
						<th class="alC">관리</th>
					</tr>
				</tbody>
				
				<%-- 지원비 비목 묶어주는 역할 --%>	
				<c:set var="rowspan" value="0"/>
				<c:set var="rowspanList" value=""/>
				<c:set var="prevCode" value="${expense[0].typeCode}"/>
				<c:forEach var="list" items="${expense}" varStatus="status">
					<c:choose>
						<c:when test="${prevCode eq list.typeCode}">
							<c:set var="rowspan" value="${rowspan + 1}"/>
						</c:when>
						<c:otherwise>
							<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
							<c:set var="rowspan" value="1"/>
							<c:set var="prevCode" value="${list.typeCode}"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
				<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
				<c:set var="listCnt" value="${rowspan[0]}"/>
				<c:set var="rowspanCnt" value="0"/>
				
				<c:set var="prevCode" value=""/>
				<c:forEach var="result" items="${expense}" varStatus="status">
					<c:if test="${prevCode ne result.typeCode}">
						<tbody class="box_exp">
					</c:if>
					<tr <c:if test="${prevCode ne result.typeCode}">class='firstExp'</c:if>>
						<c:if test="${prevCode ne result.typeCode}">
							<td class="line box_selExp" rowspan="${rowspan[rowspanCnt] + 1}">
								<select class='typeCodeList'>
									<option value=''>선택</option>
									<c:forEach var="accountCode" items="${feeList}" varStatus="sta">
										<c:if test="${accountCode.ctgryLevel eq '1'}">
											<option value="${accountCode.ctgryId }" <c:if test="${result.typeCode eq accountCode.ctgryId}">selected="selected"</c:if>><c:out value="${accountCode.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
								&nbsp;<a href='#' class='btn_delExp'><img src='/template/manage/images/btn/del.gif'></a>
							</td>
							<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
						</c:if>
						<td class="line">
							<select class='accountCodeList' name='accountCodeList'>
								<option value=''>선택</option>
								<c:forEach var="accountCode" items="${feeList}" varStatus="sta">
									<c:if test="${accountCode.ctgryLevel eq '2' and accountCode.upperCtgryId eq result.typeCode}">
										<option value="${accountCode.ctgryId }" <c:if test="${result.accountCode eq accountCode.ctgryId}">selected="selected"</c:if>><c:out value="${accountCode.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
							<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='${result.typeCode}'/>
						</td>
						<td class="line amount">
							<fmt:formatNumber value="${result.amount}" var="amount" pattern="#,###"/>
							<input type="text" name="amountList" value="${amount}" class="onlyNum wid80 money"/>원
						</td>
						<td class="line reason"><textarea class='wid80' name='reasonList'><c:out value="${result.reason}"/></textarea></td>
						<td class="line etc"><textarea class='wid80' name='etcList'><c:out value="${result.etc}"/></textarea></td>
						<td class="contr"><a href='#' class='btn_delExpMore'><img src='/template/manage/images/btn/del.gif'></a></td>
						<c:if test="${result.typeCode ne expense[status.count].typeCode}">
							<tr class='foot'><td colspan='5'><a href='#' class='btn_addExpMore'><img src='/template/manage/images/btn/btn_plus.gif'/> 계정 목 추가</a></td></tr>
						</c:if>
					</tr>
					<c:if test="${result.typeCode ne expense[status.count].typeCode}">
						</tbody>
					</c:if>
					
					<c:set var="prevCode" value="${result.typeCode}"/>
				</c:forEach>
				
				<tfoot id="box_addExp">
					<tr>
						<td colspan="6">
							<a href="#" class="btn_addExp"><img src="/template/manage/images/btn/btn_plus.gif"/> 지원비 비목 추가</a>
						</td>
					</tr>
				</tfoot>
			</table>
		</c:when>
		<c:otherwise>
			<strong>*교육과정 내용</strong>&nbsp;&nbsp;
			<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
			<table class="chart2 mb50">
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th><label>과정기간</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/>
						</td>
					</tr>
					<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
						<tr class="timeAt">
							<th><label>총 시간</label></th>
							<td><c:out value="${curriculumVO.totalTime}"/>시간</td>
							<th><label>주 횟수</label></th>
							<td><c:out value="${curriculumVO.weekNum}"/>회</td>
							<th><label>일 시간</label></th>
							<td><c:out value="${curriculumVO.dayTime}"/>시간</td>
						</tr>
					</c:if>
					<tr>
						<th><label>과정 진행 캠퍼스</label></th>
						<td colspan="3">
							<c:out value="${curriculumVO.campusNm}"/>
							&nbsp;&nbsp;
							<form:input path="campusPlace" cssClass="inp_long" required="true" placeholder="상세장소 입력"/>
						</td>
						<th><label>수업 시간표 설정</label></th>
						<td>
							<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'Y'}">캠퍼스 시간표</c:when>
								<c:otherwise>시간표 수기입력</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>주관기관/학과</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.hostCodeNm}"/>
						</td>
					</tr>
					<tr>
						<th><label>책임교원 </label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.userNm}"/>
						</td>
					</tr>
					<tr>
						<th><label>과정 개요 및 목표</label></th>
						<td colspan="5">
							<form:textarea path="crclGoal" cssClass="inp_long" cssStyle="height:100px;"/>
						</td>
					</tr>
					<tr>
						<th><label>과정의 기대효과</label></th>
						<td colspan="5">
							<form:textarea path="crclEffect" cssClass="inp_long" cssStyle="height:100px;"/>
						</td>
					</tr>
					
					<tr>
						<th><label>과정 계획서 작성기간</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.planStartDate}"/> ~ <c:out value="${curriculumVO.planEndDate}"/>
						</td>
					</tr>
					<tr>
						<th><label>수강신청기간</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
							&nbsp;&nbsp;<a href="#" class="btn_mngTxt">수강신청 결과조회</a>
						</td>
					</tr>
					<tr>
						<th><label>등록기간</label></th>
						<td colspan="5">
							<c:out value="${curriculumVO.tuitionStartDate}"/> ~ <c:out value="${curriculumVO.tuitionEndDate}"/>
						</td>
					</tr>
					<tr>
						<th><label>수강정원</label></th>
						<td colspan="5">
							<c:choose>
								<c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
								<c:otherwise>
									<c:out value="${curriculumVO.applyMaxCnt}"/>명
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>주관학과 배정여부</label></th>
						<td colspan="5">
							<c:choose>
								<c:when test="${curriculumVO.assignAt eq 'Y'}">예</c:when>
								<c:otherwise>아니오</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>등록금 납부대상과정 여부</label></th>
						<td colspan="5">
							<c:choose>
								<c:when test="${curriculumVO.tuitionAt eq 'Y'}">
									예(
										<c:if test="${not empty curriculumVO.tuitionFees and curriculumVO.tuitionFees != 0}">수업료</c:if>
										<c:if test="${not empty curriculumVO.registrationFees and curriculumVO.registrationFees != 0}">
											<c:if test="${not empty curriculumVO.tuitionFees and curriculumVO.tuitionFees != 0}">,</c:if>
											등록비
										</c:if>
									)
								</c:when>
								<c:otherwise>아니오</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th><label>등록금액</label></th>
						<td colspan="5">
							수업료 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.tuitionFees}" />원
							등록비 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.registrationFees}" />원
						</td>
					</tr>
					
				</tbody>
			</table>
			
			<strong>* 사업비(소요예산)</strong>
			<table class="chart2 mb50">
				<colgroup>
					<col width="15%"/>
					<col width="15%"/>
					<col width="10%"/>
					<col width="*"/>
					<col width="25%"/>
				</colgroup>
				<caption>등록폼</caption>
				<tbody>
					<tr>
						<th class="alC">지원비 비목</th>
						<th class="alC">계정 목</th>
						<th class="alC">금액(원)</th>
						<th class="alC">산출근거</th>
						<th class="alC">비고</th>
					</tr>
					<%-- 지원비 비목 묶어주는 역할 --%>	
					<c:set var="rowspan" value="0"/>
					<c:set var="rowspanList" value=""/>
					<c:set var="prevCode" value="${expense[0].typeCode}"/>
					<c:forEach var="list" items="${expense}" varStatus="status">
						<c:choose>
							<c:when test="${prevCode eq list.typeCode}">
								<c:set var="rowspan" value="${rowspan + 1}"/>
							</c:when>
							<c:otherwise>
								<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
								<c:set var="rowspan" value="1"/>
								<c:set var="prevCode" value="${list.typeCode}"/>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
					<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
					<c:set var="listCnt" value="${rowspan[0]}"/>
					<c:set var="rowspanCnt" value="0"/>
					
					<c:set var="prevCode" value=""/>
					<c:forEach var="result" items="${expense}" varStatus="status">
						<tr>
							<c:if test="${prevCode ne result.typeCode}">
								<td class="line" rowspan="${rowspan[rowspanCnt]}"><c:out value="${result.typeCodeNm}"/></td>
								<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
							</c:if>
							<c:set var="prevCode" value="${result.typeCode}"/>
							<td class="line"><c:out value="${result.accountCodeNm}"/></td>
							<td class="line"><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.amount}" />원</td>
							<td class="line"><c:out value="${result.reason}"/></td>
							<td><c:out value="${result.etc}"/></td>
						</tr>
					</c:forEach>
					<tr>
						<c:choose>
							<c:when test="${empty curriculumVO.totAmount or curriculumVO.totAmount eq '0' }"><td class="alC" colspan="5">무료</td></c:when>
							<c:otherwise>
								<td class="alC" colspan="2">계</td>
								<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.totAmount}" />원</td>
								<td></td>
								<td></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	
	<c:choose>
		<c:when test="${searchVO.mngAt ne 'Y'}">
			<strong>*과정 성과</strong>
			<table class="chart2 mb50">
				<caption>등록폼</caption>
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<td colspan="3">
							<ul class="outList">
								<c:forEach var="result" items="${listComCode}" varStatus="status">
									<li><label><input type="radio" name="crclOutcome" value="${result.code}" <c:if test="${result.code eq curriculumVO.crclOutcome}">checked="checked"</c:if>/>&nbsp;${result.codeNm}</label></li>
								</c:forEach>
							</ul>
						</td>	
					</tr>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<strong>*기타사항</strong>
			<table class="chart2 mb50">
				<tbody>
					<tr>
						<td>
							<textarea class="wid100 hgt50" name="etcCn"><c:out value="${curriculumVO.etcCn}"/></textarea>
						</td>	
					</tr>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
	
	<c:if test="${processSttusCode > 0}">
		<strong>*강의책임교원 배정</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="15%"/>
				<col width="*"/>
			</colgroup>
			<tbody>
				<tr>
					<td>
						<input type="text" id="searchUser2" class="wid100" placeholder="교원 명을 입력해주세요."/>
						<ul id="box_user2">
							<c:forEach var="user" items="${subUserList}" varStatus="status">
								<li>
									<input type='hidden' name='userIdList2' value='${user.userId}'/>
									<c:out value="${user.userNm}"/>(${user.mngDeptNm}_${user.emailAdres})
									<a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'/></a>
								</li>
							</c:forEach>
							<%-- 
							<c:if test="${fn:length(subUserList) == 0}">
								<li></li>
							</c:if>
							 --%>
						</ul>
					</td>	
				</tr>
			</tbody>
		</table>
		
		<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
			<strong>*교원 배정 </strong>&nbsp;&nbsp;<span class="red">수강신청 전에 필수로 입력해야 하는 항목입니다.</span>
			<table class="chart2 mb50">
				<caption>등록폼</caption>
				<colgroup>
					<col width="15%"/>
					<col width="*"/>
				</colgroup>
				<tbody>
					<tr>
						<th class="alC">시수</th>
						<th class="alC" colspan="3">교원배정</th>	
					</tr>
				</tbody>
				
				<%-- 교원배정 묶어주는 역할 --%>	
				<c:set var="rowspan" value="0"/>
				<c:set var="rowspanList" value=""/>
				<c:set var="prevCode" value="${facList[0].position}"/>
				<c:forEach var="list" items="${facList}" varStatus="status">
					<c:choose>
						<c:when test="${prevCode eq list.position}">
							<c:set var="rowspan" value="${rowspan + 1}"/>
						</c:when>
						<c:otherwise>
							<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
							<c:set var="rowspan" value="1"/>
							<c:set var="prevCode" value="${list.position}"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
				<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
				<c:set var="listCnt" value="${rowspan[0]}"/>
				<c:set var="rowspanCnt" value="0"/>
				
				<c:set var="prevCode" value=""/>
				<c:set var="number" value="0"/>
				
				<c:forEach var="result" items="${facList}" varStatus="status">
					<c:set var="number" value="${number +1}"/>
					<c:if test="${prevCode ne result.position}">
						<tbody class='box_staff'>
					</c:if>
					<tr <c:if test="${prevCode ne result.position}">class='first'</c:if>>
						<c:if test="${prevCode ne result.position}">
							<td class="line alC num" rowspan="${rowspan[rowspanCnt] + 1}"><textarea class="onlyNum timeList" name="timeList"><c:out value="${result.sisu}"/></textarea></td>
							<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
						</c:if>
						<td class="line alC num">
							<c:out value="${number}"/>
						</td>
						<td class="line">
							<c:out value="${result.facNm}"/>
							<input type="hidden" class="facIdList" name="facIdList" value="<c:out value="${result.facId}"/>">
							<input type="hidden" class="facNmList" name="facNmList" value="<c:out value="${result.facNm}"/>">
							<input type="hidden" name="positionList" value="<c:out value="${result.position}"/>">
						</td>
						<td class="line alC"><a href="#" class="btn_delStf"><img src="/template/manage/images/btn/del.gif"></a></td>
						<c:if test="${result.position ne facList[status.count].position}">
							<tr class="foot <c:out value="${result.position}"/>"><td colspan="3"><a href="#" class="btn_addStfMore" data-rand="<c:out value="${result.position}"/>"><img src="/template/manage/images/btn/btn_plus.gif"> 교원 검색 및 추가</a></td></tr>
						</c:if>
					</tr>
					<c:if test="${result.position ne facList[status.count].position}">
						<c:set var="number" value="0"/>
						</tbody>
					</c:if>
					
					<c:set var="prevCode" value="${result.position}"/>
				</c:forEach>
				
				<tfoot id="box_addTime">
					<tr>
						<td><a href="#" class="btn_addTime" colspan="4"><img src="/template/manage/images/btn/btn_plus.gif"/> 시수 추가</a></td>
					</tr>
					<tr class="box_sum">
						<td class="line alC"><span class="curTime">0</span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span></td>
						<td colspan="3">
							교원 <span class="sumUser">0</span>명
						</td>
					</tr>
				</tfoot>
			</table>
		</c:if>
		
		<strong>* 과정내용</strong>&nbsp;&nbsp;<span class="red">수강신청 화면에 과정내용으로 노출되는 정보로 단원이나 과정내용이 정확히 정해지지 않았더라도 기본적인 과정내용을  반드시 등록해주세요.</span>
		<table class="chart2 mb50 box_lesson_content">
			<colgroup>
				<col width="15%"/>
				<col width="15%"/>
				<col width="*"/>
				<col width="10%"/>
			</colgroup>
			<caption>등록폼</caption>
			<tbody>
				<tr>
					<th class="alC">번호</th>
					<th class="alC">내용범주</th>
					<th class="alC">내용</th>
					<th class="alC">관리</th>
				</tr>
			</tbody>
			
			<%-- 과정내용 번호 및 단원 묶어주는 역할 --%>	
			<c:set var="rowspan" value="0"/>
			<c:set var="rowspanList" value=""/>
			<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
			<c:forEach var="list" items="${lessonList}" varStatus="status">
				<c:choose>
					<c:when test="${prevCode eq list.lessonNm}">
						<c:set var="rowspan" value="${rowspan + 1}"/>
					</c:when>
					<c:otherwise>
						<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
						<c:set var="rowspan" value="1"/>
						<c:set var="prevCode" value="${list.lessonNm}"/>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
			<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
			<c:set var="listCnt" value="${rowspan[0]}"/>
			<c:set var="rowspanCnt" value="0"/>
			
			<c:set var="prevCode" value=""/>
			<c:set var="number" value="1"/>
			
<!-- 			<tbody class='box_lesson'>
			
			</tbody> -->
		
<%-- 			<c:forEach var="result" items="${lessonList}" varStatus="status">
				<c:if test="${prevCode ne result.lessonNm}">
					<tbody class='box_lesson'>
				</c:if>
				<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
					<c:if test="${prevCode ne result.lessonNm}">
						<td class="line alC num" rowspan="${rowspan[rowspanCnt] + 1}">
							<c:out value="${number}"/>
							<c:set var="number" value="${number + 1}"/>
						</td>
						<td rowspan='${rowspan[rowspanCnt] + 1}' class='line lesnm'><textarea class='wid100 hei100'><c:out value="${result.lessonNm}"/></textarea></td>
						<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
					</c:if>
					<td class='line'><input type='text' class='chasiList wid100' name='chasiList' value="${result.chasiNm}"/></td>
					<td class='line alC'><a href='#' class='btn_delCha'><img src='/template/manage/images/btn/del.gif'></a></td>
					<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
						<tr class='foot'><td colspan='2'><a href='#' class='btn_addChaMore'><img src='/template/manage/images/btn/btn_plus.gif'/> 과정내용 추가</a></td></tr>
					</c:if>
				</tr>
				<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
					</tbody>
				</c:if>
				
				<c:set var="prevCode" value="${result.lessonNm}"/>
			</c:forEach> --%>
			
			<tfoot id="box_addLes">
				<tr>
					<td colspan="6">
						<a href="#" class="btn_addLes"><img src="/template/manage/images/btn/btn_plus.gif"/> 내용 추가</a>
						
						<%-- 단원을 위한 div --%>
						<div id="box_lesnm" style="display:none;"></div>
					</td>
				</tr>
			</tfoot>
		</table>
		
		<strong>*성적</strong><br/>
		<%-- 
		<c:choose>
			<c:when test="${curriculumVO.evaluationAt eq 'Y' and not empty curriculumVO.gradeType}">
				절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
			</c:when>
			<c:otherwise>상대평가</c:otherwise>
		</c:choose>
		 --%>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<tbody>
				<c:choose>
					<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
						<tr>
							<th class="alC">Grade</th>
							<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
									<th class="alC"><c:out value="${result.ctgryNm}"/></th>
								</c:if>
							</c:forEach>
						</tr>
						<tr class="alC">
							<c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
							<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
							<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
							<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
							<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
							<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
							<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
							<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
							<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
							<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
							<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
							
							<td class="line alC">점수</td>
							<td class="line alC"><input type="text" value="${aplus}" class="alC" name="aplus"/></td>
							<td class="line alC"><input type="text" value="${a}" class="alC" name="a"/></td>
							<td class="line alC"><input type="text" value="${bplus}" class="alC" name="bplus"/></td>
							<td class="line alC"><input type="text" value="${b}" class="alC" name="b"/></td>
							<td class="line alC"><input type="text" value="${cplus}" class="alC" name="cplus"/></td>
							<td class="line alC"><input type="text" value="${c}" class="alC" name="c"/></td>
							<td class="line alC"><input type="text" value="${dplus}" class="alC" name="dplus"/></td>
							<td class="line alC"><input type="text" value="${d}" class="alC" name="d"/></td>
							<td class="line alC"><input type="text" value="${f}" class="alC" name="f"/></td>
						</tr>
					</c:when>
					<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
						<tr>
							<th class="alC">P/F</th>
							<th class="alC" colspan="2">점수구간</th>
						</tr>
						<tr class="alC">
							<td class="line">Pass</td>
							<td class="alC"><input type="text" value="${curriculumVO.pass}" class="alC" name="pass"/></td>
							<td class="alC">~ 100</td>
						</tr>
						<tr class="alC">
							<td class="line">Fail</td>
							<td class="alC"><input type="text" value="${curriculumVO.fail}" class="alC" name="fail"/></td>
							<td class="alC">~ 0</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<th class="alC">Grade</th>
							<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
									<th class="alC"><c:out value="${result.ctgryNm}"/></th>
								</c:if>
							</c:forEach>
						</tr>
						<tr>
							<td class="line alC">비중</td>
							<td colspan="2" class="line alC">상위 30%</td>
							<td colspan="2" class="line alC">상위 65%이내</td>
							<td colspan="5" class="line alC">66% ~ 100%</td>
						</tr>
						<%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
						<tr>
							<td class="line alC">가이드 학생 수</td>
							<td colspan="2" class="line alC">30</td>
							<td colspan="2" class="line alC">35</td>
							<td colspan="5" class="line alC">35</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<strong>*총괄평가 기준</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="40%"/>
				<col width="40%"/>
				<col width="20%"/>
			</colgroup>
			<tbody>
				<tr>
					<th class="alC">총괄평가 기준</th>
					<th class="alC">성적 반영률(%)</th>
					<th class="alC">관리</th>	
				</tr>
			</tbody>
			<tbody class='box_evt'>
				<%-- 총괄평가 등록되어 있는지 확인 및 반영률 체크--%>
				<c:set var="totper" value="0"/>
				<c:choose>
					<c:when test="${not empty evaluationList or fn:length(evaluationList) > 0}">
						<c:forEach var="result" items="${evaluationList}" varStatus="status">
							<tr class="<c:out value="${result.evtId}"/>">
								<input type='hidden' name='evtIdList' value="${result.evtId}"/>
								<input type='hidden' name='evtStandList' value="${empty result.evtStand ? '점수' : result.evtStand}"/>
								<td class="alC">
									<c:choose>
										<c:when test="${result.evtId eq 'CTG_0000000000000119'}">
											<input type="text" class="wid80" name="evtNmList" value="${result.evtNm}" placeholder="기타"/>
										</c:when>
										<c:otherwise>
											<c:out value="${result.evtNm}"/>
											<input type='hidden' name="evtNmList" value="${result.evtNm}"/>
										</c:otherwise>
									</c:choose>
								</td>
								<c:choose>
									<c:when test="${result.evtId eq 'CTG_0000000000000115'}">
										<td>
											<input type="number" class="evtper onlyNum" name="evtValList" value="${result.evtVal}"/>%
											<select name="evtType">
												<c:forEach var="ctgry" items="${homeworkComCode}">
													<option value="${ctgry.code}" <c:if test="${ctgry.code eq result.evtType}">selected="selected"</c:if>><c:out value="${ctgry.codeNm}"/></option>
												</c:forEach>
											</select>
										</td>
									</c:when>
									<c:when test="${result.evtId eq 'CTG_0000000000000117'}">
										<td>
											<input type="number" class="evtper onlyNum" name="evtValList" value="${result.evtVal}"/>%
											<a href="#" class="btn_mngTxt btn_bbs" data-id="${curriculumVO.crclId}">게시판 선택</a>
											<ul id="box_bbsmaster">
												<c:forEach var="bbsList" items="${attendBbsList}">
													<li>
														<c:out value="${bbsList.colect}"/>회-
														(
														<c:choose>
															<c:when test="${bbsList.sysTyCode eq 'ALL'}">전체</c:when>
															<c:when test="${bbsList.sysTyCode eq 'GROUP'}">조별</c:when>
															<c:when test="${bbsList.sysTyCode eq 'CLASS'}">반별</c:when>
														</c:choose>
														)
														<c:out value="${bbsList.bbsNm}"/>
														<input type="hidden" name="bbsIdList" value="${bbsList.bbsId}"/>
														<input type="hidden" name="colectList" value="${bbsList.colect}"/>
														<a href='#' class='btn_delbbs'><img src='/template/manage/images/btn/del.gif'></a>
													</li>
												</c:forEach>
											</ul>
										</td>
									</c:when>
									<c:otherwise>
										<td>
											<input type='number' class='wid80 evtper onlyNum' name='evtValList' value="${result.evtVal}"/>%
										</td>
									</c:otherwise>
								</c:choose>
								<td class='line alC'><a href='#' class='btn_delEvt'><img src='/template/manage/images/btn/del.gif'></a></td>
							</tr>
							<c:set var="totper" value="${totper  + result.evtVal}"/>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach var="result" items="${evaluationBaseList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '2'}">
								<tr class="<c:out value="${result.ctgryId}"/>">
									<input type='hidden' name='evtIdList' value="${result.ctgryId}"/>
									<input type='hidden' name='evtStandList' value="${empty result.ctgryVal ? '점수' : result.ctgryVal}"/>
									
									<td class="alC">
										<c:choose>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000119'}">
												<input type="text" class="wid80" name="evtNmList" placeholder="기타"/>
											</c:when>
											<c:otherwise>
												<c:out value="${result.ctgryNm}"/>
												<input type='hidden' name="evtNmList" value="${result.ctgryNm}"/>
											</c:otherwise>
										</c:choose>
									</td>
									<c:choose>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000115'}">
											<td>
												<input type="number" class="evtper onlyNum" name="evtValList"/>%
												<select name="evtType">
													<c:forEach var="result" items="${homeworkComCode}">
														<option value="${result.code}" ><c:out value="${result.codeNm}"/></option>
													</c:forEach>
												</select>
											</td>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000117'}">
											<td>
												<input type="number" class="evtper onlyNum" name="evtValList"/>%
												<a href="#" class="btn_mngTxt btn_bbs" data-id="${curriculumVO.crclId}">게시판 선택</a>
												<ul id="box_bbsmaster"></ul>
											</td>
										</c:when>
										<c:otherwise>
											<td>
												<input type='number' class='wid80 evtper onlyNum' name='evtValList'/>%
											</td>
										</c:otherwise>
									</c:choose>
									<td class='line alC'><a href='#' class='btn_delEvt'><img src='/template/manage/images/btn/del.gif'></a></td>
								</tr>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
			<tfoot id="box_addTime">
				<tr class="box_sum">
					<td colspan="3" class="alC">총 성적 반영률 <span class="totper"><c:out value="${totper}"/></span> / 100</td>
				</tr>
				<tr>
					<td><a href="#" class="btn_addEvt" colspan="3"><img src="/template/manage/images/btn/btn_plus.gif"/> 총괄평가 기준 추가</a></td>
				</tr>
			</tfoot>
		</table>
		
		<strong>*수료 기준(선택 입력)</strong>&nbsp;&nbsp;
		<span class="red">별도 수료 기준이 없다면 등록하지 않으셔도 됩니다.</span>
		<table class="chart2">
			<caption>등록폼</caption>
			<colgroup>
				<col width="20%"/>
				<col width="40%"/>
				<col width="40%"/>
			</colgroup>
			<tbody>
				<tr>
					<th class="alC">수료 기준</th>
					<th class="alC">수료 기준 선택및 점수, % 상세 입력(%)</th>
					<th class="alC">설명</th>	
				</tr>
				<c:forEach var="result" items="${finishList}">
					<c:if test="${result.ctgryLevel eq 1}">
						<tr>
							<td class="alC"><c:out value="${result.ctgryNm}"/></td>
							<td class="alC">
								<c:choose>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000084'}">
										<label><input type="radio" class="radiocheck" name="fnTotAt" value="N" <c:if test="${empty curriculumVO.fnTot}">checked="checked"</c:if>/> 해당 없음</label>
										<label><input type="radio" class="radiocheck" name="fnTotAt" value="Y" <c:if test="${not empty curriculumVO.fnTot}">checked="checked"</c:if>/> 대상</label>
										<input type="number" class="inpTxt onlyNum" name="fnTot" value="${curriculumVO.fnTot}" <c:if test="${empty curriculumVO.fnTot}">readonly="readonly"</c:if>/> 점 이상
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000085'}">
										<label><input type="radio" class="radiocheck" name="fnAttendAt" value="N" <c:if test="${empty curriculumVO.fnAttend}">checked="checked"</c:if>/> 해당 없음</label>
										<label><input type="radio" class="radiocheck" name="fnAttendAt" value="Y" <c:if test="${not empty curriculumVO.fnAttend}">checked="checked"</c:if>/> 대상</label>
										<input type="number" class="inpTxt onlyNum" name="fnAttend" value="${curriculumVO.fnAttend}" <c:if test="${empty curriculumVO.fnAttend}">readonly="readonly"</c:if>/> % 이상
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000086'}">
										<label><input type="radio" class="radiocheck" name="fnGradeTotAt" value="N" <c:if test="${empty curriculumVO.fnGradeTot}">checked="checked"</c:if>/> 해당 없음</label>
										<label><input type="radio" class="radiocheck" name="fnGradeTotAt" value="Y" <c:if test="${not empty curriculumVO.fnGradeTot}">checked="checked"</c:if>/> 대상</label>
										<input type="number" class="inpTxt onlyNum" name="fnGradeTot" value="${curriculumVO.fnGradeTot}" <c:if test="${empty curriculumVO.fnGradeTot}">readonly="readonly"</c:if>/> 점 이상
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000087'}">
										<label><input type="radio" class="radiocheck" name="fnGradeFailAt" value="N" <c:if test="${empty curriculumVO.fnGradeFail}">checked="checked"</c:if>/> 해당 없음</label>
										<label><input type="radio" class="radiocheck" name="fnGradeFailAt" value="Y" <c:if test="${not empty curriculumVO.fnGradeFail}">checked="checked"</c:if>/> 대상</label>
										<input type="number" class="inpTxt onlyNum" name="fnGradeFail" value="${curriculumVO.fnGradeFail}" <c:if test="${empty curriculumVO.fnGradeFail}">readonly="readonly"</c:if>/> 점 이상
									</c:when>
									<c:when test="${result.ctgryId eq 'CTG_0000000000000088'}">
										<label><input type="radio" class="radiocheck" name="fnHomeworkAt" value="N" <c:if test="${empty curriculumVO.fnHomework}">checked="checked"</c:if>/> 해당 없음</label>
										<label><input type="radio" class="radiocheck" name="fnHomeworkAt" value="Y" <c:if test="${not empty curriculumVO.fnHomework}">checked="checked"</c:if>/> 대상</label>
										<input type="number" class="inpTxt onlyNum" name="fnHomework" value="${curriculumVO.fnHomework}" <c:if test="${empty curriculumVO.fnHomework}">readonly="readonly"</c:if>/> 점 이상
									</c:when>
								</c:choose>
							</td>
							<td><c:out value="${result.ctgryCn}"/></td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<span class="red">[안내] 성적이 Letter Grade로 설정된 경우, F인 사람은 수료 기준을 만족해도 수료로 체크되지 않음</span>
		<br/><br/><br/>
		<strong>*교재 및 부교재</strong>
		<table class="chart2 mb50">
			<caption>등록폼</caption>
			<colgroup>
				<col width="10%"/>
				<col width="35%"/>
				<col width="35%"/>
				<col width="20%"/>
			</colgroup>
			<tbody class="list_book">
				<tr>
					<th class="alC">NO</th>
					<th class="alC">교재명</th>
					<th class="alC">출판사</th>
					<th class="alC">관리</th>	
				</tr>
				<c:forEach var="result" items="${bookList}" varStatus="status">
					<tr>
						<td class="num alC"><c:out value="${status.count}"/></td>
						<td class="alC">
							<c:out value="${result.nttSj}"/>
							<input type="hidden" class="nttNoList" name="nttNoList" value="${result.nttNo}"/>
						</td>
						<td class="alC"><c:out value="${result.tmp01}"/></td>
						<td class="alC"><a href='#' class='btn_delBook'><img src='/template/manage/images/btn/del.gif'></a></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(bookList) == 0}">
					<tr class="empty"><td colspan="4" class="alC">등록된 교재 및 부교재가 없습니다.</td></tr>
				</c:if>
			</tbody>
			<tfoot>
				<tr>
					<td class="alC" colspan="4"><a href="#" class="btn_addBook" data-lang="${curriculumVO.crclLang}"><img src="/template/manage/images/btn/btn_plus.gif"/> 교재 검색 추가</a></td>
				</tr>
				<tr>
					<td colspan="4">
						<textarea class="wid100 hgt100" name="bookEtcText"><c:out value="${curriculumVO.bookEtcText}"/></textarea>
					</td>
				</tr>
			</tfoot>
		</table>
		
		<strong>*자체 운영 계획 및 규정</strong>
		<textarea class="wid100 hgt100" name="managePlan"><c:out value="${curriculumVO.managePlan}"/></textarea>
	</c:if>
	
	<div class="btn_r">
		<input type="image" src="${_IMG}/btn/${_MODE eq 'REG' ? 'btn_regist.gif' : 'btn_modify.gif' }"/>
	    <c:url var="listUrl" value="/mng/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
	    <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
	</div>
</form:form>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>