<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<script>
$(document).ready(function(){
	//조 수량 저장
	$(document).on("click", ".group_set_btn", function(){
		var setGroupTr      = 0;
		var groupCnt        = parseInt($(".group_cnt").val());
		var html            = "";
		var htmlFlag        = "";
		var forFirstCnt     = 1;
		var arrRemoveUserId = new Array();
		
		if(fn_group_check() == false) {
			return false;
		}

		//기존에 있는 tr 개수 설정
		$(".tr_student_list").each(function(){
			setGroupTr++;
		});

		//설정 된 조개수와 수정할 조 개수가 같으면 false 처리
		if(setGroupTr == groupCnt) {
			return false;
		}

		//설정 된 조개수가 없으면 새로 생성
		if(setGroupTr == 0) {
			htmlFlag = "html";
		
		//설정 된 조개수 보다 수정할 조 개수가 많으면 기존 학생을 유지하고 append 처리함
		} else if(setGroupTr < groupCnt) {
			htmlFlag = "append";
			forFirstCnt = setGroupTr + 1;
		
		//설정 된 조개수 보다 수정할 조 개수가 적으면 기존 학생을 제거
		} else if(setGroupTr > groupCnt) {
			$(".tbl_student_pick tr").each(function(){
				if($(this).find("input[name=group_cnt]").val() > groupCnt) {
					//삭제되는 유저 배열에 담기
					$(this).find(".div_student_box").each(function(index) {
						arrRemoveUserId[index] = $(this).data("userid");
					})

					//tr 삭제
					$(this).remove();
				}
			});
		
			//삭제된 유저 좌측 메뉴 활설화 하기
			for(var i=0;i<arrRemoveUserId.length;i++) {
				$(".tr_student").each(function(){
					if($(this).data("userid") == arrRemoveUserId[i]) {
						$(this).removeClass("pick_complate");
						$(this).data("pickflag", "false");
					}
				});
			}
		}
		
		for(var i=forFirstCnt;i<=$(".group_cnt").val();i++) {
			html += "<tr class='tr_student_list'>";
			html += "	<td><input type='radio' name='group_cnt' value='" + i + "'/> " + i + "조</td>";
			html += "	<td class='td_student_list'></td>";
			html += "</tr>";
		}
		
		if(htmlFlag == "append") {
			$(".tbl_student_pick tbody").append(html);
		} else if(htmlFlag == "html") {
			$(".tbl_student_pick tr").remove();
			$(".tbl_student_pick tbody").html(html);
		}
	});

	//학생 선택
	$(document).on("click", ".tr_student", function(){
		var el_group_cnt    = $('input:radio[name=group_cnt]:checked');
		var html            = "";
		var groupLeaderFlag = false;
		var groupRadioTd    = $('input:radio[name=group_cnt]:checked').parent().next(); //학생 입력 td 태그

		if(fn_group_check() == false) {
			return false;
		}

		if(el_group_cnt.is(':checked') == false) {
			alert("조를 선택한 후에 학생을 선택해주세요.")
			return false;
		}

		if($(this).data("pickflag").toString() == "true") {
			alert("이미 선택된 학생입니다.")
			return false;
		}
			
		//조 선택 결과 출력
		html += "<div class='div_student_box' data-userid='" + $(this).data("userid") + "'>";
		html += "    <input type='hidden' name='userIdList' value='" + $(this).data("userid") + "'>";
		html += "    <input type='hidden' name='groupCntList' value='" + el_group_cnt.val() + "'>";
		html += "    <input type='hidden' name='groupLeaderAtList' value='' />";
		html += "    <input type='checkbox' name='groupLeaderCheckbox' />";
		html += "    <span>" + $(this).data("usernm") + " | </span>";
		html += "    <a href='#none' class='student_delete_btn'>X</a>";
		html += "</div>";

		groupRadioTd.append(html);
		
		$(this).data("pickflag", "true")
		$(this).addClass("pick_complate");
		
		//조장이 없으면 첫번째 radio checked 처리
		groupRadioTd.find("input[name=groupLeaderCheckbox]").each(function(){
			if($(this).is(':checked') == true) {
				groupLeaderFlag = true;
				return false;
			}
		});
		
		if(groupLeaderFlag == false) {
			groupRadioTd.find("input[name=groupLeaderAtList]").first().val("Y");
			groupRadioTd.find("input[name=groupLeaderCheckbox]").first().prop('checked', true);
		}
	});
	
	//학생 삭제
	$(document).on("click", ".student_delete_btn", function(){
		var userId = $(this).parent().data("userid");
		
		$(this).parent().remove();

		$(".tr_student").each(function(){
			if($(this).data("userid") == userId) {
				$(this).removeClass("pick_complate");
				$(this).data("pickflag", "false");
			}
		});
	});
	
	//조배정 확정
	$(document).on("click", ".group_complate_btn", function(){
		$("#listForm").submit();
	});
	
	//학생 검색
	$(document).on("click", ".student_search_btn", function(){
		var userNm = $("input[name=searchUserNm]").val();
		
		$(".tr_student").hide();
		
		$(".tr_student").each(function(){
			if($(this).data("usernm").indexOf(userNm) != -1) {
				$(this).show();
			}
		});
	});
	
	//학생 검색 조건 초기화
	$(document).on("click", ".search_reset", function(){
		$("input[name=searchUserNm]").val("");
		$(".tr_student").show();
	});
	
	//조장 선택 클릭 이벤트
	$(document).on("click", "input[name=groupLeaderCheckbox]", function(){
		var checkboxPrent = $(this).parent().parent();
		
		//check box 초기화
		checkboxPrent.find("input[name*=groupLeaderAtList]").val("");
		checkboxPrent.find("input[name*=groupLeaderCheckbox]").prop("checked", false);

		//check box 선택
		$(this).prevAll("input[name=groupLeaderAtList]").val("Y");
		$(this).prop("checked", true);
	});
});

//조 수량 체크 함수
function fn_group_check() {
	var groupCnt = $(".group_cnt").val();
	var flag = true;
	
	if(groupCnt =="" || groupCnt == "0") {
		alert("조 수량을 설정해 주세요.")
		flag = false;
	}
	return flag;
}
</script>
<table class="chart2 mb50">
	<tr>
		<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
	</tr>
	<tr>
		<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
		<td class="alC">
			<ul>
				<c:forEach var="user" items="${subUserList}" varStatus="status">
					<li>
						<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
					</li>
				</c:forEach>
			</ul>
		</td>
		<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
	</tr>
</table>

<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="4"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<c:import url="/mng/lms/curriculumStudenttabmenu.do" charEncoding="utf-8">
	<c:param name="step" value="2"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:param name="crclbId" value="${curriculumVO.crclbId}"/>
</c:import>

<ul class="group_list">
	<c:forEach var="group" items="${selectGroupList}" varStatus="status">
		<c:choose>
			<c:when test="${group.groupCnt eq 0}"><c:set var="groupCntTab" value="조없음"/></c:when>
			<c:otherwise><c:set var="groupCntTab" value="${group.groupCnt}개조"/></c:otherwise>
		</c:choose>

		<li class="<c:if test="${group.classCnt eq param.classCnt }">act</c:if>">
			<a href="/mng/lms/crm/curriculumGroupView.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}&crclbId=${curriculumVO.crclbId}&classCnt=${group.classCnt }">${group.classCnt }반(${groupCntTab })</a>
		</li>

		<c:if test="${!status.last }">
			<li class="partition"> | </li>
		</c:if>
	</c:forEach>
</ul>

<div class="student_search">
  	<div style="height:70px;">
  		<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
		<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>

		<input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" class="inp_s text_box" id="inp_text" placeholder="학생 이름">
		<input type=image src="${_IMG}/btn/btn_search.gif" class="student_search_btn" alt="검색" />
		<a href="#none" class="btn_mngTxt search_reset" style="width:190px;text-align:center;">검색 초기화</a>
	</div>
	
	<table class="chart2 mb50">
		<tbody>
			<tr>
				<th class="alC">소속</th>
				<th class="alC">이름</th>
			</tr>
			<c:forEach var="student" items="${selectStudentList}" varStatus="status">
				<c:choose>
					<c:when test="${empty student.groupCnt  }"><c:set var="pickFlag" value="false"/></c:when>
					<c:otherwise><c:set var="pickFlag" value="true"/></c:otherwise>
				</c:choose>
				<tr class="alC tr_student <c:if test="${!empty student.groupCnt }">pick_complate</c:if>" data-depnm="${student.mngDeptNm }" data-usernm="${student.userNm }" data-userid="${student.userId }" data-pickflag="${pickFlag }">
					<td class="line alC">${student.mngDeptNm }</td>
					<td class="line alC">${student.userNm }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>


<div class="student_list">
	<div style="height:auto;">
		<c:choose>
			<c:when test="${groupCnt eq 0 }"><c:set var="pickGroupCnt" value=""/></c:when>
			<c:otherwise><c:set var="pickGroupCnt" value="${groupCnt }"/></c:otherwise>
		</c:choose>
		<input type="text" class="inp_s onlyNum group_cnt" id="inp_text" placeholder="조 수량" value="${fn:length(groupCntList) }">
		<a href="#none" class="btn_mngTxt group_set_btn">조 수량 저장</a>
		<br/>
		<div style="padding:10px 0 10px 0;"><strong style="color:red;">학생 선택 후 체크박스를 선택하시면 조장으로 선택됩니다.</strong></div>
	</div>
	<div class="student_pick_list">
		<form id="listForm" method="post" action="/mng/lms/crm/updateCurriculumGroup.do">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
			<input type="hidden" name="classCnt" value="${param.classCnt}"/>

			<table class="tbl_student_pick">
				<colgroup>
					<col width='80px' />
					<col width='*' />
				</colgroup>
				<tbody>
					<c:forEach var="groupCnt" items="${groupCntList}" varStatus="status">
						<tr class="tr_student_list">
							<td><input type="radio" name="group_cnt" value="${groupCnt.groupCnt }"> ${groupCnt.groupCnt }조</td>
							<td class="td_student_list">
								<c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
									<c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
										<div class="div_student_box" data-userid="${pickStudent.userId }">
											<input type="hidden" name="userIdList" value="${pickStudent.userId }" />
											<input type="hidden" name="groupCntList" value="${pickStudent.groupCnt }" />
											<input type="hidden" name="groupLeaderAtList" value="<c:if test="${pickStudent.groupLeaderAt eq 'Y'}">Y</c:if>" />
											<input type="checkbox" name="groupLeaderCheckbox" <c:if test="${pickStudent.groupLeaderAt eq 'Y'}">checked</c:if> />
											<span>${pickStudent.userNm } | </span>
											<a href="#none" class="student_delete_btn">X</a>
										</div>
									</c:if>
								</c:forEach>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</div>

<div class="btn_c">
	<c:choose>
	 	<c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
	 	</c:when>
		<c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4}">
			<a href="#none" class="btn_mngTxt group_complate_btn">조배정 확정</a>
		</c:when>
	</c:choose>

    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>