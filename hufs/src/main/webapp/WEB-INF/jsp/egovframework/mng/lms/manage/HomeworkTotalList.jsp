<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CLASS_MANAGE"/>
    <c:param name="depth1" value="BASE_CRCL_MNG"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과제"/>
</c:import>

<script>
$(document).ready(function(){
	//검색 초기화
    $(document).on("click", "#resetBtn", function() {
        $("form[name='frm']").find("input").not("#resetBtn").val("");
        $("form[name='frm'] select").val("").trigger("change");
        $("form[name='frm'] input").prop("checked", false);
    });
	
    $("#searchStartCloseDate, #searchEndCloseDate").datepicker({
        dateFormat: "yy-mm-dd"
    });
});

</script>
<div id="cntnts">
    <div id="bbs_search">
        <form id="frm" name="frm" method="post" action="<c:url value="/mng/lms/manage/homeworkTotalList.do"/>">
            <input type="text" name="searchStartCloseDate" value="${searchVO.searchStartCloseDate}" id="searchStartCloseDate" class="btn_calendar" placeholder="과제 마감일" readonly="readonly"/> ~
            <input type="text" name="searchEndCloseDate" value="${searchVO.searchEndCloseDate}" id="searchEndCloseDate" class="btn_calendar" placeholder="과제 마감일" readonly="readonly"/>

            <label><input type="checkbox" id="searchCloseHomework" name="searchCloseHomework" value="Y" ${searchVO.searchCloseHomework eq 'Y' ? 'checked' : '' }/>마감된 과제만 조회</label>&nbsp;&nbsp;
            <br />

            <strong>주관기관 : </strong>
            <select id="searchHostCode" name="searchHostCode">
                <option value="">전체</option>
                <c:forEach var="result" items="${ctgryList}" varStatus="status">
                    <c:if test="${result.ctgryLevel eq '1' }">
                        <option value="${result.ctgryId}" ${searchVO.searchHostCode eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
                    </c:if>
                </c:forEach>
            </select>

            <strong>과정유형 : </strong>
            <select id="searchHwType" name="searchHwType">
                <option value="">과제유형 전체</option>
                <option value="1" <c:if test="${searchVO.searchHwType eq '1'}">selected</c:if>>개별</option>
                <option value="2" <c:if test="${searchVO.searchHwType eq '2'}">selected</c:if>>조별</option>
            </select>

            <strong>과제유형 : </strong>
            <select id="searchHwCode" name="searchHwCode">
                <option value="">과제 전체</option>
                <option value="1" <c:if test="${searchVO.searchHwCode eq '1'}">selected</c:if>>과정과제</option>
                <option value="2" <c:if test="${searchVO.searchHwCode eq '2'}">selected</c:if>>수업과제</option>
            </select>
            <br />

            <label><strong>과정명 : </strong> <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm }" class="inp_s" id="searchCrclNm" /></label>
            <label><strong>과제명 : </strong> <input type="text" name="searchHomeworkNm" value="${searchVO.searchHomeworkNm }" class="inp_s" id="searchHomeworkNm" /></label>

            <br/>
            <input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
            <input type="button" id="resetBtn" value="초기화" style="width:73px;height:26px;"/>
            
        </form>
    </div>

    <table class="chart_board">
        <thead>
          <tr>
              <th>No</th>
              <th>과제마감일</th>
              <th colspan='2'>평가구분</th>
              <th>과정명</th>
              <th>과제명</th>
              <th>교원</th>
              <th>제출</th>
          </tr> 
        </thead>
        <tbody>
            <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                <c:choose>
                    <c:when test="${result.hwCode eq '1' }">
                        <c:url var="viewUrl" value="/mng/lms/crcl/selectHomeworkArticle.do">
                            <c:param name="hwId" value="${result.hwId }" />
                            <c:param name="hwsId" value="${result.hwsId }" />
                            <c:param name="crclId" value="${result.crclId }" />
                            <c:param name="menu" value="CLASS_MANAGE" />
                            <c:param name="menuFlag" value="lessonHomework" />
                        </c:url>
                    </c:when>
                    <c:otherwise>
                        <c:url var="viewUrl" value="/mng/lms/crcl/selectHomeworkArticle.do">
                            <c:param name="hwId" value="${result.hwId }" />
                            <c:param name="crclId" value="${result.crclId }" />
                            <c:param name="plId" value="${result.plId }" />
                            <c:param name="menu" value="CLASS_MANAGE" />
                        </c:url>
                    </c:otherwise>
                </c:choose>
                <tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
                  <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
                  <td>${result.closeDate}</td>
                  <td>${result.hwCodeNm}</td>
                  <td>${result.hwTypeNm}</td>
                  <td class='left-align'>${result.crclNm }</td>
                  <td class='left-align'>${result.nttSj }</td>
                  <td>${result.userNm }</td>
                  <td>
                      <c:choose>
                          <c:when test="${result.homeworkSubmitCnt eq result.memberCnt && result.homeworkSubmitCnt ne '0' && result.memberCnt ne '0' }">전원제출</c:when>
                          <c:otherwise>${result.homeworkSubmitCnt}/${result.memberCnt }</c:otherwise>
                      </c:choose>
                  </td>
                </tr>
            </c:forEach>
            <c:if test="${fn:length(selectHomeworkList) == 0}">
                <tr>
                    <td class="listCenter" colspan="8"><spring:message code="common.nodata.msg" /></td>
                </tr>
            </c:if>
          </tbody>
    </table>
    
    <div id="paging">
        <c:url var="pageUrl" value="/mng/lms/manage/homeworkTotalList.do?${_BASE_PARAM}">
        </c:url>
    
        <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
    </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>