<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_EDITOR_ID" value="nttCn"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="hwId" value="${searchVO.hwId}"/>
    <c:param name="menuFlag" value="${param.menuFlag}"/>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
    <c:when test="${param.menuFlag eq 'lessonHomework' }">
        <c:set var="depth1" value="BASE_CRCL_MNG"/>
        <c:set var="title" value="과제"/>
    </c:when>
    <c:when test="${param.menu eq 'CLASS_MANAGE' }">
        <c:set var="depth1" value="CURRICULUM_STUDY"/>
        <c:set var="title" value="과정별 수업"/>
    </c:when>
    <c:otherwise>
        <c:set var="depth1" value="BASE_CRCL"/>
        <c:set var="title" value="과정등록관리"/>
    </c:otherwise>
</c:choose>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="${param.menu }"/>
    <c:param name="depth1" value="${depth1 }"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="${title }"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
$(function() {
    $("#openDate, #closeDate").datepicker({
        dateFormat: "yy-mm-dd"
    });
});

function fn_egov_regist() {
    
    tinyMCE.triggerSave();
    
    if($.trim($('#${_EDITOR_ID}').val()) == "") {
        alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
        tinyMCE.activeEditor.focus();
        return false;
    }
    
    if($("select[name=hwType] option:selected").val() == "") {
        alert("과제유형을 선택해 주세요.");
        return false;
    }
    
    var openDate  = $("input[name=openDate]").val();
    var closeDate = $("input[name=closeDate]").val();

    if(openDate == "" || closeDate == "") {
        alert("시작날짜 또는 종료날짜를 입력해주세요.");
        return false;
    }

    if(Number(openDate.replace(/-/gi, "")) > Number(closeDate.replace(/-/gi, ""))) {
        alert("종료일자가 시작일자보다 작을 수 없습니다.");
        return false;
    }
    
    var timeFormat = /^([01][0-9]|2[0-3]):([0-5][0-9])$/; // 시간형식 체크 정규화 hh:mm
    if(!timeFormat.test($("input[name=openTime]").val()) || !timeFormat.test($("input[name=closeTime]").val())) {
        alert("시간 형식이 잘못 되었습니다.");
        return false;
    }

    $('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

    <c:choose>
        <c:when test="${searchVO.registAction eq 'updt'}">
            if (!confirm('<spring:message code="common.update.msg" />')) {
                 return false
            }
        </c:when>
        <c:otherwise>
            if (!confirm('<spring:message code="common.regist.msg" />')) {
                return false;
            }
        </c:otherwise>
    </c:choose>
}

$(document).ready(function(){
	var adfile_config = {
            siteId:"SITE_000000000000001",
            pathKey:"Homework",
            appendPath:"HOMEWORK",
            editorId:"${_EDITOR_ID}",
            fileAtchPosblAt:"Y",
            maxMegaFileSize:100,
            atchFileId:"${homeworkVO.atchFileId}"
        };
        
    fnCtgryInit('');
    fn_egov_bbs_editor(adfile_config);
    
    //글등록
    $(document).on("click", ".insertHomework, .updateHomework", function() {
        $('#curriculum').submit();
    });
});
</script>
<c:if test="${param.menuFlag ne 'lessonHomework' and param.menu ne 'CLASS_MANAGE'}">
	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm}"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
</c:if>
<c:if test="${curriculumVO.processSttusCode > 0 and param.menuFlag ne 'lessonHomework'}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCode" value="${processSttusCode}"/>
        <c:param name="step" value="${param.menu eq 'CURRICULUM_MANAGE' ? '8' : '5'}"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
    <table class="chart2 mb50">
        <tbody>
            <tr>
                <td class="alC">
                    <ul>
                        <c:forEach var="user" items="${subUserList}" varStatus="status">
                            <li style="width:180px;float:left;padding-right:15px;">
                                <p class="title">담당교수 <b><c:out value="${user.userNm}"/>(<c:out value="${user.mngDeptNm}"/>)</b></p>
                                <p class="sub-title">문의: ${user.emailAdres }</p>
                            </li>
                        </c:forEach>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    
    <strong>
        <c:choose>
            <c:when test="${param.mode eq 'curriculum' and param.registAction eq 'regist' }">
                <c:set var="menuTitle" value="과정과제 등록하기"/>
            </c:when>
            <c:when test="${param.mode eq 'curriculum' and param.registAction eq 'updt' }">
                <c:set var="menuTitle" value="과정과제 수정하기"/>
            </c:when>
            <c:when test="${param.mode eq 'lesson' and param.registAction eq 'regist' }">
                <c:set var="menuTitle" value="수업과제 등록하기"/>
            </c:when>
            <c:when test="${param.mode eq 'lesson' and param.registAction eq 'updt' }">
                <c:set var="menuTitle" value="수업과제 수정하기"/>
            </c:when>
        </c:choose>

        <c:out value="${menuTitle }" />
    </strong>
    
    <c:choose>
        <c:when test="${empty homeworkVO.hwId }"><c:set var="formUrl" value="/mng/lms/crcl/insertHomeworkArticle.do"/></c:when>
        <c:otherwise><c:set var="formUrl" value="/mng/lms/crcl/updateHomeworkArticle.do"/></c:otherwise>
    </c:choose>
    <form:form commandName="curriculum" name="board" method="post" action="${formUrl }" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
        <input id="hwId" name="hwId" type="hidden" value="${homeworkVO.hwId}">
        <input id="atchFileId" name="atchFileId" type="hidden" value="${homeworkVO.atchFileId}">
        <input id="fileGroupId" name="fileGroupId" type="hidden" value="${homeworkVO.atchFileId}">
        <input type="hidden" name="registAction" value="<c:out value='${param.registAction}'/>"/>
        <input type="hidden" name="plId" value="${param.plId}"/>
        <input type="hidden" name="menuFlag" value="${param.menuFlag}"/>
        <input type="hidden" name="menu" value="${param.menu}"/>

        <%-- 과제구분(hwCode) : 1 = 과정과제, 2 = 수업과제 --%>
        <c:choose>
            <c:when test="${param.mode eq 'curriculum' }"><c:set var="hwCode" value="1"/></c:when>
            <c:otherwise>
                <c:set var="hwCode" value="2"/>
                <input type="hidden" name="searchHwCode" value="${hwCode }" />
            </c:otherwise>
        </c:choose>
        <input type="hidden" name="crclId" value="${param.crclId }" />
        <input type="hidden" name="hwCode" value="${hwCode }" />

        <table class="chart2">
            <colgroup>
                <col width="15%">
                <col width="*">
            </colgroup>
            <tbody>
                <tr>
                    <th><label>과제명</label></th>
                    <td><input type="text" class="table-input inp_long" name="nttSj" value="${homeworkVO.nttSj }" placeholder="과제명을 입력해주세요."></td>
                </tr>
                <tr>
                    <th><label>과제유형</label></th>
                    <td>
                        <select class="table-select select2 " name="hwType" data-select="style1">
	                        <c:choose>
	                            <c:when test="${param.registAction eq 'updt' and homeworkVO.hwType eq '1'}"><option value="1">개별과제</option></c:when>
	                            <c:when test="${param.registAction eq 'updt' and homeworkVO.hwType eq '2'}"><option value="2">조별과제</option></c:when>
	                            <c:otherwise>
	                                <option value="1">개별과제</option>
	                                <%-- 조가 있을 때에만 조별과제가 등록 되도록 처리 --%>
	                                <c:if test="${0 < groupCnt }">
	                                    <option value="2">조별과제</option>
	                                </c:if>
	                            </c:otherwise>
	                        </c:choose>
	                    </select>
	                    <span>
	                        <label class="checkbox">
	                            <input type="checkbox" name="curriculumCommentAt" value="Y" class="table-checkbox"  <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">checked</c:if> />
	                            <span class="custom-checked"></span>
	                            <span class="text">과정후기</span>
	                        </label>
	                    </span>
                    </td>
                </tr>
                <tr>
                    <th><label>과제 오픈일</label></th>
                    <td>
                        <input type="text" class="btn_calendar" name="openDate" id="openDate" value="${homeworkVO.openDate }" required="true" readonly="readonly" placeholder="오픈일">
                        <input type="text" class="table-input" name="openTime" value="${fn:substring(homeworkVO.openTime,0,2) }<c:if test="${!empty homeworkVO.openTime }">:</c:if>${fn:substring(homeworkVO.openTime,2,4) }" placeholder="00:00">
                    </td>
                </tr>
                <tr>
                    <th><label>과제 마감일</label></th>
                    <td>
                        <input type="text" class="btn_calendar" name="closeDate" id="closeDate" value="${homeworkVO.closeDate }" required="true" readonly="readonly" placeholder="마감일">
                        <input type="text" class="table-input" name="closeTime" value="${fn:substring(homeworkVO.closeTime,0,2) }<c:if test="${!empty homeworkVO.closeTime }">:</c:if>${fn:substring(homeworkVO.closeTime,2,4) }" placeholder="00:00">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><textarea id="nttCn" name="nttCn" rows="20" class="cont">${homeworkVO.nttCn}</textarea></td>
                </tr>
                <tr>
                    <th><spring:message code="cop.atchFile" /></th>
                    <td>
                        <c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
                            <c:param name="editorId" value="${_EDITOR_ID}"/>
                            <c:param name="estnAt" value="N" />
                            <c:param name="param_atchFileId" value="${homeworkVO.atchFileId}" />
                            <c:param name="imagePath" value="${_IMG }"/>
                            <c:param name="mngAt" value="Y"/>
                        </c:import>
                        <noscript>
                            <input name="file_1" id="egovComFileUploader" type="file" class="inp" />
                        </noscript>
                    </td>
                </tr>
            </tbody>
        </table>
    </form:form>

    <div class="btn_c">
        <c:choose>
            <c:when test="${param.menuFlag eq 'lessonHomework' }"><c:set var="listUrl" value="/mng/lms/manage/homeworkTotalList.do"/></c:when>
            <c:when test="${param.menu eq 'CLASS_MANAGE' }"><c:set var="listUrl" value="/mng/lms/cla/curriculumStudyList.do"/></c:when>
            <c:otherwise><c:set var="listUrl" value="/mng/lms/crcl/CurriculumList.do"/></c:otherwise>
        </c:choose>
        <a href="${listUrl }" class="alR"><img src="/template/manage/images/btn/btn_list.gif" alt="목록"></a>
        <c:choose>
            <c:when test="${param.registAction eq 'regist' }">
                <a href="#none" class="alR insertHomework">
                    <img src="/template/manage/images/btn/btn_regist.gif" alt="등록">
                </a>
            </c:when>
            <c:otherwise>
                <a href="#none" class="alR updateHomework">
		            <img src="/template/manage/images/btn/btn_modify.gif" alt="수정">
		        </a>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>