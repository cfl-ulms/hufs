<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="hwId" value="${searchVO.hwId}"/>
    <c:param name="plId" value="${param.plId}"/>
    <c:param name="menu" value="${param.menu}"/>
    <c:param name="menuFlag" value="${param.menuFlag}"/>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
    <c:when test="${param.menuFlag eq 'lessonHomework' }">
        <c:set var="depth1" value="BASE_CRCL_MNG"/>
        <c:set var="title" value="과제"/>
    </c:when>
    <c:when test="${param.menu eq 'CLASS_MANAGE' }">
        <c:set var="depth1" value="CURRICULUM_STUDY"/>
        <c:set var="title" value="과정별 수업"/>
    </c:when>
    <c:otherwise>
        <c:set var="depth1" value="BASE_CRCL"/>
        <c:set var="title" value="과정등록관리"/>
    </c:otherwise>
</c:choose>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="${param.menu }"/>
    <c:param name="depth1" value="${depth1 }"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="${param.menu eq 'CURRICULUM_MANAGE' ? '과정등록관리' : '과정별 수업'}"/>
</c:import>
<script>
$(document).ready(function(){
	//학생 공개
    $(document).on("click", ".stuOpenAtUpdate", function() {
        var dataStuOpenAt = $(this).attr("data-stuopen");

        $form = $("<form></form>");
        $form.attr("action","/mng/lms/crcl/updateStuOpenAt.do");
        $form.attr('method', 'post');
        $form.appendTo('body');
        
        var hwId = $('<input type="hidden" value="${param.hwId}" name="hwId">');
        var stuOpenAt = $('<input type="hidden" value="' + dataStuOpenAt + '" name="stuOpenAt">');
        var forwardUrl = $('<input type="hidden" name="forwardUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&stuOpenAt=' + dataStuOpenAt + '"">');

        $form.append(hwId).append(stuOpenAt).append(forwardUrl);
        $form.submit();
    });

    //평가대기 검색
    $(document).on("change", ".searchTargetDetail", function() {
        $form = $("<form></form>");
        $form.attr("action","/mng/lms/crcl/selectHomeworkArticle.do");
        $form.attr('method', 'post');
        $form.appendTo('body');
        
        var hwId = $('<input type="hidden" value="${param.hwId}" name="hwId">');
        var menuId = $('<input type="hidden" value="${param.menuId}" name="menuId">');
        var crclId = $('<input type="hidden" value="${param.crclId}" name="crclId">');
        var step = $('<input type="hidden" value="${param.step}" name="step">');
        var searchTargetDetail = $('<input type="hidden" value="' + $(".searchTargetDetail option:selected").val() + '" name="searchTargetDetail">');
   
        $form.append(hwId).append(menuId).append(crclId).append(step).append(searchTargetDetail);
        $form.submit();
    });

    //후기선정, 취소
    $(document).on("click", ".updateCommentPickAt", function() {
        var dataCommentPickAt = $(this).attr("data-commentpickat");
        var dataHwsId = $(this).attr("data-hwsid");
        
        if(dataHwsId == "") {
        	alert("제출하지 않은 과제는 후기로 선정할 수 없습니다.");
        	return false;
        }

        $form = $("<form></form>");
        $form.attr("action","/mng/lms/crcl/updateCommentPickAt.do");
        $form.attr('method', 'post');
        $form.appendTo('body');

        var hwsId = $('<input type="hidden" value="' + dataHwsId + '" name="hwsId">');
        var commentPickAt = $('<input type="hidden" value="' + dataCommentPickAt + '" name="commentPickAt">');
        var forwardUrl = $('<input type="hidden" name="forwardUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&commentPickAt=' + dataCommentPickAt + '"">');

        $form.append(hwsId).append(commentPickAt).append(forwardUrl);
        $form.submit();
    });

    //피드백 Modal 띄우기
    $(document).on("click", ".modalFdb", function() {
        var arrHwsIdCheckedCnt = $('input:checkbox[name=arrHwsId]:checked').length;

        if(0 >= arrHwsIdCheckedCnt) {
            alert("대상을 선택해 주세요.");
            return false;
        }

        $(".spanCheckboxCnt").text(arrHwsIdCheckedCnt);
        
        $("#feedback_write_modal").show();
        $("#dialog-background, .my-dialog").show();
    });

    //피드백  처리
    $(document).on("click", ".btnModalConfirm", function() {
        var validFlag = true;

        $("input:checkbox[name=arrHwsId]:checked").each(function(){
            if($(this).val() == ""){
                alert("과제 제출을 하지 않은 대상은 피드백이 불가능합니다.");
                validFlag = false;
            }
        });
        
        if(validFlag == false) {
            return false;
        }
        
        $("input[name=fdb]").val($(".areaFdb").val());
        document.frm.submit();
    });

    //모달 종료
    $(document).on("click", "#dialog-background,.btn-close-dialog, .btn-no", function(){
        $("#dialog-background, .my-dialog").hide();
    });

    //과제 제출 상세에서 점수 처리
    $(document).on("click", ".updateFdbScr", function() {
        //점수 input으로 줄때 valid 처리
        <c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
            var scr = $("input[name=scr]").val();
            
            if(scr == "") {
                alert("점수를 입력해 주세요.");
                return false;
            }
            
            if(scr > 100 || scr < 0) {
                alert("점수는 0~100 사이에 숫자만 가능합니다.");
                return false;
            }
        </c:if>

        document.fdbFrm.submit();
    });
});
</script>
<c:if test="${curriculumVO.processSttusCode > 0 and param.menuFlag ne 'lessonHomework'}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCode" value="${processSttusCode}"/>
        <c:param name="step" value="${param.menu eq 'CURRICULUM_MANAGE' ? '8' : '5'}"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
        <c:param name="menu" value="${param.menu}"/>
    </c:import>
</c:if>
<div id="cntnts">
    <table class="chart2 mb50">
        <tbody>
	        <tr>
	            <td class="alC">
	                <ul>
	                    <c:forEach var="user" items="${subUserList}" varStatus="status">
	                        <li style="width:180px;float:left;padding-right:15px;">
                                <p class="title">담당교수 <b><c:out value="${user.userNm}"/>(<c:out value="${user.mngDeptNm}"/>)</b></p>
					            <p class="sub-title">문의: ${user.emailAdres }</p>
                            </li>
	                    </c:forEach>
	                </ul>
	            </td>
	        </tr>
        </tbody>
    </table>
    
    <div class="btn_r" style="margin:0px;padding-top:0px;">
        <a href="/mng/lms/crcl/homeworkRegister.do${_BASE_PARAM }&registAction=updt&mode=${empty param.plId ? 'curriculum' : 'lesson' }" class="alR"><img src="/template/manage/images/btn/btn_modify.gif" alt="수정"></a>
        <a href="/mng/lms/crcl/deleteHomeworkArticle.do${_BASE_PARAM }" class="alR"><img src="/template/manage/images/btn/btn_del.gif" alt="삭제"></a>
    </div>
    
    <strong>과제내용 </strong>
    <table class="chart2">
        <colgroup>
            <col width="15%">
            <col width="*">
        </colgroup>
        <tbody>
            <tr>
                <th><label>과제명</label></th>
                <td>
                    ${homeworkVO.nttSj }
                    <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">(과정후기 대상 과제)</c:if>
                </td>
            </tr>
            <tr>
                <th><label>과제 오픈일</label></th>
                <td>${homeworkVO.openDate } ${fn:substring(homeworkVO.openTime,0,2) }:${fn:substring(homeworkVO.openTime,2,4) }</td>
            </tr>
            <tr>
                <th><label>과제 마감일</label></th>
                <td>${homeworkVO.closeDate } ${fn:substring(homeworkVO.closeTime,0,2) }:${fn:substring(homeworkVO.closeTime,2,4) }</td>
            </tr>
        </tbody>
    </table>
    <div class="view_cont">
        <c:out value="${homeworkVO.nttCn}" escapeXml="false" />
    </div>
    <%-- 첨부파일 --%>
    <dl class="view_tit02">
        <dt><spring:message code="cop.atchFileList" /></dt>
        <dd>
            <ul class="list">
                <c:if test="${not empty homeworkVO.atchFileId}">
                    <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
                        <c:param name="param_atchFileId" value="${homeworkVO.atchFileId}" />
                        <c:param name="imagePath" value="${_IMG }"/>
                        <c:param name="mngAt" value="Y"/>
                    </c:import>
                </c:if>
            </ul>
        </dd>
    </dl>

    <%-- 검색 --%>
    <br />
    <br />
    <%-- 과제 상세 --%>
    <c:if test="${empty param.viewFlag }">
	    <strong>과제 피드백</strong>
	    <div id="bbs_search">
	        <h3 class="title-subhead">학생 검색</h3>
	
	        <form name="searchFrm" action="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM }" method="post">	            
	            <select name="searchTargetType" id="searchTargetType">
	              <option value="">검색 유형</option>
	              <option value="mngDeptNm" <c:if test="${param.searchTargetType eq 'mngDeptNm'}">selected</c:if>>소속</option>
	              <option value="userNm" <c:if test="${param.searchTargetType eq 'userNm' }">selected</c:if>>이름</option>
	            </select>
	            
	            <input type="text" placeholder="" name="searchInputValue" value="${param.searchInputValue }">
	
	            <input type="image" src="/template/manage/images/btn/btn_search.gif" alt="검색">
	        </form>
	    </div>
	    
	    <div class="content-header">
	        <div class="feedback-title-wrap">
	          <select class="select2 searchTargetDetail" data-select="style3" name="searchTargetDetail">
	            <option value="" <c:if test="${empty param.searchTargetDetail }">selected</c:if>>전체보기</option>
	            <option value="wait" <c:if test="${param.searchTargetDetail eq 'wait'}">selected</c:if>>평가대기</option>
	          </select>
	          <a href="#none" class="btn_mngTxt modalFdb">선택 피드백 남기기</a>
	          
	          <c:choose>
	              <c:when test="${homeworkVO.stuOpenAt ne 'Y' }"><a href="#none" class="btn_mngTxt stuOpenAtUpdate" data-stuopen="Y">과제평가결과 학생에게 공개</a></c:when>
	              <c:otherwise><a href="#none" class="btn_mngTxt stuOpenAtUpdate" data-stuopen="N">공개취소</a></c:otherwise>
	          </c:choose>
	          <span class="text stuOpenText" style="<c:if test="${homeworkVO.stuOpenAt ne 'Y' }">display:none;</c:if>"><c:if test="${homeworkVO.stuOpenAt eq 'Y' }">평가결과가 학생에게 공개되었습니다. (<span class="stuOpenDateYear">${fn:substring(homeworkVO.stuOpenDate,0,4) }</span>-<span class="stuOpenDateMonth">${fn:substring(homeworkVO.stuOpenDate,5,7) }</span>-<span class="stuOpenDateDay">${fn:substring(homeworkVO.stuOpenDate,8,10) }</span> 부터)</c:if></span>
	        </div>
	    </div>
	</c:if>
    
    <c:choose>
        <%-- 과제 제출 상세 --%>
        <c:when test="${param.viewFlag eq 'homeworksubmit' }">
		    <strong>제출한 과제</strong>
		    <table class="chart2">
		        <colgroup>
		            <col width="15%">
		            <col width="*">
		        </colgroup>
		        <tbody>
		            <tr>
		                <th><label>과제명 (국문)</label></th>
		                <td>${homeworkSubmitVO.nttSj}</td>
		            </tr>
		            <tr>
                        <th><label>과제명 (${curriculumVO.crclLangNm })</label></th>
                        <td>${homeworkSubmitVO.nttSjLang}</td>
                    </tr>
                    <c:if test="${homeworkVO.hwType eq 2 }">
                        <tr>
	                        <th><label>조 정보</label></th>
	                        <td>${homeworkSubmitVO.groupCnt}조 (${fn:length(pickStudentList)}명)</td>
	                    </tr>
	                    <tr>
                            <th><label>조 원</label></th>
                            <td>
                                <c:forEach var="pickStudentList" items="${pickStudentList}" varStatus="status">
                                  ${pickStudentList.userNm }
                                  <c:if test="${!status.last}">,</c:if>
                              </c:forEach>
                            </td>
                        </tr>
                    </c:if>
                    <tr>
                        <!-- 제출일자 게산을 위한 함수 -->
	                    <c:set var="closeDate" value="${fn:replace(homeworkVO.closeDate, '-', '')}${homeworkVO.closeTime }"/>
	                    <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
	                    <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />

	                    <fmt:formatDate value="${result.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />

	                    <!-- 아래 날짜 표기를 위한 함수 -->
	                    <fmt:formatDate var="strLastUpdusrPnttmHHmm" pattern="yyyy-MM-dd HH:mm" value="${homeworkSubmitVO.lastUpdusrPnttm }"/>

	                    <!-- 제출 여부 처리  -->
	                    <c:choose>
	                        <c:when test="${empty homeworkSubmitVO.hwsId }">
	                            <c:set var="submitAt" value="미제출"/>
	                        </c:when>
	                        <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
	                            <c:set var="submitAt" value="마감후"/>
	                        </c:when>
	                        <c:otherwise>
	                            <c:set var="submitAt" value="제출"/>
	                        </c:otherwise>
	                    </c:choose>

	                    <!-- 마감 후 제출 날짜 계산 --> 
	                    <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
	                        <br />
	                        <fmt:parseDate value="${fn:substring(homeworkVO.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
	                        <fmt:formatDate var="strLastUpdusrPnttm" pattern="yyyy-MM-dd" value="${homeworkSubmitVO.lastUpdusrPnttm }"/>
	                        <fmt:parseDate value="${fn:substring(strLastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
	                        <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
	                        <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
	                    </c:if>
                        <th><label>제출일</label></th>
                        <td>
                            <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
                            
                            <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
	                        <c:if test="${dDay eq 0 }">
	                            <c:set var="dDay" value="1"/>
	                        </c:if>
	                        ${strLastUpdusrPnttmHHmm } (${submitAt } D+${dDay })
                        </td>
                    </tr>
		        </tbody>
		    </table>
		    <div class="view_cont">
		        <c:out value="${homeworkSubmitVO.nttCn}" escapeXml="false" />
		    </div>
		    <%-- 첨부파일 --%>
		    <dl class="view_tit02">
		        <dt><spring:message code="cop.atchFileList" /></dt>
		        <dd>
		            <ul class="list">
		                <c:if test="${not empty homeworkSubmitVO.atchFileId}">
		                    <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
		                        <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" />
		                        <c:param name="imagePath" value="${_IMG }"/>
		                        <c:param name="mngAt" value="Y"/>
		                    </c:import>
		                </c:if>
		            </ul>
		        </dd>
		    </dl>
		    
		    <br />
		    <br />
		    <strong>교수님 피드백</strong>
		    <form name="fdbFrm" action="/mng/lms/crcl/updateFdb.do" method="post">
                <input type="hidden" name="forwardUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}" />
                <input type="hidden" name="arrHwsId" value="${homeworkSubmitVO.hwsId }" />
			    <table class="chart2">
	                <colgroup>
	                    <col width="15%">
	                    <col width="*">
	                </colgroup>
	                <tbody>
	                    <tr>
	                        <th><label>점수</label></th>
	                        <td>
	                            <c:choose>
	                                <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
	                                    <select class='table-select select2' data-select='style1' name="scr">
	                                      <option value='100' <c:if test="${homeworkSubmitVO.scr eq '100'}">selected</c:if>>PASS</option>
	                                      <option value='0' <c:if test="${homeworkSubmitVO.scr eq '0'}">selected</c:if>>FAIL</option>
	                                    </select></div>
	                                </c:when>
	                                <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
	                                    <input class="table-input onlyNum" value="${homeworkSubmitVO.scr}" placeholder="00" name="scr" maxlength=3 />
	                                </c:when>
	                            </c:choose>
	                        </td>
	                    </tr>                    
	                    <tr>
	                        <th><label>피드백</label></th>
	                        <td><textarea class='table-textarea h-90 onlyText' name="fdb" style="width:100%;height:100px;"><c:out value="${homeworkSubmitVO.fdb}" escapeXml="false" /></textarea></td>
	                    </tr>
	                </tbody>
	            </table>
	        </form>

            <div class="btn_c">
                <a href="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}" class="btn_mngTxt" data-commentpickat="N" data-hwsid="${result.hwsId }">취소</a>
                <a href="#none" class="btn_mngTxt updateFdbScr" data-commentpickat="N" data-hwsid="${result.hwsId }">저장</a>
            </div>
		</c:when>
		<%-- 과제 상세 --%>
	    <c:otherwise>
	        <form name="frm" action="/mng/lms/crcl/updateFdb.do">
                <input type="hidden" name="forwardUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}" />
                <input type="hidden" name="fdb" value="" />
            
                <table class="chart_board">
                    <colgroup>
                      <col style='width:5%'>
                      <c:if test="${homeworkVO.hwType eq 1 }">
                          <col style='width:10%'>
                          <col style='width:10%'>
                          <col style='width:9%'>
                          <col style='width:11%'>
                      </c:if>
                      <col style='width:7%'>
                      <col style='width:7%'>
                      <col style='width:7%'>
                      <col style='width:7%'>
                      <col>
                      <col style='width:7%'>
                      <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
                          <col style='width:12%'>
                      </c:if>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>선택</th>
                            <c:if test="${homeworkVO.hwType eq 1 }">
                                <th>소속</th>
                                <th>이름</th>
                                <th>생년월일</th>
                                <th>학번</th>
                            </c:if>
                            <th>반</th>
                            <th>조</th>
                            <th>제출여부</th>
                            <th>평가여부</th>
                            <th>피드백</th>
                            <th>점수</th>
                            <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
                                <th>과정후기 선정</th>
                            </c:if>
                        </tr> 
                    </thead>
                    <tbody>
                        <c:forEach var="result" items="${selectHomeworkSubjectList}" varStatus="status">
                            <!-- 제출일자 게산을 위한 함수 -->
                            <c:set var="closeDate" value="${fn:replace(homeworkVO.closeDate, '-', '')}${homeworkVO.closeTime }"/>
                            <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                            <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />
            
                            <fmt:formatDate value="${result.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />
            
                            <!-- 제출 여부 처리  -->
                            <c:choose>
                                <c:when test="${empty result.hwsId }">
                                    <c:set var="tdFontColor" value="font-red"/>
                                    <c:set var="submitAt" value="미제출"/>
                                </c:when>
                                <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
                                    <c:set var="tdFontColor" value="font-blue"/>
                                    <c:set var="submitAt" value="마감후"/>
                                </c:when>
                                <c:otherwise>
                                   <c:set var="tdFontColor" value=""/>
                                   <c:set var="submitAt" value="제출"/>
                                </c:otherwise>
                            </c:choose>
                            
                            <c:choose>
                                <c:when test="${empty result.hwsId }">
                                    <c:set var="viewUrl" value="#none"/>
                                </c:when>
                                <c:otherwise>
                                    <c:url var="viewUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
                                    <c:param name="hwsId" value="${result.hwsId }"/>
                                       <c:param name="viewFlag" value="homeworksubmit"/>
                                       <c:param name="homeworkTotalFlag" value="${param.homeworkTotalFlag }"/>
                                   </c:url>
                                </c:otherwise>
                            </c:choose>
                            <tr class="">
                                <td scope='row'><label class='checkbox selectedList'><input type='checkbox' name="arrHwsId" value="${result.hwsId }"><span class='custom-checked'></span></label></td>
                                <c:if test="${homeworkVO.hwType eq 1 }">
                                    <td onclick="location.href='${viewUrl}'">${result.mngDeptNm }</td>
                                    <td onclick="location.href='${viewUrl}'">${result.userNm }</td>
                                    <td onclick="location.href='${viewUrl}'">${result.brthdy }</td>
                                    <td onclick="location.href='${viewUrl}'">${result.stNumber }</td>
                                </c:if>
                                <td onclick="location.href='${viewUrl}'">
                                    <c:if test="${!empty result.groupCnt }">
                                        ${result.classCnt }반
                                    </c:if>
                                </td>
                                <td onclick="location.href='${viewUrl}'">
                                    <c:if test="${!empty result.classCnt }">
                                        ${result.groupCnt }조
                                    </c:if>
                                </td>
                                <td class="${tdFontColor }" onclick="location.href='${viewUrl}'">
                                    ${submitAt }
            
                                    <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
                                        <br />
                                        <fmt:parseDate value="${fn:substring(homeworkVO.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
                                        <fmt:parseDate value="${fn:substring(result.lastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
                                        <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
                                        <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
                                        
                                        <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
                                           <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
                                        </c:if>
                                        
                                        <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
                                        <c:if test="${dDay eq 0 }">
                                            <c:set var="dDay" value="1"/>
                                        </c:if>
                                        
                                        (D+${dDay })
                                    </c:if>
                                </td>
                                <td onclick="location.href='${viewUrl}'">${result.fdbAt }</td>
                                <td onclick="location.href='${viewUrl}'">${result.fdb }</td>
                                <td onclick="location.href='${viewUrl}'">
                                    <c:choose>
                                       <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
                                           <c:choose>
                                               <c:when test="${homeworkVO.stuOpenAt ne 'Y' and empty result.scr}"></c:when>
                                               <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty result.scr}"><span class="${tdFontColor }">FAIL</span></c:when>
                                               <c:when test="${result.scr eq '100'}"><span class="${tdFontColor }">PASS</span></c:when>
                                               <c:otherwise><span class="${tdFontColor }">FAIL</span></c:otherwise>
                                           </c:choose>
                                       </c:when>
                                       <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
                                           <c:choose>
                                               <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty result.scr}">0</c:when>
                                               <c:otherwise>${result.scr }</c:otherwise>
                                           </c:choose>
                                       </c:when>
                                   </c:choose>
                                </td>
                                <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
                                    <td>
                                        <c:choose>
                                            <c:when test="${result.commentPickAt eq 'Y' }">
                                                <a href="#none" class="btn_mngTxt updateCommentPickAt" data-commentpickat="N" data-hwsid="${result.hwsId }">선정취소</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="#none" class="btn_mngTxt updateCommentPickAt" data-commentpickat="Y" data-hwsid="${result.hwsId }">선정하기</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </form>
	    </c:otherwise>
    </c:choose>
	<div class="btn_c">
	    <c:choose>
	        <c:when test="${param.menuFlag eq 'lessonHomework' }"><c:set var="listUrl" value="/mng/lms/manage/homeworkTotalList.do"/></c:when>
	        <c:when test="${param.menu eq 'CLASS_MANAGE' }"><c:set var="listUrl" value="/mng/lms/cla/curriculumStudyList.do"/></c:when>
	        <c:otherwise><c:set var="listUrl" value="/mng/lms/crcl/CurriculumList.do"/></c:otherwise>
	    </c:choose>
        <a href="${listUrl }" class="alR"><img src="/template/manage/images/btn/btn_list.gif" alt="목록"></a>
    </div>
</div>

<%-- 피드백 남기기 모달 --%>
<div class="my-dialog feedback_write_modal" style="overflow-y:auto;height:250px;">
    <div class="modal_header">피드백 남기기</div>
    <div class="modal_body ">
        <div class="modal-text mb-10" style="padding:5px;">
            선택된 학생 <span class="modal-subtext inline font-gray">총 
            <span class="spanCheckboxCnt" style="font-weight:bold;"></span>
            <c:choose>
                <c:when test="${homeworkVO.hwType eq 1 }">명</c:when>
                <c:otherwise>개조</c:otherwise>
            </c:choose>
            </span>
        </div>
        <div style="padding:10px;">
            <textarea class="content-textarea onlyText areaFdb" style="width:100%;height:100px;"></textarea>
        </div>
        <div class="box_btn">
            <button class="btn-close-dialog "><img src="/template/manage/images/btn/del.gif"></button>
            <a href="#none" class="btn_mngTxt btnModalConfirm">확인</a>
        </div>
    </div>
</div>
<div id="dialog-background"></div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>