<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:choose>
	<c:when test="${searchVO.sysTyCode eq 'SYS02'}">
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="CURRICULUM_MANAGE"/>
			<c:param name="depth1" value="CTGRY_ADMIN"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="과정코드관리"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<c:import url="/mng/template/top.do" charEncoding="utf-8">
			<c:param name="menu" value="BOARD_MANAGE"/>
			<c:param name="depth1" value="CTGRY_ADMIN"/>
			<c:param name="depth2" value=""/>
			<c:param name="title" value="카테고리관리"/>
		</c:import>
	</c:otherwise>
</c:choose>

<script>
	function fnCtgy_Manage(url) {
		var win = window.open(url ,'ctgy',' scrollbars=yes, resizable=yes, left=0, top=0, width=900,height=650');
		if(win != null) {
			win.focus();
		}
	}
	
$(document).ready(function(){
	$(".btn_code").click(function(){
		var id = $(this).data("id"),
			name = $(this).data("name"),
			url = "/mng/cop/bbs/ctg/selectBBSCtgryList.do?ctgrymasterId="+id+"&ctgrymasterNm="+name+"&sysTyCode=SYS02";
		
		window.open(url ,'ctgy',' scrollbars=yes, resizable=yes, left=0, top=0, width=900,height=650');
		return false;
	});
	
	$()
});
</script>
<style>
.overarea{width:100%;overflow:scroll;}
.box_basecode{width:2800px;}
td{text-align:center;}
td.tit{text-align:left;}
</style>
<div id="cntnts">
	<div class="overarea">
		<div class="box_basecode">
			<table class="chart3">
				<colgroup>
					<col width="110px;"/>
					<col width="200px;"/>
					<col width="120px"/>
					<col width="130px"/>
					<col width="180px"/>
					<col width="140px"/>
					<col width="50px"/>
					<col width="190px"/>
					<col width="50px"/>
					<col width="90px"/>
					<col width="60px"/>
					<col width="*"/>
					<col width="50px"/>
					<col width="50px"/>
					<col width="50px"/>
					<col width="90px"/>
					<col width="90px"/>
					<col width="100px"/>
					<col width="60px"/>
					<col width="50px"/>
					<col width="90px"/>
					<col width="110px"/>
					<col width="80px"/>
					<col width="110px"/>
					<col width="180px"/>
					<col width="120px"/>
				</colgroup>
				<thead>
					<tr>
						<th rowspan="3" class="incarnadine">이수구분</th>
						<th rowspan="3" class="incarnadine">관리구분</th>
						<th rowspan="3" class="incarnadine">강의유형</th>
						<th rowspan="3" class="incarnadine">비강의유형</th>
						<th rowspan="3" class="incarnadine">평가</th>
						<th rowspan="2" colspan="2" class="purple">언어코드</th>
						<th rowspan="2" colspan="2" class="purple">주관기관</th>
						<th rowspan="2" colspan="3" class="whiteblue">수료기준</th>
						<th colspan="3" class="green">성적기준</th>
						<th rowspan="2" colspan="2" class="pink">총괄평가기준</th>
						<th rowspan="3" class="gray">수업영역</th>
						<th rowspan="2" colspan="2" class="blue">레벨</th>
						<th rowspan="2" colspan="2" class="purple2">대상</th>
						<th rowspan="3" class="gray">학기</th>
						<th rowspan="3" class="blue2">교육장소</th>
						<th rowspan="2" colspan="2" class="purple2">사업비</th>
					</tr>
					<tr>
						<th colspan="2" class="green">Letter Grade</th>
						<th rowspan="2" class="green">P/F</th>
					</tr>
					<tr>
						<th class="purple">구분</th>
						<th class="purple">코드</th>
						<th class="purple">구분</th>
						<th class="purple">코드</th>
						<th class="whiteblue">구분</th>
						<th class="whiteblue">기준</th>
						<th class="whiteblue">설명</th>
						<th class="green">등급</th>
						<th class="green">평점</th>
						<th class="pink">1depth</th>
						<th class="pink">2depth</th>
						<th class="blue">1depth</th>
						<th class="blue">2depth</th>
						<th class="purple2">본교생여부<br/>(Y/N)</th>
						<th class="purple2"></th>
						<th class="purple2">1depth</th>
						<th class="purple2">2depth</th>
					</tr>
				</thead>
				<tbody>
					<%-- Letter Grade 등급 --%>
					<c:set var="lgTypeListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000011}">
							<c:if test="${result.upperCtgryId eq 'CTG_0000000000000090'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="lgTypeList" value="${fn:split(lgTypeListSplit,',')}"/>
					
					<%-- Letter Grade 평점 --%>
					<c:set var="lgValListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000011}">
							<c:if test="${result.upperCtgryId eq 'CTG_0000000000000090'}">
								,${result.ctgryVal}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="lgValList" value="${fn:split(lgValListSplit,',')}"/>
					
					<%-- P/F --%>
					<c:set var="pfListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000011}">
							<c:if test="${result.upperCtgryId eq 'CTG_0000000000000091'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="pfList" value="${fn:split(pfListSplit,',')}"/>
					
					<%-- 총괄평가기준 Depth1 --%>
					<c:set var="totalD1ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000012}">
							<c:if test="${result.ctgryLevel eq '1'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="totalD1List" value="${fn:split(totalD1ListSplit,',')}"/>
					
					<%-- 총괄평가기준 Depth2 --%>
					<c:set var="totalD2ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000012}">
							<c:if test="${result.ctgryLevel eq '2'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="total2DList" value="${fn:split(totalD2ListSplit,',')}"/>
					
					<%-- 레벨 Depth1 --%>
					<c:set var="lvlD1ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000014}">
							<c:if test="${result.ctgryLevel eq '1'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="lvlD1List" value="${fn:split(lvlD1ListSplit,',')}"/>
					
					<%-- 레벨 Depth2 --%>
					<c:set var="lvlD2ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000014}">
							<c:if test="${result.ctgryLevel eq '2'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="lvlD2List" value="${fn:split(lvlD2ListSplit,',')}"/>
					
					<%-- 사업비 Depth1 --%>
					<c:set var="feeD1ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000018}">
							<c:if test="${result.ctgryLevel eq '1'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="feeD1List" value="${fn:split(feeD1ListSplit,',')}"/>
					
					<%-- 사업비 Depth2 --%>
					<c:set var="feeD2ListSplit">
						<c:forEach var="result" items="${resultList.CTGMST_0000000000018}">
							<c:if test="${result.ctgryLevel eq '2'}">
								,${result.ctgryNm}
							</c:if>
						</c:forEach>
					</c:set>
					<c:set var="feeD2List" value="${fn:split(feeD2ListSplit,',')}"/>
					
					<c:forEach var="i" begin="1" end="${listSize - 2}" step="1" varStatus="status">
						<tr>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000003" data-name="이수구분">
									<c:out value="${resultList.CTGMST_0000000000003[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000005" data-name="관리구분">
									<c:out value="${resultList.CTGMST_0000000000005[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000006" data-name="강의유형">
									<c:out value="${resultList.CTGMST_0000000000006[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000007" data-name="비강의유형">
									<c:out value="${resultList.CTGMST_0000000000007[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000008" data-name="평가">
									<c:out value="${resultList.CTGMST_0000000000008[i].ctgryNm}"/>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000002" data-name="언어코드">
									<c:out value="${resultList.CTGMST_0000000000002[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000002" data-name="언어코드">
									<c:out value="${resultList.CTGMST_0000000000002[i].ctgryId}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000009" data-name="주관기관">
									<c:out value="${resultList.CTGMST_0000000000009[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000009" data-name="주관기관">
									<c:out value="${resultList.CTGMST_0000000000009[i].ctgryId}"/>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000010" data-name="수료기준">
									<c:out value="${resultList.CTGMST_0000000000010[i].ctgryNm}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000010" data-name="수료기준">
									<c:out value="${resultList.CTGMST_0000000000010[i].ctgryVal}"/>
								</a>
							</td>
							<td class="tit">
								<a href="#" class="btn_code" data-id="CTGMST_0000000000010" data-name="수료기준">
									<c:out value="${resultList.CTGMST_0000000000010[i].ctgryCn}"/>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000011" data-name="성적기준">
									<c:out value="${lgTypeList[i-1]}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000011" data-name="성적기준">
									<c:out value="${lgValList[i-1]}"/>
								</a>
							</td>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000011" data-name="성적기준">
									<c:out value="${pfList[i-1]}"/>
								</a>
							</td>
							
							<c:if test="${i ne 2 and i ne 3}">
								<td <c:if test="${i eq 1}">rowspan="3"</c:if>>
									<a href="#" class="btn_code" data-id="CTGMST_0000000000012" data-name="총괄평가기준">
										<c:choose>
											<c:when test="${i > 1}"><c:out value="${totalD1List[i-3]}"/></c:when>
											<c:otherwise><c:out value="${totalD1List[i-1]}"/></c:otherwise>
										</c:choose>
									</a>
								</td>
							</c:if>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000012" data-name="총괄평가기준">
									<c:out value="${total2DList[i-1]}"/>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000013" data-name="수업영역">
									<c:out value="${resultList.CTGMST_0000000000013[i].ctgryNm}"/>
								</a>
							</td>
							
							<c:if test="${i ne 2 and i ne 3 and i ne 5 and i ne 7}">
								<td <c:if test="${i eq 1}">rowspan="3"</c:if> <c:if test="${i eq 4 or i eq 6}">rowspan="2"</c:if>>
									<a href="#" class="btn_code" data-id="CTGMST_0000000000014" data-name="레벨">
										<c:choose>
											<c:when test="${i eq 1}"><c:out value="${lvlD1List[i-1]}"/></c:when>
											<c:when test="${i eq 4}"><c:out value="${lvlD1List[i-3]}"/></c:when>
											<c:when test="${i eq 6}"><c:out value="${lvlD1List[i-4]}"/></c:when>
										</c:choose>
									</a>
								</td>
							</c:if>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000014" data-name="레벨">
									<c:out value="${lvlD2List[i-1]}"/>
								</a>
							</td>
							
							
							<c:if test="${i eq 1 or i eq 4 or i > 6}">
								<td <c:if test="${i eq 1 or i eq 4 }">rowspan="3"</c:if>>
									<a href="#" class="btn_code" data-id="CTGMST_0000000000015" data-name="대상">
										<c:choose>
											<c:when test="${i eq 1}">일반(N)</c:when>
											<c:when test="${i eq 4}">본교생(Y)</c:when>
										</c:choose>
									</a>
								</td>
							</c:if>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000015" data-name="대상">
									<c:choose>
										<c:when test="${i < 4}"><c:out value="${resultList.CTGMST_0000000000015[i].ctgryNm}"/></c:when>
										<c:when test="${i > 3}"><c:out value="${resultList.CTGMST_0000000000015[i-3].ctgryNm}"/></c:when>
									</c:choose>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000016" data-name="학기">
									<c:out value="${resultList.CTGMST_0000000000016[i].ctgryNm}"/>
								</a>
							</td>
							
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000017" data-name="교육장소">
									<c:out value="${resultList.CTGMST_0000000000017[i].ctgryNm}"/>
								</a>
							</td>
							
							<c:if test="${i eq 1 or i eq 8 or i > 14}">
								<td <c:if test="${i eq 1 or i eq 8}">rowspan="7"</c:if>>
									<a href="#" class="btn_code" data-id="CTGMST_0000000000018" data-name="사업비">
										<c:choose>
											<c:when test="${i eq 1}"><c:out value="${feeD1List[0]}"/></c:when>
											<c:when test="${i eq 8}"><c:out value="${feeD1List[1]}"/></c:when>
											<c:when test="${i eq 15}"><c:out value="${feeD1List[2]}"/></c:when>
										</c:choose>
									</a>
								</td>
							</c:if>
							<td>
								<a href="#" class="btn_code" data-id="CTGMST_0000000000018" data-name="사업비">
									<c:out value="${feeD2List[i-1]}"/>
								</a>
							</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	