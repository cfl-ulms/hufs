<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" />

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="crclId" value="${searchVO.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
    <c:param name="menu" value="CLASS_MANAGE"/>
    <c:param name="depth1" value="${param.depth1 }"/>
    <c:param name="depth2" value=""/>
    <c:param name="title" value="과정별 수업"/>
</c:import>

<script>
$(document).ready(function(){
});

</script>
<c:if test="${curriculumVO.processSttusCode > 0}">
    <c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
        <c:param name="step" value="5"/>
        <c:param name="crclId" value="${curriculumVO.crclId}"/>
        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
    </c:import>
</c:if>
<div id="cntnts">
    <table class="chart_board">
        <thead>
          <tr>
            <th>번호</th>
            <th colspan='2'>구분</th>
            <th>수업주제</th>
            <th>과제명</th>
            <th>교원명</th>
            <th>제출기간</th>
            <th>마감일</th>
            <th>과제상태</th>
          </tr>
        </thead>
        <tbody>
            <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                <c:url var="viewUrl" value="/mng/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
                    <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                    <c:param name="hwId" value="${result.hwId }" />
                    <c:param name="plId" value="${result.plId }" />
                    <c:param name="menu" value="CLASS_MANAGE" />
                </c:url>
                <tr class="">
                  <td scope='row' onclick="location.href='${viewUrl}'"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
                  <td onclick="location.href='${viewUrl}'">${result.hwCodeNm}</td>
                  <td onclick="location.href='${viewUrl}'">${result.hwTypeNm}</td>
                  <td onclick="location.href='${viewUrl}'" class='left-align'>${result.studySubject}</td>
                  <td onclick="location.href='${viewUrl}'" class='left-align keep-all'>${result.nttSj}</td>
                  <td onclick="location.href='${viewUrl}'">
                      <c:set var="teacherCnt" value="0"/>

                      <c:forEach var="user" items="${facPlList}" varStatus="status">
                          <c:if test="${0 eq status.index }">
                              ${user.userNm}
                          </c:if>
                          <c:if test="${1 < status.index }">
                              <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
                          </c:if>
                      </c:forEach>
                      <c:if test="${0 < teacherCnt }"> 외${teacherCnt }명</c:if>
                  </td>
                  <td onclick="location.href='${viewUrl}'">${result.openDate}<br>~<br>${result.closeDate}</td>
                  <td onclick="location.href='${viewUrl}'" class='keep-all'>${result.closeDate}</td>

                  <%-- 날짜 비교를 위한 데이터 컨버팅 --%>
                  <c:set var="openDate" value="${fn:replace(result.openDate, '-', '')}${result.openTime }"/>
                  <fmt:parseDate value="${openDate}" var="openParseDate" pattern="yyyyMMddHHmm"/>
                  <fmt:formatDate value="${openParseDate}" pattern="yyyyMMddHHmm" var="openDateForm"/>

                  <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
                  <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                  <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateForm"/>

                  <td onclick="location.href='${viewUrl}'">
                  	${result.stuOpenAt}==
                      <c:choose>
                          <c:when test="${openDateForm > nowDate }">등록대기</c:when>
                          <c:when test="${closeDateForm < nowDate or result.stuOpenAt eq 'Y'}">마감</c:when>
                          <c:otherwise>제출중</c:otherwise>
                      </c:choose>
                  </td>
                </tr>
            </c:forEach>
            <c:if test="${fn:length(selectHomeworkList) == 0}">
                <tr>
                    <td class="listCenter" colspan="9"><spring:message code="common.nodata.msg" /></td>
                </tr>
            </c:if>
          </tbody>
    </table>

    <div id="paging">
        <c:url var="pageUrl" value="/mng/lms/manage/homeworkList.do?${_BASE_PARAM}">
        </c:url>

        <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" /></ul>
    </div>
</div>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>