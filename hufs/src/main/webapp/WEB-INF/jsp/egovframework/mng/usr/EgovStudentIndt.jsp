<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="MBER_MANAGE"/>
	<c:param name="depth1" value="MBER_MANAGE"/>
	<c:param name="depth2" value="STDT_MANAGE"/>
	<c:param name="validator" value="userManageVO"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<c:set var="registerFlag" value="${empty userManageVO.userId ? '등록' : '수정'}"/>

<script type="text/javascript">

$(document).ready(function(){
	var userSeCode = "${userManageVO.userSeCode}";
	if(userSeCode == 02){
		$('#td_groupCode').hide();
		$('#tr_major').hide();
		$('#tr_stnumber').hide();
	}if(userSeCode == 04){
		$('#td_txt_groupCode').hide();
		$('#tr_stnumber').hide();
	}else if(userSeCode == 06){
		$('#tr_groupcode').hide();
		$('#tr_major').hide();
		$('#tr_stnumber').show();
	}
	
	var brmonth = "${fn:substring(userManageVO.brthdy, 4, 6)}",
		brday = "${fn:substring(userManageVO.brthdy, 6, 8)}";
		
	$("select[name=brthdy02]").val(brmonth).prop("selected", true);
	$("select[name=brthdy03]").val(brday).prop("selected", true);
});

<c:if test='${not empty message}'>
alert("${message}");
</c:if>
		function fnChangeCode(obj){
			if(obj.value == 02){
				$('#tr_groupcode').show();
				$('#td_groupCode').hide();
				$('#td_txt_groupCode').show();
				$('#tr_major').hide();
				$('#tr_stnumber').hide();
				//$('#td_txt_groupCode').val(${userManageVO.groupCode});
			}
			if(obj.value == 06){
				$('#tr_groupcode').hide();
				$('#tr_major').hide();
				$('#tr_stnumber').show();
			}
			if(obj.value == 04){
				$('#tr_groupcode').show();
				$('#td_groupCode').show();
				$('#td_txt_groupCode').hide();
				$('#tr_major').show();
				$('#tr_stnumber').hide();
			}
		}
		function checkForm(form) {	
		
			if($('#email1').val() == "" || $('#email1').val() == null){
				alert("이메일을 입력하세요.");
				$('#email1').focus();
				return false;
			}else if($('#email2').val() == "" || $('#email2').val() == null){
				alert("이메일 주소를 선택하세요.");
				$('#email2').focus();
				return false;
			}else{
				var stNumber = $("input[name=userNm]").val();
				
				if(stNumber == '' || stNumber == null || stNumber == 'undefined'){
					alert("이름을 입력해 주세요.");
					return false;
				}
				/* if(stNumber){
					hufsStu(stNumber);
				}
				return false; */
				if('${registerFlag}' == '수정'){

					if(!confirm('<spring:message code="common.update.msg" />'))
						return false;
				} else {
					
					if(!confirm('<spring:message code="common.regist.msg" />'))
						return false;
					
				}
			}
			
		}
			
	    function fnIdRcomd(){
	    	var frm = document.userManageVO;
	        var url = "<c:url value='/uss/umt/cmm/EgovIdRecomendCnfirmView.do'/>";
	        window.open(url, 'RcomdIdCheck', 'menu=no, scrollbars=yes, width=420,height=300');
	    }
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}

		function fn_egov_return_IdCheck(userId) {
	    	var frm = document.userManageVO;
			frm.userId.value = userId;
	    }

		function fn_egov_return_RcomdCheck(rcomdId) {
	    	var frm = document.userManageVO;
			frm.recomendId.value = rcomdId;
	    }

		function inputDirectEmailDns(val){
		 	document.getElementById('email2').value = val;
		}

		function sendPassword() {
		    if(confirm("비밀번호를 재발급하고 "+document.userManageVO.moblphonNo.value+"번호로 전송 하시겠습니까?")) {
		        document.userManageVO.action = "${pageContext.request.contextPath}/mng/usr/SendPassword.do";
		        document.userManageVO.target = "passSand";
		        return true;
		    }else{
		    	return false;
			}
		}
		
		function fnIdCheck(){
	    	var frm = document.userManageVO;
	    	var userId = frm.userId.value;
	    	if(!fnCheckUserIdLength(userId) || !fnCheckSpace(userId) || !fnCheckNotKorean(userId) || !(fnCheckEnglish(userId) && fnCheckDigit(userId) && !fnCheckTksu(userId))){
		    	alert("아이디는 띄어쓰기 없는 영문+숫자 조합 8~20자 내에서 입력해야 합니다.");
		    } else {
		        var url = "<c:url value='/uss/umt/cmm/EgovIdDplctCnfirm.do?checkId=" + encodeURIComponent(userId) + "'/>";
		        window.open(url, 'IdCheck', 'menu=no, scrollbars=yes, width=500,height=350');
		    }
	    }
		
		function fnCheckUserIdLength(str) {
            if (str.length < 8 || str.length > 20 ){
                    return false;
            }
            return true;
        }
		
		function fnCheckSpace(str){
				for (var i=0; i < str .length; i++) {
				    ch_char = str .charAt(i);
				    ch = ch_char.charCodeAt();
				        if(ch == 32) {
				            return false;
				        }
				}
	    	    return true;
	    	}
		
		function fnCheckNotKorean(koreanStr){                  
    	    for(var i=0;i<koreanStr.length;i++){
    	        var koreanChar = koreanStr.charCodeAt(i);
    	        if( !( 0xAC00 <= koreanChar && koreanChar <= 0xD7A3 ) && !( 0x3131 <= koreanChar && koreanChar <= 0x318E ) ) { 
    	        }else{
    	            //hangul finding....
    	            return false;
    	        }
    	    }
    	    return true;
    	}
		
		function fnCheckTksu(str) { 
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    ch = ch_char.charCodeAt();
			        if( !(ch >= 33 && ch <= 47) || (ch >= 58 && ch <= 64) || (ch >= 91 && ch <= 96) || (ch >= 123 && ch <= 126) ) {
			            
			        } else {
			        	return true;
			        }
			}
            return false;
            
        }
        
        function fnCheckEnglish(str){
       		for(var i=0;i<str.length;i++){
       			var EnglishChar = str.charCodeAt(i);
       			if( !( 0x61 <= EnglishChar && EnglishChar <= 0x7A ) && !( 0x41 <= EnglishChar && EnglishChar <= 0x5A ) ) {
       				
       			} else {
		        	return true;
		        }
       		}
       		return false;
       }
       
       function fnCheckDigit(str) {  
			for (var i=0; i < str .length; i++) {
			    ch_char = str .charAt(i);
			    iValue = parseInt(ch_char);
		        if(isNaN(iValue)) {
		           
		        } else {
		        	return true;
		        }
			}
            return false;
            
        }
		
     	//외대학생 인증
       function hufsStu(stNumber){
       	var brthdy01 = $("input[name=brthdy01]").val(),
       		brthdy02 = $("#sel_month").val(),
       		brthdy03 = $("#sel_date").val(),
       		birthday = brthdy01.substring(2,4) + brthdy02 + brthdy03
       		name = "${userManageVO.userNm}";
       	
       	$.ajax({
       		url : "/crypto/ariaEn.do",
       		dataType : "json",
       		type : "POST",
       		data : {data : stNumber},
       		success : function(data){
       			$.ajax({
       		  		url:encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data),
       		  		contentType: "application/javascript",
       		  		dataType: 'jsonp',
       		  		type: "POST",
       		  		jsonp: "callback",
       		  		async: false,
       		  		success: function(data){
       		   			console.log(data);
       		   			if( data[0].SuccessFg == 'N' ){
       		    			alert("인증과정오류 : "+data[0].rtnToken);
       		   			}else if(data[0].birthday.substring(0,6) != birthday || data[0].name != name || data[0].student_yn != "Y"){
       		   				alert("사용자 정보가 존재하지 않거나 한국외국어대학교 학번 정보와 일치하지 않습니다 . 본인의 성명, 생년월일, 성별 및 학번이 정확하게 입력되었는지 다시 한 번 확인해 주시기 바랍니다. 본교생 인증의 경우 가입일자 기준 재학생에 한하여 가능합니다.");
       		   			}else{
       		    			$("input[name=stGrade]").val(data[0].grade);
       		    			$("input[name=stClass]").val(data[0].major);
       		    			$("input[name=major]").val(data[0].major);
       		    			$("#member").submit();
       		   			}
       		  		}, error:function(jqxhr, status, error){
       		  			//alert("구성원 인증 중 오류 발생!\n[code:"+jqxhr.status+"\n"+"message:"+jqxhr.responseText+"\n]");
       		  			console.log("error : " + encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data));
       		  			alert("사용자 정보가 존재하지 않거나 한국외국어대학교 학번 정보와 일치하지 않습니다 . 본인의 성명, 생년월일, 성별 및 학번이 정확하게 입력되었는지 다시 한 번 확인해 주시기 바랍니다. 본교생 인증의 경우 가입일자 기준 재학생에 한하여 가능합니다.");
       		  		}
       		 	});
       			
       		}, error : function(){
       			alert("error");
       		}
       	});
       }
	</script>
<div id="cntnts">
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<c:set var="actionUrl" value="EgovUserSelectIndt.do"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<c:set var="actionUrl" value="EgovUserSelectUpdt.do?targetId=${userManageVO.userId}"/>
			</c:when>
		</c:choose> 

	<form:form id="member" commandName="userManageVO" name="userManageVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/mng/usr/${actionUrl }"> 
	<input type="hidden" name="stGrade">
    <input type="hidden" name="stClass">
    
	<table class="chart2">
		<caption>회원관리 폼</caption>
		<colgroup>
			<col width="150"/>
			<col width=""/>
			<col width="300"/>
		</colgroup>
		<tbody>
			<tr>
				<th><span style="color:red;"><label for="userSeCode"> 구분</label></span></th>
				<td>
					<form:select path="userSeCode" onchange="javascript:fnChangeCode(this);">
						<option value="06" <c:if test="${userManageVO.userSeCode eq '06'}">selected="selected"</c:if>>한국외국어대학교</option>
						<option value="04" <c:if test="${userManageVO.userSeCode eq '04'}">selected="selected"</c:if>>기타대학</option>
						<option value="02" <c:if test="${userManageVO.userSeCode eq '02'}">selected="selected"</c:if>>일반</option>
			  	  	</form:select>
				</td>
			</tr>
		
			<tr id="tr_groupcode">
				<th><label for="groupNm01"> 소속 </label></th>
				<td id="td_groupCode">
			  	  	<form:select path="groupNm01">
						<option value="">소속선택</option>
						<c:forEach var="result" items="${groupList}" varStatus="status">
							<option value="${result.codeNm}" <c:if test="${userManageVO.groupCode eq result.codeNm}">selected="selected"</c:if>>${result.codeNm }</option>
						</c:forEach>
			  	  	</form:select>
		  	  	</td>
		  	  	<td id="td_txt_groupCode">
					<form:input path="groupNm02" value="${userManageVO.groupCode}"/>
		  	  	</td>
			</tr>

			<tr id="tr_major">
				<th><label for="major"> 세부전공 </label> </th>
				<td>
					<form:input path="major" id="major" cssClass="inp" maxlength="20"/>
		  	  	</td>
			</tr>
			
			<tr id="tr_stnumber">
				<th> 학번 </th>
				<td>
					<c:choose>
						<c:when test="${not empty userManageVO.stNumber}">
							<c:out value="${userManageVO.stNumber}"/>
							<input type="hidden" name="stNumber" value="<c:out value="${userManageVO.stNumber}"/>">
							
						</c:when>
						<c:otherwise><form:input path="stNumber" cssClass="inp" maxlength="20"/></c:otherwise>
					</c:choose>
				</td>
			</tr>
		
			<tr>
				<th><em>*</em><label for="userNm"> 이름 </label></th>
				<td>
					<input type="text" name="userNm"  required="required" title="이름" value="<c:out value="${userManageVO.userNm}"/>">
					<%-- <form:input path="userNm" id="userNm" cssClass="inp" /> --%>
				</td>
				<td rowspan="11" align="center">
					<c:choose>
						<c:when test="${empty userManageVO.photoStreFileNm}">사진없음</c:when>
						<c:otherwise><img src="${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>" width="300"/></c:otherwise>
					</c:choose>
					<br><input type="file" name="userPicFile" class="inp" style="width:300px"/>
				</td>
			</tr>
		
			<tr>
				<th> 생년월일 </th>
				<td>
					  <input type="text" class="onlyNum required" name="brthdy01" placeholder="년(4자)" required="required" title="생년월일" value="<c:out value="${fn:substring(userManageVO.brthdy, 0, 4)}"/>">
		              <select id="sel_month" class="select2 required" name="brthdy02" data-select="style1" required="required" title="생년월일">
		                <option value="">월</option>
		                <option value="01">1월</option>
		                <option value="02">2월</option>
		                <option value="03">3월</option>
		                <option value="04">4월</option>
		                <option value="05">5월</option>
		                <option value="06">6월</option>
		                <option value="07">7월</option>
		                <option value="08">8월</option>
		                <option value="09">9월</option>
		                <option value="10">10월</option>
		                <option value="11">11월</option>
		                <option value="12">12월</option>
		              </select>
		              <select id="sel_date" class="select2 required" name="brthdy03" data-select="style1" required="required" title="생년월일">
		                <option value="">일</option>
		                <option value="01">1일</option>
		                <option value="02">2일</option>
		                <option value="03">3일</option>
		                <option value="04">4일</option>
		                <option value="05">5일</option>
		                <option value="06">6일</option>
		                <option value="07">7일</option>
		                <option value="08">8일</option>
		                <option value="09">9일</option>
		                <option value="10">10일</option>
		                <option value="11">11일</option>
		                <option value="12">12일</option>
		                <option value="13">13일</option>
		                <option value="14">14일</option>
		                <option value="15">15일</option>
		                <option value="16">16일</option>
		                <option value="17">17일</option>
		                <option value="18">18일</option>
		                <option value="19">19일</option>
		                <option value="20">20일</option>
		                <option value="21">21일</option>
		                <option value="22">22일</option>
		                <option value="23">23일</option>
		                <option value="24">24일</option>
		                <option value="25">25일</option>
		                <option value="26">26일</option>
		                <option value="27">27일</option>
		                <option value="28">28일</option>
		                <option value="29">29일</option>
		                <option value="30">30일</option>
		                <option value="31">31일</option>
		              </select>
				</td>
			</tr>
		
			<tr>
				<th> 성별 <span style="color:red;">*</span></th>
				<td>
					<select name="sexdstn">
						<option selected="${userManageVO.sexdstn eq 'M' ? 'selected' : ''}" value="M">남자</option>
						<c:choose>
							<c:when test="${userManageVO.sexdstn eq 'W' or userManageVO.sexdstn eq 'F'}">
								<option selected="selected" value="W">여자</option>
							</c:when>
							<c:otherwise>
								<option value="W">여자</option>
							</c:otherwise>
						</c:choose>
					</select>
					<%-- <c:out value="${userManageVO.sexdstn eq 'M' ? '남자' : '여자'}"/> --%>
				</td>
			</tr>

			<tr>
				<th><label for="moblphonNo">휴대전화</label> <span style="color:red;">*</span></th>
				<td>
					<select class="select2" name="geocode" data-select="style1">
		              	<c:forEach var="result" items="${telNumList}" varStatus="status">
		              		<c:choose>
		              			<c:when test="${result.code eq userManageVO.geocode}">
			              			<option value="${result.code}" selected="selected">${result.codeNm} +${result.code}</option>
		              			</c:when>
		              			<c:otherwise>
			              			<option value="${result.code}">${result.codeNm} +${result.code}</option>
		              			</c:otherwise>
		              		</c:choose>
		              	</c:forEach>
	              	</select>
	              	<input type="text" class="onlyNum required" name="moblphonNo" placeholder="전화번호 입력" required="required" title="휴대전화번호" value="${userManageVO.moblphonNo}" style="text-align:left;">
				</td>
			</tr>
		
			<tr>
				<th> 아이디 <span style="color:red;">*</span></th>
				<td>
					<c:out value="${userManageVO.userId}"/>
				</td>
			</tr>
		
			<tr>
				<th><label for="emailHead"> 본인확인 이메일 </label><span style="color:red;">*</span></th>
				<td>
				<c:set var="emailArr" value="${fn:split(userManageVO.emailAdres, '@')}"/>
					<c:forEach items="${emailArr}" var="arr" varStatus="status">
						<c:if test="${status.count eq 1}">
							<c:set var="emailHead" value="${fn:trim(arr)}"/>
						</c:if>
						<c:if test="${status.count eq 2}">
							<c:set var="emailBody" value="${fn:trim(arr)}"/>
						</c:if>
					</c:forEach>
					<input type="text" name="email1" id="email1" value="${emailHead}" class="inp" /> @ <input type="text" name="email2" value="${emailBody}" id="email2" class="inp"/>
					<select id="email_choice" name="email_choice" onchange='inputDirectEmailDns(this.value);'>
						<option value="">기타(직접입력)</option>
		                <option value="naver.com"	<c:if test="${emailBody eq 'naver.com'}"> selected="selected"</c:if>>네이버(naver.com)</option>
		                <option value="daum.net"	<c:if test="${emailBody eq 'daum.net'}"> selected="selected"</c:if>>다음(daum.net)</option>
		                <option value="gmail.com"	<c:if test="${emailBody eq 'gmail.com'}"> selected="selected"</c:if>>G메일(gmail.com)</option>
						<option value="nate.com"	<c:if test="${emailBody eq 'nate.com'}"> selected="selected"</c:if>>네이트(nate.com)</option>
					</select>
					<%-- <p style="color:blue;">* 수정할 이메일 정보를 입력하신 뒤 [이메일 인증 수정]버튼을 누르세요.</p> --%>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div class="btn_r">
		<c:url var="listUrl" value="./EgovStudentManage.do">
			<c:if test="${!empty searchVO.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
			<c:if test="${!empty searchVO.searchCondition}"><c:param name="searchCondition" value="${searchVO.searchCondition}" /></c:if>
			<c:if test="${!empty searchVO.searchKeyword}"><c:param name="searchKeyword" value="${searchVO.searchKeyword}" /></c:if>
		</c:url>
		<%-- <a href="/mng/usr/EgovMberAddView.do"><img src="${_IMG}/btn/btn_regist.gif" alt="등록하기" /></a> --%>
		<c:choose>
			<c:when test="${registerFlag eq '등록' }">
				<input type="image" src="${MNG_IMG}/btn/btn_regist.gif" alt="등록하기" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
			<c:when test="${registerFlag eq '수정' }">
				<input type="image" src="<c:url value='${MNG_IMG}/btn/btn_modify.gif'/>" alt="수정" onclick="return checkForm(document.userManageVO);"/>
			</c:when>
		</c:choose>

		<a href="<c:out value="${listUrl}"/>"><img src="${MNG_IMG}/btn/btn_cancel.gif" alt="취소"/></a>
						
	</div>
	</form:form>
<iframe name="passSand" id="passSand" style='visibility: hidden; height: 0; width: 0; border: 0px'></iframe>

</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>	