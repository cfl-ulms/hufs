<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.allSubmit}"><c:param name="allSubmit" value="${searchVO.allSubmit}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.reportType}"><c:param name="reportType" value="${searchVO.reportType}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CLASS_MANAGE"/>
	<c:param name="depth1" value="CRCL_REPORT"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="운영 보고서"/>
</c:import>
<style>
.backGray{
	background : #ccc;
}

</style>
<script>
$(document).ready(function(){

	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
		$("input:checkbox").attr("checked", false);
		$("form[id='frm']").find("input").not("#resetBtn").val("");
		/* initCurriculum("",""); */
	});

	$("#searchStartDate, #searchEndDate").datepicker({
		dateFormat: "yy-mm-dd"
	 });



});
</script>

<div id="cntnts">
<c:set var="now" value="<%=new java.util.Date()%>" />
	<div id="bbs_search">
	  	<form id="frm" method="post" action="<c:url value="/mng/lms/manage/manageReportList.do"/>">
			<strong>과정기간 : </strong>
			<%-- <c:set var="nowDate"><fmt:formatDate value="${now}" pattern="yyyy-MM-dd" /></c:set> --%>
			<input type="text" name="searchStartDate" value="${empty searchVO.searchStartDate ? '' : searchVO.searchStartDate}" id="searchStartDate" class="btn_calendar" placeholder="시작일" readonly="readonly"/> ~
			<input type="text" name="searchEndDate" value="${empty searchVO.searchEndDate ? '' : searchVO.searchEndDate}" id="searchEndDate" class="btn_calendar" placeholder="종료일" readonly="readonly"/>
			&nbsp;&nbsp;
			<strong>주관기관 : </strong>
	  		<select id="searchCtgryId" name="searchCtgryId">
				<option value="">전체</option>
				<c:forEach var="result" items="${ctgryList}" varStatus="status">
					<c:if test="${result.ctgryLevel eq '1' }">
						<option value="${result.ctgryId}" ${searchVO.searchCtgryId eq result.ctgryId ? 'selected' : '' } ><c:out value="${result.ctgryNm}"/></option>
					</c:if>
				</c:forEach>
			</select>
			<br/>
			<strong>운영보고서 유형 : </strong>
	  		<select id="reportType" name="reportType">
				<option value="" ${empty searchVO.reportType ? 'checked' : ''}>전체</option>
                        <option value="project" ${searchVO.reportType eq 'project'? 'checked' : ''}>프로젝트 유형</option>
                        <option value="common" ${searchVO.reportType eq 'common' ? 'checked' : ''}>일반 유형</option>
			</select>
			&nbsp;&nbsp;
			<strong>과정명: </strong>
			<input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" class="inp_s" id="searchCrclNm" />
			<br/>
			<strong>과정상태 : </strong>
	  		<select id="searchProcessSttusCodeDate" name="searchProcessSttusCodeDate">
				<option value="">전체</option>
				<c:forEach var="result" items="${statusComCode}" varStatus="status">
					<option value="${result.code}" ${searchVO.searchProcessSttusCodeDate eq result.code ? 'selected' : '' }>${result.codeNm}</option>
				</c:forEach>
			</select>
			&nbsp;&nbsp;
			<strong>책임교원 명: </strong>
			<input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" class="inp_s" id="searchUserNm" />
			&nbsp;&nbsp;
	  		<label><input type="checkbox" name="allSubmit" value="Y" ${searchVO.allSubmit eq 'Y' ? 'checked' : ''}/>과정만족도 완료</label>
			<br/>
			<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			<input type="button" id="resetBtn" onclick="fnReset();" value="초기화" style="width:73px;height:26px;"/>
		</form>
	</div>
	<div>
		<h2>총 ${resultListCnt } 개</h2>
	</div>
	<table class="chart_board">
	    <colgroup>
			<col width=50px;/>
			<col class="co6"/>
			<col class="co6"/>
			<col width="90px"/>
			<col width="100px;"/>
			<col class="co1"/>
			<col class="co1"/>
			<col class="co4"/>
		</colgroup>
	    <thead>
	      <tr>
	        <th>No</th>
	        <th>주관기관</th>
	        <th>과정기간</th>
	        <th>운영보고서 유형</th>
	        <th>과정상태</th>
	        <th>과정명</th>
	        <th>책임교원</th>
	        <th>과정만족도 완료여부</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<c:choose>
	    		<c:when test="${not empty resultList}">
			    	<c:forEach var="result" items="${resultList}" varStatus="status">
				    	<c:url var="viewUrl" value="/mng/lms/manage/manageReport.do">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:param name="menu" value="CLASS_MANAGE"/>
							<c:param name="depth1" value="CRCL_REPORT"/>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'">
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<td>${result.ctgryNm}</td>
			    			<td>${result.startDate} ~ ${result.endDate }</td>
			    			<td>${result.reportType eq 'common' ? '일반 유형' : '프로젝트 유형'}</td>
			    			<td>
			    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
			    					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
								</c:forEach>
			    			</td>
			    			<td>${result.crclNm }</td>
			    			<td>${result.userNm }</td>
			    			<td>
		                      	<c:choose>
		                      		<c:when test="${curDate lt result.endDate }">
										대기
		                      		</c:when>
		                      		<c:otherwise>
		                      			<c:url var="popUrl" value="/lms/crcl/popCurriculumSurveyView.do">
						  					<c:param name="crclId" value="${result.crclId}"/>
						  					<c:param name="schdulId" value="${result.surveySatisfyType}"/>
						  					<c:param name="adminAt" value="Y"/>
						   				</c:url>
		                      			<c:choose>
				                      		<c:when test="${result.memberCnt ne 0 and result.memberCnt eq result.joinCnt }">
				                      			<a href="#none" onclick="window.open('${popUrl}','팝업창','width=800,height=800');event.stopPropagation();return false;" target="_blank"><span class='font-point'>전원제출<br>(${result.joinCnt }/${result.memberCnt })</span></a>
				                      		</c:when>
				                      		<c:otherwise>
				                      			<a href="#none" onclick="window.open('${popUrl}','팝업창','width=800,height=800');event.stopPropagation();return false;" target="_blank"><span class='font-point'>${result.joinCnt }/${result.memberCnt }명</span></a>
				                      		</c:otherwise>
				                      	</c:choose>
		                      		</c:otherwise>
		                      	</c:choose>
		                      </td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</tbody>
    </table>

 	 <div id="paging">
		<div id="paging">
	    	<c:url var="pageUrl" value="/mng/lms/manage/manageReportList.do${_BASE_PARAM}">
	    </c:url>
	    <ul><ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl }" /></ul>
	</div>
</div>
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>
