<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty brdMstrVO.bbsId}"><c:param name="bbsId" value="${brdMstrVO.bbsId}" /></c:if>
	<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
	<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
	<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<script>
$(document).ready(function(){

});

function fnStatistics(tempCrclId, tempBbsId){
	var tempMenu = "${param.menu}";
	var tempDepth1 = "${param.depth1}";
	location.href = "/mng/lms/manage/lmsBbsStatistics.do?crclId="+tempCrclId+"&bbsId="+tempBbsId+"&menu="+tempMenu+"&depth1="+tempDepth1;
}
</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="${param.menu eq 'CLASS_MANAGE' ? '7' : '6' }"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>

	<ul class="box_bbsmst">
		<c:forEach var="result" items="${masterList}">
			<c:url var="masterUrl" value="/mng/lms/manage/lmsBbsList.do">
				<c:param name="crclId" value="${searchVO.crclId}"/>
				<c:param name="bbsId" value="${result.bbsId}" />
				<c:param name="menu" value="${param.menu eq 'CLASS_MANAGE' ? 'CLASS_MANAGE' : 'CURRICULUM_MANAGE' }"/>
				<c:param name="depth1" value="${param.menu eq 'CLASS_MANAGE' ? param.depth1 : 'BASE_CRCL' }"/>
			</c:url>
			<li <c:if test="${result.bbsId eq brdMstrVO.bbsId}">class="on"</c:if>>
				<a href="${masterUrl}">
					<c:choose>
						<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
						<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
					</c:choose>
					<c:out value="${result.bbsNm}"/>
				</a>
			</li>
		</c:forEach>
	</ul>

	<div id="bbs_search">
		<form name="frm" method="post" action="/mng/lms/manage/lmsBbsList.do">
			<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
			<input type="hidden" name="sysTyCode" value="${searchVO.sysTyCode}"/>
			<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
			<input type="hidden" name="trgetId" value="${searchVO.trgetId}"/>
			<input name="searchCate" type="hidden" value="${searchVO.searchCate}" />
			<input type="hidden" name="searchCnd" value="0"/>

			<fieldset>
				<legend>검색조건입력폼</legend>
				<c:if test="${!empty brdMstrVO.ctgrymasterId}">
			        <label for="cate" class="hdn"><spring:message code="cop.category.view" /></label>
			        <c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
						<c:choose>
							<c:when test="${status.first}">
								<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})">
									<option value="">선택</option>
									<c:forEach var="cate" items="${boardCateList}">
										<c:if test="${cate.ctgryLevel eq 1 }">
											<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
										</c:if>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise><select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})"><option value="">선택</option></select></c:otherwise>
						</c:choose>
					</c:forEach>

					<script type="text/javascript">
						fnCtgryInit('${searchVO.searchCateList}');
					</script>
			    </c:if>
			    <%--
				<label for="ftext" class="hdn">분류검색</label>
				<select name="searchCnd" id="ftext">
					<!-- option selected value=''>--선택하세요--</option-->
		          	<option value="0" <c:if test="${searchVO.searchCnd eq '0'}">selected="selected"</c:if>>제목</option>
		          	<option value="1" <c:if test="${searchVO.searchCnd eq '1'}">selected="selected"</c:if>>내용</option>
		          	<option value="2" <c:if test="${searchVO.searchCnd eq '2'}">selected="selected"</c:if>>작성자</option>
				</select>
				 --%>
				<c:choose>
					<c:when test="${brdMstrVO.sysTyCode eq 'CLASS'}">
						<strong>반별 검색 : </strong>
						<select name="searchCnd" id="ftext1">
							<option selected value=''>전체</option>
							<c:if test="${not empty selectClassList }">
								<c:forEach items="${selectClassList}" var="result">
									<option value="${result.classCnt }" >${result.classCnt }반</option>
								</c:forEach>
							</c:if>
						</select>
					</c:when>
					<c:when test="${brdMstrVO.sysTyCode eq 'GROUP'}">
						<strong>조별 검색 : </strong>
						<select name="searchCnd" id="ftext2">
							<option selected value=''>전체</option>
							<c:if test="${not empty selectGroupList }">
								<c:forEach items="${selectGroupList}" var="result">
									<option value="${result.groupCnt }" >${result.groupCnt }조</option>
								</c:forEach>
							</c:if>
						</select>
					</c:when>
				</c:choose>
				<label for="inp_text" class="hdn">검색어입력</label>
				<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" class="inp_s" id="inp_text" />
				<input type=image src="${_IMG}/btn/btn_search.gif" alt="검색" />
			</fieldset>
		</form>
	</div>

	<form id="listForm" name="listForm" method="post">
		<input type="hidden" id="registAction" name="registAction"/>
		<input type="hidden" id="bbsId" name="bbsId" value="<c:out value="${brdMstrVO.bbsId}"/>"/>
		<input type="hidden" id="trgetId" name="trgetId"/>
		<input type="hidden" id="ctgryId" name="ctgryId"/>
		<c:url var="_LIST_HIDDEN_URL" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			<c:param name="pageIndex" value="${searchVO.pageIndex}" />
		</c:url>
		<input type="hidden" id="returnUrl" name="returnUrl" value="<c:out value="${_LIST_HIDDEN_URL}"/>"/>

		<%-- <p class="total">총 게시물 ${paginationInfo.totalRecordCount}건 ㅣ 현재페이지 <strong class="green">${paginationInfo.currentPageNo}</strong>/${paginationInfo.totalPageCount}</p> --%>
				<table class="chart_board" summary="${brdMstrVO.bbsNm}에대한 번호, 제목, 작성자, 첨부, 적성일, 조회순으로 나타낸 목록표" >
					<caption class="hdn">${brdMstrVO.bbsNm} 목록</caption>
					<colgroup>
						<%-- <col width="65px" /> --%>
						<col width="65px" />
						<c:if test="${not empty brdMstrVO.ctgrymasterId}">
							<col width="70px" />
						</c:if>
						<col width="*" />
						<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
							<col width="60px" />
						</c:if>
						<col width="70px" />
						<col width="85px" />
						<%-- <col width="70px" /> --%>
					</colgroup>
					<thead>
						<tr>
							<!-- <th class="first"><input type="checkbox" id="checkAll" value="all"/><label for="checkAll">선택</label></th> -->
							<th><spring:message code="cop.nttNo"/></th>
							<c:if test="${not empty brdMstrVO.ctgrymasterId}">
								<th>구분</th>
							</c:if>
							<th><spring:message code="cop.nttSj" /></th>
							<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
								<th><spring:message code="cop.listAtchFile" /></th>
							</c:if>
				          	<th><spring:message code="cop.ntcrNm" /></th>
							<th class="last">등록일</th>
							<%-- <th class="last"><spring:message code="cop.inqireCo" /></th> --%>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="result" items="${resultList}" varStatus="status">
						<c:url var="viewUrl" value="/mng/lms/manage/lmsBbsView.do${_BASE_PARAM}">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
					    </c:url>
						<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
							<%-- <td class="check"><input type="checkbox" name="nttNoArr" value="${result.nttNo}" title="선택"/></td> --%>
							<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
							<c:if test="${not empty brdMstrVO.ctgrymasterId}">
								<td><c:out value="${result.ctgryNm}" /></td>
							</c:if>
							<td class="tit">
								<c:if test="${result.ordrCodeDp ne 0}">
								  <img src="${_C_IMG}/sub/board/blank_bg.gif" width="${result.ordrCodeDp * 19}" height="0" alt="${result.ordrCodeDp} Depth" />
						          <img src="${_C_IMG}/sub/board/icon_re.gif" alt="reply arrow" />
						        </c:if>
								<a href="${viewUrl}" class="notice_ti"><c:out value="${result.nttSj}" /></a>
								<c:if test="${result.othbcAt eq 'N'}">
									<img src="${_C_IMG}/sub/board/icon_lock.gif" alt="비밀글" />
								</c:if>
								<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
									<strong class="org">[<c:out value="${result.commentCount}" />]</strong>
								</c:if>
							</td>
							<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
								<c:choose>
						          <c:when test="${not empty result.atchFileId}">
						          	<td><img src="${_C_IMG}/sub/board/icon_file.gif" alt="첨부파일" /></td>
						          </c:when>
						          <c:otherwise>
						           	<td>-</td>
						          </c:otherwise>
						        </c:choose>
							</c:if>
				          	<td><c:out value="${result.ntcrNm}"/></td>
						  	<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
							<%-- <td><c:out value="${result.inqireCo}"/></td> --%>
					</c:forEach>
					<c:if test="${fn:length(resultList) == 0}">
				      <tr>
				        <td colspan="10"><spring:message code="common.nodata.msg" /></td>
				      </tr>
				    </c:if>
					</tbody>
				</table>
				<div class="btn_all">
					<%--
					<div class="fL">
						<input type="image" id="btnManageMove" src="${_IMG}/btn_move.gif" alt="이동" />
						<input type="image" id="btnManageCopy" src="${_IMG}/btn_copy.gif" alt="복사" />
						<input type="image" id="btnManageHide" src="${_IMG}/btn_close.gif" alt="삭제" />
						<input type="image" id="btnManageRemove" src="${_IMG}/btn_del_all.gif" alt="완전삭제" />
						<input type="image" id="btnManageRepair" src="${_IMG}/btn_repair.gif" alt="복구" />
					</div>
					 --%>
					<div class="fR">
						<c:url var="addBoardArticleUrl" value="/mng/lms/manage/lmsBbsAdd.do${_BASE_PARAM}">
							<c:param name="registAction" value="regist" />
						</c:url>
						<a href="${addBoardArticleUrl}"><img src="${_IMG}/btn/btn_write.gif" alt="글쓰기" /></a>
						<input type="button" id="resetBtn" onclick="fnStatistics('${searchVO.crclId}','${searchVO.bbsId}');" value="통계" style="width:73px;height:26px;">
					</div>
				</div>
 			</form>
				<div id="paging">
					<c:url var="pageUrl" value="/mng/lms/manage/lmsBbsList.do${_BASE_PARAM}">
					</c:url>
				    <ul>
				      <ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="${pageUrl}" />
				    </ul>
				</div>

	<br/><br/><br/><br/>

<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>