<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="CURRICULUM_MANAGE"/>
	<c:param name="depth1" value="BASE_CRCL"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="과정등록관리"/>
</c:import>

<script>
$(document).ready(function(){
	//게시판 추가
	$(".btn_cancel").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정을 취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//게시판 추가
	$(".btn_addbbs").click(function(){
		var addHtml = "",
			ctgrymasterId = "";
		
		$.ajax({
			url : "/mng/cop/bbs/ctg/insertBBSCtgryMaster.json"
			, type : "post"
			, dataType : "json"
			, data : {siteId : "<c:out value="${curriculumVO.crclId }"/>", ctgrymasterNm : "<c:out value="${curriculumVO.crclId }"/>"}
			, success : function(data){
				if(data.successYn == "Y"){
					ctgrymasterId = data.ctgrymasterId;
				}
			}, error : function(){
				alert("error");
			}
		}).done(function(){
			addHtml = "<tr class='list'>"
				+ 	"<td>"
				+		($("tr.list").length + 1)
				+		"<input type='hidden' name='bbsIdList' value=''/>"
				+	"</td>"
				+	"<td>"
				+		"<select name='sysTyCodeList'>"
				+			"<option value='ALL'>전체</option>"
				+			"<option value='INDIV'>개별</option>"
				+			"<option value='GROUP'>조별</option>"
				+			"<option value='CLASS'>반별</option>"
				+		"</select>"
				+	"</td>"
				+	"<td>"
				+		"<a href='#' class='btn_addctgry btn_mngTxt' data-id='"+ctgrymasterId+"'>게시글 구분 태그등록</a>"
				+		"<input type='hidden' name='ctgrymasterIdList' value='"+ctgrymasterId+"'/>"
				+	"</td>"
				+	"<td><input type='text' class='wid100 required' name='bbsNmList' placeholder='게시판 명 입력'/></td>"
				+	"<td><a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'></a></td>"
				+ "</tr>";
		
			$(".box_bbs").append(addHtml);
		});
		return false;
	});
	
	//저장
	$(".btn_submit").click(function(){
		$(".required").each(function(){
			if(!$(this).val()){
				alert("게시판 명을 입력해주세요.");
				return false;
			}
		});
		
		$("#bbsControl").submit();
	});
	
});

//게시판 삭제
$(document).on("click", ".btn_del", function(){
	var bbsId = $(this).data("id"),
		ctgrymasterId = $(this).data("ctgry");
	
	$(this).parents("tr").remove();
	$.ajax({
		url : "/mng/lms/manage/lmsBbsDelete.do"
		, type : "post"
		, dataType : "json"
		, data : {crclId : "<c:out value="${curriculumVO.crclId }"/>", bbsId : bbsId, ctgrymasterId : ctgrymasterId}
		, success : function(data){
			if(data.successYn == "Y"){
				
			}
		}, error : function(){
			alert("error");
		}
	});
	return false;
});

//게시글 구분 태그등록
$(document).on("click", ".btn_addctgry", function(){
	var href = "/mng/cop/bbs/ctg/selectBBSCtgryList.do?type=N&ctgrymasterId=",
		id = $(this).data("id"),
		win = window.open(href+id ,'ctgy',' scrollbars=yes, resizable=yes, left=0, top=0, width=900,height=650');
	
	if(win != null) {
		win.focus();
	}
	return false;
});

</script>

	<table class="chart2">
		<tr>
			<td class="alC">
				<c:out value="${curriculumVO.crclNm }"/>
				<p>과정기간 : <c:out value="${curriculumVO.startDate }"/> ~ <c:out value="${curriculumVO.endDate}"/></p>
			</td>
		</tr>
	</table>
	<br/>
	<c:if test="${curriculumVO.processSttusCode > 0}">
		<c:import url="/mng/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="1"/>
			<c:param name="crclId" value="${curriculumVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
	</c:if>
	<strong>과정게시판</strong>&nbsp;&nbsp;<span>! 해당 과정에 필요한 만큼 과정게시판을 생성할 수 있습니다.</span>
	<a href="#" class="btn_addbbs btn_mngTxt fR">+ 게시판 추가</a>
	<br/><br/>
	<form id="bbsControl" method="post" action="/mng/lms/manage/lmsBbsInsert.do">
		<input type="hidden" name="crclId" value="<c:out value="${curriculumVO.crclId }"/>"/>
		<table class="chart_board">
			<colgroup>
				<col width="5%"/>
				<col width="10%"/>
				<col width="15%"/>
				<col width="*"/>
				<col width="15%"/>
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
				  	<th>사용자그룹</th>
				 	<th>게시글 구분 태그</th>
					<th>게시판 명</th>
					<th>관리</th>
			  	</tr> 
			</thead>
			<tbody class="box_bbs">
				<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
					<tr class="list">
						<td>
							<c:out value="${status.count}"/>
							<input type="hidden" name="bbsIdList" value="${result.bbsId}"/>
						</td>
						<c:choose>
							<c:when test="${status.count eq 1}">
								<td>
									<c:choose>
										<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
										<c:when test="${result.sysTyCode eq 'INDIV'}">개별</c:when>
										<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
										<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
									</c:choose>
									<input type="hidden" name="sysTyCodeList" value="${result.sysTyCode}"/>
								</td>
								<td>
									<a href="#" class="btn_addctgry btn_mngTxt" data-id="${result.ctgrymasterId}">게시글 구분 태그등록</a>
									<input type="hidden" name="ctgrymasterIdList" value="${result.ctgrymasterId}"/>
								</td>
								<td>
									<c:out value="${result.bbsNm}"/>
									<input type="hidden" name="bbsNmList" value="${result.bbsNm}"/>
								</td>
								<td>필수 게시판(삭제불가)</td>
							</c:when>
							<c:otherwise>
								<td>
									<select name="sysTyCodeList">
										<option value="ALL" <c:if test="${result.sysTyCode eq 'ALL'}">selected="selected"</c:if>>전체</option>
										<option value="INDIV" <c:if test="${result.sysTyCode eq 'INDIV'}">selected="selected"</c:if>>개별</option>
										<option value="GROUP" <c:if test="${result.sysTyCode eq 'GROUP'}">selected="selected"</c:if>>조별</option>
										<option value="CLASS" <c:if test="${result.sysTyCode eq 'CLASS'}">selected="selected"</c:if>>반별</option>
									</select>
								</td>
								<td>
									<a href="#" class="btn_addctgry btn_mngTxt" data-id="${result.ctgrymasterId}">게시글 구분 태그등록</a>
									<input type="hidden" name="ctgrymasterIdList" value="${result.ctgrymasterId}"/>
								</td>
								<td><input type="text" class="wid100 required" name="bbsNmList" value="<c:out value="${result.bbsNm}"/>" placeholder="게시판 명 입력"/></td>
								<td>
									<a href="${delUrl}" class="btn_del" data-id="${result.bbsId}" data-ctgry="${result.ctgrymasterId}"><img src="/template/manage/images/btn/del.gif"></a>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="btn_r">
			<a href="#" class="btn_submit btn_mngTxt">저장</a>
		    <c:url var="listUrl" value="/mng/lms/manage/lmsControl.do${_BASE_PARAM}"/>
		    <a href="${listUrl}"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a>
		</div>
	</form>
	<br/><br/><br/><br/>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>