<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_C_IMG" value="/template/common/images"/>

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
</head>
<body>
<br/>
<div id="cntnts">
	<div style="text-align:center">
		<c:choose>
			<c:when test="${fileInfo.fileExtNm eq '이미지'}">
				<img src='<c:url value='/cmm/fms/getImage.do'/>?siteId=SITE_000000000000001&amp;appendPath=<c:out value="${param.plId}"/>&amp;atchFileNm=${fileInfo.streFileNm}.${fileInfo.fileExtsn}&amp;fileStorePath=Study.fileStorePath'/>
			</c:when>
			<c:otherwise>
				<video  width="100%" height="300">
					<c:choose>
						<c:when test="${fn:indexOf(fileInfo.fileStreCours,'.mp4') ne -1}">
							<source src="${fileInfo.fileStreCours }" type="video/mp4; codecs="avc1.4D401E, mp4a40.2""></source>
						</c:when>
						<c:when test="${fn:indexOf(fileInfo.fileStreCours,'.ogg') ne -1}">
							<source src="${fileInfo.fileStreCours }" type="video/ogg; codecs="theora, vorbis""></source>
						</c:when>
					</c:choose>
			  </video>
			</c:otherwise>
		</c:choose>
	</div>
    <div class="btn_c">
		<a href="/cmm/fms/FileDown.do?atchFileId=${fileInfo.atchFileId}&amp;fileSn=${fileInfo.fileSn}" class="btn_ok" ><img src="/template/common/images/sub/board/btn_download.gif"></a>
	</div>
</div>