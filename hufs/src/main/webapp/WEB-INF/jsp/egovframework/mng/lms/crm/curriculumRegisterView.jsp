<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/mng/template/top.do" charEncoding="utf-8">
	<c:param name="menu" value="COURSEREG_MANAGE"/>
	<c:param name="depth1" value="CURSEREG"/>
	<c:param name="depth2" value=""/>
	<c:param name="title" value="수강신청"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
$(document).ready(function(){

});
</script>
	<table class="chart2 mb50">
		<tr>
			<td colspan="3" class="alC">수강신청기간 : <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
		</tr>
		<tr>
			<td class="alC"><c:out value="${curriculumVO.crclNm }"/></td>
			<td class="alC">
				<ul>
					<c:forEach var="user" items="${subUserList}" varStatus="status">
						<li>
							<c:out value="${user.mngDeptNm}"/>(책임) <c:out value="${user.userNm}"/>
						</li>
					</c:forEach>
				</ul>
			</td>
			<td class="alC"><a href="/mng/lms/crcl/selectCurriculum.do?crclId=<c:out value="${param.crclId}" />" class="btn_mngTxt">과정관리</a></td>
		</tr>
	</table>

	<c:import url="/mng/lms/curseregtabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="2"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
	</c:import>

	<table class="chart2 mb50">
		<colgroup>
			<col width="20%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>과정명</label></th>
				<td colspan="5">
					<c:out value="${curriculumVO.crclNm }"/>
				</td>
			</tr>
			<c:if test="${curriculumVO.tuitionAt eq 'Y'}">
			<tr>
				<th><label>등록금액</label></th>
				<td>
					수업료 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.tuitionFees}" />원
					등록비 : <fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.registrationFees}" />원
				</td>
			</tr>
			<tr>
				<th><label>등록기간</label></th>
				<td>
					<c:out value="${curriculumVO.tuitionStartDate}"/> ~ <c:out value="${curriculumVO.tuitionEndDate}"/>
				</td>
			</tr>
			</c:if>
			<tr>
				<th><label>수강신청기간</label></th>
				<td>
					<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
				</td>
			</tr>
		</tbody>
	</table>

	<c:if test="${curriculumbaseVO.stdntAplyAt eq 'Y'}">
		<strong>
			수강신청 제출서류
			<br />
			<span style="color:red;">(파일을 다운로드 받아 모두 작성하신 후, 수강신청 시 반드시 작성한 파일을 첨부해 주세요.)</span>
		</strong>
		<table class="chart2 mb50">
			<colgroup>
				<col width="20%"/>
				<col width="*"/>
			</colgroup>
			<tbody>
				<tr class="stdntAply">
					<th><label>신청서</label></th>
					<td colspan="3">
						<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.aplyFile}&amp;fileSn=0">
							<img src='/template/manage/images/ico_file.gif' alt='파일'/> <c:out value="${curriculumbaseVO.aplyFileNm}"/>
						</a>
					</td>	
				</tr>
				<tr class="stdntAply">
					<th><label>계획서</label></th>
					<td colspan="3">
						<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.planFile}&amp;fileSn=0">
							<img src='/template/manage/images/ico_file.gif' alt='파일'/> <c:out value="${curriculumbaseVO.planFileNm}"/>
						</a>
					</td>	
				</tr>
				<tr class="stdntAply">
					<th><label>기타</label></th>
					<td colspan="3">
						<a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.etcFile}&amp;fileSn=0">
							<img src='/template/manage/images/ico_file.gif' alt='파일'/> <c:out value="${curriculumbaseVO.etcFileNm}"/>
						</a>
					</td>	
				</tr>
			</tbody>
		</table>
	</c:if>
	
	<strong>1. 교육과정 내용</strong>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="*"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<tbody>
			<tr>
				<th><label>과정기간</label></th>
				<td>
					<c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/>
				</td>
				<th><label>강의시간</label></th>
				<td>
					<c:out value="${curriculumVO.totalTime}"/>시간
				</td>
			</tr>
			<tr>
				<th><label>과정 진행 캠퍼스</label></th>
				<td colspan="3">
					<c:out value="${curriculumVO.campusNm}"/>
					(<c:out value="${curriculumVO.campusPlace}"/>)
				</td>
			</tr>
			<tr>
				<th><label>과정 개요 및 목표</label></th>
				<td colspan="3">
					<c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/>
				</td>
			</tr>
			<tr>
				<th><label>과정의 기대효과</label></th>
				<td colspan="3">
					<c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/>
				</td>
			</tr>			
		</tbody>
	</table>
	
	<strong>2. 과정내용</strong>
	<table class="chart2 mb50">
		<colgroup>
			<col width="15%"/>
			<col width="15%"/>
			<col width="*"/>
		</colgroup>
		<caption>등록폼</caption>
		<tbody>
			<tr>
				<th class="alC">번호</th>
				<th class="alC">내용범주</th>
				<th class="alC">내용</th>
			</tr>
		</tbody>
		
		<%-- 과정내용 번호 및 단원 묶어주는 역할 --%>	
		<c:set var="rowspan" value="0"/>
		<c:set var="rowspanList" value=""/>
		<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
		<c:forEach var="list" items="${lessonList}" varStatus="status">
			<c:choose>
				<c:when test="${prevCode eq list.lessonNm}">
					<c:set var="rowspan" value="${rowspan + 1}"/>
				</c:when>
				<c:otherwise>
					<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
					<c:set var="rowspan" value="1"/>
					<c:set var="prevCode" value="${list.lessonNm}"/>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
		<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
		<c:set var="listCnt" value="${rowspan[0]}"/>
		<c:set var="rowspanCnt" value="0"/>
		
		<c:set var="prevCode" value=""/>
		<c:set var="number" value="1"/>
		
		<c:forEach var="result" items="${lessonList}" varStatus="status">
			<c:if test="${prevCode ne result.lessonNm}">
				<tbody class='box_lesson'>
			</c:if>
			<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
				<c:if test="${prevCode ne result.lessonNm}">
					<td class="line alC num" rowspan="${rowspan[rowspanCnt] + 1}">
						<c:out value="${number}"/>
						<c:set var="number" value="${number + 1}"/>
					</td>
					<td rowspan='${rowspan[rowspanCnt] + 1}' class='line lesnm'><c:out value="${result.lessonNm}"/></td>
					<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
				</c:if>
				<td class='line'><c:out value="${result.chasiNm}"/></td>
			</tr>
			<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
				</tbody>
			</c:if>
			
			<c:set var="prevCode" value="${result.lessonNm}"/>
		</c:forEach>
	</table>
	
	<strong>3. 교원 배정</strong>
	<br />
	<strong>과정책임부서 : <c:out value="${curriculumVO.hostCodeNm}"/></strong>
	<table class="chart2">
		<caption>등록폼</caption>
		<colgroup>
			<col width="30%"/>
			<col width="30%"/>
			<col width="40%"/>
		</colgroup>
		<tbody>
			<tr>
				<th class="alC">교원배정</th>
				<th class="alC">소속</th>
				<th class="alC">이메일</th>
			</tr>
		</tbody>
		 <c:forEach var="result" items="${facList}" varStatus="status">
			<tbody class='box_staff'>
				<tr>
					<td class="line"><c:out value="${result.userNm}"/></td>
					<td class="line"><c:out value="${result.mngDeptNm}"/></td>
					<td class="line"><c:out value="${result.emailAdres}"/></td>
				</tr>
			</tbody>
		</c:forEach>
	</table>
	<div class="mb50" style="color:red;"><strong>※ 교수진은 과정 진행 중 변경될 수 있습니다.</strong></div>
	
	<strong>4. 성적</strong>
	<%-- 
	<c:choose>
	<c:when test="${curriculumVO.evaluationAt eq 'Y' and not empty curriculumVO.gradeType}">
		절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
	</c:when>
	<c:otherwise>상대평가</c:otherwise>
	</c:choose>
	 --%>
	<table class="chart2">
		<caption>등록폼</caption>
		<tbody>
			<c:choose>
				<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
					<tr>
						<th class="alC">Grade</th>
						<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
								<th class="alC"><c:out value="${result.ctgryNm}"/></th>
							</c:if>
						</c:forEach>
					</tr>
					<tr class="alC">
						<c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
						<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
						<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
						<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
						<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
						<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
						<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
						<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
						<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
						<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
						<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
						
						<td class="line alC">점수</td>
						<td class="line alC">${aplus}</td>
						<td class="line alC">${a}</td>
						<td class="line alC">${bplus}</td>
						<td class="line alC">${b}</td>
						<td class="line alC">${cplus}</td>
						<td class="line alC">${c}</td>
						<td class="line alC">${dplus}</td>
						<td class="line alC">${d}</td>
						<td class="line alC">${f}</td>
					</tr>
				</c:when>
				<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
					<tr>
						<th class="alC">P/F</th>
						<th class="alC" colspan="2">점수구간</th>
					</tr>
					<tr class="alC">
						<td class="line">Pass</td>
						<td class="alC">${curriculumVO.pass}</td>
						<td class="alC">~ 100</td>
					</tr>
					<tr class="alC">
						<td class="line">Fail</td>
						<td class="alC">${curriculumVO.fail}</td>
						<td class="alC">~ 0</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<th class="alC">Grade</th>
						<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
							<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
								<th class="alC"><c:out value="${result.ctgryNm}"/></th>
							</c:if>
						</c:forEach>
					</tr>
					<tr>
						<td class="line alC">비중</td>
						<td colspan="2" class="line alC">상위 30%</td>
						<td colspan="2" class="line alC">상위 65%이내</td>
						<td colspan="5" class="line alC">66% ~ 100%</td>
					</tr>
					<%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
					<tr>
						<td class="line alC">가이드 학생 수</td>
						<td colspan="2" class="line alC">30</td>
						<td colspan="2" class="line alC">35</td>
						<td colspan="5" class="line alC">35</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	<div class="mb50" style="color:red;"><strong>※ 성적 기준은 과정 진행 중 변경될 수 있습니다.</strong></div>
	
	<strong>5. 교재 및 부교재</strong>
	<ul class="book_list mb50">
		<c:forEach var="result" items="${bookList}" varStatus="status">
			<li>
				<div class="textbook-img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=SITE_000000000000001&amp;appendPath=<c:out value="${result.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);"></div>
				<div class="textbook_right">
					<div class="book_title"><c:out value="${result.nttSj}"/></div>
					<div class="book_content">출판사 : <c:out value="${result.tmp01}"/></div>
					<div class="book_content">저자 : <c:out value="${result.tmp02}"/></div>
				</div>
			</li>
		</c:forEach>
	</ul>

	<strong>6. 수강취소 및 환불처리 안내</strong>
	<div>
		<a href="/mng/lms/crm/curseregManageCancelRule.do" class="btn_mngTxt">수강신청 / 환불처리 안내</a>
	</div>

	<div class="btn_c">
	    <c:url var="listUrl" value="/mng/lms/crm/curseregManage.do${_BASE_PARAM}"/>
	    <%-- <a href="${listUrl}" class="alR"><img src="${_IMG}/btn/btn_list.gif" alt="목록"/></a> --%>
	</div>
	
<c:import url="/mng/template/bottom.do" charEncoding="utf-8"/>