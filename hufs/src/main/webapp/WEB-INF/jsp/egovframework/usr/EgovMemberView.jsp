<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000126" />
</c:url>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>	

<style type="text/css">
	.btn_w{float:left;margin-left:5px;}
	.out{overflow:hidden;width:100%;}
</style>

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	var mypageFlag = "${mypageFlag}";
	if(mypageFlag == "mypage"){
		$('.manageArea').hide();
	}
	
	$('#btn-upload').click(function(e){
		e.preventDefault();
		$('#file').click();
	});
	
	$('.chgPassword').click(function(){ // 비밀번호 변경 확인 버튼 클릭
		var org_password = $('#password').val();
		var chg_password = $('#password2').val();
		var chk_chg_password = $('#chk_password2').val();
		
		if(chg_password === chk_chg_password){ // 새로운 비밀번호 입력이 동일하다면
			$.ajax({
				type:"post",
				dataType:"json",
				url:"/uss/umt/cmm/changePassword.json",
				data:"password="+org_password+"&password2="+chg_password,
				success: function(data) {
					console.log(data.successYn);
					if(data.successYn == "Y"){
						$("#openModal").hide();
						$("#confirmModal").show();
					}else{
						$("#confirmModal").hide();
						$("#openModal").show();
					}
					$(".modal-text").text(data.msg);
				},
				error:function() {
					//alert("error");
				}
			});
		}else{
			$(".modal-text").text("새로운 비밀번호 입력이 동일하지 않습니다.");
			$("#confirmModal").hide();
			$("#openModal").show();
		}
	});
});

	function fnResetPwd(){
		if(confirm("${userManageVO.userNm} 교수의 패스워드를 초기화 하시겠습니까?\n(초기 비밀번호 : 1234)로 변경됨")){
			$(document).ready(function(){
				$.ajax({
					url: '/uss/umt/cmm/EgovResetPwd.do',
					async: true,
					type: 'POST',
					data: {
						userId: '${userManageVO.userId}'
					},
					dataType: 'text',
					success: function(){
						alert("패스워드 초기화가 완료되었습니다.\n(교수:1234)로 초기화되었습니다.");
					}
				});
			});
		}
	}
	
	function fnModify(){
		$('.txtModify').show();
		$('.txtView').hide();
	}
	
	function fnUpdateImgPath(){
		$('#file').val("");
		$('.img-frame').attr("style", "background-image: url(/template/lms/imgs/common/icon_user_name.svg)")
	}

</script>

<style type="text/css">
#file{display:none;}
</style>

		
<section class="page-content-body">
            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <h3 class="title">
                    교원조회
                    <div class="desc">[교수] 시스템 등록일 : <fmt:formatDate value="${userManageVO.frstRegistPnttm}" pattern="yyyy-MM-dd"/> (<fmt:formatDate value="${userManageVO.frstRegistPnttm}" pattern="E"/>)</div>
                  </h3>
                </div>
              </div>
              <form:form commandName="userManageVO" id="userManageVO" name="userManageVO" method="post" enctype="multipart/form-data" action="/uss/umt/cmm/EgovMberModify.do">
              <form:hidden path="userId" value="${userManageVO.userId}"/>
              <div class="content-body">
                <table class="common-table-wrap table-style2 ">
                  <tbody>
                    <tr>
                      <th class="title vertical-mid">프로필 사진 </th>
                      <td>
                        <div class="profile-wrap flex-row">
	                          <div class="flex-col-auto">
	                          	<c:choose>
									<c:when test="${empty userManageVO.photoStreFileNm}"><div class="img-frame" style="background-image: url(/template/lms/imgs/common/icon_user_name.svg);"></div></c:when>
									<c:otherwise><div class='img-frame' style='background-image: url(${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>);'></div></c:otherwise>
								</c:choose>
	                          </div>
	                        <div class="flex-col-auto txtModify" style="display:none;">
								<input type="file" id="file" name="userPicFile" onchange="changeValue(this)"/>
	                          <button class="btn-sm-90 btn-outline-gray" id="btn-upload" type="button">사진 변경</button>
	                        </div>
	                        <div class="flex-col-auto txtModify" style="display:none;">
	                          <button class="btn-sm-90 btn-outline-gray" onclick="javascript:fnUpdateImgPath();" type="button">사진 삭제</button>
	                        </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">이름 <i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                             <c:out value="${userManageVO.userNm}"/> </div>
                          </div>
                          <div class="flex-col-auto">
                            <i class='icon-state bg-point'> <c:out value="${userManageVO.workStatusCodeNm}"/></i> </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">성별 <i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                              <c:out value="${userManageVO.sexdstn eq 'M' ? '남자' : '여자'}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">LMS 계정 사용 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                              <c:choose>
								<c:when test="${userManageVO.confmAt eq 'Y'}">
									LMS 사용중
								</c:when>
								<c:otherwise>
									<span style="font-color:red;">LMS 사용안함</span>
								</c:otherwise>
							</c:choose> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">사번 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                              <c:out value="${userManageVO.userSchNo}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">직위 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                              <c:out value="${userManageVO.positionCodeNm}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">소속 <i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                             <c:out value="${userManageVO.groupCode}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">주관기관 <i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                             <c:out value="${userManageVO.mngDeptCodeNm}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">교육과정 언어<i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto txtView">
                            <div class="table-data">
                              <c:out value="${userManageVO.majorNm}"/> 
                            </div>
                          </div>
                          
                          <div class="flex-col-6 txtModify" style="display:none;">
                            <form:select path="major" class="table-select select2 " data-select="style1" data-placeholder="언어를 선택해주세요.">
                            	
	                              <option value=""></option>
	                              <c:forEach var="result" items="${langList}" varStatus="status">
									<c:if test="${result.ctgryLevel ne '0' }">
										<option value="${result.ctgryId}" <c:if test="${userManageVO.majorNm eq result.ctgryNm}">selected="selected"</c:if>>${result.ctgryNm }</option>
									</c:if>
								  </c:forEach>
							  
                            </form:select> 
                          </div>
                          
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">연락처 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto txtView">
                            <div class="table-data">
                              <c:if test="${not empty userManageVO.geocode}">(${userManageVO.geocode})</c:if>
                              <c:out value="${userManageVO.moblphonNo}"/> </div>
                          </div>
                          
                          <div class="flex-col-4 txtModify" style="display:none;">
                            <form:select class="table-select select2" path="geocode" data-select="style1">
								<option value="82">대한민국 +82</option>
								<c:forEach var="result" items="${telNumList}" varStatus="status">
									<c:if test="${result.code ne '82'}">
										<option value="${result.code}">${result.codeNm} +${result.code}</option>
									</c:if>
								</c:forEach>
						  	</form:select>
                          </div>
                          <div class="flex-col-4 txtModify" style="display:none;">
                            <form:input class="table-input" path="moblphonNo" value="${fn:replace(userManageVO.moblphonNo, '-', '')}"/>
                          </div>
                        </div>
                        
                        <div class="flex-row txtModify" style="display:none;">
                          <div class="flex-col-8 ml-auto">
                            <p class="table-guide">(-)를 제외하고 번호만 입력해주세요.</p>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">이메일 <i class="font-point font-400">*</i> </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                             <c:out value="${userManageVO.emailAdres}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">아이디 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-auto">
                            <div class="table-data">
                              <c:out value="${userManageVO.userId}"/> </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="title">비밀번호 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col-6">
                            <!-- <button class="btn-sm-130 btn-outline-gray" onclick="fnResetPwd();return false;">비밀번호 초기화</button> -->
                            <button class="btn-sm-130 btn-outline-gray btnModalOpen" data-modal-type="pwchange">비밀번호 변경</button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              </form:form>
            </article>
            <div class="page-btn-wrap mt-50 mb-80">
              <a href="/uss/umt/cmm/EgovMberManage.do?menuId=MNU_0000000000000126" class="btn-xl btn-outline-gray manageArea">목록으로</a>
              <a href="javascript:fnModify();" class="btn-xl btn-point txtView">수정</a>
              
              <a href="#" onclick="window.location.reload();" class="btn-xl btn-outline-gray txtModify" style="display:none;">취소</a>
              <a href="javascript:$('#userManageVO').submit();" class="btn-xl btn-point txtModify" style="display:none;">저장</a>
            </div>
            <article class="content-wrap manageArea">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">
                    교육과정 이력
                    <div class="desc">
                      <div class="mr-20"><span class="font-basic">총 교육과정</span> 3건</div>
                      <div><span class="font-basic">수업진행</span> 6건</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:10%'>
                    <col style='width:9%'>
                    <col style='width:14%'>
                    <col style='width:10%'>
                    <col>
                    <col style='width:10%'>
                    <col>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>년도</th>
                      <th scope='col'>학기</th>
                      <th scope='col'>과정기간</th>
                      <th scope='col'>과정상태</th>
                      <th scope='col'>교육과정명</th>
                      <th scope='col'>담당직위</th>
                      <th scope='col'>수업명</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="list" items="${curriculumList}" varStatus="status">
	                    <tr class="">
	                      <td <c:if test="">rowspan=${result.currCnt}</c:if>>>${status.count}</td>
	                      <td>${list.crclYear}</td>
	                      <td>${list.crclTermNm}</td>
	                      <td>${list.startDate }<br>~<br>${list.endDate }</td>
	                      <td>
	                      	<c:forEach var="result" items="${statusComCode}">
								<c:if test="${list.code eq result.code}">
									<c:out value="${result.codeNm}"/>
								</c:if>
							</c:forEach>
	                      </td>
	                      <td class='left-align keep-all'><a href='#' class='underline keep-all'>${list.crclNm}</a></td>
	                      <td rowspan='${list.no}'>
	                      	<c:out value="${list.manageCode eq '10' ? '책임교원' : '강의책임교원' }"/>
	                      </td>
	                      <td class='left-align keep-all' >${fn:replace(list.studySubject, ',', '</td>') }</td>
	                    </tr>
                   </c:forEach>
                  </tbody>
                </table>
              </div>
            </article>
          </section>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>	

<div id="alert_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text"></p>
        <p class="modal-subtext"></p>
      </div>
      <div class="modal-footer">
      	<button type="button" id="confirmModal" style="display:none;" class="btn-xl btn-point btnModalConfirm">확인</button>
      	<button type="button" id="openModal" style="display:none;" class="btn-xl btn-point btnModalOpen" data-modal-type="pwchange">확인</button>
      	<button type="button" id="leaveOut" style="display:none;" class="btn-xl btn-point btnModalConfirm" onclick="location.href='/uat/uia/actionLogout.do'">확인</button>
      	<button type="button" id="openLeaveModal" style="display:none;" class="btn-xl btn-point btnModalOpen" data-modal-type="leave_id">확인</button>
      </div>
    </div>
  </div>
</div>

<div id="pwchange_modal" class="alert-modal">
  <div class="modal-dialog modal-top">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">비밀번호 변경</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">새로운 비밀번호를 등록해주세요.</div>
        <div class="pwchange-form-wrap">
	       <input type="text" id="password" placeholder="현재 비밀번호 입력"/>
	       <input type="text" id="password2" placeholder="새로운 비밀번호 입력"/>
	       <input type="text" id="chk_password2" placeholder="새로운 비밀번호 확인"/>
	    </div>
        <p class="pwchange-desc mt-15">* 비밀번호에 영어 대/소문자, 숫자, 특수문자를 조합하시면 비밀번호 안전도가 높아져 도용의 위험이 줄어듭니다.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button class="btn-xl btn-point btnModalOpen chgPassword" data-modal-type="alert" data-modal-size="modal-sm" data-modal-header="비밀번호 변경" data-modal-text="">확인</button>
      </div>
    </div>
  </div>
</div>