<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
<link rel="stylesheet" href="${CML}/css/my/my.css?v=2">
<link rel="stylesheet" href="${CML}/css/textbook/textbook.css?v=2">

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<%-- <c:choose>
<c:when test="${searchVO.tmplatImportAt ne 'N'}"> --%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
	<c:param name="listAt" value="Y"/>
</c:import>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<script src="${_C_JS}/board.js" ></script>

<script>
$(document).ready(function(){	
	
	//페이징 버튼
	$(".start, .prev, .next, .end").click(function(){
 		var url = $(this).data("url");
 		
 		location.href = url;
 	});
	
	//더보기
	$(".btn_more").click(function(){
		var curPage = $(this).data("page"),
			bbsId = $("input[name=bbsId]").val(),
			menuId = $("input[name=menuId]").val(),
			searchCate = $("input[name=searchCate]").val(),
			searchCateList = $("input[name=searchCateList]").val(),
			page = curPage + 1,
			lastPage = $("#lastPage").val();
		
		if(lastPage == curPage){
			alert("마지막 페이지 입니다.");
		}else{
			$.ajax({
				type : "post"
				, url : "/msi/ctn/moreBoardList.do"
				, data : {bbsId : bbsId, menuId : menuId, searchCate : searchCate, searchCateList : searchCateList, pageIndex : page}
				, dataType : "html"
				, success : function(data){
					$("#box_book").append(data);
					$(".btn_more").data("page", page);
					if(lastPage == page){
						$(".btn_more").hide();
					}
				}, error : function(){
					alert("데이터 불러 들이는데 실패했습니다.");
				}
			});	
		}
	});
});
</script>

	<section class="page-content-body">
            <article class="content-wrap">
            	<form id="frm" name="frm" method="post" action="<c:url value='/cop/bbs/selectLikeBookList.do'/>">
					<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
					<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
					<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
					<input name="searchCateList" type="hidden" value="<c:out value='${searchVO.searchCateList[0]}'/>" />
					<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>" />
					<input id="lastPage" type="hidden" value="${paginationInfo.totalPageCount}" />
					
			        <div class="box-wrap mb-45">
						<h3 class="title-subhead">교재/사전 검색</h3>
						<div class="flex-row-ten">
							<div class="flex-ten-col-4">
					            <div class="ell">
					            	<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" placeholder="교재/사전명"/>
					            </div>
							</div>
					        <div class="flex-ten-col-2">
					            <div class="ell">
					              <input name="searchTmp01" value="<c:out value="${searchVO.searchTmp01}"/>" type="text" placeholder="출판사 명">
					            </div>
					        </div>
					        <div class="flex-ten-col-2">
					            <div class="ell">
					              <input name="searchTmp02" value="<c:out value="${searchVO.searchTmp02}"/>" type="text" placeholder="저자명">
					            </div>
					        </div>
					        <div class="flex-ten-col-2 flex align-items-center">
					            <label class="checkbox">
					              <input type="checkbox" name="tmp03" value="Y" <c:if test="${searchVO.tmp03 eq 'Y'}">checked="checked"</c:if>>
					              <span class="custom-checked"></span>
					              <span class="text">e-book</span>
					            </label>
					        </div>
						</div>
						<button class="btn-sm font-400 btn-point mt-20" type="submit">검색</button>
					</div>
				</form>
            </article>
            <hr class="line-hr">
            <article class="content-wrap">
              <div class="textbook-wrap">
                <!-- 교재 리스트 -->
			    <c:forEach var="result" items="${resultList}" varStatus="status">
			    	<c:url var="viewUrl" value="/cop/bbs/selectBoardArticle.do">
					  	<c:param name="nttNo" value="${result.nttNo}" />
					  	<c:param name="menuId" value="MNU_0000000000000008" />
					  	<c:param name="bbsId" value="BBSMSTR_000000000005" />
				    </c:url>
				    
	                <div class="textbook-contents-view">
	                  <div class="left-area">
	                    <div class="textbook-img" onclick="location.href='${viewUrl}'" style="cursor: pointer;background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);"></div>
	                  </div>
	                  <div class="right-area">
	                    <div class="textbook-inner">
	                      <a href="${viewUrl}">
	                      	<%-- <c:set var="imgSrc">
								<c:choose>
		                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
		                    		<c:otherwise>
		                    			<c:url value='/cmm/fms/getImage.do'>
		                    				<c:param name="thumbYn" value="Y" />
		                    				<c:param name="siteId" value="SITE_000000000000001" />
		                    				<c:param name="appendPath" value="${result.bbsId}" />
		                    				<c:param name="atchFileNm" value="${result.atchFileNm}" />
		                    			</c:url>
		                    		</c:otherwise>
		                    	</c:choose>
							</c:set> --%>
	                        <div class="textbook-flag">
	                           <c:set var="imgSrc">
									<c:import url="/lms/common/flag.do" charEncoding="utf-8">
										<c:param name="ctgryId" value="${result.ctgryId}"/>
									</c:import>
				          		</c:set>
								<span class="flag-img">
									<img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
								</span>
								<span class="text"><c:out value="${result.ctgryNm}"/> 교재</span>
	                        </div>
	                        <div class="book-name">
			                	<c:if test="${result.tmp03 eq 'Y'}">
			                		<span class="type">e-book</span>
			                	</c:if>
			                	<c:out value="${result.nttSj}"/>
			                </div>
	                        <dl class="book-info">
	                          <dt class="info-title">출판사</dt>
	                          <dd class="info-name">${result.tmp01}</dd>
	                        </dl>
	                        <dl class="book-info">
	                          <dt class="info-title">저자</dt>
	                          <dd class="info-name">${result.tmp02}</dd>
	                        </dl>
	                        <div class="desc dotdotdot">
		                         <c:out value="${result.nttCn}" escapeXml="false"/>
	                        </div>
	                      </a>
	                    </div>
	                    <div class="textbook-button">
	                      <a href="${viewUrl}" class="btn-full btn-outline">상세보기</a>
	                      <%-- <a href="<c:out value="${result.tmp05}"/>" class="btn-full btn-point" target="_blank">구매하기</a> --%>
	                      <div class="util-wrap">
			                <button class="btn-share btnModalOpen" data-modal-type="share" onclick="location.href='http://${siteInfo.siteUrl}${viewUrl}'" data-title="<c:out value="${result.nttSj}"/>" title="공유하기"></button>
			                <c:choose>
			                	<c:when test="${not empty USER_INFO.id}">
			                		<c:set var="wishAt" value=""/>
			                		<c:forEach var="wishList" items="${wishList}" varStatus="status">
			                			<c:if test="${wishList.trgetId eq result.nttNo}">
			                				<c:set var="wishAt" value="on"/>
			                			</c:if>
			                		</c:forEach>
			                		<button class="btn-wishlist ${wishAt}" title="관심수강 담기" data-code="BOOK_LIKE" data-id="${result.nttNo}"></button>
			                	</c:when>
			                	<c:otherwise>
			                		<button class="btn-wishlist btnModalOpen"
						                      data-modal-type="confirm"
						                      data-modal-header="알림"
						                      data-modal-text="회원가입이 필요한 서비스입니다."
						                      data-modal-subtext="로그인 후 이용이 가능합니다. 회원가입 화면으로 이동하시겠습니까?"
						                      data-modal-rightbtn="확인"
						                      onclick="location.href='/uss/umt/cmm/EgovStplatCnfirmMber.do'"
						                      title="관심수강 담기">
						              </button>
			                	</c:otherwise>
			                </c:choose>
			              </div>
	                      
	                    </div>
	                  </div>
	                </div>
                </c:forEach>
              </div>
              <div class="center-align">
                <button type="button" class="mt-50 cursor-pointer btn_more"><img class="vertical-top" src="${CML}/imgs/common/btn_board_contents_more.jpg" alt="더보기"></button>
              </div>
            </article>
          </section>

</div>
</div>
</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
	<c:param name="shareAt" value="Y"/>
</c:import>
