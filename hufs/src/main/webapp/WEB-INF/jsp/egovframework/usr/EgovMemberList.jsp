<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<% /*URL 정의*/ %>
	<c:url var="_BASE_PARAM" value="">
		<c:param name="menuId" value="MNU_0000000000000126" />
		<c:param name="siteId" value="${searchVO.siteId}" />
		<c:if test="${!empty searchVO.searchPositionCode}"><c:param name="searchPositionCode" value="${searchVO.searchPositionCode}" /></c:if>
		<c:if test="${!empty searchVO.searchDept}"><c:param name="searchDept" value="${searchVO.searchDept}" /></c:if>
		<c:if test="${!empty searchVO.searchGender}"><c:param name="searchGender" value="${searchVO.searchGender}" /></c:if>
		<c:if test="${!empty searchVO.searchGroup}"><c:param name="searchGroup" value="${searchVO.searchGroup}" /></c:if>
		<c:if test="${!empty searchVO.searchWork}"><c:param name="searchWork" value="${searchVO.searchWork}" /></c:if>
		<c:if test="${!empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
		<c:if test="${!empty searchVO.templateAt}"><c:param name="templateAt" value="${searchVO.templateAt}" /></c:if>
	</c:url>
<% /*URL 정의*/ %>
<c:set var="_ACTION" value="/uss/umt/cmm/EgovMberManage.do" />
<c:choose>
<c:when test="${searchVO.templateAt ne 'N'}">
	<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
		<c:param name="isMainSite">Y</c:param>
		<c:param name="siteId" value="SITE_000000000000001"/>
	</c:import>
	
	<form:form name="listForm" action="${_ACTION }" method="post">
	<input type='hidden' name='menuId' value='MNU_0000000000000126'/>
	<section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">교원 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchGroup" id="selectGroupCode" class="select2" data-select="style3" data-placeholder="소속">
                        <option value="">전체</option>
                        <c:forEach var="result" items="${groupList}" varStatus="status">
							<option value="${result.code}" <c:if test="${searchVO.searchGroup eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchGender" id="selectGender" class="select2" data-select="style3" data-placeholder="성별">
                        <option value="">전체</option>
                        <option value="M" <c:if test="${searchVO.searchCondition eq 'M'}">selected="selected"</c:if>>남</option>
						<option value="F" <c:if test="${searchVO.searchCondition eq 'F'}">selected="selected"</c:if>>여</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="ell">
                      <select name="searchDept" id="selectDeptCode" class="select2" data-select="style3" data-placeholder="주관기관">
                        <option value="">전체</option>
                        <c:forEach var="result" items="${deptList}" varStatus="status">
							<option value="${result.ctgryId}" <c:if test="${searchVO.searchDept eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchPositionCode" id="selectPositionCode" class="select2" data-select="style3" data-placeholder="직위">
                        <option value="">전체</option>
                        <c:forEach var="result" items="${positionList}" varStatus="status">
							<option value="${result.code}" <c:if test="${searchVO.searchPositionCode eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchWork" id="selectWorkStatusCode" class="select2" data-select="style3" data-placeholder="재직상태">
                        <option value="">전체</option>
                        <c:forEach var="result" items="${workStatusList}" varStatus="status">
							<option value="${result.code}" <c:if test="${searchVO.searchWork eq result.code}">selected="selected"</c:if>>${result.codeNm }</option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="ell">
                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" id="inp_text" placeholder="성명">
                    </div>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20">검색</button>

                <button class="btn-sm font-400 btn-outline mt-20" onclick='javascript:fnResetSearch();'>초기화</button>
              </div>
            </article>
            <article class="content-wrap">
              <div class="content-header">
                <div class="btn-group-wrap">
                  <div class="left-area">
                    <a href="../admin/staff_admin.1.2.html" class="btn-md btn-outline">교원등록</a>
                    <a href="#" class="btn-md btn-outline">엑셀 일괄 등록</a>
                  </div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:13%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col' class='table-sorting asc'>성명</th>
                      <th scope='col' class='table-sorting desc'>사번</th>
                      <th scope='col' class='table-sorting asc'>재직상태</th>
                      <th scope='col' class='table-sorting asc'>직위</th>
                      <th scope='col' class='table-sorting asc'>소속</th>
                      <th scope='col' class='table-sorting asc'>주관기관</th>
                      <th scope='col'>연락처</th>
                      <th scope='col' class='table-sorting asc'>계정<br>사용여부</th>
                      <th scope='col'>교육과정 이력</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="result" items="${resultList}" varStatus="status">
	                    <tr onclick="location.href='/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000126&userId=${result.userId }&userSeCode=08'" class=" cursor-pointer">
	                      <td><fmt:formatNumber value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageSize) - (status.count - 1)}" type="number"/></td>
	                      <td><c:out value="${result.userNm}" /></td>
	                      <td><c:out value="${result.userSchNo}" /></td>
	                      <td><c:out value="${result.workStatusCodeNm}" /></td>
	                      <td><c:out value="${result.positionCodeNm}" /></td>
	                      <td><c:out value="${result.groupCode}" /></td>
	                      <td><c:out value="${result.mngDeptCodeNm}" /></td>
	                      <td class='keep-all'><c:out value="${result.moblphonNo}" /></td>
	                      <td <c:if test="${result.useYn eq 'N'}">class='font-red'</c:if>>
	                      	<c:choose>
								<c:when test="${result.useYn eq 'N'}">사용 안함</c:when>
								<c:otherwise>사용함</c:otherwise>
							</c:choose>
	                      </td>
	                      <td>
		                      <a href="/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000126&userId=${result.userId }&userSeCode=08" class='underline keep-all' target='_blank'>
									<c:choose>
										<c:when test="${result.currCnt eq 0 }">
											없음
										</c:when>
										<c:when test="${result.currCnt eq 1 }">
											<c:out value="${result.currNm }"/>
										</c:when>
										<c:when test="${result.currCnt > 1 }">
											<c:out value="${result.currNm } 외 ${result.currCnt - 1}건 "/>
										</c:when>
									</c:choose>
								</a>
	                      </td>
	                    </tr>
                    </c:forEach>
                    <c:if test="${fn:length(resultList) == 0}">
				      <tr>
				      	<td class="listtd" colspan="10">
				        	<spring:message code="common.nodata.msg" />
				        </td>
				      </tr>
				    </c:if>
                  </tbody>
                </table>
                
                <div class="pagination center-align mt-60">
					<div class="pagination-inner-wrap overflow-hidden inline-block">
						<c:url var="startUrl" value="/uss/umt/cmm/EgovMberManage.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="1" />
		   				</c:url>
		               	<button class="start" data-url="${startUrl}"></button>
		               
		               	<c:url var="prevUrl" value="/uss/umt/cmm/EgovMberManage.do${_BASE_PARAM}">
							<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
						</c:url>
		               	<button class="prev" data-url="${prevUrl}"></button>
		               
		               	<ul class="paginate-list f-l overflow-hidden">
		                 	<c:url var="pageUrl" value="/uss/umt/cmm/EgovMberManage.do${_BASE_PARAM}"/>
							<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
							<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
		               	</ul>
		               
		               	<c:url var="nextUrl" value="/uss/umt/cmm/EgovMberManage.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
		   				</c:url>
		               	<button class="next" data-url="${nextUrl}"></button>
		               
		               	<c:url var="endUrl" value="/uss/umt/cmm/EgovMberManage.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
		   				</c:url>
		               	<button class="end" data-url="${endUrl}"></button>
					</div>
				</div>
              </div>
            </article>
          </section>
          </form:form>
	
	<script>
	var fnResetSearch = function(){
		$('.select2').val('');
		$('#inp_text').val('');
	}
	</script>
	
	<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</c:when>
<c:otherwise>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>교원 검색</title>

  <meta name="title" content="교원 검색">

  <meta property="og:type" content="website">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = [],
			name = [],
			mngdept = [],
			mngdeptnm = [];
		
		
		$(".chk").each(function(){
			if($(this).is(":checked")){
				id.push($(this).val());
				name.push($(this).data("name"));
				mngdept.push($(this).data("mngdept"));
				mngdeptnm.push($(this).data("mngdeptnm"));
			}
		});
		
		window.opener.selUser(id, name, mngdept, mngdeptnm, "${searchVO.position}");
		window.close();
		
		return false;
	});
	
	$(".btnModalCancel, .btnModalClose").click(function(){
		window.close();
	});
});
</script>
</head>
<body>

<div id="staff_search_modal" class="alert-modal" style="display:block;">
  <div class="modal-dialog modal-top">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">교원 검색</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text">배정할 교원의 이름을 입력해주세요.</p>
        <form name="listForm" id="listForm" method="post" method="post" action="${_ACTION }" style="width:100%">
			<input type="hidden" name="templateAt" value="${searchVO.templateAt}"/>
			<input type="hidden" name="position" value="${searchVO.position}"/>
			<div class="flex-row-ten">
	            <div class="flex-ten-col-7">
	              <div class="ell">
	                <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" placeholder="이름을 입력해 주세요.">
	              </div>
	            </div>
	            <div class="flex-ten-col-2">
	              <div class="ell"><button class="btn-sm font-400 btn-point goods-search-btn" style="height:40px;">검색</button></div>
	            </div>
	        </div>
	        
			<table class="common-table-wrap size-sm mt-20">
	          <colgroup>
	            <col style="width:70px;">
	            <col>
	            <col>
	            <col style="width:118px">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray-light font-700">
	              <th>선택</th>
	              <th>이름</th>
	              <th>이메일</th>
	              <th>주관기관/학과</th>
	            </tr>
	          </thead>
	          <tbody>
	          	<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<td>
							<label class="checkbox">
			                  <input type="checkbox" class="chk" value="${result.userId}" data-name="${result.userNm}" data-mngdept="${result.mngDeptCode}" data-mngdeptnm="${result.mngDeptCodeNm}"/>
			                  <span class="custom-checked"></span>
			                </label>
						</td>
						<td><c:out value="${result.userNm}" /></td>
						<td><c:out value="${result.emailAdres}"/></td>
						<td><c:out value="${result.mngDeptCodeNm}"/></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(resultList) == 0}">
			      <tr>
			      	<td colspan="4"><spring:message code="common.nodata.msg" /></td>
			      </tr>
			    </c:if>
	          </tbody>
	        </table>
		</form>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button class="btn-xl btn-point btnModalConfirm btn_ok" data-target="${param.target}">등록</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
</c:otherwise>
</c:choose>
 

