<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="MNG_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>

<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000126" />
</c:url>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>	

<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/overlibmws/overlibmws.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/ajaxtags/js/ajaxtags.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
		
<section class="page-content-body">
            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <h3 class="title">
                    <div class="desc">
                      <div class="mr-20"><span class="font-basic">총 교육과정</span> ${fn:length(resultListCount)}건</div>
                      <div><span class="font-basic">수업진행</span> ${resultCnt }건</div>
                    </div>
                  </h3>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:10%'>
                    <col style='width:9%'>
                    <col style='width:14%'>
                    <col style='width:10%'>
                    <col>
                    <col style='width:10%'>
                    <col>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>년도</th>
                      <th scope='col'>학기</th>
                      <th scope='col'>과정기간</th>
                      <th scope='col'>과정상태</th>
                      <th scope='col'>교육과정명</th>
                      <th scope='col'>담당직위</th>
                      <th scope='col'>수업명</th>
                    </tr>
                  </thead>
                  <tbody>
                  <c:set var="no" value="1"/>      
                  	<c:forEach var="result" items="${resultList}" varStatus="status">
                  		<c:forEach var="list" items="${resultListCount}" varStatus="status2">
                  			<c:if test="${result.crclId eq list.crclId}">
                  				<c:set var="crclCnt" value="${list.groupCnt}"/>
                  			</c:if>
	                    </c:forEach>
		                    
	                    <c:choose>
	                  		<c:when test="${result.crclId eq crclId}">  		
			                    <tr class="">
			                      <td class='left-align keep-all'>${result.studySubject}</td>
			                    </tr>	
		                    </c:when>
		                    <c:otherwise>
		                    	<c:choose>
	                                <c:when test="${result.processSttusCode eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
	                                <c:when test="${result.processSttusCode eq 3 or result.processSttusCode eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
	                                <c:when test="${result.processSttusCode eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
	                                <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
	                            </c:choose>
	                            
	                            <c:url var="viewUrl" value="/lms/crm/myCurriculumView.do">
									<c:param name="crclId" value="${result.crclId}"/>
									<c:param name="crclbId" value="${result.crclbId}"/>
									<c:param name="subtabstep" value="1"/>
									<c:param name="tabStep" value="1"/>
									<c:param name="menuId" value="MNU_0000000000000068"/>
								</c:url>
						
		                    	<c:choose>
			                    		<c:when test="${crclCnt > 1}">
			                  				<tr class="">
						                      <td rowspan='${crclCnt}'>${no}</td>
						                      <td rowspan='${crclCnt}'>${result.crclYear}</td>
						                      <td rowspan='${crclCnt}'>${result.crclTermNm}</td>
						                      <td rowspan='${crclCnt}'>${result.startDate}<br>~<br>${result.endDate}</td>
						                      	
					                            <td rowspan='${crclCnt}' class='${processSttusColor }'>
								    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status3">
								    					<c:if test="${statusCode.code eq result.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
													</c:forEach>
								    			</td>
						                      
						                      <td class='left-align keep-all' rowspan='${crclCnt}'><a href='${viewUrl}' class='underline keep-all' target="_blank">${result.crclNm}</a></td>
						                      <td rowspan='${crclCnt}'>${result.manageCodeNm}</td>
						                      <td class='left-align keep-all'>${result.studySubject}</td>
						                    </tr>
					                    </c:when>
					                    <c:otherwise>
					                    	<tr class="">
						                      <td>${no}</td>
						                      <td>${result.crclYear}</td>
						                      <td>${result.crclTermNm}</td>
						                      <td>${result.startDate}<br>~<br>${result.endDate}</td>
					                            <td class='${processSttusColor }'>
								    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status3">
								    					<c:if test="${statusCode.code eq result.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
													</c:forEach>
								    			</td>
						                      <td class='left-align keep-all'><a href='${viewUrl}' class='underline keep-all' target="_blank">${result.crclNm}</a></td>
						                      <td>${result.manageCodeNm}</td>
						                      <td class='left-align keep-all'>${result.studySubject}</td>
						                    </tr>
					                    </c:otherwise>
					                 </c:choose>
					                 <c:set var="no" value="${no+1}"/>   
		                    </c:otherwise>
	                    </c:choose>
	                    
	                    <c:set var="crclId" value="${result.crclId}"/>
                    </c:forEach>
                  </tbody>
                </table>
              </div>
            </article>
          </section>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>	