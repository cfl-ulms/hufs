<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}${LytFileStoreWebPathByWebFile}sit/${siteInfo.lytTmplatId}/images"/>
<c:set var="CML" value="/template/lms"/>

<c:choose>
	<c:when test="${param.bbsId eq 'BBSMSTR_000000000005'}">
		</section>
	</c:when>
	<c:when test="${not empty param.bbsId and param.bbsId ne 'BBSMSTR_000000000004'}">
				</div>
			<!-- //콘텐츠바디 -->
	      </div>
	      <!-- 콘텐츠영역 -->
	    </div>
	    <!-- //페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
	  </div>
	  <!-- //area -->
	</c:when>
	<c:otherwise>
	
	</c:otherwise>
</c:choose>

<!-- footer -->
  <footer id="footer">
  	<c:if test="${param.footTopAt ne 'N'}">
	    <div class="footer-top-wrap">
	      <div class="footer-top">
	        <div class="footer-term">
	          <a class="privacy btnWindowOpen" href="/msi/indvdlInfoPolicy.do?tmplatImportAt=N" data-width="868" data-height="860">
	            개인정보처리방침
	          </a>
	          <a class="btnWindowOpen" href="/msi/useStplat.do?tmplatImportAt=N" data-width="868" data-height="full">
	            이용약관
	          </a>
	          <a class="btnWindowOpen" href="/msi/cpyrhtSttemntSvc.do?tmplatImportAt=N" data-width="868" data-height="650">
	            저작권 보호 정책
	          </a>
	        </div>
	        <select class="select2" name="" id="" data-select="style2" data-link="true" data-placeholder="패밀리사이트">
	          <option value=""></option>
	          <c:import url="/msi/ctn/linkSiteService.do" charEncoding="utf-8">
				<c:param name="tmplatCours" value="${_IMG}"/>
			  </c:import>
	        </select>
	      </div>
	    </div>
    </c:if>
    <div class="area-full">
      <div class="company-info-wrap">
        <h2 class="footer-logo">
          <img src="${CML}/imgs/common/img_footer_logo.png" alt="<c:out value="${siteInfo.siteNm}"/>">
        </h2>
        <div class="company-info">
          <p>${siteInfo.adresReplcText}</p>
          <p class="copyright">© 2019 Hankuk University of Foreign Studies. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>

<c:if test="${param.isMain eq 'Y'}">
  <c:import url="/msi/ctn/popupService.do" charEncoding="utf-8"/>
</c:if>

<c:if test="${param.modalAt eq 'Y'}">
	<div id="confirm_modal" class="alert-modal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title"></h4>
	        <button type="button" class="btn-modal-close btnModalClose"></button>
	      </div>
	      <div class="modal-body">
	        <p class="modal-text"></p>
	        <p class="modal-subtext"></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
	        <button type="button" class="btn-xl btn-point btnModalConfirm">확인</button>
	      </div>
	    </div>
	  </div>
	</div>
</c:if>
<c:if test="${param.shareAt eq 'Y'}">
	<div id="share_modal" class="alert-modal">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h4 class="modal-title">공유하기</h4>
	          <button type="button" class="btn-modal-close btnModalClose"></button>
	        </div>
	        <div class="modal-body">
	          <ul class="sns-share-wrap">
	            <li class="sns-share-list">
	              <a href="javascript:toSNS('twitter')" class="btn_sns" data-type="twitter" title="twitter 새 창 열림">
	                <img src="${CML}/imgs/popup/icon_share_twitter.png" alt="트위터로 공유하기">
	              </a>
	            </li>
	            <li class="sns-share-list">
	              <a href="javascript:toSNS('facebook')" class="btn_sns" data-type="facebook" title="facebook 새 창 열림">
	                <img src="${CML}/imgs/popup/icon_share_facebook.png" alt="페이스북으로 공유하기">
	              </a>
	            </li>
	          </ul>
	          <div class="sns-share-clipboard modal-footer">
	          	<input type="hidden" id="sns_title"/>
	            <input type="text" class="sns-share-url ell copyUrl" value="http://" readonly>
	            <p class="tooltip">Copied!</p>
	          </div>
	        </div>
	      </div>
	    </div>
	</div>
</c:if>

</body>

</html>