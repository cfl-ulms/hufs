<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="${_WEB_FULL_PATH}/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="bbsId" value="${board.bbsId}" />
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.viewType}"><c:param name="viewType" value="${searchVO.viewType}"/></c:if>	
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
	<c:param name="tableCssAt" value="Y"/>
	<c:param name="contTitleAt" value="N"/>
</c:import>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script src="${_C_JS}/board.js" ></script>
<script>
	$(document).ready(function(){
		//글등록
		$('.btnModalOpen').click(function() {
			var href = $(this).data("href");
			$(".btnModalConfirm").click(function(){
				location.href = href;
			});
			return false;
		});
	});
</script>
<style>
#froala_editor{font-family:'나눔고딕',NanumGothic,NanumGothicWeb, sans-serif;}
</style>

	<div class="page-content-wrap">
      <section class="page-content-header">
        <div class="textbook-contents-view">
          <div class="left-area">
            <div class="textbook-img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${board.atchFileNm}"/>);" title="태국어 문형사전"></div>
          </div>

          <div class="right-area">
            <div class="textbook-inner">
              <div class="textbook-flag">
              	<c:set var="imgSrc">
					<c:import url="/lms/common/flag.do" charEncoding="utf-8">
						<c:param name="ctgryId" value="${board.ctgryId}"/>
					</c:import>
          		</c:set>
                <span class="flag-img"><img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
                <span class="text"><c:out value="${board.ctgryNm}"/> 교재</span>
              </div>
              <div class="book-name">
              	<c:if test="${board.tmp03 eq 'Y'}">
               		<span class="type">e-book</span>
               	</c:if>
               	<c:out value="${board.nttSj}"/>
              </div>
              <dl class="book-info">
                <dt class="info-title">출판사</dt>
                <dd class="info-name"><c:out value="${board.tmp01}"/></dd>
              </dl>
              <dl class="book-info">
                <dt class="info-title">저자</dt>
                <dd class="info-name"><c:out value="${board.tmp02}"/></dd>
              </dl>
            </div>
            <div class="textbook-button">
              <div class="util-wrap">
                <%-- <button class="btn-share btnModalOpen" data-modal-type="share" onclick="location.href='http://${siteInfo.siteUrl}/cop/bbs/selectBoardArticle.do?${pageContext.request.queryString}'" data-title="<c:out value="${board.nttSj}"/>" title="공유하기"></button> --%>
                <c:choose>
                	<c:when test="${not empty USER_INFO.id}">
                		<c:set var="wishAt" value=""/>
                		<c:forEach var="wishList" items="${wishList}" varStatus="status">
                			<c:if test="${wishList.trgetId eq board.nttNo}">
                				<c:set var="wishAt" value="on"/>
                			</c:if>
                		</c:forEach>
                		<button class="btn-wishlist ${wishAt}" title="관심수강 담기" data-code="BOOK_LIKE" data-id="${board.nttNo}"></button>
                	</c:when>
                	<c:otherwise>
                		<button class="btn-wishlist btnModalOpen"
			                      data-modal-type="confirm"
			                      data-modal-header="알림"
			                      data-modal-text="회원가입이 필요한 서비스입니다."
			                      data-modal-subtext="로그인 후 이용이 가능합니다. 회원가입 화면으로 이동하시겠습니까?"
			                      data-modal-rightbtn="확인"
			                      onclick="location.href='/uss/umt/cmm/EgovStplatCnfirmMber.do'"
			                      title="관심수강 담기">
			              </button>
                	</c:otherwise>
                </c:choose>
              </div>
             <%--  <a href="<c:out value="${board.tmp05}"/>" class="btn-full btn-point" target="_blank">구매하기</a> --%>
            </div>
            <div class="textbook-keyword">
              <div class="keyword-title">키워드</div>
              <ul class="keyword-item">
              	<c:set var="keyword" value="${fn:split(board.tmp04,',')}"/>
				<c:forEach var="result" items="${keyword}" varStatus="status">
					<li class="item">${result}</li>
				</c:forEach>
              </ul>
            </div>
          </div>
        </div>
      </section>
      
      <section class="page-content-body">
        <article class="content-wrap mb-70">
          <div class="content-header mb-35">
            <div class="title-wrap" id="froala_editor">
              <h3 class="title mb-10">책정보</h3>
              <p class="desc"><c:out value="${board.nttCn}" escapeXml="false"/></p>
            </div>
          </div>
        </article>
        <article class="content-wrap">
          <!-- 교재 및 부교재 -->
          <div class="content-header">
            <div class="title-wrap">
              <div class="title">다른 <c:out value="${board.ctgryNm}"/> 교재</div>
            </div>
          </div>
          <div class="content-body">
            <ul class="book-list-wrap flex-row">
            	<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
					<c:param name="tmplatCours" value="${_IMG}"/>
					<c:param name="viewType" value="gallery" />
					<c:param name="linkMenuId" value="${searchVO.menuId}" />
					<c:param name="tableId" value="${board.bbsId}" />
					<c:param name="itemCount" value="7" />
					<c:param name="nttNo" value="${board.nttNo}" />
					<c:param name="ctgrymasterId" value="${brdMstrVO.ctgrymasterId}" />
					<c:param name="searchCateList" value="${board.ctgryId}"/>
				</c:import>
            </ul>
          </div>
        </article>

      </section>
    </div>
	
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
	<c:param name="shareAt" value="Y"/>
</c:import>