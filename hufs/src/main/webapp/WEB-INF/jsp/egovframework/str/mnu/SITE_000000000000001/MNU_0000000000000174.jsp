<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<%@ page import="java.util.Date" %>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>
<c:set var="INTR" value="/template/introduce"/>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>한국외국어대학교</title>

  <meta name="title" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">
  <meta property="og:url" content="http://cfl.ac.kr/">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
  <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
<c:choose>
	<c:when test="${USER_INFO.userSeCode eq '08'}">
  		<link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=3">
		<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/modal_staff.css?v=3">
  		
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">

	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">
		
		<link rel="stylesheet" href="${CML}/css/common/common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board.css?v=1">
  		<link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
  		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
	</c:otherwise>
</c:choose>

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/main/main_common.css?v=2">
  <link rel="stylesheet" href="${CML}/css/main/main_logout.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=2"></script>
  <script src="${CM}/js/common.js"></script>
  <!--=================================================
          페이지별 스크립트
  ==================================================-->
  	
	
  <link rel="stylesheet" href="${CML}/css/special/special.css?v=2">
  <link rel="stylesheet" href="${CML}/css/special/special.1.1.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
  <script src="${CML}/js/main/main.js?v=1"></script>
  <script type="text/javascript">

$(document).ready(function(){
	<c:if test="${not empty USER_INFO.id and USER_INFO.id ne 'sj1004class@hanmail.net' and USER_INFO.id ne 'stu001@daum.net'}">
		setTimer('59', '59');

		$('.login-time-text').on('click', function(){
			clearTimer();
			setTimer('59', '59');
		});
	</c:if>

	//국가 선택 검색
	$(document).on("click", ".liLang", function() {
		var langId = $(this).data("id");
		
		$(".liLang").removeClass("on");
		$(this).addClass("on");

        $.ajax({
            url:'/lms/crm/curriculumCardListAjax.do',
            type:'post',
            dataType:'html',
            data:{"searchCrclLang" : langId},
            beforeSend : function(){
            	$('#curCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
            }, success:function(data){
                $('#curCard').html(data);
            }
        });
	});
});

//타이머
var timer;
function setTimer(minute, second){
	 // 초기화
	 $(".countTimeMinute").html(minute);
	 $(".countTimeSecond").html(second);

	 timer = setInterval(function () {
	  // 설정
	  $(".countTimeMinute").html(minute);
	  $(".countTimeSecond").html(second);

	  if(minute == 0){
		  //console.log(11);
	  }
	  if(second == 0){
		  //console.log(22);
	  }

	  if(minute < 0){
		  clearInterval(timer);
		  location.href = "<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>";
	  }else{
	   second--;
	   if(second<10){
	    $(".countTimeSecond").html('0' + second);
	   }

	   // 분처리
	   if(second == 0){
	    minute--;
	    second = 59;
	   }
	   if(minute<10){
	    $(".countTimeMinute").html('0' + minute);
	   }

	   	if(minute == 4 && second == 59){
	   		if(confirm("시간연장하시겠습니까?")){
	   			clearTimer();
				setTimer('59', '59');
	   		}
	   	}
	  }
	 }, 1000); /* millisecond 단위의 인터벌 */
}

function clearTimer(){
	clearInterval(timer);
}

function fnCalDate(year, month, day){
	var tmpDt;

	if(month < 10){
		month = "0" + month;
	}
	if(day < 10){
		day = "0" + day;
	}
	tmpDt = year + "-" + month + "-" + day;

	return tmpDt;
}
  
</script>
</head>

<body>
	<header class="gnb">
		<div class="top-header area-full">
			<h1>
				<a href="/index.do" class="flex">
				<img src="${CML}/imgs/common/img_header_logo.png" alt="CFL 특수외국어교육진흥사업">
				</a>
			</h1>
			<ul class="user-info-wrap">
			<c:choose>
      			<c:when test="${not empty USER_INFO.id }">
      				<li class="list profile">
		          		<a href="#">
		            	<span class="user-img">
		              		<b><c:out value="${USER_INFO.name}"/></b>(<c:out value="${USER_INFO.id}"/>)
		            	</span>
		          		</a>
		        	</li>
		        	<li class="list login-time">
		          		<button type="button" class="cursor-pointer">
			            	<span class="login-time-num"><span class="countTimeMinute"></span>:<span class="countTimeSecond"></span></span>
			            	<span class="login-time-text">시간연장하기</span>
		          		</button>
		        	</li>
		        	<li class="list">
		          		<a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000024&menuId=MNU_0000000000000131">FAQ</a>
		        	</li>
		        	<li class="list">
			          	<c:choose>
			        		<c:when test="${USER_INFO.userSeCode eq '08' }">
			        			<a href="/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000044&userId=${USER_INFO.id}&userSeCode=${USER_INFO.userSeCode}&type=mypage">MYPAGE</a>
			        		</c:when>
			        		<c:otherwise>
			          			<a href="/uss/umt/cmm/EgovUserUpdateView.do?menuId=MNU_0000000000000044">MYPAGE</a>
			        		</c:otherwise>
			        	</c:choose>
		        	</li>
			        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06'}">
			        	<li class="list">
				          <a href="/lms/crm/selectWishCrsList.do?menuId=MNU_0000000000000165">WISHLIST</a>
				        </li>
			        </c:if>
			        <li class="list">
			          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
			        </li>
			        <li class="list">
			          <a href="<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>">LOGOUT</a>
			        </li>
      		</c:when>
      		<c:otherwise>
      			<li class="list">
		          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
		        </li>
      			<li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>">JOIN</a>
		        </li>
		        <li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>">LOGIN</a>
		        </li>
      		</c:otherwise>
      	</c:choose>
			</ul><!-- user-info-wrap -->
		</div><!-- top-header -->

	<!-- nav -->
	    <nav class="nav-wrap">
      <div class="nav-wrap-inner area-full">
        <ul class="nav-list-wrap">
            <c:import url="/msi/introduce/menuService.do" charEncoding="utf-8">
			<c:param name="menuTarget" value="Main"/>
			<c:param name="menuType" value="Simple2Depth"/>
			<c:param name="seCode" value="${SE_CODE}"/>
		  </c:import>
        </ul>
      </div><!-- nav-wrap-inner -->
      <div class="nav-bg"></div>
    </nav><!-- nav -->
	</header>

	<div class="area">
		<!-- bread crumb -->
		<ul class="main-bread-crumb">
			<li class="item">HOME</li>
			<li class="item">진흥원 소개</li>
			<li class="item">사업단 조직</li>
		</ul>
		<!-- 페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
		<div class="page-container has-sidenav">
			<!-- 네비게이션영역 -->
			<div class="sidenav-bar">
				<!-- lnb -->
				<div id="lnb" class="lnb">
					<div class="lnb-heder">
						<h2 class="title">진흥원 소개</h2>
					</div>
					<nav class="lnb-body">
						<ul class="menu-list-wrap">
<!-- 							<li class="list depth-1">
								<a href="/msi/introduce/introduce.do?menuId=MNU_0000000000000177">원장님 인사말</a>
							</li> -->
							<li class="list depth-1 active">
								<a href="/msi/introduce/introduce.do?menuId=MNU_0000000000000174">사업단 조직</a>
							</li>
						</ul>
					</nav>
				</div>
				<!-- //lnb -->
			</div>
			<!-- 콘텐츠영역 -->
			<div class="expansion-panel">
				<!-- 메인타이틀 -->
				<div class="main-common-title3 ">
					<h2 class="title">사업단 조직</h2>
					<div class="util-wrap">
						<button class="btn-expansion" type="button" title="넓게보기"></button>
					</div>
				</div>
				<div class="page-content-wrap">
					<!-- 콘텐츠바디 -->
					<section class="page-content-body">
						<div class="page-common-title">
							<p class="desc"><img class="mb-20" src="${CML}/imgs/page/special/img174_01.png" alt="" /></p>
						</div>

						<article class="common-column-layout ">
							<h3 class="content-main-title">사업단 구성원</h3>
							<div class="pt-30 content-wrap">
								<div class="content-group">
									<div class="contnet-desc">
										<!-- 테이블영역-->
										<table class="common-table-wrap border-white size-sm">
											<colgroup>
												<col style="width: 7%;" />
												<col style="width: 33%;" />
												<col style="width: 12%;" />
												<col style="width: 12%;" />
												<col style="width: 36%;" />
											</colgroup>
											<thead>
											<tr>
												<th class="font-700 bg-deep-gray">연번</th>
												<th class="font-700 bg-deep-gray">소속</th>
												<th class="font-700 bg-deep-gray">성명</th>
												<th class="font-700 bg-deep-gray">직위(급)</th>
												<th class="font-700 bg-deep-gray">담당업무</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td class="bg-gray">1</td>
												<td class="bg-gray">특수외국어교육진흥원</td>
												<td class="bg-gray">이은구</td>
												<td class="bg-gray">원장</td>
												<td class="bg-gray">특수외국어교육진흥 사업 총괄</td>
											</tr>
											<tr>
												<td class="bg-gray">2</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">김현숙</td>
												<td class="bg-gray">운영팀장</td>
												<td class="bg-gray">특수외국어교육진흥 사업운영 총괄</td>
											</tr>
											<tr>
												<td class="bg-gray">3</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">이주은</td>
												<td class="bg-gray">과장</td>
												<td class="bg-gray">예산 실무 전담 및 사업실무</td>
											</tr>
											<tr>
												<td class="bg-gray">4</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">이미경</td>
												<td class="bg-gray">연구원</td>
												<td class="bg-gray">사업실무 및 연구관련 업무</td>
											</tr>
											<tr>
												<td class="bg-gray">5</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">김하경</td>
												<td class="bg-gray">연구원</td>
												<td class="bg-gray">사업실무 및 연구관련 업무</td>
											</tr>
											<tr>
												<td class="bg-gray">6</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">이현경</td>
												<td class="bg-gray">담당</td>
												<td class="bg-gray">학부교육 내실화 사업 등</td>
											</tr>
											<tr>
												<td class="bg-gray">7</td>
												<td class="bg-gray">특수외국어교육진흥원 운영팀</td>
												<td class="bg-gray">이도영</td>
												<td class="bg-gray">담당</td>
												<td class="bg-gray">일반인 대상 사업 등</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</article>

						<article class="common-column-layout">
							<h3 class="content-main-title">사업추진위원회</h3>
							<div class="pt-30 content-wrap">
								<div class="content-group">
									<div class="contnet-desc">
										<!-- 테이블영역-->
										<table class="common-table-wrap border-white size-sm">
											<colgroup>
												<col style="width: 7%;" />
												<col style="width: 12%;" />
												<col style="width: 12%;" />
												<col style="width: 33%;" />
												<col style="width: 36%;" />
											</colgroup>
											<thead>
											<tr>
												<th class="font-700 bg-deep-gray">연번</th>
												<th class="font-700 bg-deep-gray">구분</th>
												<th class="font-700 bg-deep-gray">성명</th>
												<th class="font-700 bg-deep-gray">소속</th>
												<th class="font-700 bg-deep-gray">직위(급)</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td class="bg-gray">1</td>
												<td class="bg-gray">위원장</td>
												<td class="bg-gray">김태성</td>
												<td class="bg-gray">중국언어문화학부</td>
												<td class="bg-gray">서울캠퍼스 부총장</td>
											</tr>
											<tr>
												<td class="bg-gray">2</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">이은구</td>
												<td class="bg-gray">인도학과</td>
												<td class="bg-gray">특수외국어교육진흥원장</td>
											</tr>
											<tr>
												<td class="bg-gray">3</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">이지은</td>
												<td class="bg-gray">중앙아시아학과</td>
												<td class="bg-gray">특수외국어교육진흥원 부원장</td>
											</tr>
											<tr>
												<td class="bg-gray">4</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">김광호</td>
												<td class="bg-gray">경영학부</td>
												<td class="bg-gray">기획조정처장</td>
											</tr>
											<tr>
												<td class="bg-gray">5</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">윤성우</td>
												<td class="bg-gray">철학과</td>
												<td class="bg-gray">서울캠퍼스 및 글로벌캠퍼스 교무처장</td>
											</tr>
											<tr>
												<td class="bg-gray">6</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">정석오</td>
												<td class="bg-gray">통계학과</td>
												<td class="bg-gray">입학처장</td>
											</tr>
											<tr>
												<td class="bg-gray">7</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">김민정</td>
												<td class="bg-gray">사범대학</td>
												<td class="bg-gray">진로취업지원센터장(서울)</td>
											</tr>
											<tr>
												<td class="bg-gray">8</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">오세홍</td>
												<td class="bg-gray">바이오메디컬공학부</td>
												<td class="bg-gray">진로취업지원센터장(글로벌)</td>
											</tr>
											<tr>
												<td class="bg-gray">9</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">김현정</td>
												<td class="bg-gray">TESOL대학원</td>
												<td class="bg-gray">FLEX센터장</td>
											</tr>
											<tr>
												<td class="bg-gray">10</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">김혜진</td>
												<td class="bg-gray">그리스·불가리아학과</td>
												<td class="bg-gray">교육선진화센터장</td>
											</tr>
											<tr>
												<td class="bg-gray">11</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">최권진</td>
												<td class="bg-gray">인하대 국제학부</td>
												<td class="bg-gray">교수</td>
											</tr>
											<tr>
												<td class="bg-gray">12</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">배주경</td>
												<td class="bg-gray">한국교육과정평가원</td>
												<td class="bg-gray">연구원</td>
											</tr>
											<tr>
												<td class="bg-gray">13</td>
												<td class="bg-gray">간사</td>
												<td class="bg-gray">남우영</td>
												<td class="bg-gray">특수외국어교육진흥원</td>
												<td class="bg-gray">팀장</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</article>

						<article class="common-column-layout ">
							<h3 class="content-main-title">사업평가위원회</h3>
							<div class="pt-30 content-wrap">
								<div class="content-group">
									<div class="contnet-desc">
										<!-- 테이블영역-->
										<table class="common-table-wrap border-white size-sm">
											<colgroup>
												<col style="width: 7%;" />
												<col style="width: 15%;" />
												<col style="width: 15%;" />
												<col style="width: 33%;" />
												<col style="width: 15%;" />
												<col style="width: 15%;" />
											</colgroup>
											<thead>
											<tr>
												<th class="font-700 bg-deep-gray">연번</th>
												<th class="font-700 bg-deep-gray">구분</th>
												<th class="font-700 bg-deep-gray">성명</th>
												<th class="font-700 bg-deep-gray">소속</th>
												<th class="font-700 bg-deep-gray">직위(급)</th>
												<th class="font-700 bg-deep-gray">비고</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td class="bg-gray">1</td>
												<td class="bg-gray">위원장</td>
												<td class="bg-gray">이준</td>
												<td class="bg-gray">한국외대 교육대학원</td>
												<td class="bg-gray">교직전공 주임교수</td>
												<td class="bg-gray"></td>
											</tr>
											<tr>
												<td class="bg-gray">2</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">임대근</td>
												<td class="bg-gray">한국외대 융합인재학부</td>
												<td class="bg-gray">지식콘텐츠학부장</td>
												<td class="bg-gray"></td>
											</tr>
											<tr>
												<td class="bg-gray">3</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">김봉철</td>
												<td class="bg-gray">한국외대 국제학부</td>
												<td class="bg-gray">행정지원처장<br>(서울-글로벌)</td>
												<td class="bg-gray"></td>
											</tr>
											<tr>
												<td class="bg-gray">4</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">안정국</td>
												<td class="bg-gray">명지대 아랍지역학과</td>
												<td class="bg-gray">교수</td>
												<td class="bg-gray">외부</td>
											</tr>
											<tr>
												<td class="bg-gray">5</td>
												<td class="bg-gray">위원</td>
												<td class="bg-gray">현양원</td>
												<td class="bg-gray">카카오엔터테인먼트 태국법인</td>
												<td class="bg-gray">법인장</td>
												<td class="bg-gray">외부</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</article>
					</section>
				</div>
				<!-- //콘텐츠바디 -->
			</div>
			<!-- 콘텐츠영역 -->
		</div>
		<!-- //페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
	</div>
  </div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="isMain" value="${isMain}"/>
</c:import>

</body>

</html>