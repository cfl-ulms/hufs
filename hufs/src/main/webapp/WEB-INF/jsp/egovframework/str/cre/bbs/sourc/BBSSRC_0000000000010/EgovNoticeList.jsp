<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>



<c:set var="_PREFIX" value="/cop/bbs"/>

<c:choose>
	<c:when test="${boardVO.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
		</c:import>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<link rel="stylesheet" href="${CML}/css/materials/staff_materials.css?v=2">

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
	
	//페이징
	/* $(document).on("click", ".addPage", function() {
		var pageIndex = parseInt($(this).attr("data-pageindex"));
		var clickflag = $(this).attr("data-clickflag");
		var params    = jQuery("form[name=frm]").serialize() + "&pageIndex=" + pageIndex + "&myCurriculumPageAt=Y&ajaxFlag=myStudent";

		//데이터가 더 이상 없을 때 flag 처리
		if(clickflag == "F") {
			alert("더 이상 데이터가 없습니다.");
			return;
		}

		//여러번 호출 방지를 위한 flag 처리
		if(clickflag == "N") {
			alert("빠른 시간에 여러번 호출이 불가능 합니다.");
			return;
		}
		
		//여러번 호출 방지를 위한 flag 처리
		$(".addPage").attr("data-clickflag", "N");
		
		$.ajax({
			type:"post",
			dataType:"html",
			url:"/lms/crm/CurriculumListAjax.do",
			data:params,
			success: function(data) {
				//tr 태그 추가
				$(".list_table tbody").append(data);
				
				if($.trim(data) == "" || $.trim(data) == null) {
					alert("더 이상 데이터가 없습니다.");
					$(".addPage").attr("data-clickflag", "F");
					return;
				}

				//페이징 index 증가
				pageIndex++;
				$(".addPage").attr("data-pageindex", pageIndex);
				
				//여러번 호출 방지를 위한 flag 처리
				$(".addPage").attr("data-clickflag", "Y");
			},
			error:function() {
				alert("관리자에게 문의 바랍니다.");
			}
		});
		
	}); */
	
	//검색
	$(".goods-search-btn").click(function(){
		$("input[name=searchCate]").val($("#searchCate").val());
		$("input[name=searchWrd]").val($("#searchWrd").val());
		$("#frm").submit();
	});
	
});
</script>		

<section class="page-content-body">
    <article class="content-wrap">
      <div class="box-wrap mb-40">
        <h3 class="title-subhead">학습자료 검색</h3>
        <div class="flex-row-ten">
        	<form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
				<input type="hidden" name="bbsId" value="<c:out value='${param.bbsId}'/>" />
				<input name="menuId" type="hidden" value="<c:out value='${param.menuId}'/>" />
				<input name="searchCate" type="hidden" value="<c:out value='${param.searchCate}'/>" />
				<input name="searchCnd" type="hidden" value="0" />
				<input name="searchWrd" type="hidden" value="<c:out value="${param.searchWrd}"/>"/>
			</form>
          <div class="flex-ten-col-3 mb-20">
            <div class="ell">
              <select class="select2" name="searchCate" id="searchCate" data-select="style3">
                 <option value="">언어 전체</option>
                 <c:forEach var="result" items="${languageList}" varStatus="status">
	       			<c:if test="${not empty result.upperCtgryId}">
	                         <option value="${result.ctgryId}" <c:if test="${param.searchCate eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
	         	    </c:if>
    			</c:forEach>
               </select>
            </div>
          </div>
          <div class="flex-ten-col-7 mb-20">
            <div class="ell">
              <input type="text" id="searchWrd" placeholder="자료명를 입력해보세요." value="<c:out value="${param.searchWrd}"/>">
            </div>
          </div>
          <div class="flex-ten-col-10 flex align-items-center">
            <%-- 
            <label class="checkbox">
              <input type="checkbox">
              <span class="custom-checked"></span>
              <span class="text">파일</span>
            </label>
            <label class="checkbox">
              <input type="checkbox">
              <span class="custom-checked"></span>
              <span class="text">사진</span>
            </label>
            <label class="checkbox">
              <input type="checkbox">
              <span class="custom-checked"></span>
              <span class="text">동영상</span>
            </label>
             --%>
            <label class="checkbox">
              <input type="checkbox">
              <span class="custom-checked"></span>
              <span class="text">온라인 · 사전</span>
            </label>
            <label class="checkbox">
              <input type="checkbox">
              <span class="custom-checked"></span>
              <span class="text">K-MOOC</span>
            </label>
          </div>
        </div>

        <button class="btn-sm font-400 btn-point mt-20 goods-search-btn">검색</button>
      </div>
    </article>
    <article class="content-wrap">
      <div class="textbook-banner-wrap">
        <!-- 배너 -->
        <a href="/lms/common/app.do">
          <p>내 손안의 작은 사전, <strong>특수외국어 필수어휘사전 App 출시</strong></p>
          <p class="textbook-banner">11개 특수외국어 필수어휘사전 및 초급교재 샘플자료를 무료로 제공합니다.</p>
          <img src="${CML}/imgs/page/textbook/btn_banner_more.png" alt="MORE">
        </a>
      </div>
    </article>
    <article class="content-wrap">
      <!-- 게시판 -->
      <div class="materials-files-wrap">
        <div class="flex-row">
        	<c:forEach var="result" items="${resultList}" varStatus="status">
	       		<c:choose>
	       			<c:when test="${result.bbsId eq 'BBSMSTR_000000000005'}">
	       				<c:url var="viewUrl" value="/cop/bbs/selectBoardArticle.do">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="menuId" value="MNU_0000000000000008" />
						  	<c:param name="bbsId" value="BBSMSTR_000000000005" />
					    </c:url>
               		</c:when>
               		<c:when test="${result.bbsId eq 'BBSMSTR_000000000007'}">
               			<c:url var="viewUrl" value="${result.tmp05}">
					    </c:url>
               		</c:when>
	       		</c:choose>
	          <div class="flex-col-6">
	            <a href="${viewUrl}" class="card-st2-wrap" target="_blank">
	            	<c:set var="imgSrc">
						<c:choose>
                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
                    		<c:otherwise>
                    			<c:url value='/cmm/fms/getImage.do'>
                    				<c:param name="thumbYn" value="Y" />
                    				<c:param name="siteId" value="SITE_000000000000001" />
                    				<c:param name="appendPath" value="${result.bbsId}" />
                    				<c:param name="atchFileNm" value="${result.atchFileNm}" />
                    			</c:url>
                    		</c:otherwise>
                    	</c:choose>
					</c:set>
	              <div class="card-st2-img" style="background-image:url(${imgSrc})"></div>
	              <div class="card-title-wrap">
	                <span class="card-category font-darkgreen">
	                	<c:choose>
	                		<c:when test="${result.bbsId eq 'BBSMSTR_000000000005'}">
	                			<c:out value="${result.tmp04 }"/>
	                		</c:when>
	                		<c:when test="${result.bbsId eq 'BBSMSTR_000000000007'}">
	                			<c:out value="K-MOOC"/>
	                		</c:when>
	                		<c:otherwise>
	                		-
	                		</c:otherwise>
	                	</c:choose>
	                </span>
	                <div class="card-title dotdotdot">${result.nttSj }</div>
	                
	                <c:if test="${result.bbsId ne 'BBSMSTR_000000000007'}"><span class="card-subtitle">저자 ${result.tmp02 }</span></c:if>
	              </div>
	            </a>
	          </div>
	        </c:forEach>
        </div>
        <%-- <div class="mt-50 center-align">
          <button type="button" class="cursor-pointer addPage" data-pageindex="1" data-clickflag="Y">
            <img class="vertical-top addPage" src="${CML}/imgs/common/btn_board_contents_more.jpg" alt="더보기">
          </button>
        </div> --%>
      </div>
    </article>
  </section>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>