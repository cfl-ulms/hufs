<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
<c:when test="${searchVO.tmplatImportAt ne 'N'}">

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
	<c:param name="listAt" value="Y"/>
</c:import>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<script src="${_C_JS}/board.js" ></script>
<c:choose>
<%-- 교재/사전(공통) --%>
<c:when test="${searchVO.menuId eq 'MNU_0000000000000008'}">
<script>
<c:if test="${!empty brdMstrVO.ctgrymasterId}">		
	var boardCateLevel = ${boardCateLevel};
	var boardCateList = new Array(${fn:length(boardCateList)});
	<c:forEach var="cate" items="${boardCateList}" varStatus="status">
		boardCateList[${status.index}] = new ctgryObj('<c:out value='${cate.upperCtgryId}'/>', '<c:out value='${cate.ctgryId}'/>', '<c:out value='${cate.ctgryNm}'/>', <c:out value='${cate.ctgryLevel}'/>);
	</c:forEach>		
</c:if>

function fn_egov_addNotice(url) {
   	<c:choose>
		<c:when test="${not empty USER_INFO.id}">
			location.href = url;
		</c:when>
		<c:otherwise>
			if (confirm('로그인 하시겠습니까?')) {
				location.href = "<%=egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper.getRedirectLoginUrl()%>";	
			}
		</c:otherwise>
	</c:choose>
}

$(document).ready(function(){
	$('#btnBbsWrite').click(function() {fn_egov_addNotice(this.href);return false;});
	fnCtgryInit('<c:out value="${searchVO.searchCateList}"/>');
	
	//페이징 버튼
	$(".start, .prev, .next, .end").click(function(){
 		var url = $(this).data("url");
 		
 		location.href = url;
 	});
	
	$(".btn-gallery, .btn-list").click(function(){
		var type = $(this).data("type");
		
		$("#viewType").val(type);
		$("#frm").submit();
	});
	
	//글등록
	$('.btnModalOpen').click(function() {
		var href = $(this).data("href");
		$(".btnModalConfirm").click(function(){
			location.href = href;
		});
		return false;
	});
	
	//언어 카테고리
	$(".tab-list > a").click(function(){
		var id = $(this).data("id");
		
		$("input[name=searchCateList]").val(id);
		$("#frm").submit();
		return false;
	});
	
	//더보기
	$(".btn_more").click(function(){
		var curPage = $(this).data("page"),
			bbsId = $("input[name=bbsId]").val(),
			menuId = $("input[name=menuId]").val(),
			searchCate = $("input[name=searchCate]").val(),
			searchCateList = $("input[name=searchCateList]").val(),
			page = curPage + 1,
			lastPage = $("#lastPage").val();
		
		if(lastPage == curPage){
			alert("마지막 페이지 입니다.");
		}else{
			$.ajax({
				type : "post"
				, url : "/msi/ctn/moreBoardList.do"
				, data : {bbsId : bbsId, menuId : menuId, searchCate : searchCate, searchCateList : searchCateList, pageIndex : page}
				, dataType : "html"
				, success : function(data){
					$("#box_book").append(data);
					$(".btn_more").data("page", page);
					if(lastPage == page){
						$(".btn_more").hide();
					}
				}, error : function(){
					alert("데이터 불러 들이는데 실패했습니다.");
				}
			});	
		}
	});
});
</script>

    <div class="textbook-top-wrap">
        <div class="textbook-slide-wrap textbookSlide" data-slick="true">
            <!-- 슬라이드 -->
            <c:forEach var="result" items="${noticeList}" varStatus="status">
                <c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
                    <c:param name="nttNo" value="${result.nttNo}" />
                    <c:param name="pageIndex" value="${searchVO.pageIndex}" />
                </c:url>
                <div class="textbook-contents-wrap">
                    <div class="textbook-img" onclick="location.href='${viewUrl }'" style="cursor: pointer;background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);"></div>
                    <a href="${viewUrl }">
                        <div class="textbook-inner">
                            <div class="textbook-flag">
                                <c:set var="imgSrc">
                                    <c:import url="/lms/common/flag.do" charEncoding="utf-8">
                                        <c:param name="ctgryId" value="${result.ctgryId}"/>
                                    </c:import>
                                </c:set>
                                <span class="flag-img">
                                    <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
                                </span>
                                <span class="text"><c:out value="${result.ctgryNm}"/> 교재</span>
                            </div>
                            <div class="book-name"><c:out value="${result.nttSj}"/></div>
                            <dl class="book-info">
                                <dt class="info-title">출판사</dt>
                                <dd class="info-name"><c:out value="${result.tmp01}"/></dd>
                            </dl>
                            <dl class="book-info">
                                <dt class="info-title">저자</dt>
                                <dd class="info-name"><c:out value="${result.tmp02}"/></dd>
                            </dl>
                            <div class="desc dotdotdot">
                                <c:out value="${result.nttCn}" escapeXml="false"/>
                            </div>
                        </div>
                    </a>
                </div>
            </c:forEach>
        </div>
        <div class="textbook-app-wrap">
            <div class="appInfo">
                <div class="appLeft">
                    <img class="appLogo" src="${CML}/imgs/page/subtop/img_subtop_textbook_appLogo.jpg" alt="HUFS App Logo" />
                    <a class="btn_googlePlay" href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" target="_blank"></a>
                    <a class="btn_appStore" href="https://apps.apple.com/us/app/%ED%8A%B9%EC%88%98%EC%99%B8%EA%B5%AD%EC%96%B4-%ED%95%84%EC%88%98%EC%96%B4%ED%9C%98%EC%82%AC%EC%A0%84/id1489668996?l=ko&ls=1" target="_blank"></a>
                </div>
                <div class="appRight">
                    <h3>특수외국어 필수어휘학습사전</h3>
                    <strong>특수외국어 교육진흥원<span>무료</span></strong>
                    <p>11개 언어에 대해 단어보기, 단어학습, 단어퀴즈 등<br /> 다양한 형태로 학습 방법을 제공합니다.</p>
                    <div class="img-wrap">
                        <div><img src="${CML}/imgs/page/textbook/img_app_01.png" alt="img_app_01" /></div>
                        <div><img src="${CML}/imgs/page/textbook/img_app_02.png" alt="img_app_02" /></div>
                        <div><img src="${CML}/imgs/page/textbook/img_app_03.png" alt="img_app_03" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="textbook-banner-wrap">
      <!-- 배너 -->
      <a href="/lms/common/app.do">
        <p>특수외국어 초급교재 및 필수어휘학습사전 <strong>무료 다운로드</strong></p>
        <p class="textbook-banner">11개 특수외국어 필수어휘 학습사전 및 초급교재 샘플을 무료로 제공합니다.</p>
        <img src="${CML}/imgs/page/textbook/btn_banner_more.png" alt="MORE">
      </a>
    </div>
	
	<section class="section-gap">
      <!-- 특수외국어 출간교재 -->
      <div class="main-common-title">
        <h2 class="title">특수외국어 출간교재</h2>
        <p class="sub-title">특수외국어 표준 교육과정을 바탕으로 한 기초 교재부터 e-book까지 제공합니다.</p>
      </div>
      
      <form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
		<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
		<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
		<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
		<input name="searchCateList" type="hidden" value="<c:out value='${searchVO.searchCateList[0]}'/>" />
		<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>" />
		<input id="lastPage" type="hidden" value="${paginationInfo.totalPageCount}" />
        <div class="box-wrap mb-55">
			<h3 class="title-subhead"><c:out value="${brdMstrVO.bbsNm}"/> 검색</h3>
			<div class="flex-row-ten">
				<div class="flex-ten-col-4">
		            <div class="ell">
		            	<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" placeholder="교재/사전명"/>
		            </div>
				</div>
		        <div class="flex-ten-col-2">
		            <div class="ell">
		              <input name="searchTmp01" value="<c:out value="${searchVO.searchTmp01}"/>" type="text" placeholder="출판사 명">
		            </div>
		        </div>
		        <div class="flex-ten-col-2">
		            <div class="ell">
		              <input name="searchTmp02" value="<c:out value="${searchVO.searchTmp02}"/>" type="text" placeholder="저자명">
		            </div>
		        </div>
		        <div class="flex-ten-col-2 flex align-items-center">
		            <label class="checkbox">
		              <input type="checkbox" name="tmp03" value="Y" <c:if test="${searchVO.tmp03 eq 'Y'}">checked="checked"</c:if>>
		              <span class="custom-checked"></span>
		              <span class="text">e-book</span>
		            </label>
		        </div>
			</div>
			<button class="btn-sm font-400 btn-point mt-20" type="submit">검색</button>
		</div>
	</form>
      
      <!-- tab style -->
      <ul class="tab-wrap line-style line">
        <li class="tab-list <c:if test="${empty searchVO.searchCateList[0]}">on</c:if>">
          <a href="#" data-id="">전체</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'MN'}">on</c:if>">
          <a href="#" data-id="MN">몽골어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'SW'}">on</c:if>">
          <a href="#" data-id="SW">스와힐리어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'UZ'}">on</c:if>">
          <a href="#" data-id="UZ">우즈베크어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'FA'}">on</c:if>">
          <a href="#" data-id="FA">이란어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'MI'}">on</c:if>">
          <a href="#" data-id="MI">말레이인도네시아어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'TR'}">on</c:if>">
          <a href="#" data-id="TR">터키어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'TH'}">on</c:if>">
          <a href="#" data-id="TH">태국어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'PT'}">on</c:if>">
          <a href="#" data-id="PT">포르투갈/브라질어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'HU'}">on</c:if>">
          <a href="#" data-id="HU">헝가리어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'PL'}">on</c:if>">
          <a href="#" data-id="PL">폴란드어</a>
        </li>
        <li class="tab-list <c:if test="${searchVO.searchCateList[0] eq 'HI'}">on</c:if>">
          <a href="#" data-id="HI">힌디어</a>
        </li>
      </ul>
      <div class="page-content-header mb-0">
        <!-- 교재 리스트 -->
        <div id="box_book">
	        <c:forEach var="result" items="${resultList}" varStatus="status">
	        	<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
				  	<c:param name="nttNo" value="${result.nttNo}" />
				  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
			    </c:url>
		        <div class="textbook-contents-view">
		          <div class="left-area">
		            <div class="textbook-img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>&amp;width=393&amp;height=235);"></div>
		          </div>
		          <div class="right-area">
		            <div class="textbook-inner">
		              <a href="${viewUrl}">
		                <div class="textbook-flag">
						  <c:set var="imgSrc">
							<c:import url="/lms/common/flag.do" charEncoding="utf-8">
								<c:param name="ctgryId" value="${result.ctgryId}"/>
							</c:import>
		          		  </c:set>
		                  <span class="flag-img"><img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
		                  <span class="text"><c:out value="${result.ctgryNm}"/> 교재</span>
		                </div>
		                <div class="book-name">
		                	<c:if test="${result.tmp03 eq 'Y'}">
		                		<span class="type">e-book</span>
		                	</c:if>
		                	<c:out value="${result.nttSj}"/>
		                </div>
		                <dl class="book-info">
		                  <dt class="info-title">출판사</dt>
		                  <dd class="info-name"><c:out value="${result.tmp01}"/></dd>
		                </dl>
		                <dl class="book-info">
		                  <dt class="info-title">저자</dt>
		                  <dd class="info-name"><c:out value="${result.tmp02}"/></dd>
		                </dl>
		                <div class="desc dotdotdot">
		                	 <c:out value="${result.nttCn}" escapeXml="false"/>
		                </div>
		              </a>
		            </div>
		            <div class="textbook-button">
		              <a href="${viewUrl}" class="btn-full btn-outline" target="_blank">상세보기</a>
		              <%-- <a href="<c:out value="${result.tmp05}"/>" class="btn-full btn-point" target="_blank">구매하기</a> --%>
		              <div class="util-wrap">
		                <%-- <button class="btn-share btnModalOpen" data-modal-type="share" onclick="location.href='http://${siteInfo.siteUrl}${viewUrl}'" data-title="<c:out value="${result.nttSj}"/>" title="공유하기"></button> --%>
		                <c:choose>
		                	<c:when test="${not empty USER_INFO.id}">
		                		<c:set var="wishAt" value=""/>
		                		<c:forEach var="wishList" items="${wishList}" varStatus="status">
		                			<c:if test="${wishList.trgetId eq result.nttNo}">
		                				<c:set var="wishAt" value="on"/>
		                			</c:if>
		                		</c:forEach>
		                		<button class="btn-wishlist ${wishAt}" title="관심수강 담기" data-code="BOOK_LIKE" data-id="${result.nttNo}"></button>
		                	</c:when>
		                	<c:otherwise>
		                		<button class="btn-wishlist btnModalOpen"
					                      data-modal-type="confirm"
					                      data-modal-header="알림"
					                      data-modal-text="회원가입이 필요한 서비스입니다."
					                      data-modal-subtext="로그인 후 이용이 가능합니다. 회원가입 화면으로 이동하시겠습니까?"
					                      data-modal-rightbtn="확인"
					                      onclick="location.href='/uss/umt/cmm/EgovStplatCnfirmMber.do'"
					                      title="관심수강 담기">
					              </button>
		                	</c:otherwise>
		                </c:choose>
		              </div>
		            </div>
		          </div>
		        </div>
	        </c:forEach>
		</div>
      	<div class="mt-50 center-align">
        	<button type="button" class="cursor-pointer btn_more" data-page="1"><img class="vertical-top" src="${CML}/imgs/common/btn_board_contents_more.jpg" alt="더보기"></button>
      	</div>
    </section>
</c:when>
<c:otherwise>
	<script type="text/javascript">
	$(document).ready(function(){
		//검색
		$(".goods-search-btn").click(function(){
			$("input[name=searchWrd]").val($("#searchWrd").val());
			$("input[name=searchBgnDe]").val($("#searchBgnDe").val());
			$("input[name=searchEndDe]").val($("#searchEndDe").val());
			$("#frm").submit();
		});
	});
	</script>	
	<!-- 콘텐츠바디 -->
    <section class="page-content-body">
      <article class="content-wrap">
        <!-- 게시판 검색영역 -->
        <div class="box-wrap mb-40">
          <h3 class="title-subhead">교재 검색</h3>
          <div class="flex-row-ten">
          	<form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
				<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
				<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
				<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
				<input name="searchCnd" type="hidden" value="0" />
				<input name="searchWrd" type="hidden" value="<c:out value="${searchVO.searchWrd}"/>"/>
				<input name="searchBgnDe" type="hidden" value="<c:out value="${searchVO.searchBgnDe}"/>"/>
				<input name="searchEndDe" type="hidden" value="<c:out value="${searchVO.searchEndDe}"/>"/>
			</form>
            <div class="flex-ten-col-4">
              <div class="desc">
                <input type="text" id="searchBgnDe" class="ell date datepicker type2" placeholder="등록일" value="<c:out value="${searchVO.searchBgnDe}"/>">
                <i>~</i>
                <input type="text" id="searchEndDe" class="ell date datepicker type2" placeholder="등록일" value="<c:out value="${searchVO.searchEndDe}"/>">
              </div>

            </div>
            <div class="flex-ten-col-2">
              <div class="ell">
                <select name="searchCate" id="searchCate" class="select2" data-select="style3" data-placeholder="교재명">
                  <option value=""></option>
                  <option value="0">교재명</option>
                </select>
              </div>
            </div>
            <div class="flex-ten-col-4">
              <div class="ell">
                <input id="searchWrd" type="text" placeholder="제목을 입력해보세요.">
              </div>
            </div>
          </div>

          <button class="btn-sm font-400 btn-point mt-20 goods-search-btn">검색</button>
        </div>
      </article>
      <article class="content-wrap">
        <!-- 게시판 -->
        <div class="content-body">
          <!-- 테이블영역-->
          <table class="common-table-wrap table-type-board">
            <colgroup>
              <col style='width:7%'>
              <col style='width:12%'>
              <col style='width:12%'>
              <col>
              <col style='width:12%'>
              <col style='width:12%'>
              <col style='width:100px'>
            </colgroup>
            <thead>
              <tr class='bg-light-gray font-700'>
                <th scope='col'>No</th>
                <th scope='col'>책표지</th>
                <th scope='col'>언어</th>
                <th scope='col'>교재명</th>
                <th scope='col'>출판사</th>
                <th scope='col'>저자</th>
                <th scope='col'>등록일</th>
              </tr>
            </thead>
            <tbody>
            	<c:forEach var="result" items="${resultList}" varStatus="status">
            		
            	  <tr class="">
	                <td scope='row'>${status.count}</td>
	                <td class='book-img-wrap'><img class='vertical-mid' src='${CML}/imgs/common/${result.atchFileNm}'></td>
	                <td>${result.ctgryNm}</td>
	                <td><a href="/cop/bbs/selectBoardArticle.do?nttNo=${result.nttNo}&menuId=MNU_0000000000000008&bbsId=BBSMSTR_000000000005&pageIndex=${searchVO.pageIndex}" target="_blank">${result.nttSj}</a></td>
	                <td>${result.tmp01}</td>
	                <td>${result.tmp02}</td>
	                <td><fmt:formatDate value="${result.frstRegisterPnttm}" pattern="yyyy-MM-dd"/></td>
	              </tr>
            	</c:forEach>
            	<c:if test="${fn:length(resultList) == 0}">
          			<tr class="empty"><td colspan="7"><spring:message code="common.nodata.msg" /></td></tr>
            	</c:if>
            </tbody>
          </table>
          <div class="right-align mt-20">
          	<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
				<c:param name="registAction" value="regist" />
			</c:url>
            <a href="${addBoardArticleUrl}" class="btn-point btn-sm">교재 신규등록</a>
          </div>
          <div class="pagination center-align">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
				</div>
		  </div>
        </div>
      </article>
    </section>
  </div>
</div>
</div>
</div>
</c:otherwise>
</c:choose>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
	<c:param name="shareAt" value="Y"/>
</c:import>

</c:when>
<c:otherwise>

<!DOCTYPE html>
<html lang="ko">
<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title></title>

  <meta name="title" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="/template/lms/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/template/lms/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="/template/lms/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="/template/lms/css/common/base.css?v=1">
  <link rel="stylesheet" href="/template/lms/css/common/common.css?v=1">
  <link rel="stylesheet" href="/template/lms/css/common/board.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="/template/lms/css/textbook/textbook.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/textbook/textbook.1.1.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/modal.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
  <script src="/template/lms/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="/template/lms/js/common.js?v=1"></script>
  <!--=================================================
          페이지별 스크립트
  ==================================================-->
  <script src="/template/lms/js/textbook/textbook.js?v=1"></script>
</head>

<body>
<script>
$(document).ready(function(){
	$(".add_book").click(function(){
		var id = $(this).data("id"),
			sj = $(this).data("sj"),
			publish = $(this).data("publish");
		
		
		window.opener.addbook(id, sj, publish);
		window.close();
		return false;
	});
});
</script>
<section class="area">
    <section class="section-gap">
      <div class="page-content-header mb-0">
        <!-- 교재 리스트 -->
        <c:forEach var="result" items="${resultList}" varStatus="status">
	        <div class="textbook-contents-view">
	          <div class="left-area">
	            <div class="textbook-img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);"></div>
	          </div>
	          <div class="right-area">
	            <div class="textbook-inner">
	              <a href="#" class="add_book" data-id="${result.nttNo}" data-sj="${result.nttSj}" data-publish="${result.tmp01}">
	                <div class="textbook-flag">
					  <c:set var="imgSrc">
						<c:import url="/lms/common/flag.do" charEncoding="utf-8">
							<c:param name="ctgryId" value="${result.ctgryId}"/>
						</c:import>
	          		  </c:set>
	                  <span class="flag-img"><img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
	                  <span class="text"><c:out value="${result.ctgryNm}"/> 교재</span>
	                </div>
	                <div class="book-name">
	                	<c:if test="${result.tmp03 eq 'Y'}">
	                		<span class="type">e-book</span>
	                	</c:if>
	                	<c:out value="${result.nttSj}"/>
	                </div>
	                <dl class="book-info">
	                  <dt class="info-title">출판사</dt>
	                  <dd class="info-name"><c:out value="${result.tmp01}"/></dd>
	                </dl>
	                <dl class="book-info">
	                  <dt class="info-title">저자</dt>
	                  <dd class="info-name"><c:out value="${result.tmp02}"/></dd>
	                </dl>
	                <div class="desc dotdotdot">
	                	 <c:out value="${result.nttCn}" escapeXml="false"/>
	                </div>
	              </a>
	            </div>
	          </div>
	        </div>
        </c:forEach>
      </div>
    </section>
  </section>
</body>
</html>
</c:otherwise>
</c:choose>
