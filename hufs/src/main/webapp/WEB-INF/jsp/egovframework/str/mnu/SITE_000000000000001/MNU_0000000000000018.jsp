<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section class="section-gap special-common">
<div class="area">
<div class="page-common-title">
<h2 class="title center-align">5대 핵심사업</h2>
<p class="desc"><img class="mb-20" src="../../../template/lms/imgs/page/special/img_03.jpg" alt="1.특수외국어 표준화 위원회, 2.언어별 표준교육과정 및 교재개발 TF, 3.특수외국어 평가 및 인증 위원회, 4.언어별 교재개발 TF" /></p>
</div>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">표준교육과정 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">
<p class="desc">표준교육과정은 국민들에게 다양하고 전문적인 특수외국어 교육 기회의 제공과 특수외국어 전문 인재 양성이라는 목표를 달성하기 위한 교육 내실화의 기본 요건으로서, 교육과정의 표준화를 통해 특수외국어 교육의 체계화와 내실화를 도모하고자 합니다.</p>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">우리 대학 내 &lsquo;특수외국어 표준화 위원회&rsquo; 설립</h5>
<p class="list">우리 대학 구성원뿐만 아니라 국내외 특수외국어 관련 전문가들과 언어별 TF 구성</p>
<p class="list">특수외국어 표준 교육과정을 토대로 단계별 교재 개발 및 평가인증체제 기반 마련</p>
<p class="list">범국가적 &lsquo;아시아언어 공통기준&rsquo; 개발<br />(라틴문자에 기초한 유럽언어와 달리 고유문자를 가지고 있는 아시아 언어들에 대한 교육과정 표준화)</p>
<h5 class="title mt-20 icon-num">CEFR 체계를 참고, 언어별 특수외국어 6단계(A1, A2, B1, B2, C1, C2) 표준교육과정 3차 년도(2020년)까지 개발</h5>
<p class="list">언어별 개발목표의 현실화: 스와힐리어, 폴란드어, 헝가리어는 B2까지 개발하며, 이외 언어는 C2까지 개발</p>
<p class="list">단계별 어휘 및 can do list 작성</p>
<p class="list">A1과 A2에 해당되는 기초교육은 일반인과 대학교 1학년 학생 수준 고려하여 구체적 학습목표 제시</p>
<p class="list">개발된 표준교육과정에 근거하여 대학 내 개설된 교과 개편 또는 교과목 단계 구분 표기</p>
<p class="list">전문교육기관부터 표준교육과정 시범 적용 후 공유 및 확산</p>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">표준 교재 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">
<p class="desc">표준 교육과정의 내용을 바탕으로 한 기초 교재의 개발을 통해 특수외국어를 학습하는 전공 학생 뿐 아니라 일반인들도 표준화된 양질의 교육을 제공 받을 수 있는 기반을 마련하고자 합니다.</p>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">특수외국어 표준 교육과정을 바탕으로 한 표준 교재를 단계별(A1~C2)로 개발</h5>
<h5 class="title icon-num">기초교육 단계인 A1과 A2는 특수외국어 표준화 위원회에서 구상하여 언어별 TF 팀에서 공동개발</h5>
<h5 class="title icon-num">A1과 A2의 초급 교재는 App북으로 개발하여 일반에 공개</h5>
<h5 class="title icon-num">저변확대를 위하여 표준교육과정 및 교재와 연동한 K-MOOC 개발</h5>
<h5 class="title icon-num">B1과 B2의 중급단계 교재 또한 체계적인 학부생 양성을 위해 표준화된 교재 공동 개발</h5>
<h5 class="title icon-num">C1과 C2의 고급단계 교재는 전공언어별 자율성 및 학문적 특수성을 반영하여 개별 개발</h5>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">표준평가문항 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">
<p class="desc">현재 대학 내에서만 이루어지고 있는 특수외국어능력 평가시스템의 한계를 극복하고, 언어별로 수준의 차이없이 한국인 학습자에 최적화된 특수외국어평가시험 CFLPT(Critical Foreign Language Proficiency Test) 체제를 구축하는 것을 목표로 하고 있습니다.</p>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">CFLPT(Critical Foreign Language Proficiency Test) 평가인증체제 개발</h5>
<p class="list">특수외국어 평가&middot;인증 위원회를 통한 평가유형 및 내용에 대한 세부 사항 개발</p>
<h5 class="title mt-20 icon-num">표준 교육과정 및 공통교재와 연계한 평가</h5>
<p class="list">단계별 학습목표에 따른 성취도를 측정할 수 있는 표준화된 평가체계 완성</p>
<p class="list">적절한 난이도 분석을 통해 표준교재를 활용한 개별 학습 및 K-MOOC 수강 후 해당 학습 등급의 시험 응시가 가능하도록 추진</p>
<h5 class="title mt-20 icon-num">타 대학 특수외국어 전문가 참여 공동 개발</h5>
<p class="list">언어권 해당국가의 공인인증 시험이 있는 경우 이를 고려하여 상호보완적 개발</p>
<p class="list">타 전문교육기관 특수외국어 전문가와 함께 공동 개발하고, 평가시스템은 우리 대학에 구축함</p>
<h5 class="title mt-20 icon-num">문제은행 개발 및 CFLPT 국가 공인화</h5>
<p class="list">평가인증체계의 공신력 확보를 위해 특수외국어 전문교육기관과 협의체를 구성하여 공동 개발함</p>
<p class="list">개발된 문항관리를 위한 시스템 구축</p>
<p class="list">각 단계별 평가는 읽기와 듣기 2개 영역 기준 총 50문항으로 1개 세트를 구성하는 것을 기준으로 하여 연간 10개 세트 이상의 문항 확보&nbsp;</p>
<p class="list">언어의 특수성에 따라 언어마다 개발 시기 및 목표를 다르게 설정</p>
<p class="list">CFLPT 국가 공인화를 위해 지속적인 노력 병행</p>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">사전개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">
<p class="desc">편의성과 효율성이 극대화된 자기주도적 학습을 위해 표준교재와 연계한 CFL 단계별 필수어휘 학습사전을 App으로 개발 제공하고, 동시에 개별 언어별 필요와 학문적 특수성을 고려한 특수목적사전을 개발하여 전공자들의 학습 효과를 제고하고자 합니다.&nbsp;&nbsp;</p>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">CFL 단계별 필수어휘 사전 공통 플랫폼 APP형태로 설계 개발하고 표준교재와 연동하여 자기주도적 학습 기회 제공</h5>
<h5 class="title icon-num">CFL 필수어휘사전의 경우 A1 ~ B2 단계를 우선 개발하며, 수요에 따라 2단계 사업에서는 C단계로 순차적으로 개발</h5>
<h5 class="title icon-num">1단계 사업기간 내 최소 5건의 특수목적사전 개발 추진</h5>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">K-MOOC 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">
<p class="desc">특수외국어 온라인 학습 과정을 K-MOOC으로 개발하여 제공함으로써 국민 누구나 쉽고 편하게 특수외국어를 학습할 수 있는 기회를 제공하고 전공 수업에의 활용을 통해 대학 수업의 혁신을 도모하고자 합니다.</p>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">온라인 교육 컨텐츠(Language MOOC) 개발</h5>
<p class="list">특수외국어 교육 아시아 거점화 및 우수 강의 공개 플랫폼 공유를 목표로 특수외국어교육관련 Language-MOOC 개발</p>
<p class="list">5개 권역별(유럽, 유라시아, 인도&middot;아세안, 중동&middot;아프리카, 중남미)로 각 1~2개, 최소 6개 이상의 콘텐츠 제작</p>
<p class="list">특수외국어 교육 저변 확대에 기여하기 위해 1년차에 먼저 &lsquo;태국어(6주차)&rsquo;개발 및 탑재를 하고 2년차부터 점진적으로 확대하여 1단계 사업 종료시 최소 6개 이상의 K-MOOC 개발</p>
<h5 class="title mt-20 icon-num">K-MOOC 탑재</h5>
<p class="list">1단계 기간 동안 6개 이상의 특수외국어 교육 MOOC 콘텐츠를 개발하여 K-MOOC에 탑재</p>
<p class="list">특수외국어교육진흥원 홈페이지에 병행 게재함으로써 우수 강의에 대한 접근성 제고</p>
</div>
</div>
</div>
</article>
</div>
</section>