<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
  <section class="section-gap bg-gray-light">
    <div class="main-common-title2">
      <h2 class="title">외국어 시험 CFLPT 이란?</h2>
    </div>
    <div class="area">
      <div class="banner-content-wrap">
        <div class="inner-area">
          <p class="desc">
            <b>CFLPT(Critical Foreign Language Proficiency Test)</b>
          </p>
          <p class="desc">
            CFLPT는 한국외국어대학교의 FLEX 개발 노하우를 바탕으로 개발된 특수외국어 사용 능력을 가장 효과적으로 평가할 수 있는 표준화된 시험입니다.<br>
            CFLPT는 유럽언어공통기준(Common European Framework of Reference for Languages: CEFR)인<br>6등급제를 기반으로 개발되고 있는 특수외국어 표준교육과정을 바탕으로 A1,
            A2, B1, B2, C1 및 C2의 6등급 체계로 구분되어 시행됩니다.
          </p>
          <p class="desc">
            현재 총 11개의 특수외국어 읽기·듣기 시험을 컴퓨터 시험 형태로 제공되고 있으며,<br>특수외국어 어학능력 검증, 교육과정의 단계별 학습 성취도 측정 뿐 아니라 주요 기관 및 기업의 외국어 우수 인재 선발을 위한
            도구로 활용될 수 있습니다.
          </p>
          <script>
          	$(document).ready(function(){
          		$(".ready").click(function(){
          			alert("준비 중입니다.");
          			return false;
          		});
          	});
          </script>
          <a href="#" class="btn-link btn-xl btn-point ready">CFLPT 컴퓨터시험 안내 영상</a>
        </div>
      </div>
    </div>
  </section>
  <div class="page-content-wrap pt-70" style="min-height:405px;padding:40px 0 0;">
    <section class="page-content-body">
      <div class="area">
        <article class="content-wrap">
          <div class="content-header">
            <div class="title-wrap">
              <div class="title">평가등급</div>
              <div class="desc">
                CFLPT는 평가의 정확성을 기하기 위해 특수외국어 평가를 등급제 평가 체제로 정하고 <br>A1, A2, B1, B2, C1, C2의 평가를 각각 독립적인 평가 체제로 하며 읽기와 듣기(예, A1 읽기와 듣기)를 묶어
                각 등급별로 시행됩니다.
              </div>
            </div>
          </div>
          <div class="content-header">
            <div class="title-wrap">
              <div class="title">문항 수와 시험시간</div>
              <div class="desc">
                CFLPT의 시험시간은 등급에 따라 상이하나 최대 180분을 넘지 않으며, 문항당 1분 ~ 1분 30초 정도의 소요시간을 기준으로 구성됩니다. <br>가장 낮은 단계인 A1 등급의 시험 시간을 영역별로 구분하면 다음과
                같습니다.
              </div>
            </div>
          </div>
          <div class="content-body">
            <!-- 테이블영역-->
            <table class="common-table-wrap size-sm">
              <colgroup>
                <col style='width:14.285%'>
                <col>
                <col>
                <col>
              </colgroup>
              <thead>
                <tr class='bg-gray font-700'>
                  <th scope='col'></th>
                  <th scope='colgroup' colspan='2'>문항수</th>
                  <th scope='col'>시험시간</th>
                </tr>
              </thead>
              <tbody>
                <tr class="">
                  <td scope='row'>듣기</td>
                  <td>A1 25문항</td>
                  <td rowspan='2'>50문항</td>
                  <td>25~30분</td>
                </tr>
                <tr class="">
                  <td scope='row'>읽기</td>
                  <td>A1 25문항</td>
                  <td>35~40분</td>
                </tr>
              </tbody>
            </table>
          </div>
		</article>
      </div>
    </section>
  </div> 