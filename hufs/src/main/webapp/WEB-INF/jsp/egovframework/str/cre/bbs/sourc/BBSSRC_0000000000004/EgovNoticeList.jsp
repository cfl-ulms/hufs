<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
	<c:if test="${not empty searchVO.viewType}"><c:param name="viewType" value="${searchVO.viewType}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
		</c:import>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#btnBbsWrite').click(function() {fn_egov_addNotice(this.href);return false;});
	
	//페이징 버튼
	$(".start, .prev, .next, .end").click(function(){
 		var url = $(this).data("url");
 		
 		location.href = url;
 	});
	
	//검색
	$(".goods-search-btn").click(function(){
		$("input[name=searchWrd]").val($("#searchWrd").val());
		$("input[name=searchBgnDe]").val($("#searchBgnDe").val());
		$("input[name=searchEndDe]").val($("#searchEndDe").val());
		$("#frm").submit();
	});
});
</script>		

<c:choose>
	<c:when test="${searchVO.menuId eq 'MNU_0000000000000104'}">
		<section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">K-MOOC 검색</h3>
                <div class="flex-row-ten">
	                <form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
						<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
						<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
						<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
						<input name="searchCnd" type="hidden" value="0" />
						<input name="searchWrd" type="hidden" value="<c:out value="${searchVO.searchWrd}"/>"/>
						<input name="searchBgnDe" type="hidden" value="<c:out value="${searchVO.searchBgnDe}"/>"/>
						<input name="searchEndDe" type="hidden" value="<c:out value="${searchVO.searchEndDe}"/>"/>
					</form>
						
	                  <div class="flex-ten-col-4">
	                    <div class="desc">
	                      <input type="text" id="searchBgnDe" class="ell date datepicker type2" placeholder="등록일" value="<c:out value="${searchVO.searchBgnDe}"/>">
	                      <i>~</i>
	                      <input type="text" id="searchEndDe" class="ell date datepicker type2" placeholder="등록일" value="<c:out value="${searchVO.searchEndDe}"/>">
	                    </div>
	
	                  </div>
	                  <div class="flex-ten-col-2">
	                    <div class="ell">
	                      <select name="" id="" class="select2" data-select="style3" data-placeholder="제목">
	                        <option value=""></option>
	                        <option value="0">제목</option>
	                      </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4">
	                    <div class="ell">
	                      <input type="text" id="searchWrd" placeholder="제목을 입력해보세요." value="<c:out value="${searchVO.searchWrd}"/>">
	                    </div>
	                  </div>
	                
                </div>
 
                <button class="btn-sm font-400 btn-point mt-20 goods-search-btn">검색</button>
              </div>
            </article>
            <article class="content-wrap">
              <div class="flex-row board-video-thumb-wrap">
                <c:forEach var="result" items="${resultList}" varStatus="status">
					<div class="flex-col-4">
						<a href="<c:out value="${result.tmp05}"/>" target="_blank">
							<c:set var="imgSrc">
								<c:choose>
		                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
		                    		<c:otherwise>
		                    			<c:url value='/cmm/fms/getImage.do'/>?thumbYn=Y&amp;siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>
		                    		</c:otherwise>
		                    	</c:choose>
							</c:set>
			                <div class="board-video-img" style="background-image:url(${imgSrc});">
			                  <span class="text-hide">동영상썸네일</span>
			                </div>
			                <p class="board-video-title ell"><c:out value="${result.nttSj}"/></p>
				            <ul class="card-items-wrap">
				              	<li class="card-items icon-campus"><c:out value="${result.tmp03}"/></li>
				              	<li class="card-items icon-date"><c:out value="${result.tmp01}"/> ~ <c:out value="${result.tmp02}"/></li>
				            </ul>
			            </a>
					</div>
				</c:forEach>
              </div>
              <div class="right-align mt-20">
              	<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
					<c:param name="registAction" value="regist" />
				</c:url>
                <a href="${addBoardArticleUrl}" class="btn-point btn-sm btn-new-crcl">신규강좌 등록</a>
              </div>
              <div class="pagination center-align mt-10">
					<div class="pagination-inner-wrap overflow-hidden inline-block">
						<c:url var="startUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="1" />
		   				</c:url>
		               	<button class="start" data-url="${startUrl}"></button>
		               
		               	<c:url var="prevUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
							<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
						</c:url>
		               	<button class="prev" data-url="${prevUrl}"></button>
		               
		               	<ul class="paginate-list f-l overflow-hidden">
		                 	<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
							<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
							<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
		               	</ul>
		               
		               	<c:url var="nextUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
		   				</c:url>
		               	<button class="next" data-url="${nextUrl}"></button>
		               
		               	<c:url var="endUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
		   				</c:url>
		               	<button class="end" data-url="${endUrl}"></button>
					</div>
			  </div>
			</div>
            </article>
          </section>
	</c:when>
	<c:otherwise>
		<section class="section-gap special-common">
			<div class="area">
				<img src="/template/lms/imgs/page/special/img_top_banner.jpg" alt="K-MOOC는 온라인을 통해서 누구나, 어디서나 원하는 강좌를 무료로 들을 수 있는 온라인 공개강좌 서비스로 2015년에 시작된 한국형 무크입니다." />
				<div class="board-video-search-wrap mt-45">
			        <p class="title">한국외국어대학교 K-MOOC 온라인 강좌</p>
			        <div class="search-input-wrap">
			        	<!-- 게시판 검색영역 -->
			        	<form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
							<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
							<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
							<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
							<input name="searchCnd" type="hidden" value="0" />
							<input name="searchWrd" type="hidden" value="<c:out value="${searchVO.searchWrd}"/>"/>
						</form>
			        	<input id="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" class="search-input" placeholder="강좌 검색"/>
						<div class="goods-search-btn">
				          	<img src="${CML}/imgs/common/icon_notice_search.svg" alt="검색">
				        </div>
			        </div>
			      </div>
			      <div class="flex-row board-video-thumb-wrap">
					<c:forEach var="result" items="${resultList}" varStatus="status">
						<div class="flex-col-4">
							<a href="<c:out value="${result.tmp05}"/>" target="_blank">
								<c:set var="imgSrc">
									<c:choose>
			                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
			                    		<c:otherwise>
			                    			<c:url value='/cmm/fms/getImage.do'/>?thumbYn=Y&amp;siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>&amp;width=393&amp;height=240
			                    		</c:otherwise>
			                    	</c:choose>
								</c:set>
				                <div class="board-video-img" style="background-image:url(${imgSrc});">
				                  <span class="text-hide">동영상썸네일</span>
				                </div>
				                <p class="board-video-title ell"><c:out value="${result.nttSj}"/></p>
					            <ul class="card-items-wrap">
					              	<li class="card-items icon-campus"><c:out value="${result.tmp03}"/></li>
					              	<li class="card-items icon-date"><c:out value="${result.tmp01}"/> ~ <c:out value="${result.tmp02}"/></li>
					            </ul>
				            </a>
						</div>
					</c:forEach>
			      </div>
			      
			      <div class="pagination center-align">
						<div class="pagination-inner-wrap overflow-hidden inline-block">
							<c:url var="startUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="1" />
			   				</c:url>
			               	<button class="start" data-url="${startUrl}"></button>
			               
			               	<c:url var="prevUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
								<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
							</c:url>
			               	<button class="prev" data-url="${prevUrl}"></button>
			               
			               	<ul class="paginate-list f-l overflow-hidden">
			                 	<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
								<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
								<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
			               	</ul>
			               
			               	<c:url var="nextUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
			   				</c:url>
			               	<button class="next" data-url="${nextUrl}"></button>
			               
			               	<c:url var="endUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
			   				</c:url>
			               	<button class="end" data-url="${endUrl}"></button>
						</div>
				  </div>
				</div>
			</section>
	</c:otherwise>
</c:choose>


<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>