<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="${_WEB_FULL_PATH}/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
	<c:param name="bbsId" value="${board.bbsId}" />
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
	<c:if test="${not empty searchVO.viewType}"><c:param name="viewType" value="${searchVO.viewType}"/></c:if>	
	<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
		<c:if test="${not empty searchCate}">
			<c:param name="searchCateList" value="${searchCate}" />
		</c:if>
	</c:forEach>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
	<c:param name="tableCssAt" value="Y"/>
	<c:param name="contTitleAt" value="N"/>
</c:import>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script src="${_C_JS}/board.js" ></script>
<script>
	$(document).ready(function(){
		$('#btnBbsDelete').click(function() {
			var href = $(this).data("href");
			$(".btnModalConfirm").click(function(){
				location.href = href;
			});
			return false;
		});
	});
</script>

<div class="page-content-wrap">
    <section class="page-content-body">
      <div class="area">
      
<!-- 게시물-->
<section class="board-view-wrap">
  <!-- 제목 -->
  <article class="board-title-wrap">
    <div class="main-common-title3">
      <h2 class="title"><c:out value="${board.nttSj}" /></h2>
    </div>
    <div class="board-info-wrap">
      <dl class="item">
        <dt class="title">등록일</dt>
        <dd class="desc"><fmt:formatDate value="${board.frstRegisterPnttm}"  pattern="yyyy.MM.dd"/></dd>
      </dl>
    </div>
  </article>
  <!-- 내용 -->
  <article class="board-content-wrap">
    <div class="board-editor-content">
      <!-- 에디터영역 -->
      <div id="froala_editor" class="froala-read-only">
        <c:out value="${board.nttCn}" escapeXml="false" />
      </div>
    </div>
  </article>
</section>
<hr class="line-hr mb-20">

<!-- 첨부파일 -->
<c:if test="${not empty board.atchFileId}">
	<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
		<c:param name="param_atchFileId" value="${board.atchFileId}" />									
		<c:param name="imagePath" value="${_IMG }"/>
	</c:import>
</c:if>

			<!-- 하단버튼 -->
			<div class="page-btn-wrap mt-10 mb-100">
			  <div class="left-area">
			    <a href="<c:url value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>" class="btn-sm btn-outline-gray font-basic">목록</a>
			  </div>
			  
			  <c:if test="${not empty USER_INFO.id}">
				  <div class="right-area">
				  	<c:if test="${brdMstrVO.replyPosblAt eq 'Y' and SE_CODE >= brdMstrVO.answerAuthor}">
						<c:url var="addReplyBoardArticleUrl" value="${_PREFIX}/addReplyBoardArticle.do${_BASE_PARAM}">
				     			<c:param name="nttNo" value="${board.nttNo}" />
				  			<c:param name="registAction" value="reply" />
						</c:url>
						<a href="<c:out value="${addReplyBoardArticleUrl}"/>" class="btn-sm btn-outline-gray font-basic" title="<spring:message code="button.reply"/>(<c:out value="${brdMstrVO.bbsNm }"/>)"><spring:message code="button.reply"/></a>
				   	</c:if>
					<c:if test="${board.frstRegisterId eq USER_INFO.id or SE_CODE >= 10}">
						<%-- 
						<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardArticle.do${_BASE_PARAM}">
				      		<c:param name="nttNo" value="${board.nttNo}" />
					  		<c:param name="registAction" value="updt" />
						</c:url>
						<span class="bbtn"><a href="<c:out value="${forUpdateBoardArticleUrl}"/>" title="<spring:message code="button.update"/>(<c:out value="${brdMstrVO.bbsNm }"/>)"><spring:message code="button.update"/></a></span>
						 --%>
				      	<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardArticle.do${_BASE_PARAM}">
				      		<c:param name="nttNo" value="${board.nttNo}" />
						</c:url>
						<button id="btnBbsDelete" onclick="location.href='<c:out value="${deleteBoardArticleUrl}"/>'" class="btn-sm btn-outline-gray font-basic btnModalOpen" data-modal-type="confirm" data-modal-header="알림" data-modal-text="삭제된 글은 복구가 불가능합니다. <br>글을 삭제하시겠습니까?" data-modal-rightbtn="확인">삭제</button>
				   	</c:if>
				    
				  </div>
			  </c:if>
			</div>
		</div>
	</section>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>