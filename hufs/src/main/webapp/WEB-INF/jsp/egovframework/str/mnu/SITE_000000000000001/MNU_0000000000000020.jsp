<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section class="section-gap special-common">
<div class="area">
<div class="page-common-title">
<h2 class="title center-align">언어별 주요사업</h2>
</div>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">교과목 및 전공교재 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">사회적 수요에 부응하며 특수외국어 전문인력 양성을 위한 특화된 교과목 및 관련 교재를 개발하여 학부 교육 내실화에 기여하고 학생들의 언어역량을 강화하고자 합니다.</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">학문후속세대를 위한 전문 심화 언어교과목 개발</h5>
<h5 class="title icon-num">통번역전문가 양성을 위한 통번역 관련 교과목 신규 개설 및 확대</h5>
<h5 class="title icon-num">기업실무인재 양성을 위한 비즈니스 분야에 특화된 교과목 개설</h5>
<h5 class="title icon-num">인접어 관련 신규 교과목 개설 지원</h5>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">교수법 개발</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">효과적인 외국어교육 강의 전략과 강의의 질적 개선을 위한 다양한 교수법 개발 연구를 지원함으로써 교원의 전문 교육역량을 강화하고자 합니다.</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">특수외국어 수업 개선을 위한 교수법 및 교안 개발</h5>
<h5 class="title icon-num">기존 강좌 또는 신규개발 강좌에 적용</h5>
<h5 class="title icon-num">우수강의 교수법 및 노하우 공유 및 확산</h5>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">전공역량강화사업</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">특수외국어 전공자를 대상으로 다양한 전공연계프로그램을 제공함으로써 우수한 외국어 능력 및 해당 언어권에 대한 사회/문화적 감각과 소양을 갖춘 전문 인재로 키워 나가고자 합니다.&nbsp;</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">RC 기반 특수외국어방중집중이수과정</h5>
<p class="list">하계 및 동계 방학 기간을 이용, 우리 대학 글로벌 캠퍼스 기숙사 공간 활용한 RC 기반의 국내 어학연수과정 형태로 진행</p>
<p class="list">학과 수요에 따라 기초어학과정, 통번역실습과정, 비즈니스통번역과정 등 다양한 교과목 개설</p>
<p class="list">전공 학점 부여 및 필요시 이수 증명서 발급</p>
<h5 class="title icon-num">탄뎀(Tandem) 프로그램</h5>
<p class="list">외국인유학생-전공생, 전공생-비전공생 등 다양한 유형의 Peer-Tutoring 학습 모델 적용</p>
<p class="list">특수외국어 어학 능력 증진 및 상호문화 교류를 위한 다양한 활동 지원</p>
<p class="list">팀 단위 활동을 기반으로 하며 학기 단위 과제수행결과보고 제출</p>
<h5 class="title icon-num">전공교과 연계실습</h5>
<p class="list">정규 교과목과 연계한 추가 언어학습활동 및 실습 과정 운영</p>
<p class="list">정규 교과목 지도교수의 관리 하에 체험 활동을 통한 학습공동체 활동 형태로 진행</p>
<h5 class="title icon-num">기타 비교과 프로그램</h5>
<p class="list">이중전공생, 편입생, 군위탁생 및 학습부진 전공자를 대상으로 하는 방과 후 보충수업 진행</p>
<p class="list">특수외국어능력 제고를 위해 추진하는 전공학과별 다양한 사업 지원(말하기 대회, 워크숍, 원어연극 등 동아리 지원 외)</p>
</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">특수외국어 오픈 강좌</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목표</h4>
<div class="contnet-desc">온 국민 대상 특수외국어 교육 기회 제공 및 특수외국어 교육의 저변 확대를 위해 기관/기업위탁과정을 포함한 다양한 특수외국어 교육과정을 제공합니다.&nbsp;</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">계획 및 추진방향</h4>
<div class="contnet-desc">
<h5 class="title icon-num">기관/기업 위탁 특수외국어 교육과정</h5>
<p class="list">특수외국어 교육 수요가 있는 기관 및 기업 임직원 대상 특수외국어교육과정 개설 운영<br />기관의 교육수요에 맞춰 주중, 주말, 단기/중기/장기 등 다양한 형태의 수업 개설</p>
<h5 class="title icon-num">시민 강좌</h5>
<p class="list">우리 원 또는 특수외국어관련 전공학과 주도로 자체적으로 진행</p>
<p class="list">특수외국어 기초 어학 및 문화 등 다양한 주제의 교육과정 개설 운영</p>
</div>
</div>
</div>
</article>
</div>
</section>