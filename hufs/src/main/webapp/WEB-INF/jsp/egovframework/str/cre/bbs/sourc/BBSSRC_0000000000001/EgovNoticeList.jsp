<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
	<c:if test="${not empty searchVO.viewType}"><c:param name="viewType" value="${searchVO.viewType}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
			<c:param name="tableCssAt" value="Y"/>
		</c:import>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript">

<c:if test="${!empty brdMstrVO.ctgrymasterId}">		
	var boardCateLevel = ${boardCateLevel};
	var boardCateList = new Array(${fn:length(boardCateList)});
	<c:forEach var="cate" items="${boardCateList}" varStatus="status">
		boardCateList[${status.index}] = new ctgryObj('<c:out value='${cate.upperCtgryId}'/>', '<c:out value='${cate.ctgryId}'/>', '<c:out value='${cate.ctgryNm}'/>', <c:out value='${cate.ctgryLevel}'/>);
	</c:forEach>		
</c:if>

function fn_egov_addNotice(url) {
   	<c:choose>
		<c:when test="${not empty USER_INFO.id}">
			location.href = url;
		</c:when>
		<c:otherwise>
			if (confirm('로그인 하시겠습니까?')) {
				location.href = "<%=egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper.getRedirectLoginUrl()%>";	
			}
		</c:otherwise>
	</c:choose>
   }

$(document).ready(function(){
	$('#btnBbsWrite').click(function() {fn_egov_addNotice(this.href);return false;});
	fnCtgryInit('<c:out value="${searchVO.searchCateList}"/>');
	
	//페이징 버튼
	$(".start, .prev, .next, .end").click(function(){
 		var url = $(this).data("url");
 		
 		location.href = url;
 	});
	
	$(".btn-gallery, .btn-list").click(function(){
		var type = $(this).data("type");
		
		$("#viewType").val(type);
		$("#frm").submit();
	});
	
	//글등록
	$('.btnModalOpen').click(function() {
		var href = $(this).data("href");
		$(".btnModalConfirm").click(function(){
			location.href = href;
		});
		return false;
	});
});
</script>		


<section class="page-content-body">
    <article class="content-wrap">
      <!-- 게시판 검색영역 -->
      <form id="frm" name="frm" method="post" action="<c:url value='${_PREFIX}/selectBoardList.do'/>">
			<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
			<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
			<input name="searchCate" type="hidden" value="<c:out value='${searchVO.searchCate}'/>" />
			<input name="tmplatImportAt" type="hidden" value="<c:out value='${searchVO.tmplatImportAt}'/>" />
			<input name="searchCnd" type="hidden" value="0" />
			<input id="viewType" name="viewType" type="hidden" value="<c:out value='${searchVO.viewType}'/>" />
            <div class="box-wrap mb-40">
				<h3 class="title-subhead"><c:out value="${brdMstrVO.bbsNm}"/> 검색</h3>
				<div class="flex-row-ten">
					<c:choose>
						<c:when test="${!empty brdMstrVO.ctgrymasterId }">
							<c:set var="searchClass" value="flex-ten-col-7"/>
						</c:when>
						<c:otherwise>
							<c:set var="searchClass" value="flex-ten-col-10"/>
						</c:otherwise>
					</c:choose>
					
					<c:if test="${!empty brdMstrVO.ctgrymasterId}">
						<c:choose>
							<c:when test="${searchVO.bbsId eq 'BBSMSTR_000000000023' and SE_CODE <= 6}">
								<c:set var="searchClass" value="flex-ten-col-10"/>
								<input name="searchCateList" type="hidden" value="CTG_0000000000000272"/>
							</c:when>
							<c:otherwise>
								<div class="flex-ten-col-3">
					                <div class="ell">
					                	<select name="searchCateList" class="select2" data-select="style3">
					                		<option value="">전체</option>
											<%-- <c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status"> --%>
											<c:forEach var="ctgryLevel" begin="1" end="1" step="1" varStatus="status">
												<c:forEach var="cate" items="${boardCateList}">
													<c:if test="${cate.ctgryLevel eq 1}">
														<option value="${cate.ctgryId}" <c:if test="${cate.ctgryId eq searchVO.searchCateList[0]}">selected="selected"</c:if>><c:out value='${cate.ctgryNm}'/></option>
													</c:if>
												</c:forEach>
												<%-- 
												<c:choose>
													<c:when test="${status.first}">
														<label for="ctgry${ctgryLevel}" class="hdn"><spring:message code="cop.category.view" /></label>
														<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})">
															<option value=""><spring:message code="cop.select" /></option>
															<c:forEach var="cate" items="${boardCateList}">
																<c:if test="${cate.ctgryLevel eq 1 }">
																	<option value="${cate.ctgryId}"><c:out value='${cate.ctgryNm}'/></option>
																</c:if>
															</c:forEach>
														</select>
													</c:when>
													<c:otherwise>
														<label for="ctgry${ctgryLevel}" class="hdn"><spring:message code="cop.category.view" />${ctgryLevel}</label>
														<select name="searchCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})" class="search_sel"><option value=""><spring:message code="cop.select" /></option></select>
													</c:otherwise>
												</c:choose>
												 --%>
											</c:forEach>
										</select>
									</div>
					            </div>
							</c:otherwise>
						</c:choose>
					</c:if>
					
					<div class="${searchClass}">
		                <div class="ell">
		                  	<input name="searchWrd" value="<c:out value="${searchVO.searchWrd}"/>" type="text" class="inp_s" id="inp_text" placeholder="제목을 입력해보세요"/>
		                </div>
		            </div>
				</div>
				<button class="btn-sm font-400 btn-point mt-20" type="submit">검색</button>
			</div>
		</form>
    </article>
    
    <article class="content-wrap">
    	<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA02'}">
    		<div class="content-header">
	           <div class="board-type-wrap">
	             <!-- 게시판 타입-->
	             <a href="#" class="btn-gallery <c:if test="${searchVO.viewType eq 'photo'}">on</c:if>" title="갤러리형" data-type="photo"></a><i class="division-line"></i>
	             <a href="#" class="btn-list <c:if test="${searchVO.viewType ne 'photo'}">on</c:if>" title="리스트형" data-type="list"></a>
	           </div>
	         </div>
    	</c:if>
    	<!-- 게시판 -->
    	<div class="content-body">
			<c:choose>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA02' and searchVO.viewType eq 'photo'}">
					<!-- 갤러리 -->
		            <div class="flex-row board-gallery-thumb-wrap">
					
			         <c:forEach var="result" items="${resultList}" varStatus="status">
		            	<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
					    </c:url>
					    <c:set var="isViewEnable" value=""/>
					    <c:choose>
							<c:when test="${SE_CODE eq '10'}"><c:set var="isViewEnable" value="Y"/></c:when>
							<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}"><c:set var="isViewEnable" value="N"/></c:when>
							<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}"><c:set var="isViewEnable" value="N"/></c:when>
							<c:otherwise><c:set var="isViewEnable" value="Y"/></c:otherwise>
						</c:choose>
						<div class="flex-col-4">
							<c:if test="${isViewEnable eq 'Y'}"><a href="<c:out value="${viewUrl}"/>"></c:if>
								<c:set var="imgSrc">
									<c:choose>
			                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
			                    		<c:otherwise>
			                    			<c:url value='/cmm/fms/getImage.do'>
			                    				<c:param name="thumbYn" value="Y" />
			                    				<c:param name="siteId" value="SITE_000000000000001" />
			                    				<c:param name="appendPath" value="${result.bbsId}" />
			                    				<c:param name="atchFileNm" value="${result.atchFileNm}" />
			                    			</c:url>
			                    			<%-- <img src='<c:url value='/cmm/fms/getImage.do'/>?thumbYn=Y&amp;siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>' alt="<c:out value="${result.nttSj}"/>"/> --%>
			                    		</c:otherwise>
			                    	</c:choose>
								</c:set>
				                <div class="board-gallery-img" style="background-image:url(${imgSrc});">
				                  <span class="text-hide">
				                  	<c:out value="${result.nttSj}"/> 이미지썸네일
				                  	
				                  </span>
				                </div>
				                <div class="board-gallery-contents">
				                  <p class="gallery-country">
				                  	<c:choose>
				                  		<c:when test="${not empty result.ctgryNm}"><c:out value="${result.ctgryNm}"/></c:when>
				                  		<c:otherwise>전체</c:otherwise>
				                  	</c:choose>
				                  </p>
				                  <p class="board-gallery-title ell"><c:out value="${result.nttSj}" /></p>
				                </div>
				            <c:if test="${isViewEnable eq 'Y'}"></a></c:if>
						</div>
		             </c:forEach>
			        
					</div>
		         </div>
			</c:when>				
			<c:otherwise>
				<%-- 일반 게시판 목록 --%>
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    	<col class="" style="width:7%">
							<c:if test="${!empty brdMstrVO.ctgrymasterId}">
								<col class="" style="width:12%">
							</c:if>
	                     	<c:choose>
	                      		<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000642'}">
			                    	<col class="" style="width:79%">
	                      		</c:when>
	                      		<c:otherwise>
			                    	<col class="">
	                      		</c:otherwise>
	                     	</c:choose>
							<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><col class="" style="width:12%"></c:if>
							<c:if test="${brdMstrVO.bbsId ne 'BBSMSTR_000000000024'}">
						 		<col class="" style="width:14%">
							</c:if>
							<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><col class="" style="width:12%"></c:if>
                  </colgroup>
                  <thead>
					<tr class="bg-light-gray font-700">
                      <th scope="col" class="">No</th>
                      <c:if test="${!empty brdMstrVO.ctgrymasterId}">
                      	<th scope="col" class="">구분</th>
                      </c:if>
                      <th scope="col" class="">제목</th>
                      <c:choose>
                      	<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000394' || brdMstrVO.bbsId eq 'BBSMSTR_000000000397'}">
	                      <th scope="col" class="">등록자</th>
                      	</c:when>
                      	<c:otherwise>
	                      <c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><th scope="col" class="">등록자</th></c:if>
	                      <c:if test="${brdMstrVO.bbsId ne 'BBSMSTR_000000000024'}">
	                      	<th scope="col" class="">등록일</th>
	                      </c:if>
	                      <c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}"><th scope="col" class="">상태</th></c:if>
                      	</c:otherwise>
                      </c:choose>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="result" items="${resultList}" varStatus="status">
					<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
					  	<c:param name="nttNo" value="${result.nttNo}" />
					  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
				    </c:url>
					<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
						<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
						<c:if test="${!empty brdMstrVO.ctgrymasterId}">
							<td>
								<c:if test="${(not empty brdMstrVO.ctgrymasterId) and (not empty result.ctgryNm)}">
									<c:out value="${result.ctgryNm}" />
								</c:if>
							</td>
						</c:if>
						<td class="title">
							<div class="inner-wrap">
								<c:if test="${result.othbcAt eq 'N'}"><i class='icon-lock mr-10'>비공개</i></c:if>
								<span class='text dotdotdot'>
									<c:choose>
										<c:when test="${SE_CODE eq '10'}"><a href="<c:out value="${viewUrl}"/>"><c:out value="${result.nttSj}" /></a></c:when>
										<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
											<c:out value="${result.nttSj}" />
										</c:when>
										<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
											<c:out value="${result.nttSj}" />
										</c:when>
										<c:otherwise><a href="<c:out value="${viewUrl}"/>"><c:out value="${result.nttSj}" /></a></c:otherwise>
									</c:choose>
								</span>
								<c:if test="${not empty result.atchFileId}"><i class='icon-clip ml-10'></i></c:if>
							</div>
							<%-- 
							<c:if test="${result.ordrCodeDp gt 0}">
							  <img src="${_C_IMG}/sub/board/blank_bg.gif" width="${result.ordrCodeDp * 19}" height="0" alt="${result.ordrCodeDp} Depth" /><img src="${_IMG}/ico_reply.gif" alt="<spring:message code="cop.replyNtt"/>" />
					        </c:if>
					         --%>
							<%-- 
							<c:if test="${result.othbcAt eq 'N'}"><img src="${_IMG}/ico_board_lock.gif" alt="<spring:message code="cop.privateNtt"/>" /></c:if>
							<c:if test="${brdMstrVO.commentUseAt eq 'Y'}">
								<c:choose>
									<c:when test="${IS_MOBILE }"><em class="boardrenum"><c:out value="${result.commentCount}" /></em></c:when>
									<c:when test="${result.commentCount eq 0}"><em class="boardrenumno">[<c:out value="${result.commentCount}" />]</em></c:when>
									<c:otherwise><em class="boardrenum">[<c:out value="${result.commentCount}" />]</em></c:otherwise>
								</c:choose>
							</c:if>
							 --%>
						</td>
						<c:choose>
							<c:when test="${brdMstrVO.bbsId eq 'BBSMSTR_000000000394' || brdMstrVO.bbsId eq 'BBSMSTR_000000000397'}">
								<td><c:out value="${result.ntcrNm}"/></td>
							</c:when>
							<c:otherwise>
								<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
									<td><c:out value="${result.ntcrNm}"/></td>
								</c:if>
								<c:if test="${brdMstrVO.bbsId ne 'BBSMSTR_000000000024'}">
									<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
								</c:if>
								<c:if test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
									<c:choose>
										<c:when test="${result.processSttusCode eq 'QA01'}">
											<td><c:out value="${result.processSttusNm}" /></td>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA02'}">
											<td class="font-blue"><c:out value="${result.processSttusNm}" /></td>
										</c:when>
										<c:when test="${result.processSttusCode eq 'QA03'}">
											<td class="font-blue"><c:out value="${result.processSttusNm}" /></td>
										</c:when>
									</c:choose>
					          	</c:if>	
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
				
				<c:if test="${fn:length(resultList) == 0}">
					<c:set var="colspan">
						<c:choose>
							<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">6</c:when>
							<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA03'}">3</c:when>
							<c:otherwise>4</c:otherwise>
						</c:choose>
					</c:set>
			    	<tr class="empty"><td colspan="${colspan}"><spring:message code="common.nodata.msg" /></td></tr>
			    </c:if>
			    
                </tbody>
                </table>
			</c:otherwise>
		</c:choose>
		
		<c:if test="${brdMstrVO.registAuthor eq '02' or SE_CODE >= brdMstrVO.registAuthor or SE_CODE >= 10}">
			<div class="page-btn-wrap mt-10">
	          <div class="right-area">
				<c:choose>
		            <c:when test="${empty USER_INFO.id}">
		            	<button onclick="location.href='/uss/umt/cmm/EgovStplatCnfirmMber.do'" class="btn-sm btn-outline-gray font-basic btnModalOpen" data-modal-type="confirm" data-modal-header="알림" data-modal-text="회원가입이 필요한 서비스입니다." data-modal-subtext="로그인 후 이용이 가능합니다. 회원가입 화면으로 이동하시겠습니까?" data-modal-rightbtn="확인">문의하기</button>
		            </c:when>
		            <c:otherwise>
		            	<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
							<c:param name="registAction" value="regist" />
						</c:url>
		            	<c:choose>
		            		<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11'}">
		            			<a href="${addBoardArticleUrl}" class="btn-sm btn-outline-gray font-basic">문의하기</a>
		            		</c:when>
		            		<c:otherwise>
		            			<a href="${addBoardArticleUrl}" class="btn-sm btn-outline-gray font-basic">글쓰기</a>
		            		</c:otherwise>
		            	</c:choose>
		            </c:otherwise>
	            </c:choose>
	          </div>
	        </div>
		</c:if>
		
		<div class="pagination center-align mt-60">
			<div class="pagination-inner-wrap overflow-hidden inline-block">
				<c:url var="startUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
  					<c:param name="pageIndex" value="1" />
   				</c:url>
               	<button class="start" data-url="${startUrl}"></button>
               
               	<c:url var="prevUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
					<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
				</c:url>
               	<button class="prev" data-url="${prevUrl}"></button>
               
               	<ul class="paginate-list f-l overflow-hidden">
                 	<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
					<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
					<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
               	</ul>
               
               	<c:url var="nextUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
   				</c:url>
               	<button class="next" data-url="${nextUrl}"></button>
               
               	<c:url var="endUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
   				</c:url>
               	<button class="end" data-url="${endUrl}"></button>
			</div>
		</div>
	</div>
	<!-- //게시판 -->
  </article>
</section>
  	<%-- 
	<c:if test="${brdMstrVO.registAuthor eq '02' or SE_CODE >= brdMstrVO.registAuthor or SE_CODE >= 10}">			
	<div class="btn_all">
		<div class="fR">
			<c:url var="addBoardArticleUrl" value="${_PREFIX}/addBoardArticle.do${_BASE_PARAM}">
				<c:param name="registAction" value="regist" />
			</c:url>
			<span  class="bbtn_confirm2"><a href="<c:out value="${addBoardArticleUrl}"/>" id="btnBbsWrite" title="<spring:message code="button.write"/>(<c:out value="${brdMstrVO.bbsNm }"/>)"><spring:message code="button.write"/></a></span>
		</div>				
	</div>
	</c:if>	
	 --%>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>