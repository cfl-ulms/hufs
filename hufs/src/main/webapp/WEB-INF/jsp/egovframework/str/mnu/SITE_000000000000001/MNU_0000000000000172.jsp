<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section class="page-content-body">
            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">발급 부서</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <thead>
                    <tr class='font-700 bg-gray'>
                      <th scope='col'>특수외국어교육진흥원</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="">
                      <td class='line-height2'>Tel. 02-2173-2843~6<br>Fax. 02-2173-2847<br>Email. cfle@hufs.ac.kr</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>

            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">증명서 종류</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col style='width:50%'>
                    <col style='width:50%'>
                    <col style='width:50%'>
                  </colgroup>
                  <thead>
                    <tr class='font-700 bg-gray'>
                      <th scope='col'>증명 종류</th>
                      <th scope='col'>발급 대상</th>
                      <th scope='col'>비고</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="">
                      <td>성적 증명서</td>
                      <td>해당 과정 성적을 취득한 자</td>
                      <td>국문 / 영문</td>
                    </tr>
                    <tr class="">
                      <td>수료 증명서</td>
                      <td>해당 과정을 수료한 자</td>
                      <td>국문 / 영문</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>

            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교내 방문 신청</div>
                  <div class="desc-sm">1. 신청방법 국문/영문 증명서 신청서 작성 후 증명발급 담당자에게 제출</div>
                  <div class="desc-sm">2. 담당자가 증명서 신청서 접수 후 증명서 발급</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:80%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>발급장소</td>
                      <td>서울 동대문구 이문로 107 한국외국어대학교 교수학습개발원 501-1호 (02450)</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>이용시간</td>
                      <td>10:00 ~ 18:00</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>유의사항</td>
                      <td>본인이 신청하는 경우 증명서 신청서와 함께 신분증 제출 ※ 신분증 : 주민등록증, 운전면허증, 공무원 증, 여권 등</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>

            <article class="content-wrap">
              <div class="content-body">
                <div class="box-area bg-gray-light">
                  <p class="desc">
               		증명서 신청서와 위임장 양식 다운로드를 받으실 수 있습니다.<br/>
                  	대리인이 신청하는 대상자의 신분증(사본도 가능), 위임장, 위임인의 신분증 제출이 필요합니다. 
				  </p>
                  <div class="btn-wrap mt-25">
                    <a href="#" class="btn-xl btn-outline">증명서 양식 다운로드</a>
                    <a href="#" class="btn-xl btn-outline">위임장 양식 다운로드</a>
                  </div>
                </div>
              </div>
            </article>

            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">발급 문의 전화</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:80%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>문의 전화</td>
                      <td>02-2173-2843 / 02-2173-2846</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>팩스</td>
                      <td>02-2173-2847</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
          </section>
          
          </div>
      </div>
    </div>
  </div>