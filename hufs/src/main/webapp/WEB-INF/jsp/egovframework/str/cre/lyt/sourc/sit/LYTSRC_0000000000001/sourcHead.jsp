<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<%@ page import="java.util.Date" %>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="PUBLISH_APPEND_FREFIX"><%=Globals.getPublishAppendPrefix(request)%></c:set>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>

<c:set var="CURR_URL" value="<%=helper.getOriginatingRequestUri(request) %>"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>
  	<c:choose>
  		<c:when test="${empty param.title}">
  			<c:out value="${siteInfo.siteNm}"/>
  		</c:when>
  		<c:otherwise>
  			<c:out value="${param.title}"/>
  		</c:otherwise>
  	</c:choose>
  </title>

  <meta name="title" content="<c:out value="${siteInfo.siteNm}"/>">
  <meta name="description" content="CFL 특수외국어교육진흥사업 한국외국어대학교, 특수외국어 교육과정, 한국외국어대학교 특수외국어교육, 특수외국어 출간교재, CFLPT 외국어 시험">
  <meta name="keywords" content="CFL 특수외국어교육진흥사업 한국외국어대학교, 특수외국어 교육과정, 한국외국어대학교 특수외국어교육, 특수외국어 출간교재, CFLPT 외국어 시험">

  <meta property="og:type" content="website">
  <meta property="og:title" content="CFL 특수외국어교육진흥사업 한국외국어대학교" />
  <meta property="og:description" content="CFL 특수외국어교육진흥사업 한국외국어대학교, 특수외국어 교육과정, 한국외국어대학교 특수외국어교육, 특수외국어 출간교재, CFLPT 외국어 시험">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">
  <meta property="og:url" content="http://cfl.ac.kr/">
  <meta name="Robots" content="INDEX, FOLLOW" />
	
  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

<link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
<c:choose>
	<c:when test="${USER_INFO.userSeCode eq '08'}">
  		<link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=3">
		<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/modal_staff.css?v=3">
  		
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">

	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">
		
		<link rel="stylesheet" href="${CML}/css/common/common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board.css?v=1">
  		<link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
  		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
	</c:otherwise>
</c:choose>

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=2"></script>
  <script src="${CM}/js/common.js"></script>

  <!--=================================================
        페이지별 스타일시트, 스크립트
  ==================================================-->
  <link rel="stylesheet" href="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/style.css?v=1">
  <c:choose>
	<c:when test="${param.isMain eq 'Y'}">
		<link rel="stylesheet" href="${CML}/css/main/main_common.css?v=3">
    	<link rel="stylesheet" href="${CML}/css/main/main_logout.css?v=5">
    	<script src="${CML}/js/main/main.js?v=3"></script>
	</c:when>
	<%-- 특수외국어교육진흥사업 --%>
	<c:when test="${currMpm.upperMenuId eq 'MNU_0000000000000004'}">
	  	<link rel="stylesheet" href="${CML}/css/special/special.css?v=2">
	  	<c:choose>
	  		<%-- K-MOOC --%>
	  		<c:when test="${currMpm.menuId eq 'MNU_0000000000000022'}">
	  			<link rel="stylesheet" href="${CML}/css/special/special.4.1.css?v=2">
	  		</c:when>
	  		<c:otherwise>
	  			<link rel="stylesheet" href="${CML}/css/special/special.1.1.css?v=2">
	  			<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
	  		</c:otherwise>
	  	</c:choose>
	</c:when>
	<%-- 외국어시험 --%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000010'}">
		<link rel="stylesheet" href="${CML}/css/foreignlang/foreignlang.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
	</c:when>
	<%-- 교재/사전 --%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000008'}">
		<link rel="stylesheet" href="${CML}/css/textbook/textbook.css?v=3">
		<c:choose>
			<c:when test="${param.listAt eq 'Y'}">
				<link rel="stylesheet" href="${CML}/css/textbook/textbook.1.1.css?v=3">
				<script src="${CML}/js/textbook/textbook.js?v=1"></script>
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${CML}/css/textbook/textbook.1.2.css?v=3">
			</c:otherwise>
		</c:choose>
	    <link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
	</c:when>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000054' or currMpm.menuId eq 'MNU_0000000000000055' or currMpm.menuId eq 'MNU_0000000000000057' }">
		<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
		<c:if test="${currMpm.menuId eq 'MNU_0000000000000055'}">
			<link rel="stylesheet" href="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.min.css?v=2">
			<script src="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js?v=1"></script>
		</c:if>
	</c:when>
	<%-- 교재/사전 --%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000066' and empty USER_INFO.id}">
		<link rel="stylesheet" href="${CML}/css/curriculum/curriculum.css?v=2">
  		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
  		<script src="${CML}/js/curriculum/curriculum.js?v=1"></script>
	</c:when>
	<%-- 교원/K-MOOC 등록 --%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000104'}">
		<link rel="stylesheet" href="${CML}/css/materials/staff_materials.css?v=2">
	</c:when>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000062'}">
		<link rel="stylesheet" href="${CML }/css/common/table_staff.css?v=2">
  		<link rel="stylesheet" href="${CML }/css/process/process.css?v=2">
  		<link rel="stylesheet" href="${CML }/css/common/modal.css?v=2">
		<script type="text/javascript" src="${CML }/lib/sly-master/sly.min.js"></script>
		<script type="text/javascript" src="${CML }/lib/echarts/echarts.min.js?v=1"></script>
		<script type="text/javascript" src="${CML }/js/process/process.5.1.13.3.js?v=1"></script>
	</c:when>
	<%-- 교원 수강신청 > 수강대상자 확정--%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000070'}">
        <link rel="stylesheet" href="${CML }/css/common/table_staff.css?v=2">
        <link rel="stylesheet" href="${CML }/css/common/modal.css?v=2">
		<link rel="stylesheet" href="${CML }/css/curriculum/staff_curriculum.3.1.9.css?v=2">
        <script type="text/javascript" src="${CML }/lib/sly-master/sly.min.js"></script>
        <script src="${CML }/js/curriculum/staff_curriculum.3.1.9.js?v=1"></script>
    </c:when>
    <%-- 교원 수강신청 > 수강료 납부 확인/환불 --%>
    <c:when test="${currMpm.menuId eq 'MNU_0000000000000072'}">
        <link rel="stylesheet" href="${CML }/css/common/table_staff.css?v=2">
    </c:when>
	<%-- 교육과정개설신청 --%>
	<c:when test="${curriculumAt eq 'Y'}">
		<link rel="stylesheet" href="${CML }/css/common/table_staff.css?v=2">
		<link rel="stylesheet" href="${CML }/css/common/modal.css?v=2">
		<script src="${CML }/js/process/process.js?v=1"></script>
	</c:when>
	<%-- 마이페이지 --%>
	<c:when test="${param.mypage eq 'Y'}">
		<link rel="stylesheet" href="${CML}/css/my/my.css?v=2">
	</c:when>
	<%-- 수업계획 --%>
	<c:when test="${currMpm.menuId eq 'MNU_0000000000000086' or currMpm.menuId eq 'MNU_0000000000000094' or currMpm.menuId eq 'MNU_0000000000000163'}">
		<c:if test="${USER_INFO.userSeCode ne '08'}">
   			<link rel="stylesheet" href="${CML }/css/class/class.css?v=2">
		</c:if>
		<script type="text/javascript" src="${CML }/lib/sly-master/sly.min.js"></script>
		<script type="text/javascript" src="${CML }/lib/echarts/echarts.min.js?v=1"></script>
	</c:when>
	<%-- 수업 > 학생 > 과제 --%>
    <c:when test="${currMpm.menuId eq 'MNU_0000000000000084' or currMpm.menuId eq 'MNU_0000000000000068'}">
        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06'}">
            <link rel="stylesheet" href="${CML }/css/class/class.css?v=2">
        </c:if>
    </c:when>
    <%-- 수업 > 학생 > 과제 --%>
    <c:when test="${currMpm.menuId eq 'MNU_0000000000000084' or currMpm.menuId eq 'MNU_0000000000000068'}">
        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06'}">
            <link rel="stylesheet" href="${CML }/css/common/table.css?v=2">
            <link rel="stylesheet" href="${CML }/css/class/class.css?v=2">
        </c:if>
    </c:when>
	<c:otherwise>

	</c:otherwise>
  </c:choose>

  <script>
  	<c:if test="${not empty message}">
		alert("${message}");
	</c:if>

	$(document).ready(function(){
		<c:if test="${not empty USER_INFO.id and USER_INFO.id ne 'sj1004class@hanmail.net' and USER_INFO.id ne 'stu001@daum.net'}">
			setTimer('59', '59');

			$('.login-time-text').on('click', function(){
				clearTimer();
				setTimer('59', '59');
			});
		</c:if>

		//국가 선택 검색
		$(document).on("click", ".liLang", function() {
			var langId = $(this).data("id");
			
			$(".liLang").removeClass("on");
			$(this).addClass("on");

	        $.ajax({
	            url:'/lms/crm/curriculumCardListAjax.do',
	            type:'post',
	            dataType:'html',
	            data:{"searchCrclLang" : langId},
	            beforeSend : function(){
	            	$('#curCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
	            }, success:function(data){
	                $('#curCard').html(data);
	            }
	        });
		});

		// 오늘의 수업 (이전수업 클릭)
		$(document).on("click", ".btn-main-prev", function(event) {
			var date = $('#prevDate').text();

	        $.ajax({
	            url:'/lms/crm/todayCardListAjax.do',
	            type:'post',
	            dataType:'html',
	            data:{"startDate" : date},
	            beforeSend : function(){
	            	$('#todayCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
	            }, success:function(data){
	            	$('#todayCard').html(data);
	            }
	        });
		});

		// 오늘의 수업 (다음수업 클릭)
		$(document).on("click", ".btn-main-next", function() {
			var date = $('#afterDate').text();

	        $.ajax({
	        	url:'/lms/crm/todayCardListAjax.do',
	            type:'post',
	            dataType:'html',
	            data:{"startDate" : date},
	            beforeSend : function(){
	            	$('#todayCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
	            }, success:function(data){
	            	$('#todayCard').html(data);
	            }
	        });
		});
	});

	//타이머
	var timer;
	function setTimer(minute, second){
		 // 초기화
		 $(".countTimeMinute").html(minute);
		 $(".countTimeSecond").html(second);

		 timer = setInterval(function () {
		  // 설정
		  $(".countTimeMinute").html(minute);
		  $(".countTimeSecond").html(second);

		  if(minute == 0){
			  //console.log(11);
		  }
		  if(second == 0){
			  //console.log(22);
		  }

		  if(minute < 0){
			  clearInterval(timer);
			  location.href = "<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>";
		  }else{
		   second--;
		   if(second<10){
		    $(".countTimeSecond").html('0' + second);
		   }

		   // 분처리
		   if(second == 0){
		    minute--;
		    second = 59;
		   }
		   if(minute<10){
		    $(".countTimeMinute").html('0' + minute);
		   }

		   	if(minute == 4 && second == 59){
		   		if(confirm("시간연장하시겠습니까?")){
		   			clearTimer();
					setTimer('59', '59');
		   		}
		   	}
		  }
		 }, 1000); /* millisecond 단위의 인터벌 */
	}

	function clearTimer(){
		clearInterval(timer);
	}

	function fnCalDate(year, month, day){
		var tmpDt;

		if(month < 10){
			month = "0" + month;
		}
		if(day < 10){
			day = "0" + day;
		}
		tmpDt = year + "-" + month + "-" + day;

		return tmpDt;
	}
	//타이머 끝
  </script>
</head>
<body>
<c:if test="${param.contentAt ne 'Y'}">
<style>
/* 상단 프로필 이미지 */
.gnb .top-header .user-info-wrap .list.profile .user-img:before {
  display: inline-block;
  content: '';
  position: absolute;
  top: -3px;
  left: 0;
  width: 30px;
  height: 30px;
  background-image: url(${MembersFileStoreWebPath}${USER_INFO.photoStreFileNm});
  background-repeat: no-repeat;
  background-position: 50%;
  background-size: cover;
  background-color: #b7b7b7;
  border-radius: 50%;
  vertical-align: middle;
}


.main-myclass .date-select-wrap .selected-date .student-title {
  color: #0038a9;
}
</style>

<header class="gnb">
    <div class="top-header area-full">
      <h1>
        <a href="/index.do" class="flex">
          <img src="${CML}/imgs/common/img_header_logo.png" alt="<c:out value="${siteInfo.siteNm}"/>">
        </a>
      </h1>
      <ul class="user-info-wrap">
      	<c:choose>
      		<c:when test="${not empty USER_INFO.id }">
      			<li class="list profile">
		          <a href="#">
		            <span class="user-img">
		              <b><c:out value="${USER_INFO.name}"/></b>(<c:out value="${USER_INFO.id}"/>)
		            </span>
		          </a>
		        </li>
		        <li class="list login-time">
		          <button type="button" class="cursor-pointer">
		            <span class="login-time-num"><span class="countTimeMinute"></span>:<span class="countTimeSecond"></span></span>
		            <span class="login-time-text">시간연장하기</span>
		          </button>
		        </li>
		        <li class="list">
		          <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000024&menuId=MNU_0000000000000131">FAQ</a>
		        </li>
		        <li class="list">
		          	<c:choose>
		        		<c:when test="${USER_INFO.userSeCode eq '08' }">
		        			<a href="/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000044&userId=${USER_INFO.id}&userSeCode=${USER_INFO.userSeCode}&type=mypage">MYPAGE</a>
		        		</c:when>
		        		<c:otherwise>
		          			<a href="/uss/umt/cmm/EgovUserUpdateView.do?menuId=MNU_0000000000000044">MYPAGE</a>
		        		</c:otherwise>
		        	</c:choose>
		        </li>
		        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06'}">
		        	<li class="list">
			          <a href="/lms/crm/selectWishCrsList.do?menuId=MNU_0000000000000165">WISHLIST</a>
			        </li>
		        </c:if>
		        <li class="list">
		          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
		        </li>
		        <li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>">LOGOUT</a>
		        </li>
      		</c:when>
      		<c:otherwise>
      			<li class="list">
		          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
		        </li>
      			<li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>">JOIN</a>
		        </li>
		        <li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>">LOGIN</a>
		        </li>
      		</c:otherwise>
      	</c:choose>
      </ul><!-- user-info-wrap -->
    </div><!-- top-header -->

    <!-- nav -->
    <nav class="nav-wrap">
      <div class="nav-wrap-inner area-full">
        <ul class="nav-list-wrap">
          <c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
			<c:param name="menuTarget" value="Main"/>
			<c:param name="menuType" value="Simple2Depth"/>
			<c:param name="seCode" value="${SE_CODE}"/>
		  </c:import>
        </ul>
      </div><!-- nav-wrap-inner -->
      <div class="nav-bg"></div>
    </nav><!-- nav -->
</header>
<c:choose>
<c:when test="${param.isMain eq 'Y'}">
<div class="main-section-wrap">

    <!-- 메인비주얼 -->
    <c:choose>
    	<c:when test="${USER_INFO.userSeCode eq '08' || USER_INFO.userSeCode eq '02' || USER_INFO.userSeCode eq '04' || USER_INFO.userSeCode eq '06'}">
		<section class="mt-40">

    	<!-- 검색창 -->
	      <form id="mainSearchForm" name="mainSearchForm" method="post" action="/lms/crm/selectCurriculumAllList.do">
	       		<input type="hidden" name="menuId" value="MNU_0000000000000066"/>

		        <div class="area">
		          <div class="notice-box-wrap mb-10">
		            <p class="notice-title">
		              <img src="${CML}/imgs/common/icon_education.svg" alt="교육과정 안내 아이콘"> 교육과정 안내
		            </p>
		            <select name="searchCrclLang" class="select2" data-select="style3">
		            	<option value="">언어 전체</option>
		                <c:forEach var="result" items="${languageList}" varStatus="status">
		                	<c:if test="${not empty result.upperCtgryId}">
		                        <option value="${result.ctgryId}">${result.ctgryNm}</option>
		        	    	</c:if>
		  				</c:forEach>
					</select>

		            <input type="text" name="searchCrclNm" class="notice-input" placeholder="찾으시는 과정을 입력해주세요">
		            <button class="notice-search-btn btn-point">검색</button>
		          </div>

		  <div class="notice-box-wrap">
	          <p class="notice-title">
	            <img src="${CML}/imgs/common/icon_notice.svg" alt="알림 아이콘">
	            공지사항
	          </p>
	          <ul class="notice-info-wrap ell noticeListSlick" data-slick=" true">
		          <c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
					<c:param name="tmplatCours" value="${_IMG}"/>
					<c:param name="linkMenuId" value="MNU_0000000000000024" />
					<c:param name="viewType" value="data" />
					<c:param name="tableId" value="BBSMSTR_000000000001" />
					<c:param name="listTag" value="li" />
					<c:param name="itemCount" value="5" />

					<c:param name="colname" value="nttSj" />
					<c:param name="length" value="20" />
					<c:param name="tag" value="a" />
					<c:param name="cssClass" value=""/>
				</c:import>
	        </ul>
	          <div class="notice-icons-wrap">
		          <button class="notice-icons prev" type="button" title="이전"></button>
		          <button class="notice-icons next" type="button" title="다음"></button>
		          <button class="notice-icons play" type="button" title="재생/정지"></button>
	          </div>
	          <a class="notice-more" href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000001&menuId=MNU_0000000000000024"><img src="${CML}/imgs/common/icon_notice_plus.jpg" alt="더보기"></a>
	        </div>
	      </div>
	      </form>
	      
		 <section class="area-full">
		    <!-- 오늘의 수업 -->
		    <div class="main-myclass section-gap">
		      <div class="area">
		        <div class="main-common-title">
		          <h2 class="title"><c:choose><c:when test="${USER_INFO.userSeCode eq '08'}">오늘의 수업</c:when><c:otherwise>나의 수업</c:otherwise></c:choose></h2>
		        </div>
		        <div id="todayCard" class="myclass-wrap">
		        	<jsp:useBean id="now" class="java.util.Date" />
		        	<fmt:formatDate value="${now}" pattern="HHmmss" var="currTime" />

		        	<c:set var="yesterday" value="<%=new Date(new Date().getTime() - 60*60*24*1000)%>"/>
		        	<fmt:formatDate value="${yesterday}" pattern="yyyy-MM-dd" var="yesterDate"/>

		        	<c:set var="tomorrow" value="<%=new Date(new Date().getTime() + 60*60*24*1000)%>"/>
		        	<fmt:formatDate value="${tomorrow}" pattern="yyyy-MM-dd" var="tomorrowDate"/>

		          <!-- 날짜선택 -->
		          <div class="date-select-wrap">
		            <button class="btn-prev btn-main-prev">
		              <!-- disabled스타일 정의되어있음 -->
		              <i class="icon-arrow"></i>
		              <p class="title left-align">이전수업<span class="date" id="prevDate">${yesterDate}</span></p>
		            </button>
		            <div class="selected-date">
		              <h3 class="title <c:if test="${USER_INFO.userSeCode ne '08'}">student-title</c:if>" id="current">${today}(${dayNm})</h3>
		              <p class="sub-title">
		              	<c:choose>
		              		<c:when test="${fn:length(myCurriculumList) > 0}">
		              			<c:choose>
		              				<c:when test="${USER_INFO.userSeCode eq '08'}">
		              					오늘 총  ${fn:length(myCurriculumList)}개의 수업이 있습니다.
		              				</c:when>
		              				<c:otherwise>
		              					현재 총 ${fn:length(myCurriculumList)} 개의 특수외국어 수업이 진행 중입니다.
		              				</c:otherwise>
		              			</c:choose>
		              		</c:when>
		              		<c:otherwise>
		              			오늘은 수업이 없습니다.
		              		</c:otherwise>
		              	</c:choose>
		              </p>
		            </div>
		            <button class="btn-next btn-main-next">
		              <p class="title right-align">다음수업<span class="date" id="afterDate">${tomorrowDate}</span></p>
		              <i class="icon-arrow"></i>
		            </button>
		          </div>
		          <!-- 슬라이드 -->
		          <div class="class-list-wrap myClassSlick flex-row" data-slick="true">
		            <!-- card style -->
		            <c:forEach var="result" items="${myCurriculumList}" varStatus="status">
			            <div class="flex-col relative">
	            			<c:url var="stuViewUrl" value="/lms/manage/studyPlanView.do">
	                            <c:param name="menuId" value="MNU_0000000000000086" />
	                            <c:param name="crclId" value="${result.crclId}" />
	                            <c:param name="plId" value="${result.plId}" />
	                            <c:param name="tabType" value="T" />
	                            <c:param name="step" value="3" />
	                        </c:url>
			              <a href="${stuViewUrl}" class="card-wrap">

			                <div class="card-head">
			                	<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />
								<fmt:formatDate value="${now}" pattern="HHmm" var="todayTime" />
			                	<c:choose>
			                		<c:when test="${result.startTime > currTime}">
			                			<div class="card-label bg-green">
			                				<c:choose><c:when test="${USER_INFO.userSeCode eq '08'}">수업예정</c:when><c:otherwise>대기</c:otherwise></c:choose>
			                			</div>
			                		</c:when>
			                		<c:when test="${result.startTime <= currTime and currTime < result.endTime}">
			                			<div class="card-label bg-point">수업중</div>
			                		</c:when>
			                		<c:when test="${result.endTime < currTime}">
			                			<c:choose>
			                				<c:when test="${USER_INFO.userSeCode eq '08'}">
			                					<div class="card-label">수업완료</div>
			                				</c:when>
			                				<c:otherwise>
			                					<div class="card-label bg-blue">
			                						<c:choose>
			                							<c:when test="${empty result.attentionType}">
										            		<c:choose>
										            			<c:when test="${today eq crclDay && todayTime > result.startTime && todayTime <= result.endTime}">
										            				진행중
										            			</c:when>
										            			<c:when test="${today > crclDay}">
										            				결석
										            			</c:when>
										            			<c:otherwise>
										            				대기
										            			</c:otherwise>
										            		</c:choose>
										            	</c:when>
										            	<c:when test="${result.attentionType eq 'Y'}">
										            		출석
										            	</c:when>
										            	<c:when test="${result.attentionType eq 'L'}">
										            		지각
										            	</c:when>
										            	<c:when test="${result.attentionType eq 'N'}">
										            		결석
										            	</c:when>
										            	<c:otherwise>
										            		-
										            	</c:otherwise>
			                						</c:choose>
			                					</div>
			                				</c:otherwise>
			                			</c:choose>
			                			
			                		</c:when>
			                		<c:otherwise>-</c:otherwise>
			                	</c:choose>

			                  <p class="card-flag-img">
			                     <c:set var="imgSrc">
									<c:import url="/lms/common/flag.do" charEncoding="utf-8">
										<c:param name="ctgryId" value="${result.crclLang}"/>
									</c:import>
								  </c:set>
								  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
			                  </p>
			                  <div class="card-title-wrap">
			                    <span class="card-subtitle"><b>${fn:substring(result.startTime, 0, 2)}:${fn:substring(result.startTime, 2, 4)}</b>
			                    (${result.sisu}교시)</span>
			                    <div class="card-title">${result.crclNm}</div>
			                  </div>
			                </div>
			                <div class="card-body">

			                  <ul class="card-items-wrap">
			                    <li class="card-items icon-campus">
			                    	<c:forEach var="campus" items="${campusList}" varStatus="status">
										<c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
										  <c:out value="${campus.ctgryNm}"/>
										</c:if>
									</c:forEach>
			                    </li>
			                    <li class="card-items icon-name">
			                    	<c:set var="comma" value=","/>
									<c:set var="facCnt" value="1"/>
									<c:forEach var="list" items="${myCurriculumList}" varStatus="status">
										<c:if test="${result.crclId eq list.crclId and result.plId eq list.plId}">
											<c:forEach var="fac" items="${list.facPlList}" varStatus="status">
												<c:if test="${fac.plId eq list.plId}">
													<c:if test="${!status.last}">
														<c:out value="${fac.userNm}"/>
														<c:if test="${facCnt ne 1}">(부교원)</c:if>${comma}
													</c:if>
													<c:if test="${status.last}">
														<c:out value="${fac.userNm}"/>
														<c:if test="${facCnt ne 1}">(부교원)</c:if>교수
													</c:if>
													<c:set var="facCnt" value="${facCnt + 1}"/>
												</c:if>
											</c:forEach>
										</c:if>
									</c:forEach>
			                    </li>
			                  </ul>
			                </div>
			              </a>
			            </div>
		            </c:forEach>
		          </div>
		        </div>
		        <div class="center-align mt-50">
		          <a href="/sch/selectTodayCrclList.do?menuId=MNU_0000000000000086" class="btn-xl btn-point">나의 수업 일정 더보기</a>
		        </div>
		      </div>
		    </div>
		  </section>
		  
		  <c:choose>
		      <c:when test="${USER_INFO.userSeCode eq '08' and fn:length(selectHomeworkList) != 0}">
		          <!-- 과제 -->
		          <section class="area-full">
		            <div class="section-gap border-top-line">
		              <div class="main-common-title">
		                <h2 class="title">과제</h2>
		              </div>
		              <div class="area">
		                <div class="flex-row">
		                  <!-- card style -->
		                  <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
		                      <%-- 날짜 비교를 위한 데이터 컨버팅 --%>
		                      <c:set var="openDate" value="${fn:replace(result.openDate, '-', '')}${result.openTime }"/>
		                      <fmt:parseDate value="${openDate}" var="openParseDate" pattern="yyyyMMddHHmm"/>
		                      <fmt:formatDate value="${openParseDate}" pattern="yyyyMMddHHmm" var="openDateForm"/>
		
		                      <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
		                      <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
		                      <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateForm"/>
		
		                      <%-- 날짜 비교 계산 --%>
		                      <fmt:parseDate value="${fn:substring(result.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
		                      <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
		
		                      <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="nowDateHyphen"/>
		                      <fmt:parseDate value="${nowDateHyphen}" pattern="yyyy-MM-dd" var="closeDateHyphen2" />
		                      <fmt:parseNumber value="${closeDateHyphen2.time / (1000*60*60*24)}" integerOnly="true" var="nowDateTime" />
		
		                      <div class="flex-col-4 card-type3 relative">
		                        <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
		                            <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
		                            <c:param name="hwId" value="${result.hwId }" />
		                            <c:param name="hwsId" value="${result.hwsId }" />
		                            <c:param name="crclId" value="${result.crclId }" />
		                            <c:param name="plId" value="${result.plId }" />
		                            <c:param name="homeworkTotalFlag" value="Y" />
		                            <c:param name="boardType" value="list" />
		                            <c:param name="menuId" value="MNU_0000000000000092" />
		                        </c:url>
		                        <c:choose>
		                            <c:when test="${result.homeworkSubmitCnt eq result.memberCnt && result.homeworkSubmitCnt ne '0' && result.memberCnt ne '0' }">
		                                <a href="${viewUrl }" class="card-right-label btn-point">제출 완료</a>
		                            </c:when>
		                            <c:otherwise>
		                                <a href="${viewUrl }" class="card-right-label btn-outline">제출된 과제 ${result.homeworkSubmitCnt }</a>
		                            </c:otherwise>
		                        </c:choose>
		
		                        <a href="${viewUrl }" class="card-wrap">
		                          <c:set var="diffDate" value="${closeDateTime - nowDateTime }"/>
		                          <c:choose>
		                              <c:when test="${diffDate eq 0 }">
		                                  <c:set var="dateText" value="오늘마감"/>
		                              </c:when>
		                              <c:when test="${closeDateTime > nowDateTime }">
		                                  <c:set var="dateText" value="D-${diffDate }"/>
		                              </c:when>
		                              <c:otherwise>
		                                  <c:set var="dateText" value="제출마감"/>
		                              </c:otherwise>
		                          </c:choose>
		
		                          <div class="card-head">
		                            <div class="card-title-wrap">
		                              <span class="card-category font-point">(${dateText })</span>
		                              <div class="card-title">${result.hwCodeNm}(${fn:substring(result.hwTypeNm,0,2) })</div>
		                              <span class="card-subtitle">${result.crclNm}</span>
		                            </div>
		                          </div>
		                          <div class="card-body">
		                            <div class="card-desc ">${result.nttSj}</div>
		            
		                            <ul class="card-items-wrap">
		                              <li class="card-items icon-name">${result.userNm} 교수</li>
		                              <li class="card-items icon-date">${result.openDate} ~ ${result.closeDate}</li>
		                            </ul>
		                          </div>
		                        </a>
		                      </div>
		                  </c:forEach>
		                </div>
		              </div>
		            </div>
		          </section>
		      </c:when>
		      <c:when test="${(USER_INFO.userSeCode eq '02' || USER_INFO.userSeCode eq '04' || USER_INFO.userSeCode eq '06') and fn:length(selectHomeworkList) != 0}">
		          <!-- 과제 -->
				  <section class="area-full">
				    <div class="section-gap border-top-line">
				      <div class="main-common-title">
				        <h2 class="title">과제</h2>
				      </div>
				      <div class="area">
				        <div class="flex-row">
				          <!-- card style -->
				          <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
				              <!-- 제출일자 게산을 위한 함수 -->
		                      <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
		                      <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
		                      <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />
		                      
		                      <fmt:formatDate value="${result.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />
		                      
		                      <!-- 아래 날짜 표기를 위한 함수 -->
		                      <fmt:formatDate var="strLastUpdusrPnttmHHmm" pattern="yyyy-MM-dd HH:mm" value="${result.lastUpdusrPnttm }"/>
		
		                      <!-- 제출 여부 처리  -->
		                      <c:choose>
		                          <c:when test="${empty result.hwsId }">
		                              <c:set var="submitAt" value="미제출"/>
		                              <c:set var="registAction" value="regist"/>
		                              <c:set var="homeworkSummitFontColor" value="font-orange"/>
		                          </c:when>
		                          <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
		                              <c:set var="submitAt" value="마감후"/>
		                              <c:set var="registAction" value="updt"/>
		                              <c:set var="homeworkSummitFontColor" value="font-orange"/>
		                          </c:when>
		                          <c:otherwise>
		                              <c:set var="submitAt" value="제출"/>
		                              <c:set var="registAction" value="updt"/>
		                              <c:set var="homeworkSummitFontColor" value="font-blue"/>
		                          </c:otherwise>
		                      </c:choose>
		                      
		                      <!-- 마감 후 제출 날짜 계산 --> 
		                      <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
		                          <fmt:parseDate value="${fn:substring(result.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
		                          <fmt:formatDate var="strLastUpdusrPnttm" pattern="yyyy-MM-dd" value="${result.lastUpdusrPnttm }"/>
		                          <fmt:parseDate value="${fn:substring(strLastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
		                          <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
		                          <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
		                      </c:if>

                              <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
		                          <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
                              </c:if>

	                          <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
	                          <c:if test="${dDay eq 0 }">
	                              <c:set var="dDay" value="1"/>
	                          </c:if>

					          <div class="flex-col-4 card-type3 relative">
		                        <c:url var="viewUrl" value="/lms/manage/selectHomeworkSubmitArticle.do">
				                    <c:param name="hwId" value="${result.hwId }" />
				                    <c:param name="hwsId" value="${result.hwsId }" />
				                    <c:param name="crclId" value="${result.crclId }" />
				                    <c:param name="plId" value="${result.plId }" />
				                    <c:param name="registAction" value="${registAction }" />
				                    <c:param name="menuId" value="MNU_0000000000000068" />
				                </c:url>
					            <a href="${viewUrl }" class="card-wrap">

					              <div class="card-head">
					                <div class="card-title-wrap">
					                  <span class="card-category ${homeworkSummitFontColor }">${submitAt }
						                  <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }"> 
						                  (D+${dDay })
						                  </c:if>
					                  </span>
					                  <div class="card-title">${result.hwCodeNm}(${fn:substring(result.hwTypeNm,0,2) })</div>
					                  <span class="card-subtitle">${result.crclNm}</span>
					                </div>
					              </div>
					              <div class="card-body">
					                <div class="card-desc ">${result.nttSj}</div>
					
					                <ul class="card-items-wrap">
					                  <li class="card-items icon-name">${result.userNm}</li>
					                  <li class="card-items icon-date">${result.openDate} ~ ${result.closeDate}</li>
					                </ul>
					              </div>
					            </a>
					          </div>
				          </c:forEach>				
				        </div>
				      </div>
				    </div>
				  </section>
		      </c:when>
		  </c:choose>
		  <c:if test="${USER_INFO.userSeCode eq '08'}">
		  <section class="area-full">
		    <!-- 나의 교육과정 -->
		    <div class="main-process section-gap border-top-line">
		      <div class="area">
		        <div class="main-common-title">
		          <h2 class="title">나의 교육과정</h2>
		          <p class="sub-title">현재 총 ${myCurriculumCnt}개의 특수외국어 과정이 진행 중입니다.</p>
		        </div>
		        <div class="process-wrap">
		          <!-- 슬라이드 -->
		          <div class="process-list-wrap flex-row">
			            <!-- card style -->
			            <c:forEach var="result" items="${resultList}" varStatus="status">
		            	  <c:choose>
							<c:when test="${fn:length(resultList) >= 6}">
								<c:set var="listCnt" value="6"/>
							</c:when>
							<c:when test="${fn:length(resultList) >= 3 and fn:length(resultList) < 6}">
								<c:set var="listCnt" value="3"/>
							</c:when>
							<c:otherwise>
								<c:set var="listCnt" value="${fn:length(resultList)}"/>
							</c:otherwise>
						  </c:choose>
					  <c:if test="${status.count <= listCnt}">
						<c:url var="crmView" value="/lms/crm/CurriculumAllView.do">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:param name="crclbId" value="${result.crclbId}"/>
							<c:param name="menuId" value="MNU_0000000000000068"/>
							<c:param name="searchTargetType" value="N"/>
						</c:url>
			            <div class="flex-col-4 relative">
			              <a href="${crmView }" class="card-wrap">

			                <div class="card-head">
			                  <div class="card-label bg-point">
			                  	<c:choose>
									<c:when test="${result.processSttusCodeDate eq '0'}">관리자 등록확정대기</c:when>
									<c:when test="${result.processSttusCodeDate eq '1'}">과정계획 대기</c:when>
									<c:when test="${result.processSttusCodeDate eq '2'}">개설예정</c:when>
									<c:when test="${result.processSttusCodeDate eq '3'}">수강신청 중</c:when>
									<c:when test="${result.processSttusCodeDate eq '4'}">수강신청 종료</c:when>
									<c:when test="${result.processSttusCodeDate eq '5'}">과정개설취소</c:when>
									<c:when test="${result.processSttusCodeDate eq '6'}">수강대상자 확정</c:when>
									<c:when test="${result.processSttusCodeDate eq '7'}">과정 중</c:when>
									<c:when test="${result.processSttusCodeDate eq '8'}">과정종료</c:when>
									<c:when test="${result.processSttusCodeDate eq '9'}">과정종료</c:when>
									<c:when test="${result.processSttusCodeDate eq '10'}">과정종료(성적발표)</c:when>
									<c:when test="${result.processSttusCodeDate eq '11'}">성적발표 종료</c:when>
									<c:when test="${result.processSttusCodeDate eq '12'}">최종종료(확정)</c:when>
									<c:otherwise>-</c:otherwise>
								  </c:choose>
			                  </div>
			                  <p class="card-flag-img">
			                     <c:set var="imgSrc">
									<c:import url="/lms/common/flag.do" charEncoding="utf-8">
										<c:param name="ctgryId" value="${result.crclLang}"/>
									</c:import>
								  </c:set>
								  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
			                  </p>
			                  <div class="card-title-wrap">
			                    <span class="card-category ">${result.crclYear}년도 ${result.crclTermNm}</span>
			                    <div class="card-title">${result.crclNm}</div>
			                  </div>
			                </div>
			                <div class="card-body">

			                  <ul class="card-items-wrap">
			                    <li class="card-items icon-campus">
			                    	<c:forEach var="campus" items="${campusList}" varStatus="status">
										<c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
										  <c:out value="${campus.ctgryNm}"/>
										</c:if>
									</c:forEach>
			                    </li>
			                    <li class="card-items icon-name">
								<c:set var="comma" value=","/>
								<c:set var="teacherCnt" value="0"/>
								<c:forEach var="list" items="${resultList}" varStatus="status">
									<c:if test="${result.crclId eq list.crclId}">
										<c:forEach var="fac" items="${list.facList}" varStatus="status">
											<c:if test="${0 eq status.index }">
												<c:choose>
												    <c:when test="${1 < fn:length(list.facList)}"><c:out value="${fac.userNm}"/>${comma}</c:when>
												    <c:otherwise><c:out value="${fac.userNm}"/>교수</c:otherwise>
												</c:choose>
											</c:if>
											<c:if test="${1 eq status.index }">
											    <c:out value="${fac.userNm}"/>교수
											</c:if>
											<c:if test="${1 < status.index }">
                                                <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
                                            </c:if>
										</c:forEach>
									</c:if>
								</c:forEach>
									<c:if test="${0 < teacherCnt }">외${teacherCnt }명</c:if>
						  		</li>

			                    <li class="card-items icon-date">${result.applyStartDate } ~ ${result.applyEndDate }</li>
			                  </ul>
			                </div>
			              </a>
			            </div>
			            </c:if>
		            </c:forEach>
		            </div>
		            </div>
		        <div class="center-align mt-50">
		          <a href="/lms/crm/selectMyCurriculumList.do?firstEnter=Y&menuId=MNU_0000000000000068" class="btn-xl btn-point">나의 교육과정 더보기</a>
		        </div>
		    </div>
		    </div>
		    </c:if>
		  </section>
    	</c:when>
    	<c:otherwise>
    		<section class="main-visual">
	      <div class="visual-wrap visualSlick" data-slick="true">
	        <div class="list">
	          <div class="info-wrap">
	            <div class="flex-vertical-mid">
	              <h2 class="title">
	                <div class="keep-all">
	                  <p class="font-poppins">Unique & Best</p>
	                  <small class="small">특수외국어교육</small><br>
	                  <b>국내 NO.1 글로벌 대학<br>한국외국어대학교가 이끌어갑니다.</b>
	                </div>
	              </h2>
	              <a href="/msi/cntntsService.do?menuId=MNU_0000000000000014" class="btn-more btn-xl">MORE</a>
	            </div>
	          </div>
	          <div class="img-wrap" style="background-image: url(${CML}/imgs/main/main_visual_img_01.jpg)"></div>
	        </div>
	        <div class="list">
	          <div class="info-wrap">
	            <div class="flex-vertical-mid">
	            
	              <h2 class="title">
	                <span class="keep-all">한국외국어대학교는<br><b class="large">전문적</b>이고 <b class="large">차별화</b>된 커리큘럼으로<br>최고의
	                  특수외국어 전문 인력을 <br class="none block-xl">양성하고 있습니다.</span>
	              </h2>
	              <a href="/lms/crm/selectCurriculumAllList.do?menuId=MNU_0000000000000066" class="btn-more btn-xl">MORE</a>
	            </div>
	          </div>
	          <div class="img-wrap" style="background-image: url(${CML}/imgs/main/main_visual_img_02.jpg)"></div>
	        </div>
	        <div class="list">
	          <div class="info-wrap">
	            <div class="flex-vertical-mid">
	              <h2 class="title">
	                <span class="keep-all"> 한국외국어대학교는<br><b class="large">표준화된 시스템</b> 구축을 통해 특수외국어교육의 기준을 <br class="none block-xl">세워 나가고 있습니다.</span>
	              </h2>
	              <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000004&menuId=MNU_0000000000000010" class="btn-more btn-xl">MORE</a>
	            </div>
	          </div>
	          <div class="img-wrap" style="background-image: url(${CML}/imgs/main/main_visual_img_03.jpg)"></div>
	        </div>
	      </div>
		  <!-- 검색창 -->
		  <form id="mainSearchForm" name="mainSearchForm" method="post" action="/lms/crm/selectCurriculumAllList.do">
				<input type="hidden" name="menuId" value="MNU_0000000000000066"/>
				<div class="visual-search">
					<div class="area">
					  <div class="notice-box-wrap">
						<p class="notice-title">
						  <img src="${CML}/imgs/common/icon_education.svg" alt="교육과정 안내 아이콘"> 교육과정 안내
						</p>
						<select name="searchCrclLang" class="select2" data-select="style3">
							<option value="">언어 전체</option>
							<c:forEach var="result" items="${languageList}" varStatus="status">
								<c:if test="${not empty result.upperCtgryId}">
									<option value="${result.ctgryId}">${result.ctgryNm}</option>
								</c:if>
							</c:forEach>
						</select>

						<input type="text" name="searchCrclNm" class="notice-input" placeholder="찾으시는 과정을 입력해주세요">
						<button class="notice-search-btn btn-point">검색</button>
					  </div>
					</div>
				</div>
		  </form>
		</section>
		</c:otherwise>
		</c:choose>



        <!-- 사업소개 ~ K-MOOC -->
        <section class="main-service">
            <div class="area-full m-0 p-0 w-100 mw-100">
                <div class="bg-gray-light section-gap">
                    <div class="area-full">
                    <ul class="service-list-wrap flex-row">
                        <li class="list flex-col-3" onclick="location.href='/msi/cntntsService.do?menuId=MNU_0000000000000014'">
                            <div class="img-wrap"><img src="${CML}/imgs/main/main_service_icon_01.png" class="img" alt="아이콘"></div>
                            <h3 class="title">사업소개</h3>
                            <a href="#none" class="btn-more">더 알아보기<img src="${CML}/imgs/main/icon_line_arrow.svg" /></a>
                        </li>
                        <li class="list flex-col-3" onclick="location.href='/lms/crm/selectCurriculumAllList.do?menuId=MNU_0000000000000066'">
                            <div class="img-wrap"><img src="${CML}/imgs/main/main_service_icon_02.png" class="img" alt="아이콘"></div>
                            <h3 class="title">특수외국어교육과정</h3>
                            <a href="#none" class="btn-more">수강신청하기<img src="${CML}/imgs/main/icon_line_arrow.svg" /></a>
                        </li>
                        <li class="list flex-col-3" onclick="location.href='/lms/common/app.do'">
                            <div class="img-wrap"><img src="${CML}/imgs/main/main_service_icon_03.png" class="img" alt="아이콘"></div>
                            <h3 class="title">특수외국어 교재/앱사전</h3>
                            <a href="#none" class="btn-more">무료 다운로드<img src="${CML}/imgs/main/icon_line_arrow.svg" /></a>
                        </li>
                        <li class="list flex-col-3" onclick="location.href='/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000007&menuId=MNU_0000000000000022'">
                            <div class="img-wrap"><img src="${CML}/imgs/main/main_service_icon_04.png" class="img" alt="아이콘"></div>
                            <h3 class="title">K-MOOC</h3>
                            <a href="#none" class="btn-more">강좌 신청하기<img src="${CML}/imgs/main/icon_line_arrow.svg" /></a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
        </section>

        <!-- 추가영역 -->
        <section class="section-gap">
            <div class="area-full">
                <div class="flex-row card-box-wrap">

                    <!-- 공지사항 -->
                    <div class="flex-col-4" id="boardBox">
                        <div class="card-wrap">
                            <div class="card-head">
                                <ul class="tab-wrap line-style">
                                    <li class="tab-list on"><a href="#none">공지사항</a></li>
                                    <li class="tab-list"><a href="#none">언론보도</a></li>
                                </ul>
                                <a class="more more0" href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000001&menuId=MNU_0000000000000024"></a>
                                <a class="more more1" href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000455&menuId=MNU_0000000000000176" style="display:none;"></a>
                            </div>
                            <div class="card-body">
                                <ul class="con-board">
                                    <c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
                                        <c:param name="tmplatCours" value="${_IMG}"/>
                                        <c:param name="linkMenuId" value="MNU_0000000000000024" />
                                        <c:param name="viewType" value="data" />
                                        <c:param name="tableId" value="BBSMSTR_000000000001" />
                                        <c:param name="boardCard" value="Y" />
                                        <c:param name="listTag" value="li" />
                                        <c:param name="itemCount" value="7" />

                                        <c:param name="colname" value="nttSj" />
                                        <c:param name="length" value="20" />
                                        <c:param name="tag" value="a" />
                                        <c:param name="cssClass" value=""/>
                                    </c:import>
                                </ul>
                                <ul class="con-board" style="display:none;">
                                    <c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
                                        <c:param name="tmplatCours" value="${_IMG}"/>
                                        <c:param name="linkMenuId" value="MNU_0000000000000176" />
                                        <c:param name="viewType" value="data" />
                                        <c:param name="tableId" value="BBSMSTR_000000000455" />
                                        <c:param name="boardCard" value="Y" />
                                        <c:param name="listTag" value="li" />
                                        <c:param name="itemCount" value="7" />

                                        <c:param name="colname" value="nttSj" />
                                        <c:param name="length" value="20" />
                                        <c:param name="tag" value="a" />
                                        <c:param name="cssClass" value=""/>
                                    </c:import>
                                </ul>
                            </div>
                        </div>
                    </div>
					
					<%-- 배너 게시 중 여부 체크 --%>
                    <c:choose>
                    	<c:when test="${bannerServiceCnt > 0}">
                    		<!-- 포토갤러리 -->
	                        <div class="flex-col-4" id="galleryBox">
	                            <div class="card-wrap">
	                                <h3 class="ell">특수외국어교육 한국외국어대학교 소식</h3>
	                                <ul class="gallerySlick" id="gallerySlick" data-slick="true">
	                                	<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
	                                        <c:param name="tmplatCours" value="${_IMG}"/>
	                                        <c:param name="linkMenuId" value="MNU_0000000000000028" />
	                                        <c:param name="viewType" value="gallery" />
	                                        <c:param name="tableId" value="BBSMSTR_000000000002" />
	                                        <c:param name="listTag" value="li" />
	                                        <c:param name="itemCount" value="3" />
	                                    </c:import>
	                                </ul>
	                            </div>
	                        </div>
	                        <!-- 배너 -->
	                        <div class="flex-col-4" id="bannerBox">
	                        	<c:import url="/msi/ctn/bannerService.do" charEncoding="utf-8">
									<c:param name="tmplatCours" value="${_IMG}"/>
									<c:param name="tableId" value="BannerZone" />
									<c:param name="listTag" value="div" />
									
									<c:param name="colname" value="bannerImage" />
									<c:param name="length" value="1" />
									<c:param name="tag" value="a" />
									<c:param name="cssClass" value="" />
								</c:import>
	                        </div>
                    	</c:when>
                    	<c:otherwise>
                    		<div class="flex-col-8" id="galleryBox"><!-- .flex-col-8 -->
	                            <div class="card-wrap">
	                                <h3 class="ell">특수외국어교육 한국외국어대학교 소식</h3>
	                                <ul class="gallerySlick" id="gallerySlick2" data-slick="true"> <!-- #gallerySlick2 -->
	                                	<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
	                                        <c:param name="tmplatCours" value="${_IMG}"/>
	                                        <c:param name="linkMenuId" value="MNU_0000000000000028" />
	                                        <c:param name="viewType" value="gallery" />
	                                        <c:param name="tableId" value="BBSMSTR_000000000002" />
	                                        <c:param name="listTag" value="li" />
	                                        <c:param name="itemCount" value="6"/>
	                                    </c:import>
	                                </ul>
	                            </div>
	                        </div>
                    	</c:otherwise>
                    </c:choose>
                </div><!-- //.card-box-wrap -->
            </div>
        </section>
        <!-- //추가영역 -->
		
		<c:if test="${USER_INFO.userSeCode ne '08'}">
		<!-- 특수외국어 교육과정 -->
		<section class="section-gap pt-0">
		  <div class="area-full">
			<div class="main-common-title">
			  <h2 class="title">특수외국어 교육과정</h2>
			  <p class="sub-title">현재 총 11개 특수외국어 학습을 위한 다양한 과정이 진행 중입니다.</p>
			</div>
			<!-- tab style -->
			<form name="frm" method="post" action="<c:url value="/lms/crm/selectCurriculumAllList.do"/>">
				<input type="hidden" name="menuId" value="MNU_0000000000000066" />
				<input type="hidden" name="crclLang" value="" />

			  <ul class="tab-wrap line-style">
				<li class="tab-list liLang <c:if test="${empty searchVO.searchCrclLang }">on</c:if>" data-id="">전체</li>
				<c:forEach var="result" items="${languageList}" varStatus="status">
				   <c:if test="${not empty result.upperCtgryId}">
					 <c:set var="imgSrc">
					   <c:import url="/lms/common/flag.do" charEncoding="utf-8">
						 <c:param name="ctgryId" value="${result.ctgryId}"/>
					   </c:import>
					 </c:set>
					 <li class="tab-list liLang <c:if test="${searchVO.searchCrclLang eq result.ctgryId }">on</c:if>" data-id="${result.ctgryId}">
					   <%-- <a href="#">${result.ctgryNm}</a> --%>
					   ${result.ctgryNm}
					 </li>
				   </c:if>
				 </c:forEach>
			  </ul>
			  </form>
			<div class="main-course">
			  <div id="curCard" class="flex-row">
			  <c:forEach var="result" items="${resultList}" varStatus="status">
				  <c:choose>
					<c:when test="${fn:length(resultList) >= 8}">
						<c:set var="listCnt" value="8"/>
					</c:when>
					<c:when test="${fn:length(resultList) >= 4 and fn:length(resultList) < 8}">
						<c:set var="listCnt" value="4"/>
					</c:when>
					<c:otherwise>
						<c:set var="listCnt" value="fn:length(resultList)"/>
					</c:otherwise>
				  </c:choose>
				  <c:if test="${status.count <= listCnt}">
					<c:url var="crmView" value="/lms/crm/CurriculumAllView.do">
						<c:param name="crclId" value="${result.crclId}"/>
						<c:param name="crclbId" value="${result.crclbId}"/>
						<c:param name="menuId" value="MNU_0000000000000066"/>
						<c:param name="searchTargetType" value="N"/>
					</c:url>
					<div class="flex-col-3 relative">
						<a href="${crmView}" class="card-wrap">

						  <div class="card-head">
							<p class="card-flag-img">
							  <c:set var="imgSrc">
								<c:import url="/lms/common/flag.do" charEncoding="utf-8">
									<c:param name="ctgryId" value="${result.crclLang}"/>
								</c:import>
							  </c:set>
							  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
							</p>
							<div class="card-title-wrap">
							  <span class="card-category font-blue">
							  <fmt:parseDate value="${result.applyStartDate}" var="startDate" pattern="yyyy-MM-dd"/>
							  <fmt:formatDate value="${startDate}" var="start" pattern="yyyyMMdd"/>

							  <jsp:useBean id="toDay" class="java.util.Date"/>
							  <fmt:formatDate var="now" value="${toDay}" pattern="yyyyMMdd" />

							  <c:choose>
								<c:when test="${result.processSttusCodeDate eq '0'}">관리자 등록확정대기</c:when>
								<c:when test="${result.processSttusCodeDate eq '1'}">과정계획 대기</c:when>
								<c:when test="${result.processSttusCodeDate eq '2'}">개설예정</c:when>
								<c:when test="${result.processSttusCodeDate eq '3'}">수강신청 중</c:when>
								<c:when test="${result.processSttusCodeDate eq '4'}">수강신청 종료</c:when>
								<c:when test="${result.processSttusCodeDate eq '5'}">과정개설취소</c:when>
								<c:when test="${result.processSttusCodeDate eq '6'}">수강대상자 확정</c:when>
								<c:when test="${result.processSttusCodeDate eq '7'}">과정 중</c:when>
								<c:when test="${result.processSttusCodeDate eq '8'}">과정종료</c:when>
								<c:when test="${result.processSttusCodeDate eq '9'}">과정종료</c:when>
								<c:when test="${result.processSttusCodeDate eq '10'}">과정종료(성적발표)</c:when>
								<c:when test="${result.processSttusCodeDate eq '11'}">성적발표 종료</c:when>
								<c:when test="${result.processSttusCodeDate eq '12'}">최종종료(확정)</c:when>
								<c:otherwise>-</c:otherwise>
							  </c:choose>
							  <c:set var="dDay" value="${start-now}"/>
							  <c:if test="dDay > 0">(D-${dDay})</c:if>
							  </span>
							  <div class="card-title">${result.crclNm}</div>
							</div>
						   </div>

						  <div class="card-body">
							<ul class="card-items-wrap">
							  <li class="card-items icon-campus">
								<c:forEach var="campus" items="${campusList}" varStatus="status">
									<c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
									  <c:out value="${campus.ctgryNm}"/>
									</c:if>
								</c:forEach>
							  </li>
							  <li class="card-items icon-name">
								<c:set var="comma" value=","/>
								<c:set var="teacherCnt" value="0"/>
								<c:forEach var="list" items="${resultList}" varStatus="status">
									<c:if test="${result.crclId eq list.crclId}">
										<c:forEach var="fac" items="${list.facList}" varStatus="status">
											<c:if test="${0 eq status.index }">
												<c:choose>
												    <c:when test="${1 < fn:length(list.facList)}"><c:out value="${fac.userNm}"/>${comma}</c:when>
												    <c:otherwise><c:out value="${fac.userNm}"/>교수</c:otherwise>
												</c:choose>
											</c:if>
											<c:if test="${1 eq status.index }">
											    <c:out value="${fac.userNm}"/>교수
											</c:if>
											<c:if test="${1 < status.index }">
                                                <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
                                            </c:if>
										</c:forEach>
									</c:if>
								</c:forEach>
								<c:if test="${0 < teacherCnt }">외${teacherCnt }명</c:if>
							  </li>
							  <li class="card-items icon-date">${result.applyStartDate } ~ ${result.applyEndDate }</li>
							</ul>
						  </div>
						</a>
					  </div>
					  </c:if>
			  </c:forEach>
			  </div>
			</div>
			<div class="center-align">
			  <a href="/lms/crm/selectCurriculumAllList.do?menuId=MNU_0000000000000066" class="btn-xl btn-outline">교육과정 전체보기</a>
			</div>
		  </div>
		</section>
    	</c:if>
    
    </section>


    <!-- 특수외국어 출간 교재 -->
    <section class="main-book section-gap" style="display:none !important;">
      <div class="area-full">
        <div class="main-common-title">
          <h2 class="title">특수외국어 출간 교재</h2>
          <p class="sub-title">특수 외국어 표준 교육과정을 바탕으로 한 기초 교재부터 e-book까지 제공합니다.</p>
        </div>
        <div class="book-list-wrap bookListSlick" data-slick="true">
        	<c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
				<c:param name="tmplatCours" value="${_IMG}"/>
				<c:param name="siteId" value="SITE_000000000000001" />
				<c:param name="linkMenuId" value="MNU_0000000000000008" />
				<c:param name="viewType" value="gallery" />
				<c:param name="tableId" value="BBSMSTR_000000000005" />
				<c:param name="itemCount" value="15" />
				<c:param name="noticeAt" value="Y"/>
				<c:param name="mainAt" value="Y"/>
			</c:import>
        </div>
        <div class="center-align mt-40">
          <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000005&menuId=MNU_0000000000000008" class="btn-xl btn-point">교재/사전 더보기</a>
        </div>
      </div>
    </section>
    <c:choose>
	    <c:when test="${USER_INFO.userSeCode eq '02' || USER_INFO.userSeCode eq '04' || USER_INFO.userSeCode eq '06' || USER_INFO.userSeCode eq '08'}">
	    	<!-- 학습 자료 -->
			  <section class="area-full mb-70">
			    <div class="section-gap bg-gray-light main-files-wrap">
			      <div class="main-common-title">
			        <h2 class="title">학습자료</h2>
			      </div>
			      <div class="area">
			        <div class="flex-row">
			        	<c:forEach var="result" items="${crsBbsList}" varStatus="status">
			        		<c:url var="viewUrl" value="/cop/bbs/selectBoardArticle.do">
			        			<c:param name="menuId" value="MNU_0000000000000008" />
			        			<c:param name="bbsId" value="${result.bbsId}" />
							  	<c:param name="nttNo" value="${result.nttNo}" />
						    </c:url>
				          <div class="flex-col-3">
				            <a href="${viewUrl}" class="card-st2-wrap">
				            	<c:set var="imgSrc">
									<c:choose>
			                    		<c:when test="${empty result.atchFileNm}">${CML}/imgs/common/img_no_image.svg</c:when>
			                    		<c:otherwise>
			                    			<c:url value='/cmm/fms/getImage.do'>
			                    				<c:param name="thumbYn" value="Y" />
			                    				<c:param name="siteId" value="SITE_000000000000001" />
			                    				<c:param name="appendPath" value="${result.bbsId}" />
			                    				<c:param name="atchFileNm" value="${result.atchFileNm}" />
			                    			</c:url>
			                    		</c:otherwise>
			                    	</c:choose>
								</c:set>
				              <div class="card-st2-img" style="background-image:url(${imgSrc})"></div>
				              <div class="card-title-wrap">
				                <span class="card-category font-darkgreen">${result.tmp04 }</span>
				                <div class="card-title dotdotdot">${result.nttSj }</div>
				                <span class="card-subtitle">저자 ${result.tmp02 }</span>
				              </div>
				            </a>
				          </div>
				        </c:forEach>
				    </div>
			        <div class="center-align mt-50">
			          <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000010&menuId=MNU_0000000000000100" class="btn-xl btn-outline">나의 학습자료 더보기</a>
			        </div>
			    </div>
			  </section>
	    </c:when>
	    <c:otherwise>
	    	<!-- 공지사항 -->
	    <section class="section-gap pt-0">
	      <div class="area-full">
	        <div class="notice-box-wrap mb-30" style="display:none !important;">
	          <p class="notice-title">
	            <img src="${CML}/imgs/common/icon_notice.svg" alt="알림 아이콘">
	            공지사항
	          </p>
	          <ul class="notice-info-wrap ell noticeListSlick" data-slick=" true">
		          <c:import url="/msi/ctn/boardService.do" charEncoding="utf-8">
					<c:param name="tmplatCours" value="${_IMG}"/>
					<c:param name="linkMenuId" value="MNU_0000000000000024" />
					<c:param name="viewType" value="data" />
					<c:param name="tableId" value="BBSMSTR_000000000001" />
					<c:param name="listTag" value="li" />
					<c:param name="itemCount" value="5" />

					<c:param name="colname" value="nttSj" />
					<c:param name="length" value="20" />
					<c:param name="tag" value="a" />
					<c:param name="cssClass" value=""/>
				</c:import>
	        </ul>
	          <div class="notice-icons-wrap">
		          <button class="notice-icons prev" title="이전"></button>
		          <button class="notice-icons next" title="다음"></button>
		          <button class="notice-icons play" title="재생/정지"></button>
	          </div>
	          <a class="notice-more" href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000001&menuId=MNU_0000000000000024"><img src="${CML}/imgs/common/icon_notice_plus.jpg" alt="더보기"></a>
	        </div>
	        <ul class="flex-row main-board-wrap">
	          <li class="flex-col-3">
	            <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000006&menuId=MNU_0000000000000155" class="main-board-items">
	              <img src="${CML}/imgs/main/icon_notice_board_01.svg" alt="과정소식">
	              <p class="title">과정소식</p>
	              <span class="desc">특수외국어진흥사업 과정소식을 알립니다</span>
	            </a>
	          </li>
	          <li class="flex-col-3">
	            <a href="/lms/crcl/selectHomeworkCommentPickAtList.do?boardType=list&menuId=MNU_0000000000000158" class="main-board-items">
	              <img src="${CML}/imgs/main/icon_notice_board_02.svg" alt="과정후기">
	              <p class="title">과정후기</p>
	              <span class="desc">교육 수강 후기를 만나보세요</span>
	            </a>
	          </li>
	          <!-- <li class="flex-col-3">
	            <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000002&menuId=MNU_0000000000000028" class="main-board-items">
	              <img src="${CML}/imgs/main/icon_notice_board_03.svg" alt="포토갤러리">
	              <p class="title">포토갤러리</p>
	              <span class="desc">생생한 사진으로 소식을 한 눈에 확인하세요</span>
	            </a>
	          </li> -->
	          <li class="flex-col-3">
	            <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000003&menuId=MNU_0000000000000030" class="main-board-items">
	              <img src="${CML}/imgs/main/icon_notice_board_04.svg" alt="Q&A">
	              <p class="title">Q&A</p>
	              <span class="desc">궁금한 점을 답변해드립니다</span>
	            </a>
	          </li>
              <li class="flex-col-3">
	            <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000024&menuId=MNU_0000000000000131" class="main-board-items">
	              <img src="${CML}/imgs/main/icon_notice_board_05.svg" alt="FAQ">
	              <p class="title">FAQ</p>
	              <span class="desc">자주하시는 질문을 모았습니다</span>
	            </a>
	          </li>
	        </ul>
	      </div>
	    </section>
	    	
	    </c:otherwise>
    </c:choose>
  </div>
</c:when>
<c:otherwise>
	<c:choose>
		<%--특수외국어교육진흥사업 --%>
		<c:when test="${currMpm.upperMenuId eq 'MNU_0000000000000004'}">
			<div class="subtop-full-wrap" style="background-image:url(${CML}/imgs/page/subtop/img_subtop_special_bg_v1.jpg);">
			    <h2 class="subtop-title">특수외국어교육진흥사업</h2>
			    <p class="subtop-desc">전문성과 역량을 갖춘 특수외국어 인재 양성</p>
			    <div class="subtop-menu-group">
			      <div class="area">
			        <ul class="subtop-menu-wrap">
			          <li class="subtop-menu-items <c:if test="${currMpm.menuId eq 'MNU_0000000000000014'}">on</c:if>"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000014">특수외국어란?</a></li>
			          <li class="subtop-menu-items <c:if test="${currMpm.menuId eq 'MNU_0000000000000016'}">on</c:if>"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000016">사업개요</a></li>
			          <li class="subtop-menu-items <c:if test="${currMpm.menuId eq 'MNU_0000000000000018'}">on</c:if>"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000018">5대 핵심사업</a></li>
			          <li class="subtop-menu-items <c:if test="${currMpm.menuId eq 'MNU_0000000000000020'}">on</c:if>"><a href="/msi/cntntsService.do?menuId=MNU_0000000000000020">언어별 주요사업</a></li>
			          <li class="subtop-menu-items <c:if test="${currMpm.menuId eq 'MNU_0000000000000022'}">on</c:if>"><a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000007&menuId=MNU_0000000000000022">K-MOOC</a></li>
			        </ul>
			      </div>
			    </div>
			  </div>
		</c:when>
		<%--외국어시험 --%>
		<c:when test="${currMpm.menuId eq 'MNU_0000000000000010'}">
		  	<!-- sub top-->
			<div class="subtop-full-wrap" style="background-image:url(${CML}/imgs/page/subtop/img_subtop_foreignlang_bg.jpg);">
				<h2 class="subtop-title center-align">Critical Foreign Language Proficiency Test (CFLPT)</h2>
			    <p class="subtop-desc">11개 특수외국어능력을 수준별로 공정하고 균형 있게 평가합니다.</p>
		  	</div>

		  	<div class="area">
			    <!-- bread crumb -->
			    <c:set var="location" value="${fn:split(currMpm.menuPathByName, '>')}"/>
			    <ul class="main-bread-crumb">
			      <li class="item">HOME</li>
			      <c:forEach var="result" items="${location}" varStatus="status">
			      	<li class="item"><c:out value="${result}"/></li>
			      </c:forEach>
			    </ul>
			</div>
		</c:when>
		<%--교재/사전 --%>
		<c:when test="${currMpm.menuId eq 'MNU_0000000000000008'}">
		  	<section class="area">
			    <!-- bread crumb -->
			    <c:set var="location" value="${fn:split(currMpm.menuPathByName, '>')}"/>
			    <ul class="main-bread-crumb">
			      <li class="item">HOME</li>
			      <c:forEach var="result" items="${location}" varStatus="status">
			      	<li class="item"><c:out value="${result}"/></li>
			      </c:forEach>
			    </ul>
		</c:when>
		<%--교육과정 - 비로그인 --%>
		<c:when test="${currMpm.menuId eq 'MNU_0000000000000066' and empty USER_INFO.id}">
			<%--
		  	<section class="area">
			    <!-- bread crumb -->
			    <c:set var="location" value="${fn:split(currMpm.menuPathByName, '>')}"/>
			    <ul class="main-bread-crumb">
			      <li class="item">HOME</li>
			      <c:forEach var="result" items="${location}" varStatus="status">
			      	<li class="item"><c:out value="${result}"/></li>
			      </c:forEach>
			    </ul>
			     --%>
		</c:when>
		<c:otherwise>
			<div class="area">
			    <!-- bread crumb -->
			    <c:set var="location" value="${fn:split(currMpm.menuPathByName, '>')}"/>
			    <ul class="main-bread-crumb">
			      <li class="item">HOME</li>
			      <c:forEach var="result" items="${location}" varStatus="status">
			      	<li class="item"><c:out value="${result}"/></li>
			      </c:forEach>
			    </ul>

			    <!-- 페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
			    <div class="page-container has-sidenav">

			      <!-- 네비게이션영역 -->
			      <div class="sidenav-bar">
			        <!-- lnb -->
			        <div id="lnb" class="lnb">
			          <div class="lnb-heder">
			            <h2 class="title">
			              <c:out value="${currRootMpm.menuNm}"/>

			               	<c:if test="${currMpm.menuId eq 'MNU_0000000000000086' and USER_INFO.userSeCode ne '08'}">
			               		<c:choose>
			               			<c:when test="${myCurriculumCnt ne '0' }">
			               				<p class="state">${USER_INFO.name}의 진행중 과정 <span class="font-700">${myCurriculumCnt}</span>건</p>
			               			</c:when>
			               			<c:otherwise>
			               				<p class="state">진행중 과정이 없습니다.</p>
			               			</c:otherwise>
			               		</c:choose>
				             </c:if>
			              <%--
			              <c:if test="${param.menuId eq 'MNU_0000000000000129' or param.menuId eq 'MNU_0000000000000068' or param.menuId eq 'MNU_0000000000000066' }">
			                <p class="state">${USER_INFO.name}님의 진행중 과정 <span class="font-700">${myCurriculumCnt }</span>건</p>
			              </c:if>
			               --%>
			            </h2>
			          </div>
			          <nav class="lnb-body">
			            <ul class="menu-list-wrap">
			            	<c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
								<c:param name="menuTarget" value="Sub"/>
								<c:param name="menuType" value="mpmList"/>
								<c:param name="seCode" value="${SE_CODE}"/>
							</c:import>
			            </ul>
			          </nav>
			        </div>
			        <!-- //lnb -->
			      </div>

			      <!-- 콘텐츠영역 -->
			      <div class="expansion-panel">
			      	<c:if test="${param.contTitleAt ne 'N'}">
				        <!-- 메인타이틀 -->
				        <div class="main-common-title3 <c:if test="${param.contentLineAt eq 'Y'}">line</c:if>">
				          <h2 class="title">
				            <c:out value="${currMpm.menuNm}"/>
				            <!-- 학생 페이지의 전체, 나의, 관심 교육과정메뉴에서  나의 교육과정 개수 출력 -->
				            <%--
				            <c:if test="${param.menuId eq 'MNU_0000000000000129' or param.menuId eq 'MNU_0000000000000068' or (param.menuId eq 'MNU_0000000000000066' and not empty USER_INFO.id)}">
				              <span class="desc">현재 진행중인 과정 <b>${myCurriculumCnt }건</b></span>
				            </c:if>
				             --%>
				             <c:if test="${currMpm.menuId eq 'MNU_0000000000000086' and USER_INFO.userSeCode ne '08'}">
				             	<span class="desc">총 <b>${scheduleCnt}건</b></span>
				             </c:if>
				          </h2>
						    <div class="util-wrap">
						      <button class="btn-expansion" type="button" title="넓게보기"></button>
						    </div>
				        </div>
			        </c:if>
			        <c:choose>
			        	<c:when test="${param.contentLineAt eq 'Y'}">
			        		<div class="page-content-wrap">
			        	</c:when>
			        	<c:when test="${param.contentLineAt eq 'N'}">

			        	</c:when>
			        	<c:otherwise>
			        		<div class="page-content-wrap">
			        	</c:otherwise>
			        </c:choose>
			          <!-- 콘텐츠바디 -->

		</c:otherwise>
	</c:choose>

	<c:if test="${currMpm.htmlUseAt eq 'Y'}">
		<c:catch var="ex">
			<c:import url="/EgovPageLink.do?link=${MnuFileStoreWebPathByJspFile}${currMpm.siteId}/${currMpm.menuId}${PUBLISH_APPEND_FREFIX}" charEncoding="utf-8"/>
		</c:catch>
		<c:if test="${ex != null}">publishing not found</c:if>
	</c:if>
	<c:if test="${currMpm.cntntsTyCode eq 'CTS05'}">
		<iframe id="cntnsIframe" src="${currMpm.url}" scrolling="no" frameborder="0" width="100%" height="1200"></iframe>
	</c:if>
	<c:if test="${currMpm.cntntsTyCode eq 'CTS06'}">
		<c:catch var="ex">
			<c:import url="${currMpm.url}" charEncoding="utf-8"/>
		</c:catch>
		<c:if test="${ex != null}">포틀릿을 가져오는데 문제가 발생하였습니다.</c:if>
	</c:if>
</c:otherwise>
</c:choose>
</c:if>