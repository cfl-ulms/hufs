<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<%@ page import="java.util.Date" %>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>
<c:set var="INTR" value="/template/introduce"/>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>한국외국어대학교</title>

  <meta name="title" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">
  <meta property="og:url" content="http://cfl.ac.kr/">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
  <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
  
<c:choose>
	<c:when test="${USER_INFO.userSeCode eq '08'}">
  		<link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=3">
		<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/modal_staff.css?v=3">
  		
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">

	</c:when>
	<c:otherwise>
		<link rel="stylesheet" href="${CML}/css/main/staff_main_login.css?v=3">
		
		<link rel="stylesheet" href="${CML}/css/common/common.css?v=3">
  		<link rel="stylesheet" href="${CML}/css/common/board.css?v=1">
  		<link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
  		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
	</c:otherwise>
</c:choose>
  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/main/main_common.css?v=2">
  <link rel="stylesheet" href="${CML}/css/main/main_logout.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=2"></script>
  <script src="${CM}/js/common.js"></script>
  <!--=================================================
          페이지별 스크립트
  ==================================================-->
  <script src="${CML}/js/main/main.js?v=1"></script>
<c:if test="${USER_INFO.userSeCode eq '08'}">
	<style type="text/css">
		.greetings{display: flex;}
		figure{flex: 1; padding: 0 1rem;}
		article{flex: 2; line-height: 1.7; letter-spacing: -0.5px; padding: 0 1rem;}
		.greeting-article{line-height: 1.7; letter-spacing: -0.5px; padding: 2rem 1rem;}
	</style> 
</c:if>
<script type="text/javascript">

$(document).ready(function(){
	<c:if test="${not empty USER_INFO.id and USER_INFO.id ne 'sj1004class@hanmail.net' and USER_INFO.id ne 'stu001@daum.net'}">
		setTimer('59', '59');

		$('.login-time-text').on('click', function(){
			clearTimer();
			setTimer('59', '59');
		});
	</c:if>

	//국가 선택 검색
	$(document).on("click", ".liLang", function() {
		var langId = $(this).data("id");
		
		$(".liLang").removeClass("on");
		$(this).addClass("on");

        $.ajax({
            url:'/lms/crm/curriculumCardListAjax.do',
            type:'post',
            dataType:'html',
            data:{"searchCrclLang" : langId},
            beforeSend : function(){
            	$('#curCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
            }, success:function(data){
                $('#curCard').html(data);
            }
        });
	});
});

//타이머
var timer;
function setTimer(minute, second){
	 // 초기화
	 $(".countTimeMinute").html(minute);
	 $(".countTimeSecond").html(second);

	 timer = setInterval(function () {
	  // 설정
	  $(".countTimeMinute").html(minute);
	  $(".countTimeSecond").html(second);

	  if(minute == 0){
		  //console.log(11);
	  }
	  if(second == 0){
		  //console.log(22);
	  }

	  if(minute < 0){
		  clearInterval(timer);
		  location.href = "<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>";
	  }else{
	   second--;
	   if(second<10){
	    $(".countTimeSecond").html('0' + second);
	   }

	   // 분처리
	   if(second == 0){
	    minute--;
	    second = 59;
	   }
	   if(minute<10){
	    $(".countTimeMinute").html('0' + minute);
	   }

	   	if(minute == 4 && second == 59){
	   		if(confirm("시간연장하시겠습니까?")){
	   			clearTimer();
				setTimer('59', '59');
	   		}
	   	}
	  }
	 }, 1000); /* millisecond 단위의 인터벌 */
}

function clearTimer(){
	clearInterval(timer);
}

function fnCalDate(year, month, day){
	var tmpDt;

	if(month < 10){
		month = "0" + month;
	}
	if(day < 10){
		day = "0" + day;
	}
	tmpDt = year + "-" + month + "-" + day;

	return tmpDt;
}
  
  
</script>
</head>

<body>
  <header class="gnb">
    <div class="top-header area-full">
      <h1>
        <a href="/index.do" class="flex">
          <img src="${CML}/imgs/common/img_header_logo.png" alt="CFL 특수외국어교육진흥사업">
        </a>
      </h1>
      <ul class="user-info-wrap">
        	<c:choose>
      		<c:when test="${not empty USER_INFO.id }">
      			<li class="list profile">
		          <a href="#">
		            <span class="user-img">
		              <b><c:out value="${USER_INFO.name}"/></b>(<c:out value="${USER_INFO.id}"/>)
		            </span>
		          </a>
		        </li>
		        <li class="list login-time">
		          <button type="button" class="cursor-pointer">
		            <span class="login-time-num"><span class="countTimeMinute"></span>:<span class="countTimeSecond"></span></span>
		            <span class="login-time-text">시간연장하기</span>
		          </button>
		        </li>
		        <li class="list">
		          <a href="/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000024&menuId=MNU_0000000000000131">FAQ</a>
		        </li>
		        <li class="list">
		          	<c:choose>
		        		<c:when test="${USER_INFO.userSeCode eq '08' }">
		        			<a href="/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000044&userId=${USER_INFO.id}&userSeCode=${USER_INFO.userSeCode}&type=mypage">MYPAGE</a>
		        		</c:when>
		        		<c:otherwise>
		          			<a href="/uss/umt/cmm/EgovUserUpdateView.do?menuId=MNU_0000000000000044">MYPAGE</a>
		        		</c:otherwise>
		        	</c:choose>
		        </li>
		        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06'}">
		        	<li class="list">
			          <a href="/lms/crm/selectWishCrsList.do?menuId=MNU_0000000000000165">WISHLIST</a>
			        </li>
		        </c:if>
		        <li class="list">
		          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
		        </li>
		        <li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectLogoutUrl()%>">LOGOUT</a>
		        </li>
      		</c:when>
      		<c:otherwise>
      			<li class="list">
		          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
		        </li>
      			<li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>">JOIN</a>
		        </li>
		        <li class="list">
		          <a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>">LOGIN</a>
		        </li>
      		</c:otherwise>
      	</c:choose>
      </ul><!-- user-info-wrap -->
    </div><!-- top-header -->

    <!-- nav -->
    <nav class="nav-wrap">
      <div class="nav-wrap-inner area-full">
        <ul class="nav-list-wrap">
            <c:import url="/msi/introduce/menuService.do" charEncoding="utf-8">
			<c:param name="menuTarget" value="Main"/>
			<c:param name="menuType" value="Simple2Depth"/>
			<c:param name="seCode" value="${SE_CODE}"/>
		  </c:import>
        </ul>
      </div><!-- nav-wrap-inner -->
      <div class="nav-bg"></div>
    </nav><!-- nav -->
  </header>

  <div class="area">
	  <!-- bread crumb -->
	  <ul class="main-bread-crumb">
		  <li class="item">HOME</li>
		  <li class="item">진흥원 소개</li>
		  <li class="item">원장님 인사말</li>
	  </ul>
	  <!-- 페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
	  <div class="page-container has-sidenav">
		  <!-- 네비게이션영역 -->
		  <div class="sidenav-bar">
			  <!-- lnb -->
			  <div id="lnb" class="lnb">
				  <div class="lnb-heder">
					  <h2 class="title">진흥원 소개</h2>
				  </div>
				  <nav class="lnb-body">
					  <ul class="menu-list-wrap">
						<li class="list depth-1 active">
							<a href="/msi/introduce/introduce.do?menuId=MNU_0000000000000177">원장님 인사말</a>
						</li>
						<li class="list depth-1">
							<a href="/msi/introduce/introduce.do?menuId=MNU_0000000000000174">사업단 조직</a>
						</li>
					  </ul>
				  </nav>
			  </div>
			  <!-- //lnb -->
		  </div>
		  <!-- 콘텐츠영역 -->
		  <div class="expansion-panel">
			  <!-- 메인타이틀 -->
			  <div class="main-common-title3 ">
				  <h2 class="title"> 원장님 인사말</h2>
				  <div class="util-wrap">
					  <button class="btn-expansion" type="button" title="넓게보기"></button>
				  </div>
			  </div>
			  <div class="page-content-wrap">
				  <!-- 콘텐츠바디 -->
				  <section class="page-content-body">
					  <div class="greetings">
						  <figure>
							  <img src="${INTR}/imgs/greetings.jpg" alt="introduce" />
						  </figure>
						  <article>
							  안녕하십니까.</br>
							  한국외국어대학교 특수외국어교육진흥원 원장 오종진 교수입니다.</br></br>
							  한국외대 특수외국어교육진흥원은 “특수외국어를 배우려는 국민들에게 다양하고 전문적인 교육 기회를 제공하고 특수외국어 구사 능력을 갖춘 인재를 양성하여 국가경쟁력 강화에 이바지함”을 목적으로 하는 ‘특수외국어교육진흥에 관한 법률’(2016년 2월3일제정)에 따라 특수외국어 교육의 진흥에 필요한 교육플랫폼을 조성하기 위해 설립된 기관입니다.</br></br>
							  ‘특수외국어교육진흥에 관한 법률’ 제 2조에 따르면, 특수외국어는 국가발전을 위하여 전략적으로 필요한 외국어로서 대통령령으로 정하는 언어를 말하는데, 우리나라에서는 총 53개의 언어가 지정되었습니다. 현재 특수외국어 사업의 1단계에서는 15개의 언어가 선정되었고 이들 중 한국외국어대학교 특수외국어교육진흥원은 11개의 특수외국어 관련 사업을 진행하고 있습니다. 한국외국어대학교는 국내 및 아시아 최대 외국어대학교로서 현재 45개의 언어를 교육하고 있기에 향후 그 확장성은 더 크다고 할 수 있습니다.
						  </article>
					</div>
					<div class="greeting-article">
						4차 산업혁명 시대를 맞아 다변화된 해외시장과 복잡해진 글로벌 환경에서 외국어는 더 이상 단순한 의사소통 도구가 아닌 국익과 안보를 지키는 전략 자산이라 생각합니다. 이런 측면에서 특수외국어는 국가 입장에서 전략지역 언어라고 할 수 있습니다. 특히 수출 기반의 경제구조로 되어 있는 우리나라는 입장에서는 외국어를 기반으로 하는 지역전문가 양성은 국가의 미래와도 직결된 문제라 생각됩니다. 나아가 해당 지역과 교류와 협력을 원하는 민간 부분에서도 특수어의 중요성은 더욱 강조되고 있는 실정입니다.</br></br>
						특수외국어(Critical Foreign Language) 교육은 이미 미국, 유럽연합, 영국, 일본 등 주요 국가에서도 국가경쟁력 강화와 국가 안보 차원 등에서 다양한 정책과 제도를 통해 운영되고 있습니다. 약간의 늦은 감은 있지만, 한국도 이와 같은 문제의식을 배경으로 특수외국어교육에 관심을 가지고 다양한 사업을 진행하고 있는 것은 매우 고무적인 일이라 생각됩니다.</br></br>
						2018년 교육부와 국립국제교육원이 선정한 특수외국어 교육진흥전문교육기관으로 지정된 한국외대 특수외국어교육진흥원은 설립목적과 취지에 맞게 학부교육 내실화와 전문인력양성, 저변확대 및 인프라 구축이라는 특수어 관련 대국민 사업을 활발하게 추진하고 있습니다.</br></br>
						언어를 배운다는 것은 그 나라에 관한 관심과 애정으로 시작하여 언어로 소통하지만 결국은 사람을 배워가는 과정이라 생각합니다. 언어와 문화에 대한 이해는 우리의 우방을 넓히고 상호 간의 이해를 증진하며 서로 협력하는 새로운 세상을 가져다줄 것이라고 믿습니다. 이러한 의미에서 한국외대 특수외국어교육진흥원은 한국 내 특수외국어 교육활성화는 물론 관련 국가들과의 관계발전과 상호협력의 기회를 더욱 확대해 나가기 위해 노력을 다하겠습니다.</br></br>
						<c:choose>
							<c:when test="${USER_INFO.userSeCode eq '08'}">
								<p style="font-size: 1.5rem; text-align: right; font-style: italic; padding: 1rem 0;">한국외대 특수외국어교육진흥원, 원장 오종진.</p>
							</c:when>
							<c:otherwise>
								<p>한국외대 특수외국어교육진흥원, 원장 오종진.</p>
							</c:otherwise>
						</c:choose>
					</div>
				  </section>
			  </div>
			  <!-- //콘텐츠바디 -->
		  </div>
		  <!-- 콘텐츠영역 -->
	  </div>
	  <!-- //페이지콘텐츠 전체영역 (LNB추가되면 감싸주는 역할) -->
  </div>
  
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="isMain" value="${isMain}"/>
</c:import>

</body>

</html>