<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:if test="${param.popupAt eq 'Y'}">
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>약관 및 개인정보취급방침 동의 안내</title>

  <meta name="title" content="약관 및 개인정보취급방침 동의 안내">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="약관 및 개인정보취급방침 동의 안내">
  <meta property="og:description" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="">
  
  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

	<link rel="stylesheet" href="${CML}/css/common/base.css?v=1">

	<link rel="stylesheet" href="${CML}/css/common/common.css?v=1">
	<link rel="stylesheet" href="${CML}/css/common/board.css?v=1">
	<link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">
	<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>
  <script src="${CM}/js/common.js"></script>
  <style>
  .alert-modal{position:relative;}
  .alert-modal .term-wrap .term-content .text{height:auto;}
  
  @media print {
	.btn-box{display:none}
  }
  </style>
  <script>
  $(document).ready(function(){
	  //인쇄
	  $(".btn-print").click(function(){
		  window.print();
		  return false;
	  });
  });
  </script>
</head>
<body>
	<div class="alert-modal" style="display:block;background:#fff;">
		<div class="btn-box right-align mt-50">
          <a href="#" class="btn-xl btn-outline-gray btn-print">인쇄</a>
        </div>
		<div class="title" style="padding:20px 0;text-align:center;">교육과정 수강신청 시 약관 및 개인정보취급방침 동의서</div>
</c:if>

		<ul class="term-wrap mt-20">
            <li class="innerList">
              <div class="term-head">
                <label class="checkbox">
                  <input type="checkbox" class="provisionCheck">
                  <span class="custom-checked"></span>
                  <span class="text">특수외국어교육진흥사업 약관동의(필수)</span>
                </label>
              </div>
              <div class="term-content">
                <div class="text">
                  <div class="inner-top">
                    특수외국어교육진흥원의 교육 및 서비스를 이용해주셔서 감사합니다. 특수외국어교육진흥원 홈페이지는 구성원들 간의 네트워크를 기반으로 다양한 교육 서비스를 제공하기 위해 운영되는
                    공간입니다. 구성원들
                    간의 원활한 커뮤니티 운영을 위하여 홈페이지 이용약관을 준수해 줄 것을 당부 드립니다.
                  </div>
                  <div class="inner-desc">
                    <b>1. 이 약관은 ‘약관동의’ 버튼을 클릭함으로써 효력이 발생됩니다.</b>
                    <b>2. 약관에 동의하는 것은 정기적으로 홈페이지를 방문하여 약관의 변경사항을 확인하는 것을 필요로 합니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 이용자의 피해는 학교에서
                      책임지지
                      않습니다.</b>
                    <b>3.변경된 약관에 동의하지 않을 경우, ‘서비스’ 이용을 중단하고 탈퇴할 수 있습니다. 약관이 변경된 이후에도 계속적으로 ‘서비스’를 이용하는 경우에는 약관의 변경 사항에 동의한
                      것으로
                      간주합니다.</b>
                    <b>4. 위에 기재되지 않은 기타 약관 관련 세부사항은 홈페이지 이용약관을 준용하여 처리됩니다.</b>
                  </div>
                </div>
              </div>
            </li>
            <li class="innerList">
              <div class="term-head">
                <label class="checkbox">
                  <input type="checkbox" class="infoCheck">
                  <span class="custom-checked"></span>
                  <span class="text">특수외국어교육진흥사업 개인정보취급방침 (필수)</span>
                </label>
              </div>
              <div class="term-content">
                <div class="text">
                  <div class="inner-top">
                    한국외국어대학교 특수외국어교육진흥원(이하 ‘특교원’)은 교육 서비스 이용자의 개인정보보호 및 권익을 보호하기 위하여 다음과 같은 처리방침을 두고 있습니다.
                  </div>
                  <div class="inner-desc">
                  	<b class="mb-5">1. 개인정보의 수집 및 이용에 관한 사항</b>
                    <b class="mb-5">1.1. 개인정보의 수집 및 이용 목적</b>
                    <ul>
                      <li>
                        - 학사 업무 처리 및 서비스 제공<br>
                        프로그램 수강생 선발, 성적 관리, 특수외국어평가시험 응시자 관리, 사업비 지급, 장학생 선발, 증명서 발행 및 기타 사업 관리 업무
                      </li>
                    </ul>

                    <b class="mb-5">1.2. 수집하는 개인정보의 항목</b>
                    <ul>
                      <li>특교원은 서비스 제공을 위해 필요한 최소한의 범위 내에서 다음과 같은 개인정보를 수집하고
                        있습니다.</li>
                      <li class="dot">성명, 주민등록번호 등의 고유식별번호, E-mail, 전화번호, 생년월일, 교육과정 이력, 계좌번호 </li>
                      <li class="dot">한국외국어대학교 학생의 경우 학번, 소속, 학적사항 추가 </li>
                    </ul>

                    <b class="mb-5">1.3. 개인정보의 처리 및 보유기간 : 사업종료 후 5년 간</b>
                    <ul>
                      <li>
                        이용자의 개인정보는 수집 시에 동의받은 개인정보 보유·이용기간 내에서 개인정보를 처리·보유하며, 개인정보의 처리 목적이 달성되면 즉시 파기합니다. 다만, 학칙이나 기타 법률에
                        의해 이용자의
                        개인정보를 보존해야할 필요가 있는 경우에는 해당 법률의 규정을 따릅니다.
                      </li>
                    </ul>

                    <b class="mb-5">1.4. 동의를 거부할 권리 고지 및 동의 거부에 따른 불이익</b>
                    <ul>
                      <li>
                        이용자는 특교원에서 필수로 수집하는 개인정보에 대해 동의를 거부할 권리가 있으며,
                        필수항목에 대한 동의 거부 시에는 수강신청 및 이용이 제한됩니다.
                      </li>
                    </ul>

                    <b>1.5. 위에 기재되지 않은 개인정보 관련 기타 사항은 홈페이지 개인정보취급방침을 준용하여 처리됩니다.</b>
                    
                    <b class="mb-5">2. 개인정보 제공에 관한 사항</b>
                    <b class="mb-5">2.1. 개인정보를 제공받는 자</b>
                    <ul>
                      	<li class="dot">법령상 의무이행을 위한 중앙행정기관, 지방자치단체 외</li>
                      	<li class="dot">※ 교육부, 고용노동부, 국세청(세무서), 수사기관, 감사원 외</li>
                    </ul>
                    
                    <b class="mb-5">2.2. 제공받는 자의 개인정보 이용목적</b>
                    <ul>
                      	<li>법령상 의무이행</li>
                    </ul>
                    
                    <b class="mb-5">2.3. 제공하는 개인정보의 항목</b>
                    <ul>
                      	<li>수집·이용에 동의한 정보 중 해당 기관 및 단체의 업무수행, 법령상 의무이행을 위하여 필요한 정보에 한함</li>
                    </ul>
                    
                    <b class="mb-5">2.4. 제공받는 자의 개인정보 보유 및 이용기간</b>
                    <ul>
                      	<li>제공된 날로부터 제공된 목적을 달성할 때까지 보유·이용됨</li>
                    </ul>
                    
                    <b class="mb-5">2.5. 개인정보 제공 동의 거부 권리 및 동의 거부 시 불이익</b>
                    <ul>
                      	<li>위 개인정보 제공에 대한 동의는 사업 참여 및 관리를 위해 필수적이므로, 위 사항에 동의하지 않을 경우 사업에 참여하실 수 있습니다.</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            
            <c:if test="${param.popupAt ne 'Y'}">
	            <li class="innerList">
	                <div class="" style="padding:22px 0px;color:red;font-size:13px;">현재 과정을 수강 중이거나 다른 과정 수강을 신청한 경우 수강 시간 중복 여부를 반드시 <br />체크하시고 신청해 주시기 바랍니다. 본인의 부주의로 인한 수강 등록 취소의 경우 등록금 환불이  <br />불가능할 수 있습니다.</div>
	            </li>
            </c:if>
          </ul>
	
<c:if test="${param.popupAt eq 'Y'}">
	</div>
</body>
</html>
</c:if>