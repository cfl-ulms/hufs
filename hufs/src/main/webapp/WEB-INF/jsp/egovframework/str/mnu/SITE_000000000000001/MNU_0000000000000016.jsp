<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section class="section-gap special-common">
<div class="area">
<div class="page-common-title">
<h2 class="title center-align">사업개요</h2>
</div>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">비전</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">특수외국어 전문 창의융합 인재 양성을 통한 국가경쟁력 강화</h4>
<div class="contnet-desc">우리 대학은 어문학과 지역학이라는 고유 가치를 심화하고 그에 기반한 융복합 인재를 양성하며, 45개의 전문화된 외국어 교육을 바탕으로 인문, 사회, 자연과학, 이공학을 아우르는 세계 수준의 글로벌 융복합을 선도하고 있으며, 대한민국의 특수외국어 교육 진흥을 위해 &ldquo;글로벌 시대를 선도하는 세계 3대 특수외국어 교육 대학&rdquo;으로 성장하고자 합니다.</div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">사업목표 및 내용</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">전문성과 역량을 갖춘 특수외국어 인재 양성 및 교육저변 확대</h4>
<div class="contnet-desc"><img class="mt-20" src="../../../template/lms/imgs/page/special/img_01.jpg" alt="HUFS,특수외국어 전문교육기관 : 학교교육 내실화, 창의적 스마트 교육 플랫폼, 교육저변확대 및 인프라 구축, 공유개방형 글로컬 네트워크, 수요기반의 인력양성 및 활용, 지속 가능한 전문적 맞춤교육" /></div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">추진전략 및 핵심과제</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">4대 추진전략(4S Strategies)</h4>
<div class="contnet-desc"><img class="mt-20" src="../../../template/lms/imgs/page/special/img_02.jpg" alt="1.내실화(Strengthening):교원학생역량, 학사제도, 교육환경시설 / 2.표준화(Standardization): 표준교과과정, 표준공통교재, 평가인증시스템 / 3. 전문화(Specialization): 학문후속세대, 통번역전문인력, 기업실무인력 / 4.공유화(Sharing): 일반인 교육, Language-MOOC,국내외 네트워크" /></div>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">사업기반</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">45개 외국어 전공 기반 HUFS교육 인프라 및 전문성 + 타 전문 교육기관과의 협업</h4>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">사업목표</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">세부목적</h4>
<div class="contnet-desc">
<h5 class="title icon-num">특수외국어 교육진흥을 위한 표준화 기반 학부교육 내실화 및 평가인증 공인화</h5>
<h5 class="title icon-num">사회수요기반 인력양성 및 활용 확대</h5>
<h5 class="title icon-num">특수외국어 교육 저변 확대 및 네트워크 구축</h5>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">우리대학 특수외국어 교육모델</h4>
<div class="contnet-desc">
<h5 class="title icon-num">학문후속, 통번역 및 기업실무 등 다양한 수요 맞춤형 인재 양성을 위한 교육</h5>
<h5 class="title icon-num">특수외국어를 배우려는 일반인들에게 체계적이고 전문적인 교육과 학습 기회 제공</h5>
<h5 class="title icon-num">단계적 교육과정, 표준교재 개발 및 교과 개편을 통한 특수외국어 교육의 표준화</h5>
</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">세계 최고 수준의 특수외국어 교육&middot;연수&middot;평가 체계 구축</h4>
<!-- 테이블영역-->
<table class="common-table-wrap border-white size-sm"><colgroup> <col class="bg-deep-gray" /> <col class="bg-gray" style="width: 28.2%;" /> <col class="bg-gray" style="width: 28.2%;" /> <col class="bg-gray" style="width: 28.2%;" /> </colgroup>
<tbody>
<tr class="left-align ">
<th class="font-700 center-align" scope="row">사업영역</th>
<td>교육과정 및 평가인증</td>
<td>학문후속 및 통번역 인력 양성</td>
<td>네트워크 구축 및 교육 지원</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" scope="row">세부목표</th>
<td class="vertical-top">특수외국어 교육 진흥 위한 교육 내실화 및 평가 공인화</td>
<td class="vertical-top">사회적 수요기반 인력양성 및 활용 확대</td>
<td class="vertical-top">특수외국어 교육 저변 확대 및 네트워크 활성화</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" scope="row">중점과제</th>
<td>교육 및 평가 시스템 혁신</td>
<td>글로컬 전문 교육 시스템 구축</td>
<td>공유형 협력 플랫폼 구축</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" scope="row">핵심사업</th>
<td class="wide-spacing vertical-top">- 세계적 수준의 인력풀 구축<br />- 선진형 학부교육 시스템 구축<br />- 스마트형 교육환경 구축<br />- 아시아언어 공통기준 개발<br />- CFLPT 개발 및 국가 공인화</td>
<td class="wide-spacing vertical-top">- 특수외국어 연구인력 양성<br />- 특수외국어 통번역 인력 양성<br />- 기업수요 맞춤형 실무인력 양성<br />- 인접어 연구 및 교육과정 신설</td>
<td class="wide-spacing vertical-top">- Language-MOOC 구축<br />- 특수외국어 스마트교육 거점화<br />- 특수외국어 교육 플랫폼 공유<br />- 청소년 대상 진로탐색 지원<br />- 특수외국어 연구 아젠다 선도</td>
</tr>
</tbody>
</table>
</div>
</div>
</article>
</div>
</section>