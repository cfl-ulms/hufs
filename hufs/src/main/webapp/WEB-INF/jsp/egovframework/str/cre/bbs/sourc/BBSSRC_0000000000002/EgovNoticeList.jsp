<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="TEMPLATE_PATH" value="${IS_MOBILE ? 'mbl' : 'web'}"/>
<c:set var="_WEB_FULL_PATH" value="http://${siteInfo.siteUrl}"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
	<c:if test="${not empty searchVO.viewType}"><c:param name="viewType" value="${searchVO.viewType}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
			<c:param name="tableCssAt" value="Y"/>
		</c:import>
	</c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>
		
<script type="text/javascript" src="${_C_JS}/board.js" ></script>
<script type="text/javascript">

<c:if test="${!empty brdMstrVO.ctgrymasterId}">		
	var boardCateLevel = ${boardCateLevel};
	var boardCateList = new Array(${fn:length(boardCateList)});
	<c:forEach var="cate" items="${boardCateList}" varStatus="status">
		boardCateList[${status.index}] = new ctgryObj('<c:out value='${cate.upperCtgryId}'/>', '<c:out value='${cate.ctgryId}'/>', '<c:out value='${cate.ctgryNm}'/>', <c:out value='${cate.ctgryLevel}'/>);
	</c:forEach>		
</c:if>

function fn_egov_addNotice(url) {
   	<c:choose>
		<c:when test="${not empty USER_INFO.id}">
			location.href = url;
		</c:when>
		<c:otherwise>
			if (confirm('로그인 하시겠습니까?')) {
				location.href = "<%=egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper.getRedirectLoginUrl()%>";	
			}
		</c:otherwise>
	</c:choose>
   }

$(document).ready(function(){
	//페이징 버튼
	$(".start, .prev, .next, .end").click(function(){
 		var url = $(this).data("url");
 		
 		location.href = url;
 	});
	
});
</script>		
<div class="page-content-wrap">
    <section class="page-content-body">
      <div class="area">
	    <article class="content-wrap">
	    	<div class="content-header">
	          <div class="title-wrap">
	            <div class="title">CFLPT 안내</div>
	          </div>
	        </div>
	    
	    	<div class="content-body">
	    		<!-- 테이블영역-->
	            <table class="common-table-wrap table-type-board">
	                <colgroup>
	                  <col class="" style="width:7%">
	                  <col class="">
	                  <col class="" style="width:14%">
	                </colgroup>
	                <thead>
	                  <tr class="bg-light-gray font-700">
	                    <th scope="col" class="">No</th>
	                    <th scope="col" class="">제목</th>
	                    <th scope="col" class="">등록일</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:forEach var="result" items="${resultList}" varStatus="status">
							<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
							  	<c:param name="nttNo" value="${result.nttNo}" />
							  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
						    </c:url>
							<tr <c:if test="${result.useAt eq 'N'}">class="deleted"</c:if>>
								<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
								<td class="title">
									<div class="inner-wrap">
										<span class='text dotdotdot'>
											<c:choose>
												<c:when test="${SE_CODE eq '10'}"><a href="<c:out value="${viewUrl}"/>"><c:out value="${result.nttSj}" /></a></c:when>
												<c:when test="${result.othbcAt eq 'N' and USER_INFO.id ne result.frstRegisterId}">
													<c:out value="${result.nttSj}" />
												</c:when>
												<c:when test="${SE_CODE < brdMstrVO.inqireAuthor}">
													<c:out value="${result.nttSj}" />
												</c:when>
												<c:otherwise><a href="<c:out value="${viewUrl}"/>"><c:out value="${result.nttSj}" /></a></c:otherwise>
											</c:choose>
										</span>
										<c:if test="${not empty result.atchFileId}"><i class='icon-clip ml-10'></i></c:if>
									</div>
								</td>
								<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
							</tr>
						</c:forEach>
			
					<c:if test="${fn:length(resultList) == 0}">
				    	<tr class="empty"><td colspan="3"><spring:message code="common.nodata.msg" /></td></tr>
				    </c:if>
		    
				</tbody>
			</table>
			
			<div class="pagination center-align">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="${_PREFIX}/selectBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
				</div>
			</div>
		</div>
		<!-- //게시판 -->
	  </article>
	</div>
</section>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>