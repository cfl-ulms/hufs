<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="_C_CSS" value="/template/common/css"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}${brdMstrVO.tmplatId }/images"/>
<c:set var="_C_LIB" value="/lib"/>

<c:set var="CML" value="/template/lms"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${pageContext.request.contextPath}${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${pageContext.request.contextPath}${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${pageContext.request.contextPath}${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>


<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="BBS_TMPLATID" value="${brdMstrVO.tmplatId }"/>
	<c:param name="tableCssAt" value="Y"/>
</c:import>

<script src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script src="${_C_LIB}/upload/upload.js?v=1" ></script>
<script src="${_C_JS}/board.js?v=1" ></script>

<link rel="stylesheet" href="${_C_LIB}/jquery-ui-1.12.1.custom/jquery-ui.css">
<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script src="${_C_JS}/board.js" ></script>
<script>
function fn_egov_regist() {
	
	tinyMCE.triggerSave();
	
	<c:if test="${searchVO.registAction ne 'reply'}">
		if($.trim($('#${_EDITOR_ID}').val()) == "") {
			alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
			tinyMCE.activeEditor.focus();
			return false;
		}
	</c:if>
	
	$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());
	
	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
		for(var cmIdx = 1 ; cmIdx <= boardCateLevel ; cmIdx++){
			var cmObj = document.getElementById("ctgry" + cmIdx);
			if(cmObj != null) {
				if(fn_egov_SelectBoxValue("ctgry" + cmIdx) != '') {
					document.board.ctgryId.value = fn_egov_SelectBoxValue("ctgry" + cmIdx);
				}
			}
		}
    </c:if>

    <c:choose>
    	<c:when test="${searchVO.registAction eq 'updt'}">
			if (!confirm('<spring:message code="common.update.msg" />')) {
				 return false
			}
		</c:when>
		<c:otherwise>
			if (!confirm('<spring:message code="common.regist.msg" />')) {
				return false;
			}
		</c:otherwise>
	</c:choose>
}

<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
	var boardCateLevel = <c:out value='${boardCateLevel}'/>;
	var boardCateList = new Array(${fn:length(boardCateList)});
	<c:forEach var="cate" items="${boardCateList}" varStatus="status">
		boardCateList[${status.index}] = new ctgryObj('<c:out value='${cate.upperCtgryId}'/>', '<c:out value='${cate.ctgryId}'/>', '<c:out value='${cate.ctgryNm}'/>', <c:out value='${cate.ctgryLevel}'/>);
	</c:forEach>
</c:if>


$(document).ready( function() {
	var adfile_config = {
			siteId:"<c:out value='${brdMstrVO.siteId}'/>",
			pathKey:"Board",
			appendPath:"<c:out value='${brdMstrVO.bbsId}'/>",
			editorId:"${_EDITOR_ID}",
			fileAtchPosblAt:"${brdMstrVO.fileAtchPosblAt}",
			maxMegaFileSize:${brdMstrVO.posblAtchFileSize},
			atchFileId:"${board.atchFileId}"
		};
		
	fnCtgryInit('<c:out value='${board.ctgryPathById}'/>');
	fn_egov_bbs_editor(adfile_config);
});
</script>

<link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">

	<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
		<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
		<input type="hidden" name="cal_url" value="<c:url value='/sym/cmm/EgovNormalCalPopup.do'/>" />
		<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
        <input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
        <input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
		<input type="hidden" name="bbsId" value="<c:out value='${brdMstrVO.bbsId}'/>" />
		<input name="menuId" type="hidden" value="<c:out value='${searchVO.menuId}'/>" />
		<input type="hidden" name="registAction" value="<c:out value='${searchVO.registAction}'/>"/>
		<input type="hidden" name="tmplatImportAt" value="<c:out value='${searchVO.tmplatImportAt}'/>"/>
		       
		<form:hidden path="nttNo"/>
		<form:hidden path="ctgryId"/>
		<%-- 
		<form:hidden path="ordrCode"/>
		<form:hidden path="ordrCodeDp"/>
		 --%>
		<form:hidden path="atchFileId"/>
		
		<c:choose>
			<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
				
			</c:when>
			<c:otherwise>
			<section class="page-content-body">
            <article class="content-wrap">
            
            	<table class="common-table-wrap table-style2 mb-20">
                <tbody>
                 <tr>
                    <th class="title">강좌명</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-12">
                          <input type="text" id="nttSj" name="nttSj" class="table-input" value="" placeholder="강좌명을 입력해주세요.">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">강좌표기</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-12">
                          	<input name="file_1" id="egovComFileUploader" type="file" class="inp" /><br/>
							<span>권장사이즈 395 * 240</span>
							<c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
						    	<c:param name="atchFileId" value="${board.atchFileId}" />
						    	<c:param name="mngAt" value="Y"/>
						    	<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
						    	<c:param name="siteId" value="${brdMstrVO.siteId}"/>
						    	<c:param name="width" value="395"/>
						    	<c:param name="height" value="240"/>
							</c:import>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">강좌기간</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-12">
                         	<input type="text" name = "tmp01" id="searchBgnDe" class="ell date datepicker type2" placeholder="시작일" value="<c:out value="${searchVO.searchBgnDe}"/>">
                      		<i>~</i>
                      		<input type="text" name = "tmp02" id="searchEndDe" class="ell date datepicker type2" placeholder="종료일" value="<c:out value="${searchVO.searchEndDe}"/>">
                        </div>
                      </div>
                    </td>
                  </tr>
                  
                  <tr>
                    <th class="title">강좌운영기관</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-12">
                          <input type="text" id="tmp03" name="tmp03" class="table-input" value="" placeholder="기관을 입력해주세요.">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">K_MOOC URL</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-12">
                          <input type="text" id="tmp05" name="tmp05" class="table-input" value="" placeholder="URL을 입력해주세요.">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">학습자료로 공개여부</th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <label class="checkbox circle">
                            <input type="radio" name="tmp06" value="Y" class="table-checkbox" checked>
                            <span class="custom-checked"></span>
                            <span class="text">공개</span>
                          </label>
                        </div>
                        <div class="flex-col-auto">
                          <label class="checkbox circle">
                            <input type="radio" name="tmp06" value="N" class="table-checkbox">
                            <span class="custom-checked"></span>
                            <span class="text">비공개</span>
                          </label>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div>
              	<form:textarea path="nttCn" rows="20" cssClass="cont"/><form:errors path="nttCn" />
              </div>
            </article>
          </section>
			</c:otherwise>
		</c:choose>
		

		<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y' and searchVO.registAction ne 'reply'}">
		<div class="mt-20">
			<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
				<c:param name="editorId" value="${_EDITOR_ID}"/>
				<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
		    	<c:param name="param_atchFileId" value="${board.atchFileId}" />
		    	<c:param name="imagePath" value="${_IMG }"/>
		    	<c:param name="regAt" value="Y"/>
		    	<c:param name="commonAt" value="Y"/>
			</c:import>
		</div>
		</c:if>
		<%-- 
		<div id="bbs_wrap">
			<table class="bbs_type">
				<colgroup>
					<col style="width:20%"/>
					<col style="width:80%"/>
				</colgroup>
				<tbody>
					<c:choose>
						<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
							
							<tr>
								<td colspan="2"><c:out value="${board.nttSj}"/></td>
							</tr>
							<tr>
								<td colspan="2"><c:out value="${board.nttCn}" escapeXml="false"/></td>
							</tr>
							<tr>
								<th>
									<label for="ftext"><spring:message code="cop.processSttus" /></label>
								</th>
								<td>
									<select name="processSttusCode" id="ftext" class="select">
										<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
											<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}"/></option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							
						</c:otherwise>
					</c:choose>
					
				</tbody>
			</table>
		</div>
 	 	--%>
 	 	
 	 	
 	 	<!-- 하단버튼 -->
		<div class="page-btn-wrap mt-20">
			<div class="left-area">
				<c:url var="selectBoardListUrl" value="${_PREFIX}/selectBoardList.do">
				    <c:param name="menuId" value="${searchVO.menuId}" />
			        <c:param name="bbsId" value="${brdMstrVO.bbsId}" />
			        <c:param name="pageIndex" value="${searchVO.pageIndex}" />
					<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
					<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
					<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
					<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>	
				</c:url>
              <a href="${selectBoardListUrl}" class="btn-sm btn-outline-gray font-basic">목록으로</a>
            </div>
            <div class="right-area">
              <!-- <button href="#" class="btn-sm btn-outline-gray font-basic">취소</button> -->
              <button href="#" class="btn-sm btn-point">
              		<c:choose>
						<c:when test="${searchVO.registAction eq 'regist' and SE_CODE >= brdMstrVO.registAuthor}"><spring:message code="button.create"/></c:when>
						<c:when test="${searchVO.registAction eq 'updt' and SE_CODE >= brdMstrVO.registAuthor}"><spring:message code="button.update"/></c:when>
						<c:when test="${searchVO.registAction eq 'reply' and SE_CODE >= brdMstrVO.registAuthor}"><spring:message code="button.reply"/></c:when>
					</c:choose>	
              </button>
            </div>
			
		</div>	
	</form:form>
</div>

<c:choose>
	<c:when test="${searchVO.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
	</c:when>
	<c:otherwise>
		</body>
		</html>
	</c:otherwise>
</c:choose>