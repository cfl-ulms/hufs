<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<section class="section-gap special-common">
<div class="area">
<div class="page-common-title">
<h2 class="title center-align">특수외국어란?</h2>
</div>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">특수외국어</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">특수외국어란?</h4>
<div class="contnet-desc">특수외국어란 국가 발전을 위하여 전략적으로 필요한 외국어로서 대통령령으로 정한 언어를 말합니다.</div>
<div class="contnet-desc">2019년 현재 총 53개의 언어가 특수외국어로 지정되어 있습니다.</div>
</div>
<div class="content-group"><!-- 테이블영역-->
<table class="common-table-wrap border-white size-sm"><colgroup> <col class="bg-deep-gray" style="width: 15%;" /> <col class="bg-gray" style="width: 25%;" /> <col class="bg-gray" /> </colgroup>
<tbody>
<tr class="left-align ">
<th class="font-700 center-align" rowspan="12" scope="row">중동&middot;아프리카 <br />(12개 언어)</th>
<td>아랍어</td>
<td>레바논, 리비아, 모로코, 모리타니아, 바레인, 사우디아라비아, 소말리아, 수단, 시리아, 아랍에미리트, 알제리, 에리트리아, 예멘, 오만, 요르단, 이라크, 이스라엘, 이집트, 지부티, 차드, 카타르, 코모로, 쿠웨이트, 튀니지, 팔레스타인</td>
</tr>
<tr class="left-align ">
<td class="vertical-top">터키어</td>
<td class="vertical-top">사이프러스, 터키</td>
</tr>
<tr class="left-align ">
<td>아제르바이잔어</td>
<td>아제르바이잔</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">이란어</td>
<td class="wide-spacing vertical-top">이란</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">파슈토어</td>
<td class="wide-spacing vertical-top">아프가니스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">다리어</td>
<td class="wide-spacing vertical-top">아프가니스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">히브리어</td>
<td class="wide-spacing vertical-top">이스라엘</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">하우사어</td>
<td class="wide-spacing vertical-top">가나, 나이지리아, 니제르</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">스와힐리어</td>
<td class="wide-spacing vertical-top">우간다, 케냐, 탄자니아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">줄루어</td>
<td class="wide-spacing vertical-top">남아프리카공화국</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">르완다어</td>
<td class="wide-spacing vertical-top">르완다</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">암하라어</td>
<td class="wide-spacing vertical-top">에티오피아</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" rowspan="7" scope="row">유라시아 <br />(7개 언어)</th>
<td>카자흐어</td>
<td>카자흐스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">우즈베크어</td>
<td class="wide-spacing vertical-top">우즈베키스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">키르기스어</td>
<td class="wide-spacing vertical-top">키르기즈</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">우크라이나어</td>
<td class="wide-spacing vertical-top">우크라이나</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">투르크멘어</td>
<td class="wide-spacing vertical-top">투르크메니스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">타지키스탄어</td>
<td class="wide-spacing vertical-top">타지키스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">몽골어</td>
<td class="wide-spacing vertical-top">몽골</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" rowspan="14" scope="row">인도&middot;아세안 <br />(14개 언어)</th>
<td>힌디어</td>
<td>인도, 피지</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">우르두어</td>
<td class="wide-spacing vertical-top">파키스탄</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">산스크리트어</td>
<td class="wide-spacing vertical-top">네팔, 인도</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">네팔어</td>
<td class="wide-spacing vertical-top">네팔</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">벵골어</td>
<td class="wide-spacing vertical-top">방글라데시</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">신할리즈어</td>
<td class="wide-spacing vertical-top">스리랑카</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">베트남어</td>
<td class="wide-spacing vertical-top">베트남</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">말레이시아어</td>
<td class="wide-spacing vertical-top">말레이시아, 싱가포르, 브루나이다루살람</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">인도네시아어</td>
<td class="wide-spacing vertical-top">인도네시아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">태국어</td>
<td class="wide-spacing vertical-top">태국</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">미얀마어</td>
<td class="wide-spacing vertical-top">미얀마</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">크메르어</td>
<td class="wide-spacing vertical-top">캄보디아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">라오스어</td>
<td class="wide-spacing vertical-top">라오스</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">타갈로그어</td>
<td class="wide-spacing vertical-top">필리핀</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" rowspan="18" scope="row">유럽 <br />(18개 언어)</th>
<td>폴란드어</td>
<td>폴란드</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">루마니아어</td>
<td class="wide-spacing vertical-top">루마니아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">헝가리어</td>
<td class="wide-spacing vertical-top">헝가리</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">체코어</td>
<td class="wide-spacing vertical-top">체코</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">슬로바키아어</td>
<td class="wide-spacing vertical-top">슬로바키아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">세르비아어</td>
<td class="wide-spacing vertical-top">보스니아-헤르체고비나, 세르비아, 코소보</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">크로아티아어</td>
<td class="wide-spacing vertical-top">보스니아-헤르체고비나, 크로아티아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">라트비아어</td>
<td class="wide-spacing vertical-top">라트비아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">벨라루스어</td>
<td class="wide-spacing vertical-top">벨라루스</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">조지아어</td>
<td class="wide-spacing vertical-top">조지아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">그리스어</td>
<td class="wide-spacing vertical-top">그리스, 사이프러스</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">불가리아어</td>
<td class="wide-spacing vertical-top">불가리아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">이탈리아어</td>
<td class="wide-spacing vertical-top">산마리노, 스위스, 이탈리아</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">네덜란드어</td>
<td class="wide-spacing vertical-top">네덜란드, 벨기에, 수리남</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">노르웨이어</td>
<td class="wide-spacing vertical-top">노르웨이</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">덴마크어</td>
<td class="wide-spacing vertical-top">덴마크</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">스웨덴어</td>
<td class="wide-spacing vertical-top">스웨덴</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">핀란드어</td>
<td class="wide-spacing vertical-top">핀란드</td>
</tr>
<tr class="left-align ">
<th class="font-700 center-align" rowspan="2" scope="row">중남미 <br />(2개 언어)</th>
<td>포르투갈어</td>
<td>동티모르, 모잠비크, 상투메프린시페, 앙골라, 적도기니, 카보베르데, 포르투갈</td>
</tr>
<tr class="left-align ">
<td class="wide-spacing vertical-top">브라질어</td>
<td class="wide-spacing vertical-top">브라질</td>
</tr>
</tbody>
</table>
</div>
</div>
</article>
<article class="common-column-layout flex-row">
<div class="flex-col-3">
<h3 class="content-main-title">한국외국어대학교</h3>
<h3 class="content-main-title">특수외국어 전문교육</h3>
</div>
<div class="flex-col-9 pl-30 content-wrap">
<div class="content-group">
<h4 class="content-sub-title">한국외국어대학교 특수외국어교육진흥원</h4>
<div class="contnet-desc">4차 산업혁명 시대를 맞아 다변화된 해외시장과 복잡해진 글로벌 환경하에서 외국어, 더 나아가 특수외국어의 중요성이 증가하고 있습니다. 외국어는 더 이상 단순한 의사소통 도구가 아닌 국익과 국가 안보를 지키는 전략 자산입니다.</div>
<div class="contnet-desc">&nbsp;</div>
<div class="contnet-desc">특수외국어교육진흥원은 이에 발맞춰 모든 국민에게 특수외국어교육의 기회를 균등하게 제공하고, 외국어 전문 인력 양성을 위해 심화 교육과정 개발과 운영을 통해 전문적이고 숙달된 외국어 전문가를 육성하며, 누구나 쉽게 접근할 수 있는 특수외국어 교육과정을 선보일 것입니다.<br /><br />우리 대학만의 외국어 교육 및 평가 노하우, 우수한 교수진, 세계 각국 및 외교사절들과 맺은 긴밀한 관계, 정통한 현지 사정 등을 토대로 수준 높은 특수외국어 교육을 제공하겠습니다.&nbsp;</div>
</div>
<div class="content-group">
<h4 class="content-sub-title">한국외국어대학교 지원 특수외국어</h4>
<div class="contnet-desc">- 총 16개 언어 20개 학과</div>
<!-- 테이블영역--></div>
<div class="content-group">
<table class="common-table-wrap border-white size-sm"><colgroup> <col style="width: 30%;" /> <col style="width: 70%;" /> </colgroup>
<tbody>
<tr class="left-align ">
<th class="font-700 center-align bg-deep-gray" scope="row">언어</th>
<th class="font-700 center-align bg-deep-gray" scope="row">학과</th>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">네덜란드어</td>
<td class="bg-gray center-align">네덜란드어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align" rowspan="2">마인어</td>
<td class="bg-gray center-align">말레이·인도네시아어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">말레이·인도네시아어통번역학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">몽골어</td>
<td class="bg-gray center-align">몽골어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align" rowspan="2">포르투갈어</td>
<td class="bg-gray center-align">브라질학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">포르투갈어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">스웨덴어</td>
<td class="bg-gray center-align">스칸디나비아어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">스와힐리어</td>
<td class="bg-gray center-align">아프리카학부</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align" rowspan="3">이탈리아어</td>
<td class="bg-gray center-align">이탈리아어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">이탈리아통번역학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">이탈리아 EU전략 세부모듈(융합인재학부)</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align" rowspan="2">힌디어</td>
<td class="bg-gray center-align">인도어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">인도학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">우즈베크어/ 카자흐어</td>
<td class="bg-gray center-align">중앙아시아학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align" rowspan="2">태국어/ 라오스어</td>
<td class="bg-gray center-align">태국어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">태국어통번역학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">터키어</td>
<td class="bg-gray center-align">터키·아제르바이잔어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">이란어</td>
<td class="bg-gray center-align">페르시아어·이란학과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">폴란드어</td>
<td class="bg-gray center-align">폴란드어과</td>
</tr>
<tr class="left-align ">
<td class="bg-gray center-align">헝가리어</td>
<td class="bg-gray center-align">헝가리어과</td>
</tr>
</tbody>
</table>
</div>
</div>
</article>
</div>
</section>