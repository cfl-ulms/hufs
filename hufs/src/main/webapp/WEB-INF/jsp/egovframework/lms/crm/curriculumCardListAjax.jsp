<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>


<c:forEach var="result" items="${resultList}" varStatus="status">
	          <c:choose>
	          	<c:when test="${fn:length(resultList) >= 8}">
	          		<c:set var="listCnt" value="8"/>
	          	</c:when>
	          	<c:when test="${fn:length(resultList) >= 4 and fn:length(resultList) < 8}">
	          		<c:set var="listCnt" value="4"/>
	          	</c:when>
	          	<c:otherwise>
	          		<c:set var="listCnt" value="${fn:length(resultList)}"/>
	          	</c:otherwise>
	          </c:choose>
	          <c:if test="${status.count <= listCnt}">
	          	<c:url var="crmView" value="/lms/crm/CurriculumAllView.do">
	          		<c:param name="crclId" value="${result.crclId}"/>
	          		<c:param name="crclbId" value="${result.crclbId}"/>
	          		<c:param name="menuId" value="MNU_0000000000000066"/>
	          		<c:param name="searchTargetType" value="N"/>
	          	</c:url>
	          	<div class="flex-col-3 relative">
	            	<a href="${crmView}" class="card-wrap">
	
				      <div class="card-head">
				        <p class="card-flag-img">
				          <c:set var="imgSrc">
							<c:import url="/lms/common/flag.do" charEncoding="utf-8">
								<c:param name="ctgryId" value="${result.crclLang}"/>
							</c:import>
		          		  </c:set>
		                  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
				        </p>
				        <div class="card-title-wrap">
		                  <span class="card-category font-blue">
		                  <fmt:parseDate value="${result.applyStartDate}" var="startDate" pattern="yyyy-MM-dd"/>
		                  <fmt:formatDate value="${startDate}" var="start" pattern="yyyyMMdd"/>
		                  
		                  <jsp:useBean id="toDay" class="java.util.Date"/>
		                  <fmt:formatDate var="now" value="${toDay}" pattern="yyyyMMdd" />
	
		                  <c:choose>
		                  	<c:when test="${result.processSttusCodeDate eq '0'}">관리자 등록확정대기</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '1'}">과정계획 대기</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '2'}">개설예정</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '3'}">수강신청 중</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '4'}">수강신청 종료</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '5'}">과정개설취소</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '6'}">수강대상자 확정</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '7'}">과정 중</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '8'}">과정종료</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '9'}">과정종료</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '10'}">과정종료(성적발표)</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '11'}">성적발표 종료</c:when>
		                  	<c:when test="${result.processSttusCodeDate eq '12'}">최종종료(확정)</c:when>
		                  	<c:otherwise>-</c:otherwise>
		                  </c:choose>
		                  <c:set var="dDay" value="${start-now}"/>
		                  <c:if test="dDay > 0">(D-${dDay})</c:if>
		                  </span>
		                  <div class="card-title">${result.crclNm}</div>
		                </div>
				       </div>
	
				      <div class="card-body">
				        <ul class="card-items-wrap">
				          <li class="card-items icon-campus">
		                  	<c:forEach var="campus" items="${campusList}" varStatus="status">
			                    <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
			                      <c:out value="${campus.ctgryNm}"/>
			                    </c:if>
			                </c:forEach>
		                  </li>
				          <li class="card-items icon-name">
				          	<c:set var="comma" value=","/>
				          	<c:set var="teacherCnt" value="0"/>
				          	<c:forEach var="list" items="${resultList}" varStatus="status">
				          		<c:if test="${result.crclId eq list.crclId}">
				          			<c:forEach var="fac" items="${list.facList}" varStatus="status">				          				
				          				<c:if test="${0 eq status.index }">
                                            <c:choose>
                                                <c:when test="${1 < fn:length(list.facList)}"><c:out value="${fac.userNm}"/>${comma}</c:when>
                                                <c:otherwise><c:out value="${fac.userNm}"/>교수</c:otherwise>
                                            </c:choose>
                                        </c:if>
                                        <c:if test="${1 eq status.index }">
                                            <c:out value="${fac.userNm}"/>교수
                                        </c:if>
                                        <c:if test="${1 < status.index }">
                                            <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
                                        </c:if>
				          			</c:forEach>
								</c:if>
							</c:forEach>
							<c:if test="${0 < teacherCnt }">외${teacherCnt }명</c:if>
				          </li>
		                  <li class="card-items icon-date">${result.applyStartDate } ~ ${result.applyEndDate }</li>
				        </ul>
				      </div>
				    </a>
				  </div>
				  </c:if>
          </c:forEach>