<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000163"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
});

function fnSubmit(){
	if($('input:checkbox[name="searchSubmitArr"]:checked').length == 1){
		$("#searchSubmit").val($('input:checkbox[name="searchSubmitArr"]:checked').val());
	}

	$("#pageIndex").val(1);
	$("#frm").submit();
}

function dateChk(endDate, viewUrl, limitDate,processSttusCode ){
	var curDate = "${curDate}";
	endDate = endDate.replace(/-/gi,"");
	curDate = curDate.replace(/-/gi,"");
	//location.href = viewUrl;
	/* if(curDate < endDate){
		alert("설문 참여일이 아닙니다.");
		return false;
	}else if(limitDate != '' && curDate >= limitDate){
		alert("설문에 참여할 수 없습니다.");
		return false;
	}else{
		location.href = viewUrl;
	} */
	
	if(processSttusCode == '11' ||  processSttusCode == '12'){
		alert("설문에 참여할 수 없습니다.");
		return false;
	} else {
		location.href = viewUrl;
	}
}
</script>
        <section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <form id="frm" method="post" action="<c:url value="/lms/cla/mySurveyList.do"/>">
            	  <input type="hidden" id="pageIndex" name="pageIndex" value="${paginationInfo.currentPageNo }"/>
            	  <input type="hidden" name="menuId" value="${param.menuId }" />
            	  <input type="hidden" id="searchSubmit" name="searchSubmit" value="" />

                <h3 class="title-subhead">만족도 조사 검색</h3>
                <div class="flex-row-ten">
                	<c:if test="${USER_INFO.userSeCode ne '08'}">
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select class="select2" name="searchCrclLang" id="searchCrclLang" data-select="style3" >
	                        <option value="">언어 전체</option>
	                        <c:forEach var="result" items="${languageList}" varStatus="status">
	                        	<c:if test="${not empty result.upperCtgryId}">
									<option value="${result.ctgryId}" ${searchVo.searchCrclLang eq result.ctgryId ? 'selected' : '' }><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
	                      </select>
	                    </div>
	                  </div>
	                </c:if>
	                
                  <div class="flex-ten-col-<c:out value="${USER_INFO.userSeCode eq '08' ? '10' : 7}"/> mb-20">
                    <div class="ell">
                      <input type="text" placeholder="과정명을 입력해보세요." name="searchCrclNm" value="${searchVo.searchCrclNm}">
                    </div>
                  </div>
                  <div class="flex-ten-col-10 flex align-items-center">
                  
                   <c:forEach items="${searchVo.searchSchdulClCode}" var="result">
						<c:choose>
							<c:when test="${result eq 'TYPE_1' }">
								<c:set var="searchSchdulClCode1" value="TYPE_1"/>
							</c:when>
							<c:when test="${result eq 'TYPE_2' }">
								<c:set var="searchSchdulClCode2" value="TYPE_2"/>
							</c:when>
						</c:choose>
					</c:forEach>

					<c:forEach items="${searchVo.searchSubmitArr}" var="result">
						<c:choose>
							<c:when test="${result eq 'submitY' }">
								<c:set var="searchSubmitArr1" value="submitY"/>
							</c:when>
							<c:when test="${result eq 'submitN' }">
								<c:set var="searchSubmitArr2" value="submitN"/>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:if test="${USER_INFO.userSeCode ne '08'}">
	                    <label class="checkbox">
	                      <input type="checkbox" name="searchSchdulClCode" value="TYPE_1" ${searchSchdulClCode1 eq 'TYPE_1' ? 'checked' : '' }>
	                      <span class="custom-checked"></span>
	                      <span class="text">과정 만족도</span>
	                    </label>
	                    <label class="checkbox">
	                      <input type="checkbox" name="searchSchdulClCode" value="TYPE_2" ${searchSchdulClCode2 eq 'TYPE_2' ? 'checked' : '' }>
	                      <span class="custom-checked"></span>
	                      <span class="text">수업 만족도</span>
	                    </label>
                    </c:if>
                    <label class="checkbox">
                      <input type="checkbox" name="searchSubmitArr" value="submitY" ${searchSubmitArr1 eq 'submitY' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">설문 미제출</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchSubmitArr" value="submitN" ${searchSubmitArr2 eq 'submitN' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">설문 제출완료</span>
                    </label>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20" onclick="fnSubmit()">검색</button>
                </form>
              </div>
            </article>
            <article class="content-wrap">
              <c:if test="${USER_INFO.userSeCode ne '08'}">
              	<p class="font-point table-top-info">* 설문을 참여하지 않을 경우 과정종료 후 성적조회 등이 원활하지 않을 수 있습니다. 꼭 설문에 참여해 주세요</p>
              </c:if>
              	
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:12%'>
                  <col>
                  <col style='width:12%'>
                  <col style='width:12%'>
                  <col style='width:12%'>
                  <col style='width:130px'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>언어</th>
                    <th scope='col'>과정명</th>
                    <th scope='col'>설문구분</th>
                    <th scope='col'>제출 시작일</th>
                    <th scope='col'>제출여부</th>
                    <th scope='col'>제출일</th>
                  </tr>
                </thead>
                <tbody>
                <c:if test="${not empty surveyList}">
                	<c:forEach items="${surveyList}" var="result" varStatus="status">
                		 <c:url var="viewUrl" value="/lms/cla/surveyView.do${_BASE_PARAM }">
						  	<c:param name="schdulId" value="${result.schdulId}" />
							<c:param name="schdulClCode" value="${result.schdulClCode}" />
							<c:param name="crclId" value="${result.crclId}" />
							<c:param name="plId" value="${result.plId}" />
						</c:url>

                		 <tr onclick="dateChk('${result.endDate}${fn:replace(result.endTime,':','')}', '${viewUrl}', '${result.limitDate }', '${result.processSttusCode}');" class="${not empty result.submitDate ? 'bg-lighten' : '' }">
		                    <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVo.pageIndex-1) * searchVo.pageUnit) - (status.count - 1)}" /></td>
		                    <td>${result.ctgryNm }</td>
		                    <td class='left-align'>${result.crclNm }</td>
		                    <td>${result.type }</td>
		                    <td>${result.endDate }<br>
		                    	<c:if test="${result.type eq '과정만족도'}"> ${result.endTime}</c:if>
								<%-- <c:choose>
									<c:when test="${result.dDay eq '0' }">
										(오늘)
									</c:when>
									<c:when test="${fn:indexOf(result.dDay,'-') != -1 }">
										( D${fn:replace(result.dDay,'-', '+')} )
									</c:when>
									<c:otherwise>
										( D-${result.dDay} )
										<c:set var="surveyStatus" value="대기"/>
									</c:otherwise>
								</c:choose> --%>
							</td>
		                    <td>${not empty surveyStatus ? surveyStatus : not empty result.submitDate ? '제출완료' : '미제출'}</td>
		                    <td>${empty result.submitDate ? '-' : result.submitDate}</td>
		                  </tr>
                	</c:forEach>
                </c:if>
                </tbody>
              </table>

               <c:if test="${not empty surveyList}">
	              <div class="pagination center-align mt-60">
	               <div class="pagination-inner-wrap overflow-hidden inline-block">
						<c:url var="startUrl" value="/lms/cla/mySurveyList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="1" />
		   				</c:url>
		               	<button class="start goPage" data-url="${startUrl}"></button>

		               	<c:url var="prevUrl" value="/lms/cla/mySurveyList.do${_BASE_PARAM}">
							<c:param name="pageIndex" value="${searchVo.pageIndex > 1 ? searchVo.pageIndex - 1 : 1}"/>
						</c:url>
		               	<button class="prev goPage" data-url="${prevUrl}"></button>

		               	<ul class="paginate-list f-l overflow-hidden">
		                 	<c:url var="pageUrl" value="/lms/cla/mySurveyList.do${_BASE_PARAM}"/>
							<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
							<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
		               	</ul>

		               	<c:url var="nextUrl" value="/lms/cla/mySurveyList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${searchVo.pageIndex < paginationInfo.totalPageCount ? searchVo.pageIndex + 1 : searchVo.pageIndex}" />
		   				</c:url>
		               	<button class="next goPage" data-url="${nextUrl}"></button>

		               	<c:url var="endUrl" value="/lms/cla/mySurveyList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
		   				</c:url>
		               	<button class="end goPage" data-url="${endUrl}"></button>
					</div>
	              </div>
              </c:if>
            </article>
          </section>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>