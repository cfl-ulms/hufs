<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.plId}"><c:param name="plId" value="${param.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script src="${CML}/js/class/staff_class.2.1.12.js?v=1"></script>
<script>
$(document).ready(function(){
	//저장
	$("#btn_save").click(function(){
		var leng = $(".atdType:checked").length - 1;
		
		$("#atdTypeBox").html("");
		$(".atdType:checked").each(function(i){
			$("#atdTypeBox").append("<input type='hidden' name='attentionTypeList' value='" + $(this).val() + "'/>");
			
			if(leng == i){
				$("#detailForm").submit();
			}
		});
		
		return false;
	});
});

function vali(){
	
}
</script>

         <div class="page-content-header">
         	<c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
			<!-- 
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="11"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
        </article>
		<article class="content-wrap">
			<form name="detailForm" id="detailForm" method="post" action="/lms/atd/attendUpt.do" onsubmit="return vali();">
				<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
				<input type="hidden" name="plId" value="${searchVO.plId}"/>
				<input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
				<c:if test="${not empty param.step}"><input type="hidden" name="step" value="${param.step}"/></c:if>
				<c:if test="${not empty param.tabType}"><input type="hidden" name="tabType" value="${param.tabType}"/></c:if>
				<c:if test="${not empty param.subtabstep}"><input type="hidden" name="subtabstep" value="${param.subtabstep}"/></c:if>
				<c:if test="${not empty param.tabStep}"><input type="hidden" name="tabStep" value="${param.tabStep}"/></c:if>
				
	              <!-- 테이블영역-->
	              <table class="common-table-wrap ">
	                <colgroup>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                </colgroup>
	                <thead>
	                  <tr class='bg-gray font-700'>
	                    <th scope='col'>No</th>
	                    <th scope='col'>소속</th>
	                    <th scope='col'>이름</th>
	                    <th scope='col'>생년월일</th>
	                    <th scope='col'>학번</th>
	                    <th scope='col'>학년</th>
	                    <th scope='col'>조</th>
	                    <th scope='col' class='bg-light-gray'>출석</th>
	                    <th scope='col' class='bg-light-gray'>지각</th>
	                    <th scope='col' class='bg-light-gray'>결석</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	<c:set var="atdY" value="0"/>
	                	<c:set var="atdL" value="0"/>
	                	<c:set var="atdN" value="0"/>
	                	<c:forEach var="result" items="${resultList}" varStatus="status">
	                		<tr class="">
			                    <td scope='row'><c:out value="${status.count}"/></td>
			                    <td scope='row'><c:out value="${result.mngDeptNm}"/></td>
			                    <td scope='row'>
			                    	<c:out value="${result.userNm}"/>
			                    	<input type="hidden" name="userIdList" value="${result.userId}"/>
			                    </td>
			                    <td scope='row'><c:out value="${result.brthdy}"/></td>
			                    <td scope='row'><c:out value="${result.stNumber}"/></td>
			                    <td scope='row'><c:out value="${result.stGrade}"/></td>
			                    <td scope='row'><c:out value="${result.classCnt}"/></td>
			                    <td scope='row' <c:if test="${result.attentionType eq 'Y'}">class='bg-point-light'</c:if>>
			                    	<label class='checkbox circle'>
			                    		<input type='radio' name='name${status.count}' class="atdType" value="Y" data-color='bg-point-light' <c:if test="${result.attentionType eq 'Y'}">checked</c:if>><span class='custom-checked'></span>
			                    	</label>
			                    </td>
			                    <td scope='row' <c:if test="${result.attentionType eq 'L'}">class='bg-gray'</c:if>>
			                    	<label class='checkbox circle'>
			                    		<input type='radio' name='name${status.count}' class="atdType" value="L" data-color='bg-gray' <c:if test="${result.attentionType eq 'L'}">checked</c:if>><span class='custom-checked'></span>
			                    	</label>
			                    </td>
			                    <td scope='row' <c:if test="${result.attentionType eq 'N'}">class='bg-red-light'</c:if>>
			                    	<label class='checkbox circle'>
			                    		<input type='radio' name='name${status.count}' class="atdType" value="N" data-color='bg-red-light' <c:if test="${result.attentionType eq 'N'}">checked</c:if>><span class='custom-checked'></span>
			                    	</label>
			                    </td>
			                  </tr>
			                  
			                  <c:choose>
			                  	<c:when test="${result.attentionType eq 'Y'}">
			                  		<c:set var="atdY" value="${atdY + 1}"/>
			                  	</c:when>
			                  	<c:when test="${result.attentionType eq 'L'}">
			                  		<c:set var="atdL" value="${atdL + 1}"/>
			                  	</c:when>
			                  	<c:when test="${result.attentionType eq 'N'}">
			                  		<c:set var="atdN" value="${atdN + 1}"/>
			                  	</c:when>
			                  </c:choose>
	                	</c:forEach>
	                	
	                  <tr class="">
	                    <td scope='row' class='font-point font-700' colspan='7'>총 <c:out value="${fn:length(resultList)}"/>명</td>
	                    <td scope='row' class='bg-point-light'>
	                      <div class='input-group'><span class='unit font-gray-light'>명</span><input type='number' class='table-input' value='${atdY}' readonly="readonly"></div>
	                    </td>
	                    <td scope='row' class='bg-gray'>
	                      <div class='input-group'><span class='unit font-gray-light'>명</span><input type='number' class='table-input' value='${atdL}' readonly="readonly"></div>
	                    </td>
	                    <td scope='row' class='bg-red-light'>
	                      <div class='input-group'><span class='unit font-gray-light'>명</span><input type='number' class='table-input' value='${atdN}' readonly="readonly"></div>
	                    </td>
	                  </tr>
	                  
	                </tbody>
	              </table>
	              <div id="atdTypeBox"></div>
              </form>
            </article>
            
            <c:if test="${studyMngAt eq 'Y' or managerAt eq 'Y'}">
	            <div class="page-btn-wrap mt-50">
	              <a href="#" id="btn_save" class="btn-xl btn-point">저장</a>
	            </div>
            </c:if>
          </section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>