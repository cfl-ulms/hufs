<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:choose>
	<c:when test="${param.ctgryId eq 'MN'}">img_national_mongolia.jpg</c:when>
	<c:when test="${param.ctgryId eq 'SW'}">img_national_kenya.jpg</c:when>
	<c:when test="${param.ctgryId eq 'UZ'}">img_national_uzbekistan.jpg</c:when>
	<c:when test="${param.ctgryId eq 'FA'}">img_national_iran.jpg</c:when>
	<c:when test="${param.ctgryId eq 'MI'}">img_national_indonesia.jpg</c:when>
	<c:when test="${param.ctgryId eq 'TR'}">img_national_turkey.jpg</c:when>
	<c:when test="${param.ctgryId eq 'TH'}">img_national_thailand.jpg</c:when>
	<c:when test="${param.ctgryId eq 'PT'}">img_national_portugal.jpg</c:when>
	<c:when test="${param.ctgryId eq 'HU'}">img_national_hungary.jpg</c:when>
	<c:when test="${param.ctgryId eq 'PL'}">img_national_poland.jpg</c:when>
	<c:when test="${param.ctgryId eq 'HI'}">img_national_india.jpg</c:when>
	<c:when test="${param.ctgryId eq 'IT'}">img_national_Italy.png</c:when>
	<c:when test="${param.ctgryId eq 'ND'}">img_national_Netherlands.png</c:when>
	<c:when test="${param.ctgryId eq 'SE'}">img_national_Sweden.png</c:when>
	<c:when test="${param.ctgryId eq 'LA'}">img_national_Laos.png</c:when>
	<c:when test="${param.ctgryId eq 'KZ'}">img_national_Kazakhstan.png</c:when>
	<c:otherwise>img_national_common.jpg</c:otherwise>
</c:choose>