<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}homeworksubmit/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_JS" value="/template/common/js"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="courseReviewCn"/>
<c:set var="_ACTION" value=""/> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId}"/>
	<c:param name="boardType" value="${param.boardType}"/>
	<c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
    <c:param name="searchCourseReviewSj" value="${searchVO.searchCourseReviewSj}"/>
    <c:param name="pageIndex" value="${searchVO.pageIndex}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
function fn_egov_regist() {
    
    tinyMCE.triggerSave();
    
    if($.trim($('#${_EDITOR_ID}').val()) == "") {
        alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
        tinyMCE.activeEditor.focus();
        return false;
    }

    $('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

    <c:if test="${searchVO.registAction eq 'regist'}">
	    if (!confirm('<spring:message code="common.regist.msg" />')) {
	        return false;
	    }
	</c:if>
}

$(document).ready(function(){
 	var adfile_config = {
            siteId:"SITE_000000000000001",
            pathKey:"HomeworkSubmit",
            appendPath:"HOMEWORKSUBMIT",
            editorId:"${_EDITOR_ID}",
            fileAtchPosblAt:"Y",
            maxMegaFileSize:100,
            atchFileId:"${courseReviewVo.atchFileId}"
        };
        
    fnCtgryInit('');
    fn_egov_bbs_editor(adfile_config);
    

    
      //글등록
    $(document).on("click", ".insertHomeworkCommentSubmit, .btnModalConfirm", function() {
        $('#curriculumsubmit').submit();
    });
});
</script>

<section class="page-content-body">  
   
	<article class="content-wrap">
		<!-- 과제 제출 -->
	   	<div class="content-header">
	    	<div class="title-wrap">
	       		<div class="title">과정후기 작성하기</div>
	     	</div>
	   	</div>
	   	<div class="content-body">
	    	<%-- 등록, 수정 페이지 --%>
	  		<c:choose>
		        <c:when test="${empty courseReviewVo.crclLang }"><c:set var="formUrl" value="/lms/crcl/insertCourseReview.do"/></c:when>
		        <c:otherwise><c:set var="formUrl" value="/lms/crcl/updateCourseReview.do"/></c:otherwise>
	    	</c:choose>
	     	<form:form commandName="curriculumsubmit" name="board" method="post" action="${formUrl }" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
	         	<input name="menuId" type="hidden" value="<c:out value='${param.menuId}'/>" />
	         	<c:choose>
	         		<c:when test="${courseReviewVo.courseReviewSeq == null or courseReviewVo.courseReviewSeq == ''}">
			        	<input id="courseReviewSeq" name="courseReviewSeq" type="hidden" value="0">
	         		</c:when>
	         		<c:otherwise>
			        	<input id="courseReviewSeq" name="courseReviewSeq" type="hidden" value="${courseReviewVo.courseReviewSeq}">
	         		</c:otherwise>
	         	</c:choose>
		        <input id="atchFileId" name="atchFileId" type="hidden" value="${courseReviewVo.atchFileId}">
		        <input id="fileGroupId" name="fileGroupId" type="hidden" value="${courseReviewVo.atchFileId}">
		        <input type="hidden" name="registAction" value="<c:out value='${param.registAction}'/>" />
				<table class="common-table-wrap table-style2 mb-20">
	           		<tbody>
	             		<tr>
	               			<th class="title">구분</th>
	               			<td>
	                 			<div class="flex-row">
	                   				<div class="flex-col-12">
	                   	  				<select class="select2" name="crclLang" id="crclLang" data-select="style3">
							               <c:if test="${user.userSeCode eq '99'}">
					                   	 		<option value="total" <c:if test="${courseReviewVo.crclLang eq 'total'}">selected</c:if>>전체</option>
							               </c:if>
	                   	 					<c:forEach var="languageList" items="${languageList}" varStatus="status">
	                   	 						<c:if test="${not empty languageList.upperCtgryId && languageList.ctgryId ne 'ALL'}">
	                   	 							<option value="${languageList.ctgryId}" <c:if test="${languageList.ctgryId eq courseReviewVo.ctgryId}">selected</c:if>>${languageList.ctgryNm}</option>
	                   	 						</c:if>
	                   	 					</c:forEach>
	                   	 				</select>
	                   				</div>
	                 			</div>
	               			</td>
	               			<c:choose>
	               				<c:when test="${user.userSeCode eq '99'}">
		               				<th>공지여부</th>
		               				<td>
		               					<select class="select2 notice" name="noticeYn" id="noticeYn" data-select="style3">
						               		<option value="N" <c:if test="${courseReviewVo.noticeYn eq 'N'}">selected</c:if>>공지안함</option>
						               		<option value="Y" <c:if test="${courseReviewVo.noticeYn eq 'Y'}">selected</c:if>>공지</option>
		               					</select>
		               				</td>
	               				</c:when>
	               				<c:otherwise><input type="hidden" name="noticeYn" id="noticeYn" value="N"></c:otherwise>
	               			</c:choose>
	             		</tr>
	             		<tr>
	               			<th class="title">제목</th>
	               			<c:choose>
	               				<c:when test="${user.userSeCode eq '99'}"><td colspan="3"></c:when>
	               				<c:otherwise><td></c:otherwise>
	               			</c:choose>
	                 			<div class="flex-row">
	                   				<div class="flex-col-12">
	                     				<input type="text" class="table-input" value="${courseReviewVo.courseReviewSj}" name="courseReviewSj" placeholder="과제후기를 해당 언어로 입력해주세요.">
	                   				</div>
	                 			</div>
	               			</td>
	             		</tr>
					</tbody>
				</table>
				<div>
					<textarea id="courseReviewCn" name="courseReviewCn" rows="20" class="cont" style="width:99%">${courseReviewVo.courseReviewCn}</textarea>
				</div>
				<!-- 첨부파일 -->
				<div class="mt-20">
	            	<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
		                <c:param name="editorId" value="${_EDITOR_ID}"/>
		                <c:param name="estnAt" value="N" />
		                <c:param name="param_atchFileId" value="${courseReviewVo.atchFileId}" />
	<%-- 	                <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" /> --%>
		                <c:param name="imagePath" value="${_IMG }"/>
		                <c:param name="regAt" value="Y"/>
		                <c:param name="commonAt" value="Y"/>
	            	</c:import>
				</div>
			</form:form>
		</div>
	 </article>
     
	 <div class="mt-50">
	   <a style="float: left;" href="<c:url value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM }"/>" class="btn-sm btn-outline-gray font-basic">목록</a>
	   <c:choose>
	       <c:when test="${param.registAction eq 'updt'}">
	           <%-- 개별, 조별에 따른 modal 띄우는거 분기처리 --%>
               <a style="float: right;" href="#none" class="btn-sm btn-point insertHomeworkCommentSubmit" data-modal-type="confirm">수정</a>
	       </c:when>
	       <c:otherwise>
		       <a style="float: right;" href="#none" class="btn-sm btn-point insertHomeworkCommentSubmit">저장</a>
	       </c:otherwise>
	   </c:choose>
	 </div>
  </section>
</div>

</div>
</div>
</div>

<%-- <div id="confirm_modal" class="alert-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">제출하기</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <fmt:formatDate value="${homeworkSubmitVO.lastUpdusrPnttm }" pattern="yyyy-MM-dd HH:mm" var="lastUpdusrPnttmModalView" />
          <p class="modal-text">${lastUpdusrPnttmModalView }에 ${homeworkSubmitVO.userNm}님이 이미 제출한 과제가 있습니다.</p>
          <p class="modal-subtext"><p class="modal-subtext">${homeworkSubmitVO.groupCnt}조 ${homeworkSubmitVO.userNm}의 과제제출 내용을 수정하여, ${homeworkSubmitVO.groupCnt}조의 과제를 다시 제출합니다.<br> 재등록은 과제마감일 전까지만 가능합니다. 등록하시겠습니까?</p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button type="button" class="btn-xl btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
</div> --%>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>