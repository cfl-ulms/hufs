<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<link rel="stylesheet" href="../css/common/table_staff.css?v=2">

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.plId}"><c:param name="plId" value="${param.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
//달력
$(function() {
  $(".btn_calendar").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	
	//시작시간 & 종료시간 & 과정교수명
	var startTimeA = [],
		startTimeHH = [],
		startTimeMM = [],
		endTimeA = [],
		endTimeHH = [],
		endTimeMM = [],
		facA = [];
	
	<c:forEach var="result" items="${camSchList}">
		startTimeA.push({startTime : '${result.startTime}', period : '${result.period}', startTimeNm : '${result.period}교시 ${fn:substring(result.startTime,0,2)}:${fn:substring(result.startTime,2,4)}'});
	</c:forEach>
	
	<c:forEach var="result" items="${camSchList}">
		endTimeA.push({endTime : '${result.endTime}', period : '${result.period}', endTimeNm : '${result.period}교시 ${fn:substring(result.endTime,0,2)}:${fn:substring(result.endTime,2,4)}'});
	</c:forEach>
	
	<c:forEach var="result" begin="6" end="24" step="1">
		<c:set var="hour">
			<c:choose>
				<c:when test="${result < 10}">0${result}</c:when>
				<c:otherwise>${result}</c:otherwise>
			</c:choose>
		</c:set>
		startTimeHH.push({startTimeHH : '${hour}'});
		endTimeHH.push({endTimeHH : '${hour}'});
	</c:forEach>
	
	<c:forEach var="result" begin="0" end="55" step="5">
		<c:set var="minute">
			<c:choose>
				<c:when test="${result < 10}">0${result}</c:when>
				<c:otherwise>${result}</c:otherwise>
			</c:choose>
		</c:set>
		startTimeMM.push({startTimeMM : '${minute}'});
		endTimeMM.push({endTimeMM : '${minute}'});
	</c:forEach>
	
	<c:forEach var="result" items="${facList}">
		facA.push({facId : '${result.facId}', userNm : '${result.userNm}'});
	</c:forEach>
	
	//시수(시간표) 추가
	$(".btn_addTime").click(function(){
		var html = "",
			startTimeHtml = "<option value=''>선택</option>",
			endTimeHtml = "<option value=''>선택</option>",
			startTimeHHHtml = "<option value=''>선택</option>",
			startTimeMMHtml = "<option value=''>선택</option>",
			endTimeHHHtml = "<option value=''>선택</option>",
			endTimeMMHtml = "<option value=''>선택</option>",
			facHtml = "<option value=''>선택</option>";
		
		for(i = 0; i < startTimeA.length; i++){
			startTimeHtml += "<option value='"+ startTimeA[i].startTime +"' data-period='"+ startTimeA[i].period +"'>" + startTimeA[i].startTimeNm + "</option>";
		}
		
		for(i = 0; i < endTimeA.length; i++){
			endTimeHtml += "<option value='"+ endTimeA[i].endTime +"' data-period='"+ endTimeA[i].period +"'>" + endTimeA[i].endTimeNm + "</option>";
		}
		
		for(i = 0; i < startTimeHH.length; i++){
			startTimeHHHtml += "<option value='"+ startTimeHH[i].startTimeHH +"'>" + startTimeHH[i].startTimeHH + "</option>";
		}
		
		for(i = 0; i < startTimeMM.length; i++){
			startTimeMMHtml += "<option value='"+ startTimeMM[i].startTimeMM +"'>" + startTimeMM[i].startTimeMM + "</option>";
		}
		
		for(i = 0; i < endTimeHH.length; i++){
			endTimeHHHtml += "<option value='"+ endTimeHH[i].endTimeHH +"'>" + endTimeHH[i].endTimeHH + "</option>";
		}
		
		for(i = 0; i < endTimeMM.length; i++){
			endTimeMMHtml += "<option value='"+ endTimeMM[i].endTimeMM +"'>" + endTimeMM[i].endTimeMM + "</option>";
		}
		
		for(i = 0; i < facA.length; i++){
			facHtml += "<option value='"+ facA[i].facId +"' data-nm='"+ facA[i].userNm +"'>"+ facA[i].userNm +"</option>";
		}
		
		<c:choose>
			<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
			html = "<tr class='scheduleTr'>" +
						"<td scope='row'>" +
							"<input type='text' name='sisu' class='wid50p sisu required' />" +
							"<input type='hidden' name='crclId' value='${curriculumVO.crclId}'/>" +
						"</td>" +
						"<td class='left-align'>" +
							"<input type='text' name='startDt' class='ell date datepicker btn_calendar edit_mode required' autocomplete='off' readonly='readonly' placeholder='수업일'/>" +
						"</td>" +
						"<td class='left-align'>" +
							"<select class='table-select select2 startTimeHH' name='startTimeHH' data-select='style1' data-placeholder='선택'>" +
								startTimeHHHtml +
							"</select>" +
							" : " +
							"<select class='table-select select2 startTimeMM' name='startTimeMM' data-select='style1' data-placeholder='선택'>" +
								startTimeMMHtml +
							"</select>" +
						"</td>" +
						"<td class='left-align'>" +
							"<select class='table-select select2 endTimeHH' name='endTimeHH' data-select='style1' data-placeholder='선택'>" +
								endTimeHHHtml +
							"</select>" +
							" : " +
							"<select class='table-select select2 endTimeMM' name='endTimeMM' data-select='style1' data-placeholder='선택'>" +
								endTimeMMHtml +
							"</select>" +
						"</td>" +
						"<td class='left-align box_period'>" +
							"<input type='text' name='periodTxt'/>" +
						"</td>" +
						"<td class='left-align'>" +
							"<div class='flex-row'>" +
			                "<div class='flex-col-12'><select class='table-select select2 search fac edit_mode required' data-select='style1' data-placeholder='교원검색'>" +
			                	facHtml +
			                  "</select></div>" +
			                "<div class='flex-col-12'>" + 
			                  "<ul class='selected-items-wrap row-full box_fac'></ul></div>" +
						"</td>" +
						"<td class='left-align'>" +
							"<textarea class='table-textarea h-full edit_mode required' name='studySubject'></textarea>" +
						"</td>" +
						"<td class='left-align'></td>" +
						"<td class='left-align'>" +
							"<button class='btn-sm-full btn-outline btn_save' data-id='${scheduleMngVO.plId}' type='button'>저장</button>" +
							"<button class='btn-sm-full btn-outline-gray mt-5 btn_del' type='button'>삭제</button>" +
						"</td>" +
					"</tr>";
			</c:when>
			<c:otherwise>
				html = "<tr class='scheduleTr'>" +
							"<td scope='row'>" +
								"<input type='text' name='sisu' class='wid50p sisu required' />" +
								"<input type='hidden' name='crclId' value='${curriculumVO.crclId}'/>" +
							"</td>" +
							"<td class='left-align'>" +
								"<input type='text' name='startDt' class='ell date datepicker btn_calendar edit_mode required' autocomplete='off' readonly='readonly' placeholder='수업일'/>" +
							"</td>" +
							"<td class='left-align'>" +
								"<select class='table-select select2 startTime edit_mode required' name='startTime' data-select='style1' data-placeholder='시작시간'>" +
								startTimeHtml +
								"</select>" +
							"</td>" +
							"<td class='left-align'>" +
								"<select class='table-select select2 endTime edit_mode required' name='endTime' data-select='style1' data-placeholder='종료시간'>" +
									endTimeHtml +
								"</select>" +
							"</td>" +
							"<td class='left-align box_period'></td>" +
							"<td class='left-align'>" +
								"<div class='flex-row'>" +
				                "<div class='flex-col-12'><select class='table-select select2 search fac edit_mode required' data-select='style1' data-placeholder='교원검색'>" +
				                	facHtml +
				                  "</select></div>" +
				                "<div class='flex-col-12'>" + 
				                  "<ul class='selected-items-wrap row-full box_fac'></ul></div>" +
							"</td>" +
							"<td class='left-align'>" +
								"<textarea class='table-textarea h-full edit_mode required' name='studySubject'></textarea>" +
							"</td>" +
							"<td class='left-align'></td>" +
							"<td class='left-align'>" +
								"<button class='btn-sm-full btn-outline btn_save' data-id='${scheduleMngVO.plId}' type='button'>저장</button>" +
								"<button class='btn-sm-full btn-outline-gray mt-5 btn_del' type='button'>삭제</button>" +
							"</td>" +
						"</tr>";
			</c:otherwise>
		</c:choose>
		
				
		$(".box_timetable").append(html);
		Lms.common_ui.select2_init($(".select2"));
	});
	
	//출석
	$(".atd").click(function(){
		var id = $(this).data("id"),
			sbj = $(this).data("nm"),
			crclId = $(this).data("crclid"),
			attTCnt = $(this).data("tot"),
			attYCnt = $(this).data("ycnt"),
			attLCnt = $(this).data("lcnt"),
			attNCnt = $(this).data("ncnt");
		
		$.ajax({
			url : "/lms/atd/selectAttendList.do"
			, type : "post"
			, dataType : "html"
			, data : {plId : id, crclId : crclId, modalAt : "Y"}
			, success : function(data){
				$("#sbj").text(sbj);
				$("#plId").val(id);
				$("#atdlist").html(data);
				
				$("#goAtd").attr("href", "/lms/atd/selectAttendList.do?menuId=MNU_0000000000000084&crclId=" + crclId + "&plId=" + id + "&step=5&tabType=T");
			}, error : function(){
				alert("error");
			}
		});
		
		return false;
	});
	
});

//시작시간
$(document).on("change", ".startTime", function(){
	var sisu = $(this).parents("tr").find(".sisu").val(),
		startTime = $(this).val(),
		startP = $(this).find("option:selected").data("period"),
		selEq = 0,
		sumSisu = 0,
		cnt = 0,
		period = "";
	
	if(!sisu){
		sisu = 1;
	}
	
	sumSisu = parseInt(startP) + parseInt(sisu);
	selEq = sumSisu - 1;
	
	$(this).parents("tr").find(".endTime option:eq("+selEq+")").prop("selected", true);
	for(i = startP; i < sumSisu; i++){
		if(cnt == 0){
			period = i;
		}else{
			period += "," + i;
		}
		cnt++;
	}
	
	period += "<input type='hidden' name='periodTxt' value='"+period+"'/>";
	$(this).parents("tr").find(".box_period").html(period);
	
	sisuTotCnt();
	Lms.common_ui.select2_init($(".select2"));
	return false;
});

//종료시간
$(document).on("change", ".endTime", function(){
	var startTime = $(this).parents("tr").find(".startTime").val(),
		startP = $(this).parents("tr").find(".startTime option:selected").data("period"),
		endP = $(this).find("option:selected").data("period"),
		selEq = 0,
		sumSisu = 0,
		cnt = 0,
		period = "";
	
	sumSisu = parseInt(endP) - parseInt(startP) + 1;
	$(this).parents("tr").find(".sisu").val(sumSisu);
	for(i = startP; i < (endP + 1); i++){
		if(cnt == 0){
			period = i;
		}else{
			period += "," + i;
		}
		cnt++;
	}
	
	period += "<input type='hidden' name='periodTxt' value='"+period+"'/>";
	$(this).parents("tr").find(".box_period").html(period);
	
	sisuTotCnt();
	Lms.common_ui.select2_init($(".select2"));
	return false;
});

//교수명
$(document).on("change", ".fac", function(){
	var userId = $(this).val(),
		userNm = $(this).find("option:selected").data("nm"),
		cnt = 0;
	
	if(userId){
		$(this).siblings(".box_fac").find("input[name=facIdList]").each(function(){
			if($(this).val() == userId){
				cnt++;
				return false;
			}
		});
		
		if(cnt == 0){
			html = "<li class='item bg-blue-light'>"+userNm+" <input type='hidden' name='facIdList' value='"+userId+"'/> <button type='button' class='btn-remove btn_delfac edit_mode'>삭제</button></li>";
			$(this).parents("tr").find(".box_fac").append(html);
		}else{
			alert("이미 등록된 교원입니다.");
		}
	}
});

//교수명 삭제
$(document).on("click", ".btn_delfac", function(){
	$(this).parents("li").remove();
	return false;
});

//수정
$(document).on("click", ".btn_edit", function(){
	$(this).parents(".box_btn").hide().next().show();
	
	$(this).parents("tr").find(".view_mode").hide();
	$(this).parents("tr").find(".edit_mode").show();
	
	Lms.common_ui.select2_init($(".select2"));
	
	return false;
});

//삭제
$(document).on("click", ".btn_del", function(){
	var id = $(this).data("id"),
		crclId = $(this).data("crclid");
	$(this).parents("tr").remove();
	
	if(id){
		$.ajax({
			url : "/mng/lms/manage/dltSchedule.json"
			, type : "post"
			, dataType : "json"
			, data : {plId : id, crclId : crclId}
			, success : function(data){
				console.log(data.successYn);
			}, error : function(){
				alert("error");
			}
		}).done(function(){
			sisuTotCnt();
		});
	}else{
		sisuTotCnt();
	}
	return false;
});

//시수저장&수정
$(document).on("click", ".btn_save", function(){
	var id = $(this).data("id"),
		params = $(this).parents(".scheduleTr").find(":input").serialize(),
		tr = $(this).parents(".scheduleTr"),
		url = "/lms/manage/scheduleReg.do?crclLang=${curriculumVO.crclLang}",
		curTime = parseInt($(".curTime").text()),
		totalTime = parseInt($(".totalTime").text()),
		sisu = $(this).parents(".scheduleTr").find("input[name=sisu]").val(),
		startDate = $(this).parents(".scheduleTr").find("input[name=startDt]").val(),
		startDateReplace = startDate.replace(/-/gi, ""),
		startString = "${curriculumVO.startDate}".replace(/-/gi, ""),
		endString = "${curriculumVO.endDate}".replace(/-/gi, ""),
		
		<c:choose>
			<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
				startTimeHH = $(this).parents(".scheduleTr").find("select[name=startTimeHH]").val(),
				startTimeMM = $(this).parents(".scheduleTr").find("select[name=startTimeMM]").val(),
				endTimeHH = $(this).parents(".scheduleTr").find("select[name=endTimeHH]").val(),
				endTimeMM = $(this).parents(".scheduleTr").find("select[name=endTimeMM]").val(),
				periodTxt = $(this).parents(".scheduleTr").find("input[name=periodTxt]").val();
			</c:when>
			<c:otherwise>
				startTime = $(this).parents(".scheduleTr").find("select[name=startTime]").val(),
				endTime = $(this).parents(".scheduleTr").find("select[name=endTime]").val();
			</c:otherwise>
		</c:choose>
		
		
	
	if(curTime <= totalTime){
		if(!startDate){
			alert("수업일을 선택해 주세요.");
		}else if(startDateReplace < startString || startDateReplace > endString){
			alert("수업일을 확인해 주세요.");
		<c:choose>
			<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
				}else if(!startTimeHH){
					alert("시작시간을 선택해 주세요.");
				}else if(!startTimeMM){
					alert("시작시간 분을 선택해 주세요.");
				}else if(!endTimeHH){
					alert("종료시간을 선택해 주세요.");
				}else if(!endTimeMM){
					alert("종료시간 분을 선택해 주세요.");
				}else if((startTimeHH + "" + startTimeMM) >= (endTimeHH + "" + endTimeMM)){
					alert("종료시간이 시작시간보다 앞 시간입니다.");
				}else if(!periodTxt){
					alert("교시를 입력해 주세요.");
			</c:when>
			<c:otherwise>
				}else if(!startTime){
					alert("시작시간을 선택해 주세요.");
				}else if(!endTime){
					alert("종료시간을 선택해 주세요.");
			</c:otherwise>
		</c:choose>
		}else if(!sisu || sisu == 0){
			alert("시수를 입력해 주세요.");
		}else{
			$.ajax({
				url : url
				, type : "post"
				, dataType : "html"
				, data : params
				, success : function(data){
					tr.html(data);
				}, error : function(){
					alert("error");
				}
			});
			$(this).parents(".box_save").hide().prev().show();
		}
	}else{
		alert("총 시수 " + totalTime + "시간까지 시간표 등록이 가능합니다.");
	}
	
	return false;

});

//시수
$(document).on("change", ".sisu", function(){
	var _pattern2 = /^\d*[.]\d{2}$/;
	if(_pattern2.test($(this).val())){
		alert("소수점 첫번째 자리까지만 입력가능합니다.");
		return false;
	}
	sisuTotCnt();
});

//달력
$(document).on("focus", ".btn_calendar", function(){
	$(this).datepicker({
	    dateFormat: "yy-mm-dd"
	});
});

//시수 계산
function sisuTotCnt(){
	var sisuCnt = 0;
	
	//총 시수
	$(".sisu").each(function(){
		sisuCnt += parseFloat($(this).val());
/* 		sisuCnt += Number($(this).val()); */
	});
	
	$(".curTime").text(sisuCnt);
}

//출석 검색
function searchAtd(){
	var params = $("#atdFrom").serialize();
	
	$.ajax({
		url : "/lms/atd/selectAttendList.do"
		, type : "post"
		, dataType : "html"
		, data : params
		, success : function(data){
			$("#atdlist").html(data);
		}, error : function(){
			alert("error");
		}
	});
	
	return false;
}

</script>

          <div class="page-content-header">
            <c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
			<!-- 
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
          <section class="page-content-body">
            <article class="content-wrap">
            
              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
              	<c:choose>
              		<c:when test="${not empty param.step}">
              			<c:set var="step" value="${param.step}"/>
              		</c:when>
              		<c:otherwise>
              			<c:set var="step" value="4"/>
              		</c:otherwise>
              	</c:choose>
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="${step}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
			
            </article>
            <article class="content-wrap">
              <div class="content-header">
              	<%-- 
                <div class="class-tab-wrap">
                  <a href="/lms/manage/schedule.do${_BASE_PARAM}" class="title on">수업관리</a>
                  <a href="/lms/manage/scheduleCalendar.do${_BASE_PARAM}" class="title">달력관리</a>
                </div>
                 --%>
                <!-- 
                <div class="btn-group-wrap">
                  <div class="right-area">
                    <a href="#" class="btn-md btn-outline">엑셀 업로드</a>
                    <a href="#" class="btn-md btn-outline">엑셀 다운로드</a>
                  </div>
                </div>
                 -->
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:150px'>
                    <col style='width:12%'>
                    <col style='width:12%'>
                    <col style='width:7%'>
                    <col style='width:13%'>
                    <col style='width:13%'>
                    <col style='width:9%'>
                    <col style='width:10%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-gray font-700'>
                      <th scope='col'>시수</th>
                      <th scope='col'>수업일</th>
                      <th scope='col'>수업시작</th>
                      <th scope='col'>종료시간</th>
                      <th scope='col'>교시</th>
                      <th scope='col'>교수명</th>
                      <th scope='col'>수업주제</th>
                      <th scope='col'>출석</th>
                      <th scope='col'>관리</th>
                    </tr>
                  </thead>
                  <tbody class="box_timetable">
                  	<c:set var="curTime" value="0"/>
                  	<c:set var="todayCurTime" value="0"/>
	  				<c:forEach var="scheduleMngVO" items="${resultList}">
	  					<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
	                    <tr class="scheduleTr <c:if test="${today > startDt}">bg-light-gray</c:if>">
	                      <td scope='row'>
	                      	<div class="edit_mode" style="display:none;">
								<input type="number" name="sisu" class="wid50p onlyNum sisu required" value="${scheduleMngVO.sisu}" required/>
								<input type="hidden" name="crclId" value="<c:out value="${curriculumVO.crclId}"/>"/>
								<input type="hidden" name="plId" value="<c:out value="${scheduleMngVO.plId}"/>"/>
							</div>
							<div class="view_mode"><c:out value="${scheduleMngVO.sisu}"/></div>
	                      </td>
	                      <td class='left-align'>
	                      	<div class="edit_mode" style="display:none;">
								<input type="text" name="startDt" class="btn_calendar date edit_mode datepicker required" value="${scheduleMngVO.startDt}" autocomplete="off" readonly="readonly"/>
							</div>
							<div class="view_mode"><c:out value="${scheduleMngVO.startDt}"/></div>
	                      </td>
	                      <td class='left-align'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="edit_mode" style="display:none;">
										<select class="table-select select2 startTimeHH required" name="startTimeHH" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="6" end="24" step="1">
												<c:set var="hour">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
											</c:forEach>
										</select>
										:
										<select class="table-select select2 startTimeMM required" name="startTimeMM" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="0" end="55" step="5">
												<c:set var="minute">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div class="edit_mode" style='display:none;'>
				                      	<select class="table-select select2 startTime required" name="startTime" data-select='style1' data-placeholder='시작시간'>
											<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.startTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.startTime eq result.startTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.startTime,0,2)}"/>:<c:out value="${fn:substring(result.startTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</c:otherwise>
							</c:choose>
							<div class="view_mode">
								<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
							</div>
	                      </td>
	                      <td class='left-align'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="edit_mode" style="display:none;">
										<select class="table-select select2 endTimeHH required" name="endTimeHH" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="6" end="24" step="1">
												<c:set var="hour">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.endTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
											</c:forEach>
										</select>
										:
										<select class="table-select select2 endTimeMM required" name="endTimeMM" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="0" end="55" step="5">
												<c:set var="minute">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.endTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div class="edit_mode" style='display:none;'>
				                      	<select class="table-select select2 endTime required" name="endTime" data-select='style1' data-placeholder='종료시간'>
				                      		<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.endTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.endTime eq result.endTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.endTime,0,2)}"/>:<c:out value="${fn:substring(result.endTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</c:otherwise>
							</c:choose>
							<div class="view_mode">
								<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
							</div>
	                      </td>
	                      <td class='left-align box_period'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="view_mode">
										<c:out value="${scheduleMngVO.periodTxt}"/>
									</div>
									<input type='text' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>' class="edit_mode" style="display:none;"/>
								</c:when>
								<c:otherwise>
									<c:out value="${scheduleMngVO.periodTxt}"/>
									<input type='hidden' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>'/>
								</c:otherwise>
							</c:choose>
	                      </td>
	                      <td class='left-align'>
	                      	<div class='flex-row edit_mode' style='display:none;'>
	                          <div class='flex-col-12' >
		                          	<select class='table-select select2 search fac required' data-select='style1' data-placeholder='교원검색'>
			                             <option value="">선택</option>
			                             <c:forEach var="result" items="${facList}">
											<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
										 </c:forEach>
		                            </select>
	                            </div>
	                            <div class='flex-col-12'>
		                            <ul class='selected-items-wrap row-full box_fac'>
		                            	<c:set var="facCnt" value="1"/>
		                            	<c:forEach var="result" items="${facPlList}" varStatus="status">
											<c:if test="${result.plId eq scheduleMngVO.plId}">
				                              <li class='item bg-blue-light'>
				                              		<span><c:out value="${result.userNm}"/><c:if test="${facCnt ne 1}">(부교원)</c:if></span>
				                              		<input type='hidden' name='facIdList' value='${result.facId}'/> 
				                              		<button type='button' class='btn-remove btn_delfac edit_mode' style="display:none;">삭제</button>
				                              </li>
				                              <c:set var="facCnt" value="${facCnt + 1}"/>
				                              </c:if>
			                              </c:forEach>
		                            </ul>
		                          </div>
	                        </div>
	                        <div class="view_mode">
	                        	<input type='hidden' name='changeFacAt' value='Y'/>
								<ul>
	                            	<c:set var="facCnt" value="1"/>
	                            	<c:forEach var="result" items="${facPlList}" varStatus="status">
										<c:if test="${result.plId eq scheduleMngVO.plId}">
											<li>
												<span>
													<c:out value="${result.userNm}"/>
													<c:if test="${facCnt ne 1}">(부교원)</c:if>
												</span> 
											</li>
											<c:set var="facCnt" value="${facCnt + 1}"/>
										</c:if>
									</c:forEach>
                            	</ul>
						    </div>
	                      </td>
	                      <td class='left-align'>
	                      	<textarea class="wid80 edit_mode required" name="studySubject" style="display:none;"><c:out value="${scheduleMngVO.studySubject}"/></textarea>
							<div class="view_mode"><c:out value="${scheduleMngVO.studySubject}"/></div>
	                      </td>
	                      <td class='left-align'>
	                      	<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
							<c:choose>
								<c:when test="${today >= startDt}">
									<c:choose>
			                      		<c:when test="${not empty scheduleMngVO.attTCnt and scheduleMngVO.attTCnt == scheduleMngVO.attYCnt}">전원출석</c:when>
			                      		<c:otherwise>
			                      			<a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${scheduleMngVO.plId}" data-nm="${scheduleMngVO.studySubject}" data-crclid="${curriculumVO.crclId}" data-lcnt="${scheduleMngVO.attLCnt}" data-ncnt="${scheduleMngVO.attNCnt}" data-tot="${scheduleMngVO.attTCnt}" data-ycnt="${scheduleMngVO.attYCnt}">
				                      			<c:if test="${scheduleMngVO.attLCnt > 0}">지각 <c:out value="${scheduleMngVO.attLCnt}"/></c:if>
				                      			<c:if test="${scheduleMngVO.attLCnt > 0 and scheduleMngVO.attNCnt > 0}">,</c:if>
				                      			<c:if test="${scheduleMngVO.attNCnt > 0}">결석 <c:out value="${scheduleMngVO.attNCnt}"/></c:if>
			                      			</a>
			                      		</c:otherwise>
			                      	</c:choose>
								</c:when>
								<c:otherwise>
									대기
								</c:otherwise>
							</c:choose>
	                      </td>
	                      <td class='left-align'>
							<c:choose>
								<c:when test="${today > startDt}">
									종료
								</c:when>
								<c:otherwise>
									<div class="box_btn">
										<button class='btn-sm-full btn-outline-gray btn_edit' type='button'>수정</button>
										<button class='btn-sm-full btn-outline-gray mt-5 btn_del' data-id='${scheduleMngVO.plId}' data-crclid='${curriculumVO.crclId}' type='button'>삭제</button>
									</div>
									<div class="box_save" style="display:none;">
										<button class='btn-sm-full btn-outline btn_save' data-id='${scheduleMngVO.plId}' type='button'>저장</button>
										<button class='btn-sm-full btn-outline-gray mt-5 btn_del' data-id='${scheduleMngVO.plId}' data-crclid='${curriculumVO.crclId}' type='button'>삭제</button>
									</div>
								</c:otherwise>
							</c:choose>
	                      </td>
	                    </tr>
	                    <c:set var="curTime" value="${curTime + scheduleMngVO.sisu}"/>
	                    <c:if test="${today >= startDt}">
	                    	<c:set var="todayCurTime" value="${todayCurTime + scheduleMngVO.sisu}"/>
	                    </c:if>
                    </c:forEach>
                  </tbody>
                  <tr class="bg-point-light">
                    <td scope='row' class='btn-add-list btn_addTime' colspan='9'>시수 추가</td>
                  </tr>
                  <tr class="box_sum">
                  	<c:choose>
                  		<c:when test="${curriculumVO.totalTime == 0 || todayCurTime == 0}">
                  			<c:set var="curPercent" value="0"/>
                  		</c:when>
                  		<c:otherwise>
                  			<fmt:parseNumber var="curPercent" integerOnly="true" value="${(todayCurTime / curriculumVO.totalTime) * 100 }"/>
                  		</c:otherwise>
                  	</c:choose>
                    <td scope='row' colspan='9'><span class='font-point font-700 curTime'>${curTime}</span> / <span class="totalTime">${curriculumVO.totalTime}</span> 시수 (과정 진도율 <span class='font-point font-700'>( ${curPercent}%)</span>)</td>
                  </tr>
                  
			</tr>
                </table>
              </div>
            </article>
          </section>

</div>
</div>
</div>
</div>

<div id="attend_view_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">출석확인</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">[<span id="sbj"></span>]</div>
        <form name="atdFrom" id="atdFrom" onsubmit="return searchAtd();">
        	<input type="hidden" name="crclId" value="${curriculumVO.crclId }"/>
        	<input id="plId" type="hidden" name="plId" value=""/>
        	<input id="modalAt" type="hidden" name="modalAt" value="Y"/>
	        <div class="flex-row-ten">
	        	<div class="flex-ten-col-3">
	              <select name="attentionType" class="table-select select2 select2-hidden-accessible" data-select="style1" tabindex="-1" aria-hidden="true">
	                  <option value="">전체</option>
	                  <option value="Y">출석</option>
	                  <option value="NY">지각 및 결석자</option>
	                </select>
	            </div>
	            <div class="flex-ten-col-5">
	              <div class="ell">
	                <input type="text" name="userNm" value="" placeholder="이름을 입력해 주세요.">
	              </div>
	            </div>
	            <div class="flex-ten-col-2">
	              <div class="ell"><button class="btn-sm font-400 btn-point goods-search-btn" style="height:40px;">검색</button></div>
	            </div>
	        </div>
        </form>
        <div style="display:block;width:100%;height:300px;overflow-y:auto;">
	        <table class="modal-table-wrap size-sm center-align">
	          <colgroup>
	            <col width="10%">
	            <col width="30%">
	            <col width="*">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray">
	              <th class="font-700">No</th>
	              <th class="font-700">상태</th>
	              <th class="font-700">이름</th>
	            </tr>
	          </thead>
	          <tbody id="atdlist"></tbody>
	        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
        <a href="#" id="goAtd" class="btn-xl btn-point btnModalConfirm" target="_blank">출석 수정</a>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>