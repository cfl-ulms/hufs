<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="_L_IMG"
	value="${pageContext.request.contextPath}/template/lms/imgs" />
<c:set var="CML" value="/template/lms" />

<%
	/*URL 정의*/
%>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId }" />
	<c:if test="${not empty searchVO.searchCrclYear}">
		<c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" />
	</c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}">
		<c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" />
	</c:if>
	<c:if test="${not empty searchVO.searchApplyStartDate}">
		<c:param name="searchApplyStartDate"
			value="${searchVO.searchApplyStartDate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchApplyEndDate}">
		<c:param name="searchApplyEndDate"
			value="${searchVO.searchApplyEndDate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchCrclNm}">
		<c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" />
	</c:if>
	<c:if test="${not empty searchVO.searchStartDate}">
		<c:param name="searchStartDate" value="${searchVO.searchStartDate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchEndDate}">
		<c:param name="searchEndDate" value="${searchVO.searchEndDate}" />
	</c:if>
	<c:if test="${not empty searchVO.searchUserNm}">
		<c:param name="searchUserNm" value="${searchVO.searchUserNm}" />
	</c:if>
	<c:if test="${not empty searchVO.myCurriculumPageFlag}">
		<c:param name="myCurriculumPageFlag"
			value="${searchVO.myCurriculumPageFlag}" />
	</c:if>
	<c:if test="${not empty searchVO.searchPlanSttusCode}">
		<c:param name="searchPlanSttusCode"
			value="${searchVO.searchPlanSttusCode}" />
	</c:if>
</c:url>
<%
	/*URL 정의*/
%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001" />
	<c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
	$(document)
			.ready(
					function() {
						//전체명단 확정 승인
						$(document)
								.on(
										"click",
										"#confmAcceptBtn",
										function() {
											//수강신청 정원 확인
											var applyMaxCnt = "${curriculumVO.applyMaxCnt}";
											var classStudentCnt = "${curriculumVO.classStudentCnt}";

											/* if(applyMaxCnt != 0 & applyMaxCnt < classStudentCnt) {
											    alert("수강신청 승인 인원이 초과하였습니다.")
											    return false;
											} */

											//가상폼 생성 start------------------------------
											var $form = $('<form></form>');
											$form.attr('id', 'acceptForm');
											$form.attr('method', 'post');
											//가상폼 생성 end--------------------------------

											var confmSttusCode = $("<input type=\"hidden\" id=\"confmSttusCode\" name=\"confmSttusCode\" value=\"1\"/>");
											var crclId = $("<input type=\"hidden\" id=\"crclId\" name=\"crclId\" value=\"${curriculumVO.crclId}\"/>");
											var crclbId = $("<input type=\"hidden\" name=\"crclbId\" value=\"${curriculumVO.crclbId}\"/>");
											var menuId = $("<input type=\"hidden\" name=\"menuId\" value=\"${param.menuId}\"/>");
											var subtabstep = $("<input type=\"hidden\" name=\"subtabstep\" value=\"${param.subtabstep}\"/>");
											var thirdtabstep = $("<input type=\"hidden\" name=\"thirdtabstep\" value=\"${param.thirdtabstep}\"/>");
											var forwardUrl = $("<input type=\"hidden\" id=\"forwardUrl\" name=\"forwardUrl\" value=\"/lms/crm/curriculumStudent.do?menuId=${param.menuId}&crclId=${param.crclId}&crclbId=${param.crclbId}&subtabstep=${param.subtabstep}&thirdtabstep=${param.thirdtabstep}\"/>");

											$form
													.attr('action',
															'/lms/crm/updateProcessSttusCode.do');
											$form.attr('target', '_self');
											$form.append(confmSttusCode)
													.append(crclId).append(
															crclbId).append(
															forwardUrl).append(
															menuId).append(
															subtabstep).append(
															thirdtabstep);

											$(".area").append($form);
											$form.submit();
										});

						//전체명단 확정 취소
						$(document)
								.on(
										"click",
										"#confmCancelBtn",
										function() {
											//가상폼 생성 start------------------------------
											var $form = $('<form></form>');
											$form.attr('id', 'acceptForm');
											$form.attr('method', 'post');
											//가상폼 생성 end--------------------------------

											var confmSttusCode = $("<input type=\"hidden\" id=\"confmSttusCode\" name=\"confmSttusCode\" value=\"0\"/>");
											var crclId = $("<input type=\"hidden\" id=\"crclId\" name=\"crclId\" value=\"${curriculumVO.crclId}\"/>");
											var crclbId = $("<input type=\"hidden\" name=\"crclbId\" value=\"${curriculumVO.crclbId}\"/>");
											var menuId = $("<input type=\"hidden\" name=\"menuId\" value=\"${param.menuId}\"/>");
											var subtabstep = $("<input type=\"hidden\" name=\"subtabstep\" value=\"${param.subtabstep}\"/>");
											var thirdtabstep = $("<input type=\"hidden\" name=\"thirdtabstep\" value=\"${param.thirdtabstep}\"/>");
											var forwardUrl = $("<input type=\"hidden\" id=\"forwardUrl\" name=\"forwardUrl\" value=\"/lms/crm/curriculumStudent.do?menuId=${param.menuId}&crclId=${param.crclId}&crclbId=${param.crclbId}&subtabstep=${param.subtabstep}&thirdtabstep=${param.thirdtabstep}\"/>");

											$form
													.attr('action',
															'/lms/crm/updateProcessSttusCode.do');
											$form.attr('target', '_self');
											$form.append(confmSttusCode)
													.append(crclId).append(
															crclbId).append(
															forwardUrl).append(
															menuId).append(
															subtabstep).append(
															thirdtabstep);

											$(".area").append($form);
											$form.submit();
										});

						//수강명단 최종 확정 모달
						$(document)
								.on(
										"click",
										"#updateConfirmBtn",
										function() {
											$
													.ajax({
														type : "post",
														dataType : "json",
														url : "/mng/lms/crm/selectMemberGroupCnt.json", //관리자 controller를 같이씀.
														data : {
															"crclId" : "${curriculumVO.crclId}"
														},
														success : function(data) {
															var resultList = data.selectMemberGroupCnt[0];
															var textHtml = "";

															//분반과, 조배정을 진행하지 않고
															/*
															if (1 >= resultList["classCnt"]) {
																textHtml = "분반";
															}
															*/
															if (0 >= resultList["groupCnt"]) {
																textHtml = "조배정";
															}
															/*
															if (1 >= resultList["classCnt"]
																	&& 0 >= resultList["groupCnt"]) {
																textHtml = "분반과, 조배정";
															}
															*/
															if (textHtml != "") {
																$(
																		'#updateContent')
																		.html(
																				textHtml
																						+ "을 진행하지 않고, ");
															}
														},
														error : function() {
															alert("관리자에게 문의 바랍니다.");
														}
													});

											$(
													"#dialog-background, .process-update-dialog")
													.show();
										});

						//수강명단 최종 확정 업데이트
						$(".btn_update").click(function() {
							$("#earlyAcceptForm").submit();
						});
					});
</script>

<!-- 콘텐츠헤더 -->
<div class="page-content-header">
	<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
		<c:param name="dateFlag" value="curriculumrequest" />
		<c:param name="curriculumManageBtnFlag" value="Y" />
	</c:import>
</div>
<!-- 콘텐츠바디 -->
<section class="page-content-body">
	<!-- 두번째 tab -->
	<c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
		<c:param name="menu" value="curriculumrequest" />
		<c:param name="tabstep" value="${param.tabstep }" />
		<c:param name="menuId" value="${param.menuId }" />
		<c:param name="crclId" value="${param.crclId}" />
		<c:param name="crclbId" value="${param.crclbId}" />
		<c:param name="managerAt" value="${managerAt}" />
	</c:import>

	<!-- 세번째 tab -->
	<c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
		<c:param name="menu" value="curriculumstudent" />
		<c:param name="tabstep" value="${param.tabstep }" />
		<c:param name="menuId" value="${param.menuId }" />
		<c:param name="crclId" value="${param.crclId}" />
		<c:param name="crclbId" value="${param.crclbId}" />
	</c:import>



	<article class="content-wrap">
		<div class="content-body">
			<!-- 테이블영역-->
			<table class="common-table-wrap ">
				<colgroup>
					<col style='width: 7%'>
					<col style='width: 10%'>
					<col style='width: 7%'>
				</colgroup>
				<thead>
					<tr class='bg-light-gray font-700'>
						<th scope='col'>소속</th>
						<th scope='col'>통계자료</th>
						<th scope='col'>총 인원(${totCnt }명)</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="stats" items="${curriculumMemberStatsList}"
						varStatus="status">
						<tr class="">
							<td>${stats.mngDeptNm }</td>
							<td>${stats.ageNm }</td>
							<td>${stats.ageCnt }명</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</article>
	<article class="content-wrap">
		<form name="frm" method="post" action="/lms/crm/curriculumStudent.do">
			<input type="hidden" name="crclId" value="${curriculumVO.crclId}" />
			<input type="hidden" name="crclbId" value="${curriculumVO.crclbId}" />
			<input type="hidden" name="menuId" value="${param.menuId}" /> <input
				type="hidden" name="subtabstep" value="${param.subtabstep}" /> <input
				type="hidden" name="thirdtabstep" value="${param.thirdtabstep}" />

			<div class="box-wrap mb-40">
				<h3 class="title-subhead">학생 검색</h3>
				<div class="flex-row-ten">
					<div class="flex-ten-col-5">
						<div class="ell">
							<input type="text" name="searchHostCode"
								value="${searchVO.searchHostCode }" placeholder="소속을 검색해보세요." />
						</div>
					</div>
					<div class="flex-ten-col-5">
						<div class="ell">
							<input type="text" name="searchUserNm"
								value="${searchVO.searchUserNm }" placeholder="학생 이름을 검색해보세요." />
						</div>
					</div>
				</div>

				<button class="btn-sm font-400 btn-point mt-20">검색</button>
			</div>
		</form>
	</article>
	<article class="content-wrap">
		<div class="content-header">
			<div class="btn-group-wrap">
				<div class="left-area">
					<!-- TODO : ywkim 기능 처리해야함 -->
					<!-- <a href="#" class="btn-md btn-outline">신청서 다운로드</a>
                    <a href="#" class="btn-md btn-outline">계획서 다운로드</a>
                    <a href="#" class="btn-md btn-outline">엑셀 다운로드</a> -->
				</div>
				<div class="right-area">
					<a
						href="/lms/crm/curriculumGroupView.do${_BASE_PARAM }&crclId=${param.crclId }&crclbId=${param.crclbId }&thirdtabstep=2&subtabstep=4&classCnt=1"
						class="btn-md btn-outline-gray">(선택) 조배정</a> <a
						href="/lms/crm/curriculumClassView.do${_BASE_PARAM }&crclId=${param.crclId }&crclbId=${param.crclbId }&thirdtabstep=3&subtabstep=4"
						class="btn-md btn-outline-gray">(선택) 분반</a>
				</div>
			</div>
		</div>
		<div class="content-body">
			<!-- 테이블영역-->
			<table class="common-table-wrap ">
				<colgroup>
					<col style='width: 7%'>
					<col style='width: 11%'>
					<col style='width: 11%'>
					<col style='width: 11%'>
					<col style='width: 11%'>
					<col style='width: 7%'>
					<col style='width: 7%'>
					<col style='width: 7%'>
					<col style='width: 7%'>
					<col style='width: 11%'>
					<col style='width: 11%'>
					<col style='width: 14%'>
				</colgroup>
				<thead>
					<tr class='bg-light-gray font-700'>
						<th scope='col'>No</th>
						<th scope='col'>소속</th>
						<th scope='col'>이름</th>
						<th scope='col'>생년월일</th>
						<th scope='col'>학번</th>
						<th scope='col'>학년</th>
						<th scope='col'>신청서</th>
						<th scope='col'>계획서</th>
						<th scope='col'>기타파일</th>
						<th scope='col'>반배정</th>
						<th scope='col'>조배정</th>
						<th scope='col'>담당교수</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="student" items="${selectStudentList}"
						varStatus="status">
						<tr class="">
							<td>${totCnt - status.index }</td>
							<td>${student.mngDeptNm }</td>
							<td>${student.userNm }</td>
							<td>${student.brthdy }</td>
							<td>${student.stNumber }</td>
							<td>${student.stGrade }</td>
							<c:url var="aplyFileUrl" value="/cmm/fms/absolutePathFileDown.do">
								<c:param name="filePath" value="CurriculumAll.fileStorePath" />
								<c:param name="fileNm" value="${student.aplyFile }" />
								<c:param name="oriFileNm" value="${student.aplyOriFile }" />
							</c:url>
							<td><c:if test="${not empty student.aplyFile }">
									<a href="${aplyFileUrl }"><i class='icon-download'>다운로드</i></a>
								</c:if></td>
							<c:url var="planFileUrl" value="/cmm/fms/absolutePathFileDown.do">
								<c:param name="filePath" value="CurriculumAll.fileStorePath" />
								<c:param name="fileNm" value="${student.planFile }" />
								<c:param name="oriFileNm" value="${student.planOriFile }" />
							</c:url>
							<td><c:if test="${not empty student.planFile }">
									<a href="${planFileUrl }"><i class='icon-download'>다운로드</i></a>
								</c:if></td>
							<c:url var="etcFileUrl" value="/cmm/fms/absolutePathFileDown.do">
								<c:param name="filePath" value="CurriculumAll.fileStorePath" />
								<c:param name="fileNm" value="${student.etcFile }" />
								<c:param name="oriFileNm" value="${student.etcOriFile }" />
							</c:url>
							<td><c:if test="${not empty student.etcFile }">
									<a href="${etcFileUrl }"><i class='icon-download'>다운로드</i></a>
								</c:if></td>
							<td>${student.classCnt }</td>
							<td>${student.groupCnt }</td>
							<td>${student.manageNm }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</article>
	<c:choose>
		<c:when
			test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
		</c:when>
		<c:when
			test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<div class="page-btn-wrap mt-50">
				<a href="#none" class="btn-xl btn-outline-gray btn_mngTxt"
					id="confmAcceptBtn">전체 명단 확정</a>
			</div>
		</c:when>
		<c:when
			test="${curriculumVO.confmSttusCode eq 1 and curriculumVO.processSttusCodeDate eq 4 and empty param.menuCode}">
			<p class="page-guide-desc mt-50">※ 수강명단을 최종 확정하시면 운영팀에 통보되고 더 이상
				수정할 수 없게 됩니다.</p>
			<div class="page-btn-wrap mt-50">
				<a href="#" class="btn-xl btn-outline-gray btn_mngTxt"
					id="confmCancelBtn">전체 명단 확정 취소</a>
				<button class="btn-xl btn-point btnModalOpen btn_mngTxt"
					id="updateConfirmBtn" data-modal-type="confirm"
					data-modal-header="알림" data-modal-rightbtn="확인">전체 최종 확정</button>
			</div>
		</c:when>
	</c:choose>
</section>
</div>
</div>
</div>
</div>

<div id="confirm_modal" class="alert-modal" style="display: none;">
	<form id="earlyAcceptForm" action="/lms/crm/updateProcessSttusCode.do"
		method="post">
		<input type="hidden" name="crclId" value="${curriculumVO.crclId}" /> <input
			type="hidden" name="crclbId" value="${curriculumVO.crclbId}" /> <input
			type="hidden" name="confmSttusCode" value="2" /> <input type="hidden"
			name="menuId" value="${param.menuId}" /> <input type="hidden"
			name="subtabstep" value="${param.subtabstep}" /> <input type="hidden"
			name="thirdtabstep" value="${param.thirdtabstep}" /> <input
			type="hidden" name="forwardUrl" value="/lms/crm/curriculumStudent.do?menuId=${param.menuId}&crclId=${param.crclId}&crclbId=${param.crclbId}&subtabstep=${param.subtabstep}&thirdtabstep=${param.thirdtabstep}" />
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">알림</h4>
					<button type="button" class="btn-modal-close btnModalClose"></button>
				</div>
				<div class="modal-body">
					<p class="modal-text">
						<span id="updateContent"></span>수강명단을 최종 확정하시겠습니까?
					</p>
					<p class="modal-subtext">(총 ${totCnt}명)</p>
				</div>
				<div class="modal-footer">
					<button type="button"
						class="btn-xl btn-outline-gray btnModalCancel">취소</button>
					<button type="button"
						class="btn-xl btn-point btnModalConfirm btn_update">확인</button>
				</div>
			</div>
		</div>
	</form>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y" />
	<c:param name="shareAt" value="Y" />
	<c:param name="curriculumRequestAt" value="Y" />
</c:import>