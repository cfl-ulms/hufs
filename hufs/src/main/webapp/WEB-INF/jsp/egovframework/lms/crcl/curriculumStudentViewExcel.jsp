<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.utl.fcc.service.EgovDateUtil"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%
	String areaId = request.getParameter("areaId");
	String filenameKo = "학생명단";
	
	String browser = request.getHeader("User-Agent");
	String filename="";
	if (browser.indexOf("MSIE") > -1 || browser.indexOf("Trident") > -1) {
		filename = java.net.URLEncoder.encode(filenameKo, "UTF-8");
	}else{
		filename = new String(filenameKo.getBytes("utf-8"),"ISO8859_1");
	}
	response.setHeader("Content-Disposition", "attachment; filename="+filename+".xls");
	response.setHeader("Content-Description", "JSP Generated Data");
%>
<title><c:out value="${title}"/></title>
<style type="text/css"> 
<!--
body {font-size:10px;margin:0px;font-family:맑은 고딕;}
table {border-collapse: collapse; width:900px; padding:10px;margin-bottom:20px;}
table th {background:#edf3fa;border-top:1px solid #5d99d2;border-bottom:1px solid #b6cce2; font-weight:normal;color:#5e8dbf;font-weight:bold; height:23px;text-align:center;border-right:1px solid #dfdfdf;}
table th.caption {padding-top:10px;width:100%;height:40px;font-size:20px;color:#5e8dbf;font-weight:bold;background:#fff;border:0;}
table td {background:#ffffff;border-bottom:1px solid #dfdfdf; font-size:13px; height:25px;border-right:1px solid #dfdfdf;padding-left:5px;text-align:center;mso-number-format:\@;}
table td.alignL{text-align:left;padding-left:15px;}
.boderN{border:none;text-align:left;}

#info th{width:100px;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;}
#info td{width:auto;text-align:left;padding-left:10px;}
-->
</style>

</head>
<body>
	<table>
		<tbody>
			<tr>
				<td colspan="14" class="boderN">과정명 : <c:out value="${curriculumVO.crclNm}"/></td>
			</tr>
			<tr>
				<td colspan="14" class="boderN"></td>
			</tr>
		</tbody>
	</table>
	<table class="chart_board">
		<thead>
			<tr>
				<th>no</th>
				<th>소속</th>
				<th>이름</th>
				<th>생년월일</th>
				<th>학번</th>
				<th>학년</th>
				<th>전화번호</th>
				<th>이메일</th>
				<th>신청서</th>
				<th>계획서</th>
				<th>기타파일</th>
				<th>반배정</th>
				<th>조배정</th>
				<th>담당교수</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="student" items="${selectStudentList}" varStatus="status">
				<tr>
					<td class="border">${status.count}</td>
					<td class="border">${student.mngDeptNm }</td>
					<td class="border">${student.userNm }</td>
					<td class="border">${student.brthdy }</td>
					<td class="border">${student.stNumber }</td>
					<td class="border">${student.stGrade }</td>
					<td class="border">(+${student.geocode})${fn:substring(student.moblphonNo, 0, 3)}-${fn:substring(student.moblphonNo, 3, 7)}-${fn:substring(student.moblphonNo, 7, 11)}</td>
					<td class="border">${student.emailAdres}</td>
					<c:url var="aplyFileUrl" value="http://${siteInfo.siteUrl}/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${student.aplyFile }"/>
						<c:param name="oriFileNm" value="${student.aplyOriFile }"/>
	 			    </c:url>
	 			    <td class="border"><a href="${aplyFileUrl }">${student.aplyOriFile }</a></td>
					<c:url var="planFileUrl" value="http://${siteInfo.siteUrl}/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${student.planFile }"/>
						<c:param name="oriFileNm" value="${student.planOriFile }"/>
	 			    </c:url>
					<td class="border"><a href="${planFileUrl }">${student.planOriFile }</a></td>
					<c:url var="etcFileUrl" value="http://${siteInfo.siteUrl}/cmm/fms/absolutePathFileDown.do">
						<c:param name="filePath" value="CurriculumAll.fileStorePath"/>
						<c:param name="fileNm" value="${student.etcFile }"/>
						<c:param name="oriFileNm" value="${student.etcOriFile }"/>
	 			    </c:url>
					<td class="border"><a href="${etcFileUrl }">${student.etcOriFile }</a></td>
					<td class="border">${student.classCnt }</td>
					<td class="border">${student.groupCnt }</td>
					<td class="border">${student.manageNm }</td>
				</tr>
			</c:forEach>
			
			<c:if test="${fn:length(selectStudentList) == 0}">
				<tr>
		        	<td colspan="11">검색 데이터가 없습니다.</td>
		      	</tr>
		    </c:if>
	    </tbody>
	</table>
</body>
</html>