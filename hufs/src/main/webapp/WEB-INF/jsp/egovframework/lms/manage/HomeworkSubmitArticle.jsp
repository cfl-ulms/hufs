<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}homeworksubmit/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_JS" value="/template/common/js"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="hwId" value="${searchVO.hwId}"/>
    <c:param name="step" value="${searchVO.step}"/>
    <c:param name="plId" value="${searchVO.plId}"/>
    <c:param name="tabType" value="T"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
function fn_egov_regist() {
    
    tinyMCE.triggerSave();
    
    if($.trim($('#${_EDITOR_ID}').val()) == "") {
        alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
        tinyMCE.activeEditor.focus();
        return false;
    }

    $('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

    <c:if test="${searchVO.registAction eq 'regist'}">
	    if (!confirm('<spring:message code="common.regist.msg" />')) {
	        return false;
	    }
	</c:if>
}

$(document).ready(function(){
	var adfile_config = {
            siteId:"SITE_000000000000001",
            pathKey:"HomeworkSubmit",
            appendPath:"HOMEWORKSUBMIT",
            editorId:"${_EDITOR_ID}",
            fileAtchPosblAt:"Y",
            maxMegaFileSize:100,
            atchFileId:"${homeworkSubmitVO.atchFileId}"
        };
        
    fnCtgryInit('');
    fn_egov_bbs_editor(adfile_config);
    
      //글등록
    $(document).on("click", ".insertHomeworkSubmit, .btnModalConfirm", function() {
        $('#curriculumsubmit').submit();
    });
});
</script>

<div class="page-content-header">
  <c:choose>
      <c:when test="${empty param.plId }">
          <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
              <%-- TODO : ywkim 작업해야함 --%>
              <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
              <c:param name="curriculumManageBtnFlag" value="Y"/>
          </c:import>
      </c:when>
      <c:otherwise>
          <c:choose>
            <c:when test="${not empty param.plId and param.tabType eq 'T'}">
                <c:import url="/lms/claHeader.do" charEncoding="utf-8">
                </c:import>
            </c:when>
            <c:otherwise>
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
            </c:import>
            </c:otherwise>
          </c:choose>
      </c:otherwise>
  </c:choose>
</div>
  <section class="page-content-body">  
    <article class="content-wrap">
        <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
            <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                <c:param name="step" value="${searchVO.step }"/>
                <c:param name="crclId" value="${curriculumVO.crclId}"/>
                <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
            </c:import>
        </c:if>
   </article>
   <article class="content-wrap">
     <div class="card-user-wrap">
       ${curriculumVO.hwCode }
       <c:choose>
           <c:when test="${!empty param.plId }">
               <c:forEach var="user" items="${facPlList}" varStatus="status">
                   <div class="user-icon"><img src="${MembersFileStoreWebPath }${user.photoStreFileNm}" alt="담당 교수" onerror="this.src='/template/lms/imgs/common/icon_user_name.svg'"></div>
	                   <div class="user-info">
	                     <p class="title">
	                                                     담당교수 <b><c:out value="${user.userNm}"/>(<c:out value="${user.mngDeptNm}"/>)</b></p>
	                     <p class="sub-title">문의: ${user.emailAdres }</p>
	                   </div>
               </c:forEach>
           </c:when>
           <c:otherwise>
               <c:forEach var="user" items="${subUserList}" varStatus="status">
		           <div class="user-icon"><img src="${MembersFileStoreWebPath }${user.photoStreFileNm}" alt="담당 교수" onerror="this.src='/template/lms/imgs/common/icon_user_name.svg'"></div>
		           <div class="user-info">
		             <p class="title">
		                                             담당교수 <b><c:out value="${user.userNm}"/>(<c:out value="${user.mngDeptNm}"/>)</b></p>
		             <p class="sub-title">문의: ${user.emailAdres }</p>
		           </div>
		       </c:forEach>
           </c:otherwise>
       </c:choose>
     </div>
   </article>
   <article class="content-wrap">
     <div class="content-body subjectViewContain">
       <section class="subject-view-wrap">
         <article class="subject-title-wrap">
           <h2 class="subject-title">${homeworkVO.nttSj }</h2>
           <div class="board-info-wrap">
             <dl class="item">
               <dt class="title">과제</dt>
               <dd class="desc font-red">${homeworkVO.hwCodeNm }(${fn:substring(homeworkVO.hwTypeNm ,0,2) })</dd>
             </dl>
             <!-- <dl class="item">
               <dt class="title">교수</dt>
               <dd class="desc ">홍길동 교수</dd>
             </dl> -->
             <dl class="item">
               <dt class="title">제출 기간</dt>
               <dd class="desc ">${homeworkVO.closeDate } ${fn:substring(homeworkVO.closeTime,0,2) }:${fn:substring(homeworkVO.closeTime,2,4) }</dd>
             </dl>
           </div>
         </article>
         <article class="subject-content-wrap">
           <div class="board-editor-content">
             <!-- 에디터영역 -->
             <div class="">
               <c:out value="${homeworkVO.nttCn}" escapeXml="false" />
             </div>
           </div>
         </article>

         <!-- 첨부파일 -->
        <c:if test="${not empty homeworkVO.atchFileId}">
            <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
                <c:param name="param_atchFileId" value="${homeworkVO.atchFileId}" />                                 
                <c:param name="imagePath" value="${_IMG }"/>
                <c:param name="commonViewAt" value="Y"/>
            </c:import>
        </c:if>
       </section>
       <div class="subjectToggle" data-open="true">
         <span class="text"></span>
       </div>
     </div>
   </article>
   
   <article class="content-wrap">
	   <!-- 과제 제출 -->
	   <div class="content-header">
	     <div class="title-wrap">
	       <div class="title">
	           <c:choose>
	               <c:when test="${param.registAction eq 'regist'}">과제 제출하기</c:when>
	               <c:when test="${param.registAction eq 'updt'}">과제 재제출하기</c:when>
		           <c:when test="${param.registAction eq 'view'}">제출한 과제</c:when>
		       </c:choose>
           </div>
	     </div>
	   </div>
	   <div class="content-body">
	     <c:choose>
	         <%-- 상세 페이지 --%>
	         <c:when test="${param.registAction eq 'view'}">
	             <div class="card-user-wrap mb-20">
                  <div class="user-icon"><img src="${MembersFileStoreWebPath }${homeworkSubmitVO.photoStreFileNm}" alt="사람 아이콘" onerror="this.src='/template/lms/imgs/common/icon_user_name.svg'"></div>
                  <div class="user-info">
                    <p class="title">제출자 <b>${homeworkSubmitVO.userNm} (${homeworkSubmitVO.groupCnt}조)</b></p>
                  </div>
                </div>
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:80%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>과제명 (국문)</td>
                      <td>${homeworkSubmitVO.nttSj}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과제명 (${curriculumVO.crclLangNm })</td>
                      <td>${homeworkSubmitVO.nttSjLang}</td>
                    </tr>
                    <c:if test="${homeworkVO.hwType eq '2' }">
	                    <tr class="">
	                      <td scope='row' class='font-700'>조 정보</td>
	                      <td>${homeworkSubmitVO.groupCnt}조 (${fn:length(pickStudentList)}명)</td>
	                    </tr>
	                    <tr class="">
	                      <td scope='row' class='font-700'>조 원</td>
	                      <td>
                            <c:forEach var="pickStudentList" items="${pickStudentList}" varStatus="status">
                                ${pickStudentList.userNm }
                                <c:if test="${!status.last}">,</c:if>
                            </c:forEach>
                          </td>
	                    </tr>
	                </c:if>
                    <tr class="">
                      <td scope='row' class='font-700'>제출일</td>
                      <td>
                          <!-- 제출일자 게산을 위한 함수 -->
                          <c:set var="closeDate" value="${fn:replace(homeworkVO.closeDate, '-', '')}${homeworkVO.closeTime }"/>
                          <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                          <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />

                          <fmt:formatDate value="${homeworkSubmitVO.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />
                          <fmt:formatDate value="${homeworkSubmitVO.lastUpdusrPnttm }" pattern="yyyy-MM-dd HH:mm" var="lastUpdusrPnttmView" />

                          <!-- 제출 여부 처리  -->
                          <c:choose>
                              <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
                                  <c:set var="submitAt" value="마감후"/>
                              </c:when>
                              <c:otherwise>
                                 <c:set var="submitAt" value="제출"/>
                              </c:otherwise>
                          </c:choose>
                          
                          ${lastUpdusrPnttmView } 

                          <c:choose>
	                          <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
	                              <fmt:parseDate value="${fn:substring(homeworkVO.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
	                              <fmt:formatDate var="strLastUpdusrPnttm" pattern="yyyy-MM-dd" value="${homeworkSubmitVO.lastUpdusrPnttm }"/>
	                              <fmt:parseDate value="${fn:substring(strLastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
	                              <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
	                              <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
	                               
	                              <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
	                              
	                              <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
	                              <c:if test="${dDay eq 0 }">
	                                  <c:set var="dDay" value="1"/>
	                              </c:if>
	                               
	                              (${submitAt } D+${dDay })
	                          </c:when>
	                          <c:otherwise>(${submitAt })</c:otherwise>
                          </c:choose>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <section class="subject-view-wrap mt-20">
                  <article class="subject-content-wrap">
                    <div class="board-editor-content">
                      <!-- 에디터영역 -->
                      <div class="">
                        <c:out value="${homeworkSubmitVO.nttCn}" escapeXml="false" />
                      </div>
                    </div>
                  </article>

                  <!-- 첨부파일 -->
                  <c:if test="${not empty homeworkSubmitVO.atchFileId}">
                      <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
                          <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" />                                 
                          <c:param name="imagePath" value="${_IMG }"/>
                          <c:param name="commonViewAt" value="Y"/>
                      </c:import>
                  </c:if>
                </section>
	         </c:when>
	         <%-- 등록, 수정 페이지 --%>
	         <c:when test="${param.registAction eq 'regist' or param.registAction eq 'updt'}">
	             <c:choose>
		            <c:when test="${empty homeworkSubmitVO.hwId }"><c:set var="formUrl" value="/lms/manage/insertHomeworkSubmitArticle.do"/></c:when>
		            <c:otherwise><c:set var="formUrl" value="/lms/manage/updateHomeworkSubmitArticle.do"/></c:otherwise>
		         </c:choose>
		         <form:form commandName="curriculumsubmit" name="board" method="post" action="${formUrl }" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
		             <input name="menuId" type="hidden" value="<c:out value='${param.menuId}'/>" />
		             <input id="hwId" name="hwId" type="hidden" value="${homeworkVO.hwId}">
		             <input id="hwId" name="hwsId" type="hidden" value="${homeworkSubmitVO.hwsId}">
		             <input id="atchFileId" name="atchFileId" type="hidden" value="${homeworkSubmitVO.atchFileId}">
		             <input id="fileGroupId" name="fileGroupId" type="hidden" value="${homeworkSubmitVO.atchFileId}">
		             <input type="hidden" name="registAction" value="<c:out value='${param.registAction}'/>"/>
		             <input type="hidden" name="step" value="<c:out value='${searchVO.step}'/>"/>
		             <input type="hidden" name="plId" value="${param.plId}"/>
		             <input type="hidden" name="crclId" value="${param.crclId }" />
		             <input type="hidden" name="hwCode" value="${hwCode }" />
		             <table class="common-table-wrap table-style2 mb-20">
		               <tbody>
		                 <tr>
		                   <th class="title">과제명 (국문) </th>
		                   <td>
		                     <div class="flex-row">
		                       <div class="flex-col-12">
		                         <input type="text" class="table-input" value="${homeworkSubmitVO.nttSj}" name="nttSj" placeholder="과제명을 한글로 입력해주세요.">
		                       </div>
		                     </div>
		                   </td>
		                 </tr>
		                 <tr>
		                   <th class="title">과제명 (${curriculumVO.crclLangNm }) </th>
		                   <td>
		                     <div class="flex-row">
		                       <div class="flex-col-12">
		                         <input type="text" class="table-input" value="${homeworkSubmitVO.nttSjLang}" name="nttSjLang" placeholder="과제명을 해당 언어로 입력해주세요.">
		                       </div>
		                     </div>
		                   </td>
		                 </tr>
		                 <c:if test="${homeworkVO.hwType eq '2' }">
			                 <tr>
			                   <th class="title">조 정보 </th>
			                   <td>
			                     <div class="flex-row">
			                       <div class="flex-col-auto">
			                         <div class="table-data">
			                           ${curriculumMemberVO.groupCnt}조 (${fn:length(pickStudentList)}명) 
			                         </div>
			                       </div>
			                     </div>
			                   </td>
			                 </tr>
			                 <tr>
			                   <th class="title">조 원 </th>
			                   <td>
			                     <div class="flex-row">
			                       <div class="flex-col-auto">
			                         <div class="table-data">
			                             <c:forEach var="pickStudentList" items="${pickStudentList}" varStatus="status">
			                                 ${pickStudentList.userNm }
			                                 <c:if test="${!status.last}">,</c:if>
			                             </c:forEach>
			                         </div>
			                       </div>
			                     </div>
			                   </td>
			                 </tr>
			             </c:if>
		               </tbody>
		             </table>
		             <div>
		                 <textarea id="nttCn" name="nttCn" rows="20" class="cont" style="width:99%">${homeworkSubmitVO.nttCn}</textarea>
		             </div>
		             <!-- 첨부파일 -->
		              <div class="mt-20">
		                <c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
		                    <c:param name="editorId" value="${_EDITOR_ID}"/>
		                    <c:param name="estnAt" value="N" />
		                    <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" />
		                    <c:param name="imagePath" value="${_IMG }"/>
		                    <c:param name="regAt" value="Y"/>
		                    <c:param name="commonAt" value="Y"/>
		                </c:import>
		              </div>
		           </form:form>
	         </c:when>
	     </c:choose>
       </div>
	 </article>
	 <%-- 상세 페이지 --%>
     <c:if test="${(param.registAction eq 'view') and !empty homeworkSubmitVO.fdb }">
         <article class="content-wrap">
           <div class="content-header">
             <div class="title-wrap">
               <div class="title">교수님 피드백</div>
             </div>
           </div>
           <div class="content-body">
             <!-- 테이블영역-->
             <table class="common-table-wrap size-sm left-align">
               <colgroup>
                 <col class='bg-gray' style='width:20%'>
                 <col style='width:80%'>
               </colgroup>
               <tbody>
                 <tr class="">
                   <td scope='row' class='font-700'>점수</td>
                   <td>
                       <c:choose>
                           <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
                               <c:choose>
                                   <c:when test="${homeworkVO.stuOpenAt ne 'Y' and empty homeworkSubmitVO.scr}"></c:when>
                                   <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty homeworkSubmitVO.scr}"><span class="${tdFontColor }">FAIL</span></c:when>
                                   <c:when test="${homeworkSubmitVO.scr eq '100'}"><span class="${tdFontColor }">PASS</span></c:when>
                                   <c:otherwise><span class="${tdFontColor }">FAIL</span></c:otherwise>
                               </c:choose>
                           </c:when>
                           <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
                               <c:choose>
                                   <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty homeworkSubmitVO.scr}">0</c:when>
                                   <c:otherwise>${homeworkSubmitVO.scr }</c:otherwise>
                               </c:choose>
                           </c:when>
                       </c:choose>
                   </td>
                 </tr>
                 <tr class="">
                   <td scope='row' class='font-700'>피드백</td>
                   <td>${homeworkSubmitVO.fdb }</td>
                 </tr>
               </tbody>
             </table>
           </div>
         </article>
     </c:if>
     
	 <div class="page-btn-wrap mt-50">
	   <a href="/lms/manage/homeworkList.do${_BASE_PARAM }" class="btn-xl btn-outline-gray">목록으로</a>
	   <c:choose>
	       <c:when test="${param.registAction eq 'regist' }">
	           <a href="#none" class="btn-xl btn-point insertHomeworkSubmit">제출하기</a>
	       </c:when>
	       <c:when test="${param.registAction eq 'updt' and ((homeworkVO.hwType eq '1' and homeworkSubmitVO.lastUpdusrId eq USER_INFO.id) or (homeworkVO.hwType eq '2' and groupAt eq 'Y'))}">
	           <%-- 개별, 조별에 따른 modal 띄우는거 분기처리 --%>
	           <c:choose>
	               <c:when test="${homeworkVO.hwType eq '1' }"><c:set var="modalClassNm" value="btnModalConfirm"/></c:when>
	               <c:otherwise><c:set var="modalClassNm" value="btnModalOpen"/></c:otherwise>
	           </c:choose>
               <a href="#none" class="btn-xl btn-point ${modalClassNm }" data-modal-type="confirm">수정(재 제출하기)</a>
	       </c:when>
	   </c:choose>
	 </div>
  </section>
</div>

</div>
</div>
</div>

<div id="confirm_modal" class="alert-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">제출하기</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <fmt:formatDate value="${homeworkSubmitVO.lastUpdusrPnttm }" pattern="yyyy-MM-dd HH:mm" var="lastUpdusrPnttmModalView" />
          <p class="modal-text">${lastUpdusrPnttmModalView }에 ${homeworkSubmitVO.userNm}님이 이미 제출한 과제가 있습니다.</p>
          <p class="modal-subtext"><p class="modal-subtext">${homeworkSubmitVO.groupCnt}조 ${homeworkSubmitVO.userNm}의 과제제출 내용을 수정하여, ${homeworkSubmitVO.groupCnt}조의 과제를 다시 제출합니다.<br> 재등록은 과제마감일 전까지만 가능합니다. 등록하시겠습니까?</p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button type="button" class="btn-xl btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>