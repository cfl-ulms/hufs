<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/cla"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId}" />
	<c:param name="crclId" value="${param.crclId}" />
	<c:param name="plId" value="${param.plId}" />
	<c:param name="bbsId" value="${brdMstrVO.bbsId}" />
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.searchClass}"><c:param name="searchClass" value="${searchVO.searchClass}" /></c:if>
	<c:if test="${not empty searchVO.searchGroup}"><c:param name="searchGroup" value="${searchVO.searchGroup}" /></c:if>
	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>
<style>
.backGray{
	background : #ccc;
}

</style>

<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script>
$(document).ready(function(){

	/* $("#searchCtgryId, #searchCrclbId").change(function(){
		var tempCtgryId = $("select[id=searchCtgryId]").val();
		var tempCrclbId = $("select[id=searchCrclbId]").val();
		initCurriculum(tempCtgryId,tempCrclbId );
	}); */


	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
      	$("#searchStartDate").val("");
      	$("#searchEndDate").val("");
		$("input:checkbox").attr("checked", false);
		$("#searchKeyWord").val("");
		/* initCurriculum("",""); */
	});

	$("#searchStartDate, #searchEndDate").datepicker({
		dateFormat: "yy-mm-dd"
	 });

	$("#searchTodayClass").on("click",function(){
		if($(this).prop("checked")){
			var now = new Date();
		    var year = now.getFullYear();
	      	var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
	      	var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
	      	var chan_val = year + '-' + mon + '-' + day;
	      	$("#searchStartDate").val(chan_val);
	      	$("#searchEndDate").val(chan_val);
		}
	});
});

</script>
          <div class="page-content-header">
          	<c:choose>
          		<c:when test="${param.menuId eq 'MNU_0000000000000094' }">
          			<c:import url="/lms/crclHeader.do" charEncoding="utf-8"/>
          		</c:when>
          		<c:otherwise>
          			<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
          		</c:otherwise>
          	</c:choose>
			<!--
            <div class="util-wrap">
              <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
            </div>
             -->
          </div>
          <section class="page-content-body">
          	<c:if test="${curriculumVO.processSttusCode > 0 }">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="7"/>
					<c:param name="menuId" value="${searchVO.menuId}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
            <article class="content-wrap">
            <form id="listForm" name="listForm" method="post">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">게시판 검색</h3>
                <div class="flex-row-ten">
                	<c:choose>
						<c:when test="${brdMstrVO.sysTyCode eq 'CLASS'}">
							<div class="flex-ten-col-3">
			                    <div class="ell">
			                      <select name="searchClass" id="" class="select2" data-select="style3" data-placeholder="반 전체">
			                       <option value='0' ${empty searchVO.searchClass or searchVO.searchClass eq '0' ? 'selected' : '' }>반 전체</option>
									<c:if test="${not empty selectClassList }">
										<c:forEach items="${selectClassList}" var="result">
											<option value="${result.classCnt }" ${searchVO.searchClass eq result.classCnt ? 'selected' : '' }>${result.classCnt }반</option>
										</c:forEach>
									</c:if>
			                      </select>
			                    </div>
		                  	</div>
						</c:when>
						<c:when test="${brdMstrVO.sysTyCode eq 'GROUP'}">
						 <div class="flex-ten-col-3">
		                    <div class="ell">
		                      <select name="searchGroup" id="" class="select2" data-select="style3" data-placeholder="조 전체">
		                        <option selected value='0' ${empty searchVO.searchGroup or searchVO.searchGroup eq '0' ? 'selected' : '' }>전체</option>
								<c:if test="${not empty selectGroupList }">
									<c:forEach items="${selectGroupList}" var="result">
										<option value="${result.groupCnt }"  ${searchVO.searchGroup eq result.groupCnt ? 'selected' : '' }>${result.groupCnt }조</option>
									</c:forEach>
								</c:if>
		                      </select>
		                    </div>
		                  </div>
						</c:when>
					</c:choose>

                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <select name="searchCnd" id="" class="select2" data-select="style3" data-placeholder="게시글 구분">
                        <option value="T" ${empty searchVO.searchCnd or searchVO.searchCnd eq 'T' ? 'selected' : '' }>전체</option>
                        <option value="0" ${searchVO.searchCnd eq '0' ? 'selected' : '' }>제목</option>
                        <option value="2" ${searchVO.searchCnd eq '2' ? 'selected' : '' }>작성자</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4">
                    <div class="ell">
                      <input type="text" name="searchWrd" placeholder="검색어를 입력해보세요." value="${searchVO.searchWrd }">
                    </div>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20">검색</button>
              </div>
              </form>
            </article>

            <article class="content-wrap">

              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
                  	<c:forEach var="result" items="${masterList}">
						<c:url var="masterUrl" value="/lms/cla/curriculumBoardList.do">
							<c:param name="crclId" value="${searchVO.crclId}"/>
							<c:param name="bbsId" value="${result.bbsId}" />
							<c:param name="menuId" value="${param.menuId}" />
							<c:param name="plId" value="${param.plId}" />
						</c:url>
						<li <c:if test="${result.bbsId eq brdMstrVO.bbsId}">class="active"</c:if>>
							<a href="${masterUrl}" class="link">
								<c:choose>
									<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
									<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
								</c:choose>
								<c:out value="${result.bbsNm}"/>
							</a>
						</li>
					</c:forEach>
                </ul>
                </div>

                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
              <!--
              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
                    <li>전체게시판</li>
                    <li>(조 별) 월간 보고서</li>
                    <li>(반 별) 활동 내역</li>
                    <li>(반 별) 영수증 첨부</li>
                    <li>(조 별) 월간 보고서</li>
                    <li>(반 별) 활동 내역</li>
                    <li>(반 별) 영수증 첨부</li>
                    <li>(조 별) 월간 보고서</li>
                    <li>(반 별) 활동 내역</li>
                    <li>(반 별) 영수증 첨부</li>
                  </ul>
                </div>
                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
               -->
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:12%'>
                  <col>
                  <c:choose>
                   	<c:when test="${brdMstrVO.sysTyCode eq 'CLASS'}">
                   		<col style='width:7%'>
                   	</c:when>
                   	<c:when test="${brdMstrVO.sysTyCode eq 'GROUP'}">
                   		<col style='width:7%'>
                   	</c:when>
                   	<%-- <c:otherwise>
                   		<col style='width:7%'>
                   		<col style='width:7%'>
                   	</c:otherwise> --%>
                   </c:choose>
                  <col style='width:12%'>
                  <col style='width:130px'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>구분</th>
                    <th scope='col'>제목</th>
                    <c:choose>
                    	<c:when test="${brdMstrVO.sysTyCode eq 'CLASS'}">
                    		<th scope='col'>반</th>
                    	</c:when>
                    	<c:when test="${brdMstrVO.sysTyCode eq 'GROUP'}">
                    		<th scope='col'>조</th>
                    	</c:when>
                    	<%-- <c:otherwise>
                    		<th scope='col'>반</th>
                    		<th scope='col'>조</th>
                    	</c:otherwise> --%>
                    </c:choose>
                    <th scope='col'>작성자</th>
                    <th scope='col'>등록일</th>
                  </tr>
                </thead>
                <tbody>

                <c:choose>
                	<c:when test="${not empty resultList}">
                		<c:forEach items="${resultList }" varStatus="status" var="result">

                		<c:url var="viewUrl" value="/lms/cla/curriculumBoardView.do${_BASE_PARAM}">
						  	<c:param name="nttNo" value="${result.nttNo}" />
						  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
					    </c:url>
               		 <tr onclick="location.href='${viewUrl}'" class="cursor-pointer">
	                    <td scope='row'>${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}</td>
	                    <c:if test="${not empty brdMstrVO.ctgrymasterId}">
							<td><c:out value="${empty result.ctgryNm ? '전체' :result.ctgryNm }" /></td>
						</c:if>
	                    <td class='title'>
	                      <div class="inner-wrap">
	                      	<span class='text dotdotdot'>${result.nttSj }</span>
	                      	<c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
                      	 		<c:if test="${not empty result.atchFileId}">
						          	<i class='icon-clip ml-10'></i>
						          </c:if>
							</c:if>
	                      </div>
	                    </td>
	                    <c:choose>
	                    	<c:when test="${brdMstrVO.sysTyCode eq 'CLASS'}">
                    			<td>
	                    			<c:if test="${not empty result.classCnt}">
	                    				${result.classCnt} 반
	                    			</c:if>
                    			</td>
	                    	</c:when>
	                    	<c:when test="${brdMstrVO.sysTyCode eq 'GROUP'}">
	                    		<td>
	                    			<c:if test="${not empty result.groupCnt}">
	                    				${result.groupCnt} 조
	                    			</c:if>
                    			</td>
	                    	</c:when>
	                    </c:choose>
	                    <td><c:out value="${result.ntcrNm}"/></td>
					  	<td><fmt:formatDate value="${result.frstRegisterPnttm}"  pattern="yyyy-MM-dd"/></td>
	                  </tr>
	                  </c:forEach>
                	</c:when>
                	<c:otherwise>

                	</c:otherwise>
                </c:choose>
                </tbody>
              </table>
              <div class="right-align mt-20">
              	<c:url var="staristcsUrl" value="/lms/cla/curriculumBoardStatistics.do">
              		<c:param name="bbsId" value="${brdMstrVO.bbsId}"/>
              		<c:param name="crclId" value="${param.crclId}" />
              		<c:param name="menuId" value="${param.menuId}" />
              	</c:url>

               	<a href="${staristcsUrl}" class="btn-outline btn-sm">게시판 통계</a>


                <c:url var="addBoardArticleUrl" value="/lms/cla/curriculumBoardAdd.do${_BASE_PARAM}">
					<c:param name="registAction" value="regist" />
				</c:url>
                <a href="${addBoardArticleUrl}" class="btn-point btn-sm">게시판 등록</a>
              </div>
              <div class="pagination center-align mt-10">
                <div class="pagination-inner-wrap overflow-hidden inline-block">
                  	<c:url var="startUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>


                  	<c:url var="prevUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>


                  	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>

                  	<c:url var="nextUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>

	               	<c:url var="endUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
                </div>
              </div>
            </article>
          </section>
