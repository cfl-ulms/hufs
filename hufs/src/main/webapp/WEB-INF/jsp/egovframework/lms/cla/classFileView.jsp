<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_C_IMG" value="/template/common/images"/>

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/lms/css/common/base.css">
<link rel="stylesheet" href="/template/lms/css/common/common.css">
<link rel="stylesheet" href="/template/lms/css/common/font.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<script>
function fileDown(atchFileId,fileSn ){
	location.href = "/cmm/fms/FileDown.do?atchFileId="+atchFileId+"&fileSn="+fileSn;
}
</script>
</head>

<body class="popup-body">
  <div class="window-popup-wrap file-popup">
    <div class="p-middle-center w-100">
      <div class="modal-body">
		<c:if test="${fileInfo.fileExtNm eq '이미지'}">
			<img src='<c:url value='/cmm/fms/getImage.do'/>?siteId=SITE_000000000000001&amp;appendPath=<c:out value="${param.plId}"/>&amp;atchFileNm=${fileInfo.streFileNm}.${fileInfo.fileExtsn}&amp;fileStorePath=Study.fileStorePath'/>
		</c:if>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-sm btn-point btnWindowClose" onclick="fileDown('${fileInfo.atchFileId}','${fileInfo.fileSn}')">다운로드</button>
      </div>
    </div>
  </div>
</body>
