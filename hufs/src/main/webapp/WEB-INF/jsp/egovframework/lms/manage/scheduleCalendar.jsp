<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.plId}"><c:param name="plId" value="${param.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<link rel="shortcut icon" href="">

  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/table_staff.css?v=2">
  
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/locales-all.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/interaction/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.js?v=1"></script>
  <script src="/template/lms/js/class/fullCalendar_custom.js?v=1"></script>

  

	<script>
	$(document).ready(function(){
		
		$('#btnRegist').click(function(){
			$('#layerPopup').show();
		});

		var calendarEl = document.getElementById('calendar2');

		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
			header: {
			      left: 'prev,next today',
			      center: 'title',
			      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth',
			    },
			    locale: 'ko',
			    navLinks: true, // can click day/week names to navigate views
			    editable: true,
			    eventLimit: true, // allow "more" link when too many events
			    events: [
			    	<c:forEach var="result" items="${resultList}" varStatus="status">
			    		<c:set var="mySchYn" value="N"/>
			    		<c:forEach var="list" items="${facPlList}" varStatus="status">
				    		<c:if test="${list.crclId eq result.crclId}">
				    			<c:set var="mySchYn" value="Y"/>
				    		</c:if>
			    		</c:forEach>
				    		{
				            title: '${result.studySubject}',
				            start: new Date(<c:out value='${fn:substring(result.startDt, 0,4)}'/>, <c:out value='${fn:substring(result.startDt, 5, 7)}'/>-1, <c:out value='${fn:substring(result.startDt, 8,10)}'/>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">,<c:out value="${fn:substring(result.startTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">, <c:out value="${fn:substring(result.startTime, 2, 4)}"/></c:if>
				            		),
				            end: new Date(<c:out value='${fn:substring(result.endDt, 0,4)}'/>, <c:out value='${fn:substring(result.endDt, 5,7)}'/>-1, <c:out value='${fn:substring(result.endDt, 8,10)}'/>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">,<c:out value="${fn:substring(result.endTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">, <c:out value="${fn:substring(result.endTime, 2, 4)}"/></c:if>
				            		),
				            constraint: 'crclSchedule', // defined below
				            color: <c:choose><c:when test="${mySchYn eq 'Y'}">'#b7ca10'</c:when><c:otherwise>'#ff0000'</c:otherwise></c:choose>
				            , textColor: '#ffffff'
				            , url: '/lms/manage/scheduleCalendarView.do?menuId=MNU_0000000000000062&crclId=${result.crclId}&plId=${result.plId}&plType=crcl&crclLang=${curriculumVO.crclLang }'
				            <c:if test="${result.allDayAt eq 'Y'}">, allDay : true</c:if>
				          },
			    	</c:forEach>

			    ],
			    eventClick: function (arg) {
			      // opens events in a popup window
			    },
			}); 
		   
		calendar.render();

	});
	
	function checkForm(form) {	
		if($('#studySubject').val() == "" || $('#studySubject').val() == null){
			alert("수업주제를 입력하세요.");
			$('#studySubject').focus();
			$('#layerPopup').show();
			return false;
			
		}else{
			<c:choose>
				<c:when test="${registerFlag eq '수정'}">
					if(confirm('<spring:message code="common.update.msg" />')) {
				</c:when>
				<c:otherwise>
					if(confirm('<spring:message code="common.regist.msg" />')) {
						$('#schduleForm').submit();
				</c:otherwise>
			</c:choose>
				return true;
			}else {
				return false;
			}
		}
	}
	</script>
	
          <div class="page-content-header">
            <div class="title-wrap">
              <p class="sub-title">
              	<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
   					<c:if test="${statusCode.code eq curriculumVO.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
				</c:forEach>
              </p>
              <h2 class="title"><c:out value="${curriculumVO.crclbNm}"/></h2>
            </div>
            <!-- 
            <div class="util-wrap">
              <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
            </div>
             -->
          </div>
          <section class="page-content-body">
            <article class="content-wrap">
              <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:choose>
              		<c:when test="${not empty param.step}">
              			<c:set var="step" value="${param.step}"/>
              		</c:when>
              		<c:otherwise>
              			<c:set var="step" value="4"/>
              		</c:otherwise>
              	</c:choose>
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="${step}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
            </article>
            <article class="content-wrap">
              <div class="content-header">
                <div class="class-tab-wrap">
                  <a href="/lms/manage/schedule.do${_BASE_PARAM}" class="title">수업관리</a>
                  <a href="/lms/manage/scheduleCalendar.do${_BASE_PARAM}" class="title on">달력관리</a>
                </div>
              </div>
              <!-- <div class="box-wrap mb-40">
                <h3 class="title-subhead">수업시간표 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <select class="select2" data-select="style3" data-placeholder="언어 선택">
                        <option value=""></option>
                        <option value="0">몽골어</option>
                        <option value="1">스와힐리어</option>
                        <option value="2">우즈베크어</option>
                        <option value="3">이란어</option>
                        <option value="4">말레이인도네시아어</option>
                        <option value="5">터키어</option>
                        <option value="6">태국어</option>
                        <option value="7">포르투갈/브라질어</option>
                        <option value="8">헝가리어</option>
                        <option value="9">폴란드어</option>
                        <option value="10">힌디어</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <select name="" id="" class="select2" data-select="style3" data-placeholder="전체 교육과정">
                        <option value=""></option>
                        <option value="0">000 몽골어 기초회화</option>
                        <option value="1">000 태국어 기초회화</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4">
                    <div class="ell">
                      <input type="text" placeholder="교수명">
                    </div>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20">검색</button>
              </div> -->
            </article>
            <article class="content-wrap">
              <div id='calendar2'></div>
              <div class="right-align mt-20">
                <a id="btnRegist" href="#" class="btn-point btn-sm">수업 등록</a>
              </div>
            </article>
          </section>
        
        <c:set var="now" value="<%=new java.util.Date() %>"/>
		<c:set var="today"><fmt:formatDate value="${now }" pattern="yyyy-MM-dd"/></c:set>
		<!-- 팝업 -->
		  <div id="layerPopup" class="alert-modal" style="display:none;">
		    <div class="modal-dialog modal-top">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">과정 등록</h4>
		          <button type="button" class="btn-modal-close btnModalClose"></button>
		        </div>
		        <form:form commandName="scheduleMngVO" id="schduleForm" name="scheduleMngVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/lms/manage/scheduleCalendarReg.do">
		        <input type="hidden" id="crclId" name="crclId" value="${crclId }"/>
		        <input type="hidden" id="crclLang" name="crclLang" value="${curriculumVO.crclLang }"/>
		        <input type="hidden" id="plType" name="plType" value="crcl"/>
		        <div class="modal-body">
		          <dl class="modal-form-wrap">
		            <dt class="form-title">수업주제</dt>
		            <dd>
		              <input type="text" id="studySubject" name="studySubject" placeholder="수업주제를 입력하세요">
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap schedule-contents">
		            <dt class="form-title">
		              학습성과 (선택)
		              <div class="text-typing"><span class="font-point textCount">0</span>/100</div>
		            </dt>
		            <dd class="form-schedule-wrap">
		              <textarea class="textTyping" id="plCn" name="plCn" maxlength="100" placeholder="학습성과를 입력하세요"></textarea>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">담당 교원</dt>
		            <dd>
		              <select class='table-select select2 search fac' name="facIdList" data-select='style1' data-placeholder='담당 교원을 선택하세요'>
                        <option value="">선택</option>
                        <c:forEach var="result" items="${facList}">
							<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
		 				</c:forEach>
                      </select>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">수업일정</dt>
		            <dd>
		              <input type="text" id="startDt" name="startDt" class="ell date datepicker type2" placeholder="" autocomplete="off">
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">시간</dt>
		            <dd class="form-schedule-wrap schedule-time">
		              <select id="startTimeHH" name="startTimeHH" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              :
		               <select id="startTimeMM" name="startTimeMM" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              <i>~</i>
		              <select id="endTimeHH" name="endTimeHH" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		              :
		               <select id="endTimeMM" name="endTimeMM" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </select>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">장소</dt>
		            <dd class="form-schedule-wrap schedule-time">
		              <select id="campusId" name="campusId" class="select2" data-select="style1" data-placeholder="캠퍼스 선택">
		                <option value="">전체</option>
						<c:forEach var="result" items="${sysCodeList}" varStatus="status">
							<c:if test="${result.ctgryNm ne '대분류'}">
								<option value="${result.ctgryId}" <c:if test="${'' eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
							</c:if>
						</c:forEach>
		              </select>
		              <input id="placeDetail" name="placeDetail" placeholder="상세위치정보"/>
		            </dd>
		          </dl>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
		          <button type="button" class="btn-xl btn-point btnModalConfirm" onclick="return checkForm(document.scheduleMngVO);">확인</button>
		        </div>
		        </form:form>
		      </div>
		    </div>
		  </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>