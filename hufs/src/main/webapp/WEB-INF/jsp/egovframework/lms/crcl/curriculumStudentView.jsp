<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:if test="${not empty searchVO.menuId}"><c:param name="menuId" value="${searchVO.menuId}" /></c:if>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {
	//엑셀파일 다운로드
	$("#btn_excel").click(function(){
		
		$("#excelAt").val("Y");
		$("#frm").submit();
		
		$("#excelAt").val("");
		return false;
	});
	
	//일괄파일 다운로드
	$(".btn_zipFile").click(function(){
		var actionUrl = $("#frm").attr("action");
		
		$("#zipFileAt").val("Y");
		$("#frm").attr("action", "/lms/crm/curriculumStudent.do");
		$("#frm").submit();
		
		$("#zipFileAt").val("");
		$("#frm").attr("action", actionUrl);
		return false;
	});
});
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="N"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
                <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                    <c:param name="step" value="2"/>
                    <c:param name="crclId" value="${curriculumVO.crclId}"/>
                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                </c:import>
            </c:if>
            
            <!-- 세번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="crclstudent"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
            </c:import>

            <article class="content-wrap">
                <form id="frm" name="frm" method="post" action="/lms/crcl/curriculumStudent.do">
                  <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
                  <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
                  <input type="hidden" name="menuId" value="${param.menuId}"/> 
                  <input type="hidden" name="subtabstep" value="${param.subtabstep}"/>
                  <input type="hidden" name="thirdtabstep" value="${param.thirdtabstep}"/>
                  <input type="hidden" id="excelAt" name="excelAt" value=""/>
                  <input type="hidden" id="zipFileAt" name="zipFileAt" value=""/>

                  <div class="box-wrap mb-40">
                    <h3 class="title-subhead">학생 검색</h3>
                    <div class="flex-row-ten">
                      <div class="flex-ten-col-5">
                        <div class="ell">
                          <input type="text" name="searchHostCode" value="${searchVO.searchHostCode }" placeholder="소속을 검색해보세요." />
                        </div>
                      </div>
                      <div class="flex-ten-col-5">
                        <div class="ell">
                          <input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" placeholder="학생 이름을 검색해보세요." />
                        </div>
                      </div>
                    </div>
    
                    <button class="btn-sm font-400 btn-point mt-20">검색</button>
                  </div>
                </form>
            </article>
            
            <article class="content-wrap">
           	  <div class="content-header">
	                <div class="btn-group-wrap">
	                  <div class="right-area">
	                    <a href="#" id="btn_excel" class="btn-outline btn-md">엑셀 다운로드</a>
	                    <c:if test="${curriculumVO.stdntAplyAt eq 'Y'}">
							<a href="#" class="btn_zipFile btn-outline btn-md">파일일괄다운로드</a>
						</c:if>
	                  </div>
	                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:11%'>
                    <col style='width:11%'>
                    <col style='width:11%'>
                    <col style='width:11%'>
                    <col style='width:7%'>
                    <col style='width:7%'>
                    <col style='width:7%'>
                    <col style='width:7%'>
                    <%-- 
                    <col style='width:11%'>
                    <col style='width:11%'>
                     --%>
                     <col style='width:22%'>
                    <col style='width:14%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>소속</th>
                      <th scope='col'>이름</th>
                      <th scope='col'>생년월일</th>
                      <th scope='col'>학번</th>
                      <th scope='col'>학년</th>
                      <th scope='col'>신청서</th>
                      <th scope='col'>계획서</th>
                      <th scope='col'>기타파일</th>
                      <%-- 
                      <th scope='col'>반배정</th>
                      <th scope='col'>조배정</th>
                       --%>
                      <th scope='col'>전화번호<br/>(이메일)</th>
                      <th scope='col'>담당교수</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach var="student" items="${selectStudentList}" varStatus="status">
                      <tr class="">
                          <td>${status.count}</td>
                          <td>${student.mngDeptNm }</td>
                          <td>${student.userNm }</td>
                          <td>${student.brthdy }</td>
                          <td>${student.stNumber }</td>
                          <td>${student.stGrade }</td>
                          <c:url var="aplyFileUrl" value="/cmm/fms/absolutePathFileDown.do">
                              <c:param name="filePath" value="CurriculumAll.fileStorePath"/>
                              <c:param name="fileNm" value="${student.aplyFile }"/>
                              <c:param name="oriFileNm" value="${student.aplyOriFile }"/>
                          </c:url>
                          <td><c:if test="${not empty student.aplyFile }"><a href="${aplyFileUrl }"><i class='icon-download'>다운로드</i></a></c:if></td>
                          <c:url var="planFileUrl" value="/cmm/fms/absolutePathFileDown.do">
                              <c:param name="filePath" value="CurriculumAll.fileStorePath"/>
                              <c:param name="fileNm" value="${student.planFile }"/>
                              <c:param name="oriFileNm" value="${student.planOriFile }"/>
                          </c:url>
                          <td><c:if test="${not empty student.planFile }"><a href="${planFileUrl }"><i class='icon-download'>다운로드</i></a></c:if></td>
                          <c:url var="etcFileUrl" value="/cmm/fms/absolutePathFileDown.do">
                              <c:param name="filePath" value="CurriculumAll.fileStorePath"/>
                              <c:param name="fileNm" value="${student.etcFile }"/>
                              <c:param name="oriFileNm" value="${student.etcOriFile }"/>
                          </c:url>
                          <td><c:if test="${not empty student.etcFile }"><a href="${etcFileUrl }"><i class='icon-download'>다운로드</i></a></c:if></td>
                          <%-- 
                          <td>${student.classCnt }</td>
                          <td>${student.groupCnt }</td>
                           --%>
                          <td>
                          	${fn:substring(student.moblphonNo, 0, 3)}-${fn:substring(student.moblphonNo, 3, 7)}-${fn:substring(student.moblphonNo, 7, 11)}<br/>
                          	(${student.emailAdres})
                          </td>
                          <td>${student.manageNm }</td>
                      </tr>
                    </c:forEach>
                    <c:if test="${fn:length(selectStudentList) == 0}">
                        <tr>
                            <td class="listCenter" colspan="12"><spring:message code="common.nodata.msg" /></td>
                        </tr>
                    </c:if>
                  </tbody>
                </table>
              </div>
            </article>
          </section>
        </div>
      </div>
    </div>
  </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>