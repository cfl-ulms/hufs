<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

	<!-- tab style -->
	<article class="content-wrap">
	    <ul class="tab-wrap sub-tab-style">
	    	<c:choose>
	    		<%-- 과정 게시판--%>
	    		<c:when test="${param.menuId eq 'MNU_0000000000000094'}">
	    			<li class="tab-list on"><a href="/lms/crcl/curriculumBoardList.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&step=6">게시판</a></li>
	    		</c:when>
	    		<c:when test="${param.menuId eq 'MNU_0000000000000096'}">
	    			<li class="tab-list on"><a href="/lms/manage/manageReport.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&step=6">운영보고서</a></li>
	    		</c:when>
	    		<%-- 학생 > 설문 --%>
	    		<c:when test="${param.menuId eq 'MNU_0000000000000163'}">
	    			<li class="tab-list on"><a href="/lms/cla/mySurveyList.do?menuId=${param.menuId}">설문</a></li>
	    		</c:when>
	    		<c:when test="${pageFlag eq 'student' or param.menuId eq 'MNU_0000000000000163' or param.menuId eq 'MNU_0000000000000086' or param.tabType eq 'T'}">
	    			<li class="tab-list <c:if test="${param.step eq '1'}">on</c:if>">
			      		<a href="/lms/crm/CurriculumAllView.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=1&tabType=T">과정계획</a>
			      	</li>
			      	<c:if test="${param.processSttusCodeDate ne '5'}">
				      	<c:if test="${pageFlag eq 'teacher'}">
				      		<li class="tab-list <c:if test="${param.step eq '2'}">on</c:if>"><a href="/lms/manage/schedule.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=2&tabType=T">시간표</a></li>
				      	</c:if>
				      	<li class="tab-list <c:if test="${param.step eq '5'}">on</c:if>"><a href="/lms/manage/studyPlanView.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=5&tabType=T">수업계획</a></li>
		    			<c:if test="${evtId eq 'CTG_0000000000000058'}">
		    				<c:choose>
		    					<c:when test="${pageFlag eq 'teacher'}">
		    						<li class="tab-list <c:if test="${param.step eq '4'}">on</c:if>"><a href="/lms/quiz/QuizList.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=4&tabType=T">평가</a></li>
		    					</c:when>
		    					<c:otherwise>
		    						<li class="tab-list <c:if test="${param.step eq '4'}">on</c:if>"><a href="/lms/quiz/QuizEvalList.do?menuId=${param.menuId}&plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&step=4&tabType=T&thirdtabstep=1">평가</a></li>
		    					</c:otherwise>
		    				</c:choose>
				      	</c:if>
				      	<li class="tab-list <c:if test="${param.step eq '8'}">on</c:if>"><a href="/lms/manage/homeworkList.do?menuId=${param.menuId}&plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&step=8&tabType=T&thirdtabstep=1">과제</a></li>
				      	<c:if test="${pageFlag eq 'teacher'}">
				      		<li class="tab-list <c:if test="${param.step eq '11'}">on</c:if>"><a href="/lms/atd/selectAttendList.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=5&tabType=T">출석</a></li>
				      	</c:if>
				      	<li class="tab-list <c:if test="${param.step eq '7'}">on</c:if>"><a href="/lms/cla/curriculumBoardList.do?menuId=${param.menuId}&plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&step=6">게시판</a></li>
				      	<c:if test="${pageFlag eq 'student' and not empty resultSurveyList}">
				      		<li class="tab-list <c:if test="${param.step eq '10'}">on</c:if>"><a href="/lms/cla/myClassSurveyList.do?menuId=${param.menuId}&plId=<c:out value="${param.plId}"/>&crclId=<c:out value="${param.crclId}"/>&step=10">설문</a></li>
				      	</c:if>
				      	<c:if test="${pageFlag eq 'teacher'}">
				      		<li class="tab-list <c:if test="${param.step eq '9'}">on</c:if>">
				      			<a href="/lms/manage/manageReport.do?menuId=${param.menuId}&crclId=<c:out value="${param.crclId}"/>&plId=${param.plId}&step=9&tabType=T">운영보고서</a>
				      		</li>
				      	</c:if>
			      	</c:if>
	    		</c:when>
	    		<c:otherwise>
	    			<li class="tab-list <c:if test="${param.step eq '1'}">on</c:if>"><a href="/lms/manage/lmsControl.do?crclId=<c:out value="${param.crclId}"/>&menuId=MNU_0000000000000062&step=1">관리</a></li>
	    			<c:if test="${param.processSttusCodeDate ne '5'}">
			      		<li class="tab-list <c:if test="${param.step eq '2'}">on</c:if>"><a href="/lms/crcl/curriculumStudent.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&thirdtabstep=1">학생</a></li>
			      	</c:if>
			      	<li class="tab-list <c:if test="${param.step eq '3'}">on</c:if>"><a href="/lms/crcl/selectCurriculum.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=3">과정계획</a></li>
			      	<c:if test="${param.processSttusCodeDate ne '5'}">
				      	<c:if test="${param.totalTimeAt eq 'Y'}">
						    <li class="tab-list <c:if test="${param.step eq '4'}">on</c:if>"><a href="/lms/manage/schedule.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=4">시간표</a></li>
					      	<li class="tab-list <c:if test="${param.step eq '5'}">on</c:if>"><a href="/lms/manage/studyPlan.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=5">수업계획</a></li>
				      	</c:if>
				      	<li class="tab-list <c:if test="${param.step eq '6'}">on</c:if>"><a href="/lms/crcl/curriculumBoardList.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=6">게시판</a></li>
			      		<li class="tab-list <c:if test="${param.step eq '7'}">on</c:if>">
			      			<a href="/lms/manage/gradeTotal.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=7">성적</a>
			      		</li>
				      	<li class="tab-list <c:if test="${param.step eq '8'}">on</c:if>"><a href="/lms/crcl/homeworkList.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=8&thirdtabstep=1">과제</a></li>
				      	<li class="tab-list <c:if test="${param.step eq '9'}">on</c:if>">
				      		<a href="/lms/manage/manageReport.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=9">운영보고서</a>
				      	</li>
				      	<li class="tab-list <c:if test="${param.step eq '10'}">on</c:if>"><a href="/lms/crcl/curriculumSurveyList.do?menuId=MNU_0000000000000062&step=10&crclId=<c:out value="${param.crclId}"/>">설문</a></li>
			      	</c:if>
	    		</c:otherwise>
	    	</c:choose>
	    </ul>
    </article>
