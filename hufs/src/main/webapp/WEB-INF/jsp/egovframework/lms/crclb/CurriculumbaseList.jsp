<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="MNU_0000000000000057" />
	<c:if test="${not empty searchVO.searchcrclbNm}"><c:param name="searchcrclbNm" value="${searchVO.searchcrclbNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty searchVO.searchProjectAt}"><c:param name="searchProjectAt" value="${searchVO.searchProjectAt}" /></c:if>
	<c:if test="${not empty searchVO.searchTotalTimeAt}"><c:param name="searchTotalTimeAt" value="${searchVO.searchTotalTimeAt}" /></c:if>
	
	<c:if test="${not empty param.tmplatImportAt}"><c:param name="tmplatImportAt" value="${param.tmplatImportAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<script>
$(document).ready(function(){
	//과정코드 생성
	$("input[name=autoIdAt]").click(function(){
		if("Y" == $(this).val()){
			$("#crclbId").prop("readonly", true);
		}else{
			$("#crclbId").prop("readonly", false);
		}
	});
	
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${searchVO.searchSysCode2}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${searchVO.searchSysCode3}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
	});
	
	$("#ctgryId1").change();

});

</script>

		<section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
              	<form name="frm" id="frm" method="post" action="<c:url value="/lms/crclb/CurriculumbaseList.do?menuId=MNU_0000000000000057"/>">
	                <h3 class="title-subhead">기본과정조회 검색</h3>
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="ctgryId1" name="searchSysCode1" class="select2" data-select="style3" data-placeholder="대분류">
							<option value="">대분류</option>
							<c:forEach var="result" items="${sysCodeList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchSysCode1}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="ctgryId2" name="searchSysCode2" class="select2" data-select="style3" data-placeholder="중분류">
							<option value="">중분류</option>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 mb-20">
	                    <div class="ell">
	                      <select id="ctgryId3" name="searchSysCode3" class="select2" data-select="style3" data-placeholder="소분류">
							<option value="">소분류</option>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="division" name="searchDivision" class="select2" data-select="style3" data-placeholder="이수구분">
							<option value="">이수구분 선택</option>
							<c:forEach var="result" items="${divisionList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchDivision}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="control" name="searchControl" class="select2" data-select="style3" data-placeholder="관리구분">
							<option value="">관리구분 선택</option>
							<c:forEach var="result" items="${controlList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchControl}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 mb-20">
	                    <div class="ell">
	                      <input type="text" name="searchcrclbNm" value="${searchVO.searchcrclbNm}" id="inp_text" placeholder="기본 과정명">
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select id="targetType" name="searchTargetType" class="select2" data-select="style3" data-placeholder="대상 전체">
							<option value="">대상 선택</option>
							<option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
							<option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select id="targetDetail" name="searchTargetDetail" class="select2" data-select="style3" data-placeholder="과정유형 전체">
							<option value="">선택</option>
							<c:forEach var="result" items="${targetList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.targetDetail}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 flex align-items-center">
	                    <label class="checkbox">
	                    	<input type="checkbox" name="searchProjectAt" value="Y" <c:if test="${searchVO.searchProjectAt eq 'Y'}">checked="checked"</c:if>/>
	                      <span class="custom-checked"></span>
	                      <span class="text">프로젝트 과정</span>
	                    </label>
	                    <label class="checkbox">
	                      <input type="checkbox" name="searchTotalTimeAt" value="Y" <c:if test="${searchVO.searchTotalTimeAt eq 'Y'}">checked="checked"</c:if>/>
	                      <span class="custom-checked"></span>
	                      <span class="text">총 시수 과정</span>
	                    </label>
	                  </div>
	                </div>
	
	                <button class="btn-sm font-400 btn-point mt-20">검색</button>
	
	                <button type='button' class="btn-sm font-400 btn-outline mt-20" onclick="window.location.href=window.location.href">초기화</button>
                </form>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board p-5">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>대분류</th>
                    <th scope='col'>중분류</th>
                    <th scope='col'>소분류</th>
                    <th scope='col'>이수구분</th>
                    <th scope='col'>관리구분</th>
                    <th scope='col' colspan='2'>대상</th>
                    <th scope='col'>프로젝트<br>과정</th>
                    <th scope='col'>총<br>시수 과정</th>
                    <th scope='col'>기본과정명</th>
                  </tr>
                </thead>
                <tbody>
                	<c:forEach var="result" items="${resultList}" varStatus="status">
                		<tr onclick="location.href='/lms/crclb/CurriculumbaseView.do?crclbId=${result.crclbId}&menuId=MNU_0000000000000057'" class=" cursor-pointer">
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<c:set var="sysPath" value="${fn:split(result.sysCodeNmPath,'>')}"/>
			    			<td><c:out value="${sysPath[1]}"/></td>
			    			<td><c:out value="${sysPath[2]}"/></td>
			    			<td><c:out value="${sysPath[3]}"/></td>
			    			<td><c:out value="${result.divisionNm}"/></td>
			    			<td><c:out value="${result.controlNm}"/></td>
			    			<td>
			    				<c:choose>
			    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
			    					<c:otherwise>일반</c:otherwise>
			    				</c:choose>
			    			</td>
			    			<td><c:out value="${result.targetDetailNm}"/></td>
			    			<td>
		                    	<c:choose>
		                    		<c:when test="${result.projectAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
		                    </td>
		                    <td>
								<c:choose>
		                    		<c:when test="${result.totalTimeAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
							</td>
			    			<td><c:out value="${result.crclbNm}"/></td>
						</tr>
                	</c:forEach>
                </tbody>
              </table>
              <div class="pagination center-align mt-60">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="/lms/crclb/CurriculumbaseList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
				</div>
			</div>
            </article>
          </section>
        </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</form>
