<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000086"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:param name="plId" value="${param.plId }"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){

});
</script>

         <div class="page-content-header">
            <c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
			<!--
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="10"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="menuId" value="MNU_0000000000000086"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
        </article>
		<article class="content-wrap">
			<c:forEach items="${surveyList}" var="result">
				<c:set var="tempTxt" value="${result.schdulClCode eq 'TYPE_1' ? '수업' : '과정'}"/>
				<div class="survey-card-wrap">
	                <a href="../class/class.7.3.html">
	                  <p class="title">${tempTxt} 만족도 설문</p>
	                  <p class="sub-title">
				                    ${tempTxt}이 종료된 후 (result.endDate 부터) 설문에 참여하실 수 있습니다.<br>
				                    ${tempTxt} 만족도 설문을 참여하지 않을 경우 과정종료 후 성적조회 등이 원활하지 않을 수 있습니다.<br>
	                    <b class="font-point">꼭 설문에 참여해 주세요.</b>
	                  </p>
	                </a>
	              </div>
			</c:forEach>
           </article>
            <!-- <div class="page-btn-wrap mt-50">
              <a href="../class/class.7.1.html" class="btn-xl btn-outline-gray">목록으로</a>
            </div> -->
            </article>
          </section>
      </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>