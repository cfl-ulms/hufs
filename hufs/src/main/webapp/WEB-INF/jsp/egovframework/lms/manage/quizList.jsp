<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizRegView.do"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
	$(document).ready(function(){
		<c:if test="${search2.searchDiv eq 'popup'}">
			$('#before_quiz_modal').css("display", "block");
		</c:if>
		
		$('#btnReg').click(function(){
			$('#no_contents_quiz_modal').css("display", "block");
		});
		
		
		$('#btnLoad').click(function(){
			$('#before_quiz_modal').css("display", "block");
		});
		
		
		$('#btnSearch').click(function(){
			$("#detailForm").attr("action", "/lms/quiz/QuizList.do${_BASE_PARAM}");
			$("#searchDiv").val("popup");
			$("#detailForm").submit();
		});
		
		
		$('#btnSelMov').click(function(){
			var radioVal = $('input[name="plidList"]:checked').val();
			if(typeof radioVal == "undefined" || radioVal == null || radioVal == ""){
				alert("퀴즈를 선택해 주십시오.");
				return false;
			}
			var param = radioVal.split(",");
			$("#plId").val(param[0]);
			$("#crclId").val(param[1]);
			$("#quizType").val(param[2]);
			$("#detailForm").attr("action", "/lms/quiz/QuizCopy.do${_BASE_PARAM}"+"&plIdTarget="+param[0]+"&quizType="+param[2]);
			/*
			if(param[2] == "ON"){
				$("#detailForm").attr("action", "/lms/quiz/QuizRegView.do${_BASE_PARAM2}"+"&crclId="+param[1]+"&plIdTarget="+param[0]+"&quizType="+param[2]);
			}else{
				$("#detailForm").attr("action", "/lms/quiz/QuizAnswerRegView.do${_BASE_PARAM2}"+"&crclId="+param[1]+"&plIdTarget="+param[0]+"&quizType="+param[2]);
			}*/
			$("#detailForm").submit();
		});
		
	});
	
	
	//레이어 닫기
	$(document).on("click", ".btnModalClose", function(){
		var id = $(this).parents(".alert-modal").attr("id");
		if(id == "no_contents_quiz_modal"){
			$("#no_contents_quiz_modal").hide();
		}else if(id == "before_quiz_modal" || id == "btn_before_quiz_cancel"){
			$("#before_quiz_modal").hide();
		}else{	
			$(this).parents(".alert-modal").remove();	
		}
	});
	
	
	
	/* 
	$(document).on("click", "#btnLoad", function(){
		var plId = '${searchVO.plId}';
		var plId = '${searchVO.plId}';
		
		$.ajax({
			url : "/lms/quiz/BeforeQuizList.do"
			, type : "post"
			, dataType : "json"
			, data : {plId:plId}
			, success : function(data){
				if(data.successYn == "Y"){
					var btnHtml = "";
					var addHtml = "";
					for(var i=1; i<data.tagList.length; i++){
						 
					}
					
					$('#tagUl').html(btnHtml); // 태그삭제
					//$('#btnTagAdd').html(addHtml); // 태그추가
					
					alert("삭제되었습니다.");
				}
			}, error : function(){
				alert("error");
			}
		});
		return false;
	}); */
</script>

<div class="page-content-header">
	<c:choose>
		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
			<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
		</c:when>
		<c:otherwise>
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
		</c:otherwise>
	</c:choose>
</div>
<section class="page-content-body">
	<article class="content-wrap">
		<!-- tab style -->
		<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="4"/>
			<c:param name="crclId" value="${searchVO.crclId}"/>
			<c:param name="menuId" value="${searchVO.menuId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
    </article>
	<article class="content-wrap">
  		<div class="no-content-wrap">
	    	<p class="sub-title">
	      		온라인 퀴즈를 신규로 등록해주세요.<br>
		      	온라인 퀴즈를 기존에 등록하여 수업하신 적이 있으시다면 [나의 지난 퀴즈 불러오기] 버튼을 누르시면<br>
			      과정별로 등록되었던 퀴즈 이력을 확인하여 다시 사용하실 수 있습니다.
	    	</p>
	    	<div class="mt-30">
			    <a href="#" id="btnReg" class="btn-xl btn-point btnModalOpen" data-modal-type="no_contents_quiz">온라인 퀴즈 신규 등록하기</a>
			    <a href="#" id="btnLoad" class="btn-xl btn-outline btnModalOpen" data-modal-type="before_quiz">나의 지난 퀴즈 불러오기</a>
	    	</div>
  		</div>
	</article>
	
	<div id="no_contents_quiz_modal" class="alert-modal" style="display: none;">
		<div class="modal-dialog modal-top">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">안내</h4>
					<button type="button" class="btn-modal-close btnModalClose"></button>
				</div>
				<div class="modal-body">
					<p class="modal-text">퀴즈를 등록한 적이 없으시네요</p>
					<p class="modal-subtext">온라인 퀴즈는 아래 유형으로 간편하게 퀴즈를 등록하여 진행하실 수 있습니다.</p>
					<div class="no-contents-quiz-wrap">
						<p class="tip-label">Tip!</p>
						<p>교수님, 온라인 퀴즈는 문제를 등록하지 않는 [답안 등록형] 으로도 진행하실 수 있습니다.</p>
						<dl class="quiz-type">
							<dt>01. 답안 등록형</dt>
							<dd class="text">
								문제는 종이나 구두로 대체하시고, 학생들의 답안만 입력하여 평가를 <br>온라인으로 진행 할 수 있습니다.
							</dd>
						</dl>
						<dl class="quiz-type">
							<dt>02. 문제 등록형</dt>
							<dd class="text">문제를 모두 등록하여 별도 설명없이 퀴즈를 진행하실 수 있습니다.</dd>
						</dl>
					</div>
				</div>
				<div class="modal-footer">
					<a href="/lms/quiz/QuizAnswerRegView.do${_BASE_PARAM}&quizType=OFF" class="btn-xl btn-outline btnModalCancel">답안 등록형으로 진행</a> 
					<a href="/lms/quiz/QuizRegView.do${_BASE_PARAM}&quizType=ON" class="btn-xl btn-point btnModalConfirm">문제 등록형으로 진행</a>
				</div>
			</div>
		</div>
	</div>
	
	<form:form commandName="quizVO" name="detailForm" id="detailForm" method="post">
		<input type="hidden" id="searchDiv" name="searchDiv" value=""/>
		<%-- <input type="hidden" id="menuId" name="menuId" value="${searchVO.menuId }"/>
		<input type="hidden" id="plId" name="plId" value=""/> --%>
		
		<div id="before_quiz_modal" class="alert-modal" style="display: none;">
		    <div class="modal-dialog modal-top">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">나의 지난 퀴즈 불러오기</h4>
		          <button type="button" class="btn-modal-close btnModalClose"></button>
		        </div>
		        <div class="modal-body before-quiz-wrap">
		          <p class="modal-text">나의 지난 퀴즈를 선택해주세요.</p>
		          <div class="quiz-search-box">
		            <select name="searchYY" class="select2 select2-hidden-accessible" data-select="style1" data-placeholder="년도 선택" tabindex="-1" aria-hidden="true">
		              <option value=""></option>
		              <c:forEach var="result" begin="2020" end="2025" varStatus="status">
		              	<option value="${result}" <c:if test="${(searchVO.searchYY eq result) or (empty searchVO.searchYY and yyyy eq result)}">selected="selected"</c:if>>${result }</option>
		              </c:forEach>
		            </select>
		            <!-- <span class="select2 select2-container select2-container--style1" dir="ltr" style="width: 100%;">
		            <span class="selection">
		            <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-2ozm-container">
			            <span class="select2-selection__rendered" id="select2-2ozm-container">
			            	<span class="select2-selection__placeholder">년도 선택</span>
			            </span>
		            <span class="select2-selection__arrow" role="presentation">
		            <b role="presentation"></b>
		            </span>
		            </span>
		            </span>
		            <span class="dropdown-wrapper" aria-hidden="true"></span></span> -->
		            <input type="text" placeholder="수업명을 입력해주세요" name="searchQuizNm" value="${searchVO.searchQuizNm}">
		            <button id="btnSearch" type="button" class="btn-sm-110 btn-point">검색</button>
		          </div>
		          <table class="common-table-wrap size-sm">
		            <colgroup>
		              <col style="width:70px;">
		              <col>
		              <col style="width:138px">
		            </colgroup>
		            <c:if test="${fn:length(beforeQuizList) == 0 }">
		            	<thead>
		              		<tr class="bg-gray-light font-700">
		                	<th colspan="3">검색 결과가 존재하지 않습니다.</th>
		              	</tr>
		            </thead>
		            </c:if>
		            <c:forEach var="result" items="${beforeQuizList}" varStatus="status">
		           		<c:if test="${crclGroup ne result.plId or status.count eq 1}">
			            	<thead>
				              <tr class="bg-gray-light font-700">
				              	<fmt:parseDate value="${result.startDate}" var="startDate" pattern="yyyy-MM-dd"/>
								<fmt:formatDate var="cSDate" value="${startDate}" pattern="yyyy-MM-dd"/>
								<fmt:parseDate value="${result.endDate}" var="endDate" pattern="yyyy-MM-dd"/>
								<fmt:formatDate var="cEDate" value="${endDate}" pattern="yyyy-MM-dd"/>
				                <th colspan="3">
				                	${result.crclNm}
				                	<br/>
				                	[${result.studySubject}](${cSDate} ~ ${cEDate})
				                </th>
				              </tr>
				            </thead>
			            </c:if>
			            <tbody>
			              <tr>
			                <td><label class="checkbox circle"><input type="radio" name="plidList" value="${result.plId},${result.crclId},${result.quizType}" /><span class="custom-checked"></span></label></td>
			                <td class="left-align">${result.studySubject } / 온라인 퀴즈
			                <c:choose>
				                <c:when test="${result.quizType eq 'OFF'}">[답안 등록]</c:when>
				                <c:otherwise>[문제 등록]</c:otherwise>
			                </c:choose>
			                <span class="quiz-desc">문제수 : ${result.quizCnt }</span></td>
			                <td>${result.regDate}</td>
			              </tr>
		              </tbody>
		              <c:set var="crclGroup" value="${result.plId}"/>	
		            </c:forEach>
					</table>            
		        </div>
		        <div class="modal-footer">
		          <button type="button" id="btn_before_quiz_cancel" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
		          <button id="btnSelMov" class="btn-xl btn-point btnModalConfirm">선택한 퀴즈 불러오기</button>
		        </div>
		      </div>
		    </div>
	  	</div>
  	</form:form>
</section>
      
<c:import url="/template/bottom.do" charEncoding="utf-8"/>