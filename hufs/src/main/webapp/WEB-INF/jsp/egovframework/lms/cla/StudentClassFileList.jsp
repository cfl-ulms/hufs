<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>
<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId}" />
</c:url>

<script>

$(document).ready(function(){
	//페이징
    $(document).on("click", ".addPage", function() {
        var pageIndex = parseInt($(this).attr("data-pageindex"));
        var clickflag = $(this).attr("data-clickflag");
		$("#pageIndex").val(pageIndex);
        var params    = jQuery("form[id=frm]").serialize();

        //데이터가 더 이상 없을 때 flag 처리
        if(clickflag == "F") {
            alert("더 이상 데이터가 없습니다.");
            return;
        }

        //여러번 호출 방지를 위한 flag 처리
        if(clickflag == "N") {
            alert("빠른 시간에 여러번 호출이 불가능 합니다.");
            return;
        }

        //여러번 호출 방지를 위한 flag 처리
        $(".addPage").attr("data-clickflag", "N");

        $.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/cla/classFileListAjax.do",
            data:params,
            success: function(data) {

                if($.trim(data) == "" || $.trim(data) == null) {
                    alert("더 이상 데이터가 없습니다.");
                    $(".addPage").attr("data-clickflag", "F");
                    return;
                }else{
                	if($("#viewType").val() == "list"){
    	            	$(".content-body tbody").append(data)
    	            }else{
    	            	$(".content-body .board-gallery-thumb-wrap").append(data)
    	            }
                	 //페이징 index 증가
                	++pageIndex
               		$(".addPage").attr("data-pageindex", pageIndex);
                }

                //여러번 호출 방지를 위한 flag 처리
                $(".addPage").attr("data-clickflag", "Y");
            },
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });

    });

});

function fnSearch(){
	$("#pageIndex").val(1);
	$("#frm").submit();
}

function fnDateSubmit(type){
	$("#dateType").val(type);
	$("#pageIndex").val("1");
	$("#frm").submit();
}

function fnViewSubmit(type){
	$("#viewType").val(type);
	$("#pageIndex").val("1");
	$("#frm").submit();
}

</script>
		<c:choose>
			<c:when test="${param.addAt eq 'Y'}">
			  <!-- 콘텐츠헤더 -->
	          <div class="page-content-header">
	            <c:choose>
	                <%-- TODO : 교원은 PDF 다운로드 버튼을 사용할 수 있기 때문에 분기처리함 사용안하면 분기처리 안해도됨 --%>
	                <c:when test="${8 <= USER_INFO.userSeCode }">
	                    <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			                <%-- <c:param name="crclId" value="${param.crclId}"/> --%>
			                <c:param name="curriculumManageBtnFlag" value="Y"/>
			            </c:import>
	                </c:when>
	                <c:otherwise>
	                    <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
	                        <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
	                    </c:import>
	                </c:otherwise>
	            </c:choose>
	          </div>
	          
	          <!-- 콘텐츠바디 -->
	          <section class="page-content-body">
	            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
					<c:param name="menu" value="myschedule"/>
					<c:param name="tabstep" value="${param.tabstep }"/>
					<c:param name="menuId" value="${param.menuId }"/>
					<c:param name="crclId" value="${param.crclId}"/>
					<c:param name="crclbId" value="${param.crclbId}"/>
				</c:import>
			</c:when>
			<c:otherwise>
				<section class="page-content-body">
					<article class="content-wrap">
		              <div class="box-wrap mb-40">
		                <h3 class="title-subhead">수업자료 검색</h3>
		
		                <form id="frm" method="post" action="<c:url value="/lms/cla/classFileList.do"/>">
		            	  	<input type="hidden" id="pageIndex" name="pageIndex" value="${paginationInfo.currentPageNo }"/>
		            	  	<input type="hidden" id="lastIndex" value="${paginationInfo.lastPageNoOnPageList}"/>
		            	    <input type="hidden" name="menuId" value="${param.menuId }" />
		            	    <input type="hidden" id="dateType" name="dateType" value="${fileVo.dateType }" />
		            	    <input type="hidden" id="viewType" name="viewType" value="${fileVo.viewType }" />
		                <div class="flex-row-ten">
		                  <div class="flex-ten-col-3 mb-20">
		                    <div class="ell">
								<select class="select2" data-select="style3" name="searchCrclLang">
				                    <option value="">언어 전체</option>
				                    <c:forEach var="result" items="${languageList}" varStatus="status">
				                        <c:if test="${not empty result.upperCtgryId}">
				                            <option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
				                        </c:if>
			                    	</c:forEach>
			                  	</select>
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-7 mb-20">
		                    <div class="ell">
		                       <input type="text" name="searchCrclNm" placeholder="과정명" value="${fileVo.searchCrclNm }">
		                    </div>
		                  </div>
		                  <%-- 
		                  <div class="flex-ten-col-4 mb-20">
		                    <div class="ell">
		                      <input type="text" name="searchPlTitle" placeholder="수업명" value="${fileVo.searchPlTitle }">
		                    </div>
		                  </div>
		                  --%>
		                  <div class="flex-ten-col-3">
		                    <div class="ell">
		                      <input type="text" name="searchFileNm" placeholder="자료명" value="${fileVo.searchFileNm }">
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-3">
		                    <div class="ell">
		                      <input type="text" name="searchTeacher" placeholder="교수명" value="${fileVo.searchTeacher }">
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-4 flex align-items-center">
		                    <label class="checkbox">
		                      <input type="checkbox" name="fileExtOther" value="Y" ${param.fileExtOther eq 'Y' ? 'checked' : '' }>
		                      <span class="custom-checked"></span>
		                      <span class="text">파일</span>
		                    </label>
		                    <label class="checkbox">
		                      <input type="checkbox" name="fileExtImg" value="Y" ${param.fileExtImg eq 'Y' ? 'checked' : '' }>
		                      <span class="custom-checked"></span>
		                      <span class="text">사진</span>
		                    </label>
		                    <label class="checkbox">
		                      <input type="checkbox" name="fileExtMov" value="Y" ${param.fileExtMov eq 'Y' ? 'checked' : '' }>
		                      <span class="custom-checked"></span>
		                      <span class="text">동영상</span>
		                    </label>
		                  </div>
		                </div>
		                <button type="button" class="btn-sm font-400 btn-point mt-20" onclick="fnSearch()" >검색</button>
		                </form>
		              </div>
		            </article>
		
		            <article class="content-wrap">
		              <!-- tab style -->
		              <ul class="tab-wrap sub-tab-style">
		                <li class="tab-list ${fileVo.dateType eq 'all' ? 'on' : '' }"><a href="#none" onclick="fnDateSubmit('all');">전체</a></li>
		                <li class="tab-list ${fileVo.dateType eq 'today' ? 'on' : '' }"><a href="#none" onclick="fnDateSubmit('today');">오늘 수업자료</a></li>
		              </ul>
		            </article>
			</c:otherwise>
		</c:choose>
            

            <article class="content-wrap">
            	<c:if test="${param.addAt ne 'Y'}">
	              <div class="content-header">
	                <div class="board-type-wrap">
	                  <a href="#none" class="btn-gallery ${fileVo.viewType eq 'gallery' ? 'on' : '' }" title="갤러리형" onclick="fnViewSubmit('gallery');"></a><i class="division-line"></i>
	                  <a href="#none" class="btn-list ${fileVo.viewType eq 'list' ? 'on' : '' }"" title="리스트형" onclick="fnViewSubmit('list');"></a>
	                </div>
	              </div>
              	</c:if>

              <c:choose>
              	<c:when test="${fileVo.viewType eq 'gallery'}">
	              	<div class="content-body">
	              		<div class="flex-row board-gallery-thumb-wrap">
	              			<c:if test="${not empty fileList}">
	                  			<c:forEach items="${fileList}" var="result">
		              			<div class="flex-col-4">
		              				<c:url var="viewUrl" value="/lms/cla/classFileView.do">
										<c:param name="atchFileId" value="${result.atchFileId}" />
									  	<c:param name="fileSn" value="${result.fileSn}" />
									  	<c:param name="plId" value="${result.plId}" />
									</c:url>

				                    <a href="#none" onclick="location.href='${viewUrl}'">
				                    	<c:choose>
				                      		<c:when test="${result.fileExtNm eq '이미지'}">
				                      		<c:url var="viewImg" value="/cmm/fms/getImage.do">
												<c:param name="siteId" value="SITE_000000000000001" />
											  	<c:param name="appendPath" value="${result.plId}" />
											  	<c:param name="atchFileNm" value="${result.streFileNm}.${result.fileExtsn}" />
											  	<c:param name="fileStorePath" value="Study.fileStorePath" />
											</c:url>
												<div class="board-gallery-img" style="background-image:url(${viewImg});"></div>
				                      		</c:when>
				                      		<c:when test="${result.fileExtNm eq '동영상'}">
				                      			<div class="board-gallery-img file-overlay" style="background:#eeeeee"></div>
				                      		</c:when>
				                      		<c:otherwise>
				                      			<div class="board-gallery-img" style="background-image:url(${_L_IMG }/common/img_download_thumbnail.jpg);"></div>
				                      		</c:otherwise>
				                      	</c:choose>

				                      <div class="board-gallery-contents">
				                        <div class="gallery-flag">
						                    <c:set var="imgSrc">
						                      <c:import url="/lms/common/flag.do" charEncoding="utf-8">
						                        <c:param name="ctgryId" value="${result.crclLang}"/>
						                      </c:import>
						                    </c:set>
				                          <span class="flag-img">
				                          <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
				                          <span class="text">${result.ctgryNm }</span>
				                        </div>
				                        <p class="board-gallery-subtitle ell">${result.fileExtNm eq '이미지' ? '사진' : result.fileExtNm eq '동영상' ? '동영상' : '파일' } </p>
				                        <p class="board-gallery-title ell">${result.orignlFileNm }</p>
				                        <div class="board-gallery-cell">
				                          <dl class="card-cell-items">
				                            <dt class="card-cell-title">과정명</dt>
				                            <dd class="ell">${result.crclNm }</dd>
				                          </dl>
				                          <%-- 
				                          <dl class="card-cell-items">
				                            <dt class="card-cell-title">수업명</dt>
				                            <dd class="ell">${result.title }</dd>
				                          </dl>
				                           --%>
				                        </div>
				                        <ul class="card-items-wrap">
				                          <li class="card-items icon-name">${result.teacherNm } 교수</li>
				                        </ul>
				                      </div>
				                    </a>
				                  </div>
				                  </c:forEach>
				                 </c:if>
	              		</div>
	              		<c:if test="${not empty fileList and  (paginationInfo.currentPageNo) + 1 le paginationInfo.lastPageNoOnPageList}">
		              		<div class="center-align">
			                  <button type="button" class="cursor-pointer addPage" data-pageindex="${(paginationInfo.currentPageNo)+1 }" data-clickflag="Y">
			                  	<img class="vertical-top" src="${_L_IMG }/common/btn_board_contents_more.jpg" alt="더보기">
			                  </button>
			                </div>
		                </c:if>
              		</div>
              	</c:when>
              	<c:otherwise>
              		<div class="content-body">
						<table class="common-table-wrap table-type-board">
		                  <colgroup>
		                    <col style='width:10%'>
		                    <col style='width:12%'>
		                    <col style='width:12%'>
		                    <col>
		                    <col style='width:30%'>
		                    <%-- <col style='width:15%'> --%>
		                    <col style='width:10%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-light-gray font-700'>
		                      <th scope='col'>유형</th>
		                      <th scope='col'>언어</th>
		                      <th scope='col'>미리보기</th>
		                      <th scope='col'>자료명</th>
		                      <th scope='col'>과정명</th>
		                      <!-- <th scope='col'>수업명</th> -->
		                      <th scope='col'>교수</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                  	<c:if test="${not empty fileList}">
		                  		<c:forEach items="${fileList}" var="result">
				                  	<c:url var="viewUrl" value="/lms/cla/classFileView.do">
										<c:param name="atchFileId" value="${result.atchFileId}" />
									  	<c:param name="fileSn" value="${result.fileSn}" />
									  	<c:param name="plId" value="${result.plId}" />
									</c:url>
				                  <tr onclick="location.href='${viewUrl}'">
				                      <td scope='row' class='font-orange'>${result.fileExtNm eq '이미지' ? '사진' : result.fileExtNm eq '동영상' ? '동영상' : '파일' } </td>

				                      <td>${result.ctgryNm }</td>
				                      <td>
				                      	<c:choose>
				                      		<c:when test="${result.fileExtNm eq '이미지'}">
				                      			<c:url var="viewImg" value="/cmm/fms/getImage.do">
													<c:param name="siteId" value="SITE_000000000000001" />
												  	<c:param name="appendPath" value="${result.plId}" />
												  	<c:param name="atchFileNm" value="${result.streFileNm}.${result.fileExtsn}" />
												  	<c:param name="fileStorePath" value="Study.fileStorePath" />
												</c:url>
												<div class='board-gallery-lists' style='background-image:url(${viewImg})'></div>
				                      		</c:when>
				                      		<c:when test="${result.fileExtNm eq '동영상'}">
				                      			<div class='board-gallery-lists file-overlay' style='background:#eeeeee'></div>
				                      		</c:when>
				                      		<c:otherwise>
				                      			<div class='board-gallery-lists' style='background-image:url(${_L_IMG }/common/img_download_thumbnail_sm.jpg);'></div>
				                      		</c:otherwise>
				                      	</c:choose>

				                      </td>
				                      <td class='title'>
				                        <div class="inner-wrap"><span class='text dotdotdot'>${result.orignlFileNm }</span></div>
				                      </td>
				                      <td class='title'>
				                        <div class="inner-wrap"><span class='text dotdotdot'>${result.crclNm }</span></div>
				                      </td>
				                      <%-- 
				                      <td class='title'>
				                        <div class="inner-wrap"><span class='text dotdotdot'>${result.title }</span></div>
				                      </td>
				                       --%>
				                      <td>${result.teacherNm }</td>
				                    </tr>
		                  		</c:forEach>
		                  	</c:if>
		                  </tbody>
		                </table>
              		</div>
              		<c:if test="${not empty fileList and  (paginationInfo.currentPageNo) + 1 le paginationInfo.lastPageNoOnPageList}">
	              		<div class="mt-50 center-align">
			                <button type="button" class="cursor-pointer addPage" data-pageindex="${(paginationInfo.currentPageNo)+1 }" data-clickflag="Y">
			                  <img class="vertical-top"  src="${_L_IMG }/common/btn_board_contents_more.jpg" alt="더보기">
			                </button>
	              		</div>
	                </c:if>
              	</c:otherwise>
              </c:choose>
            </article>
          </section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>