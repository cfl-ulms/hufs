<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

	                	<c:forEach var="result" items="${resultList}" varStatus="status">
	                		<tr <c:if test="${result.attentionType eq 'N'}">class='font-red'</c:if>>
	                			<td><c:out value="${status.count}"/></td>
	                			<td>
					              	<c:choose>
					              		<c:when test="${result.attentionType eq 'Y'}">출석</c:when>
					              		<c:when test="${result.attentionType eq 'L'}">지각</c:when>
					              		<c:when test="${result.attentionType eq 'N'}">결석</c:when>
					              	</c:choose>
					            </td>
					            <td><c:out value="${result.userNm}"/></td>
	                		</tr>
	                	</c:forEach>
	                	