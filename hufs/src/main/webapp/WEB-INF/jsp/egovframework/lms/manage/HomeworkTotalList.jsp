<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
	$(document).on("click", ".searchBtn", function() {
		$("form[name='searchFrm']").submit();
	});

	//검색 초기화
	$(document).on("click", "#searchReset", function() {
        $("form[name='searchFrm']").find("input").not("#resetBtn").val("");
        $("form[name='searchFrm'] select").val("").trigger("change");
        $("form[name='searchFrm'] input").prop("checked", false);
    });
});
</script>

<section class="page-content-body">
    <article class="content-wrap">
      <div class="box-wrap mb-40">
        <h3 class="title-subhead">과제 검색</h3>
        <form name="searchFrm" action="/lms/manage/homeworkTotalList.do" method="post">
            <input type="hidden" name="menuId" value="${searchVO.menuId }" />
            <div class="flex-row-ten">

	          <div class="flex-ten-col-4 mb-20">
	            <div class="desc">
	              <input type="text" class="ell date datepicker type2" name="searchStartCloseDate" placeholder="과제 마감일" value="${searchVO.searchStartCloseDate }">
	              <i>~</i>
	              <input type="text" class="ell date datepicker type2" name="searchEndCloseDate" placeholder="과제 마감일" value="${searchVO.searchEndCloseDate }">
	            </div>
	
	          </div>
	          <div class="flex-ten-col-6 flex align-items-center mb-20">
	            <label class="checkbox">
	              <input type="checkbox"  name="searchCloseHomework" value="Y" <c:if test="${searchVO.searchCloseHomework eq 'Y'}">checked</c:if>>
	              <span class="custom-checked"></span>
	              <span class="text">마감된 과제만 조회</span>
	            </label>
	          </div>
	          <div class="flex-ten-col-4">
	            <div class="ell">
	              <select name="searchHwType" id="searchHwType" class="select2" data-select="style3">
	                <option value="">과제유형 전체</option>
	                <option value="1" <c:if test="${searchVO.searchHwType eq '1'}">selected</c:if>>개별</option>
	                <option value="2" <c:if test="${searchVO.searchHwType eq '2'}">selected</c:if>>조별</option>
	              </select>
	            </div>
	          </div>
	          <div class="flex-ten-col-2">
	            <div class="ell">
	              <select name="searchHwCode" id="searchHwCode" class="select2" data-select="style3">
	                <option value="">과제 전체</option>
	                <option value="1" <c:if test="${searchVO.searchHwCode eq '1'}">selected</c:if>>과정과제</option>
	                <option value="2" <c:if test="${searchVO.searchHwCode eq '2'}">selected</c:if>>수업과제</option>
	              </select>
	            </div>
	          </div>
	          <div class="flex-ten-col-4 flex align-items-center mb-20">
	            <label class="checkbox">
                  <input type="checkbox" name="searchMyHomework" value="Y"<c:if test="${searchVO.searchMyHomework eq 'Y'}">checked</c:if>>
                  <span class="custom-checked"></span>
                  <span class="text">나의 과제</span>
                </label>
              </div>
	          <div class="flex-ten-col-4">
	            <div class="ell">
	              <input type="text" name="searchCrclNm" placeholder="과정명" value="${searchVO.searchCrclNm }">
	            </div>
	          </div>
	          <div class="flex-ten-col-6">
	            <div class="ell">
	              <input type="text" name="searchHomeworkNm" placeholder="과제명" value="${searchVO.searchHomeworkNm }">
	            </div>
	          </div>
	      </div>
	      <button class="btn-sm font-400 btn-point mt-20 searchBtn">검색</button>
          <a class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</a>
	    </form>
      </div>
    </article>
    <article class="content-wrap">
      <div class="content-body">
        <!-- 테이블영역-->
        <table class="common-table-wrap table-type-board">
          <colgroup>
            <col style='width:7%'>
            <col style='width:100px'>
            <col style='width:10%'>
            <col style='width:10%'>
            <col style='width:20%'>
            <col style='width:24%'>
            <col style='width:10%'>
            <col style='width:10%'>
          </colgroup>
          <thead>
            <tr class='bg-light-gray font-700'>
              <th scope='col'>No</th>
              <th scope='col'>과제마감일</th>
              <th scope='col' colspan='2'>평가구분</th>
              <th scope='col'>과정명</th>
              <th scope='col'>과제명</th>
              <th scope='col'>교원</th>
              <th scope='col'>제출</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                <c:choose>
                    <c:when test="${result.hwCode eq '1' }">
                        <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
		                    <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
		                    <c:param name="hwId" value="${result.hwId }" />
		                    <c:param name="hwsId" value="${result.hwsId }" />
		                    <c:param name="crclId" value="${result.crclId }" />
		                    <c:param name="plId" value="${result.plId }" />
		                    <c:param name="homeworkTotalFlag" value="Y" />
		                    <c:param name="boardType" value="list" />
		                </c:url>
                    </c:when>
                    <c:otherwise>
                        <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do">
                            <c:param name="menuId" value="MNU_0000000000000086" />
                            <c:param name="hwId" value="${result.hwId }" />
                            <c:param name="crclId" value="${result.crclId }" />
                            <c:param name="plId" value="${result.plId }" />
                            <c:param name="boardType" value="list" />
                            <c:param name="tabType" value="T" />
                            <c:param name="step" value="8" />
                        </c:url>
                    </c:otherwise>
                </c:choose>
	            <tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
	              <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	              <td>${result.closeDate}</td>
	              <td>${result.hwCodeNm}</td>
	              <td>${result.hwTypeNm}</td>
	              <td class='left-align'>${result.crclNm }</td>
	              <td class='left-align'>${result.nttSj }</td>
	              <td>${result.userNm }</td>
	              <td>
	                  <c:choose>
                          <c:when test="${result.homeworkSubmitCnt eq result.memberCnt && result.homeworkSubmitCnt ne '0' && result.memberCnt ne '0' }">전원제출</c:when>
                          <c:otherwise>${result.homeworkSubmitCnt}/${result.memberCnt }</c:otherwise>
                      </c:choose>
                  </td>
	            </tr>
	        </c:forEach>
          </tbody>
        </table>
        <div class="pagination center-align mt-60">
            <div class="pagination-inner-wrap overflow-hidden inline-block">
                <c:url var="startUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="1" />
                </c:url>
                <button class="start goPage" data-url="${startUrl}"></button>
               
                <c:url var="prevUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
                </c:url>
                <button class="prev goPage" data-url="${prevUrl}"></button>
               
                <ul class="paginate-list f-l overflow-hidden">
                    <c:url var="pageUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}"/>
                    <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
                    <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
                </ul>
               
                <c:url var="nextUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
                </c:url>
                <button class="next goPage" data-url="${nextUrl}"></button>
               
                <c:url var="endUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
                </c:url>
                <button class="end goPage" data-url="${endUrl}"></button>
            </div>
        </div>
      </div>
    </article>
</section>
</div>
</div>
</div>
</div>

    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>