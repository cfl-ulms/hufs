<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_MODE" value=""/>
<c:set var="_EDITOR_ID" value="crclEvtCn"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
</c:url>
<% /*URL 정의*/ %>
<c:choose>
<c:when test="${param.popupAt eq 'Y'}">
<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>운영보고서_${curriculumVO.crclNm}_${today}</title>

  <meta name="title" content="운영보고서">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">
	
  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">
  <link href="/template/lms/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/template/lms/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="/template/lms/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

	<link rel="stylesheet" href="/template/lms/css/common/base.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/common/common_staff.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/common/board_staff.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/common/table_staff.css?v=2">
	<link rel="stylesheet" href="/template/lms/css/common/modal_staff.css?v=2">


  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="/template/lms/js/common.js?v=1"></script>
  <script src="/template/common/js/common.js"></script>

 <link rel="stylesheet" href="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/style.css">

<script>
function clock() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var clockDate = date.getDate();
    var day = date.getDay();
    var week = ['일', '월', '화', '수', '목', '금', '토'];
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var dataTime = $("#clock").data("time");

    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;

    $("#clock").text("출력일 : " + year + "년 " + month +"월 " + clockDate + "일 " + hours + ":" + minutes);
    $("#clock").data("time", dataTime + 1);
    if(dataTime == 1){
    	<c:if test="${param.printAt eq 'Y'}">
			window.print();
		</c:if>
    }
}

function init() {
	clock();
	setInterval(clock, 1000);
}
init();
</script>
</head>
<body>
</c:when>
<c:otherwise>
	<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
		<c:param name="isMainSite">Y</c:param>
		<c:param name="reportAt">Y</c:param>
		<c:param name="siteId" value="SITE_000000000000001"/>
	</c:import>
</c:otherwise>
</c:choose>
<script src="${CML }/lib/sly-master/sly.min.js?v=1"></script>
<script src="${CML }/lib/echarts/echarts.min.js?v=1"></script>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
$(document).ready(function(){
	var adfile_config = {
			siteId:"<c:out value='${siteInfo.siteId}'/>",
			pathKey:"CrclEvt",
			appendPath:"<c:out value='${curriculumVO.crclId}'/>",
			editorId:"${_EDITOR_ID}",
			fileAtchPosblAt:"Y",
			maxMegaFileSize:"1024",
			atchFileId:"${curriculumVO.atchFileId}"
		};

	fn_egov_bbs_editor(adfile_config);

	//운영평가 수정
	$("#btnUpt").click(function(){
		$("#detailForm").submit();
		return false;
	});
	
	//운영평가 수정 박스
	$("#btnUptBox").click(function(){
		$("#view_box").hide();
		$("#upt_box").show();
		return false;
	});
	
	//인쇄하기
	$("#btnPrint").click(function(){
		window.open("/lms/manage/manageReport.do?crclId=${curriculumVO.crclId}&popupAt=Y&printAt=Y","popup","top=1,left=1,width=900,height=" + screen.availHeight);
	});
	
	//교육과정 통계표 시작
	var myChartArr = [];

	//수업시간표
	var chartBarOption2 = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#54c374', '#396fd4'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        },
      },
      yAxis: {
        type: 'category',
        data: ['총 시간', '주 회', '일 시간',],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [${curriculumVO.totalTime}, ${curriculumVO.weekNum}, ${curriculumVO.dayTime}],
        }
      ]
    };

	var chartBarOption3 = {
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      animation: false,
      color: ['#54c374', '#396fd4'],
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능)
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['총\n학생수', '평가\n횟수', '과정\n과제수'],
        axisLabel: {
          lineHeight: 17
        },
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          barGap: 0,
          barWidth: 30,
          label: {
            normal: {
              show: true,
              position: 'insideRight',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [${curriculumSts.stuCnt}, ${curriculumSts.quizCnt}, ${curriculumSts.homeCnt}],  //data 값 변경
        }
      ]
    };

	var chartGenderBarOption = {
      color: ['#54c374', '#396fd4'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        show: true,
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['해당 교육과정'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      xAxis: {
        type: 'value',
        min: 0, // 최소 갯수 (값 확인후 변경가능) (값 확인후 변경가능)
        max: 100, // 최대 갯수 (값 확인후 변경가능) (값 확인후 변경가능)
        axisLabel: {
          formatter: '{value}%'
        },
        axisTick: {
          show: false,
        }
      },
      yAxis: {
        type: 'category',
        data: ['남자', '여자'],
        axisTick: {
          show: false,
        }
      },
      series: [
        {
          name: '해당 교육과정',
          type: 'bar',
          stack: '비율',
          barWidth: 60,
          label: {
            normal: {
              show: true,
              position: 'inside',
              textStyle: {
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },
          data: [${curriculumSts.manPer}, ${curriculumSts.womanPer}] //data 값 변경
        }
      ]
    };

    var chartStudentBarOption = {
      color: ['#396fd4', '#54c374', '#ddd'],
      animation: false,
      grid: {
        top: '2%',
        left: '2%',
        right: '4%',
        bottom: '14%',
        containLabel: true
      },
      legend: {
        orient: 'horizontal',
        x: 'center',
        bottom: 0,
        data: ['본교', '그 외 대학', '일반'],
        icon: 'rect',
        itemGap: 20,
        textStyle: {
          fontSize: 13,
          verticalAlign: 'middle',
        },
      },
      series: [
        {
          type: 'pie',
          color: ['#396fd4', '#54c374', '#ddd'],
          radius: '65%',
          center: ['50%', '43%'],
          hoverAnimation: false,
          data: [
            { value: ${curriculumSts.schStuCnt}, name: '본교'},  //data 값 변경
            { value: ${curriculumSts.othSchStuCnt}, name: '그 외 대학'},  //data 값 변경
            { value: ${curriculumSts.etcStuCnt}, name: '일반'},  //data 값 변경
          ],
          labelLine: {
            normal: {
              show: true,
            },
          },
          label: {
            normal: {
              show: true,
              formatter: '{c}',
              textStyle: {
                color: '#333',
                fontSize: 15,
                fontWeight: 700,
              }
            }
          },

        }
      ]
    };

	//echart 셋팅
    $(".boardTypeGraph").each(function (idx, el) {
      var myChart = echarts.init(el);
      var chartType = el.dataset.chartType;

      myChartArr.push(myChart);

      if (chartType == "bar2") {
        //차트옵션 셋팅
        myChart.setOption(chartBarOption2);
      } else if (chartType == "bar3") {
        //차트옵션 셋팅
        myChart.setOption(chartBarOption3);
      } else if (chartType == "genderbar") {
        //차트옵션 셋팅
        myChart.setOption(chartGenderBarOption);
      } else if (chartType == "studentPie") {
        //차트옵션 셋팅
        myChart.setOption(chartStudentBarOption);
      }
    });

    $(window).on('resize', function () {
      myChartArr.forEach(function (el, idx) {
        el.resize();
      });
    });

    $(".btn-expansion").on("click", function () {
      myChartArr.forEach(function (el, idx) {
        setTimeout(function () {
          el.resize();
        }, 310); // transition 끝난후 resize
      });
    });
	//교육과정 통계표 종료

	//과정만족도
	$(".chartDiv").each(function (idx, el) {
		var totalCnt = 0;
		var myChart = echarts.init(el);

		var tempArr = $(this).closest(".queDiv").find("tr.answerClass");
		var dataMapArr = $.makeArray(tempArr.map(function(){
			if($(this).data("excnt") != 0){
		 		var tempObject = new Object();
		 		tempObject.value = $(this).data("excnt");
		  	 	tempObject.name = $(this).data("excn");
		  	 	totalCnt += $(this).data("excnt");
		  	 	return tempObject;
			}
		}));

		var dataArr = $.makeArray(tempArr.map(function(){
		 	return  $(this).data("excn");
		}));


		var option = {
				 color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
			      animation: false,
			      legend: {
			        orient: 'vertical',
			        right: 15,
			        top: 30,
			        data: dataArr,
			        icon: 'rect',
			        itemGap: 20,
			        textStyle: {
			          fontSize: 13,
			          verticalAlign: 'middle',
			        },
			      },
			      series: [
			        {
		        		color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
			          	type: 'pie',
			          	radius: '85%',
			          	label:{
			                normal: {
			                    formatter: '{d}%',
			                    position: 'inside'
			                }
			            },
			          	padding : '20px',
			          	center: ['35%', '50%'],
			          	hoverAnimation: false,
			          	animation: false,
			          	data: dataMapArr
			        }
			      ]
			    };

		myChart.setOption(option);
			$(this).closest(".queDiv").find(".totalCnt").html("응답 (<span class=\'font-point\'>"+totalCnt+"</span>명)");
		});
	
	box_lesson();
});


function vali(){
	//파일
	$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());
	return;
}

function box_lesson(){
	var htmls = '';
	var crclId = '${curriculumVO.crclId}';
	var preLessonOrder = 0;
	var lessonOrderCnt = 0;
	var totLen =0;
	
	$.ajax({
           type: "POST",
           url: "/ajax/mng/lms/crcl/selectCurriculumLesson.json",
           data: {                
               "crclId" : crclId
               },
           dataType: "json",
           async: false,
           success: function(data, status) {
           	console.log(data);
           	console.log(data.lessonList);
           	$(".box_lesson > tr").remove();
           	if(data.status == "200"){
           		totLen=data.lessonList.length;
           		
           		$.each(data.lessonList, function(index){
           			
           			if(lessonOrderCnt == 0){
           				lessonOrderCnt = this.lessonOrderCnt;
           				preLessonOrder = this.lessonOrder;
    					htmls += '<tbody class="box_lesson">';
    					htmls += '<tr class="first">';
   					} else {
    					htmls += '<tr class="contentBody">';
   						
   					}
                   	 
   					if((lessonOrderCnt == this.lessonOrderCnt) && (preLessonOrder == this.lessonOrder)){
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line alC num">';
   						htmls += this.lessonOrder;
   						htmls += '</td>';
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line lesnm">'+this.lessonNm+'</td>';
   					}
           			
					htmls += '<td class="line">'+this.chasiNm+'</td>';
					htmls += '</tr>';
					if((lessonOrderCnt-1) == 0){
    					htmls += '</tbody">';
					}
					preLessonOrder = this.lessonOrder;
   					--lessonOrderCnt;
           		});
           		
				$("#box_addLes").before(htmls);
           	}
           },
           error: function(xhr, textStatus) {
               alert("문제가 발생하였습니다. \n 관리자에게 문의하세요");
               document.write(xhr.responseText);
               return false;
           },beforeSend:function() {
           },
           complete:function() {
           }
       }); 
}
</script>

          <div class="page-content-header">
            <c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
         	
         	<c:if test="${param.printAt ne 'Y'}">
				<div class="util-wrap">
			      <button type="button" id="btnPrint" class="btn-md btn-outline-gray">인쇄하기</button>
			    </div>
		    </c:if>
          </div>
          
          <section class="page-content-body">
          	<c:if test="${param.printAt eq 'Y'}">
          		<article class="content-wrap">
	              <div class="no-content-wrap short-160">
                	<p class="title">본 운영보고서는 <c:if test="${USER_INFO.userSe > 9}">관리자 </c:if><c:out value="${USER_INFO.name}"/>에 의해 안전하게 출력되었습니다.</p>
	                <p id="clock" class="sub-title" data-time="0">출력일 : </p>
	              </div>
	            </article>
          	</c:if>
          	
          	<c:if test="${param.printAt ne 'Y'}">
	            <article class="content-wrap">
	              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
	              	<c:choose>
	              		<c:when test="${not empty param.step}">
	              			<c:set var="step" value="${param.step}"/>
	              		</c:when>
	              		<c:otherwise>
	              			<c:set var="step" value="5"/>
	              		</c:otherwise>
	              	</c:choose>
					<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
						<c:param name="step" value="${step}"/>
						<c:param name="crclId" value="${curriculumVO.crclId}"/>
						<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
						<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
					</c:import>
				  </c:if>
				</article>
			</c:if>
			<!-- <article class="content-wrap">
              <div class="no-content-wrap short-160">
                <p class="title">본 운영보고서는 관리자 000에 의해 안전하게 출력되었습니다.</p>
                <p class="sub-title">출력일 : 2019년 10월 21일 15:28</p>
              </div>
            </article> -->
            <article class="content-wrap">
              <!-- 교육과정 개요 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">1. 교육과정 개요</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>년도</td>
                      <td><c:out value="${curriculumVO.crclYear }"/></td>
                      <td scope='row' class='font-700'>학기</td>
                      <td><c:out value="${curriculumVO.crclTermNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>언어</td>
                      <td><c:out value="${curriculumVO.crclLangNm}"/></td>
                      <td scope='row' class='font-700'>학점인정 여부</td>
	                      <c:choose>
			            	<c:when test="${curriculumVO.gradeAt eq 'Y'}">
			            		<td>예(${curriculumVO.gradeNum} 학점)</td>
			            	</c:when>
			            	<c:otherwise>
			            		<td class='font-red'>아니오</td>
			            	</c:otherwise>
			              </c:choose>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정기간</td>
                      <td><c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/></td>
                      <td scope='row' class='font-700'>과정시간</td>
                      <td>
                      	<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
                      	총 <c:out value="${curriculumVO.totalTime}"/>시간 /
                      	<%-- 주 <c:out value="${curriculumVO.weekNum}"/>회 --%>
                      	<c:out value="${curriculumVO.lectureDay}"/> /
                      	<%-- 일 <c:out value="${curriculumVO.dayTime}"/>시간 --%>
                      	<br/>
                      	<c:out value="${curriculumVO.startTime}"/> (일일 <c:out value="${curriculumVO.dayTime}"/>시간)
                      </c:if>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정진행캠퍼스</td>
                      <td colspan='3'><c:out value="${curriculumVO.campusNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>책임교수</td>
                      <td colspan='3'><c:out value="${curriculumVO.userNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정의 목표(개요)</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정의 기대효과</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 학습내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">2. 학습내용 (단원)</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col style='width:10%'>
                    <col style='width:45%'>
                    <col style='width:45%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>단원명</th>
                      <th scope='col'>학습내용</th>
                    </tr>
                  </thead>

                  <%-- 과정내용 번호 및 단원 묶어주는 역할 --%>
				<%-- 	<c:set var="rowspan" value="0"/>
					<c:set var="rowspanList" value=""/>
					<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
					<c:forEach var="list" items="${lessonList}" varStatus="status">
						<c:choose>
							<c:when test="${prevCode eq list.lessonNm}">
								<c:set var="rowspan" value="${rowspan + 1}"/>
							</c:when>
							<c:otherwise>
								<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
								<c:set var="rowspan" value="1"/>
								<c:set var="prevCode" value="${list.lessonNm}"/>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
					<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
					<c:set var="listCnt" value="${rowspan[0]}"/>
					<c:set var="rowspanCnt" value="0"/>

					<c:set var="prevCode" value=""/>
					<c:set var="number" value="1"/>

					<c:forEach var="result" items="${lessonList}" varStatus="status">
						<c:if test="${prevCode ne result.lessonNm}">
							<tbody class='box_lesson'>
						</c:if>
						<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
							<c:if test="${prevCode ne result.lessonNm}">
								<td class="center-align num" rowspan="${rowspan[rowspanCnt] + 1}">
									<c:out value="${number}"/>
									<c:set var="number" value="${number + 1}"/>
								</td>
								<td rowspan='${rowspan[rowspanCnt] + 1}' class='lesnm'><c:out value="${result.lessonNm}"/></td>
								<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
							</c:if>
							<td><c:out value="${result.chasiNm}"/></td>
						</tr>
						<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
							</tbody>
						</c:if>

						<c:set var="prevCode" value="${result.lessonNm}"/>
			  		</c:forEach> --%>
			  		<tfoot id="box_addLes"></tfoot>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 담당교원 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">3. 담당교원 <!-- (교원 총 2명, 과정 총 계획 26시간 / 실제 교육시간 16시간) <span class="font-point">100% 진행</span> --></div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col style='width:10%'>
                    <col style='width:20%'>
                    <col style='width:20%'>
                    <col style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-gray font-700'>
                      <th scope='col'>시수</th>
                      <th scope='col'>교원 명</th>
                      <th scope='col'>소속</th>
                      <th scope='col'>수업일</th>
                      <th scope='col'>수업주제</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:set var="tmpFacId" value=""/>

                  	<c:forEach var="result" items="${facPlList}" varStatus="status">
                  		<c:set var="facRowSpan" value="0"/>
                		<c:set var="sisuSum" value="0"/>
                 		<c:forEach var="cResult" items="${facPlListCnt}" varStatus="status">
	                  		<c:if test="${cResult.facId eq  result.facId}">
	                  			<c:set var="facRowSpan" value="${cResult.facCnt}"/>
	                  			<c:set var="sisuSum" value="${fn:replace(cResult.sisuSum,'.0','')}"/>
	                  		</c:if>
	                  		<c:if test="${facRowSpan eq 1 }">
	                  			<c:set var="tmpFacId" value="${result.facId}"/>
	                  		</c:if>
                  		</c:forEach>
                  		<c:if test="${tmpFacId ne  result.facId}">
	                  		<c:set var="tmpFacId" value=""/>
	                    </c:if>
                  		<c:choose>
                  			<c:when test="${tmpFacId eq result.facId and facRowSpan eq 1}">
	                  			<tr class="">
			                      <td>${sisuSum}</td>
			                      <td>${result.userNm}(${result.crclLangNm})</td>
			                      <td>${result.groupCode}</td>
			                      <td>${result.startDt}</td>
			                      <td>${result.studySubject}</td>
			                    </tr>
			                    <c:set var="tmpFacId" value=""/>
	                  		</c:when>
	                  		<c:when test="${empty tmpFacId and facRowSpan > 1}">
	                  			<c:set var="tmpFacId" value="${result.facId}"/>

	                  			<tr class="">
			                      <td scope='row' rowspan='${facRowSpan }'>${sisuSum}</td>
			                      <td rowspan='${facRowSpan }'>${result.userNm}(${result.crclLangNm})</td>
			                      <td rowspan='${facRowSpan }'>${result.groupCode}</td>
			                      <td>${result.startDt}</td>
			                      <td>${result.studySubject}</td>
			                    </tr>
	                  		</c:when>
	                  		<c:otherwise>
	                  			<c:if test="${tmpFacId eq  result.facId}">
			                  		<tr class="">
				                      <td>${result.startDt}</td>
				                      <td>${result.studySubject}</td>
				                    </tr>
			                    </c:if>
		                    </c:otherwise>
	                    </c:choose>
                  	</c:forEach>
                  </tbody>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 교육과정 통계표 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">4. 교육과정 통계표</div>
                </div>
              </div>
              <div class="content-body">
                <div class="graph-layout-wrap">
                  <div class="graph-layout-box">
                    <p class="title">수업시간표</p>
                    <div class="boardTypeGraph" style="height:350px;" data-chart-type="bar2"></div>
                  </div>
                  <div class="graph-layout-box">
                    <p class="title">총 학생 수, 평가 횟수, 과제 수</p>
                    <div class="boardTypeGraph" style="height:350px;" data-chart-type="bar3"></div>
                  </div>
                </div>
                <div class="graph-layout-wrap mt-20">
                  <div class="graph-layout-box">
                    <p class="title">학생 수 남여 비율</p>
                    <div class="boardTypeGraph" style="height:275px;" data-chart-type="genderbar"></div>
                  </div>
                  <div class="graph-layout-box">
                    <p class="title">학생수 분포 (총 학생수 <span class="font-point"><c:out value="${curriculumSts.stuCnt}"/></span>명)</p>
                    <div class="boardTypeGraph" style="height:275px;" data-chart-type="studentPie"></div>
                  </div>
                </div>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 교육과정 수료현황 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">5. 교육과정 수료현황</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:40%'>
                    <%-- <col style='width:40%'> --%>
                  </colgroup>
                  <tbody>
                  	<c:set var="studentCnt" value="${fn:length(gradeList)}"/>
                  	<c:set var="finishCnt" value="0"/>
                  	<c:set var="avgPoint" value="0"/>
                  	<c:forEach var="result" items="${gradeList}" varStatus="stauts">
                  		<c:if test="${result.finishAt eq 'Y'}"><c:set var="finishCnt" value="${finishCnt + 1}"/></c:if>
                  		<c:set var="avgPoint" value="${avgPoint + result.chScr}"/>
                   	</c:forEach>
                    <tr class="">
                      <td scope='row' class='font-700'>구분</td>
                      <td>해당 교육과정 <c:out value="${curriculumVO.crclYear}"/>년도 <c:out value="${curriculumVO.crclTermNm}"/></td>
                      <!-- <td></td> -->
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>총 학생 수</td>
                      <td><c:out value="${curriculumSts.stuCnt}"/></td>
                      <!-- <td></td> -->
                    </tr>
                    <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }">
	                    <tr class="">
	                      <td scope='row' class='font-700'>수료 학생 수</td>
	                      <td>
	                      	<c:out value="${finishCnt}"/>
	                      	<c:choose>
	                      		<c:when test="${empty finishCnt or finishCnt ne '0'}">
	                      			(<fmt:formatNumber value="${finishCnt * 100 / studentCnt }" pattern="0"/>%)
	                      		</c:when>
	                      		<c:otherwise>
	                      			(0%)
	                      		</c:otherwise>
	                      	</c:choose>
	                      </td>
	                      <!-- <td></td> -->
	                    </tr>
                    </c:if>
                    <tr class="">
                      <td scope='row' class='font-700'>평균 점수</td>
                      <td>
                      	<c:choose>
                      		<c:when test="${empty avgPoint or avgPoint ne '0'}">
                      			<fmt:formatNumber value="${avgPoint / studentCnt }" pattern=".00"/>
                      		</c:when>
                      		<c:otherwise>
                      			0
                      		</c:otherwise>
                      	</c:choose>
                      	 <%-- <span class='table-increase'>2.22점 상승</span> --%>
                      </td>
                      <!-- <td></td> -->
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            <c:if test="${not empty surveyAnswer }">
	            <article class="content-wrap">
	              <!-- 과정만족도 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">6. 과정만족도</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <div class="no-content-wrap">
	<%--                 	<c:if test="${curriculumAddInfo.sum ne 0 }">
		                	<fmt:formatNumber var="resultPoint"  pattern="0" value=" ${(curriculumAddInfo.sum/ curriculumAddInfo.joinCnt)*100} " />
		                	<fmt:formatNumber var="resultPoint"  pattern="0" value="${curriculumAddInfo.sum}"/>
	                	</c:if> --%>
	                  	<b class="satisfy">과정만족도 : ${totalSurveyScore eq '0' ? '0' : totalSurveyScore}</b>
	                </div>
	                <div style="margin-top: 10px;">
	                	<table class="common-table-wrap size-sm">
	                		<thead>
	                			<tr>
	                				<th>항목</th>
	                				<c:forEach items="${surveyAnswer}" var="result" varStatus="status">
	                					<c:choose>
	                						<c:when test="${result.qesitmTyCode eq 'multiple'}"><th>${result.qesitmSj}</th></c:when>
	                						<c:otherwise></c:otherwise>
	                					</c:choose>
	                				</c:forEach>
	                			</tr>
	                		</thead>
	                		<tbody>
	                			<tr>
	                				<td>매우그렇다</td>
	                				<c:forEach items="${veryGoodList}" var="veryGoodList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${veryGoodList eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${veryGoodList}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그렇다</td>
	                				<c:forEach items="${goodList}" var="goodList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${goodList eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${goodList}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그저 그렇다</td>
	                				<c:forEach items="${soSoList}" var="soSoList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${soSoList eq '0.00'}">0</c:when>
	                							<c:otherwise>${soSoList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그렇지 않다</td>
	                				<c:forEach items="${notBadList}" var="notBadList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${notBadList eq '0.00'}">0</c:when>
	                							<c:otherwise>${notBadList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>매우 그렇지 않다</td>
	                				<c:forEach items="${badList}" var="badList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${badList eq '0.00'}">0</c:when>
	                							<c:otherwise>${badList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>평균</td>
	                				<c:forEach items="${surveyAvgList}" var="surveyAvgList">
	                					<td>${surveyAvgList}</td>
	                				</c:forEach>
	                			</tr>
	                		</tbody>
	                	</table>
	                </div>
						<c:forEach items="${surveyAnswer}" var="result" varStatus="status">
			                <div class="satisfy-table-wrap queDiv">
			                <div class="satisfy-head">${status.count }. ${result.qesitmSj }</div>

							<c:if test="${result.qesitmTyCode eq 'multiple'}">
			                  <div class="satisfy-img-box">
			                    <div class="chartDiv" style="display:inline-block;width:100%;height: 300px;border-style: none;"></div>
			                  </div>
			                 </c:if>
			                  <!-- 테이블영역-->

			                  <c:choose>
								<c:when test="${result.qesitmTyCode eq 'multiple'}">
									<table class="common-table-wrap size-sm">
					                    <colgroup>
					                      <col style='width:33.333%'>
					                      <col style='width:33.333%'>
					                      <col style='width:33.333%'>
					                    </colgroup>
					                    <thead>
					                      <tr class='bg-gray font-700'>
					                        <th scope='col'>점수</th>
					                        <th scope='col'>항목</th>
					                        <th scope='col' class="totalCnt"></th>
					                      </tr>
					                    </thead>
					                    <tbody>
					                    	<c:forEach items="${result.answerList}" var="answer"  varStatus="aStatus">
						                      <tr class="answerClass" data-excn="${answer.exCn }" data-excnt="${answer.cnt }">
						                        <td scope='row'>${fn:length(result.answerList) - aStatus.index }</td>
						                        <td>${answer.exCn }</td>
												<td>${answer.cnt }</td>
						                      </tr>
					                     	</c:forEach>
					                    </tbody>
					                  </table>
								</c:when>
								<c:when test="${result.qesitmTyCode eq 'answer'}">
									 <table class="common-table-wrap size-sm">
					                    <colgroup>
					                      <col style='width:10%'>
					                      <col style='width:18%'>
					                      <%-- <col style='width:18%'> --%>
					                      <col>
					                    </colgroup>
					                    <thead>
					                      <tr class='bg-gray font-700'>
					                        <th scope='col'>번호</th>
					                        <th scope='col'>응답자</th>
					                        <!-- <th scope='col'>전공</th> -->
					                        <th scope='col'>응답 내용</th>
					                      </tr>
					                    </thead>
					                    <tbody>
					                    <c:forEach items="${result.essayList}" var="essay"  varStatus="eStatus">
					                      <tr>
					                        <td scope='row'>${eStatus.count}</td>
					                        <td>${essay.userNm }</td>
											<%-- <td>${essay.crclNm }</td> --%>
											<td class='left-align'>${essay.cnsr }</td>
					                      </tr>
				                     	</c:forEach>
					                    </tbody>
					                  </table>
								</c:when>
							</c:choose>
			                </div>
		               </c:forEach>
	            </article>
	            
	            <c:if test="${not empty curriculumVO.professorSatisfyType}">
		            <article class="content-wrap">
		              <!-- 과정만족도 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">7. 교원대상 과정만족도 조사</div>
		                </div>
		              </div>
		              <div class="content-body">
		                <div class="no-content-wrap">
<%-- 		                	<c:if test="${curriculumAddInfoType3.sum ne 0 }">
			                	<fmt:formatNumber var="resultPoint"  pattern="0" value="${curriculumAddInfoType3.sum}"/>
		                	</c:if> --%>
		                  	<b class="satisfy">교원대상 과정만족도 : ${totalSurveyScore3 eq '0' ? '0' : totalSurveyScore3}</b>
		                </div>
		                 <div style="margin-top: 10px; margin-bottom: 10px;">
	                	<table class="common-table-wrap size-sm">
	                		<thead>
	                			<tr>
	                				<th>항목</th>
	                				<c:forEach items="${surveyAnswerType3}" var="result3" varStatus="status">
	                					<c:choose>
	                						<c:when test="${result3.qesitmTyCode eq 'multiple'}"><th>${result3.qesitmSj}</th></c:when>
	                						<c:otherwise></c:otherwise>
	                					</c:choose>
	                				</c:forEach>
	                			</tr>
	                		</thead>
	                		<tbody>
	                			<tr>
	                				<td>매우 만족</td>
	                				<c:forEach items="${veryGoodList3}" var="veryGoodList3">
	                					<td>
	                						<c:choose>
	                							<c:when test="${veryGoodList3 eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${veryGoodList3}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>만족</td>
	                				<c:forEach items="${goodList3}" var="goodList3">
	                					<td>
	                						<c:choose>
	                							<c:when test="${goodList3 eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${goodList3}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그저 그렇다</td>
	                				<c:forEach items="${soSoList3}" var="soSoList3">
	                					<td>
	                						<c:choose>
	                							<c:when test="${soSoList3 eq '0.00'}">0</c:when>
	                							<c:otherwise>${soSoList3}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>불만족</td>
	                				<c:forEach items="${notBadList3}" var="notBadList3">
	                					<td>
	                						<c:choose>
	                							<c:when test="${notBadList3 eq '0.00'}">0</c:when>
	                							<c:otherwise>${notBadList3}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>매우 불만족</td>
	                				<c:forEach items="${badList3}" var="badList3">
	                					<td>
	                						<c:choose>
	                							<c:when test="${badList3 eq '0.00'}">0</c:when>
	                							<c:otherwise>${badList3}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>평균</td>
	                				<c:forEach items="${surveyAvgList3}" var="surveyAvgList3">
	                					<td>${surveyAvgList3}</td>
	                				</c:forEach>
	                			</tr>
	                		</tbody>
	                	</table>
	                </div>
							<c:forEach items="${surveyAnswerType3}" var="result" varStatus="status">
				                <div class="satisfy-table-wrap queDiv">
				                <div class="satisfy-head">${status.count }. ${result.qesitmSj }</div>
	
								<c:if test="${result.qesitmTyCode eq 'multiple'}">
				                  <div class="satisfy-img-box">
				                    <div class="chartDiv" style="display:inline-block;width:100%;height: 300px;border-style: none;"></div>
				                  </div>
				                 </c:if>
				                  <!-- 테이블영역-->
	
				                  <c:choose>
									<c:when test="${result.qesitmTyCode eq 'multiple'}">
										<table class="common-table-wrap size-sm">
						                    <colgroup>
						                      <col style='width:33.333%'>
						                      <col style='width:33.333%'>
						                      <col style='width:33.333%'>
						                    </colgroup>
						                    <thead>
						                      <tr class='bg-gray font-700'>
						                        <th scope='col'>점수</th>
						                        <th scope='col'>항목</th>
						                        <th scope='col' class="totalCnt"></th>
						                      </tr>
						                    </thead>
						                    <tbody>
						                    	<c:forEach items="${result.answerList}" var="answer"  varStatus="aStatus">
							                      <tr class="answerClass" data-excn="${answer.exCn }" data-excnt="${answer.cnt }">
							                        <td scope='row'>${fn:length(result.answerList) - aStatus.index }</td>
							                        <td>${answer.exCn }</td>
													<td>${answer.cnt }</td>
							                      </tr>
						                     	</c:forEach>
						                    </tbody>
						                  </table>
									</c:when>
									<c:when test="${result.qesitmTyCode eq 'answer'}">
										 <table class="common-table-wrap size-sm">
						                    <colgroup>
						                      <col style='width:10%'>
						                      <col style='width:18%'>
						                      <%-- <col style='width:18%'> --%>
						                      <col>
						                    </colgroup>
						                    <thead>
						                      <tr class='bg-gray font-700'>
						                        <th scope='col'>번호</th>
						                        <th scope='col'>응답자</th>
						                        <!-- <th scope='col'>전공</th> -->
						                        <th scope='col'>응답 내용</th>
						                      </tr>
						                    </thead>
						                    <tbody>
						                    <c:forEach items="${result.essayList}" var="essay"  varStatus="eStatus">
						                      <tr>
						                        <td scope='row'>${eStatus.count}</td>
						                        <td>${essay.userNm }</td>
												<%-- <td>${essay.crclNm }</td> --%>
												<td class='left-align'>${essay.cnsr }</td>
						                      </tr>
					                     	</c:forEach>
						                    </tbody>
						                  </table>
									</c:when>
								</c:choose>
				                </div>
			               </c:forEach>
		            </article>
		        </c:if>
            </c:if>
			
           		<div id="view_box">
            		<article class="content-wrap">
		              <!-- 과정운영 평가 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title"><c:out value="${empty curriculumVO.professorSatisfyType ? '7' : '8'}"/>. 과정운영 평가</div>
		                </div>
		              </div>
		              <div class="content-body">
		                <div class="content-textarea onlyText" contentEditable="false" placeholder="">
		                	<c:out value="${curriculumVO.crclEvtCn}" escapeXml="false"/>
		                </div>
		              </div>
		            </article>
		            <article class="content-wrap">
		            	<!-- 첨부파일 -->
						<c:if test="${not empty curriculumVO.atchFileId}">
							<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${curriculumVO.atchFileId}" />
								<c:param name="imagePath" value="${_IMG }"/>
								<c:param name="commonViewAt" value="Y"/>
							</c:import>
						</c:if>
		            </article>
		            <c:if test="${empty searchVO.plId and param.popupAt ne 'Y'}">
			            <div class="page-btn-wrap">
		            		<c:if test="${(searchVO.menuId ne 'MNU_0000000000000062' and searchVO.menuId ne 'MNU_0000000000000059')}">
			            		<c:url var="listUrl" value="${param.menuId eq 'MNU_0000000000000096' ? '/lms/cla/manageReportList.do' : '' }">
				  					<c:param name="menuId" value="${param.menuId}"/>
				   				</c:url>
			              		<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
			              	</c:if>
			              	<c:if test="${managerAt eq 'Y' and curriculumVO.processSttusCode ne '12'}">
			              		<a href="#" id="btnUptBox" class="btn-xl btn-point">수정</a>
			              	</c:if>
			            </div>
		            </c:if>
	            </div>
	            
            	<c:if test="${managerAt eq 'Y' and curriculumVO.processSttusCode ne '12' and param.popupAt ne 'Y'}">
            		<div id="upt_box" style="display:none;">
	            		<form name="detailForm" id="detailForm" method="post" action="/lms/manage/updateReport.do" enctype="multipart/form-data" onsubmit="return vali();">
	            			<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
							<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
							<input type="hidden" name="step" value="${searchVO.step}"/>
							<input type="hidden" id="fileGroupId" name="fileGroupId" value="${curriculumVO.atchFileId}"/>
							<input type="hidden" id="atchFileId" name="atchFileId" value="${curriculumVO.atchFileId}"/>
	
	            			<article class="content-wrap">
				              <!-- 과정운영 평가 -->
				              <div class="content-header">
				                <div class="title-wrap">
				                  <div class="title">
				                  	${not empty surveyAnswer ? '7.' : '6.' }  과정운영 평가
				                  </div>
				                </div>
				              </div>
				              <div class="content-body">
				              	<textarea id="crclEvtCn" name="crclEvtCn" style="width:99%;height:500px;"><c:out value="${curriculumVO.crclEvtCn}"/></textarea>
				              </div>
				            </article>
				            <article class="content-wrap">
				              <!-- 첨부파일 -->
				              <div class="file-attachment-wrap">
				                <c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
									<c:param name="editorId" value="${_EDITOR_ID}"/>
									<c:param name="estnAt" value="N" />
							    	<c:param name="param_atchFileId" value="${curriculumVO.atchFileId}" />
							    	<c:param name="imagePath" value="${_IMG }"/>
							    	<c:param name="regAt" value="Y"/>
							    	<c:param name="commonAt" value="Y"/>
							    	<c:param name="crclId" value="${curriculumVO.crclId}"/>
								</c:import>
				              </div>
				            </article>
			            </form>
			            
			            <div class="page-btn-wrap">
			              <a href="#" id="btnUpt" class="btn-xl btn-point">수정</a>
			            </div>
			        </div>
	            </c:if>
          </section>
<c:choose>
<c:when test="${param.popupAt eq 'Y'}">
	</body>
	</html>
</c:when>
<c:otherwise>
	<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</c:otherwise>
</c:choose>
