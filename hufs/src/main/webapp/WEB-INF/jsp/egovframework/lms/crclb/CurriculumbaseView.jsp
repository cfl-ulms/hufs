<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<div class="page-content-header">
  <div class="title-wrap">
    <h2 class="title">${resultVO.crclbNm }</h2>
  </div>
</div>
<section class="page-content-body">
  <article class="content-wrap">
    <div class="content-header">
      <div class="title-wrap">
        <div class="title">필수정보</div>
      </div>
    </div>
    <div class="content-body">
      <!-- 테이블영역-->
      <table class="common-table-wrap size-sm left-align">
        <colgroup>
          <col class='bg-gray' style='width:20%'>
          <col style='width:80%'>
        </colgroup>
        <tbody>
          <tr class="">
            <td scope='row' class='font-700'>기본과정코드 <i class='font-point font-400'>*</i></td>
            <td>${resultVO.crclbId }</td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>과정체계 <i class='font-point font-400'>*</i></td>
            <td>${resultVO.sysCodeNmPath }</td>
          </tr>
        </tbody>
      </table>
    </div>
  </article>
  <article class="content-wrap">
    <div class="content-header">
      <div class="title-wrap">
        <div class="title">기본정보</div>
      </div>
    </div>
    <div class="content-body">
      <!-- 테이블영역-->
      <table class="common-table-wrap size-sm left-align">
        <colgroup>
          <col class='bg-gray' style='width:20%'>
          <col style='width:30%'>
          <col class='bg-gray' style='width:20%'>
          <col style='width:30%'>
        </colgroup>
        <tbody>
          <tr class="">
            <td scope='row' class='font-700'>이수구분 <i class='font-point font-400'>*</i></td>
            <td>${resultVO.divisionNm }</td>
            <td scope='row' class='font-700'>학점인정 여부</td>
            <c:choose>
            	<c:when test="${resultVO.gradeAt eq 'Y'}">
            		<td>예(${resultVO.gradeNum})</td>
            	</c:when>
            	<c:otherwise>
            		<td class='font-red'>아니오</td>
            	</c:otherwise>
            </c:choose>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>관리구분 <i class='font-point font-400'>*</i></td>
            <td>${resultVO.controlNm }</td>
            <td scope='row' class='font-700'>프로젝트 과정 여부</td>
            <td>${resultVO.projectAt eq 'Y' ? '프로젝트 과정' : '프로젝트 과정 아님' }</td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>대상 <i class='font-point font-400'>*</i></td>
            <td>${resultVO.targetType eq 'Y' ? '본교생' : '일반' } > ${resultVO.targetDetailNm }</td>
            <td scope='row' class='font-700'>총 시간 등록과정 여부</td>
            <td>${resultVO.totalTimeAt eq 'Y' ? '총 시간 등록과정' : '총 시간 등록과정 아님' }</td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>성적처리기준 <i class='font-point font-400'>*</i></td>
            <td colspan='3'>${resultVO.evaluationAt eq 'Y' ? '절대' : '상대' }평가 
            <c:choose>
            	<c:when test="${resultVO.gradeType eq 'CTG_0000000000000090'}">> 100점 기준 점수 환산표(Letter Grade)</c:when>
            	<c:when test="${resultVO.gradeType eq 'CTG_0000000000000091'}">> P/F</c:when>
            </c:choose>
            </td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>운영보고서 유형 <i class='font-point font-400'>*</i></td>
            <td colspan='3'>
            	<c:choose>
            		<c:when test="${resultVO.reportType eq 'PRJ'}">프로젝트과정</c:when>
            		<c:when test="${resultVO.reportType eq 'NOR'}">일반과정</c:when>
            	</c:choose>
            </td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
            <td>
            	<c:forEach var="result" items="${surveyList}" varStatus="status">
					<c:if test="${result.schdulId eq resultVO.surveySatisfyType}">${result.schdulNm}</c:if>
				</c:forEach>
            </td>
            <td scope='row' class='font-700'>교원대상 과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
            <td>
            	<c:forEach var="result" items="${surveyList3}" varStatus="status">
					<c:if test="${result.schdulId eq resultVO.professorSatisfyType}">${result.schdulNm}</c:if>
				</c:forEach>
            </td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>개별 수업 만족도 조사 실시 여부 <i class='font-point font-400'>*</i></td>
            <c:choose>
            	<c:when test="${resultVO.surveyIndAt eq 'Y'}">
            		<td>예</td>
            	</c:when>
            	<c:otherwise>
            		<td class='font-red'>아니오</td>
            	</c:otherwise>
            </c:choose>
            <td scope='row' class='font-700'>학생 수강신청 제출서류 여부 <i class='font-point font-400'>*</i></td>
            <c:choose>
            	<c:when test="${resultVO.stdntAplyAt eq 'Y'}">
            		<td>예</td>
            	</c:when>
            	<c:otherwise>
            		<td class='font-red'>아니오</td>
            	</c:otherwise>
            </c:choose>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>신청서</td>
            <td colspan='3'><a href='/cmm/fms/FileDown.do?atchFileId=${resultVO.aplyFile}&fileSn=0'><i class='icon-download mr-10'>다운로드</i>${resultVO.aplyFileNm}</a></td>
          </tr>
          <tr class="">
            <td scope='row' class='font-700'>계획서</td>
            <td colspan='3'><a href='/cmm/fms/FileDown.do?atchFileId=${resultVO.planFile}&fileSn=0'><i class='icon-download mr-10'>다운로드</i>${resultVO.planFileNm}</a></td>
          </tr>
        </tbody>
      </table>
    </div>
  </article>
  <article class="content-wrap">
    <div class="content-header">
      <div class="title-wrap">
        <div class="title">(책임교원대학) 과정 학습 참고자료</div>
      </div>
    </div>
    <div class="content-body">
      <!-- 테이블영역-->
      <table class="common-table-wrap size-sm left-align">
        <colgroup>
          <col class='bg-gray' style='width:20%'>
          <col style='width:80%'>
        </colgroup>
          <tbody>
            <tr class="">
              <td scope='row' class='font-700'>참고자료</td>
              <td><a href='/cmm/fms/FileDown.do?atchFileId=${resultVO.etcFile}&fileSn=0'><i class='icon-download mr-10'>다운로드</i>${resultVO.etcFileNm}</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </article>
    <div class="page-btn-wrap mt-50">
      <a href="/lms/crclb/CurriculumbaseList.do?menuId=MNU_0000000000000057" class="btn-xl btn-outline-gray font-basic">목록으로</a>
    </div>
  </section>
</div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</form>
