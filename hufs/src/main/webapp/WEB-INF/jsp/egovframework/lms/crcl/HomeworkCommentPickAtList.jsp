<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="boardType" value="${param.boardType}"/>
    <%-- <c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
    <c:param name="searchCourseReviewSj" value="${searchVO.courseReviewSj}"/> --%>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
});
</script>

<section class="page-content-body">
            <article class="content-wrap">
              <!-- 게시판 검색영역 -->
              <form name="frm" method="post" action="<c:url value="/lms/crcl/selectHomeworkCommentPickAtList.do"/>">
                  <input type="hidden" name="boardType" value="${param.boardType }" />
                  <input type="hidden" name="menuId" value="${param.menuId }" />
                  
	              <div class="box-wrap mb-40">
	                <h3 class="title-subhead">과정후기 검색</h3>
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select class="select2" name="searchCrclLang" id="searchCrclLang" data-select="style3">
	                        <option value="">전체</option>
	                        <c:forEach var="result" items="${languageList}" varStatus="status">
	                            <c:if test="${not empty result.upperCtgryId}">
	                            	<c:if test="${result.ctgryId ne 'ALL'}">
		                                <option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
	                            	</c:if>
	                            </c:if>
	                        </c:forEach>
	                      </select>
	                    </div>
	                  </div>
<%-- 	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <input type="text" name="searchCrclNm" placeholder="찾으시는 과정명을 입력해보세요" value="${param.searchCrclNm }">
	                    </div>
	                  </div> --%>
	                  <div class="flex-ten-col-7">
	                    <div class="ell">
	                      <input type="text" name="searchCourseReviewSj" placeholder="제목을 입력해보세요" value="${param.searchCourseReviewSj }">
	                    </div>
	                  </div>
	                </div>
	
	                <button class="btn-sm font-400 btn-point mt-20">검색</button>
	              </div>
	          </form>
            </article>

            <article class="content-wrap">
              <!-- 게시판 -->
              <div class="content-header">
                <div class="board-type-wrap">
                  <!-- 게시판 타입-->
                  <a href="/lms/crcl/selectHomeworkCommentPickAtList.do?boardType=list&menuId=${param.menuId }" class="btn-list <c:if test="${param.boardType eq 'list' }">on</c:if>" title="리스트형"></a><i class="division-line"></i>
                  <a href="/lms/crcl/selectHomeworkCommentPickAtList.do?boardType=photo&menuId=${param.menuId }" class="btn-gallery <c:if test="${param.boardType eq 'photo' }">on</c:if>" title="갤러리형"></a>
                </div>
              </div>
              <div class="content-body">
                <c:choose>
                    <c:when test="${param.boardType eq 'list' }">
                        <!-- 테이블영역-->
		                <table class="common-table-wrap table-type-board">
		                  <colgroup>
		                    <col style='width:7%'>
		                    <col style='width:12%'>
		                    <col>
		                    <col style='width:14%'>
		                    <col style='width:14%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-light-gray font-700'>
		                      <th scope='col'>No</th>
		                      <th scope='col'>구분</th>
		                      <!-- <th scope='col'>과정명</th> -->
		                      <th scope='col'>제목</th>
		                      <th scope='col'>작성자</th>
		                      <th scope='col'>등록일</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                    <c:forEach var="noticeList" items="${noticeList}" varStatus="status">
		                        <c:url var="noticeViewUrl" value="/lms/crcl/selecCourseReviewAtView.do${_BASE_PARAM }">
		                            <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		                            <%-- <c:param name="hwId" value="${result.hwId }" />
		                            <c:param name="hwsId" value="${result.hwsId }" />--%>
		                            <c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
		                            <c:param name="searchCourseReviewSj" value="${searchVO.courseReviewSj}"/>
		                            <c:param name="crclLang" value="${noticeList.crclLang }"/> 
		                            <c:param name="courseReviewSeq" value="${noticeList.courseReviewSeq}"/>
		                        </c:url>
		                        <tr class="" onclick="location.href='${noticeViewUrl}'">
		                          <td scope='row'>공지</td>
		                          <td class="break-all">${noticeList.crclLangNm }</td>
		                          <%-- <td><span class="text dotdotdot" style="overflow-wrap: break-word;">${noticeList.crclNm }</span></td> --%>
		                          <td class="title">
		                              <div class="inner-wrap">
		                                  <span class="text dotdotdot" style="overflow-wrap: break-word;">${noticeList.courseReviewSj}</span>
		                                  <c:if test="${!empty noticeList.atchFileId }">
		                                      <i class="icon-clip ml-10"></i>
		                                  </c:if>
		                              </div>
		                          </td>
		                          <td>관리자</td>
		                          <td>${noticeList.lastUpdusrPnttm }</td>
		                        </tr>
		                    </c:forEach>
		                  </tbody>
		                  <tbody>
		                    <c:forEach var="result" items="${selectCourseReviewList}" varStatus="status">
		                        <c:url var="viewUrl" value="/lms/crcl/selecCourseReviewAtView.do${_BASE_PARAM }">
		                            <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
		                            <%-- <c:param name="hwId" value="${result.hwId }" />
		                            <c:param name="hwsId" value="${result.hwsId }" />--%>
		                            <c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
		                            <c:param name="searchCourseReviewSj" value="${searchVO.searchCourseReviewSj}"/>
		                            <c:param name="crclLang" value="${result.crclLang }"/> 
		                            <c:param name="courseReviewSeq" value="${result.courseReviewSeq}"/> 
		                        </c:url>
		                        <tr class="" onclick="location.href='${viewUrl}'">
		                          <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
		                          <td class="break-all">${result.crclLangNm }</td>
		                          <%-- <td><span class="text dotdotdot" style="overflow-wrap: break-word;">${result.crclNm }</span></td> --%>
		                          <td class="title">
		                              <div class="inner-wrap">
		                                  <span class="text dotdotdot" style="overflow-wrap: break-word;">${result.courseReviewSj}</span>
		                                  <c:if test="${!empty result.atchFileId }">
		                                      <i class="icon-clip ml-10"></i>
		                                  </c:if>
		                              </div>
		                          </td>
		                          <td>${result.userNm }</td>
		                          <td>${result.lastUpdusrPnttm }</td>
		                        </tr>
		                    </c:forEach>
		                    <c:if test="${fn:length(selectCourseReviewList) == 0}">
		                        <tr>
		                            <td class="listCenter" colspan="5"><spring:message code="common.nodata.msg" /></td>
		                        </tr>
		                    </c:if>
		                  </tbody>
		                </table>
                    </c:when>
                    <c:otherwise>
                        <!-- 갤러리 -->
		                <div class="flex-row board-gallery-thumb-wrap">
		                  <c:forEach var="noticeList" items="${noticeList}" varStatus="status">
		                      <c:url var="noticeViewUrl" value="/lms/crcl/selecCourseReviewAtView.do${_BASE_PARAM }">
                                  <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
           <%--                        <c:param name="hwId" value="${result.hwId }" />
                                  <c:param name="hwsId" value="${result.hwsId }" />--%>
		                            <c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
		                            <c:param name="searchCourseReviewSj" value="${searchVO.searchCourseReviewSj}"/>
		                            <c:param name="crclLang" value="${noticeList.crclLang }"/> 
		                            <c:param name="courseReviewSeq" value="${noticeList.courseReviewSeq}"/>
                              </c:url>
                              
                              <%-- 배경 이미지 --%>
                              <c:choose>
                                  <c:when test="${!empty noticeList.groupStreFileNm }"><c:set var="backgroundImg" value="${HomeworkSubmitFileStoreWebPath }/SITE_000000000000001/HOMEWORKSUBMIT/${result.groupStreFileNm}"/></c:when>
                                  <c:otherwise><c:set var="backgroundImg" value="/template/lms/imgs/common/img_no_image.svg"/></c:otherwise>
                              </c:choose>

			                  <div class="flex-col-4">
			                    <a href="#none" onclick="location.href='${noticeViewUrl}'">
			                      <div class="board-gallery-img" style="background-image:url(${backgroundImg});">
			                        <span class="text-hide">이미지썸네일</span>
			                      </div>
			                      <div class="board-gallery-contents">
			                        <p class="gallery-country">${noticeList.crclLangNm }</p>
			                        <%-- <p class="board-gallery-subtitle ell">${noticeList.crclNm } </p> --%>
			                        <p class="board-gallery-title ell">${noticeList.courseReviewSj}</p>
			                      </div>
			                    </a>
			                  </div>
		                  </c:forEach>
                        </div>
		                <div class="flex-row board-gallery-thumb-wrap">
		                  <c:forEach var="result" items="${selectCourseReviewList}" varStatus="status">
		                      <c:url var="viewUrl" value="/lms/crcl/selecCourseReviewAtView.do${_BASE_PARAM }">
                                  <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${searchVO.pageIndex}" /></c:if>
           <%--                        <c:param name="hwId" value="${result.hwId }" />
                                  <c:param name="hwsId" value="${result.hwsId }" />--%>
		                            <c:param name="searchCrclLang" value="${searchVO.searchCrclLang}"/>
		                            <c:param name="searchCourseReviewSj" value="${searchVO.searchCourseReviewSj}"/>
		                            <c:param name="crclLang" value="${result.crclLang }"/> 
		                            <c:param name="courseReviewSeq" value="${result.courseReviewSeq}"/> 
                              </c:url>
                              
                              <%-- 배경 이미지 --%>
                              <c:choose>
                                  <c:when test="${!empty result.groupStreFileNm }"><c:set var="backgroundImg" value="${HomeworkSubmitFileStoreWebPath }/SITE_000000000000001/HOMEWORKSUBMIT/${result.groupStreFileNm}"/></c:when>
                                  <c:otherwise><c:set var="backgroundImg" value="/template/lms/imgs/common/img_no_image.svg"/></c:otherwise>
                              </c:choose>

			                  <div class="flex-col-4">
			                    <a href="#none" onclick="location.href='${viewUrl}'">
			                      <div class="board-gallery-img" style="background-image:url(${backgroundImg});">
			                        <span class="text-hide">이미지썸네일</span>
			                      </div>
			                      <div class="board-gallery-contents">
			                        <p class="gallery-country">${result.crclLangNm }</p>
			                        <p class="board-gallery-subtitle ell">${result.crclNm } </p>
			                        <p class="board-gallery-title ell">${result.courseReviewSj}</p>
			                      </div>
			                    </a>
			                  </div>
		                  </c:forEach>
                        </div>
                    </c:otherwise>
                </c:choose>
                <c:if test="${user != null }">
	                <div style="float: right;">
	                	<!-- <button class="btn-sm font-400 btn-point mt-20">등록</button> -->
	                	<a href="<c:url value="/lms/crcl/selectHomeworkCommentPickArticle.do${_BASE_PARAM }"/>" class="btn-sm font-400 btn-point mt-20">등록</a>
	                </div>
                </c:if>
                <%-- 페이징 --%>
                <div class="pagination center-align mt-60">
                    <div class="pagination-inner-wrap overflow-hidden inline-block">
                        <c:url var="startUrl" value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM}">
                            <c:param name="pageIndex" value="1" />
                        </c:url>
                        <button class="start goPage" data-url="${startUrl}"></button>
                       
                        <c:url var="prevUrl" value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM}">
                            <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
                        </c:url>
                        <button class="prev goPage" data-url="${prevUrl}"></button>
                       
                        <ul class="paginate-list f-l overflow-hidden">
                            <c:url var="pageUrl" value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM}"/>
                            <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
                            <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
                        </ul>
                       
                        <c:url var="nextUrl" value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM}">
                            <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
                        </c:url>
                        <button class="next goPage" data-url="${nextUrl}"></button>
                       
                        <c:url var="endUrl" value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM}">
                            <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
                        </c:url>
                        <button class="end goPage" data-url="${endUrl}"></button>
                    </div>
                </div>
              </div>
            </article>
          </section>
        </div>

</div>
</div>
</div>

  <div id="waiting_list_modal" class="alert-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <div class="modal-scroll-wrap">
            <table class="common-table-wrap font-700 left-align">
              <tbody id="waitingMemberList">
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button class="btn-xl btn-point btnModalConfirm">확인</button>
          </div>
        </div>
      </div>
    </div>
    </div>
    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>