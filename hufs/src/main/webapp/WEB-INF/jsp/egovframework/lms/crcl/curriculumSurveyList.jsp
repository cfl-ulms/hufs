<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="crclId" value="${param.crclId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>


<script>
function fnSelectMember(schdulId, schdulClCode, plId, crclId, searchSubmitAt){
	$.ajax({
		url : "/lms/crcl/curriculumSurveyMember.json",
		dataType : "json",
		type : "POST",
		data : {"schdulId" : schdulId, "schdulClCode" : schdulClCode, "plId" : plId, "crclId" : crclId, "searchSubmitAt" : searchSubmitAt},
		success : function(data){
			var memberList = data.memberList;
			var webPath = data.MembersFileStoreWebPath;
			
			$(".modal-title").html("<a href='#' class='btn-list' data-schdulid='" + schdulId + "' data-code='" + schdulClCode + "' data-plid='" + plId + "' data-crclid='" + crclId + "' data-at='N'>미제출자 " + data.surveyMemberSummary.submitNCnt + "명</a> | " +
									"<a href='#' class='btn-list' data-schdulid='" + schdulId + "' data-code='" + schdulClCode + "' data-plid='" + plId + "' data-crclid='" + crclId + "' data-at='Y'>제출자 " + data.surveyMemberSummary.submitYCnt + "명</a>");
			
			if(memberList.length != 0){
				var html = "";
				for(var i=0; i < memberList.length; i++){
					html += "<tr>";
					html += 	"<td class=\"pl-20 pr-20\">";

					if(memberList[i].photoStreFileNm == null || memberList[i].photoStreFileNm == ""){
						html += 		"<i class=\"icon-user mr-10\" ></i>";
					}else{
						var tempUrlPath = webPath+memberList[i].photoStreFileNm
						html += 		"<i class=\"icon-user mr-10\" style=\"background-image: url("+tempUrlPath+")\"></i>";
					}
					html +=			memberList[i].userNm+"<span class=\"font-sm\">("+memberList[i].userId+")</span>";
					html += 	"</td>";
					html += "</tr>";
				}

				$(".modal-body .common-table-wrap tbody").html(html);
				$("#waiting_list_modal").show();

			}
			return false;
		}, error : function(){
			alert("error");
		}
	});
}

$(document).on("click", ".btn-list", function(){
	var schdulId = $(this).data("schdulid"),
		schdulClCode = $(this).data("code"),
		plId = $(this).data("plid"),
		crclId = $(this).data("crclid"),
		searchSubmitAt = $(this).data("at");
	
	fnSelectMember(schdulId, schdulClCode, plId, crclId, searchSubmitAt);
});

function modalClose(){
	$("#waiting_list_modal").hide();
}

function dateChk(endDate, viewUrl, limitDate){
	var curDate = "${curDate}";
	endDate = endDate.replace(/-/gi,"");
	curDate = curDate.replace(/-/gi,"");

	if(curDate < endDate){
		alert("설문 참여일이 아닙니다.");
		return false;
	}else if(limitDate != '' && curDate >= limitDate){
		alert("설문에 참여할 수 없습니다.");
		return false;
	}else{
		location.href = viewUrl;
	}

}

</script>

<style>
tr.success{
	background:#bfbfbf
}

</style>

          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
          </div>
          <section class="page-content-body">
            <article class="content-wrap">
              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="10"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
            </article>
            
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap ">
                <colgroup>
                  <col style='width:10%'>
                  <col>
                  <col>
                  <col>
                  <col>
                  <col style='width:10%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>설문구분</th>
                    <th scope='col'>수업주제</th>
                    <th scope='col'>설문 제출 시작일</th>
                    <th scope='col'>제출현황</th>
                    <th scope='col'>만족도</th>
                  </tr>
                </thead>
                <tbody>
                  <c:choose>
					<c:when test="${not empty surveyCurriculumList }">
						<c:forEach var="result" items="${surveyCurriculumList}" varStatus="status">
							<tr class="${result.memberCnt ne 0 and result.joinCnt eq result.memberCnt ? 'bg-point-light cursor-pointer' : ''}">
								<td>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
								<c:url var="viewUrl" value="/lms/crcl/curriculumSurveyView.do">
									<c:param name="schdulId" value="${result.schdulId}"/>
									<c:param name="crclId" value="${param.crclId}"/>
									<c:if test="${result.schdulClCode eq 'TYPE_2' }">
										<c:param name="plId" value="${result.id}"/>
									</c:if>
									<c:param name="menuId" value="${param.menuId}"/>
								</c:url>

								<td><a href="${viewUrl}">${result.type }</a></td>
								<td>${result.studySubject }</td>
								<td>
									${result.endDate}
									<c:if test="${result.type eq '과정만족도'}"> ${result.endTime}</c:if>
									<%-- <c:choose>
										<c:when test="${fn:indexOf(result.dDay,'-') != -1 }">
											( D${fn:replace(result.dDay,'-', '+')} )
										</c:when>
										<c:otherwise>
											( D-${result.dDay} )
										</c:otherwise>
									</c:choose> --%>
								</td>

								<fmt:formatNumber value="${result.joinCnt}" type="number" var="joinCnt" />
								<fmt:formatNumber value="${result.memberCnt}" type="number" var="memberCnt" />
								<c:choose>
									<c:when test="${ memberCnt ne 0 and result.joinCnt lt result.memberCnt}">
										<c:if test="${result.schdulClCode eq 'TYPE_2' }">
											<c:set var="plId" value="${result.id}"/>
										</c:if>
										<td><a href="#none" onclick="fnSelectMember('${result.schdulId}','${result.schdulClCode}', '${plId }' ,'${param.crclId}','N');event.stopPropagation();">${result.joinCnt}/${result.memberCnt }</a></td>
									</c:when>
									<c:otherwise>
	
										<td> ${result.joinCnt}/${result.memberCnt }</td>
									</c:otherwise>
								</c:choose>
								
								<c:if test="${result.sum ne 0 }">
									<c:choose>
										<c:when test="${result.schdulClCode eq 'TYPE_1' or result.schdulClCode eq 'TYPE_3'}">
											<fmt:formatNumber var="resultPoint"  pattern="0" value="${result.sum}"/>
										</c:when>
										<c:otherwise>
											<fmt:formatNumber var="resultPoint"  pattern="0" value=" ${(result.sum / result.joinCnt)*100} " />
										</c:otherwise>
									</c:choose>
								</c:if>
								<td> ${result.sum eq 0 ? '-' :  resultPoint}</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td colspan="6" class="alC">검색 결과가 없습니다.</td>
						</tr>
					</c:otherwise>
				</c:choose>
                </tbody>
              </table>
            </article>
          </section>


      <div id="waiting_list_modal" class="alert-modal" style="display:none;">
	    <div class="modal-dialog modal-sm">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h4 class="modal-title">
	          	<a href="/lms/manage/curriculumSurveyMember.do${_BASE_PARAM}">미제출자 <c:out value="${surveyMemberSummary.submitNCnt}"/>명</a> | 
				<a href="/lms/manage/curriculumSurveyMember.do${_BASE_PARAM}&searchSubmitAt=Y">제출자  <c:out value="${surveyMemberSummary.submitYCnt}"/>명</a>
	          </h4>
	          <button type="button" class="btn-modal-close btnModalClose" onclick="modalClose()"></button>
	        </div>
	        <div class="modal-body">
	          <div class="modal-scroll-wrap">
	            <table class="common-table-wrap font-700 left-align">
	              <tbody>
	              </tbody>
	            </table>
	          </div>
	          <div class="modal-footer">
	            <button class="btn-xl btn-point btnModalConfirm" onclick="modalClose()">확인</button>
	          </div>
	        </div>
	      </div>
	    </div>
	    </div>