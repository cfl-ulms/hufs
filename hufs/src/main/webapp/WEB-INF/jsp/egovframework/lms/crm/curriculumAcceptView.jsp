<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
    <c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
    <c:if test="${not empty searchVO.searchPlanSttusCode}"><c:param name="searchPlanSttusCode" value="${searchVO.searchPlanSttusCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {
    //승인 상태 변경 모달
    $(".btnModalOpen").click(function () {
        var sttus = $(this).data("sttus");
        
        //상태 변경 여러개 처리 확인을 위한 값
        $("#allCheck").val("one");

        //버튼 클릭 체크
        if(sttus != "") {
            $(".sttus_" + sttus).prop('checked', true);
            $("#userId").val($(this).data("userid"));
        }
    });
    
    $(".btnModalOpenCheckBoxAll").click(function () {
    	var checkListCnt = $("input:checkbox[class='checkList']:checked").length;
    	
    	if(checkListCnt <= 0) {
    		alert("대상자를 선택해 주세요.");

    		return false;
    	}

    	//상태 변경 여러개 처리 확인을 위한 값
    	$("#allCheck").val("all");
    	
    	//승인으로 기본값 설정
    	$(".sttus_1").prop('checked', true);
    	
    	$("#change_status_approval_modal").show();
    });

    //승인상태 변경
    $(".btnModalConfirm").click(function(){
    	var allCheck = $("#allCheck").val();
    	
    	if(allCheck == "all") {
    		var sttus = $("input[name='sttus']:checked").val();

    		//모달의 상태값 가져와서 sttusAllForm에 sttus 셋팅
    		$("#sttusAllForm input[name='sttus']").val(sttus);

    		$("#sttusAllForm").submit();
    	} else {
    		$("#sttusForm").submit();
    	}
    });
    
    //체크 박스 여러개 처리
    $(document).on("click", ".allCheck", function(){
	    if($("input:checkbox[class='allCheck']").is(":checked") == true) {
	    	$("input[class='checkList']:checkbox").prop("checked", true);
		} else {
			$("input[class='checkList']:checkbox").prop("checked", false);
		}
    });

    //수강신청 조기 종료 업데이트
    $(".btn_update").click(function(){
        $("#acceptForm").submit();
    });
});
</script>

          <%-- 담당 교수만 변경하도록 처리 --%>
          <%-- 
          <c:forEach var="user" items="${subUserList}" varStatus="status">
              <c:if test="${user.userId eq USER_INFO.id }">
                  <c:set var="updateFlag" value="true"/>
              </c:if>
          </c:forEach>
		   --%>
		  <c:if test="${managerAt eq 'Y'}">
		  	<c:set var="updateFlag" value="true"/>
		  </c:if>
		  
          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="Y"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumrequest"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
                <c:param name="managerAt" value="${managerAt}"/>
            </c:import>
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <form name="frm" method="post" action="<c:url value="/lms/crm/curriculumAccept.do"/>">
                    <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
                    <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
                    <input type="hidden" name="menuId" value="${param.menuId}"/>
                    <input type="hidden" name="subtabstep" value="3"/>
            
	                <h3 class="title-subhead">수강대상자 검색</h3>
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-2">
	                    <div class="ell">
	                      <input type="text" name="searchHostCode" value="${searchVO.searchHostCode }" placeholder="소속" />
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-2">
	                    <div class="ell">
	                      <select name="searchSttus" id="searchSttus" class="select2" data-select="style3">
	                        <option value="">승인상태</option>
	                        <option value="1" <c:if test="${'1' eq searchVO.searchSttus}">selected="selected"</c:if>>승인</option>
			                <option value="2" <c:if test="${'2' eq searchVO.searchSttus}">selected="selected"</c:if>>대기</option>
			                <option value="3" <c:if test="${'3' eq searchVO.searchSttus}">selected="selected"</c:if>>취소</option>
			                <option value="4" <c:if test="${'4' eq searchVO.searchSttus}">selected="selected"</c:if>>환불</option>
	                      </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy }" class="onlyNum" placeholder="생년월일 8자리" />
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <input type="text" value="${searchVO.searchUserNm }" name="searchUserNm" placeholder="이름" />
	                    </div>
	                  </div>
	                </div>
	
	                <button class="btn-sm font-400 btn-point mt-20">검색</button>
	            </form>
              </div>
            </article>
            <article class="content-wrap">
              <div class="content-header">
                <div class="btn-group-wrap">
                  <c:if test="${updateFlag eq 'true' and curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4}">
                    <div class="left-area">
                      <button class="btn-md-auto btn-outline-gray btnModalOpenCheckBoxAll" data-modal-type="change_status_approval">선택된 학생 [승인상태] 일괄 변경</button>
                    </div>
                  </c:if>
                </div>
              </div>
              <div class="content-body">
                <form id="sttusAllForm" action="/lms/crm/updateCurriculumAccept.do" method="post">
                    <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
                    <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
                    <input type="hidden" name="menuId" value="${param.menuId }"/>
                    <input type="hidden" name="subtabstep" value="${param.subtabstep }"/>
                    <input type="hidden" name="sttus" value=""/>

	                <!-- 테이블영역-->
	                <table class="common-table-wrap ">
	                  <colgroup>
	                    <col style='width:6%'>
	                    <col style='width:6%'>
	                    <col style='width:14%'>
	                    <col style='width:14%'>
	                    <col style='width:12%'>
	                    <col style='width:8%'>
	                    <col style='width:6%'>
	                    <col style='width:12%'>
	                    <col style='width:13%'>
	                    <col style='width:10%'>
	                    <col style='width:14%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='bg-light-gray font-700'>
	                      <th scope='col' class='center-align'><label class='checkbox'><input type='checkbox' class='allCheck'><span class='custom-checked'></span></label></th>
	                      <th scope='col'>No</th>
	                      <th scope='col'>소속</th>
	                      <th scope='col'>이름(ID)</th>
	                      <th scope='col'>생년월일</th>
	                      <th scope='col'>학번</th>
	                      <th scope='col'>학년</th>
	                      <th scope='col'>전화번호</th>
	                      <th scope='col'>신청일</th>
	                      <th scope='col'>승인상태</th>
	                      <th scope='col'>상태변경</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                    <c:forEach var="member" items="${curriculumMemberList}" varStatus="status">
                        <tr class="">
                            <td scope='row'>
                                <label class='checkbox'>
                                    <input type='checkbox' class='checkList' name="userIdList" value="${member.userId }"><span class='custom-checked'></span>
                                </label>
                            </td>
                            <td scope='row'>${totCnt - status.index }</td>
                            <td>${member.mngDeptNm }</td>
                            <td>
                            	${member.userNm }<br/>
                            	(${member.userId })
                            </td>
                            <td>${member.brthdy }</td>
                            <td>${member.stNumber }</td>
                            <td>${member.stGrade }</td>
                            <td>(+${member.geocode }) ${member.moblphonNo }</td>
                            <td class='keep-all'>${member.frstRegisterPnttm }</td>
                            <td>
                                <c:choose>
		                            <c:when test="${member.sttus eq 1}">승인</c:when>
		                            <c:when test="${member.sttus eq 2}">대기</c:when>
		                            <c:when test="${member.sttus eq 3}">취소</c:when>
		                            <c:when test="${member.sttus eq 4}"><span class='font-red'>환불</span></c:when>
		                        </c:choose>
                            </td>
                            <td>
                                <c:if test="${updateFlag eq 'true' and curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4}">
                                    <a href='#none' class='btn-sm-full btn-outline-gray btnModalOpen' data-modal-type='change_status_approval' data-sttus="${member.sttus }" data-userid="${member.userId }">변경</a>
                                </c:if>    
                            </td>
                        </tr>
                    </c:forEach>
                  </tbody>
                </table>
	            </form>
              </div>
            </article>
            <div class="page-btn-wrap mt-50">
              <c:if test="${curriculumVO.processSttusCodeDate eq '3'}">
                <button class="btn-xl btn-outline-gray btnModalOpen" data-modal-type="confirm" data-modal-header="알림" data-modal-text="${curriculumVO.crclNm }" data-modal-subtext="수강신청을 조기 마감하시려면 해당 과정의 상태를 [수강신청 종료] 상태로 변경해야 합니다. 과정의 상태를 [수강신청 종료] 상태로 변경하시겠습니까?" data-modal-rightbtn="수강신청 종료로 상태 변경">수강신청 조기마감</button>
              </c:if>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  <!-- 수강신청 조기 마감 modal -->
  <div id="confirm_modal" class="alert-modal">
    <jsp:useBean id="now" class="java.util.Date" />
    <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />

    <form id="acceptForm" action="/lms/crm/updateProcessSttusCode.do" method="post">
        <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
        <input type="hidden" name="processSttusCode" value="4"/>
        <input type="hidden" name="applyEndDate" value="${today }"/>
        <input type="hidden" name="forwardUrl" value="/lms/crm/curriculumAccept.do?menuId=${param.menuId}&crclId=${param.crclId}&crclbId=${param.crclbId}&subtabstep=${param.subtabstep}"/>
        <input type="hidden" name="menuId" value="${param.menuId }"/>
        <input type="hidden" name="subtabstep" value="${param.subtabstep }"/>
    </form>

    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text"></p>
          <p class="modal-subtext"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button type="button" class="btn-xl btn-point btn_update">확인</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- 승인상태 변경 modal -->
  <div id="confirm_modal" class="alert-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text"></p>
          <p class="modal-subtext"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button type="button" class="btn-xl btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>
  <div id="change_status_approval_modal" class="alert-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">승인상태 변경</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
            <form id="sttusForm" action="/lms/crm/updateCurriculumAccept.do" method="post">
              <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
              <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
              <input type="hidden" name="userId" id="userId" value=""/>
              <input type="hidden" name="menuId" value="${param.menuId }"/>
              <input type="hidden" name="subtabstep" value="${param.subtabstep }"/>
              <input type="hidden" id="allCheck" value=""/>

	          <p class="modal-text">승인 상태를 변경해주세요.</p>
	          <table class="common-table-wrap size-sm">
	            <colgroup>
	              <col style="width:70px">
	            </colgroup>
	            <thead>
	              <tr class="bg-gray font-700">
	                <th scope="col">선택</th>
	                <th scope="col">상태</th>
	              </tr>
	            </thead>
	            <tbody>
	              <tr>
	                <td>
	                  <label class="checkbox circle">
	                    <input type="radio" class="sttus_1" name="sttus" value="1" />
	                    <span class="custom-checked"></span>
	                  </label>
	                </td>
	                <td>승인</td>
	              </tr>
	              <tr>
	                <td>
	                  <label class="checkbox circle">
	                    <input type="radio" class="sttus_2" name="sttus" value="2" />
	                    <span class="custom-checked"></span>
	                  </label>
	                </td>
	                <td>대기</td>
	              </tr>
	              <tr>
	                <td>
	                  <label class="checkbox circle">
	                    <input type="radio" class="sttus_3" name="sttus" value="3" />
	                    <span class="custom-checked"></span>
	                  </label>
	                </td>
	                <td>취소</td>
	              </tr>
	              <tr>
	                <td>
	                  <label class="checkbox circle">
	                    <input type="radio" class="sttus_4" name="sttus" value="4" />
	                    <span class="custom-checked"></span>
	                  </label>
	                </td>
	                <td>환불</td>
	              </tr>
	            </tbody>
	          </table>
	        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button class="btn-xl btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>