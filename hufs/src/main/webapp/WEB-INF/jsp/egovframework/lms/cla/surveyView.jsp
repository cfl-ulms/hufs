<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId }"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:param name="plId" value="${param.plId }"></c:param>
	<c:if test="${not empty param.menu}"><c:param name="menu" value="${param.menu}" /></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<c:if test="${USER_INFO.userSeCode eq '08'}">
<link rel="stylesheet" href="/template/lms/css/class/class.css">
<style>
.survey-card-wrap{border-top-color:#068467;}
</style>
</c:if>
<script>
$(document).ready(function(){


});

function fnSubmit(){
	var dataArr = [];
	var resultCnt = 0;
	$("ul.survey-check-wrap li").each(function(){
	var dataObj = {};
		var tempVal = "";
		if($(this).hasClass("multi") === true){
			tempVal = $(this).find("input:radio[class='multiChk']:checked").val();
			if(tempVal == undefined){
				alert("설문을 완료해 주세요");
				resultCnt = 1;
				return false;
			}else{
				dataObj.cnsr = tempVal;
				dataObj.qesitmId = $(this).find(".qesitmId").val();
			}
		}else if($(this).hasClass("answer") === true){
			tempVal = $(this).find("textarea").val();
			if(tempVal == undefined || tempVal == ""){
				alert("설문을 완료해 주세요");
				resultCnt = 1;
				return false;
			}
		}
		dataObj.cnsr = tempVal;
		dataObj.qesitmId = $(this).find(".qesitmId").val();
		dataArr.push(dataObj);

	});

	if(resultCnt == 0){
		var jsonData = JSON.stringify(dataArr);
		var tempSchdulClCode = "${ param.schdulClCode}";
		 $.ajax({
			type:"post",
			dataType:"json",
			url:"/lms/cla/insertSurvey.json",
			traditional : true,
			data:{"plId":tempSchdulClCode == "TYPE_2" ? "${param.plId}" : '',"crclId":"${param.crclId}","schdulId":"${param.schdulId}", "answerList":jsonData  },
			success: function(data) {
				alert("제출 되었습니다.");
				location.href = "${referer}";
			},
			error:function() {
				//alert("error");
			}
		});
	}
}

function dateChk(endDate, viewUrl, limitDate){
	var curDate = "${curDate}";
	endDate = endDate.replace(/-/gi,"");
	curDate = curDate.replace(/-/gi,"");
	if(curDate < endDate){
		alert("설문 참여일이 아닙니다.");
		return false;
	}else if(limitDate != '' && curDate >= limitDate){
		alert("설문에 참여할 수 없습니다.");
		return false;
	}else{
		location.href = viewUrl;
	}

}
</script>

		<div class="page-content-header">
            <c:choose>
          		<%-- <c:when test="${param.menuId eq 'MNU_0000000000000163' }"> --%>
          		<c:when test="${empty param.plId}">
          			<c:import url="/lms/crclHeader.do" charEncoding="utf-8"/>
          		</c:when>
          		<c:otherwise>
          			<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
          		</c:otherwise>
          	</c:choose>
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:choose>
                    <c:when test="${!empty param.plId }">
	                    <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
	                        <c:param name="step" value="10"/>
							<c:param name="crclId" value="${curriculumVO.crclId}"/>
							<c:param name="menuId" value="${searchVO.menuId}"/>
							<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
							<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	                    </c:import>
                    </c:when>
                    <c:otherwise>
                        <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
		                    <c:param name="menu" value="${param.menu }"/>
		                    <c:param name="subtabstep" value="${param.subtabstep }"/>
		                    <c:param name="menuId" value="${param.menuId }"/>
		                    <c:param name="crclId" value="${param.crclId}"/>
		                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt }"/>
		                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		                </c:import>
                    </c:otherwise>
                </c:choose>
        </article>

        <article class="content-wrap">
        	<c:if test="${param.menuId ne 'MNU_0000000000000163' and fn:length(resultSurveyList) gt 1 and !empty param.plId}">
	              <div class="content-header">
	                <div class="class-tab-wrap">
	                	<c:forEach	items="${resultSurveyList }" var="result">
	                		<c:if test="${result.schdulClCode ne 'TYPE_3'}">
		                		<c:url var="surveyUrl" value="/lms/cla/surveyView.do${_BASE_PARAM }">
									<c:param name="schdulId" value="${result.schdulId}"></c:param>
									<c:param name="schdulClCode" value="${result.schdulClCode}"></c:param>
								</c:url>
		                		 <a href="#none" onclick="dateChk('${result.endDate}${result.endTime}', '${surveyUrl }', '${result.limitDate }')" class="title ${param.schdulClCode eq result.schdulClCode ? 'on' : '' }">${result.schdulClCode eq 'TYPE_1' ? '과정' : '수업'} 만족도</a>
	                		 </c:if>
						</c:forEach>
	                </div>
	              </div>
              </c:if>
              <c:choose>
              	<c:when test="${answerCnt ne '0' }">
              		<div class="no-content-wrap">
		                <!-- <img src="../imgs/common/icon_no_contents.svg" alt=""> -->
		                <p class="title">설문에 이미 참여하셨습니다.<br>감사합니다.</p>
		              </div>
	              </article>
              	</c:when>
              	<c:otherwise>
              		<c:set var="survayTxt">
	              		<c:choose>
	              			<c:when test="${param.schdulClCode eq 'TYPE_1'}">과정</c:when>
	              			<c:when test="${param.schdulClCode eq 'TYPE_2'}">수업</c:when>
	              			<c:when test="${param.schdulClCode eq 'TYPE_3'}">교원대상 과정</c:when>
	              		</c:choose>
              		</c:set>
					<div class="survey-card-wrap">
		                <a href="#">
		                  <p class="title">
		                  	본 설문지는 ${survayTxt}개선의 기초 및 교육의 질적인 향상을 위해 실시합니다.</p>
		                  <p class="sub-title">
	                   		 ${survayTxt} 설문을 참여하지 않을 경우 과정종료 후 성적조회 등이 원활하지 않을 수 있습니다.<br>
		                    <b class="font-point">꼭 설문에 참여해 주세요.</b>
		                  </p>
		                </a>
		              </div>
            		</article>
              	</c:otherwise>
              </c:choose>

			<c:if test="${answerCnt eq '0' }">
	            <article class="content-wrap">
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">설문 시작</div>
	                </div>
	              </div>
	              <ul class="survey-check-wrap">
	              <c:forEach items="${surveyInfo.questionsArray}" var="question" varStatus="status">
	              	<c:choose>
	              		<c:when test="${question.qesitmTyCode eq 'multiple' }">
	              			<li class="survey-check-items multi">
	              				<input type="hidden" class="qesitmId" value="<c:out value='${question.qesitmId}'/>" />
				                  <p class="title">${question.qesitmSn}. ${question.qesitmSj}</p>
				                  <c:forEach items="${question.examples }" var="example" varStatus="exStatus">
			                  		<label class="checkbox-circle">
					                    <input type="radio" name="survey_multi${status.count}" class="multiChk" value="${example.exId }">
					                    <span class="custom-checked"></span>
					                    <span class="text">${example.exCn } (${example.exSn })</span>
					                  </label>
				                  </c:forEach>
			                </li>
	              		</c:when>
	              		<c:when test="${question.qesitmTyCode eq 'answer'}">
							<li class="survey-check-items answer">
								<input type="hidden" class="qesitmId" value="<c:out value='${question.qesitmId}'/>" />
			                  <p class="title">${question.qesitmSn}. ${question.qesitmSj}</p>
			                  <textarea name="survey_answer${status.count}" placeholder="서술해주십시오."></textarea>
	                		</li>
	              		</c:when>

	              		<%-- <c:when test="${question.qesitmTyCode eq 'table'}">
            				<li class="survey-check-items">
		              		<p class="title">${question.qesitmSn}. ${question.qesitmSj}</p>
			              	<table class="common-table-wrap ${question.qesitmTy ne '6' ? 'size-sm' : '' }">
			                <colgroup>
			                	<c:forEach begin="0" end="${fn:length(question.heightExamples)}" var="colLen" varStatus="status">
			                			<c:if test="${question.qesitmTy eq '6' and status.count eq 1}">
			                				<col style="width: 20%;">
			                			</c:if>
			                		<col style="width: 10%;">
			                	</c:forEach>
			                </colgroup>

			                <c:set var="wIndex" value="0"/>
							<c:forEach begin="0" end="${fn:length(question.widthExamples) }"  step="1" varStatus="wStatus" var="wResult">
								<c:choose>
		                   				<c:when test="${wStatus.first}">
		                   					<thead>
											<c:choose>
                    							<c:when test="${question.qesitmTy eq '6' }">
                    								<tr class="font-700 bg-gray-light">
                    							</c:when>
                    							<c:otherwise>
                       								<tr class="font-700">
                    							</c:otherwise>
                    						</c:choose>

		                   				</c:when>
		                   				<c:otherwise>
	                   						<tr>
		                   				</c:otherwise>
		                   			</c:choose>
			                	<c:set var="hIndex" value="0"/>
                        		<c:forEach begin="0" end="${fn:length(question.heightExamples)}" step="1" varStatus="hStatus" var="hResult" >
	                        		<c:choose>
		                   				<c:when test="${wStatus.first}">
		                   						<c:choose>
		                    						<c:when test="${hStatus.first}">
			                    						<c:choose>
			                    							<c:when test="${question.qesitmTy eq '6' }">
			                    								<th scope="col">내용</th>
			                    							</c:when>
			                    							<c:otherwise>
			                       								<th scope="col"></th>
			                    							</c:otherwise>
			                    						</c:choose>
			                   						</c:when>
		                   							<c:otherwise>
		                   								<th scope="col">${hStatus.index}${question.qesitmTy eq '6' ? '점' : ''}</th>
		                     							<c:set var="hIndex" value="${hIndex + 1}"/>
		                   							</c:otherwise>
		                    					</c:choose>

		                    					<c:if test="${hStatus.last }">
		                    						</thread>
		                    						</tr>
		                    						<tbody>
		                    					</c:if>
		                   				</c:when>

		                   				<c:otherwise>
		                   					<c:choose>
		                    					<c:when test="${hStatus.first }">
                 									<td scope="row">${fn:trim(question.widthExamples[wIndex].exCn) }</td>
													<c:set var="wIndex" value="${wIndex + 1}"/>
		                    					</c:when>
		                    					<c:otherwise>
		                    						<c:choose>
		                    							<c:when test="${question.qesitmTy eq '6' }">
		                    								<td class="table-checkbox">
							                                    <label class="checkbox-circle">
							                                      <input type="radio" name="survey_0${wStatus.index }" value="${hStatus.index }">
							                                      <span class="custom-checked"></span>
							                                    </label>
							                                  </td>
		                    							</c:when>
		                    							<c:otherwise>
		                       								<td></td>
		                    							</c:otherwise>
		                    						</c:choose>
		                    					</c:otherwise>
		                    				</c:choose>
		                    				<c:if test="${hStatus.last }">
	                    						</tr>
	                    					</c:if>
		                   				</c:otherwise>
		                   			</c:choose>
		                   			</c:forEach>
		                   			<c:if test="${wStatus.last }">
		                   				</tbody>
		                   			</c:if>
                   			</c:forEach>
                   			</table>
	              		</c:when> --%>
	              	</c:choose>
	              	</c:forEach>
	              </ul>
	            </article>
	             <div class="page-btn-wrap mt-50">
	             	<c:if test="${param.menuId eq 'MNU_0000000000000163' }">
	              		<a href="/lms/cla/mySurveyList.do${_BASE_PARAM }" class="btn-xl btn-outline-gray">목록으로</a>
	              	</c:if>
	              	<c:if test="${curriculumVO.processSttusCodeDate < 12}">
	              		<a href="#none" class="btn-xl btn-point" onclick="fnSubmit();">제출하기</a>
	              	</c:if>
	            </div>
	           </c:if>
          </section>
      </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>