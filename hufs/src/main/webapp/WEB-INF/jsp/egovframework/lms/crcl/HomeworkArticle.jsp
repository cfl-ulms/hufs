<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}homework/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_JS" value="/template/common/js"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="hwId" value="${searchVO.hwId}"/>
    <c:param name="step" value="${searchVO.step}"/>
    <c:param name="plId" value="${searchVO.plId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){

	//학생 공개
	$(document).on("click", ".stuOpenAtUpdate", function() {
		var dataStuOpenAt = $(this).attr("data-stuopen");
		
		if(dataStuOpenAt == "Y"){
			if(!confirm("과제 피드백과 평가결과를 전체 공개하게 되면, 제출 기한에 상관없이 과제 제출이 더 이상 불가능해집니다. \n계속하시겠습니까?")){
				return false;
			}
		}
		
		$form = $("<form></form>");
        $form.attr("action","/lms/crcl/updateStuOpenAt.do");
        $form.attr('method', 'post');
        $form.appendTo('body');
        
        var hwId = $('<input type="hidden" value="${param.hwId}" name="hwId">');
        var stuOpenAt = $('<input type="hidden" value="' + dataStuOpenAt + '" name="stuOpenAt">');
        var forwardUrl = $('<input type="hidden" name="forwardUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}&stuOpenAt=' + dataStuOpenAt + '"">');

        $form.append(hwId).append(stuOpenAt).append(forwardUrl);
        $form.submit();
	});

	//평가대기 검색
	$(document).on("change", ".searchTargetDetail", function() {
		$form = $("<form></form>");
        $form.attr("action","/lms/crcl/selectHomeworkArticle.do");
        $form.attr('method', 'post');
        $form.appendTo('body');
        
        var hwId = $('<input type="hidden" value="${param.hwId}" name="hwId">');
        var menuId = $('<input type="hidden" value="${param.menuId}" name="menuId">');
        var crclId = $('<input type="hidden" value="${param.crclId}" name="crclId">');
        var step = $('<input type="hidden" value="${param.step}" name="step">');
        var boardType = $('<input type="hidden" value="${param.boardType}" name="boardType">');
        var searchTargetDetail = $('<input type="hidden" value="' + $(".searchTargetDetail option:selected").val() + '" name="searchTargetDetail">');
   
        $form.append(hwId).append(menuId).append(crclId).append(step).append(boardType).append(searchTargetDetail);
        $form.submit();
	});
	
	//후기선정, 취소
	$(document).on("click", ".updateCommentPickAt", function() {
		var dataCommentPickAt = $(this).attr("data-commentpickat");
		var dataHwsId = $(this).attr("data-hwsid");

		if(dataHwsId == "") {
            alert("제출하지 않은 과제는 후기로 선정할 수 없습니다.");
            return false;
        }

        $form = $("<form></form>");
        $form.attr("action","/lms/crcl/updateCommentPickAt.do");
        $form.attr('method', 'post');
        $form.appendTo('body');

        var hwsId = $('<input type="hidden" value="' + dataHwsId + '" name="hwsId">');
        var commentPickAt = $('<input type="hidden" value="' + dataCommentPickAt + '" name="commentPickAt">');
        var forwardUrl = $('<input type="hidden" name="forwardUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}&commentPickAt=' + dataCommentPickAt + '"">');

        $form.append(hwsId).append(commentPickAt).append(forwardUrl);
        $form.submit();
    });
	
	//피드백 Modal 띄우기
	$(document).on("click", ".modalFdb", function() {
		var arrHwsIdCheckedCnt = $('input:checkbox[name=arrHwsId]:checked').length;

		if(0 >= arrHwsIdCheckedCnt) {
			alert("대상을 선택해 주세요.");
			return false;
		}

		$(".spanCheckboxCnt").text(arrHwsIdCheckedCnt);
		
		$("#feedback_write_modal").show();
    });
	
	//피드백  처리
	$(document).on("click", ".btnModalConfirm", function() {
		var validFlag = true;

		$("input:checkbox[name=arrHwsId]:checked").each(function(){
            if($(this).val() == ""){
            	alert("과제 제출을 하지 않은 대상은 피드백이 불가능합니다.");
            	validFlag = false;
            }
        });
        
		if(validFlag == false) {
			return false;
		}
		
		$("input[name=fdb]").val($(".areaFdb").val());
		document.frm.submit();
    });
	
	//과제 제출 상세에서 점수 처리
	$(document).on("click", ".updateFdbScr", function() {
        //점수 input으로 줄때 valid 처리
		<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
		    var scr = $("input[name=scr]").val();
		    
		    if(scr == "") {
                alert("점수를 입력해 주세요.");
                return false;
            }
		    
		    if(scr > 100 || scr < 0) {
		    	alert("점수는 0~100 사이에 숫자만 가능합니다.");
		    	return false;
		    }
		</c:if>

        document.fdbFrm.submit();
    });
});
</script>

<div class="page-content-header">
  <c:choose>
      <c:when test="${empty param.plId }">
          <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
		      <%-- TODO : ywkim 작업해야함 --%>
		      <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
		      <c:param name="curriculumManageBtnFlag" value="Y"/>
		  </c:import>
      </c:when>
      <c:otherwise>
          <c:choose>
            <c:when test="${not empty param.plId and param.tabType eq 'T'}">
                <c:import url="/lms/claHeader.do" charEncoding="utf-8">
                </c:import>
            </c:when>
            <c:otherwise>
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
            </c:import>
            </c:otherwise>
          </c:choose>
      </c:otherwise>
  </c:choose>
</div>
  <section class="page-content-body">  
    <article class="content-wrap">
        <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y' and param.homeworkTotalFlag ne 'Y'}">
            <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                <c:param name="step" value="${searchVO.step }"/>
                <c:param name="crclId" value="${curriculumVO.crclId}"/>
                <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
            </c:import>
        </c:if>
   </article>
   <article class="content-wrap">
     <div class="card-user-wrap">
       <c:forEach var="user" items="${subUserList}" varStatus="status">
           <div class="user-icon"><img src="${MembersFileStoreWebPath }${user.photoStreFileNm}" alt="담당 교수" onerror="this.src='/template/lms/imgs/common/icon_user_name.svg'"></div>
           <div class="user-info">
             <p class="title">
                                             담당교수 <b><c:out value="${user.userNm}"/>(<c:out value="${user.mngDeptNm}"/>)</b></p>
             <p class="sub-title">문의: ${user.emailAdres }</p>
           </div>
       </c:forEach>
     </div>
   </article>
   <article class="content-wrap">
     <!-- 과제 내용 -->
     <div class="content-header">
       <div class="title-wrap">
         <div class="title">과제 내용</div>
       </div>
     </div>
     <div class="content-body subjectViewContain">
       <section class="subject-view-wrap">
         <article class="subject-title-wrap">
           <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
             <p class="subject-badge">과정후기 대상 과제</p>
           </c:if>
           <h2 class="subject-title">${homeworkVO.nttSj }</h2>
           <div class="board-info-modify">
             <div class="board-info-wrap">
               <dl class="item">
                 <dt class="title">과제 오픈일</dt>
                 <dd class="desc ">${homeworkVO.openDate } ${fn:substring(homeworkVO.openTime,0,2) }:${fn:substring(homeworkVO.openTime,2,4) }</dd>
               </dl>
               <dl class="item">
                 <dt class="title">과제 마감일</dt>
                 <dd class="desc ">${homeworkVO.closeDate } ${fn:substring(homeworkVO.closeTime,0,2) }:${fn:substring(homeworkVO.closeTime,2,4) }</dd>
               </dl>
             </div>

             <c:if test="${(managerAt eq 'Y' and homeworkVO.hwCode eq '1') or (studyMngAt eq 'Y' and homeworkVO.hwCode eq '2')}">
	             <ul class="modify-text">
	               <li><a href="/lms/crcl/deleteHomeworkArticle.do${_BASE_PARAM }">삭제</a></li>
	               <li><a href="/lms/crcl/homeworkRegister.do${_BASE_PARAM }&registAction=updt&tabType=${param.tabType}&mode=${empty param.plId ? 'curriculum' : 'lesson' }">수정</a></li>
	             </ul>
	         </c:if>
           </div>
         </article>
         <article class="subject-content-wrap">
           <div class="board-editor-content">
             <!-- 에디터영역 -->
             <div class="">
               <c:out value="${homeworkVO.nttCn}" escapeXml="false" />
             </div>
           </div>
         </article>

		<!-- 첨부파일 -->
		<c:if test="${not empty homeworkVO.atchFileId}">
		    <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
		        <c:param name="param_atchFileId" value="${homeworkVO.atchFileId}" />                                 
		        <c:param name="imagePath" value="${_IMG }"/>
		        <c:param name="commonViewAt" value="Y"/>
		    </c:import>
		</c:if>

       </section>
       <div class="subjectToggle" data-open="true">
         <span class="text"></span>
       </div>
     </div>
   </article>
   
   <%-- 과제 상세 --%>
   <c:if test="${empty param.viewFlag }">
	   <article class="content-wrap">
	     <!-- 과제 피드백 -->
	     <div class="content-header">
	       <div class="title-wrap">
	         <div class="title">과제 피드백</div>
	       </div>
	     </div>
	     <div class="content-body">
	       <form name="searchFrm" action="/lms/crcl/selectHomeworkArticle.do" method="post">
	           <input type="hidden" name="menuId" value="${searchVO.menuId }" />
	           <input type="hidden" name="crclId" value="${searchVO.crclId }" />
	           <input type="hidden" name="step" value="${searchVO.step }" />
	           <input type="hidden" name="hwId" value="${searchVO.hwId }" />
	           <input type="hidden" name="boardType" value="${param.boardType }" />
	
		       <div class="box-wrap mb-40">
		         <h3 class="title-subhead">학생 검색</h3>
		         <div class="flex-row-ten">
		           <div class="flex-ten-col-3">
		             <div class="ell">
		               <select name="searchTargetType" id="searchTargetType" class="select2" data-select="style3">
		                 <option value="">검색 유형</option>
		                 <option value="mngDeptNm" <c:if test="${param.searchTargetType eq 'mngDeptNm'}">selected</c:if>>소속</option>
		                 <option value="userNm" <c:if test="${param.searchTargetType eq 'userNm' }">selected</c:if>>이름</option>
		               </select>
		             </div>
		           </div>
		           <div class="flex-ten-col-7">
		             <div class="ell">
		               <input type="text" placeholder="" name="searchInputValue" value="${param.searchInputValue }">
		             </div>
		           </div>
		         </div>
		
		         <button class="btn-sm font-400 btn-point mt-20">검색</button>
		       </div>
		   </form>
	     </div>
	   </article>
   </c:if>
   <article class="content-wrap">
   <c:choose>
     <%-- 과제 제출 상세 --%>
     <c:when test="${param.viewFlag eq 'homeworksubmit' }">
         <article class="content-wrap">
              <!-- 제출한 과제 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">제출한 과제</div>
                </div>
              </div>
              <div class="content-body">
                <div class="card-user-wrap mb-20">
                  <div class="user-icon"><img src="${MembersFileStoreWebPath }${homeworkSubmitVO.photoStreFileNm}" alt="사람 아이콘" onerror="this.src='/template/lms/imgs/common/icon_user_name.svg'"></div>
                  <div class="user-info">
                    <p class="title">제출자 <b>${homeworkSubmitVO.userNm} (${homeworkSubmitVO.classCnt}조)</b></p>
                  </div>
                </div>
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:80%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>과제명 (국문)</td>
                      <td>${homeworkSubmitVO.nttSj}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과제명 (${curriculumVO.crclLangNm })</td>
                      <td>${homeworkSubmitVO.nttSjLang}</td>
                    </tr>
                    
                    <c:if test="${homeworkVO.hwType eq 2 }">
	                    <tr class="">
	                      <td scope='row' class='font-700'>조 정보</td>
	                      <td>${homeworkSubmitVO.groupCnt}조 (${fn:length(pickStudentList)}명)</td>
	                    </tr>
	                    <tr class="">
	                      <td scope='row' class='font-700'>조 원</td>
	                      <td>
	                          <c:forEach var="pickStudentList" items="${pickStudentList}" varStatus="status">
	                              ${pickStudentList.userNm }
	                              <c:if test="${!status.last}">,</c:if>
	                          </c:forEach>
	                      </td>
	                    </tr>
	                </c:if>
                    <tr class="">
                      <!-- 제출일자 게산을 위한 함수 -->
                      <c:set var="closeDate" value="${fn:replace(homeworkVO.closeDate, '-', '')}${homeworkVO.closeTime }"/>
                      <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                      <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />
                      
                      <fmt:formatDate value="${result.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />
                      
                      <!-- 아래 날짜 표기를 위한 함수 -->
                      <fmt:formatDate var="strLastUpdusrPnttmHHmm" pattern="yyyy-MM-dd HH:mm" value="${homeworkSubmitVO.lastUpdusrPnttm }"/>

                      <!-- 제출 여부 처리  -->
                      <c:choose>
                          <c:when test="${empty homeworkSubmitVO.hwsId }">
                              <c:set var="submitAt" value="미제출"/>
                          </c:when>
                          <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
                              <c:set var="submitAt" value="마감후"/>
                          </c:when>
                          <c:otherwise>
                              <c:set var="submitAt" value="제출"/>
                          </c:otherwise>
                      </c:choose>
                      
                      <!-- 마감 후 제출 날짜 계산 --> 
                      <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
                          <br />
                          <fmt:parseDate value="${fn:substring(homeworkVO.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
                          <fmt:formatDate var="strLastUpdusrPnttm" pattern="yyyy-MM-dd" value="${homeworkSubmitVO.lastUpdusrPnttm }"/>
                          <fmt:parseDate value="${fn:substring(strLastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
                          <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
                          <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
                      </c:if>
                      <td scope='row' class='font-700'>제출일</td>
                      <td>
                          <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
                                             
                         <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
                         <c:if test="${dDay eq 0 }">
                             <c:set var="dDay" value="1"/>
                         </c:if>
                          ${strLastUpdusrPnttmHHmm } (${submitAt } D+${dDay })
                      </td>
                    </tr>
                  </tbody>
                </table>
                <section class="subject-view-wrap mt-20">
                  <article class="subject-content-wrap">
                    <div class="board-editor-content">
                      <!-- 에디터영역 -->
                      <div class="">
                        <c:out value="${homeworkSubmitVO.nttCn}" escapeXml="false" />
                      </div>
                    </div>
                  </article>

                  <!-- 첨부파일 -->
			      <c:if test="${not empty homeworkSubmitVO.atchFileId}">
			          <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
			              <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" />                                 
			              <c:param name="imagePath" value="${_IMG }"/>
			              <c:param name="commonViewAt" value="Y"/>
			          </c:import>
			      </c:if>
                </section>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 교수님 피드백 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교수님 피드백</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <form name="fdbFrm" action="/lms/crcl/updateFdb.do" method="post">
                    <input type="hidden" name="forwardUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}" />
                    <input type="hidden" name="arrHwsId" value="${homeworkSubmitVO.hwsId }" />

	                <table class="common-table-wrap size-sm left-align">
	                  <colgroup>
	                    <col class='bg-gray' style='width:20%'>
	                    <col style='width:80%'>
	                  </colgroup>
	                  <tbody>
	                    <tr class="">
	                      <td scope='row' class='font-700'>점수</td>
	                      <td>
	                        <div class='flex-row'>
	                          <div class='flex-col-7'>
	                            <c:choose>
	                                <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
	                                    <select class='table-select select2' data-select='style1' name="scr">
		                                  <option value='100' <c:if test="${homeworkSubmitVO.scr eq '100'}">selected</c:if>>PASS</option>
		                                  <option value='0' <c:if test="${homeworkSubmitVO.scr eq '0'}">selected</c:if>>FAIL</option>
		                                </select></div>
	                                </c:when>
	                                <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
	                                    <input class="table-input onlyNum" value="${homeworkSubmitVO.scr}" placeholder="00" name="scr" maxlength=3 />
	                                </c:when>
	                            </c:choose>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr class="">
	                      <td>피드백</td>
	                      <td>
	                        <textarea class='table-textarea h-90 onlyText' name="fdb"><c:out value="${homeworkSubmitVO.fdb}" escapeXml="false" /></textarea>
	                      </td>
	                    </tr>
	                  </tbody>
	                </table>
	            </form>
              </div>
            </article>
            <div class="center-align mt-50">
              <a href="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}" class="btn-xl btn-outline-gray">취소</a>
              <c:if test="${(managerAt eq 'Y' and homeworkVO.hwCode eq '1') or (studyMngAt eq 'Y' and homeworkVO.hwCode eq '2')}">
                  <a href="#none" class="btn-xl btn-point updateFdbScr">저장</a>
              </c:if>
            </div>
         </section>
     </c:when>
     <%-- 과제 상세 --%>
     <c:otherwise>
	     <div class="content-header">
	       <div class="feedback-title-wrap">
	         <select class="select2 searchTargetDetail" data-select="style3" name="searchTargetDetail">
	           <option value="" <c:if test="${empty param.searchTargetDetail }">selected</c:if>>전체보기</option>
	           <option value="wait" <c:if test="${param.searchTargetDetail eq 'wait'}">selected</c:if>>평가대기</option>
	         </select>
	         <c:if test="${managerAt eq 'Y' }">
		         <button type="button" class="btn-point btn-md modalFdb" data-modal-type="feedback_write">선택 피드백 남기기</button>
		         <div class="divStuOpenAt" style="margin-right: 10px;">
			         <c:choose>
			             <c:when test="${homeworkVO.stuOpenAt ne 'Y' }"><a href="#none" class="btn-outline btn-md-auto stuOpenAtUpdate" data-stuopen="Y">과제평가결과 학생에게 공개</a></c:when>
			             <c:otherwise><a href="#none" class="btn-outline btn-md stuOpenAtUpdate" data-stuopen="N">공개취소</a></c:otherwise>
			         </c:choose>
		         </div>
		     </c:if>
	         <p class="text stuOpenText" style="<c:if test="${homeworkVO.stuOpenAt ne 'Y' }">display:none;</c:if>"><c:if test="${homeworkVO.stuOpenAt eq 'Y' }">평가결과가 학생에게 공개되었습니다. (<span class="stuOpenDateYear">${fn:substring(homeworkVO.stuOpenDate,0,4) }</span>-<span class="stuOpenDateMonth">${fn:substring(homeworkVO.stuOpenDate,5,7) }</span>-<span class="stuOpenDateDay">${fn:substring(homeworkVO.stuOpenDate,8,10) }</span> 부터)</c:if></p>
	       </div>
	       <div class="board-type-wrap">
	         <a href="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM }&boardType=list" class="btn-list <c:if test="${param.boardType eq 'list' }">on</c:if>" title="리스트형"></a>
	         <i class="division-line"></i>
	         <a href="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM }&boardType=photo" class="btn-gallery <c:if test="${param.boardType eq 'photo' }">on</c:if>" title="갤러리형"></a>
	       </div>
	     </div>
	     <div class="content-body">
	       <form name="frm" action="/lms/crcl/updateFdb.do">
	           <input type="hidden" name="forwardUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}&boardType=${param.boardType}" />
	           <input type="hidden" name="fdb" value="" />
	
		       <c:choose>
		           <c:when test="${param.boardType eq 'list'}">
		               <!-- 테이블영역-->
				       <table class="common-table-wrap ">
				         <colgroup>
				           <col style='width:5%'>
				           <c:if test="${homeworkVO.hwType eq 1 }">
				               <col style='width:10%'>
				               <col style='width:10%'>
				               <col style='width:9%'>
				               <col style='width:11%'>
				           </c:if>
				           <col style='width:7%'>
				           <col style='width:7%'>
				           <col style='width:7%'>
				           <col style='width:7%'>
				           <col>
				           <col style='width:7%'>
				           <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
				               <col style='width:12%'>
				           </c:if>
				         </colgroup>
				         <thead>
				           <tr class='bg-light-gray font-700'>
				             <th scope='col'>선택</th>
				             <c:if test="${homeworkVO.hwType eq 1 }">
				                 <th scope='col'>소속</th>
				                 <th scope='col'>이름</th>
				                 <th scope='col'>생년월일</th>
				                 <th scope='col'>학번</th>
				             </c:if>
				             <th scope='col'>반</th>
				             <th scope='col'>조</th>
				             <th scope='col'>제출여부</th>
				             <th scope='col'>평가여부</th>
				             <th scope='col'>피드백</th>
				             <th scope='col'>점수</th>
				             <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
				                 <th scope='col'>과정후기 선정</th>
				             </c:if>
				           </tr>
				         </thead>
				         <tbody>
				             <c:forEach var="result" items="${selectHomeworkSubjectList}" varStatus="status">
				                 <!-- 제출일자 게산을 위한 함수 -->
				                 <c:set var="closeDate" value="${fn:replace(homeworkVO.closeDate, '-', '')}${homeworkVO.closeTime }"/>
                                 <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                                 <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateFormat" />

		                         <fmt:formatDate value="${result.lastUpdusrPnttm }" pattern="yyyyMMddHHmm" var="lastUpdusrPnttmFormat" />

				                 <!-- 제출 여부 처리  -->
				                 <c:choose>
				                     <c:when test="${empty result.hwsId }">
				                         <c:set var="tdFontColor" value="font-red"/>
				                         <c:set var="submitAt" value="미제출"/>
				                     </c:when>
				                     <c:when test="${closeDateFormat < lastUpdusrPnttmFormat }">
		                                 <c:set var="tdFontColor" value="font-blue"/>
		                                 <c:set var="submitAt" value="마감후"/>
		                             </c:when>
		                             <c:otherwise>
		                                <c:set var="tdFontColor" value=""/>
		                                <c:set var="submitAt" value="제출"/>
		                             </c:otherwise>
				                 </c:choose>
				                 
				                 <c:choose>
                                     <c:when test="${empty result.hwsId }">
                                         <c:set var="viewUrl" value="#none"/>
                                     </c:when>
                                     <c:otherwise>
                                         <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
		                                 <c:param name="hwsId" value="${result.hwsId }"/>
		                                    <c:param name="viewFlag" value="homeworksubmit"/>
		                                    <c:param name="boardType" value="${param.boardType }"/>
		                                    <c:param name="homeworkTotalFlag" value="${param.homeworkTotalFlag }"/>
		                                </c:url>
                                     </c:otherwise>
                                 </c:choose>
				                 <tr class="">
				                     <td scope='row'><label class='checkbox selectedList'><input type='checkbox' name="arrHwsId" value="${result.hwsId }"><span class='custom-checked'></span></label></td>
				                     <c:if test="${homeworkVO.hwType eq 1 }">
				                         <td onclick="location.href='${viewUrl}'">${result.mngDeptNm }</td>
				                         <td onclick="location.href='${viewUrl}'">${result.userNm }</td>
				                         <td onclick="location.href='${viewUrl}'">${result.brthdy }</td>
				                         <td onclick="location.href='${viewUrl}'">${result.stNumber }</td>
				                     </c:if>
				                     <td onclick="location.href='${viewUrl}'">
				                         <c:if test="${!empty result.groupCnt }">
				                             ${result.classCnt }반
				                         </c:if>
				                     </td>
				                     <td onclick="location.href='${viewUrl}'">
				                         <c:if test="${!empty result.classCnt }">
				                             ${result.groupCnt }조
				                         </c:if>
				                     </td>
				                     <td class="${tdFontColor }" onclick="location.href='${viewUrl}'">
				                         ${submitAt }
		
				                         <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
				                             <br />
				                             <fmt:parseDate value="${fn:substring(homeworkVO.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
		                                     <fmt:parseDate value="${fn:substring(result.lastUpdusrPnttm,0,10)}" pattern="yyyy-MM-dd" var="lastUpdusrPnttmHyphen" />
		                                     <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />
				                             <fmt:parseNumber value="${lastUpdusrPnttmHyphen.time / (1000*60*60*24)}" integerOnly="true" var="lastUpdusrPnttmTime" />
				                             
				                             <c:if test="${closeDateFormat < lastUpdusrPnttmFormat }">
				                                <c:set var="dDay" value="${lastUpdusrPnttmTime - closeDateTime }"/>
				                             </c:if>
				                             
				                             <%-- 0일경우에는 분이 초과된거라서 1로 처리함. --%>
				                             <c:if test="${dDay eq 0 }">
				                                 <c:set var="dDay" value="1"/>
				                             </c:if>
		                                     
				                             (D+${dDay })
				                         </c:if>
				                     </td>
				                     <td onclick="location.href='${viewUrl}'">${result.fdbAt }</td>
				                     <td onclick="location.href='${viewUrl}'">${result.fdb }</td>
				                     <td onclick="location.href='${viewUrl}'">
				                         <c:choose>
		                                    <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
		                                        <c:choose>
		                                            <c:when test="${homeworkVO.stuOpenAt ne 'Y' and empty result.scr}"></c:when>
		                                            <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty result.scr}"><span class="${tdFontColor }">FAIL</span></c:when>
		                                            <c:when test="${result.scr eq '100'}"><span class="${tdFontColor }">PASS</span></c:when>
		                                            <c:otherwise><span class="${tdFontColor }">FAIL</span></c:otherwise>
		                                        </c:choose>
		                                    </c:when>
		                                    <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
		                                        <c:choose>
                                                    <c:when test="${homeworkVO.stuOpenAt eq 'Y' and empty result.scr}">0</c:when>
                                                    <c:otherwise>${result.scr }</c:otherwise>
                                                </c:choose>
		                                    </c:when>
		                                </c:choose>
				                     </td>
				                     <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">
					                     <td>
					                         <c:if test="${!empty result.hwsId and (managerAt eq 'Y'  or studyMngAt eq 'Y')}">
					                             <c:choose>
					                                 <c:when test="${result.commentPickAt eq 'Y' }">
					                                     <button class='btn-sm-full btn-outline-gray' type='button' onclick="location.href='/lms/crcl/selectHomeworkCommentPickAtArticle.do?hwId=${result.hwId }&hwsId=${result.hwsId }&crclId=${result.crclId }&menuId=MNU_0000000000000158&boardType=list'">후기보기</button>
			                                             <button class='btn-sm-full mt-5 btn-outline-gray updateCommentPickAt' data-commentpickat="N" data-hwsid="${result.hwsId }" type='button'>선정취소</button>
					                                 </c:when>
					                                 <c:otherwise>
					                                     <button class='btn-sm-full btn-outline updateCommentPickAt' data-commentpickat="Y" data-hwsid="${result.hwsId }" type='button'>선정하기</button>
					                                 </c:otherwise>
					                             </c:choose>
					                         </c:if>
					                     </td>
				                     </c:if>
				                 </tr>
				             </c:forEach>
				         </tbody>
				       </table>
		           </c:when>
		           <c:otherwise>
		               <p class="board-thumbnail-text">제출된 과제만 썸네일 뷰로 조회됩니다. (미제출 명단은 리스트 뷰로 전환해서 확인해주세요.)</p>
		
				       <ul class="flex-row">
				         <c:forEach var="result" items="${selectHomeworkSubjectList}" varStatus="status">
				             <c:if test="${!empty result.hwsId }">
				                 <c:choose>
                                     <c:when test="${empty result.hwsId }">
                                         <c:set var="viewUrl" value="#none"/>
                                     </c:when>
                                     <c:otherwise>
                                         <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
                                         <c:param name="hwsId" value="${result.hwsId }"/>
                                            <c:param name="viewFlag" value="homeworksubmit"/>
                                            <c:param name="boardType" value="${param.boardType }"/>
                                            <c:param name="homeworkTotalFlag" value="${param.homeworkTotalFlag }"/>
                                        </c:url>
                                     </c:otherwise>
                                 </c:choose>

						         <li class="flex-col-4" onclick="location.href='${viewUrl}'">
						           <div class="subject-feedback-card">
						             <div class="subject-feedback-header">
						               <label class="checkbox">
						                 <input type='checkbox' name="arrHwsId" value="${result.hwsId }">
						                 <span class="custom-checked"></span>
						               </label>
						                   <c:choose>
						                       <c:when test="${empty result.fdb }"><span class="subject-feedback-label">피드백 미등록</span></c:when>
						                       <c:otherwise><span class="subject-feedback-label complete">피드백 완료</span></c:otherwise>
						                   </c:choose>
						             </div>
						             <%-- 배경 이미지 --%>
						             <c:choose>
						                 <c:when test="${!empty result.groupStreFileNm }"><c:set var="backgroundImg" value="${HomeworkSubmitFileStoreWebPath }/SITE_000000000000001/HOMEWORKSUBMIT/${result.groupStreFileNm}"/></c:when>
						                 <c:otherwise><c:set var="backgroundImg" value="/template/lms/imgs/common/img_temporarily_05.jpg"/></c:otherwise>
						             </c:choose>
						             <%-- 학생 사진 이미지 --%>
                                     <c:choose>
                                         <c:when test="${!empty result.photoStreFileNm }"><c:set var="studentImg" value="${MembersFileStoreWebPath }${result.photoStreFileNm}"/></c:when>
                                         <c:otherwise><c:set var="studentImg" value="/template/lms/imgs/common/img_user_profile.jpg"/></c:otherwise>
                                     </c:choose>
						             <div class="subject-feedback-bg" style="background-image:url(${backgroundImg});">
						               <div class="subject-feedback-profile" style="background-image:url(${studentImg});">
						               </div>
						             </div>
						             <div class="subject-feedback-text">
						               <p class="sub-title">
						                   <c:if test="${homeworkVO.hwType eq '1' }">
						                       ${result.userNm }
						                   </c:if>
						                   ${result.classCnt }반 ${result.groupCnt }조
						               </p>
						               <p class="title ell">${result.nttSj }</p>
						               <c:if test="${!empty result.hwsId and (managerAt eq 'Y'  or studyMngAt eq 'Y')}">
	                                       <c:choose>
	                                           <c:when test="${result.commentPickAt eq 'Y' }">
	                                               <button class='btn-md btn-outline-gray updateCommentPickAt' data-commentpickat="N" data-hwsid="${result.hwsId }" type='button'>선정취소</button>
	                                           </c:when>
	                                           <c:otherwise>
	                                               <button class='btn-md btn-outline updateCommentPickAt' data-commentpickat="Y" data-hwsid="${result.hwsId }" type='button'>선정하기</button>
	                                           </c:otherwise>
	                                       </c:choose>
	                                   </c:if>
						             </div>
						           </div>
						         </li>
						     </c:if>
					     </c:forEach>
				       </ul>
		           </c:otherwise>
		       </c:choose>
		   </form>
	     </div>
     </c:otherwise>
   </c:choose>
   </article>
   <div class="page-btn-wrap mt-50">
     <c:choose>
         <c:when test="${empty searchVO.plId}"><c:set var="listUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM }"/></c:when>
         <c:otherwise><c:set var="listUrl" value="/lms/manage/homeworkList.do${_BASE_PARAM }"/></c:otherwise>     
     </c:choose>
     <a href="${listUrl }" class="btn-xl btn-outline-gray">목록으로</a>
   </div>
  </section>
</div>

</div>
</div>
</div>

<div id="feedback_write_modal" class="alert-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">피드백 남기기</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body left-align">
          <article class="modal-body-content">
            <div class="modal-text mb-10">
                선택된 학생 <div class="modal-subtext inline font-gray">총
                <span class="font-point spanCheckboxCnt"></span> 
                <c:choose>
                    <c:when test="${homeworkVO.hwType eq 1 }">명</c:when>
                    <c:otherwise>개조</c:otherwise>
                </c:choose>
                </div>
            </div>
            <div class="modal-scroll-wrap">
              <%-- TODO : ywkim 유저 조회하는거 ajax로 처리해야함 
              <table class="common-table-wrap font-700 left-align">
                <colgroup>
                  <col style="width: 50%;">
                  <col style="width: 50%;">
                </colgroup>
                <tbody>
                  <tr>
                    <td>
                      <i class="icon-user" style="background-image:url(../imgs/common/img_user_profile.jpg);"></i>홍길동 <span class="font-point">(1조)</span>
                    </td>
                    <td>
                      <i class="icon-user"></i>Saranzaya Pureviav
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>이길동
                    </td>
                    <td>
                      <i class="icon-user" style="background-image:url(../imgs/common/img_user_profile.jpg);"></i>홍길동 <span class="font-point">(2조)</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>홍길동 <span class="font-point">(2조)</span>
                    </td>
                    <td>
                      <i class="icon-user"></i>Saranzaya Pureviav <span class="font-point">(4조)</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>홍길동
                    </td>
                    <td>
                      <i class="icon-user"></i>이길동 <span class="font-point">(5조)</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user" style="background-image:url(../imgs/common/img_user_profile.jpg);"></i>홍길동 <span class="font-point">(1조)</span>
                    </td>
                    <td>
                      <i class="icon-user"></i>Saranzaya Pureviav
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>이길동
                    </td>
                    <td>
                      <i class="icon-user" style="background-image:url(../imgs/common/img_user_profile.jpg);"></i>홍길동 <span class="font-point">(2조)</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>홍길동 <span class="font-point">(2조)</span>
                    </td>
                    <td>
                      <i class="icon-user"></i>Saranzaya Pureviav <span class="font-point">(4조)</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <i class="icon-user"></i>이길동 <span class="font-point">(5조)</span>
                    </td>
                  </tr>
                </tbody>
              </table> --%>
            </div>
          </article>
          <article class="modal-body-content">
            <div class="modal-text mb-10">피드백</div>
            <textarea class="content-textarea onlyText areaFdb"></textarea>
          </article>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>