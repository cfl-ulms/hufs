<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.plId}"><c:param name="plId" value="${param.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){

});
</script>

         <div class="page-content-header">
         	<c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
			<!-- 
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="5"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
        </article>
		<article class="content-wrap">
              <!-- 사용자 정보 -->
              <div class="card-user-wrap">
                <c:forEach var="result" items="${facPlList}" varStatus="status">
	                <div class="user-icon">
	                	<c:choose>
	                		<c:when test="${not empty result.photoStreFileNm}">
	                			<img src="${MembersFileStoreWebPath}/${result.photoStreFileNm}" alt="사람 아이콘">
	                		</c:when>
	                		<c:otherwise>
	                			<img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘">
	                		</c:otherwise>
	                	</c:choose>
	                </div>
	                <div class="user-info">
	                  <p class="title">담당교수 
	                  	<b>
							<c:out value="${result.userNm}"/>
							<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
							(${result.mngDeptNm})
							<c:set var="userMail" value="${result.facId }"/>
						</b>
					  </p>
	                  <p class="sub-title">문의: ${userMail }</p>
	                </div>
                </c:forEach>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 수업내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수업내용</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>수업일</td>
                      <td>
                      	<c:out value="${scheduleMngVO.startDt}"/>
			  			&nbsp;
			  			<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
			  			~
			  			<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
                      </td>
                      <td scope='row' class='font-700'>시수</td>
                      <td><c:out value="${scheduleMngVO.sisu}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>장소</td>
                      <td>
                      	<c:out value="${scheduleMngVO.campusNm}"/>
                      	<c:out value="${empty scheduleMngVO.campusNm ? curriculumVO.campusNm : scheduleMngVO.campusNm}"/>
                      </td>
                      <td scope='row' class='font-700'>강의실</td>
                      <td><c:out value="${empty scheduleMngVO.placeDetail ? curriculumVO.campusPlace : scheduleMngVO.placeDetail}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>수업영역</td>
                      <td colspan='3'>
                      	<c:set var="lang" value="${empty scheduleMngVO.crclLang ? curriculumVO.crclLang : scheduleMngVO.crclLang}"/>
			  			<c:out value="${empty scheduleMngVO.crclLangNm ? curriculumVO.crclLangNm : scheduleMngVO.crclLangNm}"/>
			  			<c:if test="${not empty scheduleMngVO.spLevel}">
			  				> <c:out value="${scheduleMngVO.spLevelNm}"/>
			  			</c:if>
			  			<c:if test="${not empty scheduleMngVO.spLevelNum}">
			  				> <c:out value="${scheduleMngVO.spLevelNumNm}"/>
			  			</c:if>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>수업방법</td>
                      <td colspan='3'>
                      	<c:choose>
			  				<c:when test="${scheduleMngVO.spType ne 'N' }">
			  					강의식
			  				</c:when>
			  				<c:otherwise>
			  					비강의식
			  				</c:otherwise>
			  			</c:choose>
			  			- <c:out value="${scheduleMngVO.courseNm}"/>
                      </td>
                    </tr>
                    <tr class="evtTr" <c:if test="${scheduleMngVO.courseId ne 'CTG_0000000000000054'}">style="display:none;"</c:if>>
                      <td scope='row' class='font-700'>평가</td>
                      <td colspan='3' class="box_evt">
                      	<c:forEach var="result" items="${evtResultList}">
			  				<c:out value="${result.evtNm}"/>
						</c:forEach>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <!-- <div class="class-info-box mt-40">본 수업은 온라인 강의로 아래 수업내용에 첨부된 수업자료를 모두 확인하여야 출석으로 인정됩니다.</div> -->
              </div>
            </article>
            <hr class="line-hr mb-35">
            <article class="content-wrap">
              <!-- 수업내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수업내용</div>
                </div>
              </div>
              <div class="content-body">
                 <c:out value="${scheduleMngVO.spCn}" escapeXml="false"/>
              </div>
            </article>
            
            <c:if test="${not empty scheduleMngVO.atchFileId or fn:length(fileList) > 0}">
	            <article class="content-wrap">
	              <!-- 수업자료 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">수업자료</div>
	                </div>
	              </div>
	              <div class="content-body">
	              	<div class="file-attachment-wrap">
	                  <div class="file-attachment-view">
	                    <div class="inner-area">
			              	<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
								<c:param name="param_atchFileId" value="${scheduleMngVO.atchFileId}" />									
								<c:param name="imagePath" value="${_IMG }"/>
								<c:param name="addYn" value="Y"/>
							</c:import>
	                    	<c:forEach var="result" items="${fileList}" varStatus="status">
								 <c:url var="downLoad" value="/cmm/fms/FileDown.do">
									<c:param name="atchFileId" value="${result.atchFileId}"/>
									<c:param name="fileSn" value="${result.fileSn}"/>
								</c:url>
								<a href="${downLoad}" class="attachment icon-file font-gray">
		                       	 	<span class="text">${result.orignlFileNm}</span>
		                      	</a>
							</c:forEach>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </article>
            </c:if>
            
            <hr class="line-hr mb-35">
            <article class="content-wrap">
              <!-- 학습성과 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">학습성과</div>
                </div>
              </div>
              <div class="content-body">
                <div class="result-text">
                  <p><c:out value="${scheduleMngVO.spGoal}"/></p>
                </div>
              </div>
            </article>
            <hr class="line-hr">
            <div class="page-btn-wrap mt-50">
            	<c:choose>
            		<c:when test="${searchVO.menuId eq 'MNU_0000000000000066' or searchVO.menuId eq 'MNU_0000000000000129'}">
            			<c:url var="listUrl" value="/sch/selectTodayCrclList.do">
							<c:param name="menuId" value="MNU_0000000000000086"/>
						</c:url>
            		</c:when>
            		<c:when test="${searchVO.menuId eq 'MNU_0000000000000068'}">
            			<c:url var="listUrl" value="/lms/crm/myCurriculumView.do${_BASE_PARAM}"/>
            		</c:when>
            		<c:when test="${param.tabType eq 'T'}">
            			<c:url var="listUrl" value="/sch/selectTodayCrclList.do">
							<c:param name="menuId" value="MNU_0000000000000086"/>
						</c:url>
            		</c:when>
            		<c:otherwise>
            			<c:url var="listUrl" value="/lms/manage/studyPlan.do${_BASE_PARAM2}">
						</c:url>
            		</c:otherwise>
            	</c:choose>
            	<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
              <c:if test="${studyMngAt eq 'Y'}">
              	<a href="/lms/manage/studyPlanReg.do${_BASE_PARAM}" class="btn-xl btn-point">수정</a>
              </c:if>
            </div>
          </section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>