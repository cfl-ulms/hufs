<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/cla"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:param name="menuId" value="${param.menuId}" />
	<c:param name="bbsId" value="${searchVO.bbsId }" />
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>
<style>
.backGray{
	background : #ccc;
}
</style>
<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script>
$(document).ready(function(){
   var targetDiv = document.getElementById('chart_pie')
   var myChart = echarts.init(targetDiv);
   var dataMapArr = [];
   var dataArr = [];

   <c:forEach var="result" items="${pieList}" varStatus="status">
      var tempObject = new Object();
      if('${result.cnt}' != '0'){
         tempObject.value = '${result.cnt}';
         tempObject.name = '${result.ctgryNm}';
         dataMapArr.push(tempObject);
         dataArr.push(tempObject.name);
      }
   </c:forEach>

   var option = {
            animation: false,
            legend: {
             bottom: 10,
              data: dataArr,
              icon: 'rect',
              itemGap: 20,
              textStyle: {
                fontSize: 13,
                verticalAlign: 'middle',
              },
            },
            series: [
              {
                   type: 'pie',
                   radius: ['50%', '70%'],
                   label:false,
                   center: ['50%', '50%'],
                   hoverAnimation: false,
                   animation: false,
                   data: dataMapArr
              }
            ]
          };

   myChart.setOption(option);


   dataMapArr = [];
   <c:forEach var="result" items="${barList}" varStatus="status">
   var tempBarObject = new Object();
      tempBarObject.name = '${result.ctgryNm}';
      tempBarObject.type = 'bar';
      tempBarObject.stack = 'stack';
      var tempCnt = '${result.ctgCnt}';
      tempBarObject.data = tempCnt.split(",");
      dataMapArr.push(tempBarObject);
   </c:forEach>

   dataArr = [];
   <c:forEach var="result" items="${barMemberList}" varStatus="status">
      dataArr.push('${result.userNm}');
   </c:forEach>

   fnBarChart(dataMapArr,dataArr );

   $(".memtype").on("click", function(){
	   $(".memtype").removeClass("on");
	   $(this).addClass("on");
	   var searchMemType = $(this).data("memtype");
	   var tempCntType = '${masterVo.sysTyCode}'
	   var tempCrclId = '${param.crclId}';
	   var tempBbsId = '${param.bbsId}';

	   $.ajax({
			url : "/mng/lms/manage/lmsBbsStatisticsBar.json",
			dataType : "json",
			type : "POST",
			data : {"searchMemType" : searchMemType, "crclId" : tempCrclId, "bbsId" : tempBbsId, "cntType" : tempCntType.toLowerCase()},
			success : function(data){
				var memberList = data.barMemberList;
				var valueList = data.barList;
				var dataMapArr = [];
				var dataArr = [];

				if(memberList.length != 0){
					for(var i=0; i<memberList.length; i++){
						 dataArr.push(memberList[i].userNm);
					}

					for(var i=0; i<valueList.length; i++){
						var tempBarObject = new Object();
					      tempBarObject.name = valueList[i].ctgryNm;
					      tempBarObject.type = 'bar';
					      tempBarObject.stack = 'stack';
					      var tempCnt = valueList[i].ctgCnt;
					      tempBarObject.data = tempCnt.split(",");
					      dataMapArr.push(tempBarObject);
					}

				}

				fnBarChart(dataMapArr, dataArr);
				return false;
			}, error : function(){
				alert("error");
			}
		});
   });

});

function fnBarChart(valueArr, memberArr){
   var targetDiv = document.getElementById('chart_bar')
   var barChart = echarts.init(targetDiv);

   if(memberArr.length != 0){
	    option = {

	          grid: {
	              left: '3%',
	              right: '4%',
	              bottom: '3%',
	              containLabel: true
	          },
	          xAxis: {
	              	type: 'value',
	              	minInterval: 5
	          },
	          yAxis: {
	              type: 'category',
	              data: memberArr
	          },
	          series: valueArr
	      };

	   barChart.setOption(option);
   }else{
	   barChart.clear();
   }
}

function fnMemBoardList(userId){
	var tempBbsId = '${searchVO.bbsId }';
	var tempCrclId = '${searchVO.crclId }';

	 $.ajax({
			url : "/lms/cla/selectLmsBbsList.json",
			dataType : "json",
			type : "POST",
			data : {"frstRegisterId" : userId, "crclId" : tempCrclId, "bbsId" : tempBbsId},
			success : function(data){
				var masterVo = data.masterVo;
				var boardList = data.resultList;
				var attendCollect = data.attendCollect;
				var bbsTitle = masterVo.sysTyCode == 'CLASS' ? '(반 별)' :  masterVo.sysTyCode == 'GROUP' ? '(조 별)' : '' ;


				$("#board_list_modal").find(".modal-title").text(bbsTitle + masterVo.bbsNm + "게시판 통계");
				$("#board_list_modal").find(".score-label").text(boardList.length < attendCollect ? 'PASS' : 'FAIL');
				$("#board_list_modal").find(".modal-table-title").find("p").text(boardList[0].department + boardList[0].userNm);
				$("#board_list_modal").find("#totalDl .title-desc").text("총 "+boardList.length+"건");
				if(attendCollect != undefined && attendCollect != null){
					$("#board_list_modal").find("#scoreDl .title-desc").text(boardList.length+"건 이상");
				}else{
					$("#board_list_modal").find("#scoreDl").hide();
				}

				if(boardList != undefined && boardList != null){
					var html;
					for(var i=0; i < boardList.length; i++){
						html +=	"<tr>";
						html +=		"<td>"+(i+1)+"</td>";
						html +=		"<td>"+boardList[i].ctgryNm+"</td>";
						html +=		"<td>"+boardList[i].nttSj+"</td>";

						if(boardList[i].atchFileId != null && boardList[i].atchFileId != undefined){
							html += "<td><a href='#'><i class='icon-download'>다운로드</i></a></td>";
						}else{
							html += "<td></td>";
						}

						var tempDate = boardList[i].frstRegisterPnttm
						tempDate = tempDate.substr(0,10);
						html +=		"<td>"+tempDate+"</td>";
						html +=	"<tr>";

					}

					$(".table-type-board").find("tbody").html(html);
					$("#board_list_modal").show();
				}


			}, error : function(){
				console.log("error");
			}
		});

}

</script>
           <div class="page-content-header">
            <c:choose>
          		<c:when test="${param.menuId eq 'MNU_0000000000000094' }">
          			<c:import url="/lms/crclHeader.do" charEncoding="utf-8"/>
          		</c:when>
          		<c:otherwise>
          			<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
          		</c:otherwise>
          	</c:choose>
          </div>
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="7"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="menuId" value="${searchVO.menuId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>

            <article class="content-wrap">
              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
	                  <c:forEach var="result" items="${masterList}">
						<c:url var="masterUrl" value="/lms/cla/curriculumBoardList.do">
							<c:param name="crclId" value="${searchVO.crclId}"/>
							<c:param name="bbsId" value="${result.bbsId}" />
							<c:param name="menuId" value="${param.menuId}" />
						</c:url>
						<li <c:if test="${result.bbsId eq masterVo.bbsId}">class="active"</c:if>>
							<a href="${masterUrl}" class="link">
								<c:choose>
									<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
									<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
								</c:choose>
								<c:out value="${result.bbsNm}"/>
							</a>
						</li>
					</c:forEach>
                </ul>
                </div>
                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
            </article>


            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">${masterVo.sysTyCode eq 'CLASS' ? '(반 별)' : masterVo.sysTyCode eq 'GROUP' ? '(조 별)' : ''} ${masterVo.bbsNm} (총 ${totalBbsCnt}개의 게시글 등록 )</div>
                </div>
              </div>
              <div class="content-body">
                <div class="graph-layout-wrap">
                  <div class="graph-layout-box board-graph pie">
                    <p class="title">게시글 유형별 현황</p>
                    <div class="boardTypeGraph" id="chart_pie" style="height:200px;"></div>
                  </div>
                  <div class="graph-layout-box board-graph">
                    <div class="graph-text-wrap">
                      <p class="title">게시글 작성자별 현황</p>
                      <div class="class-tab-wrap">
                        <a href="#none" class="title memtype on" data-memtype="">전체</a>
                        <a href="#none" class="title memtype" data-memtype="T">교원</a>
                        <a href="#none" class="title memtype" data-memtype="S">학생</a>
                        <a href="#none" class="title memtype" data-memtype="A">관리자</a>
                      </div>
                    </div>
                    <div class="boardTypeGraph" id="chart_bar" style="height:200px;" ></div>
                  </div>
                </div>
              </div>
            </article>
            <article class="content-wrap">
              <div class="content-header">
                <div class="class-tab-wrap">

                <c:url var="searchUrl1" value="/lms/cla/curriculumBoardStatistics.do">
					<c:param name="crclId" value="${param.crclId}"/>
					<c:param name="bbsId" value="${param.bbsId}" />
					<c:param name="searchType" value="student" />
					<c:param name="menuId" value="${param.menuId}" />
				</c:url>

				<c:url var="searchUrl2" value="/lms/cla/curriculumBoardStatistics.do">
					<c:param name="crclId" value="${param.crclId}"/>
					<c:param name="bbsId" value="${param.bbsId}" />
					<c:param name="searchType" value="teacher" />
					<c:param name="menuId" value="${param.menuId}" />
				</c:url>
                  <a href="${searchUrl1 }" class="title ${searchVO.searchType eq 'student' ? 'on' : ''  }">학생</a>
                  <a href="${searchUrl2 }" class="title ${searchVO.searchType eq 'teacher' ? 'on' : ''  }">교원</a>
                </div>
              </div>
              <div class="box-wrap mb-40">
              <form id="searchForm" method="post" action="/lms/cla/curriculumBoardStatistics.do">
				<input type="hidden" name="crclId" value="${param.crclId }"/>
			  	<input type="hidden" name="bbsId" value="${param.bbsId }"/>
			  	<input type="hidden" name="searchType" value="${searchVO.searchType }"/>
			  	<input type="hidden" name="menuId" value="${param.menuId }"/>

                <h3 class="title-subhead">${searchVO.searchType eq 'student' ? '학생 검색' : '교원 검색'}</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <select name="sortType" id="sortType" class="select2" data-select="style3" data-placeholder="검색 유형">
                       	<option value="sum" ${searchVO.sortType eq 'sum' ? 'selected' : '' }>합계순</option>
		               	<option value="name" ${searchVO.sortType eq 'name' ? 'selected' : '' }>이름순</option>
		               	<c:if test="${searchVO.searchType eq 'student'}">
			               <option value="birth" ${searchVO.sortType eq 'birth' ? 'selected' : '' }>생년월일순</option>
			               <option value="class" ${searchVO.sortType eq 'class' ? 'selected' : '' }>소속순</option>
		               	</c:if>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-7">
                    <div class="ell">
                      <input type="text" id="searchWrd" name="searchWrd" value="${searchVO.searchWrd}" placeholder="${searchVO.searchType eq 'student' ? '학생 이름' : '교원 이름' } 을 검색해보세요 ">
                    </div>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20" onclick="searchSub();">검색</button>
                </form>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">

              <c:choose>
      			<c:when test="${searchVO.searchType eq 'teacher'}">
      			 <colgroup>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <c:forEach items="${pieList}" var="result">
                       <col style='width:7%'>
                   </c:forEach>
                  <col style='width:7%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col' rowspan='2'>No</th>
                    <th scope='col' rowspan='2'>교원</th>
                    <th scope='col' colspan='${fn:length(pieList)}'>글 구분</th>
                    <th scope='col' rowspan='2'>합계</th>
                  </tr>
                  <tr class='bg-light-gray font-700'>
                  	<c:forEach items="${pieList}" var="result">
                    	<th scope='col'>${result.ctgryNm }</th>
                    </c:forEach>
                  </tr>
                </thead>
                <tbody>
                <c:if test="${not empty resultList }">
               		<c:forEach var="result" items="${resultList}" varStatus="status">
	                  <tr class="">
	                    <td scope='row'>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
	                    <td>${result.userNm }</td>
	                    <c:set var="groupCnt" value="${fn:split(result.groupCnt,',') }"/>
	               		<c:forEach items="${groupCnt }" var="group">
	               			<td><a href='#' class='underline'>${group }</a></td>
	               		</c:forEach>
	               		<td>${result.totCnt }</td>
	                  </tr>
                  	</c:forEach>
                 </c:if>
                </tbody>
      			</c:when>
      			<c:otherwise>
      				 <colgroup>
                  <col style='width:7%'>
                  <col>
                  <col>
                  <col>
                  <col>
                  <col style='width:7%'>
                  <c:forEach items="${pieList}" var="result">
                       <col>
                   </c:forEach>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <c:if test="${not empty attendCollect }">
                  	<col style='width:10%'>
	              </c:if>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col' rowspan='2'>No</th>
                    <th scope='col' rowspan='2'>소속</th>
                    <th scope='col' rowspan='2'>이름</th>
                    <th scope='col' rowspan='2'>생년월일</th>
                    <th scope='col' rowspan='2'>학번</th>
                    <th scope='col' rowspan='2'>학년</th>
                    <th scope='col' colspan='${fn:length(pieList)}'>글 구분</th>
                    <th scope='col' rowspan='2'>합계</th>
                    <c:if test="${not empty attendCollect }">
	                     <th scope='col' rowspan='2'>평가기준<br>P/F<br>(5건 이상)</th>
                    </c:if>
                  </tr>
                  <tr class='bg-light-gray font-700'>
                  	<c:forEach items="${pieList}" var="result">
                    	<th scope='col'>${result.ctgryNm }</th>
                    </c:forEach>
                  </tr>
                </thead>
                <tbody>
                <c:if test="${not empty resultList }">
               		<c:forEach var="result" items="${resultList}" varStatus="status">
	                  <tr class="">
	                    <td scope='row'>${paginationInfo.totalRecordCount - ((paginationInfo.currentPageNo-1) * paginationInfo.recordCountPerPage) - (status.count - 1)}</td>
	                    <td>${result.stClass }</td>
	                    <td>${result.userNm }</td>
	               		<td>${result.birth }</td>
	               		<td>${result.stNumber }</td>
	                    <td>${result.stGrade }</td>
	                    <c:set var="groupCnt" value="${fn:split(result.groupCnt,',') }"/>
	               		<c:forEach items="${groupCnt }" var="group">
	               			<td><a href='#' class='underline'>${group }</a></td>
	               		</c:forEach>

	                    <td>
	                    <c:choose>
	                    	<c:when test="${result.totCnt ne '0'}">
             		              <a href="#none" onclick="javascript:fnMemBoardList('${result.userId }');" class='underline btnModalOpen font-point font-700' data-modal-type='board_listfile'>${result.totCnt }</a>
	                    	</c:when>
	                    	<c:otherwise>
 	             		             0
	                    	</c:otherwise>
	                    </c:choose>
	                    </td>
	                    <c:if test="${not empty attendCollect}">
	               			<td>
	               				<c:choose>
	               					<c:when test="${result.totCnt ge attendCollect}">
	               						Pass
	               					</c:when>
	               					<c:otherwise>
	               						<span class='font-red'>Fail</span>
	               					</c:otherwise>
	               				</c:choose>
	               			</td>
	               		</c:if>
	                  </tr>
                  	</c:forEach>
                 </c:if>
                </tbody>

      			</c:otherwise>
      			</c:choose>
              </table>


              <div class="pagination center-align mt-60">
                <div class="pagination-inner-wrap overflow-hidden inline-block">
                  	<c:url var="startUrl" value="/lms/cla/curriculumBoardStatistics.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>


                  	<c:url var="prevUrl" value="/lms/cla/curriculumBoardStatistics.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>


                  	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/cla/curriculumBoardStatistics.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>

                  	<c:url var="nextUrl" value="/lms/cla/curriculumBoardStatistics.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>

	               	<c:url var="endUrl" value="/lms/cla/curriculumBoardStatistics.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
                </div>
              </div>
            </article>
          </section>



   <div id="board_list_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <div class="modal-text">
            <div class="modal-table-title">
              <div class="score-label"></div>
              	<p></p>
              <div>
                <dl class="modal-subject-wrap">
                  <dt class="title-text">게시물</dt>
                  <dd class="title-desc"></dd>
                </dl>
                <dl class="modal-subject-wrap">
                  <dt class="title-text"></dt>
                  <dd class="title-desc"></dd>
                </dl>
              </div>
            </div>
          </div>
          <table class="common-table-wrap table-type-board">
            <colgroup>
              <col width="10%">
              <col width="10%">
              <col width="">
              <col width="10%">
              <col width="17%">
            </colgroup>
            <thead>
              <tr class="bg-gray">
                <th class="font-700">No</th>
                <th class="font-700">구분</th>
                <th class="font-700">제목</th>
                <th class="font-700">파일</th>
                <th class="font-700">등록일</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
        </div>
      </div>
    </div>
  </div>