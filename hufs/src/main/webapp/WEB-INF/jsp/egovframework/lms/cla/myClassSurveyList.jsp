<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ page import="egovframework.com.cmm.service.Globals"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId }"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:param name="plId" value="${param.plId }"></c:param>
	<c:if test="${not empty param.menu}"><c:param name="menu" value="${param.menu}" /></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){

});

function dateChk(endDate, viewUrl, limitDate){
	var curDate = "${curDate}";
	endDate = endDate.replace(/-/gi,"");
	curDate = curDate.replace(/-/gi,"");

	if(curDate < endDate){
		alert("설문 참여일이 아닙니다.");
		return false;
	}else if(limitDate != '' && curDate >= limitDate){
		alert("설문에 참여할 수 없습니다.");
		return false;
	}else{
		location.href = viewUrl;
	}

}
</script>
         <div class="page-content-header">
            <c:choose>
          		<%-- <c:when test="${param.menuId eq 'MNU_0000000000000163' }"> --%>
          		<c:when test="${empty param.plId}">
          			<c:import url="/lms/crclHeader.do" charEncoding="utf-8"/>
          		</c:when>
          		<c:otherwise>
          			<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
          		</c:otherwise>
          	</c:choose>
			<!--
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:choose>
                    <c:when test="${!empty param.plId }">
	                    <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
	                        <c:param name="step" value="10"/>
							<c:param name="crclId" value="${curriculumVO.crclId}"/>
							<c:param name="menuId" value="${searchVO.menuId}"/>
							<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
							<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	                    </c:import>
                    </c:when>
                    <c:otherwise>
                        <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
		                    <c:param name="menu" value="${param.menu }"/>
		                    <c:param name="subtabstep" value="${param.subtabstep }"/>
		                    <c:param name="menuId" value="${param.menuId }"/>
		                    <c:param name="crclId" value="${param.crclId}"/>
		                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt }"/>
		                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		                </c:import>
                    </c:otherwise>
                </c:choose>
        </article>
        <c:if test="${not empty resultSurveyList }">
	        <article class="content-wrap">
	        	<c:forEach items="${resultSurveyList}" var="result">
	        		<c:if test="${(result.schdulClCode eq 'TYPE_3' and USER_INFO.userSeCode > 7) or result.schdulClCode ne 'TYPE_3'}">
		        		<c:choose>
		        			<c:when test="${result.schdulClCode eq 'TYPE_1'}">
		        				<c:set var="tempText" value="과정"/>
		        			</c:when>
		        			<c:when test="${result.schdulClCode eq 'TYPE_2'}">
		        				<c:set var="tempText" value="수업"/>
		        			</c:when>
		        			<c:when test="${result.schdulClCode eq 'TYPE_3'}">
		        				<c:set var="tempText" value="교원대상 과정"/>
		        			</c:when>
		        			<c:otherwise>
		        				<c:set var="tempText" value=""/>
		        			</c:otherwise>
		        		</c:choose>
		        		<div class="survey-card-wrap">
		        		<c:url var="viewUrl" value="/lms/cla/surveyView.do${_BASE_PARAM }">
							<c:param name="schdulId" value="${result.schdulId}"/>
							<c:param name="schdulClCode" value="${result.schdulClCode}"/>
						</c:url>
		                <a href="#none" onclick="dateChk('${result.endDate}0000', '${viewUrl}', '${result.limitDate }');">
		                  <p class="title">${tempText} 만족도 설문</p>
		                  <p class="sub-title">
			                    	${tempText }이 종료된 후 (${result.endDate } 부터) 설문에 참여하실 수 있습니다.<br>
						            ${tempText } 만족도 설문을 참여하지 않을 경우 과정종료 후 성적조회 등이 원활하지 않을 수 있습니다.<br>
		                    <b class="font-point">꼭 설문에 참여해 주세요.</b>
		                  </p>
		                </a>
		              </div>
	              </c:if>
	        	</c:forEach>
	       	</article>
        </c:if>
        <c:if test="${param.menuId eq 'MNU_0000000000000163' }">
           <div class="page-btn-wrap mt-50">
             <a href="/lms/cla/mySurveyList.do${_BASE_PARAM }" class="btn-xl btn-outline-gray">목록으로</a>
           </div>
         </c:if>
          </section>
      </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>