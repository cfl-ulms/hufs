<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<% org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response)%>"/>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}" />
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchProjectAt}"><c:param name="searchProjectAt" value="${searchVO.searchProjectAt}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchHostCode" value="${searchVO.searchHostCode}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCode}"><c:param name="searchProcessSttusCode" value="${searchVO.searchProcessSttusCode}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}"/></c:if>
	<c:if test="${not empty searchVO.searchMyCulAt}"><c:param name="searchMyCulAt" value="${searchVO.searchMyCulAt}"/></c:if>
</c:url>

<script>
//달력
$(function() {
  $("#searchStartDate, #searchEndDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${searchVO.searchSysCode2}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${searchVO.searchSysCode3}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
		$("#ctgryId3").change();
	});
	
	$("#ctgryId3").change(function(){
		var sysCode3 = $(this).val(),
			optionHtml = "<option value=''>기본과정명</option>";
		
		$.ajax({
			url : "/lms/crclb/CurriculumbaseList.json"
			, type : "post"
			, dataType : "json"
			, data : {searchSysCode3 : sysCode3}
			, success : function(data){
				var searchcrclbId = "${searchVO.searchcrclbId}";
				if(data.successYn == "Y"){
					$.each(data.items, function(i){
						if(data.items[i].crclbId == searchcrclbId){
							optionHtml += "<option value='"+data.items[i].crclbId+"' selected='selected'>"+data.items[i].crclbNm+"</option>"
						}else{
							optionHtml += "<option value='"+data.items[i].crclbId+"'>"+data.items[i].crclbNm+"</option>"
						}
						
					});
					
					$("#searchcrclbId").html(optionHtml);
				}
			}, error : function(){
				alert("error");
			}
		});
		
	});
	
	$("#ctgryId1").change();
});
</script>
<section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">교육과정개설신청 검색</h3>
                <form name="frm" method="post" action="<c:url value="/lms/crcl/CurriculumList.do"/>">
                <input type="hidden" name="menuId" value="${searchVO.menuId}"/>
                <input type="hidden" name="mngAt" value="Y"/>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      	<select id="searchCrclYear" name="searchCrclYear" class="select2" data-select="style3" data-placeholder="년도">
                      		<option value="">선택</option>
							<c:forEach var="result" items="${yearList}" varStatus="status">
				  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
				  			</c:forEach>
						</select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      	<select id="searchCrclTerm" name="searchCrclTerm" class="select2" data-select="style3" data-placeholder="학기">
							<option value="">학기 전체</option>
							<c:forEach var="result" items="${crclTermList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
						</select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="desc">                  
                      <input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="ell date datepicker type2" placeholder="시작일" readonly="readonly"/> 
					  <i>~</i>
					  <input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="ell date datepicker type2" placeholder="종료일" readonly="readonly"/>
                    </div>

                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchAprvalAt" class="select2" data-select="style3" data-placeholder="신청결과 전체">
                        <option value=""></option>
                        <option value="Y" <c:if test="${searchVO.searchAprvalAt eq 'Y'}">selected="selected"</c:if>>승인</option>
                        <option value="N" <c:if test="${searchVO.searchAprvalAt eq 'N'}">selected="selected"</c:if>>반려</option>
                        <option value="R" <c:if test="${searchVO.searchAprvalAt eq 'R'}">selected="selected"</c:if>>대기</option>
                        <option value="D" <c:if test="${searchVO.searchAprvalAt eq 'D'}">selected="selected"</c:if>>승인취소</option>
                        
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" placeholder="책임교수 명">
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="ell">
                      <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" placeholder="과정명">
                    </div>
                  </div>
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      	<select id="targetType" name="searchTargetType" class="select2" data-select="style3" data-placeholder="대상 전체">
							<option value="">대상 선택</option>
							<option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
							<option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
						</select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3">
                    <div class="ell">                
                      	<select id="searchProcessSttusCode" name="searchProcessSttusCode" class="select2" data-select="style3" data-placeholder="과정유형 전체">
							<option value="">과정상태 전체</option>
							<c:forEach var="result" items="${statusComCode}" varStatus="status">
								<option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCode}">selected="selected"</c:if>>${result.codeNm}</option>
							</c:forEach>
						</select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 flex align-items-center">
                    <label class="checkbox">
                    	<input type="checkbox" name="searchProjectAt" value="Y" <c:if test="${searchVO.searchProjectAt eq 'Y'}">checked="checked"</c:if>/>
                      <span class="custom-checked"></span>
                      <span class="text">프로젝트 과정</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchTotalTimeAt" value="Y" <c:if test="${searchVO.searchTotalTimeAt eq 'Y'}">checked="checked"</c:if>/>
                      <span class="custom-checked"></span>
                      <span class="text">총 시수  과정</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchMyCulAt" value="Y" <c:if test="${searchVO.searchMyCulAt eq 'Y'}">checked="checked"</c:if>/>
                      <span class="custom-checked"></span>
                      <span class="text">내가 신청한 과정</span>
                    </label>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20">검색</button>

                <button class="btn-sm font-400 btn-outline mt-20">초기화</button>
                </form>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:12%'>
                  <col>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col style='width:9%'>
                  <col style='width:7%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>년도</th>
                    <th scope='col'>학기</th>
                    <th scope='col'>교육기간</th>
                    <th scope='col'>과정명</th>
                    <th scope='col' colspan='2'>대상</th>
                    <th scope='col' class='keep-all'>프로젝트 과정</th>
                    <th scope='col' class='keep-all'>총 시수 과정</th>
                    <th scope='col'>책임교수</th>
                    <th scope='col' class='keep-all'>신청 결과</th>
                  </tr>
                </thead>
                <tbody>
                	<c:forEach var="result" items="${resultList}" varStatus="status">
                		<c:url var="viewUrl" value="/lms/crcl/selectCurriculum.do${_BASE_PARAM}">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>
                		<tr onclick="location.href='${viewUrl}'" class="cursor-pointer">
		                    <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
		                    <td><c:out value="${result.crclYear}"/></td>
		                    <td><c:out value="${result.crclTermNm}"/></td>
		                    <td class='keep-all'><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
		                    <td class='left-align'><c:out value="${result.crclNm}"/></td>
		                    <td>
		                    	<c:choose>
			    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
			    					<c:otherwise>일반</c:otherwise>
			    				</c:choose>
		                    </td>
		                    <td><c:out value="${result.controlNm}"/></td>
		                    <td>
		                    	<c:choose>
		                    		<c:when test="${result.projectAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
		                    </td>
		                    <td>
								<c:choose>
		                    		<c:when test="${result.totalTimeAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
							</td>
		                    <td><c:out value="${result.userNm}"/></td>
		                    	<c:choose>
			    					<c:when test="${result.aprvalAt eq 'Y'}">
			    						<td class='font-point font-700'>승인<br/>
			    						<%-- (<fmt:formatDate value="${result.aprvalPnttm}"  pattern="yyyy-MM-dd"/>) --%>
			    					</c:when>
			    					<c:when test="${result.aprvalAt eq 'N'}">
			    						<td class='font-red font-700'>반려<br/>
			    						<%-- (<fmt:formatDate value="${result.aprvalPnttm}"  pattern="yyyy-MM-dd"/>) --%>
			    					</c:when>
			    					<c:otherwise><td class='font-gray font-700'>대기</c:otherwise>
			    				</c:choose>
		                    </td>
		                  </tr>
                	</c:forEach>
                	<c:if test="${fn:length(resultList) == 0}">
                		<tr><td colspan="11">검색결과가 없습니다.</td></tr>
                	</c:if>
                </tbody>
              </table>
              
              <c:if test="${USER_INFO.positionCode eq '10'}">
	              <div class="btn-group-wrap mt-20">
	                <div class="right-area">
	                  <a href="/lms/crcl/addCurriculumView.do?menuId=MNU_0000000000000059" class="btn-point btn-md">개설신청 등록</a>
	                </div>
	              </div>
              </c:if>
              
              <div class="pagination center-align mt-10">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end" data-url="${endUrl}"></button>
				</div>
			</div>
            </article>
          </section>

</div>
</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>