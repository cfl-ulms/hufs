<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000062"/>
	<c:param name="crclId" value="${curriculumVO.crclId}"/>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
	//게시판 추가
	$(".btn_cancel").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정을 취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//게시판 추가
	$(".btn_addbbs").click(function(){
		var addHtml = "",
			ctgrymasterId = "";
		
		$.ajax({
			url : "/cop/bbs/ctg/insertBBSCtgryMaster.json"
			, type : "post"
			, dataType : "json"
			, data : {siteId : "<c:out value="${curriculumVO.crclId }"/>", ctgrymasterNm : "<c:out value="${curriculumVO.crclId }"/>"}
			, success : function(data){
				if(data.successYn == "Y"){
					ctgrymasterId = data.ctgrymasterId;
				}
			}, error : function(){
				alert("error");
			}
		}).done(function(){
			addHtml = "<tr class=''>"
				+ 	"<td scope='row'>"
				+		($("tr.list").length + 1)
				+		"<input type='hidden' name='bbsIdList' value=''/>"
				+	"</td>"
				+	"<td>"
				+		"<select name='sysTyCodeList' class='table-select select2' data-select='style1'>"
				+			"<option value='ALL'>전체</option>"
				+			"<option value='INDIV'>개별</option>"
				+			"<option value='GROUP'>조별</option>"
				+			"<option value='CLASS'>반별</option>"
				+		"</select>"
				+	"</td>" 
				+	"<td>"
				+		"<button type='button' class='btn-sm-full btn-point btnModalOpen btn_addTag' data-modal-type='registr_tags' data-ctgry='"+ctgrymasterId+"'>태그등록</button>"
				+		"<input type='hidden' name='ctgrymasterIdList' value='"+ctgrymasterId+"'/>"
				+	"</td>"
				+	"<td><input type='text' class='table-input required' name='bbsNmList' placeholder='게시판 명 입력'/></td>"
				+	"<td><button type='button' class='btn-sm-full btn-outline-gray btn_del'>게시판 삭제</button></td>"
				+ "</tr>";
		
			$(".box_bbs").append(addHtml);
			Lms.common_ui.select2_init($(".select2"));
			Lms.common_ui.modal_open_evt_listener($(".btnModalOpen"));
		});
		return false;
	});
	
	//저장
	$(".btn_submit").click(function(){
		$(".required").each(function(){
			if(!$(this).val()){
				alert("게시판 명을 입력해주세요.");
				return false;
			}
		});
		
		$("#bbsControl").submit();
	});
	
});

//게시판 삭제
$(document).on("click", ".btn_del", function(){
	var bbsId = $(this).data("id"),
		ctgrymasterId = $(this).data("ctgry");
	
	$(this).parents("tr").remove();
	$.ajax({
		url : "/lms/manage/lmsBbsDelete.do"
		, type : "post"
		, dataType : "json"
		, data : {crclId : "<c:out value="${curriculumVO.crclId }"/>", bbsId : bbsId, ctgrymasterId : ctgrymasterId}
		, success : function(data){
			if(data.successYn == "Y"){
				alert("삭제되었습니다.");
			}
		}, error : function(){
			alert("error");
		}
	});
	return false;
});

//게시글 구분 태그 팝업 목록 조회
$(document).on("click", ".btn_addTag", function(){
	var ctgrymasterId = $(this).data("ctgry");
	
	$.ajax({
		url : "/lms/manage/selectBBSCtgryList.json"
		, type : "get"
		, dataType : "json"
		, data : {ctgrymasterId : ctgrymasterId}
		, success : function(data){
			if(data.successYn == "Y"){
				var btnHtml = "";
				var addHtml = "";
				for(var i=1; i<data.tagList.length; i++){
					btnHtml += '<li class="item bg-blue-light">' + data.tagList[i].ctgryNm + 
					'<button type="button" class="btn-remove tagDel" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" title="삭제"></button></li>';
					
					if(i == 1){
						addHtml = '<button type="button" class="btn-sm-110 btn-outline-gray tagAdd" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" data-upper="'+data.tagList[i].upperCtgryId+'">태그추가</button>';
						
					}
				}
				
				if(data.tagList.length <= 1){
					addHtml = '<button type="button" class="btn-sm-110 btn-outline-gray tagAdd" data-id="'+data.tagList[0].ctgryId+'" data-ctgry="'+ctgrymasterId+'" data-upper="'+data.tagList[0].ctgryId+'">태그추가</button>';
				}
				
				$('#tagUl').html(btnHtml); // 태그삭제
				$('#btnTagAdd').html(addHtml); // 태그추가
				
				
			}
		}, error : function(){
			alert("error");
		}
	});
	return false;
});

//게시글 구분 태그 삭제
$(document).on("click", ".tagDel", function(){
	var ctgryId = $(this).data("id"),
		ctgrymasterId = $(this).data("ctgry");
	
	$.ajax({
		url : "/lms/manage/tagDelete.do"
		, type : "post"
		, dataType : "json"
		, data : {ctgryId : ctgryId, ctgrymasterId : ctgrymasterId}
		, success : function(data){
			if(data.successYn == "Y"){
				var btnHtml = "";
				var addHtml = "";
				for(var i=1; i<data.tagList.length; i++){
					btnHtml += '<li class="item bg-blue-light">' + data.tagList[i].ctgryNm + 
					'<button type="button" class="btn-remove tagDel" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" title="삭제"></button></li>';
					
					//addHtml += '<button type="button" class="btn-sm-110 btn-outline-gray" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" data-upper="'+data.tagList[i].upperCtgryId+'">태그추가</button>';
				}
				
				$('#tagUl').html(btnHtml); // 태그삭제
				//$('#btnTagAdd').html(addHtml); // 태그추가
				
				alert("삭제되었습니다.");
			}
		}, error : function(){
			alert("error");
		}
	});
	return false;
});

//게시글 구분 태그 추가
$(document).on("click", ".tagAdd", function(){
	var ctgryId = $(this).data("id"),
		ctgrymasterId = $(this).data("ctgry"),
		upperCtgryId = $(this).data("upper");
	
	$.ajax({
		url : "/lms/manage/tagAdd.do"
		, type : "post"
		, dataType : "json"
		, data : {ctgryId : ctgryId, ctgrymasterId : ctgrymasterId, upperCtgryId : upperCtgryId, ctgryNm : $('#ctgryNm').val()}
		, success : function(data){
			if(data.successYn == "Y"){
				var btnHtml = "";
				var addHtml = "";
				for(var i=1; i<data.tagList.length; i++){
					btnHtml += '<li class="item bg-blue-light">' + data.tagList[i].ctgryNm + 
					'<button type="button" class="btn-remove tagDel" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" title="삭제"></button></li>';
					
					//addHtml += '<button type="button" class="btn-sm-110 btn-outline-gray" data-id="'+data.tagList[i].ctgryId+'" data-ctgry="'+ctgrymasterId+'" data-upper="'+data.tagList[i].upperCtgryId+'">태그추가</button>';
				}
				
				$('#ctgryNm').val("");
				
				$('#tagUl').html(btnHtml); // 태그삭제
				//$('#btnTagAdd').html(addHtml); // 태그추가
			}
		}, error : function(){
			alert("error");
		}
	});
	return false;
});

</script>


<div id="registr_tags_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">게시글 구분 태그등록</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text">게시글 구분에 사용할 태그를 등록해주세요.</p>
        <div class="modal-area-sm">
          <div class="flex-row mb-25">
            <div class="flex-col">
              <input type="text" id="ctgryNm" maxlength="8" placeholder="최대 8자까지 입력 가능합니다.">
            </div>
            <div id="btnTagAdd" class="flex-col-auto">
              
            </div>
          </div>
          <ul id="tagUl" class="selected-items-wrap row-full"></ul>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button class="btn-xl btn-point btnModalConfirm">등록</button> -->
      </div>
    </div>
  </div>
</div>


          <div class="page-content-header">
            <div class="title-wrap">
              <p class="sub-title">
              	<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
   					<c:if test="${statusCode.code eq curriculumVO.processSttusCode}"><c:out value="${statusCode.codeNm}"/></c:if>
				</c:forEach>
              </p>
              <h2 class="title"><c:out value="${curriculumVO.crclbNm}"/></h2>
            </div>
            <!-- TODO
            <div class="util-wrap">
              <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
            </div>
             -->
          </div>
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="1"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>

			<article class="content-wrap">
              <!-- 과정게시판 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정게시판 (${totCnt})</div>
                  <p class="desc-sm">해당 과정에 필요한 만큼 과정게시판을 생성할 수 있습니다.</p>
                </div>
                <div class="btn-group-wrap">
                  <div class="right-area">
                  	<c:url var="bbsUrl" value="/lms/manage/lmsBbsControl.do">
                  		<c:param name="menuId" value="MNU_0000000000000062" />
						<c:param name="crclId" value="${curriculumVO.crclId}" />
					</c:url>
                    <a href="${bbsUrl}" class="btn-md btn-point btn_addbbs btn_mngTxt fR">게시판 추가</a>
                  </div>
                </div>
              </div>
              <hr class="line-hr mb-20">
              <div class="content-body">
              	<form id="bbsControl" method="post" action="/lms/manage/lmsBbsInsert.do">
					<input type="hidden" name="crclId" value="<c:out value="${curriculumVO.crclId }"/>"/>
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:15%'>
                    <col style='width:15%'>
                    <col>
                    <col style='width:15%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700 keep-all'>
                      <th scope='col'>No</th>
                      <th scope='col'>사용자 그룹</th>
                      <th scope='col'>게시글 구분 태그</th>
                      <th scope='col'>게시판 명</th>
                      <th scope='col'>등록일</th>
                    </tr>
                  </thead>
                  <tbody class="box_bbs">
                  	<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
                  		<tr class="list">
							<td>
								<c:out value="${status.count}"/>
								<input type="hidden" name="bbsIdList" value="${result.bbsId}"/>
							</td>
							<c:choose>
								<c:when test="${status.count eq 1}">
									<td>
										<c:choose>
											<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
											<c:when test="${result.sysTyCode eq 'INDIV'}">개별</c:when>
											<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
											<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
										</c:choose>
										<input type="hidden" name="sysTyCodeList" value="${result.sysTyCode}"/>
									</td>
									<td>
										<button type='button' class='btn-sm-full btn-point btnModalOpen btn_addTag' data-modal-type='registr_tags' data-ctgry='${result.ctgrymasterId}' >태그등록</button>
										<input type="hidden" name="ctgrymasterIdList" value="${result.ctgrymasterId}"/>
									</td>
									<td>
										<c:out value="${result.bbsNm}"/>
										<input type="hidden" name="bbsNmList" value="${result.bbsNm}"/>
									</td>
									<td>필수 게시판(삭제불가)</td>
								</c:when>
								<c:otherwise>
									<td>
										<select class='table-select select2' data-select='style1' name="sysTyCodeList">
											<option value="ALL" <c:if test="${result.sysTyCode eq 'ALL'}">selected="selected"</c:if>>전체</option>
											<option value="INDIV" <c:if test="${result.sysTyCode eq 'INDIV'}">selected="selected"</c:if>>개별</option>
											<option value="GROUP" <c:if test="${result.sysTyCode eq 'GROUP'}">selected="selected"</c:if>>조별</option>
											<option value="CLASS" <c:if test="${result.sysTyCode eq 'CLASS'}">selected="selected"</c:if>>반별</option>
										</select>
									</td>
									<td>
										<button type='button' class='btn-sm-full btn-point btnModalOpen btn_addTag' data-modal-type='registr_tags' data-ctgry='${result.ctgrymasterId}'>태그등록</button>
										<input type="hidden" name="ctgrymasterIdList" value="${result.ctgrymasterId}"/>
									</td>
									<td><input type="text" class="wid100 required" name="bbsNmList" value="<c:out value="${result.bbsNm}"/>" placeholder="게시판 명 입력"/></td>
									<td>
										<button type='button' class='btn-sm-full btn-outline-gray btn_del' data-id="${result.bbsId}" data-ctgry="${result.ctgrymasterId}">게시판 삭제</button>
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
                  	</c:forEach>
                  </tbody>
                </table>
                </form>
              </div>
            </article>
            <div class="page-btn-wrap mt-50">
              <c:url var="listUrl" value="/lms/manage/lmsControl.do${_BASE_PARAM}"/>
              <a href="${listUrl }" class="btn-xl btn-outline-gray">취소</a>
              <a href="#" class="btn-xl btn-point btn_submit">저장</a>
            </div>
          </section>
</div>
</div>
</div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>