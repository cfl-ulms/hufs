<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId }"/>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchCampusId}"><c:param name="searchCampusId" value="${searchVO.searchCampusId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
    <c:when test="${param.ajaxFlag eq 'all' or param.ajaxFlag eq 'wish'}">
    	<c:forEach var="result" items="${resultList}" varStatus="status">
	   		<c:url var="viewUrl" value="/lms/crm/CurriculumAllView.do${_BASE_PARAM}">
				<c:param name="crclId" value="${result.crclId}"/>
				<c:param name="crclbId" value="${result.crclbId}"/>
				<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
			</c:url>
	   		<tr onclick="location.href='${viewUrl}'">
	   			<c:choose>
	   				<c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
	   				<c:when test="${result.processSttusCodeDate eq 3 or result.processSttusCodeDate eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
	   				<c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
	   				<c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
	   			</c:choose>
	   			<td scope="row" class="${processSttusColor }">
	   				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
	   					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
					</c:forEach>
	   			</td>
	   			<td><c:out value="${result.crclLangNm}"/></td>
	   			<td><c:out value="${result.crclNm}"/></td>
	   			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	   			<td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
	   			<td>
	   				<c:choose>
	   					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
	   					<c:otherwise>일반</c:otherwise>
	   				</c:choose>
	   			</td>
	   			<td>
	    			<c:forEach var="campus" items="${campusList}" varStatus="status">
                      <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
                        <c:out value="${campus.ctgryNm}"/>
                      </c:if>
                    </c:forEach>
	   			</td>
			</tr>
		</c:forEach>
    </c:when>
    <c:when test="${param.ajaxFlag eq 'myStudent' }">
        <c:forEach var="result" items="${resultList}" varStatus="status">
            <c:url var="viewUrl" value="/lms/crm/CurriculumAllView.do${_BASE_PARAM}">
                <c:param name="crclId" value="${result.crclId}"/>
                <c:param name="crclbId" value="${result.crclbId}"/>
                <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
            </c:url>
            <tr onclick="location.href='${viewUrl}'">
                <c:choose>
                    <c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
                    <c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
                    <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
                </c:choose>
                <td scope="row" class="${processSttusColor }">
                    <c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
                        <c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
                    </c:forEach>
                </td>
                <td><c:out value="${result.crclLangNm}"/></td>
                <td><c:out value="${result.crclNm}"/></td>
                <td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${result.targetType eq 'Y'}">본교생</c:when>
                        <c:otherwise>일반</c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:forEach var="campus" items="${campusList}" varStatus="status">
                      <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
                        <c:out value="${campus.ctgryNm}"/>
                      </c:if>
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
    </c:when>
    <c:when test="${param.ajaxFlag eq 'myTeather' }">
        <c:forEach var="result" items="${resultList}" varStatus="status">
            <c:url var="viewUrl" value="/lms/crm/CurriculumAllView.do${_BASE_PARAM}">
                <c:param name="crclId" value="${result.crclId}"/>
                <c:param name="crclbId" value="${result.crclbId}"/>
                <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
            </c:url>
            <tr onclick="location.href='${viewUrl}'">
                <td><c:out value="${result.crclYear}"/></td>
                <td><c:out value="${result.crclTermNm}"/></td>
                <c:choose>
                    <c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
                    <c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
                    <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
                </c:choose>
                <td scope="row" class="${processSttusColor }">
                    <c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
                        <c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
                    </c:forEach>
                </td>
                <td><c:out value="${result.crclLangNm}"/></td>
                <td><c:out value="${result.crclNm}"/></td>
                <td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${result.targetType eq 'Y'}">본교생</c:when>
                        <c:otherwise>일반</c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:forEach var="campus" items="${campusList}" varStatus="status">
                      <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
                        <c:out value="${campus.ctgryNm}"/>
                      </c:if>
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
    </c:when>
</c:choose>