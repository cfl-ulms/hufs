<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>


<%-- 관리자 페이지 때문에 분기처리 하였는데 데이터 불러오는건 똑같고 퍼블리싱만 다름 --%>
<c:choose>
    <c:when test="${searchVO.adminPageFlag eq 'Y' }">
        <c:choose>
		    <c:when test="${searchVO.hwWaitingType eq '1' }">
		        <c:forEach var="result" items="${selectHomeworkSubmitWaitingList}" varStatus="status">
		            <li style="padding:10px;">${result.userNm }<span class="font-sm">(${result.userId })</span></li>
		        </c:forEach>
		    </c:when>
		    <c:when test="${searchVO.hwWaitingType eq '2' }">
		        <c:forEach var="result" items="${selectHomeworkTestWaitingList}" varStatus="status">
		            <li style="padding:10px;">${result.userNm }<span class="font-sm">(${result.userId })</span></li>
		        </c:forEach>
		    </c:when>
		</c:choose>
    </c:when>
    <c:otherwise>
        <c:choose>
		    <c:when test="${searchVO.hwWaitingType eq '1' }">
		        <c:forEach var="result" items="${selectHomeworkSubmitWaitingList}" varStatus="status">
		            <tr>
		              <td class="pl-20 pr-20">
		                <c:choose>
		                    <c:when test="${empty result.photoStreFileNm}"><i class="icon-user mr-10"></i></c:when>
		                    <c:otherwise><i class="icon-user mr-10" style='background-image: url(${MembersFileStoreWebPath}<c:out value="${result.photoStreFileNm}"/>);'></i></c:otherwise>
		                </c:choose>
		                ${result.userNm }<span class="font-sm">(${result.userId })</span>
		              </td>
		            </tr>
		        </c:forEach>
		    </c:when>
		    <c:when test="${searchVO.hwWaitingType eq '2' }">
		        <c:forEach var="result" items="${selectHomeworkTestWaitingList}" varStatus="status">
		            <tr>
		              <td class="pl-20 pr-20">
		                <c:choose>
		                    <c:when test="${empty result.photoStreFileNm}"><i class="icon-user mr-10"></i></c:when>
		                    <c:otherwise><i class="icon-user mr-10" style='background-image: url(${MembersFileStoreWebPath}<c:out value="${result.photoStreFileNm}"/>);'></i></c:otherwise>
		                </c:choose>
		                ${result.userNm }<span class="font-sm">(${result.userId })</span>
		              </td>
		            </tr>
		        </c:forEach>
		    </c:when>
		</c:choose>
    </c:otherwise>
</c:choose>