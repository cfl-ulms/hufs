<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>게시판 선택</title>

  <meta name="title" content="게시판 선택">

  <meta property="og:type" content="website">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>
  <!--=================================================
          페이지별 스크립트
  ==================================================-->
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = $(".chk:checked").val(),
			name = $(".chk:checked").parents("tr").find(".box_bbsnm").text(),
			colect = $(".chk:checked").parents("tr").find(".colect").val(),
			size = $(".chk:checked").length;
		
		if(size == 0){
			alert("게시판을 선택해주세요.");
		}else if(!colect || colect == 0){
			alert("횟 수를 확인해주세요.");
		}else{
			name = name + " / " + colect + "회 ";
			
			window.opener.selBbs(id, name, colect);
			window.close();
		}
		
		return false;
	});
	
	
	$(".btnModalCancel, .btnModalClose").click(function(){
		window.close();
	});
});
</script>
</head>
<body>

<div id="select_board_modal" class="alert-modal" style="display:block;">
  <div class="modal-dialog modal-top">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">게시판 선택</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text">성적이 반영될 게시판을 선택해주세요.</p>
        <p class="modal-subtext mb-30">
          게시판 수에 따라 100점 환산 점수로 자동 표기됩니다. (단, 책임, 강의책임교원 필요시 수동으로 점수 수정도 가능합니다 과정관리 > 성적 메뉴에서 점수 수정도 가능.)<br>
          예시) 게시판 3개를 선택한 경우, Pass 기준으로 학생이 글쓰기 완료 시 각 게시판 1 점씩 3점 만점을 획득하고, 100점 환산점 계산으로 수업참여도는 100점으로 표기됩니다.
        </p>
        <table class="common-table-wrap size-sm">
          <colgroup>
            <col style="width:70px;">
            <col>
            <col style="width:118px">
          </colgroup>
          <thead>
            <tr class="bg-gray-light font-700">
              <th>선택</th>
              <th>게시판명</th>
              <th>Pass 기준</th>
            </tr>
          </thead>
          <tbody>
          	<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
				<tr class="list">
					<td>
						<label class="checkbox circle">
		                  <input type="radio" class="chk" name="bbsIdList" value="${result.bbsId}">
		                  <span class="custom-checked"></span>
		                </label>
					</td>
					<td class="box_bbsnm">
						(
						<c:choose>
							<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
							<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
							<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
						</c:choose>
						)
						<c:out value="${result.bbsNm}"/>
					</td>
					<td>
						<div class="input-group">
		                  <span class="unit font-gray-light">회</span>
		                  <input type="number" class="table-input onlyNum colect" name="colect" value="0" placeholder="0">
		                </div>
					</td>
				</tr>
			</c:forEach>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button class="btn-xl btn-point btn_ok">등록</button>
      </div>
    </div>
  </div>
</div>