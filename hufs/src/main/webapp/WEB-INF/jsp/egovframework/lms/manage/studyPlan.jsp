<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
	//출석
	$(".atd").click(function(){
		var id = $(this).data("id"),
			sbj = $(this).data("nm"),
			crclId = $(this).data("crclid"),
			attTCnt = $(this).data("tot"),
			attYCnt = $(this).data("ycnt"),
			attLCnt = $(this).data("lcnt"),
			attNCnt = $(this).data("ncnt");
		
		$.ajax({
			url : "/lms/atd/selectAttendList.do"
			, type : "post"
			, dataType : "html"
			, data : {plId : id, crclId : crclId, modalAt : "Y"}
			, success : function(data){
				$("#sbj").text(sbj);
				$("#plId").val(id);
				$("#atdlist").html(data);
				
				$("#goAtd").attr("href", "/lms/atd/selectAttendList.do?menuId=MNU_0000000000000084&crclId=" + crclId + "&plId=" + id + "&step=5&tabType=T");
			}, error : function(){
				alert("error");
			}
		});
		
		return false;
	});
});

function search(){
	var searchFacNm = $("#inp_text").find("option:selected").data("nm"),
		resultCnt = 0;
	
	if(searchFacNm){
		$(".schTr").hide();
		$(".facNm").each(function(){
			if($(this).val() == searchFacNm){
				$(this).parents(".schTr").show();
				resultCnt++;
			}
		});
		
		if(resultCnt == 0){
			$(".empty").show();
		}else{
			$(".empty").hide();
		}
	}else{
		$(".schTr").show();
		$(".empty").hide();
	}
	
	return false;
}

//출석 검색
function searchAtd(){
	var params = $("#atdFrom").serialize();
	
	$.ajax({
		url : "/lms/atd/selectAttendList.do"
		, type : "post"
		, dataType : "html"
		, data : params
		, success : function(data){
			$("#atdlist").html(data);
		}, error : function(){
			alert("error");
		}
	});
	
	return false;
}
</script>

          <div class="page-content-header">
            <c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
			<!-- 
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
          <section class="page-content-body">
            <article class="content-wrap">
              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
              	<c:choose>
              		<c:when test="${not empty param.step}">
              			<c:set var="step" value="${param.step}"/>
              		</c:when>
              		<c:otherwise>
              			<c:set var="step" value="5"/>
              		</c:otherwise>
              	</c:choose>
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="${step}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
			</article>
			<article class="content-wrap">
              <div class="box-wrap ">
                <h3 class="title-subhead">수업계획 검색</h3>
                <form name="frm" method="post" action="<c:url value="/lms/crcl/CurriculumList.do"/>" onsubmit="return search();">
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select class='table-select select2 search fac' data-select='style1' data-placeholder='교원검색' id="inp_text">
	                       <option value="">선택</option>
	                       <option value="all" data-nm="">전체</option>
	                        <c:forEach var="result" items="${facList}">
								<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
		 					</c:forEach>
	                     </select>
	                    </div>
	                  </div>
	                </div>
	
	                <button class="btn-sm font-400 btn-point mt-20">검색</button>
	            </form>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 
              <div class="content-header">
                <div class="btn-group-wrap">
                  <div class="left-area">
                    <button type="button" class="btn-outline btn-md">수업계획서 다운로드</button>
                  </div>
                </div>
              </div>
               -->
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:10%'>
                  <col style='width:15%'>
                  <col style='width:10%'>
                  <col>
                  <col style='width:15%'>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col style='width:15%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>차시</th>
                    <th scope='col'>수업일</th>
                    <th scope='col'>시수</th>
                    <th scope='col'>수업계획서</th>
                    <th scope='col'>교원명</th>
                    <th scope='col'>수업자료</th>
                    <th scope='col'>수업방법</th>
                    <th scope='col'>출석</th>
                  </tr>
                </thead>
                <tbody>
                	<c:choose>
				  		<c:when test="${curriculumVO.totalTime > 0}">
				  			<c:forEach var="scheduleMngVO" items="${resultList}" varStatus="status">
					  			<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
								<tr class="schTr">
									<td class="alC line"><c:out value="${status.count}"/></td>
									<td class="alC line">
										<c:out value="${scheduleMngVO.startDt}"/>
										<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
									</td>
									<td class="alC line"><c:out value="${scheduleMngVO.sisu}"/></td>
									<td class="alL line">
										<c:url var="viewUrl" value="/lms/manage/studyPlanView.do${_BASE_PARAM}">
											<c:param name="plId" value="${scheduleMngVO.plId}" />
										</c:url>
										<a href="${viewUrl}" class='underline' target='_blank'><c:out value="${scheduleMngVO.studySubject}"/></a>
									</td>
									<td class="alC line">
										<ul>
											<c:forEach var="result" items="${facPlList}" varStatus="status">
												<c:if test="${result.plId eq scheduleMngVO.plId}">
													<li>
														<span>
															<a href="${viewUrl}" class='underline' target='_blank'><c:out value="${result.userNm}"/>
															<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
															<input type="hidden" class="facNm" value="${result.userNm}"/></a>
														</span> 
													</li>
												</c:if>
											</c:forEach>
										</ul>
									</td>
									<td class="alC line box_period">
										<c:choose>
											<c:when test="${not empty scheduleMngVO.atchFileId}"><i class='icon-download'>다운로드</i></c:when>
											<c:otherwise>-</c:otherwise>
										</c:choose>
									</td>
									<td class="alC line">
										<c:if test="${not empty scheduleMngVO.courseId}">
											<c:choose>
								  				<c:when test="${scheduleMngVO.spType ne 'N' }">
								  					강의식
								  				</c:when>
								  				<c:otherwise>
								  					비강의식
								  				</c:otherwise>
								  			</c:choose>
								  			- <c:out value="${scheduleMngVO.courseNm}"/>
							  			</c:if>
									</td>
									<td class="alC line">
										<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
										<c:choose>
											<c:when test="${today >= startDt}">
												<c:choose>
						                      		<c:when test="${not empty scheduleMngVO.attTCnt and scheduleMngVO.attTCnt == scheduleMngVO.attYCnt}">전원출석</c:when>
						                      		<c:otherwise>
						                      			<a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${scheduleMngVO.plId}" data-nm="${scheduleMngVO.studySubject}" data-crclid="${curriculumVO.crclId}" data-lcnt="${scheduleMngVO.attLCnt}" data-ncnt="${scheduleMngVO.attNCnt}" data-tot="${scheduleMngVO.attTCnt}" data-ycnt="${scheduleMngVO.attYCnt}">
							                      			<c:if test="${scheduleMngVO.attLCnt > 0}">지각 <c:out value="${scheduleMngVO.attLCnt}"/></c:if>
							                      			<c:if test="${scheduleMngVO.attLCnt > 0 and scheduleMngVO.attNCnt > 0}">,</c:if>
							                      			<c:if test="${scheduleMngVO.attNCnt > 0}">결석 <c:out value="${scheduleMngVO.attNCnt}"/></c:if>
						                      			</a>
						                      		</c:otherwise>
						                      	</c:choose>
											</c:when>
											<c:otherwise>
												대기
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
							<tr class="empty" style="display:none;">
								<td colspan="8" class="alC">검색 결과가 없습니다.</td>
							</tr>
				  		</c:when>
				  		<c:when test="${curriculumVO.totalTime > 0 and today < curriculumStartDate}">
				  			<tr>
				  				<td colspan="8" class="alC">과정 기간이 아닙니다.</td>
				  			</tr>
				  		</c:when>
				  		<c:otherwise>
				  			<tr>
				  				<td colspan="8" class="alC">시수 없음</td>
				  			</tr>
				  		</c:otherwise>
				  	</c:choose>
                </tbody>
              </table>
            </article>
          </section>

</div>
</div>
</div>
</div>

<div id="attend_view_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">출석확인</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">[<span id="sbj"></span>]</div>
        <form name="atdFrom" id="atdFrom" onsubmit="return searchAtd();">
        	<input type="hidden" name="crclId" value="${curriculumVO.crclId }"/>
        	<input id="plId" type="hidden" name="plId" value=""/>
        	<input id="modalAt" type="hidden" name="modalAt" value="Y"/>
	        <div class="flex-row-ten">
	        	<div class="flex-ten-col-3">
	              <select name="attentionType" class="table-select select2 select2-hidden-accessible" data-select="style1" tabindex="-1" aria-hidden="true">
	                  <option value="">전체</option>
	                  <option value="Y">출석</option>
	                  <option value="NY">지각 및 결석자</option>
	                </select>
	            </div>
	            <div class="flex-ten-col-5">
	              <div class="ell">
	                <input type="text" name="userNm" value="" placeholder="이름을 입력해 주세요.">
	              </div>
	            </div>
	            <div class="flex-ten-col-2">
	              <div class="ell"><button class="btn-sm font-400 btn-point goods-search-btn" style="height:40px;">검색</button></div>
	            </div>
	        </div>
        </form>
        <div style="display:block;width:100%;height:300px;overflow-y:auto;">
	        <table class="modal-table-wrap size-sm center-align">
	          <colgroup>
	            <col width="10%">
	            <col width="30%">
	            <col width="*">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray">
	              <th class="font-700">No</th>
	              <th class="font-700">상태</th>
	              <th class="font-700">이름</th>
	            </tr>
	          </thead>
	          <tbody id="atdlist"></tbody>
	        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
        <a href="#" id="goAtd" class="btn-xl btn-point btnModalConfirm" target="_blank">출석 수정</a>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>