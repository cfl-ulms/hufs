<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>


<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="crclId" value="${param.crclId}"/>
</c:url>
<% /*URL 정의*/ %>


 <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>게시판 선택</title>

  <meta name="title" content="게시판 선택">

  <meta property="og:type" content="website">
  <meta property="og:image" content="${CML}/imgs/common/og.jpg">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/common_staff.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board_staff.css?v=1">

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/modal.css?v=2">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="<c:url value='/template/lms/lib/echarts/echarts.min.js'/>"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>

<script>
$(document).ready(function(){
	$(".chartDiv").each(function (idx, el) {
	var totalCnt = 0;
	var myChart = echarts.init(el);

	var tempArr = $(this).closest(".queDiv").find("tr.answerClass");
	var dataMapArr = $.makeArray(tempArr.map(function(){
		if($(this).data("excnt") != 0){
	 		var tempObject = new Object();
	 		tempObject.value = $(this).data("excnt");
	  	 	tempObject.name = $(this).data("excn");
	  	 	totalCnt += $(this).data("excnt");
	  	 	return tempObject;
		}
	}));

	var dataArr = $.makeArray(tempArr.map(function(){
	 	return  $(this).data("excn");
	}));


	var option = {
			 color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		      animation: false,
		      legend: {
		        orient: 'vertical',
		        right: 15,
		        top: 30,
		        data: dataArr,
		        icon: 'rect',
		        itemGap: 20,
		        textStyle: {
		          fontSize: 13,
		          verticalAlign: 'middle',
		        },
		      },
		      series: [
		        {
	        		color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		          	type: 'pie',
		          	radius: '85%',
		          	label:false,
		          	padding : '20px',
		          	center: ['35%', '50%'],
		          	hoverAnimation: false,
		          	animation: false,
		          	data: dataMapArr
		        }
		      ]
		    };

	myChart.setOption(option);
	$(this).closest(".queDiv").find(".totalCnt").html("응답 (<span class=\'font-point\'>"+totalCnt+"</span>명)");
	});
});
</script>
<style>
canvas{
	padding : 20px;
}
</style>

	<div class="area">
    <div id="printContent">
      <div class="page-box">
        <div class="page-content-wrap pb-0">
          <div class="page-content-header">
            <div class="title-wrap">
              <p class="sub-title">
	              	<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
						<c:if test="${statusCode.code eq curriculumVO.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
					</c:forEach>
				</p>
              <h2 class="title">${curriculumVO.crclNm}</h2>
            </div>
          </div>
          <section class="page-content-body">

            <article class="content-wrap">
              <!-- 교육과정 개요 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교육과정 개요</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>년도</td>
                      <td>${curriculumVO.crclYear }년</td>
                      <td scope='row' class='font-700'>학기</td>
                      <td>${curriculumVO.crclTermNm}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>언어</td>
                      <td>${curriculumVO.crclLangNm}</td>
                      <td scope='row' class='font-700'>학점인정 여부</td>
                      <td>${curriculumVO.gradeNum}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정기간</td>
                      <td>${curriculumVO.startDate} ~ ${curriculumVO.endDate}</td>
                      <td scope='row' class='font-700'>과정시간</td>
                      <td>총 ${curriculumVO.totalTime}시간 / 주 ${curriculumVO.weekNum}회 / 일 ${curriculumVO.dayTime}시간</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정진행캠퍼스</td>
                      <td colspan='3'>${curriculumVO.campusNm} ${curriculumVO.campusPlace}호</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>책임교수</td>
                      <td colspan='3'>${curriculumVO.userNm} (${curriculumVO.hostCodeNm})</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정 개요 및 목표</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정만족도 진행률</td>
                      	<fmt:formatNumber value="${curriculumAddInfo.joinCnt}" type="number" var="joinCnt" />
						<fmt:formatNumber value="${curriculumAddInfo.memberCnt}" type="number" var="memberCnt" />
						<td colspan='3'>
							<c:choose>
								<c:when test="${surveyVO.adminAt eq 'Y' }">
									<c:choose>
										<c:when test="${curriculumAddInfo.memberCnt ne '0' and joinCnt lt memberCnt}">
											<a href="#none">
												${ joinCnt} / ${memberCnt} (${memberCnt -  joinCnt}명 미제출)
											</a>
										</c:when>
										<c:when test="${joinCnt eq memberCnt}">
											${ joinCnt} / ${memberCnt}
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise>
										${ joinCnt} / ${memberCnt}
										<c:if test="${curriculumAddInfo.memberCnt ne '0' and joinCnt lt memberCnt}">
											(${memberCnt -  joinCnt}명 미제출)
										</c:if>
								</c:otherwise>
							</c:choose>

                     </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 과정만족도 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정만족도</div>
                </div>
              </div>
              <div class="content-body">
                <div class="no-content-wrap">
                	<c:if test="${curriculumAddInfo.sum ne 0 }">
		                	<%-- <fmt:formatNumber var="resultPoint"  pattern="0" value="${(curriculumAddInfo.sum/ curriculumAddInfo.joinCnt)*100} " /> --%>
		                	<fmt:formatNumber var="resultPoint"  pattern="0" value="${curriculumAddInfo.sum}"/>
	                	</c:if>
	                  	<b class="satisfy">과정만족도 : ${curriculumAddInfo.sum eq 0 ? 0 : resultPoint}</b>
                </div>

				<div style="margin-top: 10px;">
	                	<table class="common-table-wrap size-sm">
	                		<thead>
	                			<tr>
	                				<th>항목</th>
	                				<c:forEach items="${surveyAnswer}" var="result1" varStatus="status">
	                					<c:choose>
	                						<c:when test="${result1.qesitmTyCode eq 'multiple'}"><th>${result1.qesitmSj}</th></c:when>
	                						<c:otherwise></c:otherwise>
	                					</c:choose>
	                				</c:forEach>
	                			</tr>
	                		</thead>
	                		<tbody>
	                			<tr>
	                				<td>매우그렇다</td>
	                				<c:forEach items="${veryGoodList}" var="veryGoodList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${veryGoodList eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${veryGoodList}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그렇다</td>
	                				<c:forEach items="${goodList}" var="goodList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${goodList eq '0.00'}">0</c:when>
	                							<c:otherwise><c:out value="${goodList}"/> </c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그저 그렇다</td>
	                				<c:forEach items="${soSoList}" var="soSoList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${soSoList eq '0.00'}">0</c:when>
	                							<c:otherwise>${soSoList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>그렇지 않다</td>
	                				<c:forEach items="${notBadList}" var="notBadList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${notBadList eq '0.00'}">0</c:when>
	                							<c:otherwise>${notBadList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>매우 그렇지 않다</td>
	                				<c:forEach items="${badList}" var="badList">
	                					<td>
	                						<c:choose>
	                							<c:when test="${badList eq '0.00'}">0</c:when>
	                							<c:otherwise>${badList}</c:otherwise>
	                						</c:choose>
	                					</td>
	                				</c:forEach>
	                			</tr>
	                			<tr>
	                				<td>평균</td>
	                				<c:forEach items="${surveyAvgList}" var="surveyAvgList">
	                					<td>${surveyAvgList}</td>
	                				</c:forEach>
	                			</tr>
	                		</tbody>
	                	</table>
	                </div>

                <c:if test="${not empty surveyAnswer }">
					<c:forEach items="${surveyAnswer}" var="result" varStatus="status">

		                <div class="satisfy-table-wrap queDiv">
		                <div class="satisfy-head">${status.count }. ${result.qesitmSj }</div>

						<c:if test="${result.qesitmTyCode eq 'multiple'}">
		                  <div class="satisfy-img-box">
		                    <div class="chartDiv" style="display:inline-block;width:100%;height: 300px;border-style: none;"></div>
		                  </div>
		                 </c:if>
		                  <!-- 테이블영역-->

		                  <c:choose>
							<c:when test="${result.qesitmTyCode eq 'multiple'}">
								<table class="common-table-wrap size-sm">
				                    <colgroup>
				                      <col style='width:33.333%'>
				                      <col style='width:33.333%'>
				                      <col style='width:33.333%'>
				                    </colgroup>
				                    <thead>
				                      <tr class='bg-gray font-700'>
				                        <th scope='col'>점수</th>
				                        <th scope='col'>항목</th>
				                        <th scope='col' class="totalCnt"></th>
				                      </tr>
				                    </thead>
				                    <tbody>
				                    	<c:forEach items="${result.answerList}" var="answer"  varStatus="aStatus">
					                      <tr class="answerClass" data-excn="${answer.exCn }" data-excnt="${answer.cnt }">
					                        <td scope='row'>${fn:length(result.answerList) - aStatus.index }</td>
					                        <td>${answer.exCn }</td>
											<td>${answer.cnt }</td>
					                      </tr>
				                     	</c:forEach>
				                    </tbody>
				                  </table>
							</c:when>
							<c:when test="${result.qesitmTyCode eq 'answer'}">
								 <table class="common-table-wrap size-sm">
				                    <colgroup>
				                      <col style='width:10%'>
				                      <col style='width:18%'>
				                      <col style='width:18%'>
				                      <col>
				                    </colgroup>
				                    <thead>
				                      <tr class='bg-gray font-700'>
				                        <th scope='col'>번호</th>
				                        <th scope='col'>응답자</th>
				                        <th scope='col'>전공</th>
				                        <th scope='col'>응답 내용</th>
				                      </tr>
				                    </thead>
				                    <tbody>
				                    <c:forEach items="${result.essayList}" var="essay"  varStatus="eStatus">
				                      <tr>
				                        <td scope='row'>${eStatus.count}</td>
				                        <td>${essay.userNm }</td>
										<td>${essay.crclNm }</td>
										<td class='left-align'>${essay.cnsr }</td>
				                      </tr>
			                     	</c:forEach>
				                    </tbody>
				                  </table>
							</c:when>
						</c:choose>
		                </div>
		               </c:forEach>
	               </c:if>
              </div>
            </article>
          </section>
        </div>
      </div>
      <!-- <div class="page-btn-wrap mt-40">
        <button class="btn-xl btn-outline-gray popupPrint">설문결과 인쇄하기</button>
      </div>
      <div class="page-footer-wrap mt-70">
        <div class="logo-wrap">
          <img src="../imgs/common/img_header_logo.png" alt="">
        </div>
        <ul class="info-wrap">
          <li>Tel : 02-2173-2843~6</li>
          <li>E-mail : cfle@hufs.ac.kr</li>
          <li>ⓒ 2019 Hankuk University of Foreign Studies. All right Reserver</li>
        </ul>
      </div> -->
    </div>
    <!-- <div class="btn-top-wrap">
      <button type="button" class="btn-print popupPrint"></button>
      <div class="btn-go-top">Top</div>
    </div> -->
  </div>

