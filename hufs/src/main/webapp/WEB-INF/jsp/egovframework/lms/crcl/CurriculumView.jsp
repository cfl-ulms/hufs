<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.menuId}"><c:param name="menuId" value="${searchVO.menuId}" /></c:if>
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${param.cnAt eq 'Y'}"></c:when>
	<c:when test="${param.printAt eq 'Y'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="contentAt" value="Y"/>
			<c:param name="title" value="${curriculumVO.crclNm}"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8"/>
	</c:otherwise>
</c:choose>
<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCodeDate}"/>
<c:choose>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
$(document).ready(function(){
	//과정확정
	$(".btn_confirm").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//인쇄하기
	$("#btnPrint").click(function(){
		window.open("/lms/crcl/selectCurriculum.do?crclId=${curriculumVO.crclId}&popupAt=Y&printAt=Y","popup","top=1,left=1,width=1024,height=" + screen.availHeight);
	});
	
	//교원수 - 초기화
	facTotCnt();
	
	<c:if test="${param.printAt eq 'Y'}">
		window.print();
	</c:if>
	
	box_lesson();
});

//교원 수
function facTotCnt(){
	var dupCnt = 0,
		ids = [],
		idList = [];
	
	$(".facIdList").each(function(){
		var id = $(this).val(),
			dupAt = false;
		$.each(idList, function(i){
			if(idList[i] == id){
				dupAt = true;
			}
		});
		
		if(!dupAt){
			idList.push(id);
		}
	});
	
	$(".sumUser").text(idList.length);
}


function box_lesson(){
	var htmls = '';
	var crclId = '${curriculumVO.crclId}';
	var preLessonOrder = 0;
	var lessonOrderCnt = 0;
	var totLen =0;
	
	$.ajax({
           type: "POST",
           url: "/ajax/mng/lms/crcl/selectCurriculumLesson.json",
           data: {                
               "crclId" : crclId
               },
           dataType: "json",
           async: false,
           success: function(data, status) {
           	console.log(data);
           	console.log(data.lessonList);
           	$(".box_lesson > tr").remove();
           	if(data.status == "200"){
           		totLen=data.lessonList.length;
           		
           		$.each(data.lessonList, function(index){
           			
           			if(lessonOrderCnt == 0){
           				lessonOrderCnt = this.lessonOrderCnt;
           				preLessonOrder = this.lessonOrder;
    					htmls += '<tbody class="box_lesson">';
    					htmls += '<tr class="first">';
   					} else {
    					htmls += '<tr class="contentBody">';
   						
   					}
                   	 
   					if((lessonOrderCnt == this.lessonOrderCnt) && (preLessonOrder == this.lessonOrder)){
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line alC num">';
   						htmls += this.lessonOrder;
   						htmls += '</td>';
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="line lesnm">'+this.lessonNm+'</td>';
   					}
           			
					htmls += '<td class="line">'+this.chasiNm+'</td>';
					htmls += '</tr>';
					if((lessonOrderCnt-1) == 0){
    					htmls += '</tbody">';
					}
					preLessonOrder = this.lessonOrder;
   					--lessonOrderCnt;
           		});
           		
				$("#box_addLes").before(htmls);
           	}
           },
           error: function(xhr, textStatus) {
               alert("문제가 발생하였습니다. \n 관리자에게 문의하세요");
               document.write(xhr.responseText);
               return false;
           },beforeSend:function() {
           },
           complete:function() {
           }
       }); 
}

</script>
<c:if test="${param.cnAt ne 'Y'}">
		 <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
			
			<c:if test="${param.printAt ne 'Y'}">
				<div class="util-wrap">
			      <button type="button" id="btnPrint" class="btn-md btn-outline-gray">인쇄하기</button>
			    </div>
		    </c:if>
		    <br>
          </div>
          <section class="page-content-body">
          
          <c:choose>
          	<c:when test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y' and param.printAt ne 'Y'}">
          		<c:choose>
              		<c:when test="${not empty param.step}">
              			<c:set var="step" value="${param.step}"/>
              		</c:when>
              		<c:otherwise>
              			<c:set var="step" value="3"/>
              		</c:otherwise>
              	</c:choose>
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="${step}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
          	</c:when>
          	<c:otherwise>
          		<c:if test="${adminAt eq 'Y'}">
	          		<c:choose>
	              		<c:when test="${not empty param.step}">
	              			<c:set var="step" value="${param.step}"/>
	              		</c:when>
	              		<c:otherwise>
	              			<c:set var="step" value="3"/>
	              		</c:otherwise>
	              	</c:choose>
					<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
						<c:param name="step" value="${step}"/>
						<c:param name="crclId" value="${curriculumVO.crclId}"/>
						<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
						<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
					</c:import>
          		</c:if>
          	</c:otherwise>
          </c:choose>
<%--           	<c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y' and param.printAt ne 'Y'}">
          		<c:choose>
              		<c:when test="${not empty param.step}">
              			<c:set var="step" value="${param.step}"/>
              		</c:when>
              		<c:otherwise>
              			<c:set var="step" value="3"/>
              		</c:otherwise>
              	</c:choose>
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="${step}"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if> --%>
</c:if>			

            <article class="content-wrap">
              <!-- 기본과정 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">기본과정</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align mb-20">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>년도</td>
                      <td><c:out value="${curriculumVO.crclYear }"/></td>
                      <td scope='row' class='font-700 keep-all'>학기</td>
                      <td><c:out value="${curriculumVO.crclTermNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>언어</td>
                      <td colspan='3'><c:out value="${curriculumVO.crclLangNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정체계</td>
                      <td colspan="3"><c:out value="${curriculumVO.sysCodeNmPath}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>기본과정명</td>
                      <td colspan="3"><c:out value="${curriculumVO.crclbNm}"/></td>
                    </tr>
                  </tbody>
                </table>
                
                <table class="common-table-wrap size-sm left-align">
			        <colgroup>
			          <col class='bg-gray' style='width:20%'>
			          <col style='width:30%'>
			          <col class='bg-gray' style='width:20%'>
			          <col style='width:30%'>
			        </colgroup>
			        <tbody>
			          <tr class="">
			            <td scope='row' class='font-700'>이수구분 <i class='font-point font-400'>*</i></td>
			            <td>${curriculumVO.divisionNm}</td>
			            <td scope='row' class='font-700'>학점인정 여부</td>
			            <c:choose>
			            	<c:when test="${curriculumVO.gradeAt eq 'Y'}">
			            		<td>예(${curriculumVO.gradeNum} 학점)</td>
			            	</c:when>
			            	<c:otherwise>
			            		<td class='font-red'>아니오</td>
			            	</c:otherwise>
			            </c:choose>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>관리구분 <i class='font-point font-400'>*</i></td>
			            <td>${curriculumVO.controlNm }</td>
			            <td scope='row' class='font-700'>프로젝트 과정 여부</td>
			            <td>${curriculumVO.projectAt eq 'Y' ? '프로젝트 과정' : '프로젝트 과정 아님' }</td>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>대상 <i class='font-point font-400'>*</i></td>
			            <td>${curriculumVO.targetType eq 'Y' ? '본교생' : '일반' } > ${curriculumVO.targetDetailNm }</td>
			            <td scope='row' class='font-700'>총 시간 등록과정 여부</td>
			            <td>${curriculumVO.totalTimeAt eq 'Y' ? '총 시간 등록과정' : '총 시간 등록과정 아님' }</td>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>성적처리기준 <i class='font-point font-400'>*</i></td>
			            <td colspan='3'>${curriculumVO.evaluationAt eq 'Y' ? '절대' : '상대' }평가 
			            <c:choose>
			            	<c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">> 100점 기준 점수 환산표(Letter Grade)</c:when>
			            	<c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">> P/F</c:when>
			            </c:choose>
			            </td>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>운영보고서 유형 <i class='font-point font-400'>*</i></td>
			            <td colspan='3'>
			            	<c:choose>
			            		<c:when test="${curriculumVO.reportType eq 'PRJ'}">프로젝트과정</c:when>
			            		<c:when test="${curriculumVO.reportType eq 'NOR'}">일반과정</c:when>
			            	</c:choose>
			            </td>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
			            <td>
			            	<c:forEach var="result" items="${surveyList}" varStatus="status">
								<c:if test="${result.schdulId eq curriculumVO.surveySatisfyType}">${result.schdulNm}</c:if>
							</c:forEach>
			            </td>
			            <td scope='row' class='font-700'>교원대상 과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
			            <td>
			            	<c:choose>
								<c:when test="${empty curriculumVO.professorSatisfyType}">
									선택안함
								</c:when>
								<c:otherwise>
									<c:forEach var="result" items="${surveyList3}" varStatus="status">
										<c:if test="${result.schdulId eq curriculumVO.professorSatisfyType}">${result.schdulNm}</c:if>
									</c:forEach>
								</c:otherwise>
							</c:choose>
			            </td>
			          </tr>
			          <tr class="">
			            <td scope='row' class='font-700'>개별 수업 만족도 조사 실시 여부 <i class='font-point font-400'>*</i></td>
			            <c:choose>
			            	<c:when test="${curriculumVO.surveyIndAt eq 'Y'}">
			            		<td>예</td>
			            	</c:when>
			            	<c:otherwise>
			            		<td class='font-red'>아니오</td>
			            	</c:otherwise>
			            </c:choose>
			            <td scope='row' class='font-700'>학생 수강신청 제출서류 여부 <i class='font-point font-400'>*</i></td>
			            <c:choose>
			            	<c:when test="${curriculumVO.stdntAplyAt eq 'Y'}">
			            		<td>예</td>
			            	</c:when>
			            	<c:otherwise>
			            		<td class='font-red'>아니오</td>
			            	</c:otherwise>
			            </c:choose>
			          </tr>
			        </tbody>
			      </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 교육과정 내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교육과정 내용</div>
                </div>
                <c:if test="${managerAt eq 'Y'}">
                	<p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
                </c:if>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정명</td>
                      <td colspan="3"><c:out value="${curriculumVO.crclNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정기간</td>
                      <td class='keep-all'><c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/></td>
                      <td scope='row' class='font-700 keep-all'>과정시간</td>
                      <td>
                      <c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
                      	총 <c:out value="${curriculumVO.totalTime}"/>시간 / 
                      	<%-- 주 <c:out value="${curriculumVO.weekNum}"/>회 --%> 
                      	<c:out value="${curriculumVO.lectureDay}"/> / 
                      	<%-- 일 <c:out value="${curriculumVO.dayTime}"/>시간 --%>
                      	<br/>
                      	<c:out value="${curriculumVO.startTime}"/> (일일 <c:out value="${curriculumVO.dayTime}"/>시간)
                      </c:if>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정진행캠퍼스</td>
                      <td><c:out value="${curriculumVO.campusNm}"/></td>
                      <td scope='row' class='font-700 keep-all'>강의실</td>
                      <td><c:out value="${curriculumVO.campusPlace}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>수업 시간표 설정</td>
                      <td colspan='3'>
                      	<c:choose>
							<c:when test="${curriculumVO.campustimeUseAt eq 'Y'}">캠퍼스 시간표</c:when>
							<c:otherwise>시간표 수기입력</c:otherwise>
						</c:choose>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>주관기관/학과</td>
                      <td colspan='3'><c:out value="${curriculumVO.hostCodeNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>책임교수</td>
                      <td colspan='3'><c:out value="${curriculumVO.userNm}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정 개요 및 목표</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정의 기대효과</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정 수강대상자</td>
                      <td colspan='3'>일반 (본교생 포함 전체)</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>과정 계획서 작성기간</td>
                      <td class='keep-all' colspan='3'><c:out value="${curriculumVO.planStartDate}"/> ~ <c:out value="${curriculumVO.planEndDate}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>수강신청기간</td>
                      <td class='keep-all' colspan='3'><c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>수강정원</td>
                      <td>
                      	<c:choose>
							<c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
							<c:otherwise>
								<c:out value="${curriculumVO.applyMaxCnt}"/>명
							</c:otherwise>
						</c:choose>
                      </td>
                      <td scope='row' class='font-700 keep-all'>주관학과 배정여부</td>
                      <td>
                      	<c:choose>
							<c:when test="${curriculumVO.assignAt eq 'Y'}">예</c:when>
							<c:otherwise>아니오</c:otherwise>
						</c:choose>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>등록금 납부대상과정 <span class='inline-block'>여부 <i class='font-point font-400'>*</i></span></td>
                      <td>
                      	<c:choose>
							<c:when test="${curriculumVO.tuitionAt eq 'Y'}">
								예(
									<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">수업료</c:if>
									<c:if test="${not empty curriculumVO.registrationFees or curriculumVO.registrationFees != 0}">
										<c:if test="${not empty curriculumVO.tuitionFees or curriculumVO.tuitionFees != 0}">,</c:if>
										등록비
									</c:if>
								)
							</c:when>
							<c:otherwise>아니오</c:otherwise>
						</c:choose>
                      </td>
                      <td scope='row' class='font-700 keep-all'>등록금액</td>
                      <td>
                      	수업료 
                      		<c:choose>
                      			<c:when test="${curriculumVO.tuitionFees eq 0}">무료</c:when>
                      			<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.tuitionFees}" />원 </c:otherwise>
                      		</c:choose>
                      	/ 
                      	등록비
                      		<c:choose>
                      			<c:when test="${curriculumVO.registrationFees eq 0}">없음</c:when>
                      			<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.registrationFees}" />원 </c:otherwise>
                      		</c:choose>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700 keep-all'>등록기간 <i class='font-point font-400'>*</i></td>
                      <td class='keep-all' colspan='3'><c:out value="${curriculumVO.tuitionStartDate}"/> ~ <c:out value="${curriculumVO.tuitionEndDate}"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            
			<c:if test="${managerAt eq 'Y'}">            
	            <article class="content-wrap">
	              <!-- 사업비 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">사업비</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm">
	                  <colgroup>
	                    <col style='width:25%'>
	                    <col style='width:25%'>
	                    <col style='width:25%'>
	                    <col style='width:25%'>
	                    <col style='width:25%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='bg-gray font-700'>
	                      <th scope='col'>지원비 비목</th>
	                      <th scope='col'>계정 목</th>
	                      <th scope='col'>금액(원)</th>
	                      <th scope='col'>산출근거</th>
	                      <th scope='col'>비고</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<c:set var="rowspan" value="0"/>
						<c:set var="rowspanList" value=""/>
						<c:set var="prevCode" value="${expense[0].typeCode}"/>
						<c:forEach var="list" items="${expense}" varStatus="status">
							<c:choose>
								<c:when test="${prevCode eq list.typeCode}">
									<c:set var="rowspan" value="${rowspan + 1}"/>
								</c:when>
								<c:otherwise>
									<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
									<c:set var="rowspan" value="1"/>
									<c:set var="prevCode" value="${list.typeCode}"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
						<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
						<c:set var="listCnt" value="${rowspan[0]}"/>
						<c:set var="rowspanCnt" value="0"/>
						
						<c:set var="prevCode" value=""/>
						
						<c:forEach var="result" items="${expense}" varStatus="status">
							<tr>
								<c:if test="${prevCode ne result.typeCode}">
									<td class="line" rowspan="${rowspan[rowspanCnt]}"><c:out value="${result.typeCodeNm}"/></td>
									<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
								</c:if>
								<c:set var="prevCode" value="${result.typeCode}"/>
								<td class="line"><c:out value="${result.accountCodeNm}"/></td>
								<td class="line"><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.amount}" />원</td>
								<td class="line"><c:out value="${result.reason}"/></td>
								<td><c:out value="${result.etc}"/></td>
							</tr>
						</c:forEach>
						<tr>
							<c:choose>
								<c:when test="${empty curriculumVO.totAmount or curriculumVO.totAmount eq '0' }"><td class="alC" colspan="5">무료</td></c:when>
								<c:otherwise>
									<td class="alC" colspan="2">계</td>
									<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.totAmount}" />원</td>
									<td></td>
									<td></td>
								</c:otherwise>
							</c:choose>
						</tr>
	                  </tbody>
	                </table>
	              </div>
	            </article>
            </c:if>
            
            <article class="content-wrap">
              <!-- 기타사항 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">기타사항</div>
                </div>
              </div>
              <div class="content-body">
                <div class="content-textarea onlyText" contentEditable="false" placeholder="기타 사항을 적어주세요.">
                    <c:choose>
						<c:when test="${empty curriculumVO.etcCn}">기타사항 없음</c:when>
						<c:otherwise><c:out value="${curriculumVO.etcCn}"/></c:otherwise>
					</c:choose>
                </div>
              </div>
            </article>
            
            <c:if test="${not empty curriculumVO.comnt}">
	            <article class="content-wrap">
	              <!-- 운영팀 코멘트 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">운영팀 코멘트 <c:if test="${!empty curriculumVO.comnt}">(<fmt:formatDate value="${curriculumVO.comntPnttm}"  pattern="yyyy-MM-dd"/>)</c:if></div>
	                </div>
	              </div>
	              <div class="content-body">
	                <div class="content-textarea onlyText" contentEditable="false" placeholder="기타 사항을 적어주세요.">
							<c:out value="${curriculumVO.comnt}"/>
	                </div>
	              </div>
	            </article>
            </c:if>
            
            <c:if test="${not empty curriculumVO.crclOutcomeNm}">
	            <article class="content-wrap">
	              <!-- 과정성과 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">과정성과</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm left-align">
	                  <colgroup>
	                    <col style='width:100%'>
	                  </colgroup>
	                  <tbody>
	                    <tr class="">
	                      <td><c:out value="${curriculumVO.crclOutcomeNm}"/></td>
	                    </tr>
	                  </tbody>
	                </table>
	              </div>
	            </article>
            </c:if>
            
            <%-- 과정계획서 작성 시작 --%>
            <c:if test="${processSttusCode > 0}">
				<article class="content-wrap">
	              <!-- 강의책임교원 배정 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">강의책임교원 배정</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm left-align">
	                  <colgroup>
	                    <col class='bg-gray' style='width:20%'>
	                    <col>
	                  </colgroup>
	                  <tbody>
	                  	<c:forEach var="user" items="${subUserList}" varStatus="status">
	                  		<tr class="">
		                      <td scope='row' class='font-700'>${status.count}</td>
		                      <td><c:out value="${user.userNm}"/>(${user.mngDeptNm})</td>
		                    </tr>
						</c:forEach>
	                  </tbody>
	                </table>
	              </div>
	            </article>
				
				<c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
					<article class="content-wrap">
		              <!-- 교원배정 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">교원배정</div>
		                </div>
		                <c:if test="${managerAt eq 'Y'}">
		                	<p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
		                </c:if>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap size-sm">
		                  <colgroup>
		                    <col style='width:33.33%'>
		                    <col style='width:33.33%'>
		                    <col style='width:33.33%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-gray font-700'>
		                      <th scope='col'>배정시수</th>
		                      <th scope='col'>실제 시간표 시수</th>
		                      <th scope='col'>비고</th>
		                    </tr>
		                  </thead>
		                  <c:set var="minTimeTot" value="0"/>
						  <c:set var="realTimeTot" value="0"/>
						  <c:forEach var="result" items="${facList}" varStatus="status">
							<tbody class='box_staff'>
								<tr>
									<td class="num"><c:out value="${result.sisu}"/></td>
									<td class="realNum">
										<c:set var="realNum" value="0"/>
										<c:forEach var="facPl" items="${facPlList}">
											<c:if test="${facPl.facId eq result.facId}">
												<c:set var="realNum" value="${realNum + facPl.sisu}"/>
											</c:if>
										</c:forEach>
										<c:out value="${realNum}"/>
										<c:set var="realTimeTot" value="${realTimeTot + realNum}"/>
										<c:set var="minTime" value="${result.sisu - realNum}"/>
										<c:set var="minTimeTot" value="${minTimeTot + minTime}"/>
										<c:if test="${minTime > 0}">
											(<span class="font-red minTime"><c:out value="${minTime}"/>시간 부족</span>)
										</c:if>
									</td>
									<td><c:out value="${result.facNm}"/></td>
								</tr>
							</tbody>
							<c:set var="curTime" value="${curTime + result.sisu}"/>
						  </c:forEach>
						  <tfoot id="box_addTime">
							<tr class="box_sum bg-point-light font-700">
								<td><span class="curTime">${curTime}</span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span></td>
								<td>
									<span class="realTime"><c:out value="${realTimeTot}"/></span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span>
									<%-- 시간표 작업 되면 해당 부분 작업해야됨 --%>
									<c:set var="lackTime" value="${minTimeTot}"/>
									<c:if test="${lackTime > 0}">
										(<span class="font-red">${lackTime}시간 부족</span>)
									</c:if>
								</td>
								<td colspan="3">
									교원 <span class="sumUser">0</span>명
									<c:forEach var="result" items="${facIdList}" varStatus="status">
										<input type="hidden" class="facIdList" name="facIdList" value="<c:out value="${result}"/>">
									</c:forEach>
								</td>
							</tr>
						  </tfoot>
		                </table>
		              </div>
		            </article>
	            </c:if>
	            
				<article class="content-wrap">
	              <!-- 과정내용(단원) 관리 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">과정내용</div>
	                </div>
	                <c:if test="${managerAt eq 'Y'}">
	                	<p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
	                </c:if>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm left-align">
	                  <colgroup>
	                    <col style='width:7%'>
	                    <col style='width:50%'>
	                    <col style='width:50%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='center-align bg-gray font-700'>
	                      <th scope='col'>No</th>
	                      <th scope='col'>내용범주</th>
	                      <th scope='col'>내용</th>
	                    </tr>
	                  </thead>
	                  
	                  	<%-- 과정내용 번호 및 단원 묶어주는 역할 --%>	
					<%-- 	<c:set var="rowspan" value="0"/>
						<c:set var="rowspanList" value=""/>
						<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
						<c:forEach var="list" items="${lessonList}" varStatus="status">
							<c:choose>
								<c:when test="${prevCode eq list.lessonNm}">
									<c:set var="rowspan" value="${rowspan + 1}"/>
								</c:when>
								<c:otherwise>
									<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
									<c:set var="rowspan" value="1"/>
									<c:set var="prevCode" value="${list.lessonNm}"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
						<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
						<c:set var="listCnt" value="${rowspan[0]}"/>
						<c:set var="rowspanCnt" value="0"/>
						
						<c:set var="prevCode" value=""/>
						<c:set var="number" value="1"/>
						
						<c:forEach var="result" items="${lessonList}" varStatus="status">
							<c:if test="${prevCode ne result.lessonNm}">
								<tbody class='box_lesson'>
							</c:if>
							<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
								<c:if test="${prevCode ne result.lessonNm}">
									<td class="center-align num" rowspan="${rowspan[rowspanCnt] + 1}">
										<c:out value="${number}"/>
										<c:set var="number" value="${number + 1}"/>
									</td>
									<td rowspan='${rowspan[rowspanCnt] + 1}' class='lesnm'><c:out value="${result.lessonNm}"/></td>
									<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
								</c:if>
								<td><c:out value="${result.chasiNm}"/></td>
							</tr>
							<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
								</tbody>
							</c:if>
							
							<c:set var="prevCode" value="${result.lessonNm}"/>
					  </c:forEach> --%>
					  <tfoot id="box_addLes"></tfoot>
	                </table>
	              </div>
	            </article>
            	
            	<article class="content-wrap">
	              <!-- 성적 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">성적</div>
	                </div>
	                <%-- 
	                <p class="notice font-700">
	                	<c:choose>
							<c:when test="${curriculumVO.evaluationAt eq 'Y' and not empty curriculumVO.gradeType}">
								※ 절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
							</c:when>
							<c:otherwise>※ 상대평가</c:otherwise>
						</c:choose>
	                </p>
	                 --%>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm p-0">
	                	<c:choose>
							<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
								<colgroup>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                    <col style='width:10%'>
				                  </colgroup>
				                  <thead>
				                    <tr class='bg-gray font-700'>
				                      <th scope='col'>Grade</th>
				                      <c:forEach var="result" items="${gradeTypeList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
											<th scope='col'><c:out value="${result.ctgryNm}"/></th>
										</c:if>
									</c:forEach>
				                    </tr>
				                  </thead>
				                  <tbody>
				                    <tr class="font-700">
				                    	<c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
										<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
										<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
										<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
										<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
										<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
										<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
										<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
										<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
										<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
										<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
									
				                      	<td scope='row'>점수</td>
					                    <td scope='row'>${aplus}</td>
										<td scope='row'>${a}</td>
										<td scope='row'>${bplus}</td>
										<td scope='row'>${b}</td>
										<td scope='row'>${cplus}</td>
										<td scope='row'>${c}</td>
										<td scope='row'>${dplus}</td>
										<td scope='row'>${d}</td>
										<td scope='row'>${f}</td>
				                    </tr>
				                  </tbody>
							</c:when>
							<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
								<tr>
									<th class="alC">P/F</th>
									<th class="alC" colspan="2">점수구간</th>
								</tr>
								<tr class="alC">
									<td class="line">Pass</td>
									<td class="alC">${curriculumVO.pass}</td>
									<td class="alC">~ 100</td>
								</tr>
								<tr class="alC">
									<td class="line">Fail</td>
									<td class="alC">${curriculumVO.fail}</td>
									<td class="alC">~ 0</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<th class="alC">Grade</th>
									<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
											<th class="alC"><c:out value="${result.ctgryNm}"/></th>
										</c:if>
									</c:forEach>
								</tr>
								<tr>
									<td class="line alC">비중</td>
									<td colspan="2" class="line alC">상위 30%</td>
									<td colspan="2" class="line alC">상위 65%이내</td>
									<td colspan="5" class="line alC">66% ~ 100%</td>
								</tr>
								<%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
								<tr>
									<td class="line alC">가이드 학생 수</td>
									<td colspan="2" class="line alC">30</td>
									<td colspan="2" class="line alC">35</td>
									<td colspan="5" class="line alC">35</td>
								</tr>
							</c:otherwise>
						</c:choose>
	                </table>
	              </div>
	            </article>
            	
            	<article class="content-wrap">
	              <!-- 총괄평가 기준 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">총괄평가 기준</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm">
	                  <colgroup>
	                    <col style='width:50%'>
	                    <col style='width:50%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='bg-gray font-700'>
	                      <th scope='col'>총괄평가 기준</th>
	                      <th scope='col'>성적반영율(%)</th>
	                    </tr>
	                  </thead>
	                  <tbody class='box_evt'>
						<%-- 총괄평가 등록되어 있는지 확인 및 반영률 체크--%>
						<c:set var="totper" value="0"/>
						<c:choose>
							<c:when test="${not empty evaluationList or fn:length(evaluationList) > 0}">
								<c:forEach var="result" items="${evaluationList}" varStatus="status">
									<tr class="<c:out value="${result.evtId}"/>">
										<td scope='row' <c:if test="${result.evtId eq 'CTG_0000000000000117'}">rowspan="2"</c:if>><c:out value="${result.evtNm}"/></td>
										<td><c:out value="${result.evtVal}"/>%</td>
									</tr>
									
									<%-- 수업참여게시판 --%>
									<c:if test="${result.evtId eq 'CTG_0000000000000117'}">
										<tr>
											<td>
												<c:forEach var="bbsList" items="${attendBbsList}" varStatus="sts">
													<c:if test="${sts.count > 1}"><br/></c:if>
														(
														<c:choose>
															<c:when test="${bbsList.sysTyCode eq 'ALL'}">전체</c:when>
															<c:when test="${bbsList.sysTyCode eq 'GROUP'}">조별</c:when>
															<c:when test="${bbsList.sysTyCode eq 'CLASS'}">반별</c:when>
														</c:choose>
														)
														<c:out value="${bbsList.bbsNm}"/>
														/
														<c:out value="${bbsList.colect}"/>회 이상
												</c:forEach>
											</td>
										</tr>
									</c:if>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr><td colspan="3" class="alC">총괄평가 기준 데이터가 없습니다.</td></tr>
							</c:otherwise>
						</c:choose>
					  </tbody>
	                </table>
	              </div>
	            </article>
            	
				<article class="content-wrap">
	              <!-- 수료기준 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">수료기준</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm">
	                  <colgroup>
	                    <col style='width:29%'>
	                    <col style='width:29%'>
	                    <col style='width:42%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='bg-gray font-700'>
	                      <th scope='col'>수료기준</th>
	                      <th scope='col'>상세 내용(점수, %)</th>
	                      <th scope='col'>설명</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<c:forEach var="result" items="${finishList}">
							<c:if test="${result.ctgryLevel eq 1}">
								<tr <c:if test="${result.ctgryId eq 'CTG_0000000000000084' or result.ctgryId eq 'CTG_0000000000000087' or result.ctgryId eq 'CTG_0000000000000088'}">class="bg-light-gray"</c:if>>
									<td><c:out value="${result.ctgryNm}"/></td>
									<td>
										<c:choose>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000084'}">
												<c:choose>
													<c:when test="${empty curriculumVO.fnTot}">해당없음</c:when>
													<c:otherwise><strong>대상</strong> - ${curriculumVO.fnTot}점 이상</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000085'}">
												<c:choose>
													<c:when test="${empty curriculumVO.fnAttend}">해당없음</c:when>
													<c:otherwise><strong>대상</strong> - ${curriculumVO.fnAttend}% 이상</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000086'}">
												<c:choose>
													<c:when test="${empty curriculumVO.fnGradeTot}">해당없음</c:when>
													<c:otherwise><strong>대상</strong> - ${curriculumVO.fnGradeTot}점 이상</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000087'}">
												<c:choose>
													<c:when test="${empty curriculumVO.fnGradeFail}">해당없음</c:when>
													<c:otherwise><strong>대상</strong> - ${curriculumVO.fnGradeFail}점 이상</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000088'}">
												<c:choose>
													<c:when test="${empty curriculumVO.fnHomework}">해당없음</c:when>
													<c:otherwise><strong>대상</strong> - ${curriculumVO.fnHomework}점 이상</c:otherwise>
												</c:choose>
											</c:when>
										</c:choose>
									</td>
									<td class='left-align'><c:out value="${result.ctgryCn}"/></td>
								</tr>
							</c:if>
						</c:forEach>
	                  </tbody>
	                </table>
	              </div>
	            </article>
	            
	            <article class="content-wrap">
	              <!-- 교재 및 부교재 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">교재 및 부교재</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <!-- 테이블영역-->
	                <table class="common-table-wrap size-sm mb-20">
	                  <colgroup>
	                    <col style='width:95px'>
	                    <col style='width:50%'>
	                    <col style='width:50%'>
	                  </colgroup>
	                  <thead>
	                    <tr class='center-align bg-gray font-700'>
	                      <th scope='col'>No</th>
	                      <th scope='col'>교재명</th>
	                      <th scope='col'>출판사</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<c:forEach var="result" items="${bookList}" varStatus="status">
							<tr>
								<td class="num center-align"><c:out value="${status.count}"/></td>
								<td><c:out value="${result.nttSj}"/></td>
								<td><c:out value="${result.tmp01}"/></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(bookList) == 0}">
							<tr><td colspan="3" class="center-align">등록된 교재 및 부교재가 없습니다.</td></tr>
						</c:if>
	                  </tbody>
	                </table>
	                <div></div>
	                <div class="content-textarea onlyText" contentEditable="false" placeholder="내용이 없습니다.">
	                  	<c:out value="${fn:replace(curriculumVO.bookEtcText, LF, '<br>')}" escapeXml="false"/>
	                </div>
	              </div>
	            </article>
	            
	            <article class="content-wrap">
	              <!-- 자체 운영 계획 및 규정 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">자체 운영 계획 및 규정</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <div class="content-textarea onlyText" contentEditable="false" placeholder="내용이 없습니다.">
	                  	<c:out value="${fn:replace(curriculumVO.managePlan, LF, '<br>')}" escapeXml="false"/>
	                </div>
	              </div>
	            </article>
			</c:if>
			
<c:if test="${param.cnAt ne 'Y' and param.step ne '1' and param.printAt ne 'Y'}">

            <c:choose>
            	<c:when test="${managerAt eq 'N'}">
            		<div class="page-btn-wrap mt-50">
		            	<c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
		              	<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
		            </div>
            	</c:when>
            	<c:when test="${curriculumVO.aprvalAt eq 'R'}">
            		<p class="page-guide-desc mt-50">
		              	※ 신청서가 운영팀에 접수되었습니다. (검토기간 : 2주정도가 소요됩니다.)
		            </p>
		            <div class="page-btn-wrap mt-50">
		            	<c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
		              	<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
		            </div>
            	</c:when>
            	<c:when test="${curriculumVO.aprvalAt eq 'T'}">
            		<p class="page-guide-desc mt-50">
		              	※ 신청서를 확정하여 등록하면 더 이상 수정 불가하며 운영팀에서 신청서를 검토합니다.
		            </p>
		            <div class="page-btn-wrap mt-50">
		            	<c:url var="uptUrl" value="/lms/crcl/updateCurriculumView.do${_BASE_PARAM}"/>
			            <a href="${uptUrl}" class="btn-xl btn-outline-gray">수정</a>
			            <c:url var="uptUrl" value="/lms/crcl/updateCurriculum.do${_BASE_PARAM}">
			            	<c:param name="aprvalAt" value="R"/>
			            </c:url>
			            <a href="${uptUrl}" class="btn-xl btn-point">확인 및 등록</a>
		            </div>
            	</c:when>
            	<c:when test="${curriculumVO.aprvalAt eq 'N'}">
            		<p class="page-guide-desc mt-50">
		              	※ 신청서가 반려되었습니다. 운영팀 코멘트를 확인해 주세요.
		            </p>
            		<div class="page-btn-wrap mt-50">
		            	<c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
		              	<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
		            </div>
            	</c:when>
            	<c:when test="${curriculumVO.processSttusCodeDate > 7}">
            		<div class="page-btn-wrap mt-50">
	            		<c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
					    <a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
				    </div>
            	</c:when>
            	<c:otherwise>
            		<c:if test="${curriculumVO.aprvalAt eq 'Y'}">
            			<p class="page-guide-desc mt-50">
			              	<strong>교육과정 개설 신청이 승인되었습니다.</strong>
			              	<c:if test="${curriculumVO.processSttusCodeDate < 3 }">
				              	<br/><br/>
				              	<c:out value="${curriculumVO.planEndDate}"/> 일까지 추가 정보 입력을 완료하여 주시기 바랍니다.<br/><br/>
				              	[추가 정보]<br/>
				              	- 강의 인력 및 시수<br/>
				              	- 교육내용 및 단원<br/>
				              	- 과정 평가 및 수료 기준<br/>
				              	- 기타 과정 운영관련 주요 사항
			              	</c:if>
			            </p>
            		</c:if>
		            <div class="page-btn-wrap mt-50">
		            	<%-- 
			            <c:if test="${curriculumVO.processSttusCode eq '0'}">
							<c:url var="confirmUrl" value="/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}">
								<c:param name="crclNm" value="${curriculumVO.crclNm}" />
								<c:param name="processSttusCode" value="1" />
							</c:url>
						    <a href="${confirmUrl}" class="btn-xl btn-point btn_confirm">확정등록</a>
					    </c:if>
					     --%>
						<c:url var="uptUrl" value="/lms/crcl/updateCurriculumView.do${_BASE_PARAM}"/>
			            <a href="${uptUrl}" class="btn-xl btn-point">필수정보 추가 입력</a>
					    
					    <c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
					    <a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
		            </div>
            	</c:otherwise>
            </c:choose>
      </section>
</div>
</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>
</c:if>
<c:if test="${param.printAt eq 'Y'}">
</body>
</html>
</c:if>