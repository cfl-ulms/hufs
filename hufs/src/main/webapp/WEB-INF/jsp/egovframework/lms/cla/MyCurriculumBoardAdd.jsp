<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/cla"/>
<c:set var="_ACTION" value=""/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_EDITOR_STAFF" value="staff"/>

<c:choose>
	<c:when test="${searchVO.registAction eq 'regist' }">
		<c:set var="_ACTION" value="${_PREFIX}/insertBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'updt' }">
		<c:set var="_ACTION" value="${_PREFIX}/updateBoardArticle.do"/>
	</c:when>
	<c:when test="${searchVO.registAction eq 'reply' }">
		<c:set var="_ACTION" value="${_PREFIX}/replyBoardArticle.do"/>
	</c:when>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId}" />
	<c:param name="plId" value="${param.plId}" />
	<c:param name="bbsId" value="${param.bbsId}" />
	<c:if test="${not empty param.menu}"><c:param name="menu" value="${param.menu}" /></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}" /></c:if>
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty param.depth1}"><c:param name="depth1" value="${param.depth1}" /></c:if>
</c:url>
<% /*URL 정의*/ %>


<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>
<style>
ul#chkGroup {
    list-style:none;
    margin:0;
    padding:0;
}

ul#chkGroup li {
    margin: 0 10px 0 0;
    padding: 0 0 0 0;
    border : 0;
    float: left;
}

</style>

<script type="text/javascript">
	function fn_egov_regist() {
		//$("#openScope").val($("#openScope").val().substring(1,$("#openScope").val().length));

		tinyMCE.triggerSave();

		$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

		<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
			for(var cmIdx = 1 ; cmIdx <= boardCateLevel ; cmIdx++){
				var cmObj = document.getElementById("ctgry" + cmIdx);
				if(cmObj != null) {
					if(fn_egov_SelectBoxValue("ctgry" + cmIdx) != '') {
						document.board.ctgryId.value = fn_egov_SelectBoxValue("ctgry" + cmIdx);
					}
				}
			}
	    </c:if>

	    <c:choose>
	    	<c:when test="${searchVO.registAction eq 'updt'}">
				if (!confirm('<spring:message code="common.update.msg" />')) {
					 return false
				}
			</c:when>
			<c:otherwise>
				if (!confirm('<spring:message code="common.regist.msg" />')) {
					return false;
				}
			</c:otherwise>
		</c:choose>


		$('#board').submit();
	}

	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">

		var boardCateLevel = ${boardCateLevel};
		var boardCateList = new Array(${fn:length(boardCateList)});
		<c:forEach var="cate" items="${boardCateList}" varStatus="status">
			boardCateList[${status.index}] = new ctgryObj('${cate.upperCtgryId}', '${cate.ctgryId}', '${cate.ctgryNm}', ${cate.ctgryLevel});
		</c:forEach>
	</c:if>


	$(document).ready(function(){

		var adfile_config = {
				siteId:"<c:out value='${brdMstrVO.siteId}'/>",
				pathKey:"Board",
				appendPath:"<c:out value='${brdMstrVO.bbsId}'/>",
				editorId:"${_EDITOR_ID}",
				fileAtchPosblAt:"${brdMstrVO.fileAtchPosblAt}",
				maxMegaFileSize:1024,
				atchFileId:"${board.atchFileId}"
			};

		fnCtgryInit('<c:out value='${board.ctgryPathById}'/>');
		fn_egov_bbs_editor(adfile_config);

		$("#egovComFileUploader").change(function(){
			var fileVal = $(this).val(),
				ext = fileVal.split("."),
				splitSize = ext.length - 1,
				fileExt = ext[splitSize].toLowerCase();


			if($("#box_images > li").length > 0){
				alert("기존 파일을 삭제 후 등록해주세요.")
				$(this).val("");
			}else if(fileExt=="bmp" || fileExt=="gif" || fileExt=="jpeg" || fileExt=="jpg" || fileExt=="png" || fileExt=="BMP" || fileExt=="GIF" || fileExt=="JPEG" || fileExt=="JPG" || fileExt=="PNG"){

			}else{
				alert("이미지 확장자만 저장할 수 있습니다.");
				$(this).val("");
			}
		});

});
</script>
<input type="hidden" id="staffPage" value="staff"/>
          <div class="page-content-header">
            <c:choose>
          		<%-- <c:when test="${param.menuId eq 'MNU_0000000000000094' }"> --%>
          		<c:when test="${empty param.plId}">
          			<c:import url="/lms/crclHeader.do" charEncoding="utf-8"/>
          		</c:when>
          		<c:otherwise>
          			<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
          		</c:otherwise>
          	</c:choose>
			<!--
            <div class="util-wrap">
              <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
            </div>
             -->
          </div>
          <section class="page-content-body">
          	<c:choose>
                <c:when test="${!empty param.plId }">
                 <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                    <c:param name="step" value="7"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="menuId" value="${searchVO.menuId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                 </c:import>
                </c:when>
                <c:otherwise>
                  <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                  <c:param name="menu" value="${param.menu }"/>
                  <c:param name="subtabstep" value="${param.subtabstep }"/>
                  <c:param name="menuId" value="${param.menuId }"/>
                  <c:param name="crclId" value="${param.crclId}"/>
                  <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt }"/>
                  <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
              </c:import>
                </c:otherwise>
            </c:choose>
			<article class="content-wrap">
              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
	                  <c:forEach var="result" items="${masterList}">
						<c:url var="masterUrl" value="/lms/cla/curriculumBoardList.do">
							<c:param name="crclId" value="${searchVO.crclId}"/>
							<c:param name="bbsId" value="${result.bbsId}" />
							<c:param name="menuId" value="${param.menuId}" />
							<c:param name="plId" value="${param.plId}" />
						</c:url>
						<li class="${result.bbsId eq brdMstrVO.bbsId ? 'active' : ''}" id="${result.bbsId }">
							<a href="${masterUrl}" class="link">
								<c:choose>
									<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
									<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
								</c:choose>
								<c:out value="${result.bbsNm}"/>
							</a>
						</li>
					</c:forEach>
                </ul>
                </div>
                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
            </article>

		 <article class="content-wrap">
			<form:form commandName="board" name="board" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return">
	        <input type="hidden" name="pageIndex"  value="<c:out value='${searchVO.pageIndex}'/>"/>
			<input type="hidden" id="posblAtchFileNumber_${_EDITOR_ID}" name="posblAtchFileNumber_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileNumber}" />
		    <input type="hidden" id="posblAtchFileSize_${_EDITOR_ID}" name="posblAtchFileSize_${_EDITOR_ID}" value="${brdMstrVO.posblAtchFileSize * 1024 * 1024}" />
		    <input type="hidden" id="fileGroupId" name="fileGroupId" value="${board.atchFileId}"/>
			<input type="hidden" name="bbsId" value="<c:out value='${searchVO.bbsId}'/>" />
			<input type="hidden" name="crclId" value="<c:out value='${searchVO.crclId}'/>" />
			<input type="hidden" name="menuId"  value="<c:out value='${searchVO.menuId}'/>" />
			<input type="hidden" name="registAction" value="<c:out value='${searchVO.registAction}'/>"/>
			<input type="hidden" name="tmplatImportAt" value="<c:out value='${searchVO.tmplatImportAt}'/>"/>
			<input type="hidden" name="plId" value="<c:out value='${param.plId}'/>"/>
	        <input type="hidden" name="nttNo" value="<c:out value='${board.nttNo}'/>"/>
	        <input type="hidden" name="atchFileId" value="<c:out value='${board.atchFileId}'/>"/>
	        <input type="hidden" name="menu" value="${param.menu}"/>
	        <input type="hidden" name="subtabstep" value="${param.subtabstep}"/>

	        <c:choose>
	        	<c:when test="${brdMstrVO.sysTyCode eq 'ALL'}">
					<input type="hidden" id="openScope" name="openScope" value="T"/>
	        	</c:when>
	        	<c:otherwise>
	        		<input type="hidden" id="openScope" name="openScope" value="G"/>
	        		<input type="hidden" id="openGroup" name="openGroup" value="${brdMstrVO.sysTyCode eq 'CLASS' ?  userInfo.classCnt : userInfo.groupCnt }"/>
	        	</c:otherwise>

	        </c:choose>

	        <c:if test="${param.depth1 eq 'CRCL_BOARD' }">
	        	<input type="hidden"  name="depth1" value="CRCL_BOARD"/>
	        </c:if>

              <table class="common-table-wrap table-style2 mb-20">
                <tbody>
	                <c:choose>
						<c:when test="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply'}">
							<input type="hidden" name="nttSj" value="dummy"/>
							<tr>
								<th><spring:message code="cop.processSttus" /></th>
								<td>
									<select name="processSttusCode" class="select">
										<c:forEach var="resultState" items="${qaCodeList}" varStatus="status">
											<option value='<c:out value="${resultState.code}"/>' <c:if test="${board.processSttusCode eq resultState.code}">selected="selected"</c:if>><c:out value="${resultState.codeNm}"/></option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
		                  <tr>
		                    <th class="title">제목 </th>
		                    <td>
		                      <div class="flex-row">
		                        <div class="flex-col-12">
		                          <input type="text" id="nttSj" name="nttSj" class="table-input"  placeholder="제목을 입력해주세요." value="${board.nttSj}">
		                        </div>
		                      </div>
		                    </td>
		                  </tr>

                  	<c:if test="${!empty brdMstrVO.ctgrymasterId and searchVO.registAction ne 'reply'}">
                  <tr>
                    <th class="title">게시글 구분 </th>
                    <td>
                    	<div class="flex-row">
		                    <div class="flex-col-2">
			                    <c:forEach var="ctgryLevel" begin="1" end="${boardCateLevel}" step="1" varStatus="status">
									<c:choose>
										<c:when test="${status.first}">
											<select name="ctgryId" id="ctgry${ctgryLevel}" class="table-select select2 " data-select="style1" data-placeholder="게시글 구분">
												<option value=""><spring:message code="cop.select" /></option>
												<c:forEach var="cate" items="${boardCateList}">
													<c:if test="${cate.ctgryLevel eq 1 }">
														<option value="${cate.ctgryId}">${cate.ctgryNm}</option>
													</c:if>
												</c:forEach>
											</select>
										</c:when>
										<c:otherwise><select name="regCateList" id="ctgry${ctgryLevel}" onchange="fnCtgryChange(${ctgryLevel})"><option value=""><spring:message code="cop.select" /></option></select></c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</div>
						<script type="text/javascript">
							fnCtgryInit('${board.ctgryPathById}');
						</script>
                    </td>
                   </tr>
                  </c:if>
                  </c:otherwise>
                  </c:choose>
                </tbody>
              </table>


              	<div>
              		<textarea id="nttCn" style="width:99%" name="nttCn" rows="30" ><c:out value="${board.nttCn}" escapeXml="false"/></textarea>
              	</div>


	              <!-- 첨부파일 -->
	              <c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
						<div class="mt-20">
							<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
								<c:param name="editorId" value="${_EDITOR_ID}"/>
								<c:param name="estnAt" value="${brdMstrVO.bbsAttrbCode eq 'BBSA11' and searchVO.registAction eq 'reply' ? 'Y' : 'N'}" />
						    	<c:param name="param_atchFileId" value="${board.atchFileId}" />
						    	<c:param name="imagePath" value="${_IMG }"/>
						    	<c:param name="mngAt" value="Y"/>
						    	<c:param name="viewDivision" value="staff"/>
							</c:import>
							<noscript>
								<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
							</noscript>
						</div>
					</c:if>
              </form:form>
            </article>

             <div class="page-btn-wrap mt-50">
				<c:url var="listUrl" value="/lms/cla/curriculumBoardList.do${_BASE_PARAM}"/>
              <a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
              <a href="#none" class="btn-xl btn-point" onclick="fn_egov_regist();">${searchVO.registAction eq 'regist' ? '등록하기' : searchVO.registAction eq 'updt' ? '수정하기' : '답변하기' }</a>
            </div>
          </section>
</div>

