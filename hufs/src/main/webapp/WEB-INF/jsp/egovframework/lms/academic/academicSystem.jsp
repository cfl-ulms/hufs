<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

  <!--=================================================
        페이지별 스타일시트
  ==================================================-->
  <link rel="stylesheet" href="/template/lms/css/common/table_staff.css?v=2">

<script type="text/javaScript" language="javascript" defer="defer">
	
	function fn_egov_delete(url){
		if(confirm('<spring:message code="common.delete.msg" />')){
			document.location.href = url;	
		}		
	}
</script>

          <!-- 테이블영역-->
          <table class="common-table-wrap ">
            <colgroup>
              <col style='width:33.3%'>
              <col style='width:33.3%'>
              <col style='width:*'>
            </colgroup>
            <thead>
              <tr class='bg-light-gray font-700'>
                <th scope='col'>대분류</th>
                <th scope='col'>중분류</th>
                <th scope='col'>소분류</th>
              </tr>
            </thead>
            <tbody>
            	<c:set var="rowspan1" value="0"/>
            	<c:set var="rowspan2" value="0"/>
				<c:set var="rowspan1List" value=""/>
				<c:set var="rowspan2List" value=""/>
            	<c:forEach var="result" items="${resultList}" varStatus="status">
            		<c:choose>
				    	<c:when test="${empty result.upperCtgryId}">
				    		<c:set var="_ROOT_ID" value="${result.ctgryId}"/>
				    	</c:when>
				    	<c:when test="${result.ctgryLevel eq 3}">
				    		<c:set var="ctgryNmSplit" value="${fn:split(result.ctgryPathByName,'>')}"/>
				    		<c:if test="${prevNm1 ne ctgryNmSplit[1]}">
				    			<c:set var="rowspan1List" value="${rowspan1List},${rowspan1}"/>
				    			<c:set var="rowspan1" value="0"/>
				    		</c:if>
				    		<c:if test="${prevNm2 ne ctgryNmSplit[2]}">
				    			<c:set var="rowspan2List" value="${rowspan2List},${rowspan2}"/>
				    			<c:set var="rowspan2" value="0"/>
				    		</c:if>
				    		
							<c:set var="prevNm1" value="${ctgryNmSplit[1]}"/>
	    					<c:set var="prevNm2" value="${ctgryNmSplit[2]}"/>
	    					<c:set var="rowspan1" value="${rowspan1 + 1}"/>
            				<c:set var="rowspan2" value="${rowspan2 + 1}"/>
						</c:when>
					</c:choose>
					<c:if test="${status.last}">
						<c:set var="rowspan1List" value="${rowspan1List},${rowspan1}"/>
						<c:set var="rowspan2List" value="${rowspan2List},${rowspan2}"/>
					</c:if>
            	</c:forEach>
            	
            	<c:set var="prevNm1" value=""/>
	    		<c:set var="prevNm2" value=""/>
	    		<c:set var="rowspan1Split" value="${fn:split(rowspan1List,',')}"/>
	    		<c:set var="rowspan2Split" value="${fn:split(rowspan2List,',')}"/>
	    		<c:set var="rowspan1Cnt" value="1"/>
	    		<c:set var="rowspan2Cnt" value="1"/>
            	<c:forEach var="result" items="${resultList}" varStatus="status">
					<c:choose>
				    	<c:when test="${empty result.upperCtgryId}">
				    		<c:set var="_ROOT_ID" value="${result.ctgryId}"/>
				    	</c:when>
				    	<c:when test="${result.ctgryLevel eq 3}">
				    		<c:set var="ctgryNmSplit" value="${fn:split(result.ctgryPathByName,'>')}"/>
							<tr>
								<c:if test="${prevNm1 ne ctgryNmSplit[1]}">
									<td rowspan="${rowspan1Split[rowspan1Cnt]}">
										<c:out value="${ctgryNmSplit[1]}"/>
										<c:set var="rowspan1Cnt" value="${rowspan1Cnt + 1}"/>
									</td>
								</c:if>
								<c:if test="${prevNm2 ne ctgryNmSplit[2]}">
									<td rowspan="${rowspan2Split[rowspan2Cnt]}">
										<c:out value="${ctgryNmSplit[2]}"/>
										<c:set var="rowspan2Cnt" value="${rowspan2Cnt + 1}"/>
									</td>
								</c:if>
								<td>
						        	<c:choose>
						        		<c:when test="${not empty result.upperCtgryId}"><c:out value="${result.ctgryNm}"/></c:when>
						        		<c:otherwise><c:out value="${param.ctgrymasterNm}"/></c:otherwise>
						        	</c:choose>
								</td>
							</tr>
							<c:set var="prevNm1" value="${ctgryNmSplit[1]}"/>
	    					<c:set var="prevNm2" value="${ctgryNmSplit[2]}"/>
						</c:when>
					</c:choose>
				</c:forEach>
            </tbody>
          </table>
        </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>