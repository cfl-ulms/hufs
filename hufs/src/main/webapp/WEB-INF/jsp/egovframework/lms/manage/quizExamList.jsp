<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
  
<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
	$(document).ready(function(){
		$('#btnInit').click(function(){
			$("#initForm").attr("action", "/lms/quiz/QuizExamList.do${_BASE_PARAM}");
			$("#initForm").submit();
		});
		
		$('#btnSearch').click(function(){
			$("#detailForm").attr("action", "/lms/quiz/QuizExamList.do${_BASE_PARAM}");
			$("#detailForm").submit();
		});
		
		$('#btnSelMov').click(function(){
			var radioVal = $('input[name="plidList"]:checked').val();
			if(typeof radioVal == "undefined" || radioVal == null || radioVal == ""){
				alert("퀴즈를 선택해 주십시오.");
				return false;
			}
			var param = radioVal.split(",");
			$("#plId").val(param[0]);
			$("#crclId").val(param[1]);
			$("#quizType").val(param[2]);
			$("#detailForm").attr("action", "/lms/quiz/QuizCopy.do${_BASE_PARAM}"+"&plIdTarget="+param[0]+"&quizType="+param[2]);
			$("#detailForm").submit();
		});
	});
</script>
<div class="expansion-panel">
        <!-- 메인타이틀 -->
        <div class="page-content-wrap pt-0">
          <section class="page-content-body">
            <article class="content-wrap">
            	<form:form commandName="quizVO" name="initForm" id="initForm" method="post" />
            	<form:form commandName="quizVO" name="detailForm" id="detailForm" method="post">
		              <div class="box-wrap mb-40">
		                <h3 class="title-subhead">온라인 퀴즈 검색</h3>
		                <div class="flex-row-ten">
		                  <div class="flex-ten-col-5 mb-20">
		                    <div class="desc">
		                      <input type="text" name="searchStartDt" value="${searchVO.searchStartDt}" class="ell date datepicker type2" placeholder="시작일">
		                      <i>~</i>
		                      <input type="text" name="searchEndDt" value="${searchVO.searchEndDt}" class="ell date datepicker type2" placeholder="종료일">
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-5 flex align-items-center mb-20">
		                    <label class="checkbox">
		                      <input type="checkbox" name="quizEnd" id="quizEnd" value="Y" <c:if test="${searchVO.quizEnd eq 'Y'}">checked="checked"</c:if> >
		                      <span class="custom-checked"></span>
		                      <span class="text">종료된 온라인 퀴즈만 조회</span>
		                    </label>
		                  </div>
		                  <div class="flex-ten-col-4">
		                    <div class="ell">
		                      <input type="text" name="crclNm" placeholder="과정명" value="${searchVO.crclNm }">
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-6">
		                    <div class="ell">
		                      <input type="text" name="searchQuizNm" placeholder="온라인 퀴즈 명(수업 명)" value="${searchVO.searchQuizNm }">
		                    </div>
		                  </div>
		                </div>
		                <button id="btnSearch" class="btn-sm font-400 btn-point mt-20">검색</button>
		                <a href="/lms/quiz/QuizExamList.do?menuId=MNU_0000000000000090 "><button class="btn-sm font-400 btn-outline mt-20" type="button">초기화</button></a>
		              </div>
              </form:form>
            </article>
            <article class="content-wrap">
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    <col style="width:7%">
                    <col style="width:100px">
                    <%-- <col style="width:14%"> --%>
                    <col style="width:21%">
                    <col style="width:28%">
                    <col style="width:11%">
                    <col style="width:9%">
                  </colgroup>
                  <thead>
                    <tr class="bg-light-gray font-700">
                      <th scope="col">No</th>
                      <th scope="col">수업일</th>
                      <!-- <th scope="col">평가구분</th> -->
                      <th scope="col">과정명</th>
                      <th scope="col">온라인 퀴즈 명(수업 명)</th>
                      <th scope="col">교원</th>
                      <th scope="col">
                      	<c:choose>
                      		<c:when test="${USER_INFO.userSe eq '08'}">평균점</c:when>
                      		<c:otherwise>내 점수</c:otherwise>
                      	</c:choose>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="result" items ="${beforeQuizList}" varStatus="status">
                  		<c:choose>
							<c:when test="${result.quizEnd eq 'Y'}">
								<c:url var="viewUrl" value="/lms/quiz/QuiResultList.do${_BASE_PARAM}">
									<c:param name="crclId" value="${result.crclId}"/>
									<c:param name="plId" value="${result.plId}"/>
									<c:param name="quizType" value="${result.quizType}"/>
									<c:param name="tabType" value="T"/>
									<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
								</c:url>
							</c:when>
							<c:otherwise>
								<c:url var="viewUrl" value="/lms/quiz/QuizEvalList.do">
									<c:param name="menuId" value="${searchVO.menuId}"/>
									<c:param name="crclId" value="${result.crclId}"/>
									<c:param name="plId" value="${result.plId}"/>
									<c:param name="step" value="4"/>
									<c:param name="tabType" value="T"/>
									<c:param name="thirdtabstep" value="1"/>
								</c:url>
							</c:otherwise>
						</c:choose>
						<tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
	                      <td scope="row"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	                      <td>${result.plStartDt }</td>
	                      <!-- <td>온라인</td> -->
	                      <td class="left-align">${result.crclNm}</td>
	                      <td class="left-align">${result.studySubject}
	                     	  <c:choose>
	                      	  	<c:when test="${result.quizType eq 'ON'}">[문제등록형]</c:when>
	                      	  	<c:otherwise>[답안등록형]</c:otherwise>
	                      	  </c:choose>
	                      </td>
	                      <td>${result.facNm}</td>
	                      <td>
	                      	  <c:choose>
	                      	  	<c:when test="${result.quizEnd eq 'N' }">대기</c:when>
	                      	  	<c:when test="${result.quizEnd eq 'P' }">진행중</c:when>
	                      	  	<c:otherwise>
	                      	  		<c:choose>
	                      	  			<c:when test="${USER_INFO.userSe eq '08'}">
	                      	  				<c:choose>
					                      	  	<c:when test="${result.submitAt eq 'Y' }">
					                      	  		<c:choose>
					                      	  			<c:when test="${result.answerCnt > 0}">
					                      	  				<c:set var="jumsu1" value="${(result.answerCnt/(result.quizCnt*result.examineeCnt))*100}"/>
					                      	  				<fmt:formatNumber var="jumsu" value="${jumsu1}" pattern=".00"/>
					                      	  				<%-- <fmt:parseNumber var="jumsu" value="${result.answerCnt/result.quizCnt*100}" integerOnly="true" /> --%>
					                      	  				<c:out value="${jumsu }"/>
					                      	  			</c:when>
					                      	  			<c:otherwise>00.00</c:otherwise>
					                      	  		</c:choose>
					                      	  	</c:when>
					                      	  	<c:otherwise>
					                      	  		대기
					                      	  	</c:otherwise>
					                      	</c:choose>
	                      	  			</c:when>
	                      	  			<c:otherwise>
	                      	  				<c:choose>
	                      	  					<c:when test="${empty result.chScr}">미제출</c:when>
	                      	  					<c:otherwise><c:out value="${result.chScr}"/></c:otherwise>
	                      	  				</c:choose>
	                      	  			</c:otherwise>
	                      	  		</c:choose>
	                      	  	</c:otherwise>
	                      	  </c:choose>
	                      </td>
	                    </tr>
                  	</c:forEach>
                    <c:if test="${fn:length(beforeQuizList) == 0}">
						<tr>
				        	<td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
				      	</tr>
				    </c:if>
                  </tbody>
                </table>
                
                <div class="pagination center-align mt-60">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="/lms/quiz/QuizExamList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start goPage" data-url="${startUrl}"></button>

	               	<c:url var="prevUrl" value="/lms/quiz/QuizExamList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev goPage" data-url="${prevUrl}"></button>

	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/quiz/QuizExamList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>

	               	<c:url var="nextUrl" value="/lms/quiz/QuizExamList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next goPage" data-url="${nextUrl}"></button>

	               	<c:url var="endUrl" value="/lms/quiz/QuizExamList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end goPage" data-url="${endUrl}"></button>
				</div>
              </div>
            </article>
          </section>
        </div>
      </div>
      
<c:import url="/template/bottom.do" charEncoding="utf-8"/>