<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
    <c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
    <c:if test="${not empty searchVO.searchPlanSttusCode}"><c:param name="searchPlanSttusCode" value="${searchVO.searchPlanSttusCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>

$(document).ready(function() {
	//제출서류 압축파일 다운로드
    $(document).on("click", ".zip_down", function(){
        document.frmZipDown.submit();
    });
});
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="Y"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumrequest"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
                <c:param name="managerAt" value="${managerAt}"/>
            </c:import>

            <article class="content-wrap">
              <!-- 수강신청 상태 -->
              <div class="content-body">
                <div class="curriculum-info-area flex-row">
                  <%-- 학생 권한에만 버튼 나오도록 처리 --%>
                  <c:choose>
                    <c:when test="${8 > USER_INFO.userSeCode }"><c:set var="divClassName" value="flex-col-10"/></c:when>
                    <c:otherwise><c:set var="divClassName" value="flex-col-15"/></c:otherwise>
                  </c:choose>
                  <div class="${divClassName }">
                    <!-- 테이블영역-->
                    <table class="common-table-wrap size-sm left-align">
                      <colgroup>
                        <c:choose>
                          <c:when test="${8 > USER_INFO.userSeCode }"><c:set var="colClass" value="bg-light-blue"/></c:when>
                          <c:otherwise><c:set var="colClass" value="bg-point-light"/></c:otherwise>
                        </c:choose>
                        <col class='${colClass }' style='width:24%'>
                        <col>
                      </colgroup>
                      <tbody>
                        <tr class=" ">
                          <td scope='row' class='font-700'>수강신청기간</td>
                          <td>
                            <c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
                            <c:choose>
                              <c:when test="${applyDateDDay > 0 }">(D-${applyDateDDay })</c:when>
                              <c:when test="${applyDateDDay eq 0 }">(D-DAY)</c:when>
                            </c:choose>
                          </td>
                        </tr>
                        <tr class=" ">
                          <td scope='row' class='font-700'>수강정원</td>
                          <td>
                            <c:choose>
                                <c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
                                <c:otherwise>
                                    <c:out value="${curriculumVO.applyMaxCnt}"/>명
                                </c:otherwise>
                            </c:choose>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 수강신청 제출서류 압축 다운로드 form -->
              <form id="frmZipDown" name="frmZipDown" action="/cmm/fms/paperFileDownLoadZip.do" method="post">
                <input type="hidden" name="atchFileIdArr" value="${curriculumbaseVO.aplyFile}" />
                <input type="hidden" name="atchFileIdArr" value="${curriculumbaseVO.planFile}" />
                <input type="hidden" name="atchFileIdArr" value="${curriculumbaseVO.etcFile}" />
                <input type="hidden" name="downLoadType" value="arr" />
              </form>
              
              <!-- 수강신청 제출서류 -->
              <c:if test="${curriculumbaseVO.stdntAplyAt eq 'Y'}">
                <div class="content-header">
                  <div class="title-wrap">
                    <div class="title">수강신청 제출서류</div>
                    <div class="desc-sm">(파일을 다운로드 받아 신청서를 모두 작성하신 후, 수강신청 접수를 할 수 있습니다)</div>
                  </div>
                  <a class="btn-md btn-outline zip_down" href="#none">전체 다운로드 </a>
                </div>
                <div class="content-body">
                  <!-- 테이블영역-->
                  <table class="common-table-wrap size-sm left-align">
                    <colgroup>
                      <col class='bg-light-blue' style='width:20%'>
                      <col>
                    </colgroup>
                    <tbody>
                      <tr class=" ">
                        <td scope='row' class='font-700'>신청서</td>
                        <td>
                          <c:if test="${!empty curriculumbaseVO.aplyFileNm}">
                            <a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.aplyFile}&amp;fileSn=0"><i class='icon-download mr-10'>다운로드</i><c:out value="${curriculumbaseVO.aplyFileNm}"/></a>
                          </c:if>
                        </td>
                      </tr>
                      <tr class=" ">
                        <td scope='row' class='font-700'>계획서</td>
                        <td>
                            <c:if test="${!empty curriculumbaseVO.planFileNm}">
                            <a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.planFile}&amp;fileSn=0"><i class='icon-download mr-10'>다운로드</i><c:out value="${curriculumbaseVO.planFileNm}"/></a>
                          </c:if>
                        </td>
                      </tr>
                      <tr class=" ">
                        <td scope='row' class='font-700'>기타</td>
                        <td>
                            <c:if test="${!empty curriculumbaseVO.etcFileNm}">
                            <a href="/cmm/fms/FileDown.do?atchFileId=${curriculumbaseVO.etcFile}&amp;fileSn=0"><i class='icon-download mr-10'>다운로드</i><c:out value="${curriculumbaseVO.etcFileNm}"/></a>
                          </c:if>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </c:if>
            </article>
            <hr class="line-hr mb-35">
            <article class="content-wrap">
              <!-- 과정안내 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정안내</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class=" ">
                      <td scope='row' class='font-700'>기간</td>
                      <td><c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/></td>
                      <td scope='row' class='font-700'>강의시간</td>
                      <td><c:out value="${curriculumVO.totalTime}"/>시간</td>
                    </tr>
                    <tr class=" ">
                      <td scope='row' class='font-700'>과정진행캠퍼스</td>
                      <td><c:out value="${curriculumVO.campusNm}"/></td>
                      <td scope='row' class='font-700'>강의실</td>
                      <td><c:out value="${curriculumVO.campusPlace}"/></td>
                    </tr>
                    <tr class=" ">
                      <td scope='row' class='font-700'>과정 개요 및 목표</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class=" ">
                      <td scope='row' class='font-700'>과정의 기대효과</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclEffect, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 과정내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정내용</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col style='width:50%'>
                    <col style='width:50%'>
                  </colgroup>
                  <thead>
                    <tr class='font-700 bg-gray'>
                      <th scope='col'>내용범주</th>
                      <th scope='col'>내용</th>
                    </tr>
                  </thead>
                  <tbody>
                    <%-- 과정내용 번호 및 단원 묶어주는 역할 --%>  
                    <c:set var="rowspan" value="0"/>
                    <c:set var="rowspanList" value=""/>
                    <c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
                    <c:forEach var="list" items="${lessonList}" varStatus="status">
                        <c:choose>
                            <c:when test="${prevCode eq list.lessonNm}">
                                <c:set var="rowspan" value="${rowspan + 1}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
                                <c:set var="rowspan" value="1"/>
                                <c:set var="prevCode" value="${list.lessonNm}"/>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
                    <c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
                    <c:set var="listCnt" value="${rowspan[0]}"/>
                    <c:set var="rowspanCnt" value="0"/>
                    
                    <c:set var="prevCode" value=""/>
                    <c:set var="number" value="1"/>
        
                    <c:forEach var="result" items="${lessonList}" varStatus="status">
                        <c:if test="${prevCode ne result.lessonNm}">
                            <tbody class='box_lesson'>
                        </c:if>
                        <tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
                            <c:if test="${prevCode ne result.lessonNm}">
                                <td rowspan='${rowspan[rowspanCnt] + 1}' class=" "><c:out value="${result.lessonNm}"/></td>
                                <c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
                            </c:if>
                            <td class=" "><c:out value="${result.chasiNm}"/></td>
                        </tr>
                        <c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
                            </tbody>
                        </c:if>
                        
                        <c:set var="prevCode" value="${result.lessonNm}"/>
                    </c:forEach>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 교수진 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교수진</div>
                </div>
                <p class="notice font-basic">과정책임부서: <c:out value="${curriculumVO.hostCodeNm}"/></p>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <colgroup>
                    <col style='width:30%'>
                    <col style='width:40%'>
                    <col style='width:30%'>
                  </colgroup>
                  <thead>
                    <tr class='font-700 bg-gray'>
                      <th scope='col'>교수명</th>
                      <th scope='col'>소속</th>
                      <th scope='col'>이메일</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach var="result" items="${facList}" varStatus="status">
                        <tr>
                            <td class="line"><c:out value="${result.userNm}"/></td>
                            <td class="line"><c:out value="${result.mngDeptNm}"/></td>
                            <td class="line"><c:out value="${result.emailAdres}"/></td>
                        </tr>
                    </c:forEach>
                  </tbody>
                </table>
                <p class="notice">※ 교수진은 과정 진행 중 변경될 수 있습니다</p>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 성적 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">성적</div>
                </div>
                <%-- 
                <p class="notice font-700">
                	※
                	<c:choose>
					<c:when test="${curriculumVO.evaluationAt eq 'Y' and not empty curriculumVO.gradeType}">
						절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
					</c:when>
					<c:otherwise>상대평가</c:otherwise>
					</c:choose>
                </p>
                 --%>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm">
                  <c:choose>
                    <c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
                      <thead>
                        <tr class='font-700 bg-gray'>
                          <th scope='col'>Grade</th>
                          <c:forEach var="result" items="${gradeTypeList}" varStatus="status">
                            <c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
                                <th scope='col'><c:out value="${result.ctgryNm}"/></th>
                            </c:if>
                          </c:forEach>
                        </tr>
                      </thead>
                      <tbody>
                      <tr class=" ">
                        <c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
                        <c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
                        <c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
                        <c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
                        <c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
                        <c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
                        <c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
                        <c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
                        <c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
                        <c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
                        <c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
                        
                        <td class=" ">점수</td>
                        <td class=" ">${aplus}</td>
                        <td class=" ">${a}</td>
                        <td class=" ">${bplus}</td>
                        <td class=" ">${b}</td>
                        <td class=" ">${cplus}</td>
                        <td class=" ">${c}</td>
                        <td class=" ">${dplus}</td>
                        <td class=" ">${d}</td>
                        <td class=" ">${f}</td>
                      </tr>
                      </tbody>
                    </c:when>
                    <c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
                      <thead>
                        <tr class='font-700 bg-gray'>
                            <th class=" ">P/F</th>
                            <th class=" " colspan="2">점수구간</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class=" ">
                            <td class=" ">Pass</td>
                            <td class=" " colspan="2">${curriculumVO.pass} ~ 100</td>
                        </tr>
                        <tr class=" ">
                            <td class=" ">Fail</td>
                            <td class=" " colspan="2">${curriculumVO.fail} ~ 0</td>
                        </tr>
                      </tbody>
                    </c:when>
                    <c:otherwise>
                      <thead>
                        <tr class='font-700 bg-gray'>
                            <th class=" ">Grade</th>
                            <c:forEach var="result" items="${gradeTypeList}" varStatus="status">
                                <c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
                                    <th class=" "><c:out value="${result.ctgryNm}"/></th>
                                </c:if>
                            </c:forEach>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td class=" ">비중</td>
                            <td colspan="2" class=" ">상위 30%</td>
                            <td colspan="2" class=" ">상위 65%이내</td>
                            <td colspan="5" class=" ">66% ~ 100%</td>
                        </tr>
                        <%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
                        <tr>
                            <td class=" ">가이드 학생 수</td>
                            <td colspan="2" class=" ">30</td>
                            <td colspan="2" class=" ">35</td>
                            <td colspan="5" class=" ">35</td>
                        </tr>
                      </tbody>
                    </c:otherwise>
                  </c:choose>
                </table>
                <p class="notice">※ 성적 기준은 과정 진행 중 변경될 수 있습니다</p>
              </div>
            </article>
            <article class="content-wrap mb-20">
              <!-- 교재 및 부교재 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교재 및 부교재</div>
                </div>
              </div>
              <div class="content-body">
                <ul class="book-list-wrap flex-row">
                  <c:forEach var="result" items="${bookList}" varStatus="status">
                  	<c:url var="viewURL" value='/cop/bbs/selectBoardArticle.do'>
				    	<c:param name="menuId" value="MNU_0000000000000008"></c:param>
				    	<c:param name="bbsId" value="${result.bbsId}"></c:param>
				    	<c:param name="nttNo" value="${result.nttNo}"></c:param>
				    </c:url>
                    <li class="flex-col-2">
                      <div class="list">
                        <div class="img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=SITE_000000000000001&amp;appendPath=<c:out value="${result.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);" title="${result.nttSj}"></div>
                        <a href="${viewURL}" class="w-100 btn-md btn-outline" target="_blank">상세보기</a>
                      </div>
                    </li>
                  </c:forEach>
                </ul>
              </div>
            </article>
            <c:if test="${not empty curriculumVO.bookEtcText}">
		        <article class="content-wrap mb-20">
		        	<c:if test="${fn:length(bookList) eq '0'}">
			        	<div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">교재 및 부교재</div>
			                </div>
			            </div>
		            </c:if>
		        	<div class="content-textarea onlyText" contentEditable="false" placeholder="내용이 없습니다.">
	                  	<c:out value="${fn:replace(curriculumVO.bookEtcText, LF, '<br>')}" escapeXml="false"/>
	                </div>
		        </article>
	        </c:if>
            <article class="content-wrap">
              <!-- 수강신청 환불처리 안내 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수강취소 및 환불처리 안내</div>
                </div>
              </div>
              <div class="content-body">
                <div class="box-area bg-gray-light">
                  <p class="desc">수강취소 및 환불처리를 원하시는 분께서는 아래 안내문을 참조하시어 환불처리를 진행해주세요</p>
                  <div class="btn-wra mt-25">
                    <a href="#" class="block btn-xl btn-outline btnModalOpen" data-modal-type="refund_policy">수강취소 및 환불처리 안내문</a>
                  </div>
                </div>
              </div>
            </article>
            <div class="page-btn-wrap mt-50">
              <c:url var="listUrl" value="/lms/crm/selectCurseregManage.do?${_BASE_PARAM}"/>
              <a href="${listUrl }" class="btn-xl btn-outline-gray font-basic">목록으로</a>              
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  <!-- 취소 환불 규정 start -->
  <div id="refund_policy_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">환불처리 기준 안내</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <div class="refund_content">${board.nttCn }</div>

          <c:import url="/lms/crm/selectFileInfs.do" charEncoding="utf-8">
            <c:param name="param_atchFileId" value="${board.atchFileId}" />
          </c:import>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
      </div>
    </div>
  </div>
  <!-- 취소 환불 규정 end -->

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>