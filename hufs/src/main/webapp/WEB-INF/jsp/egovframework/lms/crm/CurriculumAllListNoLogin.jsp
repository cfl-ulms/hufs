<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId }"/>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchCampusId}"><c:param name="searchCampusId" value="${searchVO.searchCampusId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>

$(document).ready(function() {
	$("document").scrollTop($(document).height());
	
	var paramCrclLang = "${param.crclLang}";
	if(paramCrclLang != ""){
		$("#searchCrclLang").val(paramCrclLang).prop("selected", true);
		$("form[name=frm]").submit();
		
	}
	//국가 선택 검색
	/*  $(document).on("click", ".liLang", function() {
		var langId = $(this).data("id");

		$("#searchCrclLang").val(langId).prop("selected", true);
		$("form[name=frm]").submit();
	});  */
	
	//국가 선택 검색
    $(document).on("click", ".liLang", function() {
        var langId = $(this).data("id");

        $("#searchCrclLang").val(langId).prop("selected", true);
        //$("form[name=frm]").submit();


        //ajax 처리
        var clickflag = $(this).attr("data-clickflag");
        var params    = jQuery("form[name=frm]").serialize() + "&pageIndex=0&ajaxFlag=all";

        //여러번 호출 방지를 위한 flag 처리
        if(clickflag == "N") {
            alert("빠른 시간에 여러번 호출이 불가능 합니다.");
            return;
        }
        
        //여러번 호출 방지를 위한 flag 처리
        $(this).attr("data-clickflag", "N");

        $.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/crm/CurriculumListAjax.do",
            data:params,
            success: function(data) {
                //tr 태그 추가
                $(".list_table tbody tr").remove();
                $(".list_table tbody").append(data);

                //여러번 호출 방지를 위한 flag 처리
                $(this).attr("data-clickflag", "Y");

                //버튼 활성화 처리
                $(".liLang").removeClass("active");
                $(this).addClass("active");
                
                //리스트 페이징 버튼 index 초기화
                $(".addPage").attr("data-pageindex", 1);
            }.bind(this),
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });
    });
    
    //페이징
    $(document).on("click", ".addPage", function() {
        var pageIndex = parseInt($(this).attr("data-pageindex"));
        var clickflag = $(this).attr("data-clickflag");
        var params    = jQuery("form[name=frm]").serialize() + "&pageIndex=" + pageIndex + "&ajaxFlag=all";
        
        //데이터가 더 이상 없을 때 flag 처리
        if(clickflag == "F") {
            alert("더 이상 데이터가 없습니다.");
            return;
        }
        
        //여러번 호출 방지를 위한 flag 처리
        if(clickflag == "N") {
            alert("빠른 시간에 여러번 호출이 불가능 합니다.");
            return;
        }
        
        //여러번 호출 방지를 위한 flag 처리
        $(".addPage").attr("data-clickflag", "N");

        $.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/crm/CurriculumListAjax.do",
            data:params,
            success: function(data) {
                //tr 태그 추가
                $(".list_table tbody").append(data);
                
                if($.trim(data) == "" || $.trim(data) == null) {
                    alert("더 이상 데이터가 없습니다.");
                    $(".addPage").attr("data-clickflag", "F");
                    return;
                }

                //페이징 index 증가
                pageIndex++;
                $(".addPage").attr("data-pageindex", pageIndex);

                //여러번 호출 방지를 위한 flag 처리
                $(".addPage").attr("data-clickflag", "Y");
            },
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });
        
    });
});

</script>

<!-- sub top-->
  <div class="subtop-full-wrap" style="background-image:url(${CML}/imgs/page/subtop/img_subtop_education_bg.jpg);">
    <h2 class="subtop-title center-align">11개의 특수외국어를 누구나 배울 수 있습니다.<br>지금 도전해 보세요!</h2>
  </div>
  <!-- bread crumb -->
  <div class="area">
    <ul class="main-bread-crumb">
      <li class="item">HOME</li>
      <li class="item">교육과정</li>
    </ul>
  </div>
  <section class="section-gap bg-gray-light">
    <div class="area">
      <div class="main-common-title2">
        <h2 class="title">특수외국어 교육 주요과정 소개</h2>
        <p class="sub-title">연간 200개 이상의 다양한 특수외국어 교육과정이 운영되고 있습니다.</p>
      </div>
      <div class="educourse-intro-wrap">
        <ul class="educourse-tab-wrap">
          <li class="tab-lists on" data-target=".educourseTab1">방중집중이수강좌</li>
          <li class="tab-lists" data-target=".educourseTab2">학부전공연계과정</li>
          <li class="tab-lists" data-target=".educourseTab3">특수외국어 오픈강좌</li>
        </ul>
        <ul class="educourse-con-wrap">
          <li class="educourse-con-lists educourseTab1 on">
            <span class="num"><b>01</b> / 03</span>
            <p class="title">
              특수외국어 전공생과 일반인을 대상으로 효과적인 언어능력향상을 위한<br class="none block-lg">
              <b>국내 단기 집중이수과정입니다.</b>
            </p>
            <ul class="educourse-con-desc">
              <li class="desc-text"><span class="desc-num">01</span>희망 언어에 한해 RC 기반 방학 중 “특수외국어 집중 국내어학연수 과정” 개설</li>
              <li class="desc-text"><span class="desc-num">02</span>하계 및 동계 방학 기간을 이용, 우리대학 글로벌 캠퍼스 기숙사 공간 활용</li>
              <li class="desc-text"><span class="desc-num">03</span>학과 수요에 따라 기초어학과정, 통번역실습과정, 비즈니스통번역과정 등 다양한 과정 운영</li>
              <li class="desc-text"><span class="desc-num">04</span>집중이수제 시스템을 활용하여 학점 부여 및 이수 증명서 발급</li>
            </ul>
          </li>
          <li class="educourse-con-lists educourseTab2">
            <span class="num"><b>02</b> / 03</span>
            <p class="title">
              특수외국어 전공 학생들의 언어역량 강화를 위한<br>
              <b>다양한 전공연계 비교과과정이 운영됩니다. </b>
            </p>
            <ul class="educourse-con-desc">
              <li class="desc-text">
                <span class="desc-num">01</span>
                외국인유학생-전공생 간의 상호 교류를 통해 학습자 중심의 외국어 학습을 지원하는 탄뎀프로그램
              </li>
              <li class="desc-text"><span class="desc-num">02</span>특수외국어 전공 교과목과 연계한 외국어 능력의 심화를 위한 실습 과정 </li>
              <li class="desc-text"><span class="desc-num">03</span>저학년, 편입생, 군위탁생 및 타전공자를 대상으로 하는 맞춤형 특수외국어 보충과정</li>
            </ul>
          </li>
          <li class="educourse-con-lists educourseTab3">
            <span class="num"><b>03</b> / 03</span>
            <p class="title">
              <b>누구나 쉽게 특수외국어를 배울 수 있도록 다양한 강좌를 개설·운영합니다.</b>
            </p>
            <ul class="educourse-con-desc">
              <li class="desc-text">
                <span class="desc-num">01</span>
                학과별 일반인 대상 특수외국어 오픈 강좌 운영
              </li>
              <li class="desc-text">
                <span class="desc-num">02</span>
                수요 맞춤형 야간 및 주말 과정 운영
              </li>
              <li class="desc-text">
                <span class="desc-num">03</span>
                정부·중앙기관 및 지방자치단체와의 협력을 통한 특별 강좌 개설
              </li>
              <li class="desc-text">
                <span class="desc-num">04</span>
                산업체 현장 수요 및 직무요구를 반영한 기업체 위탁특별교육과정 운영
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </section>
	
	<c:if test="${fn:length(recentCurriculumList) > 0}">
	  <section class="section-gap">
	    <!-- 최근 접수가 시작된 수강신청 -->
	    <div class="area">
	      <div class="main-common-title2">
	        <h2 class="title">최근 수강신청 접수가 시작된 과정</h2>
	      </div>
	      <div class="area">
	        <div class="flex-row">
	        	<c:forEach var="result" items="${recentCurriculumList}" varStatus="status">
	              <div class="flex-col-4 relative">
		            <a href="/lms/crm/CurriculumAllView.do?crclId=${result.crclId}&crclbId=${result.crclbId}&menuId=MNU_0000000000000066" class="card-wrap">
		
		              <div class="card-head">
		                <p class="card-flag-img">
		                 <c:set var="imgSrc">
							<c:import url="/lms/common/flag.do" charEncoding="utf-8">
								<c:param name="ctgryId" value="${result.crclLang}"/>
							</c:import>
		          		  </c:set>
		                  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
		                </p>
		                <div class="card-title-wrap">
		                  <span class="card-category font-blue">
		                  <fmt:parseDate value="${result.applyStartDate}" var="startDate" pattern="yyyy-MM-dd"/>
		                  <fmt:formatDate value="${startDate}" var="start" pattern="yyyyMMdd"/>
		                  
		                  <jsp:useBean id="toDay" class="java.util.Date"/>
		                  <fmt:formatDate var="now" value="${toDay}" pattern="yyyyMMdd" />
	
		                  <c:choose>
		                  	<c:when test="${result.processSttusCodeDate eq '3'}">
		                  		신청접수중
		                  	</c:when>
		                  	<c:otherwise>-</c:otherwise>
		                  </c:choose>
		                  <c:set var="dDay" value="${start-now}"/>
		                  <c:if test="dDay > 0">(D-${dDay})</c:if>
		                  </span>
		                  <div class="card-title">${result.crclNm}</div>
		                </div>
		              </div>
		              <div class="card-body">
		                <ul class="card-items-wrap">
		                  <li class="card-items icon-campus">
		                  	<c:forEach var="campus" items="${campusList}" varStatus="status">
			                    <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
			                      <c:out value="${campus.ctgryNm}"/>
			                    </c:if>
			                </c:forEach>
		                  </li>
		                  <li class="card-items icon-name">
		                  	${result.userNm } 교수
		                  </li>
		                  <li class="card-items icon-date">${result.applyStartDate } ~ ${result.applyEndDate }</li>
		                </ul>
		              </div>
		            </a>
		          </div>
	            </c:forEach>
	        </div>
	      </div>
	    </div>
	  </section>
	</c:if>
	
	
	
	
	
	<section class="section-gap">
    <form name="frm" method="post" action="<c:url value="/lms/crm/selectCurriculumAllList.do"/>">
    <input type="hidden" name="menuId" value="${param.menuId }" />
    <div class="area">
      <div class="box-wrap mb-55">
        <h3 class="title-subhead">교육과정 검색</h3>
        <div class="flex-row-ten">
          <div class="flex-ten-col-3 mb-20">
            <div class="ell">
              <select class="select2" name="searchCrclLang" id="searchCrclLang" data-select="style3">
                <option value="">언어 전체</option>
                <c:forEach var="result" items="${languageList}" varStatus="status">
                	<c:if test="${not empty result.upperCtgryId}">
                    	<option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
                    </c:if>
  				</c:forEach>
              </select>
            </div>
          </div>
          <div class="flex-ten-col-7 mb-20">
            <div class="ell">
              <input name="searchCrclNm" type="text" placeholder="찾으시는 과정명을 입력해보세요" value="${searchVO.searchCrclNm }">
            </div>
          </div>
          <div class="flex-ten-col-4">
            <div class="desc">
              <input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="ell date datepicker type2" placeholder="교육 시작일" readonly="readonly"/>
              <i>~</i>
              <input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="ell date datepicker type2" placeholder="교육 종료일" readonly="readonly"/>
            </div>
          </div>
          <div class="flex-ten-col-2">
            <div class="ell">
              <select id="searchProcessSttusCodeDate" name="searchProcessSttusCodeDate" class="select2" data-select="style3">
                <option value="">과정상태 전체</option>
                <c:forEach var="result" items="${statusComCode}" varStatus="status">
                  <c:if test="${result.code ne 0 and result.code ne 1 and result.code ne 5 }">
                    <option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCodeDate}">selected="selected"</c:if>>${result.codeNm}</option>
                  </c:if>
                </c:forEach>
              </select>
            </div>
          </div>
          <div class="flex-ten-col-2">
            <div class="ell">
              <select id="targetType" name="searchTargetType" class="select2" data-select="style3">
                  <option value="">대상 전체</option>
                  <option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
                  <option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
              </select>
            </div>
          </div>
          <div class="flex-ten-col-2">
            <div class="ell">
              <select name="searchCampusId" id="searchCampusId" class="select2" data-select="style3">
                <option value="">장소 전체</option>
                <c:forEach var="result" items="${campusList}" varStatus="status">
                  <c:if test="${result.ctgryLevel eq '1'}">
                    <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCampusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
                  </c:if>
                </c:forEach>
              </select>
            </div>
          </div>
        </div>

        <button class="btn-sm font-400 btn-point mt-20">검색</button>
      </div>

      <!-- tab style -->
      <ul class="tab-wrap line-style line">
      	<li class="tab-list liLang <c:if test="${empty searchVO.searchCrclLang }">on</c:if>" data-id="">전체</li>
      	<c:forEach var="result" items="${languageList}" varStatus="status">
           <c:if test="${not empty result.upperCtgryId}">
             <c:set var="imgSrc">
               <c:import url="/lms/common/flag.do" charEncoding="utf-8">
                 <c:param name="ctgryId" value="${result.ctgryId}"/>
               </c:import>
             </c:set>
             <li class="tab-list liLang <c:if test="${searchVO.searchCrclLang eq result.ctgryId }">on</c:if>" data-id="${result.ctgryId}">
               <a href="#">${result.ctgryNm}</a>
             </li>
           </c:if>
         </c:forEach>
      </ul>
      
      <!-- 테이블영역-->
      <table class="common-table-wrap table-type-board list_table">
          <colgroup>
            <col style='width:11%'>
            <col style='width:12%'>
            <col>
            <col style='width:130px'>
            <col style='width:13%'>
            <col style='width:10%'>
            <col style='width:13%'>
          </colgroup>
          <thead>
            <tr class='bg-light-gray font-700'>
              <th scope='col'>과정진행<br>상태</th>
              <th scope='col'>언어</th>
              <th scope='col'>교육과정명</th>
              <th scope='col'>교육기간</th>
              <th scope='col'>수강신청기간</th>
              <th scope='col'>대상</th>
              <th scope='col'>장소</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="result" items="${resultList}" varStatus="status">
				<c:url var="viewUrl" value="/lms/crm/CurriculumAllView.do${_BASE_PARAM}">
					<c:param name="crclId" value="${result.crclId}"/>
					<c:param name="crclbId" value="${result.crclbId}"/>
					<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
				</c:url>
				<tr onclick="location.href='${viewUrl}'">
					<c:choose>
						<c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
						<c:when test="${result.processSttusCodeDate eq 3 or result.processSttusCodeDate eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
						<c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
						<c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
					</c:choose>
					<td scope="row" class="${processSttusColor }">
						<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
							<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
						</c:forEach>
					</td>
					<td><c:out value="${result.crclLangNm}"/></td>
					<td><c:out value="${result.crclNm}"/></td>
					<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
					<td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
					<td>
						<c:choose>
							<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
							<c:otherwise>일반</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:forEach var="campus" items="${campusList}" varStatus="status">
		                    <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
		                      <c:out value="${campus.ctgryNm}"/>
		                    </c:if>
		                </c:forEach>
		 			</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(resultList) == 0}">
				<tr><td class="listCenter" colspan="7"><spring:message code="common.nodata.msg" /></td></tr>
			</c:if>
          </tbody>
        </table>
        
      <div class="mt-50 center-align">
        <button type="button" class="cursor-pointer addPage" data-pageindex="1" data-clickflag="Y"><img class="vertical-top" src="${_L_IMG }/common/btn_board_contents_more.jpg" alt="더보기"></button>
      </div>
    </div>
    </form>
  </section>
	
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>