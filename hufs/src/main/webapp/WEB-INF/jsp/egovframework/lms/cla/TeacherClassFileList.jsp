<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId}" />
	<c:if test="${not empty searchVO.searchcrclbNm}"><c:param name="searchcrclbNm" value="${searchVO.searchcrclbNm}" /></c:if>
	<c:if test="${not empty searchVO.searchcrclbId}"><c:param name="searchcrclbId" value="${searchVO.searchcrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode1}"><c:param name="searchSysCode1" value="${searchVO.searchSysCode1}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode2}"><c:param name="searchSysCode2" value="${searchVO.searchSysCode2}" /></c:if>
	<c:if test="${not empty searchVO.searchSysCode3}"><c:param name="searchSysCode3" value="${searchVO.searchSysCode3}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty param.tmplatImportAt}"><c:param name="tmplatImportAt" value="${param.tmplatImportAt}" /></c:if>
</c:url>

<script>

$(document).ready(function(){
	$(document).on("click", "#searchReset", function() {
	    $("form[id='frm']").find("input").not("#resetBtn").val("");
	    $("form[id='frm'] select").val("").trigger("change");
	    $("form[id='frm'] input").prop("checked", false);
	});
	
	$(".btn_public").click(function(){
		var publicat = $(this).data("publicat"),
			id = $(this).data("id"),
			sn = $(this).data("sn"),
			publicAtKo = publicat == "Y" ? "공개" : "비공개",
			obj = $(this);
		
		if(confirm("선택한 수업자료를 " + publicAtKo + "로 변경하시겠습니까?")){
			$.ajax({
	       		url : "/cmm/fms/updateFilePublic.do",
	       		dataType : "json",
	       		type : "POST",
	       		data : {publicAt : publicat, atchFileId : id, fileSn : sn},
	       		success : function(data){
	       			if(data.successYn == "Y"){
	       				$(obj).hide();
	       				$(obj).siblings().show();
	       			}
	       		}, error : function(){
	       			alert("error");
	       		}
	       	});
		}
		
		return false;
	});
});

function fnSearch(){
	$("#frm").submit();
}

</script>
<c:choose>
	<c:when test="${param.addAt eq 'Y'}">
	  <!-- 콘텐츠헤더 -->
         <div class="page-content-header">
           <c:choose>
               <%-- TODO : 교원은 PDF 다운로드 버튼을 사용할 수 있기 때문에 분기처리함 사용안하면 분기처리 안해도됨 --%>
               <c:when test="${8 <= USER_INFO.userSeCode }">
                   <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
	                <%-- <c:param name="crclId" value="${param.crclId}"/> --%>
	                <c:param name="curriculumManageBtnFlag" value="Y"/>
	            </c:import>
               </c:when>
               <c:otherwise>
                   <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                       <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
                   </c:import>
               </c:otherwise>
           </c:choose>
         </div>
         
         <!-- 콘텐츠바디 -->
         <section class="page-content-body">
           <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
			<c:param name="menu" value="myschedule"/>
			<c:param name="tabstep" value="${param.tabstep }"/>
			<c:param name="menuId" value="${param.menuId }"/>
			<c:param name="crclId" value="${param.crclId}"/>
			<c:param name="crclbId" value="${param.crclbId}"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">수업자료 검색</h3>

                <form id="frm" method="post" action="<c:url value="/lms/cla/classFileList.do"/>">
            	  <input type="hidden" id="pageIndex" name="pageIndex" value="${paginationInfo.currentPageNo }"/>
            	   <input type="hidden" name="menuId" value="${param.menuId }" />
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <input type="text" name="searchCrclNm" placeholder="과정명" value="${fileVo.searchCrclNm }">
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <input type="text" name="searchRegisterNm" placeholder="파일등록자" value="${fileVo.searchRegisterNm}">
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="ell">
                    	<input type="number" name="searchUseCnt" placeholder="활용횟수" value="${fileVo.searchUseCnt }">
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <input type="text" name="searchFileNm" placeholder="자료명" value="${fileVo.searchFileNm }">
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchPublicAt" id="searchPublicAt" class="select2" data-select="style3">
                        <option value="" ${empty fileVo.searchPublicAt ? 'selected' : ''  }>공개여부 전체</option>
                        <option value="N" ${fileVo.searchPublicAt eq 'N' ? 'selected' : ''  }>비공개</option>
                        <option value="Y" ${fileVo.searchPublicAt eq 'Y' ? 'selected' : ''  }>공개</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 flex align-items-center">
                    <label class="checkbox">
                      <input type="checkbox" name="fileExtOther" value="Y" ${param.fileExtOther eq 'Y' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">파일</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="fileExtImg" value="Y" ${param.fileExtImg eq 'Y' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">사진</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="fileExtMov" value="Y" ${param.fileExtMov eq 'Y' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">동영상</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchFrstRegisterId" value="Y" ${param.searchFrstRegisterId eq 'Y' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">나의 수업자료</span>
                    </label>
                  </div>
                </div>
                <button type="button" class="btn-sm font-400 btn-point mt-20" onclick="fnSearch()" >검색</button>
                <button type="button" class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</button>
                </form>


              </div>
            </article>
            <article class="content-wrap">
              <!-- <div class="content-header">
                <div class="btn-group-wrap">
                  <div class="left-area">
                    <a href="#" class="btn-outline btn-md">선택 자료 다운로드</a>
                  </div>
                </div>
              </div> -->
              <div class="content-body">
              
              </c:otherwise>
		</c:choose>
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:12%'>
                    <col style='width:7%'>
                    <col style='width:91px'>
                    <col style='width:20%'>
                    <col style='width:14%'>
                    <col style='width:11'>
                    <col style='width:7%'>
                    <col style='width:11%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <!-- <th scope='col'>선택</th> -->
                      <th scope='col'>No</th>
                      <th scope='col'>등록일</th>
                      <th scope='col'>유형</th>
                      <th scope='col'>미리보기</th>
                      <th scope='col'>자료명</th>
                      <th scope='col'>과정명</th>
                      <th scope='col'>등록자</th>
                      <th scope='col'>활용횟수</th>
                      <th scope='col'>공개여부</th>
                    </tr>
                  </thead>
                  <tbody>

                  	<c:choose>
						<c:when test="${not empty fileList}">
							<c:forEach var="result" items="${fileList}" varStatus="status">
								<c:url var="viewUrl" value="/lms/cla/classFileView.do">
									<c:param name="atchFileId" value="${result.atchFileId}" />
								  	<c:param name="fileSn" value="${result.fileSn}" />
								  	<c:param name="plId" value="${result.plId}" />
								</c:url>
			                  <tr onclick="location.href='${viewUrl}'" class="cursor-pointer">
									<td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((fileVo.pageIndex-1) * fileVo.pageUnit) - (status.count - 1)}" /></td>
			                      <!-- <td scope='row' class='center-align'><label class='checkbox'><input type='checkbox' class='checkList'><span class='custom-checked'></span></label></td> -->
			                      <td>${result.frstRegisterPnttm}</td>
				                      <c:choose>
					    				<c:when test="${result.fileExtNm eq '이미지'}">
					    					<td>사진</td>
					    					<td>
						                        <div class='board-gallery-lists' style="background-image:url(${fileStorePath}/SITE_000000000000001/${result.plId }/${result.streFileNm });"></div>
						                      </td>
					    				</c:when>
					    				<c:when test="${result.fileExtNm eq '동영상'}">
											<td>동영상</td>
											<td>
						                       <div class='board-gallery-lists file-overlay' style="background:#eeeeee"></div>
						                    </td>
					    				</c:when>
					    				<c:otherwise>
					    					<td>파일</td>
					    					<td>
						                       <div class='board-gallery-lists' style='background-image:url(../imgs/common/img_download_thumbnail_sm.jpg);'></div>
						                    </td>
					    				</c:otherwise>
					    			</c:choose>
			                      <td class='left-align'>${result.orignlFileNm }</td>
			                      <td class='left-align'>${result.crclNm}</td>
			                      <td>${result.userNm}</td>
			                      <td>${result.attachCnt}</td>
			                      <td>
			                      	<c:if test="${result.frstRegisterId eq USER_INFO.id}">
			                      		<button class='btn_public btn-sm-full btn-outline' type='button' data-publicat="N" data-id="${result.atchFileId}" data-sn="${result.fileSn}" <c:if test="${result.publicAt eq 'N'}">style="display:none;"</c:if>>공개</button>
				                      	<button class='btn_public btn-sm-full btn-outline-gray' type='button' data-publicat="Y" data-id="${result.atchFileId}" data-sn="${result.fileSn}" <c:if test="${result.publicAt eq 'Y'}">style="display:none;"</c:if>>비공개</button>
			                      	</c:if>
			                      </td>
			                    </tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<td colspan="9" class='center-align'>자료가 없습니다.</td>
						</c:otherwise>
                  	</c:choose>
                  </tbody>
                </table>
                <c:if test="${param.addAt ne 'Y'}">
	                <div class="pagination center-align mt-60">
						<div class="pagination-inner-wrap overflow-hidden inline-block">
							<c:url var="startUrl" value="/lms/cla/classFileList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="1" />
			   				</c:url>
			               	<button class="start goPage" data-url="${startUrl}"></button>
		
			               	<c:url var="prevUrl" value="/lms/cla/classFileList.do${_BASE_PARAM}">
								<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
							</c:url>
			               	<button class="prev goPage" data-url="${prevUrl}"></button>
		
			               	<ul class="paginate-list f-l overflow-hidden">
			                 	<c:url var="pageUrl" value="/lms/cla/classFileList.do${_BASE_PARAM}"/>
								<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
								<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
			               	</ul>
		
			               	<c:url var="nextUrl" value="/lms/cla/classFileList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
			   				</c:url>
			               	<button class="next goPage" data-url="${nextUrl}"></button>
		
			               	<c:url var="endUrl" value="/lms/cla/classFileList.do${_BASE_PARAM}">
			  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
			   				</c:url>
			               	<button class="end goPage" data-url="${endUrl}"></button>
						</div>
					</div>
				</c:if>
              </div>
            </article>
          </section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>