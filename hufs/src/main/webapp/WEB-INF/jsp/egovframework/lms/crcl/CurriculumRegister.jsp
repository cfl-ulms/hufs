<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<%--과정체계코드 --%>
<c:set var="sysCode" value="${fn:split(curriculumVO.sysCodePath,'>')}"/>
<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
	<c:if test="${not empty searchVO.menuId}"><c:param name="menuId" value="${searchVO.menuId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${empty searchVO.crclId}">
		<c:set var="_MODE" value="REG"/>
		<c:set var="_ACTION" value="${_PREFIX}/addCurriculum.do${_BASE_PARAM }"/>
	</c:when>
	<c:otherwise>
		<c:set var="_MODE" value="UPT"/>
		<c:set var="_ACTION" value="${_PREFIX}/updateCurriculum.do${_BASE_PARAM}"/>
	</c:otherwise>
</c:choose>
<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<script>
//달력
$(function() {
  $("#startDate, #endDate, #planStartDate, #planEndDate, #applyStartDate, #applyEndDate, #tuitionEndDate, #tuitionStartDate").datepicker({
    dateFormat: "yy-mm-dd"
  });
});

$(document).ready(function(){
	//초기화 시작
	<c:if test="${curriculumVO.applyMaxCnt == 0}">
		$("#applyMaxCnt").prop("readonly", true);
	</c:if>
	
	if($("input[name=tuitionAt]:checked").val() == "Y"){
		$("input[name=tuitionFeesAt]").prop("disabled", false);
		$("input[name=registrationFeesAt]").prop("disabled", false);
	}else{
		$("input[name=tuitionFeesAt]").prop("checked", false);
		$("input[name=tuitionFeesAt]").prop("disabled", true);
		$("input[name=registrationFeesAt]").prop("checked", false);
		$("input[name=registrationFeesAt]").prop("disabled", true);
		$("#tuitionFees").prop("readonly", true);
		$("#registrationFees").prop("readonly", true);
	}
	
	sisuTotCnt();
	facTotCnt();
	//초기화 끝
	
	$("#btn-reg").click(function(){
		$("#detailForm").submit();
		return false;
	});
	
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${sysCode[2]}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
		autoTxt();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${sysCode[3]}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
		$("#ctgryId3").change();
		autoTxt();
	});
	
	$("#ctgryId3").change(function(){
		var sysCode3 = $(this).val(),
			optionHtml = "<option value=''>기본과정명</option>";
		
		$.ajax({
			url : "/mng/lms/crclb/CurriculumbaseList.json"
			, type : "post"
			, dataType : "json"
			, data : {searchSysCode3 : sysCode3}
			, success : function(data){
				var searchcrclbId = "${curriculumVO.crclbId}";
				if(data.successYn == "Y"){
					$.each(data.items, function(i){
						if(data.items[i].crclbId == searchcrclbId){
							optionHtml += "<option value='"+data.items[i].crclbId+"' selected='selected'>"+data.items[i].crclbNm+"</option>"
						}else{
							optionHtml += "<option value='"+data.items[i].crclbId+"'>"+data.items[i].crclbNm+"</option>"
						}
						
					});
					
					$("#crclbId").html(optionHtml);
				}
			}, error : function(){
				alert("error");
			}
		});
	});
	
	$("#ctgryId1").change();
	
	//기본과정 선택
	$("#crclbId").change(function(){
		var id = $(this).val();
		
		selectcrclb(id);
	});
	
	//과정명 Auto
	$(".autoTxt").change(function(){
		autoTxt();
	});
	
	//수강정원
	$("input[name=applyMaxCntAt]").click(function(){
		if("Y" == $(this).val()){
			$("#applyMaxCnt").prop("readonly", false);
		}else{
			$("#applyMaxCnt").prop("readonly", true);
			$("#applyMaxCnt").val("0");
		}
	});
	
	//등록금
	$("input[name=tuitionAt]").click(function(){
		if("Y" == $(this).val()){
			$("input[name=tuitionFeesAt]").prop("disabled", false);
			$("input[name=registrationFeesAt]").prop("disabled", false);
		}else{
			if(confirm("등록 금액과 기간이 삭제됩니다.")){
				$("input[name=tuitionFeesAt]").prop("checked", false);
				$("input[name=tuitionFeesAt]").prop("disabled", true);
				$("input[name=registrationFeesAt]").prop("checked", false);
				$("input[name=registrationFeesAt]").prop("disabled", true);
				$("#tuitionFees").val('').prop("readonly", true);
				$("#registrationFees").val('').prop("readonly", true);
				$("#tuitionStartDate").val('').prop("readonly", true);
				$("#tuitionEndDate").val('').prop("readonly", true);
			}else{
				return false;
			}
		}
	});
	
	$("input[name=tuitionFeesAt]").click(function(){
		if($(this).is(":checked")){
			$("#tuitionFees").prop("readonly", false);
		}else{
			$("#tuitionFees").prop("readonly", true);
		}
	});
	
	$("input[name=registrationFeesAt]").click(function(){
		if($(this).is(":checked")){
			$("#registrationFees").prop("readonly", false);
		}else{
			$("#registrationFees").prop("readonly", true);
		}
 	});
	
	//교원검색
	$("#searchUser").autocomplete({
		source : function(request, response){
			$.ajax({
				type:"post",
   	          	dataType:"json",
   	          	url:"/mng/usr/EgovMberManage.json",
   	          	data:{userSeCode : "08", searchUserNm : $("#searchUser").val()}, //, searchDept : $("#hostCode").val()
   	          	success:function(result){
   	          		response($.map(result.items, function(item){     //function의 item에 data가 바인딩된다.
   	            		return{
	       	             label:item.userNm + "("+item.userId+")",
	       	             value:item.userId,
	       	             userNm:item.userNm
   	            		}
   	           		}));
   	          	},
   	          	error: function(){
   	          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
   	          }
			})
		},
   	    //autoFocus:true,             //첫번째 값을 자동 focus한다.
   	    matchContains:true,
   	    selectFirst:false,
   	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
   	    delay:100,                 //milliseconds
   	    select:function(event,ui){         //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
   	    	var selHtml = "<li class='item bg-blue-light'>" +
   	    					"<input type='hidden' name='userIdList' value='"+ui.item.value+"'/>" + ui.item.label +
   	    					"<button type='button' class='btn-remove btn_del' title='삭제'></button>" +
   	    				  "</li>",
   	    		cnt = 0;
   	    	
   	    	$("#box_user input[name='userIdList']").each(function(){
   	    		if($(this).val() == ui.item.value){
   	    			alert("이미 등록 된 교원 입니다.");
   	    			cnt++;
   	    			return false;
   	    		} 
   	    	});
   	    	
   	    	if(cnt == 0){
   	    		$("#box_user").append(selHtml);
   	    	}
   	    	
   	    	$("#searchUser").val(ui.item.userNm);
   	    	return false;
   	    },
   	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	});
	
	//강의책임교원검색
	$("#searchUser2").autocomplete({
		source : function(request, response){
			$.ajax({
				type:"post",
   	          	dataType:"json",
   	          	url:"/mng/usr/EgovMberManage.json",
   	          	data:{userSeCode : "08", searchUserNm : $("#searchUser2").val()}, //, searchDept : $("#hostCode").val()
   	          	success:function(result){
   	          		response($.map(result.items, function(item){     //function의 item에 data가 바인딩된다.
   	            		return{
            			 label:item.userNm + "("+item.mngDeptCodeNm+"_"+item.emailAdres+")",
	       	             value:item.userId,
	       	             userNm:item.userNm
   	            		}
   	           		}));
   	          	},
   	          	error: function(){
   	          		alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
   	          }
			})
		},
   	    //autoFocus:true,             //첫번째 값을 자동 focus한다.
   	    matchContains:true,
   	    selectFirst:false,
   	    minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
   	    delay:100,                 //milliseconds
   	    select:function(event,ui){         //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
   	    	var selHtml = "<li><input type='hidden' name='userIdList2' value='"+ui.item.value+"'/>"+ui.item.label+" <a href='#' class='btn_del'><img src='/template/manage/images/btn/del.gif'/></a></li>";
   	    	var selHtml = "<tr>" +
   	    					"<td class='left-align'>" +
   	    						"<input type='hidden' name='userIdList2' value='"+ui.item.value+"'/>" +
   	    						ui.item.label +
   	    					"</td>" +
   	    				  	"<td class='left-align'><button class='btn-sm-full btn-outline-gray btn_del' type='button'>삭제</button></td>" +
   	    				  "</tr>"
   	    		cnt = 0;
   	    	
   	    	$("#box_user2 input[name='userIdList2']").each(function(){
   	    		if($(this).val() == ui.item.value){
   	    			alert("이미 등록 된 교원 입니다.");
   	    			cnt++;
   	    			return false;
   	    		} 
   	    	});
   	    	
   	    	if(cnt == 0){
   	    		$("#box_user2").append(selHtml);
   	    	}
   	    	
   	    	$("#searchUser2").val(ui.item.userNm);
   	    	return false;
   	    },
   	    focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
	});
	
	//사업비
	var expDepth1 = [],
		expDepth2 = [];
	
	<c:forEach var="result" items="${feeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '1'}">
			expDepth1.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '2'}">
			expDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	//지원비 전체 선택
	$("#allCheckExp").click(function(){
		if($(this).is(":checked")){
			$(".expCheck").prop("checked", true);
		}else{
			$(".expCheck").prop("checked", false);
		}
	});
	
	//지원비 비목 추가
	$(".btn_addExp").click(function(){
		var selectHtml = "",
			actSelHtml = "<select class='accountCodeList table-select select2' name='accountCodeList' data-select='style1'><option value=''>선택</option></select>",
			tbodyHtml = "";
		
		//지원비 비목
		for(i = 0; i < expDepth1.length; i++){
			if(i == 0){
				selectHtml = "<select class='typeCodeList table-select select2' data-select='style1'><option value=''>선택</option>";
			}
			selectHtml += "<option value='"+expDepth1[i].ctgryId+"'>"+expDepth1[i].ctgryNm+"</option>";
			
			if(i == expDepth1.length - 1){
				selectHtml += "</select>";
			}
		}
		
		tbodyHtml = "<tbody class='box_exp'>" +
						 "<tr class='firstExp'>" +
						 	"<td scope='row' class='box_check' rowspan='2'><label class='checkbox'><input type='checkbox' class='checkList expCheck'><span class='custom-checked'></span></label></td>" +
							"<td scope='row' class='box_selExp' rowspan='2'>" +
								selectHtml+
							"</td>" +
							"<td>" + actSelHtml + "</td>" +
							"<td class='amount'></td>" +
							"<td class='reason'></td>" +
							"<td class='etc'></td>" +
							"<td class='contr'></td>" +
						"</tr>" +
						"<tr class='foot'><td scope='row' class='btn_addExpMore btn-add-list' colspan='5'>계정 목 추가</td></tr>" +
					"</tbody>";
					
		//$("#box_addExp").before(tbodyHtml);
		$(".expTable").append(tbodyHtml);
		
		Lms.common_ui.init();
		
		return false;
	});
	
	//지원비 비목 선택
	$(document).on("change", ".typeCodeList", function(){
		var code = $(this).val(),
			actSelHtml = "<option value=''>선택</option>",
			expIdHtml = "<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='"+code+"'/>";
			
		//계정 목
		for(i = 0; i < expDepth2.length; i++){
			if(code == expDepth2[i].upperCtgryId){
				actSelHtml += "<option value='"+expDepth2[i].ctgryId+"'>"+expDepth2[i].ctgryNm+"</option>";
			}
		}
		//계정목초기화
		$(this).parents("tbody.box_exp").find(".typeCodeListInp").remove();
		$(this).parents("tbody.box_exp").find(".accountCodeList").html(actSelHtml);
		$(this).parents("tbody.box_exp").find(".accountCodeList").after(expIdHtml);
		$(this).parents("tbody.box_exp").find(".amount").html("");
		$(this).parents("tbody.box_exp").find(".reason").html("");
		$(this).parents("tbody.box_exp").find(".etc").html("");
		$(this).parents("tbody.box_exp").find(".contr").html("");
	});
	
	//계정목 선택
	$(document).on("change", ".accountCodeList", function(){
		var code = $(this).val(),
			amoutHtml = "<input type='text' class='table-input money onlyNum' name='amountList' value='' placeholder='금액 입력'>",
			reasonHtml = "<input type='text' class='table-input' name='reasonList' value='' placeholder='산출근거를 입력해주세요.'>",
			etcHtml = "<input type='text' class='table-input' name='etcList' value='' placeholder=''>",
			btnHtml = "<button class='btn_delExpMore btn-sm-full btn-outline-gray' type='button'>삭제</button>";
		
		$(this).parents("tr").find(".amount").html(amoutHtml);
		$(this).parents("tr").find(".reason").html(reasonHtml);
		$(this).parents("tr").find(".etc").html(etcHtml);
		$(this).parents("tr").find(".contr").html(btnHtml);
	});
	
	//계정 목 추가
	$(document).on("click", ".btn_addExpMore", function(){
		var trHtml = "",
			code = $(this).parents("tbody.box_exp").find(".typeCodeList").val();
			rowspan = parseInt($(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan")) + 1,
			actSelHtml = "",
			expIdHtml = "<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='"+code+"'/>";
		
		for(i = 0; i < expDepth2.length; i++){
			if(i == 0){
				actSelHtml = "<select class='accountCodeList table-select select2' name='accountCodeList' data-select='style1'><option value=''>선택</option>";
			}
			
			if(code == expDepth2[i].upperCtgryId){
				actSelHtml += "<option value='"+expDepth2[i].ctgryId+"'>"+expDepth2[i].ctgryNm+"</option>";
			}
			
			if(i == expDepth2.length - 1){
				actSelHtml += "</select>" + expIdHtml;
			}
		}
		
		trHtml = "<tr>" +
					"<td>" + actSelHtml + "</td>" +
					"<td class='amount'><input type='text' class='table-input money onlyNum' name='amountList' value='' placeholder='금액 입력'></td>" +
					"<td class='reason'><input type='text' class='table-input' name='reasonList' value='' placeholder='산출근거를 입력해주세요.'></td>" +
					"<td class='etc'><input type='text' class='table-input' name='etcList' value='' placeholder=''></td>" +
					"<td class='contr'><button class='btn_delExpMore btn-sm-full btn-outline-gray' type='button'>삭제</button></td>" +
				"</tr>";
		
		$(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan", rowspan);
		$(this).parents("tbody.box_exp").find(".box_check").attr("rowspan", rowspan);
		$(this).parents("tr.foot").before(trHtml);
		Lms.common_ui.init();
		
		return false;
	});
	
	//단원추가
	$(".btn_addLes").click(function(){
		var tbodyHtml = "";
		
		tbodyHtml = "<tbody class='box_lesson'>" +
						 "<tr class='first'>" +
							"<td rowspan='2' class='num'>"+
							"<input type='hidden' name='lessonOrderList' class='lessonOrder' value='"+($(".box_lesson").length + 1)+"'>"+
							($(".box_lesson").length + 1) +"</td>" +
							"<td rowspan='2' class='lesnm'><textarea class='table-textarea h-full' placeholder='단원 내용을 입력해주세요.'></textarea></td>" +
							"<td class='left-align'><input type='text' class='table-input chasiList' name='chasiList' placeholder='학습 내용을 입력해주세요'/></td>" +
							"<td><button class='btn-sm-full btn-outline-gray btn_delCha' type='button'>삭제</button></td>" +
						"</tr>" +
						"<tr class='foot'><td colspan='2' class='btn-add-list btn_addChaMore'>과정내용 추가</td></tr>" +
					"</tbody>"
		
		$("#box_addLes").before(tbodyHtml);
		
		return false;
	});
	
	//과정내용 추가
	$(document).on("click", ".btn_addChaMore", function(){
		var trHtml = "",
			rowspan = parseInt($(this).parents("tbody.box_lesson").find(".num").attr("rowspan")) + 1;
		
		trHtml = "<tr class='contentBody'>" +
					"<td class='left-align'>" +
						"<input type='text' class='table-input chasiList' name='chasiList' placeholder='학습 내용을 입력해주세요'/>" +
					"</td>" +
					"<td><button class='btn-sm-full btn-outline-gray btn_delCha' type='button'>삭제</button></td>" +
				"</tr>";
		
		$(this).parents("tbody.box_lesson").find(".num").attr("rowspan", rowspan);
		$(this).parents("tbody.box_lesson").find(".lesnm").attr("rowspan", rowspan);
		$(this).parents("tr.foot").before(trHtml);
		return false;
	});
	
	//시수추가
	$(".btn_addTime").click(function(){
		var tbodyHtml = "",
			rand = randOnlyString(8);
		
		tbodyHtml = "<tbody class='box_staff'>" +
						 "<tr class='first' style='height:0px'>" +
						 	"<td rowspan='2' class='sisu'><textarea class='onlyNum timeList table-textarea h-full' name='timeList' placeholder='시수를 입력해주세요.'></textarea></td>" +
						 	"<td colspan='3' style='height:0px;padding:0px;'></td>" +
						 	//"<td class='line alC num'>1</td>" +
							//"<td class='line'></td>" +
							//"<td class='line alC'><a href='#' class='btn_delStf'><img src='/template/manage/images/btn/del.gif'></a></td>" +
						"</tr>" +
						"<tr class='foot "+rand+"'><td class='btn-add-list btn_addStfMore' colspan='3' data-modal-type='staff_search' data-rand='"+rand+"'>교원 검색 및 추가</td></tr>" +
					"</tbody>";
		
		$("#box_addTime").before(tbodyHtml);
		
		return false;
	});
	
	//교원검색 추가 팝업
	$(document).on("click", ".btn_addStfMore", function(){
		var rand = $(this).data("rand"),
			searchDept = "${curriculumVO.hostCode}";
		//window.open("/uss/umt/cmm/EgovMberManage.do?templateAt=N&position="+rand+"&searchDept="+searchDept, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
		window.open("/uss/umt/cmm/EgovMberManage.do?templateAt=N&position="+rand, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");

		return false;
	});
	
	//수료기준
	$(".radiocheck").change(function(){
		var val = $(this).val();
		if(val == "Y"){
			$(this).parents("td").find(".inpTxt").attr("readonly", false);
		}else{
			$(this).parents("td").find(".inpTxt").attr("readonly", true);
		}
	});
	
	//총괄평가 기준 추가
	/*
	$(".btn_addEvt").click(function(){
		window.open("/lms/manage/lmsEvtPopList.do", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=520,height=600");
		return false;
	});
	*/
	$("#btn_ok_evt").click(function(){
		var id = $(".chk:checked").val(),
			name = $(".chk:checked").data("nm"),
			stand = $(".chk:checked").parents("tr").find(".sel_stand").val();
		
		selEvt(id, name, stand);
		return false;
	});
	
	//교재검색
	$(".btn_addBook").click(function(){
		var lang = $(this).data("lang");
		window.open("/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_000000000005&tmplatImportAt=N&searchCateList="+lang, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=1050,height=600");
		return false;
	});
	
	box_lesson();
	
});

//교원삭제
$(document).on("click", ".btn_del", function(){
	$(this).parents("li").remove();
	$(this).parents("tr").remove();
	return false;
});

//지원비삭제
/*
$(document).on("click", ".btn_delExp", function(){
	$(this).parents("tbody.box_exp").remove();
	return false;
});
*/
$(document).on("click", "#btn_delExp", function(){
	$(".expCheck:checked").each(function(){
		$(this).parents("tbody.box_exp").remove();
	});
	return false;
});


//계정목삭제
$(document).on("click", ".btn_delExpMore", function(){
	var rowspan = $(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan") - 1;
	
	if(trCnt = $(this).parents("tr").hasClass("firstExp")){
		alert("계정 목 최상단은 삭제 하실 수 없습니다.");
	}else{
		$(this).parents("tbody.box_exp").find(".box_selExp").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	return false;
});

//과정내용삭제
$(document).on("click", ".btn_delCha", function(){
	var rowspan = $(this).parents("tbody.box_lesson").find(".num").attr("rowspan") - 1;
	var innerHtml = '';
	if($(this).parents("tr").hasClass("first")){
		if(confirm("과정내용 최상단 삭제 시 해당 단원명 및 과정내용이 같이 삭제 됩니다. 삭제하시겠습니까?")){
			$(this).parents("tbody.box_lesson").remove();
			$("tbody.box_lesson .num").each(function(i){
				innerHtml = (i+1)+'<input type="hidden" value='+(i+1)+' name="lessonOrderList">';
				$(this).html(innerHtml);
			});
		}
	}else{
		$(this).parents("tbody.box_lesson").find(".num").attr("rowspan", rowspan);
		$(this).parents("tbody.box_lesson").find(".lesnm").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	
	return false;
});

//시수 입력 시
$(document).on("change", ".timeList", function(){
	sisuTotCnt();
});

//교원삭제
$(document).on("click", ".btn_delStf", function(){
	var rowspan = $(this).parents("tbody.box_staff").find(".sisu").attr("rowspan") - 1;
	
	if($(this).parents("tr").hasClass("first")){
		if(confirm("과정내용 최상단 삭제 시 해당 시수 및 교원이 같이 삭제 됩니다. 삭제하시겠습니까?")){
			$(this).parents("tbody.box_staff").remove();
		}
	}else{
		$(this).parents("tbody.box_staff").find(".sisu").attr("rowspan", rowspan);
		$(this).parents("tr").remove();
	}
	
	sisuTotCnt();
	facTotCnt();
	
	return false;
});

//총괄평과 입력 시
$(document).on("change", ".evtper", function(){
	totperCnt();
});

//총괄평가삭제
$(document).on("click", ".btn_delEvt", function(){
	$(this).parents("tr").remove();
	totperCnt();
	return false;
});

//게시판선택
$(document).on("click", ".btn_bbs", function(){
	var id = $(this).data("id");
	
	window.open("/lms/manage/lmsBbsPopList.do?crclId="+id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=650,height=770");
	return false;
});

//게시판삭제
$(document).on("click", ".btn_delbbs", function(){
	$(this).parents("li").remove();
	return false;
});

//교재삭제
$(document).on("click", ".btn_delBook", function(){
	$(this).parents("tr").remove();
	bookNumSort();
	return false;
});


function selcrclb(id, name, code1, code2, code3){
	$.ajax({
		url : "/lms/crclb/selectCurriculumbaseAjax.do"
		, type : "post"
		, dataType : "html"
		, data : {crclbId : id}
		, success : function(data){
			$("#box_base").html(data);
			
			$(".base_name").text(name);
			$("#crclbId").val(id);
			
			$("#ctgryId1").val(code1).attr("selected", true);
			$("#ctgryId1").change();
			$("#ctgryId2").val(code2).attr("selected", true);
			$("#ctgryId2").change();
			$("#ctgryId3").val(code3).attr("selected", true);
			$("#ctgryId3").change();
		}, error : function(){
			alert("기본과정을 불러오는데 실패했습니다.");
		}
	});
}

function selectcrclb(id){
	$.ajax({
		url : "/lms/crclb/selectCurriculumbaseAjax.do"
		, type : "post"
		, dataType : "html"
		, data : {crclbId : id}
		, success : function(data){
			$("#box_base").html(data);
		}, error : function(){
			alert("기본과정을 불러오는데 실패했습니다.");
		}
	});
}

function autoTxt(){
	var yearTxt = $("#crclYear").val() + "년도",
		crclTermTxt = "",
		crclLangTxt = "",
		ctgryId1Txt = "",
		ctgryId2Txt = "",
		ctgryId3Txt = "";
	
	if($("#crclTerm").val()){
		crclTermTxt = $("#crclTerm > option:selected").text();
	}
	if($("#crclLang").val()){
		crclLangTxt = $("#crclLang > option:selected").text();
	}
	if($("#ctgryId1").val()){
		ctgryId1Txt = $("#ctgryId1 > option:selected").text();
	}
	if($("#ctgryId2").val()){
		ctgryId2Txt = $("#ctgryId2 > option:selected").text();
	}
	if($("#ctgryId3").val()){
		ctgryId3Txt = $("#ctgryId3 > option:selected").text();
	}
	
	var txt = yearTxt + " " + crclTermTxt + " " + crclLangTxt + " " + ctgryId1Txt + " " + ctgryId2Txt + " " + ctgryId3Txt;
	//$("#crclNm").val(txt);
}

function selUser(id, name, mngdept, mngdeptnm, position){
	var trHtml = "",
		nameStr = "",
		idStr = "",
		facCnt = $(".sumUser").text();
	
	for(i = 0; i < id.length; i++){
		var rowspan = parseInt($("."+position).parents("tbody.box_staff").find(".sisu").attr("rowspan")) + 1,
			prfNum = $("."+position).parents("tbody.box_staff").find(".box_prf").length + 1;
		
		nameStr = name[i] + "(" + mngdeptnm[i] + ")";
		
		trHtml = "<tr class='box_prf'>" +
				 	"<td class='num'>"+prfNum+"</td>" +
					"<td class='left-align'>" +
						nameStr +
						"<input type='hidden' class='facIdList' name='facIdList' value='"+id[i]+"'/>" +
						"<input type='hidden' class='facNmList' name='facNmList' value='"+nameStr+"'/>" +
						"<input type='hidden' name='positionList' value='"+position+"'/>" +
					"</td>" +
					"<td><button class='btn-sm-full btn-outline-gray btn_delStf' type='button'>삭제</button></td>" +
				 "</tr>";
		
		$("."+position).parents("tbody.box_staff").find(".sisu").attr("rowspan", rowspan);
		$("tr."+position).before(trHtml);
	}
	
	//교원 수
	facTotCnt();
}

//교원 수
function facTotCnt(){
	var dupCnt = 0,
		ids = [],
		idList = [];
	
	$(".facIdList").each(function(){
		var id = $(this).val(),
			dupAt = false;
		$.each(idList, function(i){
			if(idList[i] == id){
				dupAt = true;
			}
		});
		
		if(!dupAt){
			idList.push(id);
		}
	});
	
	$(".sumUser").text(idList.length);
}

//시수 계산
function sisuTotCnt(){
	var sisuCnt = 0;
	
	//총 시수
	$(".timeList").each(function(){
		sisuCnt += Number($(this).val());
	});
	
	$(".curTime").text(sisuCnt);
}

//총괄평과 계산
function totperCnt(){
	var totper = 0,
		emptyHtml = "";
	
	if($(".evtper").length > 0){
		$(".evtper").each(function(){
			totper += Number($(this).val());
		});
	}else{
		emptyHtml = "<tr class='box_empty'><td class='alC' colspan='3'>아래 [ + 총괄평가 기준 추가하기] 버튼을 눌러 성적 대상을 선택하세요.</td></tr>";
		$(".box_evt").html(emptyHtml);
	}
	
	$(".totper").text(totper);
}

//게시판
function selBbs(id, name, colect){
	var html = "",
		bbsCnt = 0;
	
	$("input[name=bbsIdList]").each(function(){
		if($(this).val() == id){
			bbsCnt++;
			return false;
		}
	});
	
	if(bbsCnt == 0){
		html = "<li class='item bg-blue-light'>" +
					name +
					"<input type='hidden' name='bbsIdList' value='"+id+"'/>" +
					"<input type='hidden' name='colectList' value='"+colect+"'/>" +
					"<button type='button' class='btn-remove btn_delbbs'>삭제</button>" +
				"</li>";
		
		$("#box_bbsmaster").append(html);	
	}else{
		alert("이미 등록된 게시판입니다.");
	}
}

//총괄평가기준추가
function selEvt(id, name, stand){
	var idCnt = $(".box_evt").find("."+id).length,
		html = "",
		evtSel = "<c:forEach var="result" items="${homeworkComCode}"><option value='${result.code}'>${result.codeNm}</option></c:forEach>";
	
	if(idCnt == 0 || id == "CTG_0000000000000119"){
		if(id == "CTG_0000000000000119"){//기타
			html = "<tr class='"+id+"'>" +
						"<td class='left-align'>" +
							"<input type='text' class='table-input' name='evtNmList' value='"+name+"' placeholder='기타 기준을 입력해주세요.'>"
						"</td>";
		}else{
			html = "<tr class='"+id+"'>" +
						"<td class='left-align'>" +
							name +
							"<input type='hidden' name='evtNmList' value='"+name+"'/>" +
						"</td>";
		}
		
		html += "<input type='hidden' name='evtIdList' value='"+id+"'/>" +
				"<input type='hidden' name='evtStandList' value='"+stand+"'/>";
		
		if(id == "CTG_0000000000000115"){//과제
			html += "<td>" +
						"<div class='flex-row'>" +
	                        "<div class='flex-col-6'>" +
	                          "<div class='input-group'>" +
	                          	"<span class='unit font-gray-light'>%</span>" +
	                          	"<input type='number' class='evtper onlyNum table-input' name='evtValList'/>" +
	                          "</div>" +
	                        "</div>" +
	                        "<div class='flex-col-6'>" +
	                        	"<select class='table-select select2' data-select='style1' name='evtType'>" +
	                            	evtSel +
	                            "</select>" +
	                        "</div>" +
	                      "</div>" +
					"</td>";
		}else if(id == "CTG_0000000000000117"){//수업참여도
			html += "<td>" +
						"<div class='flex-row'>" +
	                        "<div class='flex-col'>" +
	                          "<div class='input-group'>" +
	                          	"<span class='unit font-gray-light'>%</span>" +
	                          	"<input type='number' class='table-input evtper onlyNum' name='evtValList'></div>" +
	                        "</div>" +
	                        "<div class='flex-col-auto'>" +
	                        	"<button type='button' class='btn-sm-110 btn-outline-gray btn_bbs' data-id='${curriculumVO.crclId}'>게시판 선택</button>" +
	                        "</div>" +
	                        "<div class='flex-col-12'>" +
	                          "<ul id='box_bbsmaster' class='selected-items-wrap row-full'></ul>" +
	                        "</div>" +
	                      "</div>" +
					"</td>";
		}else{
			html += "<td>" +
						"<div class='input-group'>" +
							"<span class='unit font-gray-light'>%</span><input type='number' class='table-input evtper onlyNum' name='evtValList'>" +
						"</div>" +
					"</td>";
		}
		
		html += "<td><button class='btn-sm-full btn-outline-gray btn_delEvt' type='button'>삭제</button></td>" +
				"</tr>";
				
		$(".box_evt").append(html);
		$(".box_evt .box_empty").remove();
	}else{
		alert("["+ name +"]은(는) 이미 추가된 총괄평가 기준입니다.");
	}
}

//교재 및 부교재 추가
function addbook(id, sj, publish){
	var html = "",
		bookCnt = 0;
	
	$(".nttNoList").each(function(){
		if($(this).val() == id){
			bookCnt++;
			return false;
		}
	});
	
	if(bookCnt == 0){
		$(".list_book .empty").remove();
		
		html = "<tr>" +
					"<td class='num center-align'></td>" +
					"<td>" +
						sj +
						"<input type='hidden' class='nttNoList' name='nttNoList' value='"+id+"'/>" +
					"</td>" +
					"<td>"+publish+"</td>" +
					"<td><button class='btn-sm-full btn-outline-gray btn_delBook' type='button'>삭제</button></td>" +
				"</tr>";
			
		$(".list_book").append(html);
		bookNumSort();
	}else{
		alert("이미 등록된 교재입니다.");
	}
}

//교재번호 재정렬
function bookNumSort(){
	$(".list_book tr").each(function(i){
		$(this).find(".num").text(i + 1);
	});
}

//폼 validator
function vali(){
	<%-- 수강신청 전 --%>
	<c:if test="${processSttusCode < 3}">
		if($("#box_user li").length == 0){
			alert("책임교원을 등록해주세요.");
			return false;
		}
	</c:if>
	
	$("select[name=accountCodeList]").each(function(){
		if(!$(this).val()){
			$(this).parents("tr").remove();
		}
	});
	
	
	<c:if test="${processSttusCode > 0}">
		//과정내용
		$("textarea[name=lessonNmList]").remove();
		$(".lesnm").each(function(){
			var rownum = $(this).attr("rowspan"),
				text = $(this).children("textarea").val();
			for(i = 1; i < rownum; i++){
				textarea = "<textarea class='wid100 hei100' name='lessonNmList'>"+text+"</textarea>";
				$("#box_lesnm").append(textarea);
			}
		});
		
		//시수-교원배정
		$("input[name=sisuList]").remove();
		$(".facIdList").each(function(){
			var sisu = $(this).parents("tbody.box_staff").find(".timeList").val();
			
			$(this).parents("td").append("<input type='hidden' name='sisuList' value='"+sisu+"'/>");
		});
		
		//총괄평가기준
		if($(".totper").text().trim() != "100"){
			alert("총괄평가기준 성적 반영률이 100%인지 확인해주세요.");
			return false;
		}
	</c:if>
	
	// 과정내용 
	var totalContentList = '';  	// 전체 문자열
	var totalContentArr = [];  		// 전체 문자배열
	var totalContentLen = 0;  		// 전체 과정내용 길이
	
	var contentList = '';  	  		
	var contentArr = [];  	  
	var contentBodyLen = 0;  		// 과정내용 길이(첫번재 row 제외)
	var contentRow  = 0 ;			// 과정내용 번호
	var contentLessonNm = ''		// 내용범주
	var lessonNmCnt = 0			// 내용범주
	var contentChasiNm ='';			// 과정내용의 내용
	var contentChasiNmList ='';		// 과정내용의 문자열
	var contentChasiNmArr =[];		// 과정내용의 내용
	
	totalContentLen = $(".box_lesson").length;
	for(var i = 0 ; i < totalContentLen ; i++){
		contentList = '';
		contentArr = [];
		contentChasiNmArr = [];
		contentChasiNmList = '';
		
		contentRow = 0;
		contentRow = $(".box_lesson").eq(i).find(".first > td > input[name=lessonOrderList]").val();
		
		contentLessonNm  = '';
		if($(".box_lesson").eq(i).find(".first > .lesnm").find("textarea").val() == '')
			lessonNmCnt = lessonNmCnt+1;
		
		contentLessonNm = $(".box_lesson").eq(i).find(".first > .lesnm").find("textarea").val();
		
		contentChasiNm = '';
		
		if($(".box_lesson").eq(i).find(".first > td > input[name=chasiList]").val() == '')
			contentChasiNm =  'emptyContentChasiNm';
		else
			contentChasiNm =  $(".box_lesson").eq(i).find(".first > td > input[name=chasiList]").val();
		
		contentChasiNmArr.push(contentChasiNm);

		contentBodyLen = $(".box_lesson").eq(i).find(".contentBody").length;
	
		if(contentBodyLen > 0 ){
			contentChasiNm = '';
			
			for(var j = 0;  j < contentBodyLen ;  j++){
				
				contentChasiNm = $(".box_lesson").eq(i).find(".contentBody .chasiList").eq(j).val();
				if(contentChasiNm != '')
					contentChasiNmArr.push(contentChasiNm);
				else 
					contentChasiNmArr.push("emptyContentChasiNm");
			}
			
		}
		
		contentChasiNmList = contentChasiNmArr.join("#third#");
		
		contentArr.push(contentRow);
		contentArr.push(contentLessonNm);
		contentArr.push(contentChasiNmList);
		contentList = contentArr.join("#second#");
		
		totalContentArr.push(contentList);
	}
	
	totalContentList = totalContentArr.join("#first#");
	$("#lessonContentList").val(totalContentList);
	
	console.log("totalContentList : "+totalContentList);
	
	if(lessonNmCnt > 0){
		alert("입력하지 않은 내용범주가 있습니다.\n내용범주를 입력해 주세요.");
		return false;
	}
	
	return;
}

function box_lesson(){
	var htmls = '';
	var crclId = '${curriculumVO.crclId}';
	var preLessonOrder = 0;
	var lessonOrderCnt = 0;
	var totLen =0;
	
	$.ajax({
           type: "POST",
           url: "/ajax/mng/lms/crcl/selectCurriculumLesson.json",
           data: {                
               "crclId" : crclId
               },
           dataType: "json",
           async: false,
           success: function(data, status) {
           	$(".box_lesson > tr").remove();
           	if(data.status == "200"){
           		totLen=data.lessonList.length;
           		
           		$.each(data.lessonList, function(index){
           			
           			if(lessonOrderCnt == 0){
           				lessonOrderCnt = this.lessonOrderCnt;
           				preLessonOrder = this.lessonOrder;
    					htmls += '<tbody class="box_lesson">';
    					htmls += '<tr class="first">';
   					} else {
    					htmls += '<tr class="contentBody">';
   						
   					}
                   	 
   					if((lessonOrderCnt == this.lessonOrderCnt) && (preLessonOrder == this.lessonOrder)){
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="num">';
   						htmls += this.lessonOrder;
   						htmls += '<input type="hidden" name="lessonOrderList" class="lessonOrder" value="'+this.lessonOrder+'"></td>';
   						htmls += '<td rowspan="'+(this.lessonOrderCnt+1)+'" class="lesnm"><textarea class="table-textarea h-full" placeholder="단원 내용을 입력해주세요.">'+this.lessonNm+'</textarea></td>';
   					}
           			
					htmls += '<td class="left-align"><input type="text" class="table-input chasiList" name="chasiList" placeholder="학습 내용을 입력해주세요" value="'+this.chasiNm+'"/></td>';
					htmls += '<td><button class="btn-sm-full btn-outline-gray btn_delCha" type="button">삭제</button></td>';
					htmls += '</tr>';
					if((lessonOrderCnt-1) == 0){
						htmls += '<tr class="foot"><td colspan="2" class="btn-add-list btn_addChaMore">과정내용 추가</td></tr>';
    					htmls += '</tbody">';
						
					}
					preLessonOrder = this.lessonOrder;
   					--lessonOrderCnt;
           		});
           		
				$("#box_addLes").before(htmls);
           	}
           },
           error: function(xhr, textStatus) {
               alert("문제가 발생하였습니다. \n 관리자에게 문의하세요");
               document.write(xhr.responseText);
               return false;
           },beforeSend:function() {
           },
           complete:function() {
           }
       }); 
}
</script>

<c:if test="${processSttusCode > 0}">
<div class="page-content-header">
	<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
		<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
	</c:import>
	<!-- TO DO
	<div class="util-wrap">
     <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
   </div>
    -->
</div>
</c:if>
<c:if test="${curriculumVO.processSttusCode > 0}">
	<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
		<c:param name="step" value="3"/>
		<c:param name="crclId" value="${curriculumVO.crclId}"/>
		<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
		<c:param name="curriculumAt" value="Y"/>
		<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	</c:import>
</c:if>

		<div class="page-content-wrap">
			<form:form commandName="curriculumVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return vali();">
				<input type="hidden" name="retViewAt" value="Y"/>
				<input type="hidden" id="totalTimeAt" value="<c:out value="${curriculumVO.totalTimeAt eq 'Y' ? 'Y' : 'N'}"/>"/>
				<input type="hidden" id="lessonContentList" name="lessonContentList" value=""/>
	          <section class="page-content-body">
	          	<c:choose>
					<c:when test="${processSttusCode < 3}">
						<article class="content-wrap">
			              <!-- 기본과정 선택 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">기본과정 선택</div>
			                </div>
			                <!-- <p class="notice font-700">* 년도, 학기, 언어, 과정체계를 입력하시면 과정명이 자동으로 표기됩니다.(수정가능)</p> -->
			              </div>
			              <div class="content-body">
			                <table class="common-table-wrap table-style2 ">
			                  <tbody>
			                    <tr>
			                      <th class="title">년도 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                          	<c:choose>
			                          		<c:when test="${not empty curriculumVO.crclYear}">
			                          			<c:set var="year" value="${curriculumVO.crclYear}"/>
			                          		</c:when>
			                          		<c:otherwise>
			                          			<c:set var="year" value="${todayYear}"/>
			                          		</c:otherwise>
			                          	</c:choose>
			                            <select id="crclYear" name="crclYear" class="table-select select2 autoTxt" data-select="style1">
											<c:forEach var="result" items="${yearList}" varStatus="status">
								  				<option value="${yearList[status.index]}" <c:if test="${year eq yearList[status.index]}">selected="selected"</c:if>>${yearList[status.index]}</option>
								  			</c:forEach>
										</select>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">학기 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                            <select id="crclTerm" name="crclTerm" class="autoTxt table-select select2" data-select="style1" data-placeholder="선택" required>
											<option value="">선택</option>
											<c:forEach var="result" items="${crclTermList}" varStatus="status">
												<c:if test="${result.ctgryLevel eq '1'}">
													<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.crclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
												</c:if>
											</c:forEach>
										</select>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">언어 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-auto">
			                            <div class="table-data">
											<c:forEach var="result" items="${langList}" varStatus="status">
												<c:if test="${result.ctgryLevel eq '1' and result.ctgryId eq USER_INFO.major}">
													<c:out value="${result.ctgryNm}"/>
													<input type="hidden" id="crclLang" name="crclLang" value="${result.ctgryId}"/>
												</c:if>
											</c:forEach>
			                            </div>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정체계 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                            <select id="ctgryId1" name="sysCodeList" class="table-select select2" data-select="style1" data-placeholder="대분류" required>
			                              <option value="">대분류</option>
			                              <c:forEach var="result" items="${sysCodeList}" varStatus="status">
											<c:if test="${result.ctgryLevel eq '1'}">
												<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq sysCode[1]}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
											</c:if>
										  </c:forEach>
			                            </select>
			                          </div>
			                          <div class="flex-col-4">
			                            <select id="ctgryId2" name="sysCodeList" required class="table-select select2 " data-select="style1" data-placeholder="중분류">
			                              <option value=""></option>
			                            </select>
			                          </div>
			                          <div class="flex-col-4">
			                            <select id="ctgryId3" name="sysCodeList" required class="table-select select2 autoTxt" data-select="style1" data-placeholder="소분류">
			                              <option value=""></option>
			                            </select>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">기본과정 선택 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                            <select id="crclbId" name="crclbId" required class="table-select select2 " data-select="style1" data-placeholder="과정체계를 먼저 선택해 주세요.">
			                              <option value=""></option>
			                            </select>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>
			              </div>
			            </article>
					</c:when>
					<c:otherwise>
						<article class="content-wrap">
			              <!-- 기본과정 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">기본과정</div>
			                </div>
			              </div>
			              <div class="content-body">
			                <!-- 테이블영역-->
			                <table class="common-table-wrap size-sm left-align mb-20">
			                  <colgroup>
			                    <col class='bg-gray' style='width:20%'>
			                    <col style='width:30%'>
			                    <col class='bg-gray' style='width:20%'>
			                    <col style='width:30%'>
			                  </colgroup>
			                  <tbody>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>년도</td>
			                      <td><c:out value="${curriculumVO.crclYear }"/></td>
			                      <td scope='row' class='font-700 keep-all'>학기</td>
			                      <td><c:out value="${curriculumVO.crclTermNm}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>언어</td>
			                      <td colspan='3'><c:out value="${curriculumVO.crclLangNm}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정체계</td>
			                      <td colspan="3"><c:out value="${curriculumVO.sysCodeNmPath}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>기본과정명</td>
			                      <td colspan="3"><c:out value="${curriculumVO.crclbNm}"/></td>
			                    </tr>
			                  </tbody>
			                </table>
			              </div>
			            </article>
					</c:otherwise>
				</c:choose>
				<div id="box_base">
					<c:if test="${not empty curriculumVO.crclbId}">
						<article class="content-wrap">
						    <div class="content-header">
						      <div class="title-wrap">
						        <div class="title">기본정보</div>
						      </div>
						    </div>
						    <div class="content-body">
						      <!-- 테이블영역-->
						      <table class="common-table-wrap size-sm left-align">
						        <colgroup>
						          <col class='bg-gray' style='width:20%'>
						          <col style='width:30%'>
						          <col class='bg-gray' style='width:20%'>
						          <col style='width:30%'>
						        </colgroup>
						        <tbody>
						          <tr class="">
						            <td scope='row' class='font-700'>이수구분 <i class='font-point font-400'>*</i></td>
						            <td>${curriculumVO.divisionNm}</td>
						            <td scope='row' class='font-700'>학점인정 여부</td>
						            <c:choose>
						            	<c:when test="${curriculumVO.gradeAt eq 'Y'}">
						            		<td>예(${curriculumVO.gradeNum} 학점)</td>
						            	</c:when>
						            	<c:otherwise>
						            		<td class='font-red'>아니오</td>
						            	</c:otherwise>
						            </c:choose>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>관리구분 <i class='font-point font-400'>*</i></td>
						            <td>${curriculumVO.controlNm }</td>
						            <td scope='row' class='font-700'>프로젝트 과정 여부</td>
						            <td>${curriculumVO.projectAt eq 'Y' ? '프로젝트 과정' : '프로젝트 과정 아님' }</td>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>대상 <i class='font-point font-400'>*</i></td>
						            <td>${curriculumVO.targetType eq 'Y' ? '본교생' : '일반' } > ${curriculumVO.targetDetailNm }</td>
						            <td scope='row' class='font-700'>총 시간 등록과정 여부</td>
						            <td>${curriculumVO.totalTimeAt eq 'Y' ? '총 시간 등록과정' : '총 시간 등록과정 아님' }</td>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>성적처리기준 <i class='font-point font-400'>*</i></td>
						            <td colspan='3'>${curriculumVO.evaluationAt eq 'Y' ? '절대' : '상대' }평가 
						            <c:choose>
						            	<c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">> 100점 기준 점수 환산표(Letter Grade)</c:when>
						            	<c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">> P/F</c:when>
						            </c:choose>
						            </td>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>운영보고서 유형 <i class='font-point font-400'>*</i></td>
						            <td colspan='3'>
						            	<c:choose>
						            		<c:when test="${curriculumVO.reportType eq 'PRJ'}">프로젝트과정</c:when>
						            		<c:when test="${curriculumVO.reportType eq 'NOR'}">일반과정</c:when>
						            	</c:choose>
						            </td>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
						            <td>
						            	<c:forEach var="result" items="${surveyList}" varStatus="status">
											<c:if test="${result.schdulId eq curriculumVO.surveySatisfyType}">${result.schdulNm}</c:if>
										</c:forEach>
						            </td>
						            <td scope='row' class='font-700'>교원대상 과정만족도 설문 유형 <i class='font-point font-400'>*</i></td>
						            <td>
						            	<c:choose>
											<c:when test="${empty curriculumbaseVO.professorSatisfyType}">
												선택안함
											</c:when>
											<c:otherwise>
												<c:forEach var="result" items="${surveyList3}" varStatus="status">
													<c:if test="${result.schdulId eq curriculumVO.professorSatisfyType}">${result.schdulNm}</c:if>
												</c:forEach>
											</c:otherwise>
										</c:choose>
						            </td>
						          </tr>
						          <tr class="">
						            <td scope='row' class='font-700'>개별 수업 만족도 조사 실시 여부 <i class='font-point font-400'>*</i></td>
						            <c:choose>
						            	<c:when test="${curriculumVO.surveyIndAt eq 'Y'}">
						            		<td>예</td>
						            	</c:when>
						            	<c:otherwise>
						            		<td class='font-red'>아니오</td>
						            	</c:otherwise>
						            </c:choose>
						            <td scope='row' class='font-700'>학생 수강신청 제출서류 여부 <i class='font-point font-400'>*</i></td>
						            <c:choose>
						            	<c:when test="${curriculumVO.stdntAplyAt eq 'Y'}">
						            		<td>예</td>
						            	</c:when>
						            	<c:otherwise>
						            		<td class='font-red'>아니오</td>
						            	</c:otherwise>
						            </c:choose>
						          </tr>
						        </tbody>
						      </table>
						    </div>
						</article>
					</c:if>
				</div>
				
				<c:choose>
					<c:when test="${processSttusCode < 3}">
			            <article class="content-wrap mt-50">
			              <!-- 교육과정 등록 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">교육과정 등록</div>
			                </div>
		                	<p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
			              </div>
			              <div class="content-body">
			                <table class="common-table-wrap table-style2 ">
			                  <tbody>
			                    <tr>
			                      <th class="title">과정명 </th>
			                      <td>
			                      	<div class="flex-row">
			                          <div class="flex-col-12">
			                            <form:input path="crclNm" cssClass="table-input" required="true"/>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정기간 </th>
			                      <td>
			                        <div class="flex-row pb-5">
			                          <div class="flex-col-3">
			                            <form:input path="startDate" required="true" cssClass="ell date datepicker" placeholder="시작일" readonly="true"/>
			                          </div>
			                          <div class="flex-col-auto">
			                            <div class="table-data">
			                              ~ </div>
			                          </div>
			                          <div class="flex-col-3">
			                            <form:input path="endDate" required="true" cssClass="ell date datepicker" placeholder="종료일" readonly="true"/>
			                          </div>
			                        </div>
			                        <c:if test="${curriculumVO.totalTimeAt eq 'Y' or _MODE eq 'REG'}">
				                        <div class="flex-row timeAt">
				                          <div class="flex-ten-col-2">
				                            <div class="input-group">
				                              <span class="title">총</span>
				                              <span class="unit">시간</span>
				                              <input type="number" id="totalTime" name="totalTime" value="${curriculumVO.totalTime}" class="onlyNum table-input" required="required"/>
				                            </div>
				                          </div>
				                          <div class="flex-ten-col-2">
				                            <div class="input-group">
				                              <span class="title">주</span>
				                              <span class="unit">회</span>
				                              <input type="number" id="weekNum" name="weekNum" value="${curriculumVO.weekNum}" class="onlyNum table-input" required="required"/>
				                            </div>
				                          </div>
				                          <div class="flex-ten-col-4">
				                            <div class="input-group">
				                              <span class="title">요일</span>
				                              <input type="text" id="lectureDay" name="lectureDay" value="${curriculumVO.lectureDay}" class="table-input" placeholder="강의요일 입력 ex.월,화(주 2회)" required="required"/>
				                            </div>
				                          </div>
				                        </div>
				                        <div class="flex-row timeAt">
				                          <div class="flex-ten-col-2">
				                            <div class="input-group">
				                              <span class="title">일</span>
				                              <span class="unit">시간</span>
				                              <input type="number" id="dayTime" name="dayTime" value="${curriculumVO.dayTime}" class="onlyNum table-input" required="required"/>
				                            </div>
				                          </div>
				                          <div class="flex-ten-col-3">
				                            <div class="input-group">
				                              <span class="title">시작시간</span>
				                              <input type="text" id="startTime" name="startTime" value="${curriculumVO.startTime}" class="table-input" placeholder="시작시간 ex.오후 3:00" required="required"/>
				                            </div>
				                          </div>
				                        </div>
				                    </c:if>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정 진행 캠퍼스 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                            <select id="campusId" name="campusId" required class="table-select select2 " data-select="style1" data-placeholder="캠퍼스 선택">
			                              <option value=""></option>
			                              <c:forEach var="result" items="${campusList}" varStatus="status">
											<c:if test="${result.ctgryLevel eq '1'}">
												<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.campusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
											</c:if>
										  </c:forEach>
			                            </select>
			                          </div>
			                          <div class="flex-col-4">
			                            <form:input path="campusPlace" cssClass="table-input" required="true" placeholder="상세장소 입력"/>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">수업 시간표 설정 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="campustimeUseAt" class="table-checkbox" value="Y" <c:if test="${curriculumVO.campustimeUseAt ne 'N'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">캠퍼스 시간표</span>
			                            </label>
			                          </div>
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="campustimeUseAt" class="table-checkbox" value="N" <c:if test="${curriculumVO.campustimeUseAt eq 'N'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">시간표 수기입력</span>
			                            </label>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">주관기관/학과 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
										<c:forEach var="result" items="${insList}" varStatus="status">
											<c:choose>
												<c:when test="${empty curriculumVO.hostCode}">
													<c:if test="${result.ctgryLevel eq '1' and result.ctgryId eq USER_INFO.mngDeptCode}">
														<c:out value="${result.ctgryNm}"/>
														<input type="hidden" id="hostCode" name="hostCode" value="${result.ctgryId}"/>
													</c:if>
												</c:when>
												<c:otherwise>
													<c:if test="${result.ctgryLevel eq '1' and result.ctgryId eq curriculumVO.hostCode}">
														<c:out value="${result.ctgryNm}"/>
														<input type="hidden" id="hostCode" name="hostCode" value="${result.ctgryId}"/>
													</c:if>
												</c:otherwise>
											</c:choose>
										  </c:forEach>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">책임교수 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-4">
			                          	<input type="text" id="searchUser" placeholder="교원 명을 입력해주세요."/>
			                          	<%-- 
			                            <select class="table-select select2 search" data-select="style1" data-placeholder="이름 검색">
			                              <option value=""></option>
			                              <option value="0">항목</option>
			                            </select>
			                             --%>
			                          </div>
			                          <div class="flex-col-12">
			                            <ul id="box_user" class="selected-items-wrap">
			                            	<c:choose>
			                            		<c:when test="${not empty curriculumVO.userId}">
			                            			<c:set var="userId" value="${fn:split(curriculumVO.userId,',')}"/>
													<c:set var="userNm" value="${fn:split(curriculumVO.userNm,',')}"/>
													
													<c:forEach var="user" items="${userId}" varStatus="status">
														<li class="item bg-blue-light">
															<input type='hidden' name='userIdList' value='${userId[status.count-1]}'/>
															<c:out value="${userNm[status.count-1]}"/>(${userId[status.count-1]})
															<button type="button" class="btn-remove btn_del" title="삭제"></button>
														</li>
													</c:forEach>
			                            		</c:when>
			                            		<c:otherwise>
			                            			<li class="item bg-blue-light">
														<input type='hidden' name='userIdList' value='${USER_INFO.id}'/>
														<c:out value="${USER_INFO.name}"/>(${USER_INFO.id})
														<button type="button" class="btn-remove btn_del" title="삭제"></button>
													</li>
			                            		</c:otherwise>
			                            	</c:choose>
			                              <!-- 
			                              <li class="item bg-blue-light">Saranzaya Pureviav<button type="button" class="btn-remove" title="삭제"></button></li>
			                               -->
			                            </ul>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정 개요 및 목표 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-12">
			                            <form:textarea path="crclGoal" cssClass="content-textarea" placeholder="과정 목표를 입력해주세요."/>
			                            <%-- <form:textarea path="crclGoal" cssClass="inp_long" cssStyle="height:100px;"/> --%>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정의 기대효과 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-12">
			                            <form:textarea path="crclEffect" cssClass="content-textarea" placeholder="과정 기대효과를 입력해주세요."/>
			                            <%-- 
			                            <form:textarea path="crclEffect" cssClass="inp_long" cssStyle="height:100px;"/>
			                             --%>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">과정계획서 작성기간 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-3">
			                            <form:input path="planStartDate" cssClass="ell date datepicker" placeholder="시작일" readonly="true"/>
			                          </div>
			                          <div class="flex-col-auto">
			                            <div class="table-data">
			                              ~ </div>
			                          </div>
			                          <div class="flex-col-3">
			                            <form:input path="planEndDate" cssClass="ell date datepicker" placeholder="종료일" readonly="true"/>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">수강신청기간 </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-3">
			                            <form:input path="applyStartDate" required="true" cssClass="ell date datepicker" placeholder="시작일" readonly="true"/>
			                          </div>
			                          <div class="flex-col-auto">
			                            <div class="table-data">
			                              ~ </div>
			                          </div>
			                          <div class="flex-col-3">
			                            <form:input path="applyEndDate" required="true" cssClass="ell date datepicker" placeholder="종료일" readonly="true"/>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">수강정원 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-ten-col-3">
			                            <div class='input-group'>
			                            	<label class='checkbox circle'>
			                            		<input type="radio" name="applyMaxCntAt" class="table-checkbox" value="Y" <c:if test="${curriculumVO.applyMaxCnt != 0}">checked="checked"</c:if>/>
				                            	<span class='custom-checked'></span>
				                            	<span class='text'>인원제한</span>
			                            	</label>
			                            	<input type="number" id="applyMaxCnt" name="applyMaxCnt" value="${curriculumVO.applyMaxCnt}" class="onlyNum money num table-input"/>
			                            	<span class='unit'>명</span>
			                            </div>
			                          </div>
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                            	<input type="radio" name="applyMaxCntAt" class="table-checkbox" value="N" <c:if test="${curriculumVO.applyMaxCnt == 0}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">제한없음</span>
			                            </label>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">주관학과 배정여부 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="assignAt" class="table-checkbox" value="Y" <c:if test="${curriculumVO.assignAt eq 'Y'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">예</span>
			                            </label>
			                          </div>
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="assignAt" class="table-checkbox" value="N" <c:if test="${curriculumVO.assignAt ne 'Y'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">아니오</span>
			                            </label>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">등록금 납부대상과정 여부 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="tuitionAt" class="table-checkbox" value="Y" <c:if test="${curriculumVO.tuitionAt eq 'Y'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">예</span>
			                            </label>
			                            (
			                            <label class="checkbox">
				                            <input type="checkbox" name="tuitionFeesAt" class="table-checkbox" value="Y" <c:if test="${not empty curriculumVO.tuitionFees and curriculumVO.tuitionFees != 0}">checked="checked"</c:if> <c:if test="${curriculumVO.tuitionAt ne 'Y'}">disabled</c:if>/>
			                            	<span class="custom-checked"></span>
			                              	<span class="text">수업료</span>
			                            </label>
			                            <label class="checkbox">
				                            <input type="checkbox" name="registrationFeesAt" value="Y" <c:if test="${not empty curriculumVO.registrationFees and curriculumVO.registrationFees != 0}">checked="checked"</c:if> <c:if test="${curriculumVO.tuitionAt ne 'Y'}">disabled</c:if>/>
			                            	<span class="custom-checked"></span>
			                              	<span class="text">등록비</span>
			                            </label>
		                            	)	
			                          </div>
			                          <div class="flex-col-auto">
			                            <label class="checkbox circle">
			                              <input type="radio" name="tuitionAt" class="table-checkbox" value="N" <c:if test="${curriculumVO.tuitionAt ne 'Y'}">checked="checked"</c:if>/>
			                              <span class="custom-checked"></span>
			                              <span class="text">아니오</span>
			                            </label>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">등록금액</th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-ten-col-3">
			                            <div class='input-group'>
			                            	<input type="number" id="tuitionFees" name="tuitionFees" value="${curriculumVO.tuitionFees}" class="onlyNum money table-input" placeholder="수업료"/>
			                            	<span class='unit'>원</span>
			                            </div>
			                          </div>
			                          <div class="flex-col-auto">
			                            <div class='input-group'>
			                            	<input type="number" id="registrationFees" name="registrationFees"  value="${curriculumVO.registrationFees}" class="onlyNum money table-input" placeholder="등록비"/>
			                            	<span class='unit'>원</span>
			                            </div>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                    <tr>
			                      <th class="title">등록기간 <i class="font-point font-400">*</i> </th>
			                      <td>
			                        <div class="flex-row">
			                          <div class="flex-col-3">
			                            <form:input path="tuitionStartDate" cssClass="ell date datepicker" placeholder="시작일" readonly="true"/>
			                          </div>
			                          <div class="flex-col-auto">
			                            <div class="table-data">
			                              ~ </div>
			                          </div>
			                          <div class="flex-col-3">
			                            <form:input path="tuitionEndDate" cssClass="ell date datepicker" placeholder="종료일" readonly="true"/>
			                          </div>
			                        </div>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>
			              </div>
			            </article>
			            <article class="content-wrap">
			              <!-- 사업비 (소요예산) -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">사업비 (소요예산)</div>
			                </div>
			              </div>
			              <div class="content-body">
			                <!-- 테이블영역-->
			                <table class="common-table-wrap mb-20 expTable">
			                  <colgroup>
			                    <col style='width:7%'>
			                    <col style='width:20%'>
			                    <col style='width:15%'>
			                    <col style='width:15%'>
			                    <col style='width:20%'>
			                    <col style='width:15%'>
			                    <col style='width:14%'>
			                  </colgroup>
			                  <thead>
			                    <tr class='bg-gray font-700'>
			                      <th scope='col' class='center-align'><label class='checkbox'><input type='checkbox' id="allCheckExp" class='allCheck'><span class='custom-checked'></span></label></th>
			                      <th scope='col'>지원비 비목</th>
			                      <th scope='col'>계정 목</th>
			                      <th scope='col'>금액(원)</th>
			                      <th scope='col'>산출근거</th>
			                      <th scope='col'>비고</th>
			                      <th scope='col'>관리</th>
			                    </tr>
			                  </thead>
			                  
			                  	<%-- 지원비 비목 묶어주는 역할 --%>	
								<c:set var="rowspan" value="0"/>
								<c:set var="rowspanList" value=""/>
								<c:set var="prevCode" value="${expense[0].typeCode}"/>
								<c:forEach var="list" items="${expense}" varStatus="status">
									<c:choose>
										<c:when test="${prevCode eq list.typeCode}">
											<c:set var="rowspan" value="${rowspan + 1}"/>
										</c:when>
										<c:otherwise>
											<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
											<c:set var="rowspan" value="1"/>
											<c:set var="prevCode" value="${list.typeCode}"/>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
								<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
								<c:set var="listCnt" value="${rowspan[0]}"/>
								<c:set var="rowspanCnt" value="0"/>
								
								<c:set var="prevCode" value=""/>
			                  
			                  <c:forEach var="result" items="${expense}" varStatus="status">
								<c:if test="${prevCode ne result.typeCode}">
									<tbody class="box_exp">
								</c:if>
								<tr <c:if test="${prevCode ne result.typeCode}">class='firstExp'</c:if>>
									<c:if test="${prevCode ne result.typeCode}">
										<td scope="row" class="box_check" rowspan="${rowspan[rowspanCnt] + 1}">
											<label class="checkbox"><input type="checkbox" class="checkList expCheck"><span class="custom-checked"></span></label>
										</td>
										<td class="box_selExp" rowspan="${rowspan[rowspanCnt] + 1}">
											<select class='typeCodeList table-select select2' data-select='style1'>
												<option value=''>선택</option>
												<c:forEach var="accountCode" items="${feeList}" varStatus="sta">
													<c:if test="${accountCode.ctgryLevel eq '1'}">
														<option value="${accountCode.ctgryId }" <c:if test="${result.typeCode eq accountCode.ctgryId}">selected="selected"</c:if>><c:out value="${accountCode.ctgryNm}"/></option>
													</c:if>
												</c:forEach>
											</select>
										</td>
										<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
									</c:if>
									<td>
										<select class='accountCodeList table-select select2' name='accountCodeList' data-select='style1'>
											<option value=''>선택</option>
											<c:forEach var="accountCode" items="${feeList}" varStatus="sta">
												<c:if test="${accountCode.ctgryLevel eq '2' and accountCode.upperCtgryId eq result.typeCode}">
													<option value="${accountCode.ctgryId }" <c:if test="${result.accountCode eq accountCode.ctgryId}">selected="selected"</c:if>><c:out value="${accountCode.ctgryNm}"/></option>
												</c:if>
											</c:forEach>
										</select>
										<input type='hidden' class='typeCodeListInp' name='typeCodeList' value='${result.typeCode}'/>
									</td>
									<td class="amount">
										<input type='text' class='table-input money onlyNum' name="amountList" value='${result.amount}' placeholder='금액 입력'>
									</td>
									<td class="reason">
										<input type='text' class='table-input' name='reasonList' value='<c:out value="${result.reason}"/>' placeholder='산출근거를 입력해주세요.'>
									</td>
									<td class="etc">
										<input type='text' class='table-input' name='etcList' value='<c:out value="${result.etc}"/>' placeholder=''>
									</td>
									<td class="contr">
										<button class='btn_delExpMore btn-sm-full btn-outline-gray' type='button'>삭제</button>
									</td>
									<c:if test="${result.typeCode ne expense[status.count].typeCode}">
										<tr class='foot'>
											<td scope="row" class="btn_addExpMore btn-add-list" colspan="5">계정 목 추가</td>
										</tr>
									</c:if>
								</tr>
								<c:if test="${result.typeCode ne expense[status.count].typeCode}">
									</tbody>
								</c:if>
								
								<c:set var="prevCode" value="${result.typeCode}"/>
							  </c:forEach>
			                </table>
			              </div>
			              
			              <div class="btn-group-wrap">
			                <div class="left-area">
			                  <button id="btn_delExp" class="btn-md btn-outline-gray">선택삭제</button>
			                  <button class="btn-md btn-point btn_addExp">지원비 목 추가</button>
			                </div>
			              </div>
			            </article>
			            <article class="content-wrap">
			              <!-- 기타사항 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">기타사항</div>
			                </div>
			              </div>
			              <div class="content-body">
			              	<textarea class="content-textarea onlyText" name="etcCn" placeholder="기타 사항을 적어주세요."><c:out value="${curriculumVO.etcCn}"/></textarea>
			              	<%-- 
			                <div class="content-textarea onlyText" contentEditable="true" name="etcCn" placeholder="기타 사항을 적어주세요.">
			                	<c:out value="${curriculumVO.etcCn}"/>
			                </div>
			                 --%>
			              </div>
			            </article>
					</c:when>
					<c:otherwise>
			            <article class="content-wrap mt-50">
			              <!-- 교육과정 내용 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">교육과정 내용</div>
			                </div>
			                <p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
			              </div>
			              <div class="content-body">
			                <!-- 테이블영역-->
			                <table class="common-table-wrap size-sm left-align">
			                  <colgroup>
			                    <col class='bg-gray' style='width:20%'>
			                    <col style='width:30%'>
			                    <col class='bg-gray' style='width:20%'>
			                    <col>
			                  </colgroup>
			                  <tbody>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정명</td>
			                      <td colspan="3"><c:out value="${curriculumVO.crclNm}"/></td>
			                    </tr>
			                    <tr class="timeAt">
			                      <td scope='row' class='font-700 keep-all'>과정기간</td>
			                      <td class='keep-all'><c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/></td>
			                      <td scope='row' class='font-700 keep-all'>과정시간</td>
			                      <td>
			                      <c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
			                      	총 <c:out value="${curriculumVO.totalTime}"/>시간 / 주 <c:out value="${curriculumVO.weekNum}"/>회 / 일 <c:out value="${curriculumVO.dayTime}"/>시간
			                      </c:if>
			                      </td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정 진행 캠퍼스</td>
			                      <td>
			                      	<c:out value="${curriculumVO.campusNm}"/>
								  </td>
			                      <td scope='row' class='font-700 keep-all'>강의실</td>
			                      <td><form:input path="campusPlace" cssClass="table-input" required="true" placeholder="상세장소 입력"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>주관기관/학과</td>
			                      <td colspan='3'><c:out value="${curriculumVO.hostCodeNm}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>책임교수</td>
			                      <td colspan='3'><c:out value="${curriculumVO.userNm}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정 개요 및 목표</td>
			                      <td colspan='3'>
			                      	<form:textarea path="crclGoal" cssClass="content-textarea" placeholder="과정 목표를 입력해주세요."/>
			                      </td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정의 기대효과</td>
			                      <td colspan='3'>
			                      	<form:textarea path="crclEffect" cssClass="content-textarea" placeholder="과정 기대효과를 입력해주세요."/>
			                      </td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정 수강대상자</td>
			                      <td colspan='3'>일반 (본교생 포함 전체)</td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>과정 계획서 작성기간</td>
			                      <td class='keep-all' colspan='3'><c:out value="${curriculumVO.planStartDate}"/> ~ <c:out value="${curriculumVO.planEndDate}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>수강신청기간</td>
			                      <td class='keep-all' colspan='3'><c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/></td>
			                    </tr>
			                    <tr class="">
			                      <td scope='row' class='font-700 keep-all'>수강정원</td>
			                      <td>
			                      	<c:choose>
										<c:when test="${curriculumVO.applyMaxCnt eq '0' or empty curriculumVO.applyMaxCnt}">제한없음</c:when>
										<c:otherwise>
											<c:out value="${curriculumVO.applyMaxCnt}"/>명
										</c:otherwise>
									</c:choose>
			                      </td>
			                      <td scope='row' class='font-700 keep-all'>주관학과 배정여부</td>
			                      <td>
			                      	<c:choose>
										<c:when test="${curriculumVO.assignAt eq 'Y'}">예</c:when>
										<c:otherwise>아니오</c:otherwise>
									</c:choose>
			                      </td>
			                    </tr>
			                  </tbody>
			                </table>
			              </div>
			            </article>
			            <article class="content-wrap">
			              <!-- 사업비 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">사업비</div>
			                </div>
			              </div>
			              <div class="content-body">
			                <!-- 테이블영역-->
			                <table class="common-table-wrap size-sm">
			                  <colgroup>
			                    <col style='width:25%'>
			                    <col style='width:25%'>
			                    <col style='width:25%'>
			                    <col style='width:25%'>
			                    <col style='width:25%'>
			                  </colgroup>
			                  <thead>
			                    <tr class='bg-gray font-700'>
			                      <th scope='col'>지원비 비목</th>
			                      <th scope='col'>계정 목</th>
			                      <th scope='col'>금액(원)</th>
			                      <th scope='col'>산출근거</th>
			                      <th scope='col'>비고</th>
			                    </tr>
			                  </thead>
			                  <tbody>
			                  	<c:set var="rowspan" value="0"/>
								<c:set var="rowspanList" value=""/>
								<c:set var="prevCode" value="${expense[0].typeCode}"/>
								<c:forEach var="list" items="${expense}" varStatus="status">
									<c:choose>
										<c:when test="${prevCode eq list.typeCode}">
											<c:set var="rowspan" value="${rowspan + 1}"/>
										</c:when>
										<c:otherwise>
											<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
											<c:set var="rowspan" value="1"/>
											<c:set var="prevCode" value="${list.typeCode}"/>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
								<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
								<c:set var="listCnt" value="${rowspan[0]}"/>
								<c:set var="rowspanCnt" value="0"/>
								
								<c:set var="prevCode" value=""/>
								
								<c:forEach var="result" items="${expense}" varStatus="status">
									<tr>
										<c:if test="${prevCode ne result.typeCode}">
											<td class="line" rowspan="${rowspan[rowspanCnt]}"><c:out value="${result.typeCodeNm}"/></td>
											<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
										</c:if>
										<c:set var="prevCode" value="${result.typeCode}"/>
										<td class="line"><c:out value="${result.accountCodeNm}"/></td>
										<td class="line"><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.amount}" />원</td>
										<td class="line"><c:out value="${result.reason}"/></td>
										<td><c:out value="${result.etc}"/></td>
									</tr>
								</c:forEach>
								<tr>
									<c:choose>
										<c:when test="${empty curriculumVO.totAmount or curriculumVO.totAmount eq '0' }"><td class="alC" colspan="5">무료</td></c:when>
										<c:otherwise>
											<td class="alC" colspan="2">계</td>
											<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${curriculumVO.totAmount}" />원</td>
											<td></td>
											<td></td>
										</c:otherwise>
									</c:choose>
								</tr>
			                  </tbody>
			                </table>
			              </div>
			            </article>
			            <article class="content-wrap">
			              <!-- 기타사항 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">기타사항</div>
			                </div>
			              </div>
			              <div class="content-body">
			                <div class="content-textarea onlyText" contentEditable="false" placeholder="기타 사항을 적어주세요.">
			                    <c:choose>
									<c:when test="${empty curriculumVO.etcCn}">기타사항 없음</c:when>
									<c:otherwise><c:out value="${curriculumVO.etcCn}"/></c:otherwise>
								</c:choose>
			                </div>
			              </div>
			            </article>
					</c:otherwise>
				</c:choose>
				
				<c:if test="${processSttusCode > 0}">
					<article class="content-wrap">
		              <!-- 강의책임교원 배정 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">강의책임교원 배정</div>
		                </div>
		                <p class="notice font-700">※ 강의책임교원은 강의 교원 중에서 배정하셔야 하며, 각 교원별 시수 배정 및 수업계획<br> 작성 업무에 대한 관리 권한을 가지게 됩니다. 수강신청 전에 필수로 입력하셔야 합니다.</p>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap ">
		                  <colgroup>
		                    <%-- <col style='width:7%'> --%>
		                    <col class='left-align'>
		                    <col class='left-align' style='width:14%'>
		                  </colgroup>
		                  <tbody>
		                  	<tr>
		                  		<td colspan="2">
		                  			<input type="text" id="searchUser2" class="wid100" placeholder="교원 명을 입력해주세요."/>
		                  		</td>
		                  	</tr>
		                  </tbody>
		                  <tbody id="box_user2">
		                  	<c:forEach var="user" items="${subUserList}" varStatus="status">
								<tr>
									<%-- <td>1</td> --%>
									<td class='left-align'>
										<input type='hidden' name='userIdList2' value='${user.userId}'/>
										<c:out value="${user.userNm}"/>(${user.mngDeptNm}_${user.emailAdres})
									</td>
									<td class='left-align'><button class='btn-sm-full btn-outline-gray btn_del' type='button'>삭제</button></td>
								</tr>
							</c:forEach>
		                  </tbody>
		                </table>
		              </div>
		            </article>
		            
		            <c:if test="${curriculumVO.totalTimeAt eq 'Y'}">
			            <article class="content-wrap">
			              <!-- 교원배정 -->
			              <div class="content-header">
			                <div class="title-wrap">
			                  <div class="title">교원배정</div>
			                </div>
			                <p class="notice font-700">※ 수강신청 전에 필수로 입력해야 하는 항목입니다.</p>
			              </div>
			              <div class="content-body">
			                <!-- 테이블영역-->
			                <table class="common-table-wrap ">
			                  <colgroup>
			                    <col style='width:22%'>
			                    <col style='width:7%'>
			                    <col>
			                    <col style='width:14%'>
			                  </colgroup>
			                  <thead>
			                    <tr class='bg-gray font-700'>
			                      <th scope='col'>시수</th>
			                      <th scope='col' colspan='3'>교원 배정</th>
			                    </tr>
			                  </thead>
			                  
			                  	<%-- 교원배정 묶어주는 역할 --%>	
								<c:set var="rowspan" value="0"/>
								<c:set var="rowspanList" value=""/>
								<c:set var="prevCode" value="${facList[0].position}"/>
								<c:forEach var="list" items="${facList}" varStatus="status">
									<c:choose>
										<c:when test="${prevCode eq list.position}">
											<c:set var="rowspan" value="${rowspan + 1}"/>
										</c:when>
										<c:otherwise>
											<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
											<c:set var="rowspan" value="1"/>
											<c:set var="prevCode" value="${list.position}"/>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
								<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
								<c:set var="listCnt" value="${rowspan[0]}"/>
								<c:set var="rowspanCnt" value="0"/>
								
								<c:set var="prevCode" value=""/>
								<c:set var="number" value="0"/>
			                  
			                  <c:forEach var="result" items="${facList}" varStatus="status">
								<c:set var="number" value="${number +1}"/>
								<c:if test="${prevCode ne result.position}">
									<tbody class='box_staff'>
								</c:if>
								<tr <c:if test="${prevCode ne result.position}">class='first'</c:if>>
									<c:if test="${prevCode ne result.position}">
										<td class="num" rowspan="${rowspan[rowspanCnt] + 1}"><textarea class="onlyNum timeList table-textarea h-full" name="timeList" placeholder='시수를 입력해주세요.'><c:out value="${result.sisu}"/></textarea></td>
										<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
									</c:if>
									<td class="num">
										<c:out value="${number}"/>
									</td>
									<td class='left-align'>
										<c:out value="${result.facNm}"/>
										<input type="hidden" class="facIdList" name="facIdList" value="<c:out value="${result.facId}"/>">
										<input type="hidden" class="facNmList" name="facNmList" value="<c:out value="${result.facNm}"/>">
										<input type="hidden" name="positionList" value="<c:out value="${result.position}"/>">
									</td>
									<td>
										<button class='btn-sm-full btn-outline-gray btn_delStf' type='button'>삭제</button>
									</td>
									<c:if test="${result.position ne facList[status.count].position}">
										<tr class="foot <c:out value="${result.position}"/>">
											<td class='btn-add-list btn_addStfMore' colspan='3' data-modal-type="staff_search" data-rand="<c:out value="${result.position}"/>">
												교원 검색 및 추가
											</td>
										</tr>
									</c:if>
								</tr>
								<c:if test="${result.position ne facList[status.count].position}">
									<c:set var="number" value="0"/>
									</tbody>
								</c:if>
								
								<c:set var="prevCode" value="${result.position}"/>
							  </c:forEach>
			                  <tfoot id="box_addTime">
								<tr class="bg-point-light">
									<td class='btn-add-list btn_addTime' colspan='4'>시수 추가</td>
								</tr>
								<tr class="box_sum">
									<td class="font-point"><span class="curTime">0</span>/<span class="totalTime"><c:out value="${curriculumVO.totalTime}"/></span></td>
									<td colspan="3" class='left-align'>
										교원 총 <span class="sumUser">0</span>명
									</td>
								</tr>
							  </tfoot>
			                </table>
			              </div>
			            </article>
			        </c:if>
			        
		            <article class="content-wrap">
		              <!-- 과정내용(단원) 관리 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">과정내용</div>
		                </div>
		                <p class="notice font-700">※ 수강신청 화면에 출력되는 정보로 과정내용이 구체적으로 정해지지 않았더라도<br> 기본적인 내용은 반드시 등록해 주셔야 합니다.</p>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap ">
		                  <colgroup>
		                    <col style='width:7%'>
		                    <col style='width:25%'>
		                    <col>
		                    <col style='width:14%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-gray font-700'>
		                      <th scope='col'>No</th>
		                      <th scope='col' class=' '>내용범주</th>
		                      <th scope='col'>내용</th>
		                      <th scope='col'>관리</th>
		                    </tr>
		                  </thead>
		                  
		                  	<%-- 과정내용 번호 및 단원 묶어주는 역할 --%>	
							<c:set var="rowspan" value="0"/>
							<c:set var="rowspanList" value=""/>
							<c:set var="prevCode" value="${lessonList[0].lessonNm}"/>
							<c:forEach var="list" items="${lessonList}" varStatus="status">
								<c:choose>
									<c:when test="${prevCode eq list.lessonNm}">
										<c:set var="rowspan" value="${rowspan + 1}"/>
									</c:when>
									<c:otherwise>
										<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
										<c:set var="rowspan" value="1"/>
										<c:set var="prevCode" value="${list.lessonNm}"/>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:set var="rowspanList" value="${rowspanList},${rowspan}"/>
							<c:set var="rowspan" value="${fn:split(rowspanList,',')}"/>
							<c:set var="listCnt" value="${rowspan[0]}"/>
							<c:set var="rowspanCnt" value="0"/>
							
							<c:set var="prevCode" value=""/>
							<c:set var="number" value="1"/>
		                  
		                  <%-- <c:forEach var="result" items="${lessonList}" varStatus="status">
							<c:if test="${prevCode ne result.lessonNm}">
								<tbody class='box_lesson'>
							</c:if>
							<tr <c:if test="${prevCode ne result.lessonNm}">class='first'</c:if>>
								<c:if test="${prevCode ne result.lessonNm}">
									<td class="num" rowspan="${rowspan[rowspanCnt] + 1}">
										<c:out value="${number}"/>
										<c:set var="number" value="${number + 1}"/>
									</td>
									<td rowspan='${rowspan[rowspanCnt] + 1}' class='lesnm'><textarea class='table-textarea h-full' placeholder='단원 내용을 입력해주세요.'><c:out value="${result.lessonNm}"/></textarea></td>
									<c:set var="rowspanCnt" value="${rowspanCnt+1}"/>
								</c:if>
								<td class='left-align'><input type='text' class='table-input chasiList' name='chasiList' value="${result.chasiNm}" placeholder='학습 내용을 입력해주세요'/></td>
								<td><button class='btn-sm-full btn-outline-gray btn_delCha' type='button'>삭제</button></td>
								<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
									<tr class='foot'><td colspan='2' class="btn-add-list btn_addChaMore">과정내용 추가</td></tr>
								</c:if>
							</tr>
							<c:if test="${result.lessonNm ne lessonList[status.count].lessonNm}">
								</tbody>
							</c:if>
							
							<c:set var="prevCode" value="${result.lessonNm}"/>
						  </c:forEach> --%>
		                  <tfoot id="box_addLes">
							<tr class="bg-point-light">
								<td colspan="4" class="btn-add-list btn_addLes">
									내용 추가
									
									<%-- 단원을 위한 div --%>
									<div id="box_lesnm" style="display:none;"></div>
								</td>
							</tr>
						  </tfoot>
		                </table>
		              </div>
		            </article>
		            
		            <article class="content-wrap">
		              <!-- 성적 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">성적</div>
		                </div>
		                <p class="notice font-700">
	                		<c:choose>
								<c:when test="${curriculumVO.evaluationAt eq 'Y'}">
									※ 절대평가(<c:if test="${curriculumVO.gradeType eq 'CTG_0000000000000090'}">100점기준 점수환산표 </c:if><c:out value="${curriculumVO.gradeTypeNm}"/>)
								</c:when>
								<c:otherwise>※ 상대평가</c:otherwise>
							</c:choose>
		                </p>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                
		                <table class="common-table-wrap size-sm">
		                
		                	<c:choose>
								<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000090'}">
									<colgroup>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                    <col style='width:10%'>
					                </colgroup>
					                <thead>
					                    <tr class='bg-gray font-700'>
					                      <th scope='col'>Grade</th>
					                      <%--
					                      <th scope='col'>A+</th>
					                      <th scope='col'>A</th>
					                      <th scope='col'>B+</th>
					                      <th scope='col'>B</th>
					                      <th scope='col'>C+</th>
					                      <th scope='col'>C</th>
					                      <th scope='col'>D+</th>
					                      <th scope='col'>D</th>
					                      <th scope='col'>F</th>
					                      --%>
					                      <c:forEach var="result" items="${gradeTypeList}" varStatus="status">
											<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
												<th scope='col'><c:out value="${result.ctgryNm}"/></th>
											</c:if>
										  </c:forEach>
					                    </tr>
					                </thead>
					                <tbody>
					                    <tr class="font-700">
					                      	<c:set var="aplus"><c:out value="${curriculumVO.aplus}" default="95~100"/></c:set>
											<c:set var="a"><c:out value="${curriculumVO.a}" default="90~94"/></c:set>
											<c:set var="bplus"><c:out value="${curriculumVO.bplus}" default="85~89"/></c:set>
											<c:set var="b"><c:out value="${curriculumVO.b}" default="80~84"/></c:set>
											<c:set var="cplus"><c:out value="${curriculumVO.cplus}" default="75~79"/></c:set>
											<c:set var="c"><c:out value="${curriculumVO.c}" default="70~74"/></c:set>
											<c:set var="dplus"><c:out value="${curriculumVO.dplus}" default="65~69"/></c:set>
											<c:set var="d"><c:out value="${curriculumVO.d}" default="60~64"/></c:set>
											<c:set var="f"><c:out value="${curriculumVO.f}" default="0~59"/></c:set>
											<c:set var="pass"><c:out value="${curriculumVO.pass}" default="0"/></c:set>
											<c:set var="fail"><c:out value="${curriculumVO.fail}" default="0"/></c:set>
					                      
					                      	<td scope='row'>점수</td>
					                      	<td><input type="text" value="${aplus}" class="table-input center-align" name="aplus"/></td>
											<td><input type="text" value="${a}" class="table-input center-align" name="a"/></td>
											<td><input type="text" value="${bplus}" class="table-input center-align" name="bplus"/></td>
											<td><input type="text" value="${b}" class="table-input center-align" name="b"/></td>
											<td><input type="text" value="${cplus}" class="table-input center-align" name="cplus"/></td>
											<td><input type="text" value="${c}" class="table-input center-align" name="c"/></td>
											<td><input type="text" value="${dplus}" class="table-input center-align" name="dplus"/></td>
											<td><input type="text" value="${d}" class="table-input center-align" name="d"/></td>
											<td><input type="text" value="${f}" class="table-input center-align" name="f"/></td>
					                    </tr>
					                </tbody>
								</c:when>
								<c:when test="${curriculumVO.evaluationAt eq 'Y' and curriculumVO.gradeType eq 'CTG_0000000000000091'}">
									<tr>
										<th class="alC">P/F</th>
										<th class="alC" colspan="2">점수구간</th>
									</tr>
									<tr class="alC">
										<td class="line">Pass</td>
										<td class="alC"><input type="text" value="${curriculumVO.pass}" class="alC" name="pass"/></td>
										<td class="alC">~ 100</td>
									</tr>
									<tr class="alC">
										<td class="line">Fail</td>
										<td class="alC"><input type="text" value="${curriculumVO.fail}" class="alC" name="fail"/></td>
										<td class="alC">~ 0</td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<th class="alC">Grade</th>
										<c:forEach var="result" items="${gradeTypeList}" varStatus="status">
											<c:if test="${result.ctgryLevel eq '2' and result.upperCtgryId eq 'CTG_0000000000000090'}">
												<th class="alC"><c:out value="${result.ctgryNm}"/></th>
											</c:if>
										</c:forEach>
									</tr>
									<tr>
										<td class="line alC">비중</td>
										<td colspan="2" class="line alC">상위 30%</td>
										<td colspan="2" class="line alC">상위 65%이내</td>
										<td colspan="5" class="line alC">66% ~ 100%</td>
									</tr>
									<%-- 실제 수강신청으로 배정된 학생의 수로 계산 해야됨 아래는 100명 기준 값 --%>
									<tr>
										<td class="line alC">가이드 학생 수</td>
										<td colspan="2" class="line alC">30</td>
										<td colspan="2" class="line alC">35</td>
										<td colspan="5" class="line alC">35</td>
									</tr>
								</c:otherwise>
							</c:choose>
		                </table>
		              </div>
		            </article>
		            
		            <article class="content-wrap">
		              <!-- 총괄평가 기준 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">총괄평가 기준</div>
		                </div>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap ">
		                  <colgroup>
		                    <col style='width:43%'>
		                    <col style='width:43%'>
		                    <col style='width:14%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-gray font-700'>
		                      <th scope='col'>총괄평가 기준</th>
		                      <th scope='col'>성적반영율(%)</th>
		                      <th scope='col'>관리</th>
		                    </tr>
		                  </thead>
		                  <tbody class='box_evt'>
		                  	<%-- 총괄평가 등록되어 있는지 확인 및 반영률 체크--%>
							<c:set var="totper" value="0"/>
							<c:choose>
								<c:when test="${not empty evaluationList or fn:length(evaluationList) > 0}">
									<c:forEach var="result" items="${evaluationList}" varStatus="status">
										<tr class="<c:out value="${result.evtId}"/>">
											<input type='hidden' name='evtIdList' value="${result.evtId}"/>
											<input type='hidden' name='evtStandList' value="${empty result.evtStand ? '점수' : result.evtStand}"/>
											<td scope='row' class='left-align'>
												<c:choose>
													<c:when test="${result.evtId eq 'CTG_0000000000000119'}">
														<input type='text' class='table-input' name="evtNmList" value="${result.evtNm}" placeholder='기타 기준을 입력해주세요.'>
													</c:when>
													<c:otherwise>
														<c:out value="${result.evtNm}"/>
														<input type='hidden' name="evtNmList" value="${result.evtNm}"/>
													</c:otherwise>
												</c:choose>
											</td>
											<c:choose>
												<c:when test="${result.evtId eq 'CTG_0000000000000115'}">
													<td>
														<div class='flex-row'>
								                          <div class='flex-col-6'>
								                            <div class='input-group'>
								                            	<span class='unit font-gray-light'>%</span>
								                            	<input type="number" class="evtper onlyNum table-input" name="evtValList" value="${result.evtVal}"/>
								                            </div>
								                          </div>
								                          <div class='flex-col-6'>
								                          	<select class='table-select select2' data-select='style1' name="evtType">
								                              	<c:forEach var="ctgry" items="${homeworkComCode}">
																	<option value="${ctgry.code}" <c:if test="${ctgry.code eq result.evtType}">selected="selected"</c:if>><c:out value="${ctgry.codeNm}"/></option>
																</c:forEach>
								                            </select>
								                          </div>
								                        </div>
													</td>
												</c:when>
												<c:when test="${result.evtId eq 'CTG_0000000000000117'}">
													<td>
														<div class='flex-row'>
								                          <div class='flex-col'>
								                            <div class='input-group'>
								                            	<span class='unit font-gray-light'>%</span>
								                            	<input type='number' class='table-input evtper onlyNum' name="evtValList" value="${result.evtVal}"></div>
								                          </div>
								                          <div class='flex-col-auto'>
								                          	<button type='button' class='btn-sm-110 btn-outline-gray btn_bbs' data-id="${curriculumVO.crclId}">게시판 선택</button>
								                          </div>
								                          <div class='flex-col-12'>
								                            <ul id="box_bbsmaster" class='selected-items-wrap row-full'>
								                              <c:forEach var="bbsList" items="${attendBbsList}">
																<li class='item bg-blue-light'>
																	(
																	<c:choose>
																		<c:when test="${bbsList.sysTyCode eq 'ALL'}">전체</c:when>
																		<c:when test="${bbsList.sysTyCode eq 'GROUP'}">조별</c:when>
																		<c:when test="${bbsList.sysTyCode eq 'CLASS'}">반별</c:when>
																	</c:choose>
																	)
																	<c:out value="${bbsList.bbsNm}"/> / <c:out value="${bbsList.colect}"/>회
																	<input type="hidden" name="bbsIdList" value="${bbsList.bbsId}"/>
																	<input type="hidden" name="colectList" value="${bbsList.colect}"/>
																	<button type='button' class='btn-remove btn_delbbs'>삭제</button>
																</li>
															  </c:forEach>
								                            </ul>
								                          </div>
								                        </div>
													</td>
												</c:when>
												<c:otherwise>
													<td>
														<div class='input-group'>
															<span class='unit font-gray-light'>%</span><input type='number' class='table-input evtper onlyNum' name='evtValList' value="${result.evtVal}">
														</div>
													</td>
												</c:otherwise>
											</c:choose>
											<td><button class='btn-sm-full btn-outline-gray btn_delEvt' type='button'>삭제</button></td>
										</tr>
										<c:set var="totper" value="${totper  + result.evtVal}"/>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach var="result" items="${evaluationBaseList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '2'}">
											<tr class="<c:out value="${result.ctgryId}"/>">
												<input type='hidden' name='evtIdList' value="${result.ctgryId}"/>
												<input type='hidden' name='evtStandList' value="${empty result.ctgryVal ? '점수' : result.ctgryVal}"/>
												
												<td scope='row' class='left-align'>
													<c:choose>
														<c:when test="${result.ctgryId eq 'CTG_0000000000000119'}">
															<input type='text' class='table-input' name="evtNmList" placeholder='기타 기준을 입력해주세요.'>
														</c:when>
														<c:otherwise>
															<c:out value="${result.ctgryNm}"/>
															<input type='hidden' name="evtNmList" value="${result.ctgryNm}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<c:choose>
													<c:when test="${result.ctgryId eq 'CTG_0000000000000115'}">
														<td>
															<div class='flex-row'>
									                          <div class='flex-col-6'>
									                            <div class='input-group'>
									                            	<span class='unit font-gray-light'>%</span>
									                            	<input type="number" class="evtper onlyNum table-input" name="evtValList"/>
									                            </div>
									                          </div>
									                          <div class='flex-col-6'>
									                          	<select class='table-select select2' data-select='style1' name="evtType">
									                              	<c:forEach var="result" items="${homeworkComCode}">
																		<option value="${result.code}" ><c:out value="${result.codeNm}"/></option>
																	</c:forEach>
									                            </select>
									                          </div>
									                        </div>
														</td>
													</c:when>
													<c:when test="${result.ctgryId eq 'CTG_0000000000000117'}">
														<td>
															<div class='flex-row'>
									                          <div class='flex-col'>
									                            <div class='input-group'>
									                            	<span class='unit font-gray-light'>%</span>
									                            	<input type='number' class='table-input evtper onlyNum' name="evtValList"></div>
									                          </div>
									                          <div class='flex-col-auto'>
									                          	<button type='button' class='btn-sm-110 btn-outline-gray btn_bbs' data-id="${curriculumVO.crclId}">게시판 선택</button>
									                          </div>
									                          <div class='flex-col-12'>
									                            <ul id="box_bbsmaster" class='selected-items-wrap row-full'></ul>
									                          </div>
									                        </div>
														</td>
													</c:when>
													<c:otherwise>
														<td>
															<div class='input-group'>
																<span class='unit font-gray-light'>%</span><input type='number' class='table-input evtper onlyNum' name='evtValList'>
															</div>
														</td>
													</c:otherwise>
												</c:choose>
												<td><button class='btn-sm-full btn-outline-gray btn_delEvt' type='button'>삭제</button></td>
											</tr>
										</c:if>
									</c:forEach>
								</c:otherwise>
							</c:choose>
		                  </tbody>
		                  <tfoot id="box_addTime">
		                  	<tr class="box_sum">
		                      <td class='font-point' colspan='3'>총 성적 반영률 <span class="totper"><c:out value="${totper}"/></span> / 100</td>
		                    </tr>
		                    <tr class="bg-point-light">
		                      <td class='btn-add-list btnModalOpen btn_addEvt' colspan='3' data-modal-type="evaluation_criteria">총괄평가 기준 추가하기 </td>
		                    </tr>
		                  </tfoot>
		                </table>
		              </div>
		            </article>
		            
		            <article class="content-wrap">
		              <!-- 수료기준 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">수료기준&nbsp;&nbsp;<span class="red" style="font-size:12px">(별도 수료 기준이 없다면 등록하지 않으셔도 됩니다.)</span></div>
		                </div>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap size-sm">
		                  <colgroup>
		                    <col style='width:20%'>
		                    <col style='width:36%'>
		                    <col style='width:44%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='bg-gray font-700'>
		                      <th scope='col'>수료기준</th>
		                      <th scope='col'>상세 내용(점수, %)</th>
		                      <th scope='col'>설명</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                    <c:forEach var="result" items="${finishList}">
								<c:if test="${result.ctgryLevel eq 1}">
									<tr <c:if test="${result.ctgryId eq 'CTG_0000000000000084' or result.ctgryId eq 'CTG_0000000000000087' or result.ctgryId eq 'CTG_0000000000000088'}">class="bg-light-gray"</c:if>>
										<td class="alC"><c:out value="${result.ctgryNm}"/></td>
										<td class="alC">
											<c:choose>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000084'}">
													<div class="flex-row">
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnTotAt" class="table-checkbox radiocheck" value="N" <c:if test="${empty curriculumVO.fnTot}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">해당 없음</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnTotAt" class="table-checkbox radiocheck" value="Y" <c:if test="${not empty curriculumVO.fnTot}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">대상</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-5">
							                          	<div class='input-group'>
							                            	<span class='unit font-gray-light'>점 이상</span>
							                            	<input type="number" class="inpTxt onlyNum table-input" name="fnTot" value="${curriculumVO.fnTot}" <c:if test="${empty curriculumVO.fnTot}">readonly="readonly"</c:if>/>
							                            </div>
							                          </div>
							                        </div>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000085'}">
													<div class="flex-row">
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnAttendAt" class="table-checkbox radiocheck" value="N" <c:if test="${empty curriculumVO.fnAttend}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">해당 없음</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnAttendAt" class="table-checkbox radiocheck" value="Y" <c:if test="${not empty curriculumVO.fnAttend}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">대상</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-5">
							                          	<div class='input-group'>
							                            	<span class='unit font-gray-light'>% 이상</span>
							                            	<input type="number" class="inpTxt onlyNum table-input" name="fnAttend" value="${curriculumVO.fnAttend}" <c:if test="${empty curriculumVO.fnAttend}">readonly="readonly"</c:if>/>
							                            </div>
							                          </div>
							                        </div>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000086'}">
													<div class="flex-row">
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnGradeTotAt" class="table-checkbox radiocheck" value="N" <c:if test="${empty curriculumVO.fnGradeTot}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">해당 없음</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnGradeTotAt" class="table-checkbox radiocheck" value="Y" <c:if test="${not empty curriculumVO.fnGradeTot}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">대상</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-5">
							                          	<div class='input-group'>
							                            	<span class='unit font-gray-light'>점 이상</span>
							                            	<input type="number" class="inpTxt onlyNum table-input" name="fnGradeTot" value="${curriculumVO.fnGradeTot}" <c:if test="${empty curriculumVO.fnGradeTot}">readonly="readonly"</c:if>/>
							                            </div>
							                          </div>
							                        </div>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000087'}">
													<div class="flex-row">
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnGradeFailAt" class="table-checkbox radiocheck" value="N" <c:if test="${empty curriculumVO.fnGradeFail}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">해당 없음</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnGradeFailAt" class="table-checkbox radiocheck" value="Y" <c:if test="${not empty curriculumVO.fnGradeFail}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">대상</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-5">
							                          	<div class='input-group'>
							                            	<span class='unit font-gray-light'>점 이상</span>
							                            	<input type="number" class="inpTxt onlyNum table-input" name="fnGradeFail" value="${curriculumVO.fnGradeFail}" <c:if test="${empty curriculumVO.fnGradeFail}">readonly="readonly"</c:if>/>
							                            </div>
							                          </div>
							                        </div>
												</c:when>
												<c:when test="${result.ctgryId eq 'CTG_0000000000000088'}">
													<div class="flex-row">
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnHomeworkAt" class="table-checkbox radiocheck" value="N" <c:if test="${empty curriculumVO.fnHomework}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">해당 없음</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-auto">
							                            <label class="checkbox circle">
							                              <input type="radio" name="fnHomeworkAt" class="table-checkbox radiocheck" value="Y" <c:if test="${not empty curriculumVO.fnHomework}">checked="checked"</c:if>/>
							                              <span class="custom-checked"></span>
							                              <span class="text">대상</span>
							                            </label>
							                          </div>
							                          <div class="flex-col-5">
							                          	<div class='input-group'>
							                            	<span class='unit font-gray-light'>점 이상</span>
							                            	<input type="number" class="inpTxt onlyNum table-input" name="fnHomework" value="${curriculumVO.fnHomework}" <c:if test="${empty curriculumVO.fnHomework}">readonly="readonly"</c:if>/>
							                            </div>
							                          </div>
							                        </div>
												</c:when>
											</c:choose>
										</td>
										<td class='left-align keep-all wide-spacing '><c:out value="${result.ctgryCn}"/></td>
									</tr>
								</c:if>
							</c:forEach>
		                  </tbody>
		                </table>
		              </div>
		            </article>
		            
		            <article class="content-wrap">
		              <!-- 교재 및 부교재 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">교재 및 부교재</div>
		                </div>
		              </div>
		              <div class="content-body">
		                <!-- 테이블영역-->
		                <table class="common-table-wrap size-sm mb-20">
		                  <colgroup>
		                    <col style='width:95px'>
		                    <col style='width:43%'>
		                    <col style='width:43%'>
		                    <col style='width:14%'>
		                  </colgroup>
		                  <thead>
		                    <tr class='center-align bg-gray font-700'>
		                      <th scope='col'>No</th>
		                      <th scope='col'>교재명</th>
		                      <th scope='col'>출판사</th>
		                      <th scope='col'>관리</th>
		                    </tr>
		                  </thead>
		                  <tbody class="list_book">
		                    <c:forEach var="result" items="${bookList}" varStatus="status">
								<tr>
									<td class="num center-align"><c:out value="${status.count}"/></td>
									<td>
										<c:out value="${result.nttSj}"/>
										<input type="hidden" class="nttNoList" name="nttNoList" value="${result.nttNo}"/>
									</td>
									<td><c:out value="${result.tmp01}"/></td>
									<td><button class='btn-sm-full btn-outline-gray btn_delBook' type='button'>삭제</button></td>
								</tr>
							</c:forEach>
							<c:if test="${fn:length(bookList) == 0}">
								<tr class="empty"><td colspan="4" class="center-align">등록된 교재 및 부교재가 없습니다.</td></tr>
							</c:if>
		                  </tbody>
		                  <tfoot>
		                    <tr class="bg-point-light">
		                      <td class='btn-add-list btn_addBook' colspan='4' data-lang="${curriculumVO.crclLang}">교재 검색 추가 </td>
		                    </tr>
							<tr>
								<td colspan="4">
									<%-- <div class="content-textarea onlyText" contentEditable="false" placeholder="교재 및 부교재를 입력해주세요."></div> --%>
									<textarea class="content-textarea onlyText" name="bookEtcText" placeholder="교재 및 부교재를 입력해주세요."><c:out value="${curriculumVO.bookEtcText}"/></textarea>
								</td>
							</tr>
		                  </tfoot>
		                </table>
		              </div>
		            </article>
		            
		            <article class="content-wrap">
		              <!-- 자체 운영 계획 및 규정 -->
		              <div class="content-header">
		                <div class="title-wrap">
		                  <div class="title">자체 운영 계획 및 규정</div>
		                </div>
		              </div>
		              <div class="content-body">
		              	<textarea class="content-textarea onlyText" name="managePlan" placeholder="자체 운영 계획 및 규정을 입력해주세요."><c:out value="${curriculumVO.managePlan}"/></textarea>
		              	<%-- 
		                <div class="content-textarea onlyText" contentEditable="false" placeholder="자체 운영 계획 및 규정을 입력해주세요.">
		                  언어 수업에 대해 000 운영 규정에 따라 진행될 예정<br>언어 수업에 대해 000 운영 규정에 따라 진행될 예정
		                </div>
		                 --%>
		              </div>
		            </article>
				</c:if>
				
	            <div class="page-btn-wrap mt-50">
	              <c:url var="listUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
	              <a href="${listUrl}" class="btn-xl btn-outline-gray">취소</a>
	              <a href="#" id="btn-reg" class="btn-xl btn-point">${_MODE eq 'REG' ? '등록' : '수정' }</a>
	            </div>
	          </section>
	      </form:form>
        </div>
 
 <div id="evaluation_criteria_modal" class="alert-modal">
  <div class="modal-dialog modal-top">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">총괄평가 기준</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text">총괄평가 기준을 선택해주세요</p>
        <table class="common-table-wrap size-sm">
          <colgroup>
            <col style="width:70px;">
            <col>
            <col style="width:138px">
          </colgroup>
          <thead>
            <tr class="bg-gray-light font-700">
              <th>선택</th>
              <th>게시판명</th>
              <th>기준</th>
            </tr>
          </thead>
          <tbody>
          	<c:forEach var="result" items="${evaluationBaseList}" varStatus="status">
				<c:if test="${result.ctgryLevel eq '2'}">
					<tr class="list">
						<td>
							<label class="checkbox circle">
			                  <input type="radio" class="chk" name="ctgryId" value="${result.ctgryId}" data-nm="${result.ctgryNm}">
			                  <span class="custom-checked"></span>
			                </label>
						</td>
						<td>
							<c:out value="${result.ctgryNm}"/>
							<c:if test="${not empty result.ctgryCn}"><br/>(<c:out value="${result.ctgryCn}"/>)</c:if>
						</td>
						<td>
							<c:choose>
								<c:when test="${not empty result.ctgryVal}">
									<c:out value="${result.ctgryVal}"/>
									<input type="hidden" class="sel_stand" value="${result.ctgryVal eq '점수' ? '1' : 2}"/>
								</c:when>
								<c:otherwise>
									<select id="sel_stand" class="table-select select2 sel_stand" data-select="style1">
					                  <option value="1">점수</option>
					                  <option value="2">횟수</option>
					                </select>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
			</c:forEach>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button id="btn_ok_evt" class="btn-xl btn-point btnModalConfirm">선택</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>