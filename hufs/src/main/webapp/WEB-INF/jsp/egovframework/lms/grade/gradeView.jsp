<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId }"/>	
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8"/>

<script type="text/javascript">
	<c:if test="${fn:length(gradeList) == 0}">
		alert("성적 정보가 없습니다.");
		history.back(-1);
	</c:if>

	// 학생 수료증 발급 유효값 체크(성공 할 시 발급화면으로 이동)
	function certificateCheck(){
		var crclId ='${curriculumVO.crclId}';
		
		var finishAt = 'N';
		finishAt = $("input[name=finishAt]").val();
		if(finishAt == 'N'){
			alert("수료여부가 미수료이기 때문에 발급이 불가합니다. \n관리자에게 문의해주세요.");
			return false;
		}
		
		// 유효값 체크(과정종료 / 과정만족도 참여여부 / 관리자 수료증 발급 여부) 
		$.ajax({
			type : "POST",
			url : "/ajax/lms/st/certificateCheck.json",
			data : {"crclId" : crclId},
			dataType : "json",
			success : function(data){
				if(data.result=='Y'){
					selectCertificate(data.certificateId);
				} else {
					if(data.result=='case1Fail'){
						alert("수료증 발급은 과정종료(성적발표) 이후에 가능합니다.");
					} else if(data.result=='case2Fail'){
						alert("해당 과정의 과정만족도 조사를 완료해야 수료증 발급이 가능합니다.");
						goSuverView(data.surveyVO.schdulId,data.surveyVO.schdulClCode,data.surveyVO.crclId, data.surveyVO.plId);
					} else if(data.result=='case23ail'){
						alert("아직 수료증 발급이 되지 않았습니다. 관리자에게 문의하세요.");
					} else if(data.result=='N'){
						alert("수료증 발급 관련 관리자에게 문의해주세요.");
					} else {
						alert("수료증 발급 관련 관리자에게 문의해주세요.");
					}
				}
				
			}, error : function(){
				alert("error");
			}
		});
	}

	function goSuverView(schdulId,schdulClCode,crclId,plId){
		location.href="/lms/cla/surveyView.do?schdulId="+schdulId+"&schdulClCode="+schdulClCode+"&crclId="+crclId+"&plId="+plId+"&menuId=MNU_0000000000000163";
	}
	
	// 수료증 발급
	function selectCertificate(certificateId){
		var url ="/lms/st/selectCertificate.do";
		
		var form = document.createElement("form");
		form.setAttribute("method","post");
		form.setAttribute("action",url);
		form.setAttribute("target","_blank");
		
		var input1 = document.createElement("input");
		input1.setAttribute("type","hidden");
		input1.setAttribute("name","searchCertificateId");
		input1.setAttribute("value",certificateId);
		
		form.appendChild(input1);
		
		document.body.appendChild(form);
		form.submit();
		
	}

</script>

		
		<div class="page-content-header">
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
        </div>
		
        <section class="page-content-body">
            <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수료정보</div>
                </div>
              </div>
              <!-- 테이블영역-->
              <table class="common-table-wrap grade-certificate">
                <colgroup>
                  <col>
                  <col>
                  <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }">
                  	<col style='width:10%'>
                  </c:if>
                  <col style='width:10%'>
                  <col style='width:10%'>
                </colgroup>
                <thead>
                  <tr class='font-700'>
                    <th scope='col' class='left-align'>과정명</th>
                    <th scope='col'>과정기간</th>
                    <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }">
                    	<th scope='col'>수료여부</th>
                    </c:if>
                    <th scope='col'>점수</th>
                    <th scope='col'>학점</th>
                  </tr>
                </thead>
                <tbody>
                	<c:forEach var="result" items="${gradeList}" varStatus="stauts">
	                  <tr class="">
	                    <td class='left-align'><c:out value="${curriculumVO.crclNm}"/></td>
	                    <td><c:out value="${curriculumVO.startDate}"/> ~ <c:out value="${curriculumVO.endDate}"/></td>
	                    <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }">
	                    	<td>${result.finishAt eq 'Y' ? '수료' : '미수료'}
	                    		<input type="hidden" name="finishAt" value="${result.finishAt}">
	                    	</td>
	                    </c:if>
	                    <td><c:out value="${fn:replace(result.chScr,'.0','')}"/></td>
	                    <td><c:out value="${result.confirmGrade}"/></td>
	                  </tr>
                  	</c:forEach>
                </tbody>
              </table>
            </article>
            <div class="page-btn-wrap mt-50">
            	<c:choose>
            		<c:when test="${param.menu eq 'myCrcl'}">
            			<c:url var="listUrl" value="/lms/crm/selectMyCurriculumList.do${_BASE_PARAM}">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:param name="tabStep" value="${param.tabStep}"/>
							<c:if test="${not empty param.closeCurriculumAt}"><c:param name="closeCurriculumAt" value="${param.closeCurriculumAt}" /></c:if>
							<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>
            		</c:when>
            		<c:otherwise>
            			<c:url var="listUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}"/>
            		</c:otherwise>
            	</c:choose>
              <a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
              <a href="#" class="btn-xl btn-point" onclick="certificateCheck();">수료증 발급</a>
              <!-- <a href="/msi/cntntsService.do?menuId=MNU_0000000000000172" class="btn-xl btn-point">성적·증명서 신청방법 안내</a> -->
            </div>
          </section>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>