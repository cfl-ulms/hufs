<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
    $(document).on("click", ".searchBtn", function() {
        $("form[name='searchFrm']").submit();
    });

    //검색 초기화
    $(document).on("click", "#searchReset", function() {
        $("form[name='searchFrm']").find("input").not("#resetBtn").val("");
        $("form[name='searchFrm'] select").val("").trigger("change");
        $("form[name='searchFrm'] input").prop("checked", false);
    });
});
</script>

<section class="page-content-body">
    <article class="content-wrap">
      <div class="box-wrap mb-40">
        <h3 class="title-subhead">과제 검색</h3>
        <form name="searchFrm" action="/lms/manage/homeworkTotalList.do" method="post">
            <input type="hidden" name="menuId" value="${searchVO.menuId }" />
	        <div class="flex-row-ten">
              <div class="flex-ten-col-3 mb-20">
                <div class="ell">
                  <select class="select2" data-select="style3" name="searchCrclLang">
                    <option value="">언어 전체</option>
                    <c:forEach var="result" items="${languageList}" varStatus="status">
                        <c:if test="${not empty result.upperCtgryId}">
                            <option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
                        </c:if>
                    </c:forEach>
                  </select>
                </div>
              </div>
              <div class="flex-ten-col-7 mb-20">
                <input type="text" name="searchCrclNm" placeholder="과정명" value="${searchVO.searchCrclNm }">
              </div>
              <div class="flex-ten-col-3 mb-20">
                <div class="ell">
                  <input type="text" name="searchUserNm" placeholder="교수명" value="${searchVO.searchUserNm }">
                </div>
              </div>
              <div class="flex-ten-col-7 mb-20">
                <div class="ell">
                  <input type="text" name="searchHomeworkNm" placeholder="과제명" value="${searchVO.searchHomeworkNm }">
                </div>
              </div>
              <div class="flex-ten-col-5 mb-20">
                <div class="desc">
                  <input type="text" class="ell date datepicker type2" name="searchOpenDate" placeholder="과제 시작일" value="${searchVO.searchOpenDate }">
                  <i>~</i>
                  <input type="text" class="ell date datepicker type2" name="searchCloseDate" placeholder="과제 종료일" value="${searchVO.searchCloseDate }">
                </div>

              </div>
              <div class="flex-ten-col-5 flex align-items-center mb-20">
                <label class="checkbox">
                  <input type="checkbox"  name="searchCloseHomework" value="Y" <c:if test="${searchVO.searchCloseHomework eq 'Y'}">checked</c:if>>
                  <span class="custom-checked"></span>
                  <span class="text">마감된 과제</span>
                </label>
                <label class="checkbox">
                  <input type="checkbox"  name="searchNotSubmitAt" value="Y" <c:if test="${searchVO.searchNotSubmitAt eq 'Y'}">checked</c:if>>
                  <span class="custom-checked"></span>
                  <span class="text">미제출 과제</span>
                </label>
                <label class="checkbox">
                  <input type="checkbox"  name="searchSubmitAt" value="Y" <c:if test="${searchVO.searchSubmitAt eq 'Y'}">checked</c:if>>
                  <span class="custom-checked"></span>
                  <span class="text">제출완료 과제</span>
                </label>
              </div>
              <div class="flex-ten-col-2">
                <div class="ell">
                  <select name="searchHwType" id="searchHwType" class="select2" data-select="style3">
                    <option value="">과제유형 전체</option>
                    <option value="1" <c:if test="${searchVO.searchHwType eq '1'}">selected</c:if>>개별</option>
                    <option value="2" <c:if test="${searchVO.searchHwType eq '2'}">selected</c:if>>조별</option>
                  </select>
                </div>
              </div>
              <div class="flex-ten-col-2">
                <div class="ell">
                  <select name="searchHwCode" id="searchHwCode" class="select2" data-select="style3">
                    <option value="">과제 전체</option>
                    <option value="1" <c:if test="${searchVO.searchHwCode eq '1'}">selected</c:if>>과정과제</option>
                    <option value="2" <c:if test="${searchVO.searchHwCode eq '2'}">selected</c:if>>수업과제</option>
                  </select>
                </div>
              </div>
            </div>
            <button class="btn-sm font-400 btn-point mt-20 searchBtn">검색</button>
            <a class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</a>
        </form>
      </div>
    </article>
    <article class="content-wrap">
      <div class="content-body">
        <!-- 테이블영역-->
        <table class="common-table-wrap table-type-board">
          <colgroup>
            <col style='width:7%'>
            <col style='width:12%'>
            <col style='width:12%'>
            <col style='width:12.5%'>
            <col style='width:12%'>
            <col>
            <col style='width:12%'>
            <col style='width:12%'>
          </colgroup>
          <thead>
            <tr class='bg-light-gray font-700'>
              <th scope='col'>No</th>
              <th scope='col'>언어</th>
              <th scope='col'>과제마감일</th>
              <th scope='col'>구분</th>
              <th scope='col'>과정명</th>
              <th scope='col'>과제명</th>
              <th scope='col'>교수</th>
              <th scope='col'>제출</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                <c:choose>
                    <c:when test="${empty result.hwsId}">
                        <c:set var="submitResultText" value="미제출"/>
                        <c:set var="textcolorClass" value=""/>
                        <c:set var="registAction" value="regist"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="submitResultText" value="제출완료"/>
                        <c:set var="textcolorClass" value="font-blue"/>
                        <c:set var="registAction" value="updt"/>
                    </c:otherwise>
                </c:choose>
                <c:if test="${result.stuOpenAt eq 'Y'}">
                	<c:set var="registAction" value="view"/>
                </c:if>
                <c:url var="viewUrl" value="/lms/manage/selectHomeworkSubmitArticle.do${_BASE_PARAM}">
                    <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                    <c:param name="hwId" value="${result.hwId }" />
                    <c:param name="hwsId" value="${result.hwsId }" />
                    <c:param name="crclId" value="${result.crclId }" />
                    <c:param name="plId" value="${result.plId }" />
                    <c:param name="registAction" value="${registAction }" />
                </c:url>
		            <tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
		              <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
		              <td>${result.crclLangNm }</td>
		              <td>${result.closeDate}</td>
		              <td>${result.hwCodeNm}/${result.hwTypeNm}</td>
		              <td class='left-align'>${result.crclNm}</td>
		              <td class='left-align'>${result.nttSj}</td>
		              <td>${result.userNm}</td>
		              <td class="${textcolorClass }">${submitResultText }</td>
		            </tr>
		    </c:forEach>
          </tbody>
        </table>
        <div class="pagination center-align mt-60">
            <div class="pagination-inner-wrap overflow-hidden inline-block">
                <c:url var="startUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="1" />
                </c:url>
                <button class="start goPage" data-url="${startUrl}"></button>
               
                <c:url var="prevUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
                </c:url>
                <button class="prev goPage" data-url="${prevUrl}"></button>
               
                <ul class="paginate-list f-l overflow-hidden">
                    <c:url var="pageUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}"/>
                    <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
                    <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
                </ul>
               
                <c:url var="nextUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
                </c:url>
                <button class="next goPage" data-url="${nextUrl}"></button>
               
                <c:url var="endUrl" value="/lms/manage/homeworkTotalList.do${_BASE_PARAM}">
                    <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
                </c:url>
                <button class="end goPage" data-url="${endUrl}"></button>
            </div>
        </div>
      </div>
    </article>
</section>
</div>
</div>
</div>
</div>

    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>