<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${param.step}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<link rel="stylesheet" href="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.min.css?v=2">
<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script src="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js?v=1"></script>

<script>
$(document).ready(function(){
	//초기 값 설정
	$(".table-inside-input").val($("input[name=hwIdList]:checked").length);
	
	//반 선택
	$(".selClass").change(function(){
		var clas = $(this).val();
		
		$("input[name=searchClassCnt]").val(clas);
		$("#searchForm").submit();
	});
	
	//저장
	$("#btn-reg").click(function(){
		$("#frm").submit();
	});
});
</script>

<div class="page-content-header">
	<c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
</div>
          
<section class="page-content-body">            
            <article class="content-wrap">
            	<form name="frm" id="searchForm" method="post" action="<c:url value="/lms/quiz/QuizExamView.do"/>">
					<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
					<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
					<input type="hidden" name="step" value="${searchVO.step}"/>
					<input type="hidden" name="thirdtabstep" value="${searchVO.thirdtabstep}"/>
					
		            <div class="box-wrap mb-40">
		                <h3 class="title-subhead">학생 검색</h3>
		                <div class="flex-row-ten">
		                  <div class="flex-ten-col-10">
		                    <div class="ell">
		                      <input type="text" name="searchStudentUserNm" value="${searchVO.searchStudentUserNm}" placeholder="학생 이름을 검색해보세요">
		                    </div>
		                  </div>
		                </div>
		
		                <button class="btn-sm font-400 btn-point mt-20">검색</button>
		            </div>
              	</form>
            </article>
            
            <form name="frm" id="frm" method="post" action="<c:url value="/lms/quiz/QuizExamView.do"/>">
            	<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
				<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
				<input type="hidden" name="step" value="${searchVO.step}"/>
				<input type="hidden" name="thirdtabstep" value="${searchVO.thirdtabstep}"/>
					
	            <article class="content-wrap">
                  <!-- 테이블영역-->
                  <table class="common-table-wrap ">
	                <colgroup>
	                  <col style='width:7%'>
	                  <col>
	                  <col>
	                  <col>
	                  <col>
	                  <col style='width:7%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                  <col style='width:10%'>
	                </colgroup>
	                <thead>
	                  <tr class='font-700'>
	                    <th scope='col' class='bg-gray'>No</th>
	                    <th scope='col' class='bg-gray'>소속</th>
	                    <th scope='col' class='bg-gray'>이름</th>
	                    <th scope='col' class='bg-gray'>생년월일</th>
	                    <th scope='col' class='bg-gray'>학번</th>
	                    <th scope='col' class='bg-gray'>학년</th>
	                    <th scope='col' class='bg-light-gray'>총문항</th>
	                    <th scope='col' class='bg-light-gray'>정답수</th>
	                    <th scope='col' class='bg-light-gray'>점수 (100점 환산점수)</th>
	                    <th scope='col' class='bg-light-gray'>평균</th>
	                    <th scope='col' class='bg-light-gray'>편차</th>
	                  </tr>
	                </thead>
	                <tbody>
	                  <tr class="">
	                    <td scope='row'>35</td>
	                    <td>몽골어과</td>
	                    <td>강바른</td>
	                    <td>000000</td>
	                    <td>0000000</td>
	                    <td>1학년</td>
	                    <td>50</td>
	                    <td>20</td>
	                    <td>40</td>
	                    <td>6</td>
	                    <td>34</td>
	                  </tr>
	                  <tr class="">
	                    <td scope='row'>34</td>
	                    <td>몽골어과</td>
	                    <td>강바른</td>
	                    <td>000000</td>
	                    <td>0000000</td>
	                    <td>1학년</td>
	                    <td>50</td>
	                    <td>20</td>
	                    <td>40</td>
	                    <td>6</td>
	                    <td>34</td>
	                  </tr>
	                  <tr class="">
	                    <td scope='row'>33</td>
	                    <td>몽골어과</td>
	                    <td>강바른</td>
	                    <td>000000</td>
	                    <td>0000000</td>
	                    <td>1학년</td>
	                    <td>50</td>
	                    <td>20</td>
	                    <td>40</td>
	                    <td>6</td>
	                    <td>34</td>
	                  </tr>
	                </tbody>
	              </table>
	            </article>
            	<!-- 
	            <div class="page-btn-wrap mt-50">
	              <a href="#" id="btn-reg" class="btn-xl btn-point">저장</a>
	            </div>
	             -->
            </form>
          </section>
        </div>

</div>
</div>
</div>

    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>