<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%> <%@
taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> <%@ taglib
uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="CML" value="/template/lms" />
<!DOCTYPE html>

<html lang="ko">
  <head>
    <!--=================================================
            메타 태그
  ==================================================-->
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <title>한국외국어대학교</title>

    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <meta property="og:type" content="website" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="${CML}/imgs/common/og.jpg" />
    <meta property="og:url" content="" />

    <!--=================================================
        파비콘
  ==================================================-->
    <link rel="shortcut icon" href="${CML}/imgs/common/favicon.png" />

    <!--=================================================
        공통 스타일시트
  ==================================================-->
    <link href="${CML}/font/font.css" rel="stylesheet" />
    <!-- 나눔스퀘어 -->
    <link
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap"
      rel="stylesheet"
    />
    <!-- Poppins -->
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      rel="stylesheet"
      href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"
    />
    <!-- froala_editor -->
    <link rel="stylesheet" href="${CML}/lib/slick/slick.css" />
    <!-- slick -->
    <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css" />
    <link
      rel="stylesheet"
      href="${CML}/lib/daterangepicker/daterangepicker.css"
    />
    <!--daterangepicker -->

    <link rel="stylesheet" href="${CML}/css/common/base.css?v=1" />
    <link rel="stylesheet" href="${CML}/css/common/common.css?v=1" />
    <link rel="stylesheet" href="${CML}/css/common/board.css?v=1" />

    <!--=================================================
        페이지별 스타일시트
  ==================================================-->
    <link rel="stylesheet" href="${CML}/css/textbook/textbook.2.1.css?v=2" />

    <!--=================================================
          공통 스크립트
  ==================================================-->
    <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <!-- 제이쿼리 -->
    <script src="${CML}/lib/select2/select2.min.js"></script>
    <!-- select2 -->
    <script src="${CML}/lib/slick/slick.js"></script>
    <!-- slick -->
    <script src="${CML}/lib/slick/slick.min.js"></script>
    <!-- slick -->
    <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
    <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script>
    <!-- froala_editor -->
    <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script>
    <!-- dotdotdot(말줄임) -->
    <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
    <!--daterangepicker -->

    <script src="${CML}/js/common.js?v=1"></script>
    <!--=================================================
          페이지별 스크립트
  ==================================================-->
    <script src="${CML}/js/textbook/textbook.js?v=1"></script>
    <script>
      $(document).ready(function () {
        $(".tab-list").click(function () {
          var id = $(this).children("a").data("id");

          $(".tab-list").removeClass("on");
          $(this).addClass("on");

          if (id != "all") {
            $(".dic-app-group").hide();
            $("#" + id)
              .parents(".dic-app-group")
              .show();
            $(".layout-wrap").hide();
            $("#" + id).show();
          } else {
            $(".dic-app-group").show();
            $(".layout-wrap").show();
          }

          return false;
        });
      });
    </script>
  </head>

  <body>
    <div class="dic-app-wrap">
      <div class="dic-app-header-wrap">
        <header class="gnb">
          <div class="top-header area">
            <h1>
              <a href="/index.do" class="flex">
                <img src="${CML}/imgs/common/img_header_logo.png" alt="헤더로고"/>
              </a>
            </h1>
          </div>
        </header>
        <div class="area dic-app-subtop-wrap">
          <div class="flex-row no-gutters">
            <img src="${CML}/imgs/page/subtop/img_subtop_textbook_appLogo.jpg"/>
            <div class="ml-30">
              <p>특수외국어</p>
              <p class="title">초급교재 및 필수어휘 학습사전 APP</p>
              <p class="desc">
                11개 특수외국어 필수어휘 학습사전 및<br />초급교재 샘플자료를
                <strong>무료로 다운로드 받으세요!</strong>
              </p>
            </div>
          </div>
        </div>
      </div>
      <section class="dic-app-contents-wrap">
        <div class="area">
          <!-- tab style -->
          <ul class="tab-wrap line-style">
            <li class="tab-list on">
              <a href="#" data-id="all">전체</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app1">몽골어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app2">스와힐리어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app3">우즈베크어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app4">이란어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app5">말레이인도네시아어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app6">터키어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app7">태국어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app8">포르투갈/브라질어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app9">헝가리어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app10">폴란드어</a>
            </li>
            <li class="tab-list">
              <a href="#" data-id="app11">힌디어</a>
            </li>
          </ul>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app1" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">몽골어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/25/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"/></a>
                    <a href="javascript:void(0)" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"/></a>
                    <a href="http://www.특수외국어.com/b1_mo/index.html" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                    <a href=""><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
            <dl id="app2" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">스와힐리어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/6/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"/></a>
                    <a href="http://cflehufs.pubple.com/viewer/fixed/26/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"/></a>
                    <a href="http://ebookva2.gabia.io/b1_sw/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                    <a href="http://www.특수외국어.com/b2_sw/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app3" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">우즈베크어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/7/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"/></a>
                    <a href="http://cflehufs.pubple.com/viewer/fixed/27/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"/></a>
                    <a href="http://ebookva2.gabia.io/b1_uz/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"/></a>
                    <a href="http://www.특수외국어.com/b2_uz/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
            <dl id="app4" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">이란어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                   	<a href="http://cflehufs.pubple.com/viewer/fixed/8/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/28/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_pe/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="http://www.특수외국어.com/b2_pe/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app5" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">말레이인도네시아어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/9/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/29/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_in/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="http://www.특수외국어.com/b2_in/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
            <dl id="app6" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">터키어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/10/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="javascript:void(0)" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_tu/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="javascript:void(0)"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app7" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">태국어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/14/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a0_level.svg" alt="A0 Level"></a>
                    <a href="http://cflehufs.pubple.com/viewer/fixed/15/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/30/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_th/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                 	<a href="http://www.특수외국어.com/b2_th/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
            <dl id="app8" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">포르투갈·브라질어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/11/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/31/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                 	<a href="http://ebookva2.gabia.io/b1_po/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                 	<a href="http://www.특수외국어.com/b2_po/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app9" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">헝가리어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                     <a href="http://cflehufs.pubple.com/viewer/fixed/19/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/33/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_hu/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="http://www.특수외국어.com/b2_hu/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
            <dl id="app10" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">폴란드어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/18/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/32/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_pol/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="http://www.특수외국어.com/b2_pol/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
          <!-- 사전 행 -->
          <div class="dic-app-group">
            <dl id="app11" class="layout-wrap">
              <dt class="dic-app-name">
                <span class="app-title">힌디어</span>
              </dt>
              <dd class="dic-app-items">
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_free.svg" alt="어휘사전APP"/>
                  </p>
                  <div class="app-btn">
                    <a href="https://play.google.com/store/apps/details?id=com.hufs.essentialdictionary" class="mb-3">
                    	<img src="${CML}/imgs/page/textbook/btn_app_google_play.svg" alt="Google Play"/>
                    </a>
                    <a href="https://apps.apple.com/us/app/특수외국어-필수어휘사전/id1489668996?l=ko&ls=1">
                    	<img src="${CML}/imgs/page/textbook/btn_app_store.svg" alt="App Store"/>
                    </a>
                  </div>
                </div>
                <div class="dic-app-inner">
                  <p class="app-imgs">
                    <img src="${CML}/imgs/page/textbook/icon_app_sample.svg" alt="초급교재Sample"/>
                  </p>
                  <div class="app-btn">
                    <a href="http://cflehufs.pubple.com/viewer/fixed/12/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a0_level.svg" alt="A0 Level"></a>
                    <a href="http://cflehufs.pubple.com/viewer/fixed/13/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a1_level.svg" alt="A1 Level"></a>
                  	<a href="http://cflehufs.pubple.com/viewer/fixed/34/en/pd/" class="mb-3"><img src="${CML}/imgs/page/textbook/btn_app_a2_level.svg" alt="A2 Level"></a>
                  	<a href="http://ebookva2.gabia.io/b1_hi/index.html" class="mb-3" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b1_level.svg" alt="B1 Level"></a>
                  	<a href="http://www.특수외국어.com/b2_hi/index.html" target="_blank"><img src="${CML}/imgs/page/textbook/btn_app_b2_level.svg" alt="B2 Level"></a>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
        </div>
      </section>
    </div>

    <c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
      <c:param name="footTopAt" value="N" />
    </c:import>
  </body>
</html>
