<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
    <c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
    <c:if test="${not empty searchVO.searchPlanSttusCode}"><c:param name="searchPlanSttusCode" value="${searchVO.searchPlanSttusCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function() {
	//검색 초기화
	$(document).on("click", "#searchReset", function() {
	    $("form[name='frm']").find("input").not("#resetBtn").val("");
	    $("form[name='frm'] select").val("").trigger("change");
	    $("form[name='frm'] input").prop("checked", false);
	});
});

</script>
        <section class="page-content-body">
          <form name="frm" method="post" action="<c:url value="/lms/crm/selectCurseregManage.do"/>">
            <input type="hidden" name="menuId" value="${param.menuId }" />

            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">수강신청 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                        <select class="select2" name="searchCrclYear" id="searchCrclYear" data-select="style3">
                            <option value="">년도</option>
                            <c:forEach var="result" items="${yearList}" varStatus="status">
                                <option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
                            </c:forEach>
                        </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                      <select class="select2" data-select="style3" id="searchCrclTerm" name="searchCrclTerm">
                        <option value="">학기</option>
                        <c:forEach var="result" items="${crclTermList}" varStatus="status">
                            <c:if test="${result.ctgryLevel eq '1'}">
                                <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
                            </c:if>
                        </c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="desc">
                      <input type="text" name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" id="searchApplyStartDate" class="ell date datepicker type2" placeholder="신청 시작일" readonly="readonly"/>
                      <i>~</i>
                      <input type="text" name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" id="searchApplyEndDate" class="ell date datepicker type2" placeholder="신청 종료일" readonly="readonly"/>
                    </div>
                  </div>
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                        <select class="select2" name="searchApplySttusCode" id="searchApplySttusCode" data-select="style3">
                            <option value="">수강신청 현황</option>
                            <option <c:if test="${param.searchApplySttusCode eq 1}">selected</c:if> value="1">대기</option>
                            <option <c:if test="${param.searchApplySttusCode eq 3}">selected</c:if> value="3">접수중</option>
                            <option <c:if test="${param.searchApplySttusCode eq 4}">selected</c:if> value="4">종료</option>
                        </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4">
                    <div class="desc">
                      <input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="ell date datepicker type2" placeholder="과정 시작일" readonly="readonly"/>
                      <i>~</i>
                      <input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="ell date datepicker type2" placeholder="과정 종료일" readonly="readonly"/>
                    </div>
                  </div>
                  <div class="flex-ten-col-6 mb-20">
                    <div class="ell">
                      <input name="searchCrclNm" type="text" placeholder="찾으시는 과정명을 입력해보세요" value="${searchVO.searchCrclNm }">
                    </div>
                  </div>
                  <div class="flex-ten-col-4">
                    <div class="ell">
                      <input name="searchUserNm" type="text" placeholder="찾으시는 교수명을 입력해보세요" value="${searchVO.searchCrclNm }">
                    </div>
                  </div>
                  <div class="flex-ten-col-4 flex align-items-center">
                    <label class="checkbox">
                      <input type="checkbox" name="myCurriculumPageFlag" value="teacher" <c:if test="${searchVO.myCurriculumPageFlag eq 'teacher' }">checked</c:if> />
                      <span class="custom-checked"></span>
                      <span class="text">나의 과정</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchPlanSttusCode" value="1" <c:if test="${searchVO.searchPlanSttusCode eq '1' }">checked</c:if> />
                      <span class="custom-checked"></span>
                      <span class="text">과정계획 대기</span>
                    </label>
                  </div>
                </div>
                <button class="btn-sm font-400 btn-point mt-20">검색</button>
                <a class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</a>
              </div>
            </article>
          </form>
          <article class="content-wrap">
            <div class="content-header">
              <div class="btn-group-wrap">
                <div class="left-area">
                  <a href="#" class="btn-outline-gray btn-md-auto btnModalOpen" data-modal-type="refund_policy">환불처리 기준 안내</a>
                  <!-- <a href="#" class="btn-outline btn-md">엑셀 다운로드</a> -->
                </div>
              </div>
            </div>
            <div class="content-body">
             <div class="mt-20">
               <!-- 테이블영역-->
               <table class="common-table-wrap table-type-board list_table">
                 <colgroup>
                   <col style='width:7%'>
                   <col style='width:9%'>
                   <col style='width:9%'>
                   <col style='width:20%'>
                   <col style='width:12%'>
                   <col style='width:60px'>
                   <col style='width:100px'>
                   <col style='width:100px'>
                   <col style='width:9%'>
                   <col style='width:70px'>
                   <col style='width:9%'>
                 </colgroup>
                 <thead>
                   <tr class='bg-light-gray font-700'>
                     <th scope='col'>No</th>
                     <th scope='col'>년도</th>
                     <th scope='col'>학기</th>
                     <th scope='col'>과정명</th>
                     <th scope='col'>책임/강의책임교원</th>
                     <th scope='col'>과정<br>계획서</th>
                     <th scope='col'>과정기간</th>
                     <th scope='col'>수강신청<br>기간</th>
                     <th scope='col'>수강정원</th>
                     <th scope='col'>신청현황</th>
                     <th scope='col'>수강대상자 확정</th>
                   </tr>
                 </thead>
                 <tbody>
                   <c:forEach var="result" items="${resultList}" varStatus="status">
                       <c:url var="viewUrl" value="/lms/crm/curriculumManage.do${_BASE_PARAM}">
                           <c:param name="crclId" value="${result.crclId}"/>
                           <c:param name="crclbId" value="${result.crclbId}"/>
                           <c:param name="subtabstep" value="1"/>
                           <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                       </c:url>
                       <tr onclick="location.href='${viewUrl}'">
                           <td class="row"><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
                           <td><c:out value="${result.crclYear}"/></td>
                           <td><c:out value="${result.crclTermNm}"/></td>
                           <td><c:out value="${result.crclNm}"/></td>
                           <td><c:out value="${result.userNm}"/></td>
                           <td>
	                        <c:choose>
	                            <c:when test="${result.planSttusCode eq 1 }">대기
	                                <c:url var="redirectUrl" value="/lms/crcl/selectCurriculum.do">
		                                <c:param name="crclId" value="${result.crclId}"/>
		                                <c:param name="menuId" value="MNU_0000000000000062"/>
		                            </c:url>
	                                <a href="${redirectUrl }" class="btn-auto btn-outline mt-5">등록</a>
	                            </c:when>
	                            <c:when test="${result.planSttusCode eq 2 }">진행중</c:when>
	                            <c:when test="${result.planSttusCode eq 3 }">완료</c:when>
	                        </c:choose>
	                    </td>
	                    <td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
	                    <td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
	                    <td>
	                        <c:choose>
	                            <c:when test="${result.applyMaxCnt eq '0' or empty result.applyMaxCnt}">제한없음</c:when>
	                            <c:otherwise>
	                                <c:out value="${result.applyMaxCnt}"/>명
	                            </c:otherwise>
	                        </c:choose>
	                    </td>
	                    <td>
	                        <c:choose>
	                            <c:when test="${result.applySttusCode eq '0' }">
                                          <span class='font-gray-light font-700'>[대기]</span><br>과정계획<br>미등록
	                            </c:when>
	                            <c:when test="${result.applySttusCode eq '1' }"><span class='font-gray-light font-700'>[대기]</span></c:when>
	                            <c:when test="${result.applySttusCode eq '3' }">
	                                <c:if test="${result.applyMaxCnt ne 0 }"><c:out value="${result.acceptStudentCnt}"/>/<c:out value="${result.applyMaxCnt}"/></c:if>
	                                <br />
	                                <span class="ing">접수중</span>
	                            </c:when>
	                            <c:when test="${result.applySttusCode eq '4' }"><span class="ready">종료</span></c:when>
	                            <c:when test="${result.applySttusCode eq '5' }"><span class="ready">과정개설취소</span></c:when>
	                            <c:otherwise><span class="ready">종료</span></c:otherwise>
	                        </c:choose>
	                    </td>
	                    <td>
	                        <c:choose>
	                          <c:when test="${6 <= result.processSttusCodeDate }">완료</c:when>
	                          <c:otherwise>대기</c:otherwise>
	                        </c:choose>
	                    </td>
                       </tr>
                   </c:forEach>
                   <c:if test="${fn:length(resultList) == 0}">
                       <tr>
                           <td class="listCenter" colspan="11"><spring:message code="common.nodata.msg" /></td>
                       </tr>
                   </c:if>
                 </tbody>
               </table>
               <div class="pagination center-align mt-60">
	                <div class="pagination-inner-wrap overflow-hidden inline-block">
	                    <c:url var="startUrl" value="/lms/crm/selectCurseregManage.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="1" />
	                    </c:url>
	                    <button class="start goPage" data-url="${startUrl}"></button>
	                   
	                    <c:url var="prevUrl" value="/lms/crm/selectCurseregManage.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
	                    </c:url>
	                    <button class="prev goPage" data-url="${prevUrl}"></button>
	                   
	                    <ul class="paginate-list f-l overflow-hidden">
	                        <c:url var="pageUrl" value="/lms/crm/selectCurseregManage.do${_BASE_PARAM}"/>
	                        <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
	                        <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	                    </ul>
	                   
	                    <c:url var="nextUrl" value="/lms/crm/selectCurseregManage.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	                    </c:url>
	                    <button class="next goPage" data-url="${nextUrl}"></button>
	                   
	                    <c:url var="endUrl" value="/lms/crm/selectCurseregManage.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	                    </c:url>
	                    <button class="end goPage" data-url="${endUrl}"></button>
	                </div>
	            </div>
             </div>
            </div>
          </article>
        </section>
      </div>
    </div>
  </div>
</div>

<div id="refund_policy_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">환불처리 기준 안내</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <div class="refund_content">${board.nttCn }</div>
          <c:import url="/lms/crm/selectFileInfs.do" charEncoding="utf-8">
	        <c:param name="param_atchFileId" value="${board.atchFileId}" />
	      </c:import>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
      </div>
    </div>
  </div>
  
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
</c:import>