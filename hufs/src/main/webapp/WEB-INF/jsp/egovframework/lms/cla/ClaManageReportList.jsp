<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" />

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.allSubmit}"><c:param name="allSubmit" value="${searchVO.allSubmit}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.reportType}"><c:param name="reportType" value="${searchVO.reportType}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>

function fnSubmit(){
	$("form[name='frm']").submit();
}

function fnReset(){
	 $("form[name='frm']").find("input").not("#resetBtn").val("");
     $("form[name='frm'] select").val("").trigger("change");
     $("form[name='frm'] input").prop("checked", false);
}
</script>

<section class="page-content-body">
		<form name="frm" method="post" action="/lms/cla/manageReportList.do">
			<input type="hidden" name="menuId" value="${searchVO.menuId }"/>
            <article class="content-wrap">
              <!-- 게시판 검색영역 -->
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">운영보고서 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select name="searchProcessSttusCodeDate" id="searchProcessSttusCodeDate" class="select2" data-select="style3" >
                        <option value="">과정상태 전체</option>
							<c:forEach var="result" items="${statusComCode}" varStatus="status">
								<option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCodeDate}">selected="selected"</c:if>>${result.codeNm}</option>
							</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" placeholder="책임교원 명">
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" placeholder="과정명">
                    </div>
                  </div>
                  <div class="flex-ten-col-2 mb-20 flex align-items-center">
                    <label class="checkbox">
                      <input type="checkbox" name="allSubmit" value="Y" ${searchVO.allSubmit eq 'Y' ? 'checked' : ''}>
                      <span class="custom-checked"></span>
                      <span class="text">과정만족도 완료</span>
                    </label>
                  </div>
                  <div class="flex-ten-col-6">
                    <div class="desc">
                      <input type="text" class="ell date datepicker type2" value="${searchVO.searchStartDate}" name="searchStartDate" id="searchStartDate" placeholder="과정기간 시작일">
                      <i>~</i>
                      <input type="text" class="ell date datepicker type2" value="${searchVO.searchEndDate}" name="searchEndDate" id="searchEndDate" placeholder="과정기간 종료일">
                    </div>

                  </div>
                  <div class="flex-ten-col-4">
                    <div class="ell">
                      <select name="reportType" id="reportType" class="select2" data-select="style3">
                        <option value="" ${empty searchVO.reportType ? 'checked' : ''}>운영보고서 유형 전체</option>
                        <option value="project" ${searchVO.reportType eq 'project'? 'checked' : ''}>프로젝트 유형</option>
                        <option value="common" ${searchVO.reportType eq 'common' ? 'checked' : ''}>일반 유형</option>
                      </select>
                    </div>
                  </div>
                </div>

                <button type="button" class="btn-sm font-400 btn-point mt-20" onclick="fnSubmit()">검색</button>

                <button type="button" class="btn-sm font-400 btn-outline mt-20" onclick="fnReset()">초기화</button>
              </div>
            </article>
            </form>
            <article class="content-wrap">
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:14%'>
                    <col style='width:14%'>
                    <col>
                    <col style='width:14%'>
                    <col style='width:14%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>과정 시작일</th>
                      <th scope='col'>과정 종료일</th>
                      <th scope='col'>운영보고서 유형</th>
                      <th scope='col'>과정상태</th>
                      <th scope='col'>과정명</th>
                      <th scope='col'>책임교원</th>
                      <th scope='col'>과정만족도<br>완료여부</th>
                    </tr>
                  </thead>
                  <tbody>
                  <c:choose>
                  	<c:when test="${not empty resultList }">
                  		<c:forEach items="${resultList}" var="result"  varStatus="status">
			   				<c:set var="viewUrlYn" value="N"/>
                  			<c:url var="viewUrl" value="/lms/manage/manageReport.do">
			  					<c:param name="crclId" value="${result.crclId}"/>
			  					<c:param name="menuId" value="${searchVO.menuId}"/>
			  					<c:param name="step" value="9"/>
			   				</c:url>
		   					<c:if test="${(result.manageYn eq 'Y' and curDate ge result.endDate) or (result.manageYn eq 'N' and result.processSttusCodeDate eq '12')}">
	   							<c:set var="viewUrlYn" value="Y"/>
		   					</c:if>
	                  		<tr <c:if test="${viewUrlYn eq 'Y'}"> onclick="location.href='${viewUrl}'" </c:if> class=" cursor-pointer">
		                      <td scope='row'>${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}</td>
		                      <td>${result.startDate }</td>
		                      <td>${result.endDate }</td>
		                      <td>${result.reportType eq 'common' ? '일반 유형' : '프로젝트 유형'}</td>
		                      <td>
			    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
			    					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
								</c:forEach>
			    			  </td>
		                      <td class='left-align'>${result.crclNm }</td>
		                      <td>${result.userNm }</td>
		                      <td>
		                      	<c:choose>
		                      		<c:when test="${curDate lt result.endDate }">
										대기
		                      		</c:when>
		                      		<c:otherwise>
		                      			<c:url var="popUrl" value="/lms/crcl/popCurriculumSurveyView.do">
						  					<c:param name="crclId" value="${result.crclId}"/>
						  					<c:param name="schdulId" value="${result.surveySatisfyType}"/>
						   				</c:url>
		                      			<c:choose>
				                      		<c:when test="${result.memberCnt ne 0 and result.memberCnt eq result.joinCnt }">
				                      			<a href="#none" onclick="window.open('${popUrl}','팝업창','width=800,height=800');event.stopPropagation();" target="_blank"><span class='font-point'>전원제출<br>(${result.joinCnt }/${result.memberCnt })</span></a>
				                      		</c:when>
				                      		<c:otherwise>
				                      			<a href="#none" onclick="window.open('${popUrl}','팝업창','width=800,height=800');event.stopPropagation();" target="_blank"><span class='font-point'>${result.joinCnt }/${result.memberCnt }명</span></a>
				                      		</c:otherwise>
				                      	</c:choose>
		                      		</c:otherwise>
		                      	</c:choose>
		                      </td>
		                    </tr>
                  		</c:forEach>
                  	</c:when>
                  	<c:otherwise>
						<tr>
				        	<td class="listCenter" colspan="8"><spring:message code="common.nodata.msg" /></td>
				      	</tr>
                  	</c:otherwise>
                  </c:choose>

                  </tbody>
                </table>
                <div class="pagination center-align mt-60">
                  <div class="pagination-inner-wrap overflow-hidden inline-block">

                   <c:url var="startUrl" value="/lms/cla/manageReportList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start goPage" data-url="${startUrl}"></button>

	               	<c:url var="prevUrl" value="/lms/cla/manageReportList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev goPage" data-url="${prevUrl}"></button>

	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/cla/manageReportList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>

	               	<c:url var="nextUrl" value="/lms/cla/manageReportList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next goPage" data-url="${nextUrl}"></button>

	               	<c:url var="endUrl" value="/lms/cla/manageReportList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end goPage" data-url="${endUrl}"></button>
                  </div>
                </div>
              </div>
            </article>
          </section>
</div>
</div>
</div>
</div>


<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>