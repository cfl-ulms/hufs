<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.crclbId}"><c:param name="crclbId" value="${searchVO.crclbId}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
    <c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
    <c:if test="${not empty searchVO.searchPlanSttusCode}"><c:param name="searchPlanSttusCode" value="${searchVO.searchPlanSttusCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<style>
.div_student_box{float:left;min-width:75px;}
</style>

<script>
alert("준비 중입니다.");
history.back();

$(document).ready(function() {
	//반 추가
	$(document).on("click", ".btn-add-list", function(){
		var setClassTr      = 0;
        var classCnt        = parseInt($(".class_cnt").val());
        var html            = "";
        var forFirstCnt     = 1;
        var arrRemoveUserId = new Array();
        
        //기존에 있는 tr 개수 설정
        $(".tr_student_list").each(function(){
            setClassTr++;
        });
        setClassTr++; //다음반이므로 한번더 더한다.

        //담당교수 select box option html 추가
        var htmlSelectOption = $("select[name='manageIdList']:first").html();

	    html += "<tr class='tr_student_list' id='classnum_" + setClassTr + "'>";
	    html += "   <td scope='row'>";
	    html += "        <span class='class_number'>" + setClassTr + "반</span>";
	    //html += "        <input type='hidden' class='manageIdList' name='manageIdList' value='" + managerId + "' />";
	    html += "        <input type='hidden' class='manageClassCntList' name='manageClassCntList' value='" + setClassTr + "' />";
	    html += "   </td>";
	    html += "   <td>";
	    html += "   <select class='table-select select2 search' data-select='style1' name='manageIdList'>";
	    html +=     htmlSelectOption;
	    html += "   </select>";
	    html += "   </td>";
	    html += "   <td><input type='text' class='searchUserNm' placeholder='학생 명을 입력.' data-classcnt='" + setClassTr + "' /></td>";
	    html += "   <td class='keep-all'>";
	    
	    html += "      <div class='flex-row'>";   
	    html += "          <div class='flex-col self-center keep-all divStudent'>";
	    html += "          </div>";
	    html += "          <div class='flex-col-auto'><button class='btn-delete btnModalOpen classDeleteModal' data-classnum='" + setClassTr + "' type='button' title='반 삭제' data-modal-type='confirm' data-modal-header='알림' data-modal-text='삭제된 반은 복구가 불가능합니다. <br>반을 삭제하시겠습니까?' data-modal-rightbtn='삭제'></button></div>";
	    html += "      </div>";
	    
	    
	    html += "   </td>";
	    html += "</tr>";
	    
	    $(".trAddBtn").before(html);
	    
	    Lms.common_ui.init();
	});
	
	//학생검색
    $(document).on("focus", ".searchUserNm", function(){
        var option = fn_student_search_opion();
        $(this).autocomplete(option);
    });
	
    //학생 삭제
    $(document).on("click", ".student_delete_btn", function(){
        var userId = $(this).parent().data("userid");
        
        $(this).parent().remove();

        $(".tr_student").each(function(){
            if($(this).data("userid") == userId) {
                $(this).removeClass("pick_complate");
                $(this).data("pickflag", "false");
            }
        });
    });
    
    //반배정 확정 modal
    $(document).on("click", ".class_complate_btn", function(){
    	$("#update_modal").show();
    });
    
    //반배정 확정
    $(document).on("click", ".class_update_btn", function(){
        var classCnt = $(".tr_student_list").length;
        
        if(classCnt <= 0) {
            alert("최소 한개의 반이 필요합니다.");
            return false;
        }
        
        $("#listForm").submit();
    });
    
    //반삭제 modal
    $(document).on("click", ".classDeleteModal", function(){
    	$("#classnum").val($(this).data("classnum"));
    });
    
    //반삭제 modal
    $(document).on("click", ".deleteClass", function(){
        $("#classnum_" + $("#classnum").val()).remove();

        //조이름 다시 처리
        var class_number = 1;

        $(".tbl_student_pick tr.tr_student_list").each(function(){
            $(this).find("td span.class_number").text(class_number + "반");
            $(this).find("td .manageClassCntList").val(class_number);
            class_number++;
        });
    });
});

//담당교수 검색 autocomplate option
function fn_student_search_opion() {
    var option = {
        source : function(request, response){
            $.ajax({
                type:"post",
                    dataType:"json",
                    url:"/mng/lms/crm/studentJson.json",
                    data:{crclId : "${curriculumVO.crclId}", searchUserNm : $(this.element).val()},
                    success:function(result){
                        response($.map(result.items, function(item){
                        	var userName = "";

                        	if(item.mngDeptNm == null) {
                        		userName = item.userNm;
                        	} else {
                        		userName = item.userNm + "("+item.mngDeptNm+")";
                        	}
                            return{
                            label:userName,
                            value:item.userId,
                            userNm:item.userNm
                            }
                        }));
                    },
                    error: function(){
                        alert('문제가 발생하여 작업을 완료하지 못하였습니다.');
                    }
            })
        },
        matchContains:true,
        selectFirst:false,
        minLength:2,               //1글자 이상 입력해야 autocomplete이 작동한다.
        delay:100,                 //milliseconds
        select:function(event,ui){ //ui에는 선택된 item이 들어가있다.(custom 변수 포함)
            var html        = "";
            var alreadyFlag = true;
            
            html += "<div class='div_student_box' data-userid='" + ui.item.value + "'>";
            html += "    <input type='hidden' name='userIdList' value='" + ui.item.value + "'>";
            html += "    <input type='hidden' name='classCntList' value='" + $(this).data("classcnt") + "'>";
            html += "    <span>" + ui.item.userNm + " | </span>";
            html += "    <a href='#none' class='student_delete_btn'>X</a>";
            html += "</div>";

            $(".div_student_box").each(function(){
                if($(this).data("userid") == ui.item.value){
                    alert("이미 등록 된 학생 입니다.");
                    alreadyFlag = false;

                    return false;
                } 
            });
            
            if(alreadyFlag) {
                $(this).parent().next().find(".divStudent").append(html);
            }

            return false;
        },
        focus:function(event, ui){return false;} //한글입력시 포커스이동하면 서제스트가 삭제되므로 focus처리
    };

    return option;
}
</script>
          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="Y"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <!-- 두번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumrequest"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
                <c:param name="managerAt" value="${managerAt}"/>
            </c:import>
            
            <!-- 세번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumstudent"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
            </c:import>
            <%-- 담당 교수만 변경하도록 처리 --%>
            <c:forEach var="user" items="${subUserList}" varStatus="status">
                <c:if test="${user.userId eq USER_INFO.id }">
                    <c:set var="updateFlag" value="true"/>
                </c:if>
            </c:forEach>
            <c:choose>
                <c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and updateFlag eq 'true' }">
		            <article class="content-wrap">
		              <form id="listForm" method="post" action="/lms/crm/updateCurriculumClass.do">
		                  <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
		                  <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
		                  <input type="hidden" name="menuId" value="${param.menuId}"/>
		                  <input type="hidden" name="subtabstep" value="${param.subtabstep}"/>
		                  <input type="hidden" name="thirdtabstep" value="3"/>
		
			              <!-- 테이블영역-->
			              <table class="common-table-wrap tbl_student_pick">
			                <colgroup>
			                  <col style='width:11%'>
			                  <col style='width:25%'>
			                  <col style='width:190px'>
			                  <col>
			                </colgroup>
			                <thead>
			                  <tr class='bg-light-gray font-700'>
			                    <th scope='col'>분반</th>
			                    <th scope='col'>담당교수</th>
			                    <th scope='col'>학생배정</th>
			                    <th scope='col'>배정된 학생 명단</th>
			                  </tr>
			                </thead>
			                <tbody>
			                  <c:forEach var="curriculumClass" items="${curriculumClassList}" varStatus="status">
			                      <tr class="tr_student_list" id="classnum_${curriculumClass.classCnt }">
				                    <td scope='row'>
				                      <span class='class_number'>${curriculumClass.classCnt }반</span>
				                      <input type="hidden" class="manageClassCntList" name="manageClassCntList" value="${curriculumClass.classCnt }" />
				                    </td>
				                    
				                    <td>
				                      <select class='table-select select2 search' data-select='style1' name="manageIdList">
				                        <c:forEach var="fac" items="${facList}" varStatus="status">
				                            <option value="${fac.facId}" <c:if test="${curriculumClass.manageId eq fac.facId }">selected</c:if>>${fac.userNm}</option>
				                        </c:forEach>
				                      </select>
				                    </td>
				                    <td><input type="text" class="searchUserNm" placeholder="학생 명을 입력." data-classcnt="${curriculumClass.classCnt }" /></td>
				                    <td class='keep-all'>
				                        <div class="flex-row">
					                        <div class="flex-col self-center keep-all divStudent">
						                        <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status2">
					                                <c:if test="${pickStudent.classCnt eq curriculumClass.classCnt }">
					                                    <div class="div_student_box" data-userid="${pickStudent.userId }">
					                                        <input type="hidden" name="userIdList" value="${pickStudent.userId }">
					                                        <input type="hidden" name="classCntList" value="${pickStudent.classCnt }">
					                                        <span>${pickStudent.userNm } | </span>
					                                        <a href="#none" class="student_delete_btn">X</a>
					                                    </div>
					                                </c:if>
					                            </c:forEach>
					                        </div>
					                        <div class="flex-col-auto"><button class="btn-delete btnModalOpen classDeleteModal" type="button" data-classnum="${curriculumClass.classCnt }" title="반 삭제" data-modal-type="confirm" data-modal-header="알림" data-modal-text="삭제된 반은 복구가 불가능합니다. <br>반을 삭제하시겠습니까?" data-modal-rightbtn="삭제"></button></div>
					                    </div>
				                    </td>
				                  </tr>
			                  </c:forEach>
			                  <tr class="bg-point-light trAddBtn">
			                    <td scope='row' class='btn-add-list' colspan='4'>분반 추가</td>
			                  </tr>
			                </tbody>
			              </table>
			          </form>
		            </article>
	           </c:when>
	           <c:otherwise>
                    <section class="page-content-body">
                        <article class="content-wrap">
                            <div class="content-body flex-column">
                                <div class="box-area mb-0">
                                    <p>분반을 진행할 수 없는 상태입니다.</p>
                                </div>
                            </div>
                        </article>
                    </section>
                </c:otherwise>
	        </c:choose>
            <div class="page-btn-wrap mt-50">                                
              <c:choose>
	              <c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
			      </c:when>
			      <c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and updateFlag eq 'true'}">
			          <a href="/lms/crm/selectCurseregManage.do${_BASE_PARAM }&thirdtabstep=${param.thirdtabstep }&subtabstep=${param.subtabstep }" class="btn-xl btn-outline-gray">분반 취소</a>
			          <a href="#none" class="btn-xl btn-point class_complate_btn">분반 확정</a>
			      </c:when>
		      </c:choose>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

<div id="confirm_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text"></p>
        <p class="modal-subtext"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button type="button" class="btn-xl btn-point btnModalConfirm deleteClass">확인</button>
        <input type="hidden" id="classnum" val="" />
      </div>
    </div>
  </div>
</div>

<div id="update_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">알림</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text">이미 조가 배정되어 있을 경우, 분반 확정을 하면 초기화가 됩니다.<br />반을 수정하시겠습까?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button type="button" class="btn-xl btn-point class_update_btn">확인</button>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>