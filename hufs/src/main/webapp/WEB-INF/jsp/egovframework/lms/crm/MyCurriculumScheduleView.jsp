<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}" /></c:if>
    <c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
    <c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
    <c:if test="${not empty searchVO.searchCampusId}"><c:param name="searchCampusId" value="${searchVO.searchCampusId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
	<c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {
	//제출서류 압축파일 다운로드
	$(document).on("click", ".zip_down", function(){
		document.frmZipDown.submit();
	});
	
	//출석
	$(".atd").click(function(){
		var id = $(this).data("id"),
			sbj = $(this).data("nm"),
			crclId = $(this).data("crclid"),
			attTCnt = $(this).data("tot"),
			attYCnt = $(this).data("ycnt"),
			attLCnt = $(this).data("lcnt"),
			attNCnt = $(this).data("ncnt");
		
		$.ajax({
			url : "/lms/atd/selectAttendList.do"
			, type : "post"
			, dataType : "html"
			, data : {plId : id, crclId : crclId, modalAt : "Y"}
			, success : function(data){
				$("#sbj").text(sbj);
				$("#plId").val(id);
				$("#atdlist").html(data);
				
				$("#goAtd").attr("href", "/lms/atd/selectAttendList.do?menuId=MNU_0000000000000084&crclId=" + crclId + "&plId=" + id + "&step=5&tabType=T");
			}, error : function(){
				alert("error");
			}
		});
		
		return false;
	});
});

//출석 검색
function searchAtd(){
	var params = $("#atdFrom").serialize();
	
	$.ajax({
		url : "/lms/atd/selectAttendList.do"
		, type : "post"
		, dataType : "html"
		, data : params
		, success : function(data){
			$("#atdlist").html(data);
		}, error : function(){
			alert("error");
		}
	});
	
	return false;
}
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:choose>
                <%-- TODO : 교원은 PDF 다운로드 버튼을 사용할 수 있기 때문에 분기처리함 사용안하면 분기처리 안해도됨 --%>
                <c:when test="${8 <= USER_INFO.userSeCode }">
                    <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
		                <%-- <c:param name="crclId" value="${param.crclId}"/> --%>
		                <c:param name="curriculumManageBtnFlag" value="Y"/>
		            </c:import>
                </c:when>
                <c:otherwise>
                    <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                        <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
                    </c:import>
                </c:otherwise>
            </c:choose>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
				<c:param name="menu" value="myschedule"/>
				<c:param name="tabstep" value="${param.tabstep }"/>
				<c:param name="menuId" value="${param.menuId }"/>
				<c:param name="crclId" value="${param.crclId}"/>
				<c:param name="crclbId" value="${param.crclbId}"/>
			</c:import>
			
			<article class="content-wrap">
			  <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col style='width:10%'>
                  <col style='width:10%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>수업일</th>
                    <th scope='col'>시작시간</th>
                    <th scope='col'>종료시간</th>
                    <th scope='col'>교시</th>
                    <th scope='col'>과정명</th>
                    <th scope='col'>수업주제</th>
                    <th scope='col'>교수명</th>
                    <th scope='col'>강의실</th>
                    <th scope='col'>출석</th>
                  </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty resultList and param.subtabstep eq 1 }">
                    <tr>
                        <td colspan="9">시간표가 등록되지 않았습니다.</td>
                    </tr>
                    </c:when>
                    <c:when test="${empty resultList and param.subtabstep eq 2 }">
                    <tr>
                        <td colspan="9">오늘의 수업이 없습니다.</td>
                    </tr>
                    </c:when>
                    <c:when test="${curriculumVO.totalTime > 0}">
                        <c:forEach var="scheduleMngVO" items="${resultList}" varStatus="status">
                            <c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>

                            <%-- 수업일 폰트와 배경 색상 학생, 교원 분기 처리 --%>
                            <c:choose>
                                <c:when test="${8 <= USER_INFO.userSeCode and today eq startDt }">
                                    <c:set var="fontClass" value="font-point font-700 keep-all"/>
                                    <c:set var="trBgClass" value="bg-point-light cursor-pointer"/>
                                </c:when>
                                <c:when test="${8 > USER_INFO.userSeCode and today eq startDt }">
                                    <c:set var="fontClass" value="font-blue"/> 
                                    <c:set var="trBgClass" value="bg-lighten font-gray"/>
                                </c:when>
                            </c:choose>
                            
                            <c:url var="viewUrl" value="/lms/manage/studyPlanView.do${_BASE_PARAM}">
								<c:param name="plId" value="${scheduleMngVO.plId}" />
								<c:param name="crclId" value="${scheduleMngVO.crclId}" />
								<c:param name="tabType" value="T" />
							</c:url>
                            
	                        <tr class="<c:if test="${today >= startDt}">${trBgClass }</c:if>" onclick="location.href='${viewUrl}';">
		                        <td scope='row' class="<c:if test="${today eq startDt}">${fontClass }</c:if>"><c:out value="${scheduleMngVO.startDt}"/><c:if test="${today eq startDt}"><br />[오늘]</c:if></td>
		                        <td><c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/></td>
		                        <td><c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/></td>
		                        <td><c:out value="${scheduleMngVO.periodTxt}"/></td>
		                        <td class='left-align'>${scheduleMngVO.crclNm}</td>
		                        <td class='left-align'><c:out value="${scheduleMngVO.studySubject}"/></td>
		                        <td>
		                            <c:set var="facCnt" value="1"/>
                                    <c:forEach var="result" items="${facPlList}" varStatus="status">
                                        <c:if test="${result.plId eq scheduleMngVO.plId}">
                                            <div>
                                                <span>
                                                    <c:out value="${result.userNm}"/>
                                                    <c:if test="${facCnt ne 1}">(부교원)</c:if>
                                                </span>
                                            </div>
                                            <c:set var="facCnt" value="${facCnt + 1}"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
		                        <td>${scheduleMngVO.campusPlace }</td>
		                        <td>
		                        	<c:choose>
		                        		<c:when test="${USER_INFO.userSeCode eq '08'}">
		                        			<c:set var="startDt" value="${fn:replace(scheduleMngVO.startDt,'-','')}"/>
											<c:choose>
												<c:when test="${today >= startDt}">
													<c:choose>
							                      		<c:when test="${not empty scheduleMngVO.attTCnt and scheduleMngVO.attTCnt == scheduleMngVO.attYCnt}">전원출석</c:when>
							                      		<c:otherwise>
							                      			<a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${scheduleMngVO.plId}" data-nm="${scheduleMngVO.studySubject}" data-crclid="${curriculumVO.crclId}" data-lcnt="${scheduleMngVO.attLCnt}" data-ncnt="${scheduleMngVO.attNCnt}" data-tot="${scheduleMngVO.attTCnt}" data-ycnt="${scheduleMngVO.attYCnt}">
								                      			<c:if test="${scheduleMngVO.attLCnt > 0}">지각 <c:out value="${scheduleMngVO.attLCnt}"/></c:if>
								                      			<c:if test="${scheduleMngVO.attLCnt > 0 and scheduleMngVO.attNCnt > 0}">,</c:if>
								                      			<c:if test="${scheduleMngVO.attNCnt > 0}">결석 <c:out value="${scheduleMngVO.attNCnt}"/></c:if>
							                      			</a>
							                      		</c:otherwise>
							                      	</c:choose>
												</c:when>
												<c:otherwise>
													대기
												</c:otherwise>
											</c:choose>
		                        		</c:when>
		                        		<c:otherwise>
		                        			<c:choose>
		                        				<c:when test="${scheduleMngVO.attYCnt > 0}">출석</c:when>
		                        				<c:when test="${scheduleMngVO.attLCnt > 0}">지각</c:when>
		                        				<c:when test="${scheduleMngVO.attNCnt > 0}">결석</c:when>
		                        			</c:choose>
		                        		</c:otherwise>
		                        	</c:choose>
		                        </td>
	                        </tr>
                        </c:forEach>
                    </c:when>
                    <c:when test="${curriculumVO.totalTime > 0 and today < curriculumStartDate}">
	                    <tr>
	                        <td colspan="9" class="alC">과정 기간이 아닙니다.</td>
	                    </tr>
	                </c:when>
                </c:choose>
                </tbody>
              </table>
			</article>

            <div class="page-btn-wrap mt-50">
              <c:url var="listUrl" value="/lms/crm/selectMyCurriculumList.do${_BASE_PARAM}"/>
              <a href="${listUrl }" class="btn-xl btn-outline-gray font-basic">목록으로</a>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  
<div id="attend_view_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">출석확인</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">[<span id="sbj"></span>]</div>
        <form name="atdFrom" id="atdFrom" onsubmit="return searchAtd();">
        	<input type="hidden" name="crclId" value="${curriculumVO.crclId }"/>
        	<input id="plId" type="hidden" name="plId" value=""/>
        	<input id="modalAt" type="hidden" name="modalAt" value="Y"/>
	        <div class="flex-row-ten">
	        	<div class="flex-ten-col-3">
	              <select name="attentionType" class="table-select select2 select2-hidden-accessible" data-select="style1" tabindex="-1" aria-hidden="true">
	                  <option value="">전체</option>
	                  <option value="Y">출석</option>
	                  <option value="NY">지각 및 결석자</option>
	                </select>
	            </div>
	            <div class="flex-ten-col-5">
	              <div class="ell">
	                <input type="text" name="userNm" value="" placeholder="이름을 입력해 주세요.">
	              </div>
	            </div>
	            <div class="flex-ten-col-2">
	              <div class="ell"><button class="btn-sm font-400 btn-point goods-search-btn" style="height:40px;">검색</button></div>
	            </div>
	        </div>
        </form>
        <div style="display:block;width:100%;height:300px;overflow-y:auto;">
	        <table class="modal-table-wrap size-sm center-align">
	          <colgroup>
	            <col width="10%">
	            <col width="30%">
	            <col width="*">
	          </colgroup>
	          <thead>
	            <tr class="bg-gray">
	              <th class="font-700">No</th>
	              <th class="font-700">상태</th>
	              <th class="font-700">이름</th>
	            </tr>
	          </thead>
	          <tbody id="atdlist"></tbody>
	        </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
        <a href="#" id="goAtd" class="btn-xl btn-point btnModalConfirm" target="_blank">출석 수정</a>
      </div>
    </div>
  </div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
	<c:param name="shareAt" value="Y"/>
	<c:param name="curriculumRequestAt" value="Y"/>
</c:import>