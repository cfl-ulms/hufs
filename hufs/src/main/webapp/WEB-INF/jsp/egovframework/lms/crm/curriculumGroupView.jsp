<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.crclbId}"><c:param name="crclbId" value="${searchVO.crclbId}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
    <c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
    <c:if test="${not empty searchVO.searchPlanSttusCode}"><c:param name="searchPlanSttusCode" value="${searchVO.searchPlanSttusCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {
	//학생 검색
    $(document).on("click", ".student_search_btn", function(){
        var userNm = $("input[name=searchUserNm]").val();
        
        $(".tr_student").hide();
        
        $(".tr_student").each(function(){
            if($(this).data("usernm").indexOf(userNm) != -1) {
                $(this).show();
            }
        });
    });
	
	//검색 초기화
	$(document).on("click", ".searchReset", function(){
		$("#searchDiv").find("input").not("#searchReset").val("");
        $("#searchDiv select").val("").trigger("change");
        $("#searchDiv input").prop("checked", false);
        $(".tr_student").show();
	});
	
	//조 클릭 활성화
	$(document).on("click", ".divGroup", function(){
		$(".divGroup").find("table").removeClass("active");
		$(this).find("table").addClass("active");
	});
	
	//조추가
	$(document).on("click", ".btn-add-list", function(){
		var html = "";
		var groupCnt = $(".divGroup").length + 1; //조 개수

		html += "<div class='divGroup'>";
		html += "   <table class='common-table-wrap mb-2'>";
		html += "       <input type='hidden' name='group_cnt' value='" + groupCnt + "' />";
		html += "       <tbody>";
		html += "           <tr>";
		html += "               <th class='w-30 font-700 group_number'>" + groupCnt + "조</th>";
		html += "               <td>";
		html += "                   <div class='flex-row'>";
		html += "                       <div class='div_memeber_cnt flex-col self-center'>-</div>";
		html += "                       <div class='flex-col-auto'>";
		html += "                           <button class='btn-delete group_delete' type='button' title='조 삭제' data-modal-type='confirm' data-modal-header='알림' data-modal-text='삭제된 조는 복구가 불가능합니다. <br>조를 삭제하시겠습니까?' data-modal-rightbtn='삭제'></button>";
		html += "                       </div>";
		html += "                   </div>";
		html += "               </td>";
		html += "           </tr>";
		html += "           <tr>";
		html += "               <td colspan='2'>";
		html += "                   <ul class='selected-items-wrap'>";
		html += "                   </ul>";
		html += "               </td>";
		html += "           </tr>";
		html += "       </tbody>";
		html += "   </table>";
		html += "</div>";
		
		$(".group-lists #listForm").append(html);
	});
	
	//학생 선택
    $(document).on("click", ".tr_student", function(){
        var el_group_cnt    = $('.divGroup').find(".active input[name='group_cnt']").val();
        var html            = "";
        var groupLeaderFlag = false;
        var groupRadioTd    = $('.divGroup').find(".active .selected-items-wrap"); //학생 입력 td 태그
        var groupSelectFlag = false;

        if(fn_group_check() == false) {
            return false;
        }

        //조 선택 확인
        $('.divGroup').each(function(){
        	if($(this).find("table").hasClass("active") == true) {
        		groupSelectFlag = true;
        	}
        });
        
        if(groupSelectFlag == false) {
            alert("조를 선택한 후에 학생을 선택해주세요.")
            return false;
        }

        if($(this).attr("data-pickflag").toString() == "true") {
            alert("이미 선택된 학생입니다.")
            return false;
        }
            
        //조 선택 결과 출력
        html += "<li class='item bg-blue-light div_student_box' data-userid='" + $(this).data("userid") + "' style='width:130px;'>";
        html += "    <input type='hidden' name='userIdList' value='" + $(this).data("userid") + "'>";
        html += "    <input type='hidden' name='groupCntList' value='" + el_group_cnt + "'>";
        html += "    <input type='hidden' name='groupLeaderAtList' value='' />";
        html += "    <label class='checkbox'><input type='checkbox' class='checkList colorChange' name='groupLeaderCheckbox' /><span class='custom-checked'></span></label>";
        html +=      $(this).data("usernm");
        html += "    <button type='button' class='btn-remove student_delete_btn' title='삭제'></button>";
        html += "</li>";

        groupRadioTd.append(html);
        
        $(this).attr("data-pickflag", "true")
        $(this).addClass("pick_complate");
        $(this).addClass("bg-gray");
        
        //조원 개수 처리
        memberCntReset(groupRadioTd);
        
        //조장이 없으면 첫번째 radio checked 처리
        groupRadioTd.find("input[name=groupLeaderCheckbox]").each(function(){
            if($(this).is(':checked') == true) {
                groupLeaderFlag = true;
                return false;
            }
        });
        
        if(groupLeaderFlag == false) {
            groupRadioTd.find("input[name=groupLeaderAtList]").first().val("Y");
            groupRadioTd.find("input[name=groupLeaderCheckbox]").first().prop('checked', true);
        }
    });
	
    //학생 삭제
    $(document).on("click", ".student_delete_btn", function(){
        var userId = $(this).parent().data("userid");
        
        $(this).parent().remove();

        $(".tr_student").each(function(){
            if($(this).data("userid") == userId) {
                $(this).removeClass("bg-gray");
                $(this).attr("data-pickflag", "false");
            }
        });
        
        //조원 개수 처리
        var groupRadioTd = $('.divGroup').find(".active .selected-items-wrap"); //학생 입력 td 태그

        memberCntReset(groupRadioTd);
    });
    
    //조삭제
    $(document).on("click", ".group_delete", function(){
    	$(this).parent().parent().parent().parent().parent().parent().parent().remove();
    	
    	//좌측 학생 선택되도록 처리
    	$(this).parent().parent().parent().parent().parent().find(".div_student_box").each(function(){
    		var userId = $(this).data("userid");

    		$(".tr_student").each(function(){
                if($(this).data("userid") == userId) {
                    $(this).removeClass("pick_complate");
                    $(this).data("pickflag", "false");
                }
            });
    	});
    	
    	//조이름 다시 처리
        var group_number = 1;

        $(".divGroup").each(function(){
            $(this).find(".group_number").text(group_number + "조");
            group_number++;
        });
    });
    
    //조장 선택 클릭 이벤트
    $(document).on("click", "input[name=groupLeaderCheckbox]", function(){
        var checkboxPrent = $(this).parent().parent().parent();
        
        //check box 초기화
        checkboxPrent.find("input[name*=groupLeaderAtList]").val("");
        checkboxPrent.find("input[name*=groupLeaderCheckbox]").prop("checked", false);

        //check box 선택
        $(this).parent().parent().find("input[name=groupLeaderAtList]").val("Y");
        $(this).prop("checked", true);
    });

    //반배정 확정
    $(document).on("click", ".class_complate_btn", function(){
        $("#listForm").submit();
    });
});

//조 수량 체크 함수
function fn_group_check() {
    var groupCnt = $(".divGroup").length;
    var flag = true;
    
    if(groupCnt =="" || groupCnt == "0") {
        alert("조 수량을 확인해 주세요.")
        flag = false;
    }
    return flag;
}

//조원 개수 처리
function memberCntReset(groupRadioTd) {
	var member_cnt = groupRadioTd.find(".div_student_box").length;

	$('.divGroup').each(function(){
	    if($(this).find("table").hasClass("active") == true) {
	    	if(member_cnt == 0) {
	    		$(this).find(".div_memeber_cnt").text("-");
	    	} else {
	    		$(this).find(".div_memeber_cnt").text(member_cnt + "명");
	    	}
	    }
	});
}
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="Y"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <!-- 두번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumrequest"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
                <c:param name="managerAt" value="${managerAt}"/>
            </c:import>
            
            <!-- 세번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="curriculumstudent"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
            </c:import>
            <%-- 담당 교수만 변경하도록 처리 --%>
            <%-- 
            <c:forEach var="user" items="${subUserList}" varStatus="status">
                <c:if test="${user.userId eq USER_INFO.id }">
                    <c:set var="updateFlag" value="true"/>
                </c:if>
            </c:forEach>
             --%>
            <c:if test="${managerAt eq 'Y'}">
			  	<c:set var="updateFlag" value="true"/>
			</c:if>
            <c:choose>
	            <c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and updateFlag eq 'true' }">
		            <article class="content-wrap">
		              <div class="board-tab-wrap">
		                <div class="slyWrap">
		                  <ul class="board-tab-lists clear">
		                    <c:forEach var="group" items="${selectGroupList}" varStatus="status">
						        <c:choose>
						            <c:when test="${group.groupCnt eq 0}"><c:set var="groupCntTab" value="조없음"/></c:when>
						            <c:otherwise><c:set var="groupCntTab" value="${group.groupCnt}개조"/></c:otherwise>
						        </c:choose>
		
						        <li class="<c:if test="${group.classCnt eq param.classCnt }">active</c:if>">
						            <a style="padding:0px;" href="/lms/crm/curriculumGroupView.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}&crclbId=${curriculumVO.crclbId}&classCnt=${group.classCnt }&menuId=${param.menuId }&subtabstep=${param.subtabstep}&thirdtabstep=2">${group.classCnt }반(${groupCntTab })</a>
						        </li>
						    </c:forEach>
		                  </ul>
		                </div>
		                <div class="scrollbar">
		                  <div class="handle"></div>
		                </div>
		              </div>
		            </article>
		            <article class="content-wrap">
		              <div class="box-wrap mb-40" id="searchDiv">
		                <h3 class="title-subhead">학생 검색</h3>
		                <div class="flex-row-ten">
		                  <div class="flex-ten-col-12">
		                    <div class="ell">
		                      <input type="text" name="searchUserNm" placeholder="이름을 검색해보세요." />
		                    </div>
		                  </div>
		                </div>
		
		                <button class="btn-sm font-400 btn-point mt-20 student_search_btn">검색</button>
		
		                <button class="btn-sm font-400 btn-outline mt-20 searchReset">초기화</button>
		              </div>
		            </article>  
		            <article class="content-wrap">
		              <div class="assigned-groups-content">
		                <div class="section-student w-30">
		                  <!-- 학생목록 -->
		                  <table class="common-table-wrap">
		                    <colgroup>
		                      <%-- <col style="width: 20%;"> --%>
		                      <col style="width: 50%;">
		                      <col style="width: 50%;">
		                    </colgroup>
		                    <thead>
		                      <tr class="font-700 bg-gray-light">
		                        <!-- <th scope="col">
		                          <label class="checkbox">
		                            <input type="checkbox" class="allCheck">
		                            <span class="custom-checked"></span>
		                          </label>
		                        </th> -->
		                        <th scope="col">소속</th>
		                        <th scope="col">이름</th>
		                      </tr>
		                    </thead>
		                    <tbody>
		                        <c:forEach var="student" items="${selectStudentList}" varStatus="status">
		                          <c:choose>
				                      <c:when test="${empty student.groupCnt  }"><c:set var="pickFlag" value="false"/></c:when>
				                      <c:otherwise><c:set var="pickFlag" value="true"/></c:otherwise>
				                  </c:choose>
			                      <tr class="tr_student <c:if test="${!empty student.groupCnt }">bg-gray</c:if>" data-depnm="${student.mngDeptNm }" data-usernm="${student.userNm }" data-userid="${student.userId }" data-pickflag="${pickFlag }">
			                        <!-- <td>
			                          <label class="checkbox">
			                            <input type="checkbox" class="checkList" />
			                            <span class="custom-checked"></span>
			                          </label>
			                        </td> -->
			                        <td>${student.mngDeptNm }</td>
			                        <td>${student.userNm }</td>
			                      </tr>
			                    </c:forEach>
		                    </tbody>
		                  </table>
		                </div>
		                <div class="section-arrow w-10">
		                </div>
		                <div class="section-group w-60">
		                  <!-- 조배정 -->
		                  <div class="group-lists">
		                      <form id="listForm" method="post" action="/lms/crm/updateCurriculumGroup.do">
		                          <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
					              <input type="hidden" name="crclbId" value="${curriculumVO.crclbId}"/>
					              <input type="hidden" name="classCnt" value="${param.classCnt}"/>
					              <input type="hidden" name="menuId" value="${param.menuId}"/>
					              <input type="hidden" name="subtabstep" value="${param.subtabstep}"/>
					              <input type="hidden" name="thirdtabstep" value="2"/>
					              
			                      <c:forEach var="groupCnt" items="${groupCntList}" varStatus="status">
				                      <div class='divGroup'>
									      <table class='common-table-wrap mb-2'>
									          <input type='hidden' name='group_cnt' value='${groupCnt.groupCnt }' />
									          <tbody>
									              <tr>
									                  <th class='w-30 font-700 group_number'>${groupCnt.groupCnt }조</th>
									                  <td>
									                      <div class='flex-row'>
									                          <c:set var="studentCnt" value="0"/>

											                  <%-- 조원 명수 계산 --%>
											                  <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
											                      <c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
											                          <c:set var="studentCnt" value="${studentCnt + 1 }"/>
											                      </c:if>
											                  </c:forEach>
									                          <div class='div_memeber_cnt flex-col self-center'>${studentCnt }명</div>
									                          <div class='flex-col-auto'>
									                              <button class='btn-delete group_delete' type='button' title='조 삭제' data-modal-type='confirm' data-modal-header='알림' data-modal-text='삭제된 조는 복구가 불가능합니다. <br>조를 삭제하시겠습니까?' data-modal-rightbtn='삭제'></button>
									                          </div>
									                      </div>
									                  </td>
									              </tr>
									              <tr>
									                  <td colspan='2'>
									                      <ul class='selected-items-wrap'>
									                          <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
										                          <c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
										                              <li class='item bg-blue-light div_student_box' data-userid='${pickStudent.userId }' style='width:130px;'>
																	      <input type='hidden' name='userIdList' value='${pickStudent.userId }'>
																	      <input type='hidden' name='groupCntList' value='${pickStudent.groupCnt }'>
																	      <input type="hidden" name="groupLeaderAtList" value="<c:if test="${pickStudent.groupLeaderAt eq 'Y'}">Y</c:if>" />
																	      <label class='checkbox'><input type='checkbox' class='checkList' name='groupLeaderCheckbox' <c:if test="${pickStudent.groupLeaderAt eq 'Y'}">checked</c:if>/><span class='custom-checked'></span></label>
																	      ${pickStudent.userNm }
																	      <button type='button' class='btn-remove student_delete_btn' title='삭제'></button>
																	  </li>
                                                                  </c:if>
									                          </c:forEach>
									                      </ul>
									                  </td>
									              </tr>
									          </tbody>
									      </table>
			                          </div>
			                      </c:forEach>
			                  </form>
		                  </div>
		                  <button class="btn-add-list">조 추가</button>
		                </div>
		              </div>
		            </article>
		        </c:when>
		        <c:otherwise>
			        <section class="page-content-body">
			            <article class="content-wrap">
				            <div class="content-body flex-column">
					            <div class="box-area mb-0">
			                        <p>조배정을 진행할 수 없는 상태입니다.</p>
			                    </div>
		                    </div>
	                    </article>
	                </section>
		        </c:otherwise>
		    </c:choose>
            <div class="page-btn-wrap mt-50">
              <c:choose>
			      <c:when test="${curriculumVO.confmSttusCode >= 2 or curriculumVO.processSttusCodeDate >= 5}">
			      </c:when>
			      <c:when test="${curriculumVO.confmSttusCode eq 0 and curriculumVO.processSttusCodeDate eq 4 and updateFlag eq 'true' }">
			          <a href="/lms/crm/selectCurseregManage.do${_BASE_PARAM }&classCnt=${group.classCnt }&subtabstep=${param.subtabstep}&thirdtabstep=2" class="btn-xl btn-outline-gray">조배정 취소</a>
			          <a href="#none" class="btn-xl btn-point class_complate_btn">조배정 확정</a>
			      </c:when>
			  </c:choose>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>