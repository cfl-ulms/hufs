<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}homework/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_JS" value="/template/common/js"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId}"/>
    <c:param name="boardType" value="${param.boardType}"/>
    <c:param name="searchCrclLang" value="${param.searchCrclLang}"/>
    <c:param name="searchCrclNm" value="${param.searchCrclNm}"/>
    <c:param name="searchHomeworkSubmitNttSj" value="${param.searchHomeworkSubmitNttSj}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
});
</script>

<div class="expansion-panel">
  <div class="page-content-wrap pt-0">
    <!-- 게시물-->
    <section class="board-view-wrap">
      <!-- 제목 -->
      <article class="board-title-wrap">
        <div class="main-common-title3">

          <h2 class="title">${homeworkSubmitVO.nttSj }</h2>
        </div>
        <div class="board-info-wrap">
          <dl class="item">
            <dt class="title">구분</dt>
            <dd class="desc">${curriculumVO.crclLangNm }</dd>
          </dl>
          <dl class="item">
            <dt class="title">과정명</dt>
            <dd class="desc">${curriculumVO.crclNm }</dd>
          </dl>
          <dl class="item">
            <dt class="title">등록일</dt>
            <dd class="desc"><fmt:formatDate value="${homeworkSubmitVO.frstRegisterPnttm }" pattern="yyyy-MM-dd"/></dd>
          </dl>
        </div>
      </article>
      <!-- 내용 -->
      <article class="board-content-wrap">
        <div class="board-editor-content">
          <!-- 에디터영역 -->
          <div class="">
            <c:out value="${homeworkSubmitVO.nttCn}" escapeXml="false" />
          </div>
        </div>
      </article>
    </section>

    <hr class="line-hr mb-20">

    <!-- 첨부파일 -->
    <c:if test="${not empty homeworkSubmitVO.atchFileId}">
        <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
            <c:param name="param_atchFileId" value="${homeworkSubmitVO.atchFileId}" />                                 
            <c:param name="imagePath" value="${_IMG }"/>
            <c:param name="commonViewAt" value="Y"/>
        </c:import>
    </c:if>

    <!-- 하단버튼 -->
    <div class="page-btn-wrap mt-20">
      <div class="left-area">
        <a href="<c:url value="/lms/crcl/selectHomeworkCommentPickAtList.do${_BASE_PARAM }"/>" class="btn-sm btn-outline-gray font-basic">목록</a>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>