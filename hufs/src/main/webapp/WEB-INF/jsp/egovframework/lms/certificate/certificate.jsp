<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<!doctype html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <title>${resultList[0].crclNm}_수료증</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            font: 12pt "Tahoma";
        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
			position:relative;
            width: 21cm;
            min-height: 29.7cm;
            padding: 2cm;
            margin: 1cm auto;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }
        .subpage {
            padding: 1cm;
            height: 256mm;
        }
        @page {
            size: A4 portrait;
            margin: 0;
            /*size: landscape;*/
        }
        @media print {
			html, body {
				width: 210mm;
				height: 297mm;
			}
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
            }
        }

		.logo{
			position:absolute;
			top:2cm;
			right:2cm;
			margin: 1em 0;
		}
		.box-sign{position:relative;}
		.box-sign img{
			position:absolute;
			top:0;
			right:136px;
		}
		.footer{
			height:103px;
			padding-top:10px;
			line-height:42px;
			/*background:url('/template/lms/imgs/page/certificate/image02.png') no-repeat 75% center;*/
			position:relative;
		}
		.footer p{
			z-index:100;
			margin:0;
		}
   </style>
</head>
<body>
	<div class="book">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<div class="page">
				<p style="text-align: left;">No. <c:out value="${result.certificateId}"/></p><img src="/template/lms/imgs/page/certificate/image01.png" class="logo"/>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<h1 style="text-align: center;"><font size="8">수 료 증</font></h1>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p style="text-align: left;"><strong>성&nbsp; &nbsp; &nbsp; &nbsp;명&nbsp;</strong><strong>: <c:out value="${result.userNm}"/></strong></p>
				<c:choose>
					<c:when test="${result.userSeCode eq '06'}">
						<p style="text-align: left;"><strong>소&nbsp; &nbsp; &nbsp; &nbsp;속&nbsp;</strong><strong>: </strong><strong><c:out value="${result.major}"/></strong><strong></strong></p>
						<p style="text-align: left;"><strong>학&nbsp; &nbsp; &nbsp; &nbsp;번&nbsp;</strong><strong>: <c:out value="${result.stNumber}"/></strong></p>
					</c:when>
					<c:otherwise>
						<p style="text-align: left;"><strong>생년월일</strong><font size="1"> </font><strong>: </strong><strong><c:out value="${fn:substring(result.brthdy, 0, 4)}년 ${fn:substring(result.brthdy, 4, 6)}월 ${fn:substring(result.brthdy, 6, 8)}일"/></strong></p>
						
					</c:otherwise>
				</c:choose>
				<p style="text-align: left;"><strong>과 <font size="2"> &nbsp;</font>정<font size="2"> &nbsp;</font>명<font size="1"> </font></strong><strong>: <c:out value="${result.crclYear}"/></strong><strong>학년도 <c:out value="${result.crclNm }"/></strong>&nbsp;</p>
				<p style="text-align: left;"><strong>수강기간<font size="1"> </font></strong><strong>: <c:out value="${result.startDate}"/> ~ <c:out value="${result.endDate}"/></strong></p>
				<c:if test="${result.userSeCode ne '06'}">
					<p>&nbsp;</p>
				</c:if>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p style="text-align: center;"><strong><font size="3">위 사람은 특수외국어교육진흥사업의 일환으로 운영된 상기 과정을 수료하였음을</font></strong></p>
				<p style="text-align: center;"><strong><font size="3">확인합니다</font></strong><strong>.</strong></p>
				<p style="text-align: center;">&nbsp;</p>
				<p style="text-align: center;">&nbsp;</p>
				<p style="text-align: center;"><font size="4"><strong><c:out value="${fn:substring(today,0,4)}"/> </strong><strong>년 </strong><strong><c:out value="${fn:substring(today,4,6)}"/> </strong><strong>월 </strong><strong><c:out value="${fn:substring(today,6,8)}"/> </strong><strong>일</strong></font></p>
				<p style="text-align: center;">&nbsp;&nbsp;</p>
				<p style="text-align: center;">&nbsp;</p>
				<p style="text-align: center;">&nbsp;</p>
				<div class="box-sign">
					<img src="/template/lms/imgs/page/certificate/image02.png" class="sign"/>
				</div>
				<div class="footer">
					<p style="text-align: center;"><strong><font size="5">한국외국어대학교</font> </strong></p>
					<p style="text-align: center;"><strong><font size="5">특수외국어교육진흥원장</font></strong></p>
				</div>
			</div>
		</c:forEach>
	</div>
<script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script>
<script>
$(document).ready(function(){
	window.print();
});
</script>
</body>
</html>

