<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${param.step}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<link rel="stylesheet" href="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.min.css?v=2">
<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script src="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js?v=1"></script>

<script>
$(document).ready(function(){
	//저장
	$("#btn-reg").click(function(){
		$("#frm").submit();
	});
});
</script>

<div class="page-content-header">
  <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
      <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
  </c:import>
  <!-- 
  <div class="util-wrap">
    <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
  </div>
   -->
</div>
<section class="page-content-body">            
            <article class="content-wrap">
                <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
                    <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                        <c:param name="step" value="${param.step }"/>
                        <c:param name="crclId" value="${curriculumVO.crclId}"/>
                        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                    </c:import>
                </c:if>
            </article>
            
            <%-- <c:if test="${not empty curriculumVO.fnTot or not empty curriculumVO.fnAttend or not empty curriculumVO.fnGradeTot or not empty curriculumVO.fnGradeFail or not empty curriculumVO.fnHomework }"> --%>
	            <article class="content-wrap">
	              <div class="content-header">
	                <div class="class-tab-wrap">
	                  <a href="/lms/manage/gradeTotal.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title">총괄평가</a>
	                 <a href="/lms/manage/completeStand.do?menuId=${searchVO.menuId}&crclId=${curriculumVO.crclId}&step=7" class="title on">수료기준</a>
	                </div>
	              </div>
	            </article>
            <%-- </c:if> --%>
            
            <article class="content-wrap">
            	<form name="frm" id="searchForm" method="post" action="<c:url value="/lms/manage/completeStand.do"/>">
					<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
					<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
					<input type="hidden" name="step" value="${searchVO.step}"/>
					
		              <div class="box-wrap mb-40">
		                <h3 class="title-subhead">학생 검색</h3>
		                <div class="flex-row-ten">
		                  <div class="flex-ten-col-3">
		                    <div class="ell">
		                      <select name="searchFinishAt" class="select2" data-select="style3" data-placeholder="수료여부">
		                      	<option value="">수료여부</option>
		                        <option value="ALL">전체</option>
								<option value="Y" <c:if test="${searchVO.searchFinishAt eq 'Y'}">selected="selected"</c:if>>수료</option>
								<option value="N" <c:if test="${searchVO.searchFinishAt eq 'N'}">selected="selected"</c:if>>미수료</option>
		                      </select>
		                    </div>
		                  </div>
		                  <div class="flex-ten-col-7">
		                    <div class="ell">
		                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" placeholder="학생 이름을 검색해보세요">
		                    </div>
		                  </div>
		                </div>
		
		                <button class="btn-sm font-400 btn-point mt-20">검색</button>
		              </div>
		        </form>
            </article>
            
            <article class="content-wrap">
              <div class="content-header">
              	전체 : <c:out value="${gradeSummary.stuCnt}"/>명 | 수료 : <c:out value="${gradeSummary.finishYCnt}"/>명  | 미수료 : <c:out value="${gradeSummary.finishNCnt}"/>명
              	<!-- 
                <div class="feedback-title-wrap">
                  <select class="select2" data-select="style3" data-placeholder="1반">
                    <option value=""></option>
                    <option value="1반">1반</option>
                  </select>
                  <a href="#" class="btn-outline btn-md-auto">수료기준 결과 다운로드</a>
                </div>
                 -->
              </div>
              <div class="tbl">
                <div class="tbl-left" data-size="tbl-sm">
                  <!-- 테이블영역-->
                  <table class="common-table-wrap p-5">
                    <colgroup>
                      <col>
                      <col>
                      <col>
                      <col>
                      <col>
                    </colgroup>
                    <thead>
                      <tr class='bg-gray font-700'>
                        <th scope='colgroup' colspan='5'>학생정보</th>
                      </tr>
                      <tr class='bg-gray font-700'>
                        <th scope='col'>소속</th>
                        <th scope='col'>생년월일</th>
                        <th scope='col'>이름</th>
                        <th scope='col'>조</th>
                        <th scope='col'>수료여부</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<c:choose>
               				<c:when test="${empty gradeSummary}">
               					<c:set var="studentList" value="${selectStudentList}"/>
               				</c:when>
               				<c:otherwise>
               					<c:set var="studentList" value="${gradeList}"/>
               				</c:otherwise>
               			</c:choose>
	                  	<c:forEach var="result" items="${studentList}" varStatus="stauts">
	                  		<tr class="">
	                        <td scope='row' class='title'>
	                          <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.mngDeptNm}"/></span></div>
	                        </td>
	                        <td><c:out value="${result.brthdy}"/></td>
	                        <td><c:out value="${result.userNm}"/></td>
	                        <td>
	                        	<c:if test="${not empty result.groupCnt}">
	                        		<c:out value="${result.groupCnt}"/>조
	                        	</c:if>
	                        </td>
	                        <td><c:out value="${result.finishAt}"/></td>
	                     </tr>
	                  	</c:forEach>
                    </tbody>
                  </table>
                </div>
                <div class="tbl-main-wrap" data-size="tbl-lg">
                  <div class="tbl-main">
                    <!-- 테이블영역-->
                    <table class="common-table-wrap p-5">
                      <thead>
                        <tr class='bg-light-gray font-700'>
                        	<c:forEach var="result" items="${finishList}">
								<c:if test="${result.ctgryLevel eq 1}">
									<c:choose>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
											<th scope='col'><c:out value="${result.ctgryNm}"/></th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
											<th scope='col'>출결</th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
											<th scope='col'><c:out value="${result.ctgryNm}"/></th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
											<th scope='col'><c:out value="${result.ctgryNm}"/></th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
											<th scope='col'><c:out value="${result.ctgryNm}"/></th>
										</c:when>
									</c:choose>
								</c:if>
							</c:forEach>
                        </tr>
                        <tr class='bg-light-gray font-700'>
                        	<c:forEach var="result" items="${finishList}">
								<c:if test="${result.ctgryLevel eq 1}">
									<c:choose>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
											<th scope='col'><c:out value="${curriculumVO.fnTot}"/>점 이상</th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
											<th scope='col'><c:out value="${curriculumVO.fnAttend}"/>% 이상</th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
											<th scope='col'><c:out value="${curriculumVO.fnGradeTot}"/>점 이상</th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
											<th scope='col'><c:out value="${curriculumVO.fnGradeFail}"/>점 이상</th>
										</c:when>
										<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
											<th scope='col'><c:out value="${curriculumVO.fnHomework}"/>점 이상</th>
										</c:when>
									</c:choose>
								</c:if>
							</c:forEach>
                        </tr>
                      </thead>
                      <tbody>
                      	<c:choose>
               				<c:when test="${empty gradeSummary}">
               					<c:set var="studentList" value="${selectStudentList}"/>
               				</c:when>
               				<c:otherwise>
               					<c:set var="studentList" value="${gradeList}"/>
               				</c:otherwise>
               			</c:choose>
                    	<c:forEach var="stResult" items="${studentList}" varStatus="stauts">
                    		<tr class="">
                    			<c:forEach var="result" items="${finishList}">
									<c:if test="${result.ctgryLevel eq 1}">
										<c:choose>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000084' and not empty curriculumVO.fnTot}">
												<th scope='col'>
													<span <c:if test="${stResult.chScr < curriculumVO.fnTot}">class='font-red'</c:if>>
														<c:out value="${fn:replace(stResult.chScr,'.0','')}"/>
													</span>
												</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000085' and not empty curriculumVO.fnAttend}">
												<th scope='col'>
													<span <c:if test="${stResult.attPer < curriculumVO.fnAttend}">class='font-red'</c:if>>
														<c:out value="${stResult.attPer}"/>
													</span>
												</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000086' and not empty curriculumVO.fnGradeTot}">
												<th scope='col'>
													<span <c:if test="${stResult.examScr < curriculumVO.fnGradeTot}">class='font-red'</c:if>>
														<c:out value="${fn:replace(stResult.examScr,'.00','')}"/>
													</span>
												</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000087' and not empty curriculumVO.fnGradeFail}">
												<th scope='col'>
													<span <c:if test="${stResult.examMin < curriculumVO.fnGradeFail}">class='font-red'</c:if>>
														<c:out value="${stResult.examMin}"/>
													</span>
												</th>
											</c:when>
											<c:when test="${result.ctgryId eq 'CTG_0000000000000088' and not empty curriculumVO.fnHomework}">
												<th scope='col'>
													<span <c:if test="${stResult.hChScr < curriculumVO.fnHomework}">class='font-red'</c:if>>
														<c:out value="${stResult.hChScr}"/>
													</span>
												</th>
											</c:when>
										</c:choose>
									</c:if>
								</c:forEach>
		                     </tr>
                    	</c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </article>
          </section>
        </div>

</div>
</div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>