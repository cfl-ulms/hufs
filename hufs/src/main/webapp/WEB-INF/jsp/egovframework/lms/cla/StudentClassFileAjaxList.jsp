<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<c:choose>
	<c:when test="${fileVo.viewType eq 'gallery' }">
		<c:forEach items="${fileList}" var="result">
   			<div class="flex-col-4">
	           <c:url var="viewUrl" value="/lms/cla/classFileView.do">
					<c:param name="atchFileId" value="${result.atchFileId}" />
				  	<c:param name="fileSn" value="${result.fileSn}" />
				  	<c:param name="plId" value="${result.plId}" />
				</c:url>
                <a href="#none" onclick="location.href='${viewUrl}'">
	             <c:choose>
               		<c:when test="${result.fileExtNm eq '이미지'}">
						<div class="board-gallery-img" style="background-image:url(${fileStorePath}/SITE_000000000000001/${result.plId }/${result.streFileNm });"></div>
               		</c:when>
               		<c:when test="${result.fileExtNm eq '동영상'}">
               			<div class="board-gallery-img file-overlay" style="background:#eeeeee"></div>
               		</c:when>
               		<c:otherwise>
               			<div class="board-gallery-img" style="background-image:url(${_L_IMG }/common/img_download_thumbnail.jpg);"></div>
               		</c:otherwise>
               	</c:choose>
	             <div class="board-gallery-contents">
	               <div class="gallery-flag">
	             <c:set var="imgSrc">
	               <c:import url="/lms/common/flag.do" charEncoding="utf-8">
	                 <c:param name="ctgryId" value="${result.crclLang}"/>
	               </c:import>
	             </c:set>
	                 <span class="flag-img">
	                 <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
	                 <span class="text">${result.ctgryNm }</span>
	               </div>
	               <p class="board-gallery-subtitle ell">${result.fileExtNm eq '이미지' ? '사진' : result.fileExtNm eq '동영상' ? '동영상' : '파일' } </p>
	               <p class="board-gallery-title ell">${result.orignlFileNm }</p>
	               <div class="board-gallery-cell">
	                 <dl class="card-cell-items">
	                   <dt class="card-cell-title">과정명</dt>
	                   <dd class="ell">${result.crclNm }</dd>
	                 </dl>
	                 <dl class="card-cell-items">
	                   <dt class="card-cell-title">수업명</dt>
	                   <dd class="ell">${result.title }</dd>
	                 </dl>
	               </div>
	               <ul class="card-items-wrap">
	                 <li class="card-items icon-name">${result.teacherNm } 교수</li>
	               </ul>
	             </div>
	           </a>
	         </div>
         </c:forEach>
	</c:when>
	<c:otherwise>
		<c:forEach items="${fileList}" var="result">
			<c:url var="viewUrl" value="/lms/cla/classFileView.do">
				<c:param name="atchFileId" value="${result.atchFileId}" />
			  	<c:param name="fileSn" value="${result.fileSn}" />
			  	<c:param name="plId" value="${result.plId}" />
			</c:url>
            <tr onclick="location.href='${viewUrl}'">
	           <td scope='row' class='font-orange'>${result.fileExtNm eq '이미지' ? '사진' : result.fileExtNm eq '동영상' ? '동영상' : '파일' } </td>

	           <td>${result.ctgryNm }</td>
	           <td>
	           <c:choose>
            		<c:when test="${result.fileExtNm eq '이미지'}">
						<div class='board-gallery-lists' style='background-image:url(${fileStorePath}/SITE_000000000000001/${result.plId }/${result.streFileNm });'></div>
            		</c:when>
            		<c:when test="${result.fileExtNm eq '동영상'}">
            			<div class='board-gallery-lists file-overlay' style='background:#eeeeee'></div>
            		</c:when>
            		<c:otherwise>
            			<div class='board-gallery-lists' style='background-image:url(${_L_IMG }/common/img_download_thumbnail_sm.jpg);'></div>
            		</c:otherwise>
                </c:choose>
	           </td>
	           <td class='title'>
	             <div class="inner-wrap"><span class='text dotdotdot'>${result.orignlFileNm }</span></div>
	           </td>
	           <td class='title'>
	             <div class="inner-wrap"><span class='text dotdotdot'>${result.crclNm }</span></div>
	           </td>
	           <td class='title'>
	             <div class="inner-wrap"><span class='text dotdotdot'>${result.title }</span></div>
	           </td>
	           <td>${result.teacherNm }</td>
	         </tr>
 		</c:forEach>
	</c:otherwise>
</c:choose>