<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizRegView.do"/>
		
<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
	<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:if test="${param.iptAt ne 'Y'}">
	<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
		<c:param name="isMainSite">Y</c:param>
		<c:param name="siteId" value="SITE_000000000000001"/>
	</c:import>
</c:if>
<link rel="stylesheet" href="/template/lms/css/quiz/staff_class.css">
<link rel="stylesheet" href="/template/lms/css/quiz/modal_staff.css">

<script>
	$(document).ready(function(){
		<c:if test="${search2.searchDiv eq 'popup'}">
			$('#before_quiz_modal').css("display", "block");
		</c:if>
		
		$('#btnReg').click(function(){
			$('#no_contents_quiz_modal').css("display", "block");
		});
		
		
		$('#btnLoad').click(function(){
			$('#before_quiz_modal').css("display", "block");
		});
		
		$('#btnSearch').click(function(){
			<c:choose>
				<c:when test="${param.iptAt eq 'Y'}">
					$("#detailForm").attr("action", "/lms/quiz/QuizList.do${_BASE_PARAM}");
				</c:when>
				<c:otherwise>
					$("#detailForm").attr("action", "/lms/quiz/QuiResultList.do${_BASE_PARAM}&quizType="+${searchVO.quizType});
				</c:otherwise>
			</c:choose>
			$("#detailForm").submit();
		});
		
	});
	
	
	//레이어 닫기
	$(document).on("click", ".btnModalClose", function(){
		var id = $(this).parents(".alert-modal").attr("id");
		if(id == "no_contents_quiz_modal"){
			$("#no_contents_quiz_modal").hide();
		}else if(id == "before_quiz_modal" || id == "btn_before_quiz_cancel"){
			$("#before_quiz_modal").hide();
		}else{	
			$(this).parents(".alert-modal").remove();	
		}
	});
	
</script>

<c:if test="${param.iptAt ne 'Y'}">
<div class="area">
	<div class="page-content-header">
		<c:choose>
			<c:when test="${not empty param.plId and param.tabType eq 'T'}">
				<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
			</c:when>
			<c:otherwise>
				<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
				</c:import>
			</c:otherwise>
		</c:choose>
	</div>
	<section class="page-content-body">
			<article class="content-wrap">
			<!-- tab style -->
			<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
				<c:param name="step" value="4"/>
				<c:param name="crclId" value="${searchVO.crclId}"/>
				<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
			</c:import>
	    </article>
	    <c:if test="${param.menuId ne 'MNU_0000000000000090'}">
			<article class="content-wrap">
				<!-- 사용자 정보 -->
				<div class="card-user-wrap">
					<div class="user-icon">
						<img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘">
					</div>
					<div class="user-info">
						<p class="title">
							담당교수 
							<b>
								<c:forEach var="result" items="${facPlList}" varStatus="status">
								   <c:out value="${result.userNm}" />
								   <c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
								   (${result.mngDeptNm})
									<c:set var="userMail" value="${result.facId }" />
							  	</c:forEach>
						  	</b>
						</p>
						<p class="sub-title">문의: ${userMail }</p>
					</div>
				</div>
			</article>
		</c:if>
		<article class="content-wrap">
			 <!-- <p class="quiz-info">* 사진을 클릭하여 퀴즈 관련 사진을 넣어주시면 퀴즈 시작 전에 커버로 학생들에게 노출됩니다.</p> -->
		     <!-- 퀴즈 -->
		     <c:forEach var="result" items="${beforeQuizList}" varStatus="status">
		     	<c:if test="${result.quizType eq searchVO.quizType }">
			         <div class="quiz-box-wrap">
			           <div class="quiz-flex-wrap">
			             <div class="quiz-flex-wrap">
			               <div class="quiz-text-wrap">
			               	<c:set var="schDt1" value="${fn:substring(result.plStartDt, '5', '7')}"/>
			               	<c:set var="schDt2" value="${fn:substring(result.plStartDt, '9', '11')}"/>
			                 <p class="date">${schDt1}월 ${schDt2}일 ${result.dayOfWeek } ${result.periodTxt }교시</p>
			                 <p class="title">[온라인퀴즈 /
			                 		<c:choose>
			                 			<c:when test="${result.quizType eq 'ON' }">문제등록형</c:when>
			  	 					<c:otherwise>답안등록형</c:otherwise>
			  	 				</c:choose>총 ${result.quizCnt}문제] ${result.studySubject} 퀴즈</p>
			                 <p class="desc">퀴즈가 완료되었으며, [평가결과조회] 버튼을 누르시면 학생의 평가결과를 확인할 수 있습니다. 답안이 잘못 체크된 경우가 있으셨다면 [답안 수정] 으로 문제의 답을 수정하여 평가결과를 다시 확인할 수 있습니다.</p>
			               </div>
			             </div>
			           </div>
			         </div>
		         </c:if>
	         </c:forEach>
		</article>
</c:if>
		
		<article class="content-wrap <c:if test="${param.iptAt eq 'Y'}">mt-20</c:if>">
			<form:form commandName="quizVO" name="detailForm" id="detailForm" method="post" action="">
			  	<%-- <input type="hidden" id="quizType" name="quizType" value="${searchVO.quizType }"/> --%>
	            <div class="box-wrap mb-40">
	              <h3 class="title-subhead">학생 검색</h3>
	              <div class="flex-row-ten">
	                <div class="flex-ten-col-10">
	                  <div class="ell">
	                    <input type="text" name="searchVal" value="${searchVO.searchVal}" placeholder="학생 이름을 검색해보세요">
	                  </div>
	                </div>
	              </div>
	
	              <button id="btnSearch" class="btn-sm font-400 btn-point mt-20">검색</button>
	            </div>
            </form:form>
		</article>
          
	    <article class="content-wrap">
              <div class="content-header">
                <div class="title-wrap">
                  <!-- <a href="#" class="btn-outline btn-md btnModalOpen" data-modal-type="excel_register">엑셀일괄등록</a>
                  <a href="#" class="btn-outline btn-md">성적다운로드</a> -->
                </div>
                <!-- <p class="notice">* 성적의 최초 등록은 엑셀일괄등록만 가능합니다.</p> -->
              </div>
              <!-- 테이블영역-->
              <table class="common-table-wrap ">
                <colgroup>
                  <col style="width:7%">
                  <col>
                  <col>
                  <col>
                  <col>
                  <col style="width:7%">
                  <col style="width:10%">
                  <col style="width:10%">
                  <%-- 
                  <col style="width:10%">
                  <col style="width:10%">
                   --%>
                </colgroup>
                <thead>
                  <tr class="font-700">
                    <th scope="col" class="bg-gray">No</th>
                    <th scope="col" class="bg-gray">소속</th>
                    <th scope="col" class="bg-gray">이름</th>
                    <th scope="col" class="bg-gray">생년월일</th>
                    <th scope="col" class="bg-gray">학번</th>
                    <th scope="col" class="bg-gray">학년</th>
                    <th scope="col" class="bg-light-gray">총문항</th>
                    <th scope="col" class="bg-light-gray">정답수</th>
                    <th scope="col" class="bg-light-gray">점수<br/>(100점 환산점수)</th>
                    <!-- 
                    <th scope="col" class="bg-light-gray">평균</th>
                    <th scope="col" class="bg-light-gray">편차</th>
                     -->
                  </tr>
                </thead>
                <tbody>
                  <c:forEach var="result" items="${quizResult}" varStatus="status">
	                  <tr class="">
	                    <td scope="row">${status.count }</td>
	                    <td>${result.major}</td>
	                    <td>${result.userNm }</td>
	                    <td>${result.brthdy }</td>
	                    <td>${result.stNumber }</td>
	                    <td>${result.stGrade }</td>
	                    <td>${result.totCnt }</td>
	                    <td>${result.myScore }</td>
	                    <c:choose>
	                    	<c:when test="${result.myScore eq 0}">
	                    		<fmt:parseNumber var="num" value="0" integerOnly="true" />
	                    	</c:when>
	                    	<c:otherwise><fmt:parseNumber var="num" value="${result.myScore/result.totCnt*100}" integerOnly="true" /></c:otherwise>
	                    </c:choose>
	                    <td>${num }</td>
	                    <%-- 
	                    <td>???</td>
	                    <td>${result.ranking }</td>
	                     --%>
	                  </tr>
                  </c:forEach>
                </tbody>
              </table>
              <div class="page-btn-wrap mt-50">
              	<c:choose>
					<c:when test="${searchVO.menuId eq 'MNU_0000000000000090'}">
						<c:url var="listUrl" value="/lms/quiz/QuizExamList.do?menuId=${searchVO.menuId}"/>
					</c:when>
					<c:otherwise>
						<c:url var="listUrl" value="/lms/quiz/QuizEvalList.do${_BASE_PARAM}"/>
					</c:otherwise>
				</c:choose>
				<c:if test="${param.iptAt ne 'Y'}">
                	<a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
                </c:if>
              </div>
            </article>

<c:if test="${param.iptAt ne 'Y'}">            
	</section>
</div>
</c:if>

<c:if test="${param.iptAt ne 'Y'}">
	<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</c:if>