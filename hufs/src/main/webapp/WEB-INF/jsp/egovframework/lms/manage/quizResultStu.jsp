<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
	<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
</c:url>
<% /*URL 정의*/ %>

<%-- 포틀릿여부 --%>
<c:if test="${param.iptAt ne 'Y'}">
	<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
		<c:param name="isMainSite">Y</c:param>
		<c:param name="siteId" value="SITE_000000000000001"/>
	</c:import>

<div class="area">
	<div class="page-content-header">
		<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
	</div>
	<section class="page-content-body">
		<article class="content-wrap">
			<!-- tab style -->
			<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
				<c:param name="step" value="4"/>
				<c:param name="crclId" value="${searchVO.crclId}"/>
				<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
			</c:import>
   		</article>
   		
		<article class="content-wrap">
			<!-- 사용자 정보 -->
			<div class="card-user-wrap">
				<div class="user-icon">
					<img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘">
				</div>
				<div class="user-info">
					<p class="title">
						담당교수 
						<b>
							<c:forEach var="result" items="${facPlList}" varStatus="status">
							   <c:out value="${result.userNm}" />
							   <c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
							   (${result.mngDeptNm})
								<c:set var="userMail" value="${result.facId }" />
						  	</c:forEach>
					  	</b>
					</p>
					<p class="sub-title">문의: ${userMail }</p>
				</div>
			</div>
		</article>
</c:if>

		<article class="content-wrap">
          <div class="content-header">
            <div class="title-wrap">
              <div class="title">전체 성적 지표</div>
            </div>
          </div>
          <div class="content-body">
            <!-- 테이블영역-->
            <table class="common-table-wrap size-sm left-align">
              <colgroup>
                <col class='bg-gray' style='width:20%'>
                <col style='width:30%'>
                <col class='bg-gray' style='width:20%'>
                <col style='width:30%'>
              </colgroup>
              <tbody>
                <tr class="">
                  <td scope='row' class='font-700'>최고점</td>
                  <td><c:out value="${fn:replace(summary.maxScr, '.00', '')}"/></td>
                  <td scope='row' class='font-700'>최저점</td>
                  <td><c:out value="${fn:replace(summary.minScr, '.00', '')}"/></td>
                </tr>
                <tr class="">
                  <td scope='row' class='font-700'>평균</td>
                  <td colspan='3'><c:out value="${fn:replace(summary.avgScr, '.00', '')}"/></td>
                </tr>
              </tbody>
            </table>
          </div>
        </article>
		
		<article class="content-wrap">
          <div class="content-header">
            <div class="title-wrap">
              <div class="title">개인 성적표</div>
            </div>
          </div>
          <div class="content-body">
            <!-- 테이블영역-->
            <table class="common-table-wrap size-sm left-align">
              <colgroup>
                <col class='bg-gray' style='width:20%'>
                <col style='width:30%'>
                <col class='bg-gray' style='width:20%'>
                <col style='width:30%'>
              </colgroup>
              <tbody>
              	<c:forEach var="result" items="${quizResult}" varStatus="status">
	                <tr class="">
	                  <td scope='row' class='font-700'>소속</td>
	                  <td>${result.major}</td>
	                  <td scope='row' class='font-700'>생년월일</td>
	                  <td>${result.brthdy }</td>
	                </tr>
	                <tr class="">
	                  <td scope='row' class='font-700'>이름</td>
	                  <td>${result.userNm }</td>
	                  <td scope='row' class='font-700'>조</td>
	                  <td>1조</td>
	                </tr>
	                <tr class="">
	                  <td scope='row' class='font-700'>정답 수 / 총 문제 수</td>
	                  <td>${result.myScore }/${result.totCnt }</td>
	                  <td scope='row' class='font-700'>100점 환산점수</td>
	                  <td>
	                  	<c:choose>
	                    	<c:when test="${result.myScore eq 0}">
	                    		<fmt:parseNumber var="num" value="0" integerOnly="true" />
	                    	</c:when>
	                    	<c:otherwise><fmt:parseNumber var="num" value="${result.myScore/result.totCnt*100}" integerOnly="true" /></c:otherwise>
	                    </c:choose>
	                    ${num}
	                  </td>
	                </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </article>
        
<c:if test="${param.iptAt ne 'Y'}">
	</section>
</div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>
</c:if>