<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:if test="${not empty searchVO.menuId}"><c:param name="menuId" value="${searchVO.menuId}" /></c:if>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<style>
.div_student_box{float:left;min-width:75px;}
</style>

<script>
$(document).ready(function() {
});
</script>
          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="Y"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0}">
                <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                    <c:param name="step" value="2"/>
                    <c:param name="crclId" value="${curriculumVO.crclId}"/>
                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                </c:import>
            </c:if>
            
            <!-- 세번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="crclstudent"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
            </c:import>
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap ">
                <colgroup>
                  <col style='width:11%'>
                  <col style='width:25%'>
                  <col style='width:190px'>
                  <col>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>분반</th>
                    <th scope='col'>담당교수</th>
                    <th scope='col'>학생 수</th>
                    <th scope='col'>배정된 학생 명단</th>
                  </tr>
                </thead>
                <tbody>
                  <c:forEach var="curriculumClass" items="${curriculumClassList}" varStatus="status">
	                  <tr class="">
	                    <td scope='row'>${curriculumClass.classCnt }반</td>
	                    <td scope='row'>
	                       <c:forEach var="fac" items="${facList}" varStatus="status">
                               ${fac.userNm}
                               <c:if test="${!status.last }">,</c:if>
                           </c:forEach>
	                    </td>
	                    <td scope='row'>
	                        <c:set var="studentTotCnt" value="0"/>
                            <c:set var="studentCntIndex" value="0"/>
                            <c:forEach var="pickStudentCnt" items="${pickStudentList}" varStatus="status2">
                                <c:if test="${pickStudentCnt.classCnt eq curriculumClass.classCnt }">
                                    <c:set var="studentTotCnt" value="${studentTotCnt + 1 }"/>
                                </c:if>
                            </c:forEach>
	                        ${studentTotCnt }명
	                    </td>
	                    <td class='keep-all'>
	                       
	                       (${studentTotCnt }명)

	                       <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status3">
                               <c:if test="${pickStudent.classCnt eq curriculumClass.classCnt }">
                                   ${pickStudent.userNm }
                                   <c:set var="studentCntIndex" value="${studentCntIndex + 1 }"/>
                                   <c:if test="${studentTotCnt ne studentCntIndex }">,</c:if>
                               </c:if>
                           </c:forEach>
	                    </td>
	                  </tr>
                  </c:forEach>
                </tbody>
              </table>
            </article>
          </section>
        </div>
      </div>
    </div>
  </div>

<div id="confirm_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text"></p>
        <p class="modal-subtext"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button type="button" class="btn-xl btn-point btnModalConfirm deleteClass">확인</button>
        <input type="hidden" id="classnum" val="" />
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>