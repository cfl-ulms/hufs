<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId}" />
	<c:param name="crclId" value="${param.crclId}" />
	<c:param name="bbsId" value="${param.bbsId}" />
	<c:if test="${not empty param.searchWrd}"><c:param name="searchWrd" value="${param.searchWrd}" /></c:if>
	<c:if test="${not empty param.searchClass}"><c:param name="searchClass" value="${param.searchClass}" /></c:if>
	<c:if test="${not empty param.searchGroup}"><c:param name="searchGroup" value="${param.searchGroup}" /></c:if>
	<c:if test="${not empty param.searchCnd}"><c:param name="searchCnd" value="${param.searchCnd}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<c:set var="processSttusCode" value="${curriculumVO.processSttusCode}"/>
<c:choose>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.planEndDate,'-','') and today < fn:replace(curriculumVO.applyStartDate,'-','')}">
		<c:set var="processSttusCode" value="2"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today >= fn:replace(curriculumVO.applyStartDate,'-','') and today <= fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="3"/>
	</c:when>
	<c:when test="${curriculumVO.processSttusCode eq '1' and today > fn:replace(curriculumVO.applyEndDate,'-','')}">
		<c:set var="processSttusCode" value="4"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>
<style>
.backGray{
	background : #ccc;
}

</style>
<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script>
$(document).ready(function(){

	/* $("#searchCtgryId, #searchCrclbId").change(function(){
		var tempCtgryId = $("select[id=searchCtgryId]").val();
		var tempCrclbId = $("select[id=searchCrclbId]").val();
		initCurriculum(tempCtgryId,tempCrclbId );
	}); */


	$("#resetBtn").on("click", function(){
		$('select').find('option:first').attr('selected', 'selected');
      	$("#searchStartDate").val("");
      	$("#searchEndDate").val("");
		$("input:checkbox").attr("checked", false);
		$("#searchKeyWord").val("");
		/* initCurriculum("",""); */
	});

	$("#searchStartDate, #searchEndDate").datepicker({
		dateFormat: "yy-mm-dd"
	 });

	$("#searchTodayClass").on("click",function(){
		if($(this).prop("checked")){
			var now = new Date();
		    var year = now.getFullYear();
	      	var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
	      	var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
	      	var chan_val = year + '-' + mon + '-' + day;
	      	$("#searchStartDate").val(chan_val);
	      	$("#searchEndDate").val(chan_val);
		}
	});
	
	//인쇄하기
	$("#btnPrint").click(function(){
		window.open("/lms/crcl/CurriculumBoardPrintStaff.do${_BASE_PARAM}&nttNo=${board.nttNo}&popupAt=Y&printAt=Y","popup","top=1,left=1,width=900,height=" + screen.availHeight);
	});
});

function fn_egov_delete_notice(url) {
	if (confirm('<spring:message code="common.delete.msg" />')) {
		document.location.href = url;
	}
}

function fnMove(url){
	if(confirm("수정 하시겠습니까?")){
		document.location.href = url;
	}
}



</script>
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
			<!-- 
            <div class="util-wrap">
              <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
            </div>
             -->
          </div>
          <section class="page-content-body">
          	<c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="6"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
				</c:import>
			</c:if>


			<article class="content-wrap">
              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
	                  <c:forEach var="result" items="${masterList}">
						<c:url var="masterUrl" value="/lms/crcl/curriculumBoardList.do">
							<c:param name="crclId" value="${searchVO.crclId}"/>
							<c:param name="bbsId" value="${result.bbsId}" />
							<c:param name="menuId" value="${param.menuId}" />
						</c:url>
						<li <c:if test="${result.bbsId eq brdMstrVO.bbsId}">class="active"</c:if>>
							<a href="${masterUrl}" class="link">
								<c:choose>
									<c:when test="${result.sysTyCode eq 'CLASS'}">(반별)&nbsp;</c:when>
									<c:when test="${result.sysTyCode eq 'GROUP'}">(조별)&nbsp;</c:when>
								</c:choose>
								<c:out value="${result.bbsNm}"/>
							</a>
						</li>
					</c:forEach>
                </ul>
                </div>
                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
            </article>


            <article class="content-wrap">
              <!-- 게시물-->
              <section class="board-view-wrap">
                <!-- 제목 -->
                <article class="board-title-wrap">
                  <div class="main-common-title3">

                    <h2 class="title">${board.nttSj}</h2>
                  </div>
                  <div class="board-info-wrap">
                    <dl class="item">
                      <dt class="title">작성자</dt>
                      <dd class="desc">${board.ntcrNm}</dd>
                    </dl>
                    <dl class="item">
                      <dt class="title">구분</dt>
                      <dd class="desc">${board.ctgryNm}</dd>
                    </dl>
                    <dl class="item">
                      <dt class="title">등록일</dt>
                      <dd class="desc"><fmt:formatDate value="${board.frstRegisterPnttm}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
                    </dl>
                  </div>
                </article>
                <!-- 내용 -->
                <article class="board-content-wrap">
                  <div class="board-editor-content">
                    <!-- 에디터영역 -->
                    <div class="froala-read-only">
                     <c:out value="${board.nttCn}" escapeXml="false" />
                    </div>
                  </div>
                </article>
              </section>
              <hr class="line-hr mb-20">
              <!-- 첨부파일 -->

              <c:if test="${brdMstrVO.fileAtchPosblAt eq 'Y'}">
				<c:if test="${not empty board.atchFileId}">
					<c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
						<c:param name="param_atchFileId" value="${board.atchFileId}" />
						<c:param name="viewDivision" value="staff" />
					</c:import>
				</c:if>
			</c:if>
              <!-- 하단버튼 -->
              <div class="page-btn-wrap mt-20">
                <div class="left-area">
                <c:url var="listUrl" value="/lms/crcl/curriculumBoardList.do${_BASE_PARAM}"/>
<%--                 <c:url var="printUrl" value="/lms/crcl/curriculumBoardView.do${_BASE_PARAM}">
				  	<c:param name="nttNo" value="${board.nttNo}" />
				  	<c:param name="popupAt" value="Y" />
				  	<c:param name="printAt" value="Y" />
                </c:url> --%>
                  <a href="${listUrl}" class="btn-sm btn-outline-gray font-basic">목록</a>
                  <a id="btnPrint" class="btn-sm btn-outline-gray font-basic">인쇄</a>
                </div>
                <div class="right-area">
                	 <fmt:parseNumber var="tempSeCode" type="number" value="${board.userSeCode}"/>
                	<c:if test="${tempSeCode lt 10 or board.frstRegisterId eq sessionUniqId }">

	                	<c:url var="deleteBoardArticleUrl" value="${_PREFIX}/deleteBoardStaffArticle.do${_BASE_PARAM}">
				      		<c:param name="nttNo" value="${board.nttNo}" />
						</c:url>

	                  <button class="btn-sm btn-outline-gray font-basic" onclick="fn_egov_delete_notice('${deleteBoardArticleUrl}');return false;">삭제</button>
	                </c:if>
                  <c:if test="${board.frstRegisterId eq sessionUniqId}">
                  	<c:url var="forUpdateBoardArticleUrl" value="${_PREFIX}/forUpdateBoardStaffArticle.do${_BASE_PARAM}">
			      		<c:param name="nttNo" value="${board.nttNo}" />
				  		<c:param name="registAction" value="updt" />
					</c:url>

                  	<button class="btn-sm btn-outline-gray font-basic" onclick="fnMove('${forUpdateBoardArticleUrl}')">수정</button>
                  </c:if>
                </div>
              </div>
            </article>
          </section>
        </div>

</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>