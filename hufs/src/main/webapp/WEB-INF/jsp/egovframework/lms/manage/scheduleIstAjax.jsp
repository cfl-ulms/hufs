<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

					<td scope='row'>
	                      	<div class="edit_mode" style="display:none;">
								<input type="number" name="sisu" class="wid50p onlyNum sisu" value="${scheduleMngVO.sisu}"/>
								<input type="hidden" name="crclId" value="<c:out value="${curriculumVO.crclId}"/>"/>
								<input type="hidden" name="plId" value="<c:out value="${scheduleMngVO.plId}"/>"/>
							</div>
							<div class="view_mode"><c:out value="${scheduleMngVO.sisu}"/></div>
	                      </td>
	                      <td class='left-align'>
	                      	<div class="edit_mode" style="display:none;">
								<input type="text" name="startDt" class="btn_calendar edit_mode" value="${scheduleMngVO.startDt}" autocomplete="off" readonly="readonly"/>
							</div>
							<div class="view_mode"><c:out value="${scheduleMngVO.startDt}"/></div>
	                      </td>
	                      <td class='left-align'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="edit_mode" style="display:none;">
										<select class="table-select select2 startTimeHH required" name="startTimeHH" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="6" end="24" step="1">
												<c:set var="hour">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
											</c:forEach>
										</select>
										:
										<select class="table-select select2 startTimeMM required" name="startTimeMM" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="0" end="55" step="5">
												<c:set var="minute">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div class="edit_mode" style='display:none;'>
				                      	<select class="table-select select2 startTime required" name="startTime" data-select='style1' data-placeholder='시작시간'>
											<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.startTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.startTime eq result.startTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.startTime,0,2)}"/>:<c:out value="${fn:substring(result.startTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</c:otherwise>
							</c:choose>
							<div class="view_mode">
								<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
							</div>
	                      </td>
	                      <td class='left-align'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="edit_mode" style="display:none;">
										<select class="table-select select2 endTimeHH required" name="endTimeHH" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="6" end="24" step="1">
												<c:set var="hour">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
											</c:forEach>
										</select>
										:
										<select class="table-select select2 endTimeMM required" name="endTimeMM" data-select='style1' data-placeholder='선택'>
											<option value="">선택</option>
											<c:forEach var="result" begin="0" end="55" step="5">
												<c:set var="minute">
													<c:choose>
														<c:when test="${result < 10}">0${result}</c:when>
														<c:otherwise>${result}</c:otherwise>
													</c:choose>
												</c:set>
												<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								<c:otherwise>
									<div class="edit_mode" style='display:none;'>
				                      	<select class="table-select select2 endTime required" name="endTime" data-select='style1' data-placeholder='종료시간'>
				                      		<option value="">선택</option>
											<c:forEach var="result" items="${camSchList}">
												<option value="${result.endTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.endTime eq result.endTime}">selected="selected"</c:if>>
													<c:out value="${result.period}"/>교시
													<c:out value="${fn:substring(result.endTime,0,2)}"/>:<c:out value="${fn:substring(result.endTime,2,4)}"/>
												</option>
											</c:forEach>
										</select>
									</div>
								</c:otherwise>
							</c:choose>
							<div class="view_mode">
								<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
							</div>
	                      </td>
	                      <td class='left-align box_period'>
	                      	<c:choose>
								<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
									<div class="view_mode">
										<c:out value="${scheduleMngVO.periodTxt}"/>
									</div>
									<input type='text' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>' class="edit_mode" style="display:none;"/>
								</c:when>
								<c:otherwise>
									<c:out value="${scheduleMngVO.periodTxt}"/>
									<input type='hidden' name='periodTxt' value='<c:out value="${scheduleMngVO.periodTxt}"/>'/>
								</c:otherwise>
							</c:choose>
	                      </td>
	                      <td class='left-align'>
	                      	<div class='flex-row edit_mode' style='display:none;'>
	                          <div class='flex-col-12' >
		                          	<select class='table-select select2 search fac required' data-select='style1' data-placeholder='교원검색'>
			                             <option value="">선택</option>
			                             <c:forEach var="result" items="${facList}">
											<option value="${result.facId}" data-nm="${result.userNm}"><c:out value="${result.userNm}"/></option>
										 </c:forEach>
		                            </select>
	                            </div>
	                            <div class='flex-col-12'>
		                            <ul class='selected-items-wrap row-full box_fac'>
		                            	<c:set var="facCnt" value="1"/>
		                            	<c:forEach var="result" items="${facPlList}" varStatus="status">
											<c:if test="${result.plId eq scheduleMngVO.plId}">
				                              <li class='item bg-blue-light'>
				                              		<span><c:out value="${result.userNm}"/><c:if test="${facCnt ne 1}">(부교원)</c:if></span> 
				                              		<input type='hidden' name='facIdList' value='${result.facId}'/> 
				                              		<button type='button' class='btn-remove btn_delfac edit_mode' style="display:none;">삭제</button>
				                              </li>
				                              <c:set var="facCnt" value="${facCnt + 1}"/>
				                              </c:if>
			                              </c:forEach>
		                            </ul>
		                          </div>
	                        </div>
	                        <div class="view_mode">
	                        	<input type='hidden' name='changeFacAt' value='Y'/>
								<ul>
                            	<c:set var="facCnt" value="1"/>
                            	<c:forEach var="result" items="${facPlList}" varStatus="status">
									<c:if test="${result.plId eq scheduleMngVO.plId}">
										<li>
											<span>
												<c:out value="${result.userNm}"/>
												<c:if test="${facCnt ne 1}">(부교원)</c:if>
											</span> 
										</li>
										<c:set var="facCnt" value="${facCnt + 1}"/>
									</c:if>
								</c:forEach>
                            </ul>
						    </div>
	                      </td>
	                      <td class='left-align'>
	                      	<textarea class="wid80 edit_mode" name="studySubject" style="display:none;"><c:out value="${scheduleMngVO.studySubject}"/></textarea>
							<div class="view_mode"><c:out value="${scheduleMngVO.studySubject}"/></div>
	                      </td>
	                      <td class='left-align'><!-- 출석 완료 후 작업 --></td>
	                      <td class='left-align'>
								<div class="box_btn">
									<button class='btn-sm-full btn-outline-gray btn_edit' type='button'>수정</button>
									<button class='btn-sm-full btn-outline-gray mt-5 btn_del' data-id='${scheduleMngVO.plId}' data-crclid='${curriculumVO.crclId}' type='button'>삭제</button>
								</div>
								<div class="box_save" style="display:none;">
									<button class='btn-sm-full btn-outline btn_save' data-id='${scheduleMngVO.plId}' type='button'>저장</button>
								</div>
	                      </td>
