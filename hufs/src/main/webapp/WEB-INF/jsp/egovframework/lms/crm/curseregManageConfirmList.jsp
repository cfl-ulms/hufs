<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyStartDate}"><c:param name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchApplyEndDate}"><c:param name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchHostCode" value="${searchVO.searchHostCode}" /></c:if>
    <c:if test="${not empty searchVO.searchStudentUserNm}"><c:param name="searchStudentUserNm" value="${searchVO.searchStudentUserNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStudentHostCode}"><c:param name="searchStudentHostCode" value="${searchVO.searchStudentHostCode}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
//수강 신청 이후 return 메세지 출력
<c:if test="${!empty requestMessage }">
    alert("${requestMessage}");
</c:if>

$(document).ready(function() {
	//검색 초기화
    $(document).on("click", "#searchReset", function() {
        $("form[name='frm']").find("input").not("#resetBtn").val("");
        $("form[name='frm'] select").val("").trigger("change");
        $("form[name='frm'] input").prop("checked", false);
    });
});

</script>
        <section class="page-content-body">
          <form name="frm" method="post" action="<c:url value="/lms/crm/curseregManageConfirmList.do"/>">
            <input type="hidden" name="menuId" value="${param.menuId }" />

            <article class="content-wrap mb-40">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">수강신청 승인/취소 내역 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                        <select class="select2" name="searchCrclYear" id="searchCrclYear" data-select="style3">
                            <option value="">년도</option>
                            <c:forEach var="result" items="${yearList}" varStatus="status">
                                <option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
                            </c:forEach>
                        </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select class="select2" data-select="style3" id="searchCrclTerm" name="searchCrclTerm">
                        <option value="">학기</option>
                        <c:forEach var="result" items="${crclTermList}" varStatus="status">
                            <c:if test="${result.ctgryLevel eq '1'}">
                                <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
                            </c:if>
                        </c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-4 mb-20">
                    <div class="desc">
                      <input type="text" name="searchApplyStartDate" value="${searchVO.searchApplyStartDate}" id="searchApplyStartDate" class="ell date datepicker type2" placeholder="신청 시작일" readonly="readonly"/>
                      <i>~</i>
                      <input type="text" name="searchApplyEndDate" value="${searchVO.searchApplyEndDate}" id="searchApplyEndDate" class="ell date datepicker type2" placeholder="신청 종료일" readonly="readonly"/>
                    </div>
                  </div>
                  <div class="flex-ten-col-10 mb-20">
                    <div class="ell">
                      <input name="searchCrclNm" type="text" placeholder="찾으시는 과정명을 입력해보세요" value="${searchVO.searchCrclNm }">
                    </div>
                  </div>
                  <%-- 
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                      <select class="select2" name="searchCrclLang" id="searchCrclLang" data-select="style3">
                        <option value="">언어 전체</option>
                        <c:forEach var="result" items="${languageList}" varStatus="status">
                            <c:if test="${not empty result.upperCtgryId}">
                                <option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
                            </c:if>
                        </c:forEach>
                      </select>
                    </div>
                  </div>
                   --%>
                  <%-- <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                      <select class="select2" name="searchHostCode" id="searchHostCode" data-select="style3">
                        <option value="">주관기관 전체</option>
                        <c:forEach var="result" items="${insList}" varStatus="status">
                            <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchHostCode}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
                        </c:forEach>
                      </select>
                    </div>
                  </div> --%>
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <input name="searchStudentHostCode" value="${searchVO.searchStudentHostCode }" type="text" placeholder="학생 소속">
                    </div>
                  </div>
                  <div class="flex-ten-col-7">
                    <div class="ell">
                      <input type="text" name="searchStudentUserNm" placeholder="학생이름" value="${searchVO.searchStudentUserNm }">
                    </div>
                  </div>
                </div>
            
                <button class="btn-sm font-400 btn-point mt-20">검색</button>
                
                <a href="#none" class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</a>
              </div>
            </article>
          </form>
          <article class="content-wrap">
            <div class="content-header">
              <div class="btn-group-wrap">
                <div class="left-area">
                  <%-- TODO : ywkim 엑셀 다운로드 처리해야함 --%>
                  <!-- <a href="#" class="btn-outline btn-md">엑셀 다운로드</a> -->
                </div>
              </div>
            </div>
            <div class="content-body">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:20%'>
                  <col style='width:90px'>
                  <col style='width:90px'>
                  <col style='width:90px'>
                  <col style='width:100px'>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:6%'>
                  <col style='width:6%'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>년도</th>
                    <th scope='col'>학기</th>
                    <th scope='col'>현황</th>
                    <th scope='col'>과정명</th>
                    <th scope='col'>언어</th>
                    <th scope='col'>주관기관/<br>학과</th>
                    <th scope='col'>책임교원</th>
                    <th scope='col'>수강신청<br>기간</th>
                    <th scope='col'>수업료</th>
                    <th scope='col'>등록비</th>
                    <th scope='col'>신청<br>승인</th>
                    <th scope='col'>취소<br>환불</th>
                  </tr>
                </thead>
                <tbody>
                  <c:forEach var="result" items="${resultList}" varStatus="status">
                      <c:url var="viewUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=5">
                          <c:param name="crclId" value="${result.crclId}"/>
                          <c:param name="crclbId" value="${result.crclbId}"/>
                          <c:param name="tabstep" value="1"/>
                          <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                      </c:url>
                      <tr onclick="location.href='${viewUrl}'">
                       <td scope='row'><c:out value="${result.crclYear}"/></td>
                       <td><c:out value="${result.crclTermNm}"/></td>
                       <td>
                          <c:choose>
                            <c:when test="${result.processSttusCodeDate eq '1' }">대기</c:when>
                            <c:when test="${result.processSttusCodeDate eq '3' }">접수중</c:when>
                            <c:when test="${result.processSttusCodeDate eq '4' }">종료</c:when>
                            <c:when test="${result.processSttusCodeDate eq '5' }">과정개설취소</c:when>
                            <c:when test="${result.processSttusCodeDate eq '6' }">수강대상자 확정</c:when>
                            <c:when test="${result.processSttusCodeDate eq '7' }">과정 중</c:when>
                            <c:when test="${result.processSttusCodeDate eq '8' }">과정종료</c:when>
                            <c:when test="${result.processSttusCodeDate eq '9' }">과정종료(성적산정)</c:when>
                            <c:when test="${result.processSttusCodeDate eq '10' }">과정종료(성적발표)</c:when>
                            <c:when test="${result.processSttusCodeDate eq '11' }">성적발표 종료</c:when>
                            <c:when test="${result.processSttusCodeDate eq '12' }">최종종료(확정)</c:when>
                        </c:choose>
                          </td>
                       <td class='left-align'><c:out value="${result.crclNm}"/></td>
                       <td><c:out value="${result.crclLangNm}"/></td>
                       <td><c:out value="${result.hostCodeNm}"/></td>
                       <td><c:out value="${result.userNm}"/></td>
                       <td><c:out value="${result.applyStartDate}"/> ~ <br/> <c:out value="${result.applyEndDate}"/></td>
                       <td>
                           <c:choose>
                            <c:when test="${result.tuitionFees eq 0 }">무료</c:when>
                            <c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.tuitionFees}" />원</c:otherwise>
                        </c:choose>
                       </td>
                       <td>
                           <c:choose>
                            <c:when test="${result.registrationFees eq 0 }">없음</c:when>
                            <c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.registrationFees}" />원</c:otherwise>
                        </c:choose>
                       </td>
                       <td><c:out value="${result.acceptStudentCnt}"/></td>
                          <td><c:out value="${result.cancleStudentCnt}"/></td>
                   </tr>
                  </c:forEach>
                </tbody>
              </table>
              <div class="pagination center-align mt-60">
                <div class="pagination-inner-wrap overflow-hidden inline-block">
                    <c:url var="startUrl" value="/lms/crm/curseregManageConfirmList.do${_BASE_PARAM}">
                        <c:param name="pageIndex" value="1" />
                    </c:url>
                    <button class="start goPage" data-url="${startUrl}"></button>
                   
                    <c:url var="prevUrl" value="/lms/crm/curseregManageConfirmList.do${_BASE_PARAM}">
                        <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
                    </c:url>
                    <button class="prev goPage" data-url="${prevUrl}"></button>
                   
                    <ul class="paginate-list f-l overflow-hidden">
                        <c:url var="pageUrl" value="/lms/crm/curseregManageConfirmList.do${_BASE_PARAM}"/>
                        <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
                        <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
                    </ul>
                   
                    <c:url var="nextUrl" value="/lms/crm/curseregManageConfirmList.do${_BASE_PARAM}">
                        <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
                    </c:url>
                    <button class="next goPage" data-url="${nextUrl}"></button>
                   
                    <c:url var="endUrl" value="/lms/crm/curseregManageConfirmList.do${_BASE_PARAM}">
                        <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
                    </c:url>
                    <button class="end goPage" data-url="${endUrl}"></button>
                </div>
            </div>
            </div>
          </article>
        </section>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
</c:import>