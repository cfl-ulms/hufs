<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="crclId" value="${param.crclId}"/>
</c:url>
<% /*URL 정의*/ %>


<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>


<script>
$(document).ready(function(){
	$(".chartDiv").each(function (idx, el) {
	var totalCnt = 0;
	var myChart = echarts.init(el);

	var tempArr = $(this).closest(".queDiv").find("tr.answerClass");
	var dataMapArr = $.makeArray(tempArr.map(function(){
		if($(this).data("excnt") != 0){
	 		var tempObject = new Object();
	 		tempObject.value = $(this).data("excnt");
	  	 	tempObject.name = $(this).data("excn");
	  	 	totalCnt += $(this).data("excnt");
	  	 	return tempObject;
		}
	}));

	var dataArr = $.makeArray(tempArr.map(function(){
	 	return  $(this).data("excn");
	}));


	var option = {
			 color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		      animation: false,
		      legend: {
		        orient: 'vertical',
		        right: 15,
		        top: 30,
		        data: dataArr,
		        icon: 'rect',
		        itemGap: 20,
		        textStyle: {
		          fontSize: 13,
		          verticalAlign: 'middle',
		        },
		      },
		      series: [
		        {
	        		color: ['#396fd4', '#199bfc', '#54c374', '#facc32', '#ddd'],
		          	type: 'pie',
		          	radius: '85%',
		          	label:false,
		          	padding : '20px',
		          	center: ['35%', '50%'],
		          	hoverAnimation: false,
		          	animation: false,
		          	data: dataMapArr
		        }
		      ]
		    };

	myChart.setOption(option);
	$(this).closest(".queDiv").find(".totalCnt").html("응답 (<span class=\'font-point\'>"+totalCnt+"</span>명)");
	});
});
</script>
<style>
canvas{
	padding : 20px;
}
</style>

	<div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
          </div>
          <section class="page-content-body">
			<article class="content-wrap">
              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="10"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
            </article>
            <article class="content-wrap">
              <!-- 교육과정 개요 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">교육과정 개요</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>년도</td>
                      <td>${curriculumVO.crclYear }년</td>
                      <td scope='row' class='font-700'>학기</td>
                      <td>${curriculumVO.crclTermNm}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>언어</td>
                      <td>${curriculumVO.crclLangNm}</td>
                      <td scope='row' class='font-700'>학점인정 여부</td>
                      <td>${curriculumVO.gradeNum}</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정기간</td>
                      <td>${curriculumVO.startDate} ~ ${curriculumVO.endDate}</td>
                      <td scope='row' class='font-700'>과정시간</td>
                      <td>총 ${curriculumVO.totalTime}시간 / 주 ${curriculumVO.weekNum}회 / 일 ${curriculumVO.dayTime}시간</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정진행캠퍼스</td>
                      <td colspan='3'>${curriculumVO.campusNm} ${curriculumVO.campusPlace}호</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>책임교수</td>
                      <td colspan='3'>${curriculumVO.userNm} (${curriculumVO.hostCodeNm})</td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정 개요 및 목표</td>
                      <td colspan='3'><c:out value="${fn:replace(curriculumVO.crclGoal, LF, '<br>')}" escapeXml="false"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>과정만족도 진행률</td>
                      	<fmt:formatNumber value="${curriculumAddInfo.joinCnt}" type="number" var="joinCnt" />
						<fmt:formatNumber value="${curriculumAddInfo.memberCnt}" type="number" var="memberCnt" />
						<td colspan='3'>
							${ joinCnt} / ${memberCnt}
							<c:if test="${curriculumAddInfo.memberCnt ne '0' and joinCnt lt memberCnt}">
								(${memberCnt -  joinCnt}명 미제출)
							</c:if>
                     </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 과정만족도 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">${not empty param.plId ? '수업만족도' : '과정만족도'}</div>
                </div>
              </div>
              <div class="content-body">
                <div class="no-content-wrap">
                	<c:if test="${curriculumAddInfo.sum ne 0 }">
	                	<%-- <fmt:formatNumber var="resultPoint"  pattern="0" value=" ${(curriculumAddInfo.sum/ curriculumAddInfo.joinCnt)*100} " /> --%>
	                	<fmt:formatNumber var="resultPoint"  pattern="0" value="${curriculumAddInfo.sum}"/>
                	</c:if>
                  	<b class="satisfy">${not empty param.plId ? '수업만족도' : '과정만족도'} : ${curriculumAddInfo.sum eq 0 ? 0 : resultPoint}</b>
                </div>

                <c:if test="${not empty surveyAnswer }">
					<c:forEach items="${surveyAnswer}" var="result" varStatus="status">

		                <div class="satisfy-table-wrap queDiv">
		                <div class="satisfy-head">${status.count }. ${result.qesitmSj }</div>

						<c:if test="${result.qesitmTyCode eq 'multiple'}">
		                  <div class="satisfy-img-box">
		                    <div class="chartDiv" style="display:inline-block;width:100%;height: 300px;border-style: none;"></div>
		                  </div>
		                 </c:if>
		                  <!-- 테이블영역-->

		                  <c:choose>
							<c:when test="${result.qesitmTyCode eq 'multiple'}">
								<table class="common-table-wrap size-sm">
				                    <colgroup>
				                      <col style='width:33.333%'>
				                      <col style='width:33.333%'>
				                      <col style='width:33.333%'>
				                    </colgroup>
				                    <thead>
				                      <tr class='bg-gray font-700'>
				                        <th scope='col'>점수</th>
				                        <th scope='col'>항목</th>
				                        <th scope='col' class="totalCnt"></th>
				                      </tr>
				                    </thead>
				                    <tbody>
				                    	<c:forEach items="${result.answerList}" var="answer"  varStatus="aStatus">
					                      <tr class="answerClass" data-excn="${answer.exCn }" data-excnt="${answer.cnt }">
					                        <td scope='row'>${fn:length(result.answerList) - aStatus.index }</td>
					                        <td>${answer.exCn }</td>
											<td>${answer.cnt }</td>
					                      </tr>
				                     	</c:forEach>
				                    </tbody>
				                  </table>
							</c:when>
							<c:when test="${result.qesitmTyCode eq 'answer'}">
								 <table class="common-table-wrap size-sm">
				                    <colgroup>
				                      <col style='width:10%'>
				                      <col style='width:18%'>
				                      <col style='width:18%'>
				                      <col>
				                    </colgroup>
				                    <thead>
				                      <tr class='bg-gray font-700'>
				                        <th scope='col'>번호</th>
				                        <th scope='col'>응답자</th>
				                        <th scope='col'>전공</th>
				                        <th scope='col'>응답 내용</th>
				                      </tr>
				                    </thead>
				                    <tbody>
				                    <c:forEach items="${result.essayList}" var="essay"  varStatus="eStatus">
				                      <tr>
				                        <td scope='row'>${eStatus.count}</td>
				                        <td>${essay.userNm }</td>
										<td>${essay.crclNm }</td>
										<td class='left-align'>${essay.cnsr }</td>
				                      </tr>
			                     	</c:forEach>
				                    </tbody>
				                  </table>
							</c:when>
						</c:choose>
		                </div>
		               </c:forEach>
		               </c:if>
              </div>
            </article>
          </section>