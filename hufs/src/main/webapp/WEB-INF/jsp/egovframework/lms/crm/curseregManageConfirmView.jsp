<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.crclbId}"><c:param name="crclbId" value="${searchVO.crclbId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {

});
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
	        <article class="content-wrap">
	          <!-- tab style -->
	          <ul class="tab-wrap sub-tab-style">
	            <li class="tab-list <c:if test="${param.tabstep eq 1 }">on</c:if>"><a href="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM }&tabstep=1&searchSttus=5">신청(납부)자</a></li>
	            <li class="tab-list <c:if test="${param.tabstep eq 2 }">on</c:if>"><a href="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM }&tabstep=2&searchSttus=6">취소, 환불자</a></li>
	          </ul>
	        </article>
            <article class="content-wrap">
              <div class="box-wrap mb-40">
                <form name="frm" method="post" action="<c:url value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM }"/>">
                    <input type="hidden" name="tabstep" value="${param.tabstep }" />

	                <h3 class="title-subhead">수강대상자 검색</h3>
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-2">
	                    <div class="ell">
	                      <input name="searchHostCode" value="${searchVO.searchHostCode }" type="text" placeholder="소속">
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-2">
	                    <div class="ell">
	                      <select name="searchSttus" id="searchSttus" class="select2" data-select="style3">
	                        <option value="">승인상태</option>
	                        <option value="1" <c:if test="${'1' eq searchVO.searchSttus}">selected="selected"</c:if>>승인</option>
			                <option value="2" <c:if test="${'2' eq searchVO.searchSttus}">selected="selected"</c:if>>대기</option>
			                <option value="3" <c:if test="${'3' eq searchVO.searchSttus}">selected="selected"</c:if>>취소</option>
			                <option value="4" <c:if test="${'4' eq searchVO.searchSttus}">selected="selected"</c:if>>환불</option>
	                      </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <input type="text" name="searchBrthdy" value="${searchVO.searchBrthdy }" class="onlyNum" placeholder="생년월일 8자리" />
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm }" class="" placeholder="학생 이름" />
	                    </div>
	                  </div>
	                </div>
	
	                <button class="btn-sm font-400 btn-point mt-20">검색</button>
	              </div>
	          </form>
            </article>
            <article class="content-wrap">
              <%-- 
              <div class="content-header">
                <div class="btn-group-wrap">
                  <div class="left-area">
                    <a href="/lms/crm/curriculumAccept.do?menuId=MNU_0000000000000070&crclId=${searchVO.crclId}&crclbId=${searchVO.crclbId}&subtabstep=3" class="btn-outline-gray btn-md-auto">수강신청 메뉴에서 승인상태 변경/조회</a>
                  </div>
                  <div class="right-area">
                    TODO : ywkim 처리해야 함
                    <a href="#" class="btn-outline btn-md">엑셀 다운로드</a>
                    <a href="#" class="btn-outline btn-md">인쇄</a>
                  </div>
                </div>
              </div>
               --%>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col style='width:10%'>
                    <col>
                    <col style='width:10%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col'>소속</th>
                      <th scope='col'>이름</th>
                      <th scope='col'>생년월일</th>
                      <th scope='col'>학번</th>
                      <th scope='col'>학년</th>
                      <th scope='col'>수업료</th>
                      <th scope='col'>등록비</th>
                      <th scope='col'>수강신청접수일</th>
                      <th scope='col'>승인상태</th>
                    </tr>
                  </thead>
                  <tbody>
                      <c:forEach var="member" items="${curriculumMemberList}" varStatus="status">
	                      <tr class="">
	                          <td scope='row'>${totCnt - status.index }</td>
	                          <td>${member.mngDeptNm }</td>
	                          <td>${member.userNm }</td>
	                          <td>${member.brthdy }</td>
	                          <td>${member.stNumber }</td>
	                          <td>${member.stGrade }</td>
	                          <td>
	                              <c:choose>
		                              <c:when test="${curriculumVO.tuitionFees ne 0}">
		                                  <fmt:formatNumber type="number" maxFractionDigits="3" value="${member.tuitionFees}" />원
		                              </c:when>
		                              <c:otherwise>무료</c:otherwise>
		                          </c:choose>
	                          </td>
	                          <td>
	                              <c:choose>
                                      <c:when test="${curriculumVO.registrationFees ne 0}">
                                          <fmt:formatNumber type="number" maxFractionDigits="3" value="${member.registrationFees}" />원
                                      </c:when>
                                      <c:otherwise>없음</c:otherwise>
                                  </c:choose>
	                          </td>
	                          <td>${member.frstRegisterPnttm }</td>
	                          <td>
                                  <c:choose>
			                          <c:when test="${member.sttus eq 1}">승인</c:when>
			                          <c:when test="${member.sttus eq 2}">대기</c:when>
			                          <c:when test="${member.sttus eq 3}">취소</c:when>
			                          <c:when test="${member.sttus eq 4}">환불</c:when>
			                      </c:choose>
                              </td>
	                      </tr>
                      </c:forEach>
                      <c:if test="${fn:length(curriculumMemberList) == 0}">
                        <tr>
                            <td class="listCenter" colspan="10"><spring:message code="common.nodata.msg" /></td>
                        </tr>
                    </c:if>
                  </tbody>
                </table>
                <div class="pagination center-align mt-60">
                  <div class="pagination-inner-wrap overflow-hidden inline-block">
                      <c:url var="startUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=${param.searchSttus }">
                          <c:param name="pageIndex" value="1" />
                      </c:url>
                      <button class="start" data-url="${startUrl}"></button>
                     
                      <c:url var="prevUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=${param.searchSttus }">
                          <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
                      </c:url>
                      <button class="prev" data-url="${prevUrl}"></button>
                     
                      <ul class="paginate-list f-l overflow-hidden">
                          <c:url var="pageUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=${param.searchSttus }"/>
                          <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
                          <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
                      </ul>
                     
                      <c:url var="nextUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=${param.searchSttus }">
                          <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
                      </c:url>
                      <button class="next" data-url="${nextUrl}"></button>
                     
                      <c:url var="endUrl" value="/lms/crm/curseregManageConfirmView.do${_BASE_PARAM}&tabstep=1&searchSttus=${param.searchSttus }">
                          <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
                      </c:url>
                      <button class="end" data-url="${endUrl}"></button>
                  </div>
                </div>
              </div>  
            </article>
          </section>
        </div>
      </div>
    </div>
  </div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>