<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${param.menuId }"/>	
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8"/>

        <section class="page-content-body">
        	<div class="box-wrap mb-40">
	            <h3 class="title-subhead">수료/성적 검색</h3>
	            <form name="frm" method="post" action="<c:url value="/lms/grade/gradeList.do"/>">
	            	<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
		            <div class="flex-row-ten">
		              <div class="flex-ten-col-2">
		                <div class="ell">
		                	<select id="searchCrclYear" name="searchCrclYear" class="select2" data-select="style3" data-placeholder="년도">
	                    		<option value="">선택</option>
								<c:forEach var="result" items="${yearList}" varStatus="status">
					  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
					  			</c:forEach>
							</select>
		                </div>
		              </div>
		              <div class="flex-ten-col-2">
		                <div class="ell">
		                  <select id="searchCrclTerm" name="searchCrclTerm" class="select2" data-select="style3" data-placeholder="학기">
								<option value="">학기 전체</option>
								<c:forEach var="result" items="${crclTermList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
		                </div>
		              </div>
		              <div class="flex-ten-col-2">
		                <div class="ell">
		                	<select class="select2" name="searchCrclLang" id="searchCrclLang" data-select="style3">
		                        <option value="">언어 전체</option>
		                        <c:forEach var="result" items="${languageList}" varStatus="status">
				            		<c:if test="${not empty result.upperCtgryId}">
		                                <option value="${result.ctgryId}" <c:if test="${searchVO.searchCrclLang eq result.ctgryId}">selected</c:if>>${result.ctgryNm}</option>
				              	    </c:if>
				  			    </c:forEach>
		                    </select>
		                </div>
		              </div>
		              <div class="flex-ten-col-4">
		                <div class="ell">
		                  <input name="searchCrclNm" type="text" placeholder="과정명을 입력해보세요." value="${searchVO.searchCrclNm }">
		                </div>
		              </div>
		            </div>
		
		            <button class="btn-sm font-400 btn-point mt-20">검색</button>
		        </form>
	          </div>
	          
	          <!-- 테이블영역-->
	          <table class="common-table-wrap table-type-board">
	            <colgroup>
	              <col style='width:7%'>
	              <col style='width:12%'>
	              <col style='width:12%'>
	              <col style='width:12%'>
	              <col>
	              <col style='width:130px'>
	            </colgroup>
	            <thead>
	              <tr class='bg-light-gray font-700'>
	                <th scope='col'>No</th>
	                <th scope='col'>년도</th>
	                <th scope='col'>학기</th>
	                <th scope='col'>언어</th>
	                <th scope='col'>과정명</th>
	                <th scope='col'>과정기간</th>
	              </tr>
	            </thead>
	            <tbody>
	            	<c:forEach var="result" items="${resultList}" varStatus="status">
			    		<c:url var="viewUrl" value="/lms/grade/selectGrade.do${_BASE_PARAM}">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'">
			    			<td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			                <td><c:out value="${result.crclYear}"/></td>
			    			<td><c:out value="${result.crclTermNm}"/></td>
			                <td><c:out value="${result.crclLangNm}"/></td>
			                <td class='left-align'><c:out value="${result.crclNm}"/></td>
			                <td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
			    		</tr>
			    	</c:forEach>
			    	<c:if test="${fn:length(resultList) == 0}">
						<tr>
				        	<td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
				      	</tr>
				    </c:if>
	            </tbody>
	          </table>
	          <div class="pagination center-align mt-60">
	            <div class="pagination-inner-wrap overflow-hidden inline-block">
	              	<c:url var="startUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start goPage" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev goPage" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next goPage" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="/lms/grade/gradeList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end goPage" data-url="${endUrl}"></button>
	            </div>
	          </div>
          
        </section>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
	<c:param name="modalAt" value="Y"/>
</c:import>