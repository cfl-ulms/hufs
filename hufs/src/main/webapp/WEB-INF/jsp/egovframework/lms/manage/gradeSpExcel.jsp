<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<%
	String areaId = request.getParameter("areaId");
	String filenameKo = "gradeSample";
	
	String browser = request.getHeader("User-Agent");
	String filename="";
	if (browser.indexOf("MSIE") > -1 || browser.indexOf("Trident") > -1) {
		filename = java.net.URLEncoder.encode(filenameKo, "UTF-8");
	}else{
		filename = new String(filenameKo.getBytes("utf-8"),"ISO8859_1");
	}
	response.setHeader("Content-Disposition", "attachment; filename="+filename+".xls");
	response.setHeader("Content-Description", "JSP Generated Data");
%>
<title>성적 엑셀 샘플</title>
<style type="text/css"> 
<!--
body {font-size:10px;margin:0px;font-family:돋움;}
table {border-collapse: collapse; width:900px; padding:10px;margin-bottom:20px;}
table th {background:#edf3fa;border-top:1px solid #5d99d2;border-bottom:1px solid #b6cce2; font-weight:normal;color:#5e8dbf;font-weight:bold; height:23px;text-align:center;border-right:1px solid #dfdfdf;}
table th.caption {padding-top:10px;width:100%;height:40px;font-size:20px;color:#5e8dbf;font-weight:bold;background:#fff;border:0;}
table td {background:#ffffff;border-bottom:1px solid #dfdfdf; font-size:10px; height:18px;border-right:1px solid #dfdfdf;padding-left:5px;text-align:center;mso-number-format:\@;}
table td.alignL{text-align:left;padding-left:15px;}

#info th{width:100px;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;}
#info td{width:auto;text-align:left;padding-left:10px;color:#f00;}
-->
</style>
</head>
<body>
	<table class="chart_board">
		<thead>
			<tr id="info">
				<td colspan="4">※ 파일 저장 시 다른이름으로 저장 선택 후 파일형식을 xls형식(97-2003)으로 변경 후 저장하고 업로드 하세요.</td>
			</tr>
			<tr>
				<th rowspan="2">이름</th>
				<th rowspan="2">아이디</th>
				<c:forEach var="result" items="${evaluationList}" varStatus="status">
					<%-- 출석, 과제, 수업참여도는 관리하는 제외  --%>
					<c:if test="${result.evtId ne 'CTG_0000000000000117' and result.evtId ne 'CTG_0000000000000109' and result.evtId ne 'CTG_0000000000000115'}">
						<th colspan="${result.evtId eq 'CTG_0000000000000119' ? 1 : 2}">
							<c:out value="${result.evtNm}"/>
							(<c:out value="${result.evtVal}"/>%)
						</th>
					</c:if>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach var="result" items="${evaluationList}" varStatus="status">
					<%-- 출석, 과제, 수업참여도는 관리하는 제외  --%>
					<c:if test="${result.evtId ne 'CTG_0000000000000117' and result.evtId ne 'CTG_0000000000000109' and result.evtId ne 'CTG_0000000000000115'}">
						<th class="border">변환점수</th>
						<c:if test="${result.evtId ne 'CTG_0000000000000119'}">
							<th class="border">총점</th>
						</c:if>
					</c:if>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="result" items="${selectStudentList}" varStatus="status">
				<tr>
					<td class="border"><c:out value="${result.userNm}"/></td>
					<td class="border"><c:out value="${result.userId}"/></td>
					<c:forEach var="result" items="${evaluationList}" varStatus="status">
						<%-- 출석, 과제, 수업참여도는 관리하는 제외  --%>
						<c:if test="${result.evtId ne 'CTG_0000000000000117' and result.evtId ne 'CTG_0000000000000109' and result.evtId ne 'CTG_0000000000000115'}">
							<td class="border"></td>
							<c:if test="${result.evtId ne 'CTG_0000000000000119'}">
								<td class="border"></td>
							</c:if>
						</c:if>
					</c:forEach>
				</tr>
			</c:forEach>
	    </tbody>
	</table>
</body>
</html>