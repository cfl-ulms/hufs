<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/mng/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="/template/manage/css/default.css">
<link rel="stylesheet" href="/template/manage/css/page.css">
<link rel="stylesheet" href="/template/manage/css/com.css">
<link href="/template/common/js/jquery/themes/base/jquery.ui.all.css" rel="stylesheet">
<script src="/template/common/js/jquery/jquery-1.9.1.min.js"></script>
<title>총괄평가 기준</title>
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var id = $(".chk:checked").val(),
			name = $(".chk:checked").data("nm"),
			stand = $(".chk:checked").parents("tr").find(".sel_stand").val();
		
		window.opener.selEvt(id, name, stand);
		window.close();
		return false;
	});
	
	
	$(".btn_cancel").click(function(){
		window.close();
	});
});
</script>
</head>
<body>
<br/>
<div id="cntnts">
<form name="listForm" id="listForm" method="post" method="post" action="">
	<table class="chart_board">
		<colgroup>
			<col width="10%"/>
			<col width="*"/>
			<col width="40%"/>
		</colgroup>
		<thead>
			<tr>
				<th>선택</th>
				<th>게시판 명</th>
				<th>기준</th>
		  	</tr> 
		</thead>
		<tbody class="box_bbs">
			<c:forEach var="result" items="${evaluationBaseList}" varStatus="status">
				<c:if test="${result.ctgryLevel eq '2'}">
					<tr class="list">
						<td>
							<input type="radio" class="chk" name="ctgryId" value="${result.ctgryId}" data-nm="${result.ctgryNm}"/>
						</td>
						<td>
							<c:out value="${result.ctgryNm}"/>
							<c:if test="${not empty result.ctgryCn}"><br/>(<c:out value="${result.ctgryCn}"/>)</c:if>
						</td>
						<td>
							<c:choose>
								<c:when test="${not empty result.ctgryVal}">
									<c:out value="${result.ctgryVal}"/>
									<input type="hidden" class="sel_stand" value="점수"/>
								</c:when>
								<c:otherwise>
									<select class="sel_stand">
										<option value="1">점수</option>
										<option value="2">횟수</option>
									</select>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	
    <div class="btn_c">
		<a href="#" class="btn_ok btn_mngTxt">선택</a>&nbsp;
		<a href="#" class="btn_cancel btn_mngTxt">취소</a>
	</div>
</form>
</div>