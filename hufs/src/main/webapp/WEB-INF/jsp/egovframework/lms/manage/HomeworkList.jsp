<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${searchVO.step}"/>
    <c:param name="plId" value="${searchVO.plId}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
});
</script>

<div class="page-content-header">
<c:choose>
    <c:when test="${not empty param.plId and param.tabType eq 'T'}">
        <c:import url="/lms/claHeader.do" charEncoding="utf-8">
        </c:import>
    </c:when>
    <c:otherwise>
        <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
            <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
        </c:import>
    </c:otherwise>
</c:choose>
</div>
<section class="page-content-body">
            <article class="content-wrap">
                <!-- tab style -->
                <%-- 과정, 수업 분기처리 --%>
                <c:choose>
                    <c:when test="${!empty param.plId }">
                        <c:if test="${curriculumVO.processSttusCode > 0}">
		                    <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
		                        <c:param name="step" value="${param.step }"/>
		                        <c:param name="crclId" value="${curriculumVO.crclId}"/>
		                        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
		                        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		                    </c:import>
		                </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
		                    <c:param name="menu" value="${param.menu }"/>
		                    <c:param name="tabstep" value="${param.tabstep }"/>
		                    <c:param name="menuId" value="${param.menuId }"/>
		                    <c:param name="crclId" value="${param.crclId}"/>
		                    <c:param name="crclbId" value="${param.crclbId}"/>
		                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt }"/>
		                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		                </c:import>
                    </c:otherwise>
                </c:choose>
            </article>
            <%-- <article class="content-wrap">
              <!-- 사용자 정보 -->
              <div class="card-user-wrap">
                <div class="user-icon"><img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘"></div>
                <div class="user-info">
                  <p class="title">담당교수 <b><c:forEach var="result" items="${facPlList}" varStatus="status">
                        <c:out value="${result.userNm}"/>
                        <c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
                        (${result.mngDeptNm})
                        <c:set var="userMail" value="${result.facId }"/>
                  </c:forEach></b></p>

                  <p class="sub-title">문의: ${userMail }</p>
                </div>
              </div>
            </article> --%>
            <article class="content-wrap">
              <c:if test="${USER_INFO.userSeCode eq '08' }">
              
                  <%-- 과정 시작일 이후에 과제가 등록 되도록 처리하기 위한 날짜 --%>
                  <fmt:parseDate var="startDate_D"  value="${curriculumVO.startDate }" pattern="yyyy-MM-dd"/>
				  <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="nowDateHyphen3"/>
                  <fmt:parseDate value="${nowDateHyphen3}" pattern="yyyy-MM-dd" var="now_D" />
				
				  <fmt:parseNumber var="startDate_N" value="${startDate_D.time / (1000*60*60*24)}" integerOnly="true" />
				  <fmt:parseNumber var="now_N" value="${now_D.time / (1000*60*60*24)}" integerOnly="true" />
                  <div class="content-header">
                    <div class="title-wrap">
                      <p class="title">등록한 과제</p>
                      <c:choose>
                      	<c:when test="${(managerAt eq 'Y' or studyMngAt eq 'Y') and startDate_N <= now_N and !empty param.plId}">
                      	
                      	</c:when>
                      	<c:otherwise>
                      		(과정이 시작되면 과제를 등록할 수 있습니다.)
                      	</c:otherwise>
                      </c:choose>
                    </div>
                    <div class="btn-group-wrap">
                      <div class="right-area">

                        <c:if test="${(managerAt eq 'Y' or studyMngAt eq 'Y') and startDate_N <= now_N and !empty param.plId}">
                            <a href="/lms/crcl/homeworkRegister.do?registAction=regist&mode=lesson&menuId=${param.menuId }&crclId=${param.crclId }&step=${param.step }&plId=${param.plId }&boardType=list&tabType=T" class="btn-md btn-point">과제등록</a>
                        </c:if>
                      </div>
                    </div>
                  </div>
              </c:if>
              <div class="content-body">
                <div class="flex-row">
                  <!-- card style -->
                  <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                      <%-- 날짜 비교를 위한 데이터 컨버팅 --%>
                      <c:set var="openDate" value="${fn:replace(result.openDate, '-', '')}${result.openTime }"/>
                      <fmt:parseDate value="${openDate}" var="openParseDate" pattern="yyyyMMddHHmm"/>
                      <fmt:formatDate value="${openParseDate}" pattern="yyyyMMddHHmm" var="openDateForm"/>

                      <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
                      <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                      <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateForm"/>

                      <%-- 날짜 비교 계산 --%>
                      <fmt:parseDate value="${fn:substring(result.closeDate,0,10)}" pattern="yyyy-MM-dd" var="closeDateHyphen" />
                      <fmt:parseNumber value="${closeDateHyphen.time / (1000*60*60*24)}" integerOnly="true" var="closeDateTime" />

                      <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="nowDateHyphen"/>
                      <fmt:parseDate value="${nowDateHyphen}" pattern="yyyy-MM-dd" var="closeDateHyphen2" />
                      <fmt:parseNumber value="${closeDateHyphen2.time / (1000*60*60*24)}" integerOnly="true" var="nowDateTime" />

                      <c:if test="${openDateForm <= nowDate or USER_INFO.userSeCode eq '08' }">
	                      <c:choose>
		                      <c:when test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06' }">
		                          <%-- 과제 등록, 조회 여부를 위한 분기 처리 --%>
		                          <c:choose>
	                                  <c:when test="${result.stuOpenAt eq 'Y'}"><c:set var="registAction" value="view"/></c:when>
	                                  <c:when test="${result.stuOpenAt ne 'Y' and empty result.hwsId}"><c:set var="registAction" value="regist"/></c:when>
	                                  <c:when test="${result.stuOpenAt ne 'Y' and !empty result.hwsId}"><c:set var="registAction" value="updt"/></c:when>
	                              </c:choose>
	                              
	                              <%-- 개별, 조별 과제에 따른 plId 분기처리 --%>
	                              <%-- <c:choose>
                                      <c:when test="${result.hwCode eq '2'}"><c:set var="plId" value="${searchVO.plId }"/></c:when>
                                      <c:otherwise><c:set var="plId" value=""/></c:otherwise>
                                  </c:choose> --%>
                                  
			                      <c:url var="viewUrl" value="/lms/manage/selectHomeworkSubmitArticle.do">
			                          <c:param name="menuId" value="${searchVO.menuId}"/>
								      <c:param name="crclId" value="${searchVO.crclId}"/>
								      <c:param name="step" value="${searchVO.step}"/>
								      <c:param name="plId" value="${searchVO.plId}"/>
			                          <c:param name="hwId" value="${result.hwId }" />
			                          <c:param name="hwsId" value="${result.hwsId }" />
			                          <c:param name="tabType" value="T" />
			                          <c:param name="registAction" value="${registAction }" />
			                      </c:url>
			                  </c:when>
			                  <c:otherwise>
			                      <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
	                                  <c:param name="boardType" value="list" />
	                                  <c:param name="hwId" value="${result.hwId }" />
	                                  <c:param name="tabType" value="T" />
	                              </c:url>
			                  </c:otherwise>
	                      </c:choose>
		                <div class="flex-col-6 card-type4 relative">
		                    <a href="${viewUrl}" class="card-wrap">
		                      <div class="card-head">
		                        <div class="card-title-wrap">                              
	                              <c:set var="diffDate" value="${closeDateTime - nowDateTime }"/>
		                          
		                          <c:choose>
		                              <c:when test="${diffDate eq 0 }">
	                                      <c:set var="dateText" value="오늘마감"/>
	                                  </c:when>
		                              <c:when test="${closeDateTime > nowDateTime }">
		                                  <c:set var="dateText" value="D-${diffDate }"/>
		                              </c:when>
		                              <c:otherwise>
		                                  <c:set var="dateText" value="제출마감"/>
		                              </c:otherwise>
		                          </c:choose>
		                          
		                          <span class="card-category ">${dateText }</span>
		                          
		                          <c:choose>
		                              <c:when test="${result.hwCode eq '1'}"><c:set var="hwCodeFontColor" value="font-red"/></c:when>
		                              <c:when test="${result.hwCode eq '2'}"><c:set var="hwCodeFontColor" value="font-blue"/></c:when>
		                          </c:choose>
		                          <div class="card-title ${hwCodeFontColor }">${result.hwCodeNm}(${fn:substring(result.hwTypeNm,0,2) })</div>
		                        </div>
		                      </div>
		                      <div class="card-body">
		                        <div class="card-desc2 dotdotdot" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:350px;">${result.nttSj }</div>
		
		                        <ul class="card-items-wrap">
		                          <li class="card-items icon-name" style="height:18px;">
	                                  <c:set var="teacherCnt" value="0"/>
	
		                              <c:choose>
		                                  <%-- 책임 교수 --%>
		                                  <c:when test="${result.hwCode eq '1' }">
		                                      <c:forEach var="user" items="${subUserList}" varStatus="status">
		                                          <c:if test="${0 eq status.index }">
				                                      ${user.userNm} 교수
				                                  </c:if>
				                                  <c:if test="${1 < status.index }">
			                                          <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
			                                      </c:if>
			                                  </c:forEach>
		                                  </c:when>
		                                  <%-- 수업 담당 교수 --%>
		                                  <c:when test="${result.hwCode eq '2' }">
		                                      <c:forEach var="user" items="${facPlList}" varStatus="status">
		                                          <c:if test="${0 eq status.index }">
		                                              ${user.userNm} 교수
		                                          </c:if>
		                                          <c:if test="${1 < status.index }">
	                                                  <c:set var="teacherCnt" value="${teacherCnt + 1 }"/>
	                                              </c:if>
		                                      </c:forEach>
		                                  </c:when>
		                              </c:choose>
	
		                              <c:if test="${0 < teacherCnt }"> 외${teacherCnt }명</c:if>
		                          </li>
		                          <li class="card-items icon-date">제출 기간 : ${result.openDate} ${fn:substring(result.openTime,0,2) }:${fn:substring(result.openTime,2,4) } ~ ${result.closeDate} ${fn:substring(result.closeTime,0,2) }:${fn:substring(result.closeTime,2,4) }</li>
		                          <li class="card-items icon-info" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:350px;height:18px;">
		                              <c:out value='${result.nttCn.replaceAll("\\\<.*?\\\>","")}' escapeXml="false"/>
		                          </li>
		                        </ul>
		                        <c:if test="${USER_INFO.userSeCode eq '02' or USER_INFO.userSeCode eq '04' or USER_INFO.userSeCode eq '06' }">
			                        <div class="card-footer">
				                        <div class="class-comment-wrap">
				                        	<c:if test="${result.stuOpenAt eq 'Y'}">
					                            <div class="class-comment" style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:180px;height:19px;">
		                                            <c:out value='${result.fdb.replaceAll("\\\<.*?\\\>","")}' escapeXml="false"/>
		                                        </div>
	                                        </c:if>
				                            <c:choose>
		                                        <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000091'}">
		                                            <c:choose>
		                                                <c:when test="${result.stuOpenAt eq 'Y' and empty result.scr}"><span class="class-score">FAIL</span></c:when>
		                                                <c:when test="${result.stuOpenAt eq 'Y' and result.scr eq '0'}"><span class="class-score">FAIL</span></c:when>
		                                                <c:when test="${result.stuOpenAt eq 'Y' and result.scr eq '100'}"><span class="class-score font-blue">PASS</span></c:when>
		                                            </c:choose>
		                                        </c:when>
		                                        <c:when test="${curriculumVO.gradeType eq 'CTG_0000000000000090' or curriculumVO.evaluationAt eq 'N'}">
		                                            <c:choose>
		                                                <c:when test="${result.stuOpenAt eq 'Y' and empty result.scr}"><span class="class-score">0점</span></c:when>
		                                                <c:when test="${result.stuOpenAt eq 'Y'}"><span class="class-score font-blue">${result.scr }점</span></c:when>
		                                            </c:choose>
		                                        </c:when>
		                                    </c:choose>
				                        </div>
				                        <c:choose>
	                                        <c:when test="${result.stuOpenAt eq 'Y' or curriculumVO.processSttusCodeDate > 9}"><button class="flex-none btn-lg btn-light-gray">제출마감</button></c:when>
	                                        <c:when test="${result.stuOpenAt ne 'Y' and empty result.hwsId}"><button class="flex-none btn-lg btn-point">과제 제출하기</button></c:when>
	                                        <c:when test="${result.stuOpenAt ne 'Y' and !empty result.hwsId}"><button class="flex-none btn-lg btn-dark-gray">과제조회</button></c:when>
	                                    </c:choose>
				                    </div>
				                </c:if>
		                      </div>
		                    </a>
	                    </div>
	                  </c:if>
                  </c:forEach>
                </div>
              </div>
            </article>
          </section>
        </div>

</div>
</div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>