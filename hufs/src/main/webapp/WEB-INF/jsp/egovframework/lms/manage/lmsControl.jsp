<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<% pageContext.setAttribute("LF", "\n"); %>

<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="MNU_0000000000000062"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<%-- 과정 진행 단계 --%>
<c:set var="processSttusCode" value="${curriculumVO.processSttusCodeDate}"/>
<c:choose>
	<c:when test="${empty curriculumVO.processSttusCode}">
		<c:set var="processSttusCode" value="0"/>
	</c:when>
</c:choose>
<c:forEach var="result" items="${statusComCode}">
	<c:if test="${result.code eq processSttusCode}">
		<c:set var="codeNm" value="${result.codeNm}"/>
		<c:set var="codeDc" value="${result.codeDc}"/>
	</c:if>
</c:forEach>

<link type="text/css" href="<c:url value='/template/common/js/jquery/css/jquery.timepicker.css'/>" rel="stylesheet" />
<script type="text/javascript" src="<c:url value='/template/common/js/jquery/jquery.timepicker.min.js'/>"></script>

<script>
//달력
$(function() {
  $(".btn_calendar").datepicker({
    dateFormat: "yy-mm-dd"
  });
  
	//timepicker
	$("#resultStartTime, #resultEndTime, #surveyStartTime").timepicker({
		step: 1,          //시간간격 : 5분
		timeFormat: "H:i" //시간:분 으로표시
	});
	
	$("#btn-open-dialog,#dialog-background,#btn-close-dialog").click(function () {
		$("#my-dialog,#dialog-background").toggle();
	});
});

$(document).ready(function(){
	//과정확정취소
	$(".btn_cancel").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정을 확정을 취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//과정폐기
	$(".btn_del").click(function(){
		var href = $(this).attr("href");
		
		if(confirm("<c:out value="${curriculumVO.crclNm }"/> 과정개설취소 하시겠습니까?")){
			return;
		}else{
			return false;
		}
		
		return false;
	});
	
	//수강신청기간 수정
	$(".btn_apl").click(function(){
		var href = $(this).attr("href"),
			applyStartDate = $("#applyStartDate").val(),
			applyEndDate = $("#applyEndDate").val(),
			type = $(this).data("type"),
			btn = $(this);
		
		if(type != 1){
			$.ajax({
				url : href
				, type : "post"
				//, dataType : "json"
				, data : {applyStartDate : applyStartDate, applyEndDate : applyEndDate}
				, success : function(data){
					console.log(data);
					if(data.successYn == "Y"){
						$(".box_apl_text").text(applyStartDate + " ~ " + applyEndDate);
						$(".box_apl_inp").hide();
						$(".box_apl_text").show();
						
						btn.data("type", "1");
						btn.text("기간 수정");
					}
				},  error:function(request,status,error){
				    alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);

				}
			});
		}else{
			$(".box_apl_inp").show();
			$(".box_apl_text").hide();
			$(this).data("type", "0");
			$(this).text("기간 등록");
		}
		return false;
	});
	
	//성적발표
	$(".btn_result").click(function(){
		var href = $(this).data("href"),
			resultStartDate = $("#resultStartDate").val(),
			resultStartTime = $("#resultStartTime").val(),
			resultEndDate = $("#resultEndDate").val(),
			resultEndTime = $("#resultEndTime").val(),
			type = $(this).data("type");
		
		if(type != 1){
			$.ajax({
				url : href
				, type : "post"
				, dataType : "json"
				, data : {resultStartDate : resultStartDate, resultStartTime : resultStartTime, resultEndDate : resultEndDate, resultEndTime : resultEndTime}
				, success : function(data){
					if(data.successYn == "Y"){
						$(".box_result_text").find(".table-data").text(resultStartDate + " " + resultStartTime + " ~ " + resultEndDate + " " + resultEndTime);
						$(".box_result_inp").hide();
						$(".box_result_text").show();
					}
				}, error : function(){
					alert("error");
				}
			});
		}else{
			$(".box_result_inp").show();
			$(".box_result_text").hide();
			//$(this).data("type", "0");
			//$(this).text("기간 등록");
		}
		return false;
	});
	
	//만족도 설문제출
	$(".btn_survey").click(function(){
		var href = $(this).data("href"),
			surveyStartDate = $("#surveyStartDate").val(),
			surveyStartTime = $("#surveyStartTime").val(),
			type = $(this).data("type");
		
		
		if(type != 1){
			if(surveyStartDate && surveyStartTime){
				$.ajax({
					url : href
					, type : "post"
					, dataType : "json"
					, data : {surveyStartDate : surveyStartDate, surveyStartTime : surveyStartTime}
					, success : function(data){
						if(data.successYn == "Y"){
							$(".box_survey_text").find(".table-data").text(surveyStartDate + " " + surveyStartTime + " ~ 과정상태가 과정종료(성적발표)까지 제출가능");
							$(".box_survey_inp").hide();
							$(".box_survey_text").show();
						}
					}, error : function(){
						alert("error");
					}
				});
			}else{
				alert("시작일 및 시간을 확인해주세요.");
			}
		}else{
			$(".box_survey_inp").show();
			$(".box_survey_text").hide();
		}
		return false;
	});
	
	//과정상태 선택
	$("#processCode").on('change', function(){
		var expHtml = "",
			codeNm = $('#processCode option:selected').attr("data-codeNm"),
			codeDc = $('#processCode option:selected').attr("data-codeDc");
		
		expHtml = "<strong>[ " + codeNm + " ]</strong>"
				+ "<br/>"
				+ codeDc;
		
		$(".box_exp").html(expHtml);
	});
	
	//과정상태 변경
	$(".btn_change").click(function(){
		$("#prcsForm").submit();
	});
});
</script>

          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
				<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
			<!-- TO DO
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="1"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
            <article class="content-wrap">
              <!-- 과정게시판 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정게시판 (${totCnt})</div>
                  <p class="desc-sm">해당 과정에 필요한 만큼 과정게시판을 생성할 수 있습니다.</p>
                </div>
                <div class="btn-group-wrap">
                  <div class="right-area">
                  	<c:url var="bbsUrl" value="/lms/manage/lmsBbsControl.do">
                  		<c:param name="menuId" value="MNU_0000000000000062" />
						<c:param name="crclId" value="${curriculumVO.crclId}" />
					</c:url>
                    <a href="${bbsUrl}" class="btn-md btn-point">게시판 수정</a>
                  </div>
                </div>
              </div>
              <hr class="line-hr mb-20">
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap ">
                  <colgroup>
                    <col style='width:7%'>
                    <col style='width:15%'>
                    <col style='width:15%'>
                    <col>
                    <col style='width:15%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700 keep-all'>
                      <th scope='col'>No</th>
                      <th scope='col'>사용자 그룹</th>
                      <th scope='col'>게시글 구분 태그</th>
                      <th scope='col'>게시판 명</th>
                      <th scope='col'>등록일</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<c:forEach var="result" items="${bbsMasterList}" varStatus="status">
	                    <tr class="">
	                      <td scope='row'><c:out value="${status.count}"/></td>
	                      <td>
	                      	<c:choose>
								<c:when test="${result.sysTyCode eq 'ALL'}">전체</c:when>
								<c:when test="${result.sysTyCode eq 'INDIV'}">개별</c:when>
								<c:when test="${result.sysTyCode eq 'CLASS'}">반별</c:when>
								<c:when test="${result.sysTyCode eq 'GROUP'}">조별</c:when>
							</c:choose>
	                      </td>
	                      <td>
								<select class='table-select select2' data-select='style1'>
									<option>전체</option>
									<c:forEach var="ctgry" items="${ctgryList}" varStatus="sts">
										<c:if test="${ctgry.ctgrymasterId eq result.ctgrymasterId and not empty ctgry.upperCtgryId}">
											<option value=""><c:out value="${ctgry.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
							</td>
							<td><c:out value="${result.bbsNm}"/></td>
							<td class='keep-all'><c:out value="${result.frstRegisterPnttm}"/></td>
	                    </tr>
                    </c:forEach>
                  </tbody>
                </table>
              </div>
            </article>

            <article class="content-wrap">
              <!-- 수강신청기간 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수강신청기간</div>
                </div>
              </div>
              <div class="content-body">
                <table class="common-table-wrap table-style2 ">
                  <tbody>
                    <tr>
                      <th class="title">기간 수정 및 등록 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col">
                          	<div class="flex-row flex-nowrap box_apl_inp" style="display:none;">
								<c:choose>
									<c:when test="${processSttusCode < 4}">
										<div class="flex-col-3">
			                            	<input type="text" id="applyStartDate" class="ell date datepicker" value="${curriculumVO.applyStartDate}" placeholder="시작일" readonly="readonly">
			                            </div>
			                            	~
			                            <div class="flex-col-3">
			                            	<input type="text" id="applyEndDate" class="ell date datepicker" value="${curriculumVO.applyEndDate}" placeholder="종료일" readonly="readonly">
			                            </div>
										
									</c:when>
									<c:when test="${today < fn:replace(curriculumVO.applyEndDate,'-','') and today > fn:replace(curriculumVO.applyStartDate,'-','')}">
										<c:out value="${curriculumVO.applyStartDate}"/>
										<input type="hidden" id="applyStartDate" value="<c:out value="${curriculumVO.applyStartDate}"/>"/>
										~
										<div class="flex-col-3">
											<input type="text" id="applyEndDate" class="ell date datepicker" value="<c:out value="${curriculumVO.applyEndDate}"/>" placeholder="종료일" readonly="readonly"/>
										</div>
									</c:when>
								</c:choose>
							</div>
                            <div class="table-data box_apl_text">
                            	<c:out value="${curriculumVO.applyStartDate}"/> ~ <c:out value="${curriculumVO.applyEndDate}"/>
                            </div>
                          </div>
                          <div class="flex-col-auto">
                            <c:choose>
								<c:when test="${processSttusCode > 3}">
									<c:url var="aplViewwUrl" value="/lms/crm/curriculumStudent.do">
										<c:param name="crclId" value="${curriculumVO.crclId}" />
										<c:param name="crclbId" value="${curriculumVO.crclbId}" />
										<c:param name="menuId" value="MNU_0000000000000070" />
										<c:param name="subtabstep" value="4" />
										<c:param name="thirdtabstep" value="1" />
									</c:url>
									<a href="${aplViewwUrl}" class="btn-sm-130 btn-outline" target="_blank">수강신청 조회</a>
								</c:when>
								<c:otherwise>
									<c:url var="uptUrl" value="/lms/crcl/updateCurriculumPart.do">
										<c:param name="crclId" value="${curriculumVO.crclId}" />
									</c:url>
									<a href="${uptUrl}" class="btn-sm-130 btn-outline btn_apl" data-type="1">기간 수정</a>
								</c:otherwise>
							</c:choose>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>

            <article class="content-wrap">
              <!-- 성적발표기간 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">성적발표기간</div>
                </div>
              </div>
              <div class="content-body">
                <table class="common-table-wrap table-style2 ">
                  <tbody>
                    <tr>
                      <th class="title">기간 수정 및 등록 </th>
                      <td>
                      	<c:url var="resultUrl" value="/lms/crcl/updateCurriculumPart.do">
							<c:param name="crclId" value="${curriculumVO.crclId}" />
						</c:url>
						<div class="flex-row flex-nowrap box_result_inp"<c:if test="${not empty curriculumVO.resultStartDate }">style="display:none;"</c:if>>
	                          <div class="flex-col-3">
	                            <input type="text" id="resultStartDate" class="ell date datepicker" value="${curriculumVO.resultStartDate}" placeholder="시작일" readonly="readonly">
	                          </div>
	                          <div class="flex-col-2">
	                            <input type="text" id="resultStartTime" class="table-input" value="${curriculumVO.resultStartTime}" placeholder="시작 시간">
	                          </div>
	                          <div class="flex-col-auto">
	                            <div class="table-data">
	                              ~ </div>
	                          </div>
	                          <div class="flex-col-3">
	                            <input type="text" id="resultEndDate" class="ell date datepicker" value="${curriculumVO.resultEndDate}" placeholder="종료일">
	                          </div>
	                          <div class="flex-col-2">
	                            <input type="text" id="resultEndTime" class="table-input" value="${curriculumVO.resultEndTime}" placeholder="종료 시간">
	                          </div>
	                          <div class="flex-col-auto">
                        		<button class="btn-sm-130 btn-point btn_result" data-href="${resultUrl}">기간 등록</button>
                       		  </div>
                        </div>                      
                        
						<div class="flex-row box_result_text" <c:if test="${empty curriculumVO.resultStartDate }">style="display:none;"</c:if>>
							<div class="flex-col">
		                        <div class="table-data">
									<c:out value="${curriculumVO.resultStartDate}"/>
									<c:out value="${curriculumVO.resultStartTime}"/>
									~
									<c:out value="${curriculumVO.resultEndDate}"/>
									<c:out value="${curriculumVO.resultEndTime}"/>
								</div>
							</div>
							<div class="flex-col-auto">
								<button class="btn-sm-130 btn-outline btn_result" data-href="${resultUrl}" data-type="1">기간 수정</button>
							</div>
						</div>
                    </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
			
			<article class="content-wrap">
              <!-- 과정만족도 설문제출 시작일 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정만족도 설문제출 시작일</div>
                </div>
              </div>
              <div class="content-body">
                <table class="common-table-wrap table-style2 ">
                  <tbody>
                    <tr>
                      <th class="title">기간 수정 및 등록 </th>
                      <td>
						<div class="flex-row flex-nowrap box_survey_inp"<c:if test="${not empty curriculumVO.surveyStartDate }">style="display:none;"</c:if>>
	                          <div class="flex-col-3">
	                            <input type="text" id="surveyStartDate" class="ell date datepicker" value="${curriculumVO.surveyStartDate}" placeholder="시작일" readonly="readonly">
	                          </div>
	                          <div class="flex-col-2">
	                            <input type="text" id="surveyStartTime" class="table-input" value="${curriculumVO.surveyStartTime}" placeholder="시작 시간">
	                          </div>
	                          <div class="flex-col-auto">
	                            <div class="table-data">
	                              ~ </div>
	                          </div>
	                          <div class="flex-col-5">
	                            	과정상태가 과정종료(성적발표)까지 제출가능
	                          </div>
	                          <div class="flex-col-auto">
                        		<button class="btn-sm-130 btn-point btn_survey" data-href="${resultUrl}">기간 등록</button>
                       		  </div>
                        </div>                      
                        
						<div class="flex-row box_survey_text" <c:if test="${empty curriculumVO.surveyStartDate }">style="display:none;"</c:if>>
							<div class="flex-col">
		                        <div class="table-data">
									<c:out value="${curriculumVO.surveyStartDate}"/>
									<c:out value="${curriculumVO.surveyStartTime}"/>
									~
									과정상태가 과정종료(성적발표)까지 제출가능
								</div>
							</div>
							<div class="flex-col-auto">
								<button class="btn-sm-130 btn-outline btn_survey" data-href="${resultUrl}" data-type="1">기간 수정</button>
							</div>
						</div>
                    </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>
			
            <article class="content-wrap">
              <!-- 과정상태 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">과정상태정보</div>
                </div>
              </div>
              <div class="content-body">
                <table class="common-table-wrap table-style2 ">
                  <tbody>
                    <tr>
                      <th class="title">과정상태 변경 및 수정 </th>
                      <td>
                        <div class="flex-row">
                          <div class="flex-col">
	                          <div class="table-data">
								<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
			    					<c:if test="${statusCode.code eq curriculumVO.processSttusCodeDate}">
			    						<c:out value="${statusCode.codeNm}"/>
			    						<c:set var="codeNm" value="${statusCode.codeNm}"/>
										<c:set var="codeDc" value="${statusCode.codeDc}"/>
			    					</c:if>
								</c:forEach>
							</div>
                          </div>
                          <div class="flex-col-auto">
	                        <c:choose>
								<c:when test="${processSttusCode eq '1'}">
									<c:url var="cancelUrl" value="/lms/crcl/updatePsCodeCurriculum.do${_BASE_PARAM}">
										<c:param name="crclId" value="${curriculumVO.crclId}" />
										<c:param name="processSttusCode" value="0" />
									</c:url>
									<button type='button' class='btn-sm-130 btn-outline btnModalOpen btn_cancel btn_mngTxt' data-modal-type='change_status_process' onclick='${cancelUrl}'>과정 확정 취소</button>
								</c:when>
								<c:otherwise>
									<button type='button' id="btn-open-dialog" class='btn-sm-130 btn-outline btnModalOpen btn_mngTxt' data-modal-type='change_status_process'>과정 상태 변경</button>
								</c:otherwise>
							</c:choose>
						</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </article>

          </section>

        </div>
        
<div id="change_status_process_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">과정상태 변경</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text">현재 [<c:out value="${codeNm}"/>] 상태입니다. 상태를 변경하시겠습니까?</p>
          <p class="modal-subtext mb-30 box_exp">
            [<c:out value="${codeNm}"/>] <br>
            <c:out value="${codeDc}"/>
          </p>
          <form id="prcsForm" action="/lms/crcl/updatePsCodeCurriculum.do" method="post">
          	  <input type="hidden" name="crclId" value="${curriculumVO.crclId}"/>
          	  <input type="hidden" name="menuId" value="MNU_0000000000000062"/>
          	  <input type="hidden" name="step" value="1"/>
	          <div class="modal-area-sm">
	            <select class="table-select select2" id="processCode" name="processSttusCode" data-select="style1">
	            	<c:forEach var="result" items="${statusComCode}">
	            		<option value="<c:out value="${result.code}"/>" data-codeNm="${result.codeNm}" data-codeDc="${result.codeDc}" <c:if test="${result.code eq curriculumVO.processSttusCode}">selected="selected"</c:if>><c:out value="${result.codeNm}"/></option>
	            	</c:forEach>
	            </select>          
	          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">닫기</button>
          <button class="btn-xl btn-point btnModalConfirm btn_change">상태 변경</button>
        </div>
      </div>
    </div>
  </div>
  
</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>