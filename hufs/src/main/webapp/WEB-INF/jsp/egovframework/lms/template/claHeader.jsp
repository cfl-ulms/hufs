<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

	<div class="title-wrap">
          <p class="sub-title"><c:out value="${curriculumVO.crclNm}"/></p>
          <h2 class="title"><c:out value="${scheduleMngVO.studySubject}"/></h2>
          <div class="desc-wrap">
	          <c:if test="${USER_INFO.userSeCode ne '08'}">
	          	<jsp:useBean id="now" class="java.util.Date" />
    			<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />
    			<fmt:formatDate value="${now}" pattern="HHmm" var="todayTime" />
    			
	            <p class="progress">
	            <%-- ${empty scheduleAttention ? '대기' : scheduleAttention.attentionType eq 'Y' ? '출석' : scheduleAttention.attentionType eq 'L' ? '지각' : '결석'} --%>
	            
	            <c:choose>
	            	<c:when test="${empty scheduleAttention}">
	            		<c:choose>
	            			<c:when test="${today eq scheduleMngVO.endDt && todayTime > scheduleMngVO.startTime && todayTime <= scheduleMngVO.endTime}">
	            				진행중
	            			</c:when>
	            			<c:when test="${today > scheduleMngVO.endDt}">
	            				결석
	            			</c:when>
	            			<c:otherwise>
	            				대기
	            			</c:otherwise>
	            		</c:choose>
	            	</c:when>
	            	<c:when test="${scheduleAttention.attentionType eq 'Y'}">
	            		출석
	            	</c:when>
	            	<c:when test="${scheduleAttention.attentionType eq 'L'}">
	            		지각
	            	</c:when>
	            	<c:when test="${scheduleAttention.attentionType eq 'N'}">
	            		결석
	            	</c:when>
	            	<c:otherwise>
	            		-
	            	</c:otherwise>
	            </c:choose>
	            </p>
	          </c:if>
	          <p class="date">
	          	수업일  : ${scheduleMngVO.startDt}
	          	<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
	  			~
	  			<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
	          </p>
          </div>
    </div>

    <c:if test="${param.pdfFlag eq 'Y'}">
    	<!-- TO DO
        <div class="util-wrap">
            <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
        </div>
         -->
    </c:if>