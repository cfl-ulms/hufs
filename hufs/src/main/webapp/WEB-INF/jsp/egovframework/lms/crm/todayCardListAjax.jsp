<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="java.util.Date" %>

<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="HHmmss" var="currTime" />

			  <!-- 날짜선택 -->
	          <div class="date-select-wrap">
	            <button class="btn-prev btn-main-prev">
	              <!-- disabled스타일 정의되어있음 -->
	              <i class="icon-arrow" data-id="${yesterDate}"></i>
	              <p class="title left-align">이전수업<span class="date" id="prevDate">${yesterDate}</span></p>
	            </button>
	            <div class="selected-date">
	              <h3 class="title <c:if test="${USER_INFO.userSeCode ne '08'}">student-title</c:if>" id="current" >${today}(${dayNm})</h3>
	              <p class="sub-title">
	              	<c:choose>
	              		<c:when test="${fn:length(myCurriculumList) > 0}">
	              			<%-- 현재 총 ${fn:length(myCurriculumList)} 개의 특수외국어 수업이 진행 중입니다. --%>
	              			오늘 총  ${fn:length(myCurriculumList)}개의 수업이 있습니다.
	              		</c:when>
	              		<c:otherwise>
	              			오늘은 수업이 없습니다.
	              		</c:otherwise>
	              	</c:choose>
	              </p>
	            </div>
	            <button class="btn-next btn-main-next">
	              <p class="title right-align">다음수업<span class="date" id="afterDate">${tomorrowDate}</span></p>
	              <i class="icon-arrow" data-id="${tomorrowDate}"></i>
	            </button>
	          </div>
	          <!-- 슬라이드 -->
	          <div class="class-list-wrap myClassSlick flex-row" data-slick="true">
	            <!-- card style -->
	            <c:forEach var="result" items="${myCurriculumList}" varStatus="status">
		            <div class="flex-col relative">
		            	<c:url var="stuViewUrl" value="/lms/manage/studyPlanView.do">
                            <c:param name="menuId" value="MNU_0000000000000086" />
                            <c:param name="crclId" value="${result.crclId}" />
                            <c:param name="plId" value="${result.plId}" />
                            <c:param name="tabType" value="T" />
                            <c:param name="step" value="3" />
                        </c:url>
		              <a href="${stuViewUrl}" class="card-wrap">
		
		                <div class="card-head">
		                	<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />
							<fmt:formatDate value="${now}" pattern="HHmm" var="todayTime" />
		                	<c:choose>
		                		<c:when test="${dayFlag eq 'TODAY'}">
				                	<c:choose>
				                		<c:when test="${result.startTime > currTime}">
				                			<div class="card-label bg-green">대기</div>
				                		</c:when>
				                		<c:when test="${result.startTime <= currTime and currTime < result.endTime}">
				                			<div class="card-label bg-point">수업중</div>
				                		</c:when>
				                		<c:when test="${result.endTime < currTime}">
				                			<c:choose>
				                				<c:when test="${userSeCode eq '08'}">
				                					<div class="card-label">수업완료</div>
				                				</c:when>
				                				<c:otherwise>
				                					<div class="card-label bg-blue">
				                						<c:choose>
				                							<c:when test="${empty result.attentionType}">
											            		<c:choose>
											            			<c:when test="${today eq crclDay && todayTime > result.startTime && todayTime <= result.endTime}">
											            				진행중
											            			</c:when>
											            			<c:when test="${today > crclDay}">
											            				결석
											            			</c:when>
											            			<c:otherwise>
											            				대기
											            			</c:otherwise>
											            		</c:choose>
											            	</c:when>
											            	<c:when test="${result.attentionType eq 'Y'}">
											            		출석
											            	</c:when>
											            	<c:when test="${result.attentionType eq 'L'}">
											            		지각
											            	</c:when>
											            	<c:when test="${result.attentionType eq 'N'}">
											            		결석
											            	</c:when>
											            	<c:otherwise>
											            		-
											            	</c:otherwise>
				                						</c:choose>
				                					</div>
				                				</c:otherwise>
				                			</c:choose>
				                		</c:when>
				                		<c:otherwise>-</c:otherwise>
				                	</c:choose>
				                </c:when>
				                <c:when test="${dayFlag eq 'YESTERDAY'}">
				                	<c:choose>
		                				<c:when test="${userSeCode eq '08'}">
		                					<div class="card-label">수업완료</div>
		                				</c:when>
		                				<c:otherwise>
		                					<div class="card-label bg-blue">
		                						<c:choose>
		                							<c:when test="${empty result.attentionType}">
									            		<c:choose>
									            			<c:when test="${today eq crclDay && todayTime > result.startTime && todayTime <= result.endTime}">
									            				진행중
									            			</c:when>
									            			<c:when test="${today > crclDay}">
									            				결석
									            			</c:when>
									            			<c:otherwise>
									            				대기
									            			</c:otherwise>
									            		</c:choose>
									            	</c:when>
									            	<c:when test="${result.attentionType eq 'Y'}">
									            		출석
									            	</c:when>
									            	<c:when test="${result.attentionType eq 'L'}">
									            		지각
									            	</c:when>
									            	<c:when test="${result.attentionType eq 'N'}">
									            		결석
									            	</c:when>
									            	<c:otherwise>
									            		-
									            	</c:otherwise>
		                						</c:choose>
		                					</div>
		                				</c:otherwise>
		                			</c:choose>
				                </c:when>
				                <c:when test="${dayFlag eq 'TOMORROW'}">
				                	<div class="card-label bg-green">대기</div>
				                </c:when>
				                <c:otherwise>-</c:otherwise>
				           </c:choose>
		                  <p class="card-flag-img">
		                     <c:set var="imgSrc">
								<c:import url="/lms/common/flag.do" charEncoding="utf-8">
									<c:param name="ctgryId" value="${result.crclLang}"/>
								</c:import>
							  </c:set>
							  <img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기">
		                  </p>
		                  <div class="card-title-wrap">
		                    <span class="card-subtitle"><b>${fn:substring(result.startTime, 0, 2)}:${fn:substring(result.startTime, 2, 4)}</b> 
		                    (${result.sisu}교시)</span>
		                    <div class="card-title">${result.crclNm}</div>
		                  </div>
		                </div>
		                <div class="card-body">
		
		                  <ul class="card-items-wrap">
		                    <li class="card-items icon-campus">
		                    	<c:forEach var="campus" items="${campusList}" varStatus="status">
									<c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
									  <c:out value="${campus.ctgryNm}"/>
									</c:if>
								</c:forEach>
		                    </li>
		                    <li class="card-items icon-name">
		                    	<c:set var="comma" value=","/>								
								<c:set var="facCnt" value="1"/>
								<c:forEach var="list" items="${myCurriculumList}" varStatus="status">
									<c:if test="${result.crclId eq list.crclId and result.plId eq list.plId}">
										<c:forEach var="fac" items="${list.facPlList}" varStatus="status">
											<c:if test="${fac.plId eq list.plId}">
												<c:if test="${!status.last}">
													<c:out value="${fac.userNm}"/>
													<c:if test="${facCnt ne 1}">(부교원)</c:if>${comma}
												</c:if>
												<c:if test="${status.last}">
													<c:out value="${fac.userNm}"/>
													<c:if test="${facCnt ne 1}">(부교원)</c:if>교수
												</c:if>
												<c:set var="facCnt" value="${facCnt + 1}"/>
											</c:if>
										</c:forEach>
									</c:if>
								</c:forEach>
		                    </li>
		                  </ul>
		                </div>
		              </a>
		            </div>
	            </c:forEach>
	          </div>
	          
	          <script>
	          Lms.common_ui.init();
	          </script>