<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>   
    <c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
    <c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
    <c:if test="${not empty searchVO.searchCampusId}"><c:param name="searchCampusId" value="${searchVO.searchCampusId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
//수강 신청 이후 return 메세지 출력
<c:if test="${!empty requestMessage }">
    alert("${requestMessage}");
</c:if>

$(document).ready(function() {
    //국가 선택 검색
    $(document).on("click", ".chkTargetType", function() {
        if($(this).is(":checked") == true) {
            $("#targetType").val("Y").prop("selected", true);
        } else {
            $("#targetType").val("").prop("selected", true);
        }
        $("form[name=frm]").submit();
    });
    
    //페이징
    $(document).on("click", ".addPage", function() {
        var pageIndex = parseInt($(this).attr("data-pageindex"));
        var clickflag = $(this).attr("data-clickflag");
        var params    = jQuery("form[name=frm]").serialize() + "&pageIndex=" + pageIndex + "&myCurriculumPageAt=Y&ajaxFlag=myTeather";

        //데이터가 더 이상 없을 때 flag 처리
        if(clickflag == "F") {
            alert("더 이상 데이터가 없습니다.");
            return;
        }

        //여러번 호출 방지를 위한 flag 처리
        if(clickflag == "N") {
            alert("빠른 시간에 여러번 호출이 불가능 합니다.");
            return;
        }
        
        //여러번 호출 방지를 위한 flag 처리
        $(".addPage").attr("data-clickflag", "N");
        
        $.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/crm/CurriculumListAjax.do",
            data:params,
            success: function(data) {
                //tr 태그 추가
                $(".list_table tbody").append(data);
                
                if($.trim(data) == "" || $.trim(data) == null) {
                    alert("더 이상 데이터가 없습니다.");
                    $(".addPage").attr("data-clickflag", "F");
                    return;
                }

                //페이징 index 증가
                pageIndex++;
                $(".addPage").attr("data-pageindex", pageIndex);
                
                //여러번 호출 방지를 위한 flag 처리
                $(".addPage").attr("data-clickflag", "Y");
            },
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });
        
    });
});

</script>
        <section class="page-content-body">
          <form name="frm" method="post" action="<c:url value="/lms/crm/selectMyCurriculumList.do"/>">
            <input type="hidden" name="menuId" value="${param.menuId }" />
            <input type="hidden" name="tabStep" value="${param.tabStep }" />

            <article class="content-wrap mb-40">
              <div class="box-wrap mb-40">
                <h3 class="title-subhead">나의 교육과정 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
	                    <select class="select2" name="searchCrclYear" id="searchCrclYear" data-select="style3">
			                <option value="">년도</option>
			                <c:forEach var="result" items="${yearList}" varStatus="status">
			                    <option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
			                </c:forEach>
			            </select>
			        </div>
                  </div>
                  <div class="flex-ten-col-2 mb-20">
                    <div class="ell">
                      <select class="select2" data-select="style3" id="searchCrclTerm" name="searchCrclTerm">
                        <option value="">학기</option>
		                <c:forEach var="result" items="${crclTermList}" varStatus="status">
		                    <c:if test="${result.ctgryLevel eq '1'}">
		                        <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
		                    </c:if>
		                </c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-6 mb-20">
                    <div class="ell">
                      <input name="searchCrclNm" type="text" placeholder="찾으시는 과정명을 입력해보세요" value="${searchVO.searchCrclNm }">
                    </div>
                  </div>
                  <div class="flex-ten-col-4">
                    <div class="desc">
                      <input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="ell date datepicker type2" placeholder="교육 시작일" readonly="readonly"/>
                      <i>~</i>
                      <input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="ell date datepicker type2" placeholder="교육 종료일" readonly="readonly"/>
                    </div>
                  </div>
                  <div class="flex-ten-col-2">
                    <div class="ell">
                      <select id="searchProcessSttusCodeDate" name="searchProcessSttusCodeDate" class="select2" data-select="style3">
                        <option value="">과정상태 전체</option>
                        <c:forEach var="result" items="${statusComCode}" varStatus="status">
                          <c:if test="${result.code ne 0 and result.code ne 1 and result.code ne 5 }">
                            <option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCodeDate}">selected="selected"</c:if>>${result.codeNm}</option>
                          </c:if>
                        </c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-2">
                    <div class="ell">
                      <select id="targetType" name="searchTargetType" class="select2" data-select="style3">
                          <option value="">대상 전체</option>
                          <option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
                          <option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-2">
                    <div class="ell">
                      <select name="searchCampusId" id="searchCampusId" class="select2" data-select="style3">
                        <option value="">장소 전체</option>
                        <c:forEach var="result" items="${campusList}" varStatus="status">
                          <c:if test="${result.ctgryLevel eq '1'}">
                            <option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCampusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
                          </c:if>
                        </c:forEach>
                      </select>
                    </div>
                  </div>
                </div>
            
                <button class="btn-sm font-400 btn-point mt-20">검색</button>
              </div>
            </article>
            <article class="content-wrap">
              <!-- tab style -->
              <c:url var="tabUrl" value="/lms/crm/selectMyCurriculumList.do">
                    <c:param name="menuId" value="${param.menuId}"/>
                </c:url>
              <ul class="tab-wrap sub-tab-style">
                <li class="tab-list <c:if test="${param.tabStep eq 1 or tabStep eq 1 }">on</c:if>"><a href="${tabUrl }&tabStep=1">전체</li>
                <li class="tab-list <c:if test="${param.tabStep eq 2 or tabStep eq 2 }">on</c:if>"><a href="${tabUrl }&tabStep=2&curriculumIngFlag=Y">수강신청중 과정</a></li>
                <li class="tab-list <c:if test="${param.tabStep eq 3 or tabStep eq 3 }">on</c:if>"><a href="${tabUrl }&tabStep=3&searchProcessSttusCodeDate=7">진행중 과정</a></li>
                <li class="tab-list <c:if test="${param.tabStep eq 4 or tabStep eq 4 }">on</c:if>"><a href="${tabUrl }&tabStep=4&closeCurriculumAt=Y">수강완료된 과정</a></li>
              </ul>
              <div class="mt-20">
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board list_table">
                  <colgroup>
                    <col style="width:8%">
                    <col style="width:10%">
                    <col style="width:11%">
                    <col style="width:12%">
                    <col>
                    <col style="width:130px">
                    <col style="width:9%">
                    <col style="width:11%">
                    <c:if test="${searchVO.searchProcessSttusCodeDate eq 7}">
                    	<col style="width:8%">
                    </c:if>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>년도</th>
                      <th scope='col'>학기</th>
                      <th scope='col'>과정진행<br>상태</th>
                      <th scope='col'>언어</th>
                      <th scope='col'>교육과정명</th>
                      <th scope='col'>교육기간</th>
                      <th scope='col'>대상</th>
                      <th scope='col'>장소</th>
                      <c:if test="${searchVO.searchProcessSttusCodeDate eq 7}">
                    	<th scope='col'>수업자료</th>
                      </c:if>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach var="result" items="${resultList}" varStatus="status">
                        <c:url var="viewUrl" value="/lms/crm/myCurriculumView.do${_BASE_PARAM}">
                            <c:param name="crclId" value="${result.crclId}"/>
                            <c:param name="crclbId" value="${result.crclbId}"/>
                            <c:param name="subtabstep" value="1"/>
                            <c:choose>
                                <c:when test="${!empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"/></c:when>
                                <c:otherwise><c:param name="tabStep" value="${tabStep}"/></c:otherwise>
                            </c:choose>
                            
                            <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                        </c:url>
                        <tr onclick="location.href='${viewUrl}'">
                            <td><c:out value="${result.crclYear}"/></td>
                            <td><c:out value="${result.crclTermNm}"/></td>
                            <c:choose>
                                <c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
                                <c:when test="${result.processSttusCodeDate eq 3 or result.processSttusCodeDate eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
                                <c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
                                <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
                            </c:choose>
                            <td scope="row" class="${processSttusColor }">
                                <c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
                                    <c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
                                </c:forEach>
                            </td>
                            <td><c:out value="${result.crclLangNm}"/></td>
                            <td><c:out value="${result.crclNm}"/></td>
                            <td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
                            <td>
                                <c:choose>
                                    <c:when test="${result.targetType eq 'Y'}">본교생</c:when>
                                    <c:otherwise>일반</c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <c:forEach var="campus" items="${campusList}" varStatus="status">
                                  <c:if test="${campus.ctgryLevel eq '1' and campus.ctgryId eq result.campusId}">
                                    <c:out value="${campus.ctgryNm}"/>
                                  </c:if>
                                </c:forEach>
                            </td>
                            <c:if test="${searchVO.searchProcessSttusCodeDate eq 7}">
			    				<c:url var="fileUrl" value="/lms/cla/classFileList.do">
									<c:param name="crclId" value="${result.crclId}"/>
									<c:param name="crclbId" value="${result.crclbId}"/>
									<c:param name="menuId" value="${param.menuId}"/>
									<c:param name="tabStep" value="${param.tabStep}"/>
									<c:param name="searchProcessSttusCodeDate" value="7"/>
									<c:param name="subtabstep" value="3"/>
									<c:param name="addAt" value="Y"/>
								</c:url>
		                      	<td>
		                      		<a href="${fileUrl}">조회</a>
		                      	</td>
		                    </c:if>
                        </tr>
                    </c:forEach>
                    <c:if test="${fn:length(resultList) == 0}">
                        <tr>
                            <td class="listCenter" colspan="8"><spring:message code="common.nodata.msg" /></td>
                        </tr>
                    </c:if>
                  </tbody>
                </table>
                <div class="mt-50 center-align">
                  <button type="button" class="cursor-pointer addPage" data-pageindex="1" data-clickflag="Y"><img class="vertical-top" src="${_L_IMG }/common/btn_board_contents_more.jpg" alt="더보기"></button>
                </div>
              </div>
            </article>
          </form>
        </section>
      </div>
    </div>
  </div>
</div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
</c:import>