<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizRegView.do"/>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

  <footer id="footer">
    <div class="area">
      <div class="footer-top">
        <div class="footer-term">
          <button type="button" class="item btnModalOpen" data-modal-type="privacy_policy">개인정보처리방침</button>
          <button type="button" class="item btnModalOpen" data-modal-type="terms_of_use">이용약관</button>
          <button type="button" class="item btnModalOpen" data-modal-type="copyright_policy">저작권 보호 정책 </button>
          <a href="http://210.127.211.111/ftp/ufolio/lms/main/main_logout.html" class="item">PC버전</a>
        </div>
      </div>
      <div class="company-info-wrap">
        <div class="company-info">
          <p class="item">(02450) 서울시동대문구 이문로 107<br>한국외국어대학교 교수학습개발원 501-1호</p>
          <p class="item">TEL 02-2173-2843~6 FAX 02-2173-2847</p>
          <p class="item">E-mail cfle@hufs.ac.kr</p>
          <p class="item copyright">© 2019 Hankuk University of Foreign Studies. All Rights Reserved.</p>
          <h2 class="footer-logo">
            <img class="img-logo" src="/template/lms//imgs/common/common_footer_img_logo.png" alt="로고">
          </h2>
        </div>
      </div>
    </div>
  </footer>
  <div id="privacy_policy_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">개인정보처리방침</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text">개인정보수집∙이용에 따른 약관동의</p>
          <p class="modal-subtext">
            <div class="policy-wrap">
              <div class="text">
                <div class="inner-top">
                  한국외국어대학교 특수외국어교육진흥원(이하 ‘특교원’이라 한다)이 취급하는 모든 개인정보는 개인정보보호법 등 관련 법령상의 개인정보보호 규정을 준수하여 이용자의 개인정보보호 및 권익을
                  보호하고 개인정보와 관련한 이용자의 고충을 원활하게 처리할 수 있도록 다음과 같은 처리방침을 두고 있습니다.
                </div>
                <div class="inner-desc">
                  <b>제1조. 개인정보의 처리목적</b>
                  특교원은 개인정보를 다음의 목적을 위해 처리합니다. 처리하고 있는 개인정보는 다음의 목적이외의 용도로는 이용되지 않으며 이용 목적이 변경될 시에는 사전 동의를 받아 필요한 조치를 이행할
                  예정입니다.
                  <ul>
                    <li class="dot">회원 가입 및 관리</li>
                    <li>
                      회원제 서비스 제공에 따른 본인확인, 개인 식별, 불량회원의 부정이용 방지와 비인가 사용방지, 가입의사 확인, 연령확인, 분쟁 조정을 위한 기록 보존, 불만처리 등 민원처리, 고지사항 전달 등
                    </li>
                    <li class="dot">학사 업무 처리 및 서비스 제공</li>
                    <li>프로그램 수강생 선발, 성적 관리, 특수외국어평가시험 업무, 장학금 업무, 증명서 발행 및 기타 사업 관리 업무</li>
                  </ul>

                  <b>제2조. 개인정보 처리, 항목 및 보유 기간</b>
                  <ul>
                    <li>
                      ① 이용자의 개인정보는 수집 시에 동의받은 개인정보 보유·이용기간 내에서 개인정보를 처리·보유하며, 개인정보의 처리 목적이 달성되면 즉시 파기합니다. 다만, 학칙이나 기타 법률에
                      의해 이용자의 개인정보를 보존해야할 필요가 있는 경우에는 해당 법률의 규정을 따릅니다.
                    </li>
                    <li>② 특교원은 다음의 개인정보 항목을 처리하고 있습니다.</li>
                    <li class="dot">
                      <button class="btnModalOpen" data-modal-type="privacy_policy_1">
                        개인정보 항목 [보기]
                      </button>
                    </li>
                  </ul>

                  <b>제3조. 개인정보의 제3자 제공</b>
                  <ul>
                    <li>
                      ① 특교원은 원칙적으로 이용자의 개인정보를 제1조(개인정보의 처리 목적)에서 명시한 범위 내에서 처리하며,
                      정보주체의 동의, 법률의 특별한 규정 등 개인정보보호법 제17조 및 제18조에 해당하는 경우에만 개인정보를 제3자에게 제공하고 있습니다.
                    </li>
                    <li class="dot">
                      <button class="btnModalOpen" data-modal-type="privacy_policy_2">
                        개인정보 제3자 제공현황 [보기]
                      </button>
                    </li>
                  </ul>

                  <b>제4조. 정보주체의 권리·의무 및 그 행사방법</b>
                  <ul>
                    <li>
                      ① 정보주체는 특교원에 대해 다음과 같은 권리를 행사할 수 있으며, 만14세 미만 아동의 법정대리인은 그 아동의 개인정보에 대한 열람, 정정·삭제, 처리정지를 요구할 수 있습니다.
                      <ul>
                        <li class="dot">개인정보 열람 요구</li>
                        <li>
                          특교원에서 보유하고 있는 개인정보파일은 「개인정보보호법」 제35조(개인정보의 열람)에 따라 열람을 요구할 수 있습니다.
                          다만, 개인정보 열람 요구 시 법 제35조 4항 각 호의 어느
                          하나에 해당하는 경우에는 정보주체에 그 사유를 알리고 열람을 제한하거나 거절할 수 있습니다.
                        </li>
                        <li class="dot">개인정보 정정·삭제 요구</li>
                        <li>
                          특교원이 보유하고 있는 개인정보파일은 「개인정보보호법」 제36조(개인정보의 정정·삭제)에 따라 정정·삭제를 요구할 수 있습니다. 다만, 다른 법령에서 그 개인정보가 수집
                          대상으로
                          명시되어 있는 경우에는 그 삭제를 요구할 수 없습니다.
                        </li>
                        <li class="dot">개인정보 처리정지 요구</li>
                        <li>
                          특교원에서 보유하고 있는 개인정보파일은 「개인정보보호법」 제37조(개인정보의 처리정지 등)에 따라 처리정지를 요구할 수 있습니다. 다만, 개인정보 처리정지 요구 시 법
                          제37조 2항에 의하여 정보주체의 권리가 거절될 수 있습니다.
                        </li>
                      </ul>
                    </li>
                    <li>
                      ② 제1항에 따른 권리 행사는 서면, 전자우편, 모사전송(FAX) 등을 통하여 하실 수 있으며, 특교원 10일 이내에 이에 대한 조치를 통지합니다.
                    </li>
                    <li>
                      ③ 제1항에 따른 권리 행사는 정보주체의 법정대리인이나 위임을 받은 자 등 대리인을 통하여 하실 수 있습니다.
                    </li>
                  </ul>

                  <b>제5조. 개인정보의 파기</b>
                  특교원은 원칙적으로 개인정보 보유기간 경과, 처리목적이 달성된 경우에는 지체없이 해당 개인정보를 파기합니다. 파기의 절차, 기한 및 방법은 다음과 같습니다.
                  <ul>
                    <li class="dot">파기절차 </li>
                    <li>이용자가 입력한 정보는 목적 달성 후 별도의 DB에 옮겨져(종이의 경우 별도의 서류) 내부 방침 및 기타 관련 법령에 따라 즉시 혹은 일정기간 저장된 후 개인정보보호책임자
                      승인을 받아
                      개인정보를 파기합니다. 이 때, DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 다른 목적으로 이용되지 않습니다.</li>
                    <li class="dot">파기기한 </li>
                    <li>
                      이용자의 개인정보는 보유기간 경과 처리 목적 달성, 해당 서비스의 폐지, 사업의 종료 등 그 개인정보가 불필요하게 되었을 때에는 인정되는 날로부터 5일 이내에 해당 개인정보를
                      파기합니다.
                    </li>
                    <li class="dot">파기방법</li>
                    <li>전자적 파일 형태의 정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 파기하며, 종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.</li>
                  </ul>

                  <b>제6조. 개인정보의 안전성 확보조치</b>
                  특교원은 「개인정보보호법」 제29조에 따라 안전성 확보에 필요한 기술적·관리적 및 물리적 조치를 하고 있으며 그 절차와 방법은 우리 대학의 규정을 준용하고 있습니다.

                  <b>제7조. 권익침해 구제 방법</b>
                  <ul>
                    <li>① 정보주체는 아래의 기관에 대해 개인정보 침해에 대한 피해구제, 상담 등을 문의하실 수 있습니다.
                      <ul>
                        <li>[아래의 기관은 특교원과는 별개의 기관으로서, 특교원의 자체적인 개인정보 불만처리, 피해구제 결과에 만족하지 못하시거나 보다 자세한 도움이 필요하시면 문의하여 주시기
                          바랍니다]</li>
                        <li>1. 개인정보 침해신고센터(한국인터넷진흥원 운영)
                          <ul>
                            <li>
                              가. 소관업무 : 개인정보 침해사실 신고, 상담 신청<br>
                              나. 홈페이지 : privacy.kisa.or.kr<br>
                              다. 연락처 : (국번없이) 118
                            </li>
                          </ul>
                        </li>
                        <li>2. 개인정보 분쟁조정위원회
                          <ul>
                            <li>
                              가. 소관업무 : 개인정보 분쟁조정신청, 집단분쟁조정(민사적 해결)<br>
                              나. 홈페이지 : www.kopico.go.kr<br>
                              다. 연락처 : (국번없이) 1833-6972
                            </li>
                          </ul>
                        </li>
                        <li>3. 대검찰청 사이버수사과
                          <ul>
                            <li>
                              가. 홈페이지 : www.spo.go.kr<br>
                              나. 연락처 : 국번없이) 1301<br>
                            </li>
                          </ul>
                        </li>
                        <li>4. 경찰청 사이버안전국
                          <ul>
                            <li>
                              가. 홈페이지 : http://cyberbureau.police.go.kr<br>
                              나. 연락처 : (국번없이) 182<br>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li>
                      ② 개인정보의 열람, 정정·삭제, 처리정지 등에 대한 정보주체자의 요구에 대하여 특교원이 행한 처분 또는 부작위로 인하여 권리 또는 이익을 침해 받은 자는 행정심판법이 정하는 바에
                      따라
                      행정심판을 청구할 수 있습니다.
                      <ul>
                        <li class="mt-5">1. 중앙행정심판위원회(www.simpan.go.kr)의 전화번호 안내 참조</li>
                      </ul>
                    </li>
                  </ul>

                  <b>제8조. 개인정보보호책임자</b>
                  <ul>
                    <li>① 특교원은 개인정보를 보호하고 개인정보와 관련한 불만처리와 피해구제 등을 위하여 아래와 같이 개인정보보호책임자 및 실무담당자를 지정하고 있습니다.
                      <ul>
                        <li>□ 개인정보보호책임자 및 담당자</li>
                        <li>
                          <table class="common-table-wrap font-sm">
                            <thead class="bg-gray">
                              <tr>
                                <th scrop="col" rowspan="2">구분</th>
                                <th scrop="col" rowspan="2">소속 및 직급</th>
                                <th scrop="col" rowspan="2">성명</th>
                                <th scope="colgroup" colspan="2">연락처</th>
                              </tr>
                              <tr>
                                <th>전화</th>
                                <th>전자우편</th>
                              </tr>
                            </thead>
                            <tbody">
                              <tr>
                                <td>개인정보보호책임자</td>
                                <td>특수외국어교육진흥원장</td>
                                <td>이승용</td>
                                <td>02-2173-3435</td>
                                <td>hlamb@hufs.ac.kr</td>
                              </tr>
                              <tr>
                                <td>개인정보보호담당자</td>
                                <td>특수외국어교육진흥원 담당</td>
                                <td>김별</td>
                                <td>02-2173-2846</td>
                                <td>manzana@hufs.ac.kr</td>
                              </tr>
                              </tbody>
                          </table>
                        </li>
                      </ul>
                    </li>
                    <li>② 정보주체께서는 특교원의 서비스를 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보보호책임자 및 담당부서로 문의하실 수
                      있으며,
                      특교원은 정보주체의 문의에 대해 지체없이 답변 및 처리해드릴 것입니다. </li>
                  </ul>

                  <b>제9조. 개인정보 처리방침 변경</b>
                  <ul>
                    <li>① 이 개인정보 처리방침은 2019.11.30 부터 적용됩니다. </li>
                  </ul>
                </div>
              </div>
            </div>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>
  <div id="privacy_policy_1_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">개인정보 항목</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <table class="common-table-wrap font-sm">
            <colgroup>
              <col style="width:16%">
              <col style="width:30%">
              <col style="width:48%">
              <col style="width:16%">
            </colgroup>
            <thead class="bg-gray">
              <tr>
                <th scrop="col">파일명</th>
                <th scrop="col">처리 목적</th>
                <th scrop="col">개인정보의 항목 (정보주체)</th>
                <th scrop="col">보유기간</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>홈페이지 가입 정보</td>
                <td>홈페이지 이용자 관리</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 성별, 학번(사번), 소속</td>
                <td>2년/회원탈퇴 시</td>
              </tr>
              <tr>
                <td>전공연계 프로그램 수강자 명단</td>
                <td>과정등록, 학사관리, 학적정보 조회 및 사업비 사용, 평가 관리 등</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 학번, 소속, 학적사항, 교육과정 이력, 기타: 계좌번호</td>
                <td>5년</td>
              </tr>
              <tr>
                <td>일반과정 프로그램 수강자 명단</td>
                <td>과정등록, 학사관리, 학적정보 조회 및 사업비 사용, 평가 관리 등</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 소속, 기타: 계좌번호</td>
                <td>5년</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalOpen" data-modal-type="privacy_policy">확인</button>
        </div>
      </div>
    </div>
  </div>
  <div id="privacy_policy_2_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">개인정보 제3자 제공현황</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <table class="common-table-wrap font-sm">
            <colgroup>
              <col style="width:13%">
              <col style="width:15%">
              <col style="width:20%">
              <col style="width:24%">
              <col style="width:29%">
            </colgroup>
            <thead class="bg-gray">
              <tr>
                <th scrop="col">제공받는 기관명</th>
                <th scrop="col">제공근거</th>
                <th scrop="col">제공목적</th>
                <th scrop="col">개인정보 파일명</th>
                <th scrop="col">제공항목</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>엔에스데블</td>
                <td>개인정보 제공 동의</td>
                <td>홈페이지/LMS 시스템 관리</td>
                <td>홈페이지 가입 정보</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 성별, 학번(사번), 소속</td>
              </tr>
              <tr>
                <td>엔에스데블</td>
                <td>개인정보 제공 동의</td>
                <td>홈페이지/LMS 시스템 관리</td>
                <td>전공연계 프로그램 수강자 명단</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 학번, 소속, 학적사항, 교육과정 이력, 기타: 계좌번호 </td>
              </tr>
              <tr>
                <td>엔에스데블</td>
                <td>개인정보 제공 동의</td>
                <td>홈페이지/LMS 시스템 관리</td>
                <td>일반과정 프로그램 수강자 명단</td>
                <td>필수: 이름, E-mail, 전화번호, 생년월일, 소속, 기타: 계좌번호 </td>
              </tr>
              <tr>
                <td>국립국제교육원</td>
                <td>특수외국어교육진흥에 관한 법률 시행령 제 5조</td>
                <td>특수외국어교육 현황 자료 제출</td>
                <td>특교원 프로그램 수강자 명단</td>
                <td>필수: 이름, E-mail, 생년월일, 소속</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalOpen" data-modal-type="privacy_policy">확인</button>
        </div>
      </div>
    </div>
  </div>
  <div id="terms_of_use_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">이용약관</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text">홈페이지 이용에 따른 약관동의</p>
          <p class="modal-subtext">
            <div class="policy-wrap">
              <div class="text">
                <div class="inner-top">
                  특수외국어교육진흥사업의 교육 및 서비스를 이용해주셔서 감사합니다.<br>
                  특수외국어교육진흥원 홈페이지는 구성원들 간의 네트워크를 기반으로 사이버 공동체를 구축하여 다양한 교육 서비스를 제공하기 위해
                  운영되는 공간입니다. 구성원들 간의 훌륭한 커뮤니케이션 공간이 될 수 있도록 각별한 관심과 애정을 가지고 이용해주시기 바라며 교육 서비스 이용과 관련하여 다음과 같은 이용약관(이하 ‘본
                  약관‘)을 준수해 줄 것을 당부 드립니다.
                </div>
                <div class="inner-desc">
                  <b>제 1조 목적</b>
                  본 약관은 한국외국어대학교 특수외국어교육진흥원 홈페이지(이하 ‘홈페이지’)와 이용자(이하 ‘이용자’)간에 한국외국어대학교(이하 ‘학교’)에서 제공하는 각종 서비스의 이용 조건 및 필요한
                  제반 절차에 관한 사항을 정하는데 목적이 있습니다.
                  <b>제 2조 약관의 효력 및 변경</b>
                  본 약관은 홈페이지에서 회원 가입시 실명인증을 거처야 되며, 이후 회원가입 약관에 동의함으로써 효력을 발휘합니다.
                  <b>제 3조 학교의 의무</b>
                  학교는 홈페이지서비스 제공과 관련된 회원정보를 회원의 허락없이 타인에게 제공하지 않습니다. 단, 개인정보 제공에 있어 수사기관의 법적인 충분한 근거가 있는 경우는 절차에 따라 제공할 수
                  있습니다. 학교는 홈페이지의 운영, 유지에 관련된 법률 사항을 준수합니다. 학교는 이용자의 개인정보보호를 위해 보안시스템을 구축하며, 개인정보보호정책을 공시하고 준수합니다.
                  <b>제 4조 이용자의 의무</b>
                  이용자의 아이디와 비밀번호의 관리와 책임은 회원에게 있으며 관리 소홀과 부정사용으로 인한 모든 사항은 이용자에게 귀책됩니다. 이용자는 홈페이지의 서비스를 이용함에 있어 관계법령을 준수하며
                  요구하는 절차를 성실히 이행할 것을 동의해야 합니다. 이용자는 상업적인 목적으로 본 홈페이지에 있는 내용을 임의로 이용할 수 없습니다.
                  <b>제 5조 컨텐츠 관련 조항</b>
                  학교에서 제공하는 모든 컨텐츠와 정보들의 재산권과 권리는 학교에 있습니다. 이용자는 학교가 승인한 경우를 제외하고 전항의 각 재산권들에 대한 복제, 출판, 배포 등 상업적인 행위를 할 수
                  없으며, 3자로 하여금 이와 같은 행위를 하도록 허락할 수 없습니다. 이용자가 게시한 게시물 등 서비스와 관련한 자료들에 대한 권리와 책임은 이용자에게 있으며, 이로 인하여 발생할 수
                  있는 모든 민,형사상 책임은 이용자에게 있습니다. 부에서 링크된 자료는 그 출처를 정확하게 밝혀야 합니다.
                  <b>제 6조 서비스의 이용</b>
                  학교의 홈페이지 서비스는 업무상 또는 기타 시스템의 점검, 증설 및 교체 등 부득이한 경우를 제외하고 연중무휴 1일 24시간 동안 제공되는 것을 원칙으로 합니다.
                  <b>제 7조 개인정보의 보호</b>
                  학교는 이용자의 개인정보보호를 위해 관련 법령 및 학교가 정하는 ‘개인정보처리방침’에 정한 바에 의합니다. 개인정보처리방침은 특별한 목적에 따라 갱신될 수 있으며 이를 공지함을 원칙으로
                  합니다. 이용자의 부주의와 관리소홀로 노출된 개인정보로 인해 발생하는 모든 결과는 이용자에게 있습니다.
                  <b>제 8조 이용자의 회원 탈퇴</b>
                  홈페이지를 통해 이용자 본인이 직접 회원탈퇴신청을 하면 바로 회원탈퇴를 승인합니다.
                  <b>제 9조 손해배상</b>
                  학교의 고의 또는 과실 없이 이용자에게 발생한 일체의 손해에 대하여 학교는 책임을 부담하지 않습니다.
                </div>
              </div>
            </div>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>
  <div id="copyright_policy_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">저작권 보호 정책</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text">저작권 이용에 따른 약관동의</p>
          <p class="modal-subtext">
            <div class="policy-wrap">
              <div class="text">
                <div class="inner-top">
                  특수외국어교육진흥원이 제공하는 제공하는 모든 자료는 저작권법에 의하여 보호받는 저작물로서 모든 권리는 특수외국어교육진흥원에 있습니다. 이용자는 아래의 저작권 보호정책을 준수하여야
                  합니다.<br>
                  본 정책의 목적은 특수외국어교육진흥원 홈페이지를 통하여 제공되는 정보가 정보 출처를 밝히지 않고 무단 사용, 변조, 상업적인 용도 등으로 사용되어 정보 이용자에게 피해를 끼치는 사례를
                  방지하기 위함입니다.
                </div>
                <div class="inner-desc">
                  특수외국어교육진흥원 홈페이지에서 제공하는 모든 자료는 저작권법에 의하여 보호받는 저작물로서, 무단으로 복제, 배포할 경우 저작권 침해에 해당합니다(저작권법 제136조). 우리 교육원
                  홈페이지 자료들을 무단 복제, 배포하는 경우에는 저작권법 제136조(권리의 침해죄)에 해당하여 5년 이하의 징역 또는 5천만원 이하의 벌금에 처해질 수 있습니다.<br>
                  <br>
                  특수외국어교육진흥원 홈페이지에 게재된 자료들은 저작권법에 의하여 보호받는 저작물로 그 저작권은 특수외국어교육진흥원에 있습니다. 따라서, 본 교육원 홈페이지 자료에 대한 무단 복제 및
                  배포를 원칙적으로 금지합니다.<br>
                  <br>
                  본 홈페이지 자료에 대해서 영리를 목적으로 하지 아니하고 개인적으로 이용하거나 가정 및 이에 준하는 범위안에서는 복제가 가능합니다. 다만 회사를 포함한 기타 단체에서 내부적으로 이용하기
                  위하여 복제하는 경우에는 그 회사가 영리 회사가 아니더라도 복제가 허용되지 않습니다.
                </div>
              </div>
            </div>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>