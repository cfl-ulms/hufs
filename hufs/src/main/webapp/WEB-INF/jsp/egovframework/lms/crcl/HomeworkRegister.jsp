<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${BbsFileStoreWebPathByWebFile}homework/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_JS" value="/template/common/js"/>

<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="_EDITOR_ID" value="nttCn"/>
<c:set var="_ACTION" value=""/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${searchVO.step}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
function fn_egov_regist() {
    
    tinyMCE.triggerSave();
    
    if($.trim($('#${_EDITOR_ID}').val()) == "") {
        alert('<spring:message code="cop.nttCn" />은(는) 필수 입력값입니다');
        tinyMCE.activeEditor.focus();
        return false;
    }
    
    if($("select[name=hwType] option:selected").val() == "") {
    	alert("과제유형을 선택해 주세요.");
        return false;
    }
    
    var openDate  = $("input[name=openDate]").val();
    var closeDate = $("input[name=closeDate]").val();

    if(openDate == "" || closeDate == "") {
        alert("시작날짜 또는 종료날짜를 입력해주세요.");
        return false;
    }

    if(Number(openDate.replace(/-/gi, "")) > Number(closeDate.replace(/-/gi, ""))) {
        alert("종료일자가 시작일자보다 작을 수 없습니다.");
        return false;
    }
    
    var timeFormat = /^([01][0-9]|2[0-3]):([0-5][0-9])$/; // 시간형식 체크 정규화 hh:mm
    if(!timeFormat.test($("input[name=openTime]").val()) || !timeFormat.test($("input[name=closeTime]").val())) {
    	alert("시간 형식이 잘못 되었습니다.");
        return false;
    }

    $('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());

    <c:choose>
        <c:when test="${searchVO.registAction eq 'updt'}">
            if (!confirm('<spring:message code="common.update.msg" />')) {
                 return false
            }
        </c:when>
        <c:otherwise>
            if (!confirm('<spring:message code="common.regist.msg" />')) {
                return false;
            }
        </c:otherwise>
    </c:choose>
}

$(document).ready(function(){
	var adfile_config = {
            siteId:"SITE_000000000000001",
            pathKey:"Homework",
            appendPath:"HOMEWORK",
            editorId:"${_EDITOR_ID}",
            fileAtchPosblAt:"Y",
            maxMegaFileSize:100,
            atchFileId:"${homeworkVO.atchFileId}"
        };
        
    fnCtgryInit('');
    fn_egov_bbs_editor(adfile_config);
    
    //글등록
    $(document).on("click", ".insertHomework, .updateHomework", function() {
    	$('#curriculum').submit();
    });
});
</script>

<div class="page-content-header">
  <c:choose>
      <c:when test="${empty param.plId }">
          <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
		      <%-- TODO : ywkim 작업해야함 --%>
		      <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
		      <c:param name="curriculumManageBtnFlag" value="Y"/>
		  </c:import>
      </c:when>
      <c:otherwise>
          <c:choose>
			<c:when test="${not empty param.plId and param.tabType eq 'T'}">
			    <c:import url="/lms/claHeader.do" charEncoding="utf-8">
			    </c:import>
			</c:when>
			<c:otherwise>
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			    <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
			</c:otherwise>
          </c:choose>
      </c:otherwise>
  </c:choose>
</div>
  <section class="page-content-body">  
    <article class="content-wrap">
        <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y' and homeworkTotalFlag ne 'Y'}">
            <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                <c:param name="step" value="${searchVO.step }"/>
                <c:param name="crclId" value="${curriculumVO.crclId}"/>
                <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
            </c:import>
        </c:if>

        <div class="content-header">
          <div class="title-wrap">
            <c:choose>
                <c:when test="${param.mode eq 'curriculum' and param.registAction eq 'regist' }">
                    <c:set var="menuTitle" value="과정과제 등록하기"/>
                </c:when>
                <c:when test="${param.mode eq 'curriculum' and param.registAction eq 'updt' }">
                    <c:set var="menuTitle" value="과정과제 수정하기"/>
                </c:when>
                <c:when test="${param.mode eq 'lesson' and param.registAction eq 'regist' }">
                    <c:set var="menuTitle" value="수업과제 등록하기"/>
                </c:when>
                <c:when test="${param.mode eq 'lesson' and param.registAction eq 'updt' }">
                    <c:set var="menuTitle" value="수업과제 수정하기"/>
                </c:when>
            </c:choose>
            <p class="title">${menuTitle }</p>
          </div>
        </div>
        
        <c:choose>
            <c:when test="${empty homeworkVO.hwId }"><c:set var="formUrl" value="/lms/crcl/insertHomeworkArticle.do"/></c:when>
            <c:otherwise><c:set var="formUrl" value="/lms/crcl/updateHomeworkArticle.do"/></c:otherwise>
        </c:choose>
        <form:form commandName="curriculum" name="board" method="post" action="${formUrl }" enctype="multipart/form-data" onsubmit="return fn_egov_regist()">
          <input name="menuId" type="hidden" value="<c:out value='${param.menuId}'/>" />
          <input id="hwId" name="hwId" type="hidden" value="${homeworkVO.hwId}">
          <input id="atchFileId" name="atchFileId" type="hidden" value="${homeworkVO.atchFileId}">
          <input id="fileGroupId" name="fileGroupId" type="hidden" value="${homeworkVO.atchFileId}">
          <input type="hidden" name="registAction" value="<c:out value='${param.registAction}'/>"/>
          <input type="hidden" name="step" value="<c:out value='${searchVO.step}'/>"/>
          <input type="hidden" name="plId" value="${param.plId}"/>

          <%-- 과제구분(hwCode) : 1 = 과정과제, 2 = 수업과제 --%>
          <c:choose>
              <c:when test="${param.mode eq 'curriculum' }"><c:set var="hwCode" value="1"/></c:when>
              <c:otherwise><c:set var="hwCode" value="2"/></c:otherwise>
          </c:choose>
          <input type="hidden" name="crclId" value="${param.crclId }" />
          <input type="hidden" name="hwCode" value="${hwCode }" />

          <table class="common-table-wrap table-style2 mb-20">
            <tbody>
              <tr>
                <th class="title">과제명 </th>
                <td>
                  <div class="flex-row">
                    <div class="flex-col-12">
                      <input type="text" class="table-input" name="nttSj" value="${homeworkVO.nttSj }" placeholder="과제명을 입력해주세요.">
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="title">과제유형 </th>
                <td>
                  <div class="flex-row">
                    <div class="flex-col-4">
                      <select class="table-select select2 " name="hwType" data-select="style1">
                        <c:choose>
                            <c:when test="${param.registAction eq 'updt' and homeworkVO.hwType eq '1'}"><option value="1">개별과제</option></c:when>
                            <c:when test="${param.registAction eq 'updt' and homeworkVO.hwType eq '2'}"><option value="2">조별과제</option></c:when>
                            <c:otherwise>
                                <option value="1">개별과제</option>
                                <%-- 조가 있을 때에만 조별과제가 등록 되도록 처리 --%>
                                <c:if test="${0 < groupCnt }">
                                    <option value="2">조별과제</option>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                      </select>
                    </div>
                    <div class="flex-col-auto">
                      <label class="checkbox">
                        <input type="checkbox" name="curriculumCommentAt" value="Y" class="table-checkbox"  <c:if test="${homeworkVO.curriculumCommentAt eq 'Y' }">checked</c:if> />
                        <span class="custom-checked"></span>
                        <span class="text">과정후기</span>
                      </label>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="title">과제 오픈일 </th>
                <td>
                  <div class="flex-row">
                    <div class="flex-col-3">
                      <input type="text" class="ell date datepicker" name="openDate" value="${homeworkVO.openDate }" placeholder="오픈일" autocomplete="off">
                    </div>
                    <div class="flex-col-3">
                      <input type="text" class="table-input" name="openTime" value="${fn:substring(homeworkVO.openTime,0,2) }<c:if test="${!empty homeworkVO.openTime }">:</c:if>${fn:substring(homeworkVO.openTime,2,4) }" placeholder="00:00" autocomplete="off">
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="title">과제 마감일 </th>
                <td>
                  <div class="flex-row">
                    <div class="flex-col-3">
                      <input type="text" class="ell date datepicker" name="closeDate" value="${homeworkVO.closeDate }" placeholder="마감일" autocomplete="off">
                    </div>
                    <div class="flex-col-3">
                      <input type="text" class="table-input" name="closeTime" value="${fn:substring(homeworkVO.closeTime,0,2) }<c:if test="${!empty homeworkVO.closeTime }">:</c:if>${fn:substring(homeworkVO.closeTime,2,4) }" placeholder="00:00" autocomplete="off">
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <div>
            <textarea id="nttCn" name="nttCn" rows="20" class="cont" style="width:99%">${homeworkVO.nttCn}</textarea>
          </div>
          <!-- 첨부파일 -->
          <div class="mt-20">
            <c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
                <c:param name="editorId" value="${_EDITOR_ID}"/>
                <c:param name="estnAt" value="N" />
                <c:param name="param_atchFileId" value="${homeworkVO.atchFileId}" />
                <c:param name="imagePath" value="${_IMG }"/>
                <c:param name="regAt" value="Y"/>
                <c:param name="commonAt" value="Y"/>
            </c:import>
          </div>
	      <div class="page-btn-wrap mt-50">
	        <a href="/lms/crcl/homeworkList.do${_BASE_PARAM }" class="btn-xl btn-outline-gray">취소</a>
	        <c:choose>
	           <c:when test="${param.registAction eq 'regist' }"><a class="btn-xl btn-point insertHomework">등록</a></c:when>
	           <c:otherwise><a class="btn-xl btn-point updateHomework">수정</a></c:otherwise>
	        </c:choose>
	      </div>
       </form:form>
   </article>
  </section>
</div>

</div>
</div>
</div>
    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>