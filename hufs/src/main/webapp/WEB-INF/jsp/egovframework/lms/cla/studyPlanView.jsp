<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:param name="plId" value="${param.plId }"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="contentLineAt">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){

});
</script>

         <div class="page-content-header">
            <c:import url="/lms/claHeader.do" charEncoding="utf-8"/>
			<!--
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
        <section class="page-content-body">
        <article class="content-wrap">
              <!-- tab style -->
              <c:if test="${curriculumVO.processSttusCode > 0}">
				<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
					<c:param name="step" value="5"/>
					<c:param name="crclId" value="${curriculumVO.crclId}"/>
					<c:param name="menuId" value="${searchVO.menuId}"/>
					<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
					<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
				</c:import>
			</c:if>
        </article>
		<article class="content-wrap">
              <!-- 사용자 정보 -->
              <div class="card-user-wrap">
                <div class="user-icon"><img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘"></div>
                <div class="user-info">
                  <p class="title">담당교수 <b><c:forEach var="result" items="${facPlList}" varStatus="status">
						<c:out value="${result.userNm}"/>
						<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
						(${result.mngDeptNm})
						<c:set var="userMail" value="${result.facId }"/>
				  </c:forEach></b></p>


                  <p class="sub-title">문의: ${userMail }</p>
                </div>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 수업내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수업내용</div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap size-sm left-align">
                  <colgroup>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                    <col class='bg-gray' style='width:20%'>
                    <col style='width:30%'>
                  </colgroup>
                  <tbody>
                    <tr class="">
                      <td scope='row' class='font-700'>수업일</td>
                      <td>
                      	<c:out value="${scheduleMngVO.startDt}"/>
			  			&nbsp;
			  			<c:out value="${fn:substring(scheduleMngVO.startTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.startTime,2,4)}"/>
			  			~
			  			<c:out value="${fn:substring(scheduleMngVO.endTime,0,2)}"/>:<c:out value="${fn:substring(scheduleMngVO.endTime,2,4)}"/>
                      </td>
                      <td scope='row' class='font-700'>시수</td>
                      <td><c:out value="${scheduleMngVO.sisu}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>장소</td>
                      <td><c:out value="${scheduleMngVO.campusNm}"/></td>
                      <td scope='row' class='font-700'>강의실</td>
                      <td><c:out value="${empty scheduleMngVO.placeDetail ? curriculumVO.campusPlace : scheduleMngVO.placeDetail}"/></td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>수업영역</td>
                      <td colspan='3'>
                      	<c:set var="lang" value="${empty scheduleMngVO.crclLang ? curriculumVO.crclLang : scheduleMngVO.crclLang}"/>
			  			<c:out value="${empty scheduleMngVO.crclLangNm ? curriculumVO.crclLangNm : scheduleMngVO.crclLangNm}"/>
			  			<c:if test="${not empty scheduleMngVO.spLevel}">
			  				> <c:out value="${scheduleMngVO.spLevelNm}"/>
			  			</c:if>
                      </td>
                    </tr>
                    <tr class="">
                      <td scope='row' class='font-700'>수업방법</td>
                      <td colspan='3'>
                      	<c:choose>
			  				<c:when test="${scheduleMngVO.spType eq 'Y' }">
			  					강의식
			  				</c:when>
			  				<c:otherwise>
			  					비강의식
			  				</c:otherwise>
			  			</c:choose>
			  			- <c:out value="${scheduleMngVO.courseNm}"/>
                      </td>
                    </tr>
                    <tr class="evtTr" <c:if test="${scheduleMngVO.courseId ne 'CTG_0000000000000054'}">style="display:none;"</c:if>>
                      <td scope='row' class='font-700'>평가</td>
                      <td colspan='3' class="box_evt">
                      	<c:forEach var="result" items="${evtResultList}">
			  				<c:out value="${result.evtNm}"/>
						</c:forEach>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <!-- <div class="class-info-box mt-40">본 수업은 온라인 강의로 아래 수업내용에 첨부된 수업자료를 모두 확인하여야 출석으로 인정됩니다.</div> -->
              </div>
            </article>
            <hr class="line-hr mb-35">
            <article class="content-wrap">
              <!-- 수업내용 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수업내용</div>
                </div>
              </div>
              <div class="content-body">
                 <c:out value="${scheduleMngVO.spCn}" escapeXml="false"/>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 수업자료 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">수업자료</div>
                   <c:if test="${not empty fileList }">
                  	<p class="desc font-point">수업자료 ${fn:length(fileList)}건을 모두 확인하여야 출석으로 인정됩니다.</p>
                  </c:if>
                </div>
                <c:if test="${not empty fileList }">
	                <div class="class-files-wrap">
	                  <p class="class-files-check">다운로드 <b>${fileDownCnt}/${fn:length(fileList) }</b></p>
	                  <p class="class-files-check">학습진도 <b><fmt:formatNumber value="${fileDownCnt/fn:length(fileList)}" type="percent"/></b></p>
	                </div>
                </c:if>
              </div>
              <div class="content-body">
                <div class="file-attachment-wrap">
                  <div class="file-attachment-view">
                    <div class="inner-area">
                    	<c:forEach var="result" items="${fileList}" varStatus="status">
							 <c:url var="downLoad" value="/cmm/fms/FileDown.do">
								<c:param name="atchFileId" value="${result.atchFileId}"/>
								<c:param name="fileSn" value="${result.fileSn}"/>
								<c:param name="plId" value="${param.plId }"/>
								<c:param name="id" value="${USER_INFO.id }"/>
								<c:param name="streFileNm" value="${result.streFileNm }"/>
							</c:url>
							<a href="${downLoad }" class="attachment icon-file font-gray">
	                       	 <span class="text">${result.orignlFileNm}</span>
	                      	</a>
						</c:forEach>
                    </div>
                  </div>
                </div>
              </div>
            </article>
            <hr class="line-hr mb-35">
            <article class="content-wrap">
              <!-- 학습성과 -->
              <div class="content-header">
                <div class="title-wrap">
                  <div class="title">학습성과</div>
                </div>
              </div>
              <div class="content-body">
                <div class="result-text">
                  <p><c:out value="${scheduleMngVO.spGoal}"/></p>
                </div>
              </div>
            </article>
            <hr class="line-hr">
            <div class="page-btn-wrap mt-50">
            <c:url var="listUrl" value="/sch/selectTodayCrclList.do">
				<c:param name="menuId" value="${searchVO.menuId }"></c:param>
				<c:param name="startDt" value="${scheduleMngVO.startDt }"></c:param>
			</c:url>
              <a href="${listUrl}" class="btn-xl btn-outline-gray">목록으로</a>
            </div>
          </section>
      </div>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>