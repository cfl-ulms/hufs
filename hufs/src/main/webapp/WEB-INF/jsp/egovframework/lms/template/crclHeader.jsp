<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

	<div class="title-wrap">
          <p class="sub-title">
          	<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
				<c:if test="${statusCode.code eq curriculumVO.processSttusCodeDate}">
					<c:out value="${statusCode.codeNm}"/>
					<c:set var="processSttusCodeNm" value="${statusCode.codeNm}"/>
				</c:if>
			</c:forEach>
          </p>
          <h2 class="title"><c:out value="${curriculumVO.crclNm}"/></h2>
          <div class="desc-wrap">
          	<c:if test="${curriculumVO.processSttusCodeDate < 8}">
	            <p class="progress">
	            	${processPerc}% 진행중
	            </p>
            </c:if>
            <c:choose>
                <c:when test="${param.dateFlag eq 'curriculumrequest' }">
                    <p class="date">수강신청기간  : ${curriculumVO.applyStartDate}~${curriculumVO.applyEndDate}</p>
                </c:when>
                <c:otherwise>
                    <p class="date">과정기간 : ${curriculumVO.startDate}~${curriculumVO.endDate}</p>
                </c:otherwise>
            </c:choose>
          </div>
    </div>
    
    <%-- 과정관리 바로가기 버튼 --%>
    <c:if test="${param.curriculumManageBtnFlag eq 'Y' and managerAt eq 'Y'}">
        <div class="util-wrap">
            <a href="/lms/manage/schedule.do?crclId=${curriculumVO.crclId}&menuId=MNU_0000000000000062&step=4" class="btn-md btn-outline icon-arrow">과정관리</a>
        </div>
    </c:if>
    <c:if test="${param.pdfFlag eq 'Y'}">
    	<!-- TO DO
        <div class="util-wrap">
            <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
        </div>
         -->
    </c:if>