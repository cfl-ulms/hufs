<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="?">
	<c:param name="menuId" value="${param.menuId}" />
	<c:if test="${not empty searchVO.searchCtgryId}"><c:param name="searchCtgryId" value="${searchVO.searchCtgryId}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclbId}"><c:param name="searchCrclbId" value="${searchVO.searchCrclbId}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclId}"><c:param name="searchCrclId" value="${searchVO.searchCrclId}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCode}"><c:param name="searchProcessSttusCode" value="${searchVO.searchProcessSttusCode}" /></c:if>
	<c:if test="${not empty searchVO.searchSysType}">
		<c:forEach items="${searchVO.searchSysType}" var="result">
			<c:param name="searchSysType" value="${result}" />
		</c:forEach>
	</c:if>
	<c:if test="${not empty searchVO.searchKeyWord}"><c:param name="searchKeyWord" value="${searchVO.searchKeyWord}" /></c:if>
</c:url>

<script>

$(document).ready(function(){
	$(document).on("click", "#searchReset", function() {
	    $("form[id='frm']").find("input").not("#resetBtn").val("");
	    $("form[id='frm'] select").val("").trigger("change");
	    $("form[id='frm'] input").prop("checked", false);
	});
});

function fnSearch(){
	$("#frm").submit();
}
</script>
<section class="page-content-body">
            <article class="content-wrap">
              <!-- 게시판 검색영역 -->
              <div class="box-wrap mb-40">
              <form id="frm" method="post" action="<c:url value="/lms/cla/curBoardList.do"/>">
            	  <input type="hidden" id="pageIndex" name="pageIndex" value="${paginationInfo.currentPageNo }"/>
            	  <input type="hidden" name="menuId" value="${param.menuId }" />

                <h3 class="title-subhead">과정 게시판 검색</h3>
                <div class="flex-row-ten">
                   <div class="flex-ten-col-3 mb-20">
                    <div class="ell">
                      <select class="select2" data-select="style3" data-placeholder="언어 선택">
                        <option value=""></option>
                        <option value="0">몽골어</option>
                        <option value="1">스와힐리어</option>
                        <option value="2">우즈베크어</option>
                        <option value="3">이란어</option>
                        <option value="4">말레이인도네시아어</option>
                        <option value="5">터키어</option>
                        <option value="6">태국어</option>
                        <option value="7">포르투갈/브라질어</option>
                        <option value="8">헝가리어</option>
                        <option value="9">폴란드어</option>
                        <option value="10">힌디어</option>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-6 mb-20">
                    <div class="ell">
                      <select name="" id="" class="select2" data-select="style3" >
                        <option value="">과정상태 전체</option>
                        <c:forEach var="result" items="${statusComCode}" varStatus="status">
							<option value="${result.code}" ${searchVO.searchProcessSttusCode eq result.code ? 'selected' : '' }>${result.codeNm}</option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <select name="" id="" class="select2" data-select="style3">
                        <option value="">과정명 전체</option>
                        <c:forEach var="result" items="${curriculumList}" varStatus="status">
							<option value="${result.crclId}" ${searchVO.searchCrclId eq result.crclId ? 'selected' : '' }><c:out value="${result.crclNm}"/></option>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="flex-ten-col-7 mb-20">
                    <div class="ell">
                      <input type="text" name="searchKeyWord" value="${searchVO.searchKeyWord }" placeholder="게시판 명">
                    </div>
                  </div>

                  <c:forEach items="${searchVO.searchSysType}" var="result">
					<c:choose>
						<c:when test="${result eq 'ALL' }">
							<c:set var="searchSysType1" value="ALL"/>
						</c:when>
						<c:when test="${result eq 'GROUP' }">
							<c:set var="searchSysType2" value="GROUP"/>
						</c:when>
						<c:otherwise>
							<c:set var="searchSysType3" value="CLASS"/>
						</c:otherwise>
					</c:choose>
				</c:forEach>
                  <div class="flex-ten-col-6 flex align-items-center">
                    <label class="checkbox">
                      <input type="checkbox" name="searchSysType" value="ALL" ${searchSysType1 eq 'ALL' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">전체</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchSysType" value="GROUP" ${searchSysType2 eq 'GROUP' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">조별</span>
                    </label>
                    <label class="checkbox">
                      <input type="checkbox" name="searchSysType" value="CLASS" ${searchSysType3 eq 'CLASS' ? 'checked' : '' }>
                      <span class="custom-checked"></span>
                      <span class="text">반별</span>
                    </label>
                  </div>
                </div>
                <button type="button" class="btn-sm font-400 btn-point mt-20" onclick="fnSearch()">검색</button>
                <button type="button" class="btn-sm font-400 btn-outline mt-20" id="searchReset">초기화</button>
              </form>
              </div>
            </article>
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:10%'>
                  <col style='width:13%'>
                  <col style='width:100px'>
                  <col style='width:10%'>
                  <col style='width:21%'>
                  <col style='width:26%'>
                  <col style='width:100px'>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700'>
                    <th scope='col'>No</th>
                    <th scope='col'>과정상태</th>
                    <th scope='col'>과정명</th>
                    <th scope='col'>과정기간</th>
                    <th scope='col'>사용자 그룹</th>
                    <th scope='col'>게시판명</th>
                    <th scope='col'>최근 게시물</th>
                    <th scope='col'>등록일</th>
                  </tr>
                </thead>
                <tbody>
                	<c:if test="${not empty boardList}">
                		<c:forEach var="result" items="${boardList}" varStatus="status">
		                  <tr onclick="location.href='../class/staff_class.2.1.13.html'" class=" cursor-pointer">
            			    <td scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
		                    <td>${result.codeNm }</td>
		                    <td class='left-align'>${result.crclNm}</td>
		                    <td class='keep-all'>${result.startDate} ~ ${result.endDate}</td>
		                    <td>${result.sysTyCode eq 'ALL' ? '전체' : result.sysTyCode eq 'GROUP' ? '조별' : '반별'}</td>
		                    <td class='left-align'>(${result.sysTyCode eq 'ALL' ? '전체' : result.sysTyCode eq 'GROUP' ? '조별' : '반별'}) ${result.bbsNm}</td>
		                    <td class='left-align'>${result.nttSj}</td>
		                    <td class='keep-all'>${fn:substring(result.frstRegisterPnttm,0,10)}</td>
		                  </tr>
	                  </c:forEach>
                  </c:if>
                </tbody>
              </table>

				<c:if test="${not empty boardList}">
	              <div class="pagination center-align mt-60">
	               <div class="pagination-inner-wrap overflow-hidden inline-block">
						<c:url var="startUrl" value="/lms/cla/curBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="1" />
		   				</c:url>
		               	<button class="start goPage" data-url="${startUrl}"></button>

		               	<c:url var="prevUrl" value="/lms/cla/curBoardList.do${_BASE_PARAM}">
							<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
						</c:url>
		               	<button class="prev goPage" data-url="${prevUrl}"></button>

		               	<ul class="paginate-list f-l overflow-hidden">
		                 	<c:url var="pageUrl" value="/lms/cla/curBoardList.do${_BASE_PARAM}"/>
							<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
							<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
		               	</ul>

		               	<c:url var="nextUrl" value="/lms/cla/curBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
		   				</c:url>
		               	<button class="next goPage" data-url="${nextUrl}"></button>

		               	<c:url var="endUrl" value="/lms/cla/curBoardList.do${_BASE_PARAM}">
		  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
		   				</c:url>
		               	<button class="end goPage" data-url="${endUrl}"></button>
					</div>
	              </div>
              </c:if>
            </article>
          </section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>