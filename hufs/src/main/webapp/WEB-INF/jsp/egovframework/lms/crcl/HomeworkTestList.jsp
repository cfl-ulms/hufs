<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${param.step}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<link rel="stylesheet" href="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.min.css?v=2">
<script src="${CML}/lib/sly-master/sly.min.js?v=1"></script>
<script src="${CML}/lib/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js?v=1"></script>

<script>
$(document).ready(function(){
	//초기 값 설정
	$(".table-inside-input").val($("input[name=hwIdList]:checked").length);
	
	//반 선택
	$(".selClass").change(function(){
		var clas = $(this).val();
		
		$("input[name=searchClassCnt]").val(clas);
		$("#searchForm").submit();
	});
	
	//저장
	$("#btn-reg").click(function(){
		$("#frm").submit();
	});
});
</script>

<div class="page-content-header">
  <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
      <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
  </c:import>
  <!-- 
  <div class="util-wrap">
    <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
  </div>
   -->
</div>
<section class="page-content-body">            
            <article class="content-wrap">
                <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
                    <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                        <c:param name="step" value="${param.step }"/>
                        <c:param name="crclId" value="${curriculumVO.crclId}"/>
                        <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                        <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                    </c:import>
                </c:if>
            </article>
            <article class="content-wrap">
              <!-- 페이지 탭메뉴 -->
              <div class="content-header">
                <div class="class-tab-wrap">                  
                  <a href="/lms/crcl/homeworkList.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=8&thirdtabstep=1" class="title <c:if test="${param.thirdtabstep eq '1' }">on</c:if>">과제</a>
                  <a href="/lms/crcl/homeworkTestList.do?menuId=MNU_0000000000000062&crclId=<c:out value="${param.crclId}"/>&step=8&thirdtabstep=2" class="title <c:if test="${param.thirdtabstep eq '2' }">on</c:if>">과제평가</a>
                </div>
              </div>
            </article>
            
            <article class="content-wrap">
            	<form name="frm" id="searchForm" method="post" action="<c:url value="/lms/crcl/homeworkTestList.do"/>">
					<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
					<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
					<input type="hidden" name="step" value="${searchVO.step}"/>
					<input type="hidden" name="thirdtabstep" value="${searchVO.thirdtabstep}"/>
					<input type="hidden" name="searchClassCnt" value="${searchVO.searchClassCnt}"/>
					
		            <div class="box-wrap mb-40">
		                <h3 class="title-subhead">학생 검색</h3>
		                <div class="flex-row-ten">
		                  <%-- 
		                  <div class="flex-ten-col-3">
		                    <div class="ell">
		                      <select name="" id="" class="select2" data-select="style3" data-placeholder="검색 유형">
		                        <option value=""></option>
		                        <option value="0">검색 유형</option>
		                      </select>
		                    </div>
		                  </div>
		                   --%>
		                  <div class="flex-ten-col-10">
		                    <div class="ell">
		                      <input type="text" name="searchStudentUserNm" value="${searchVO.searchStudentUserNm}" placeholder="학생 이름을 검색해보세요">
		                    </div>
		                  </div>
		                </div>
		
		                <button class="btn-sm font-400 btn-point mt-20">검색</button>
		            </div>
              	</form>
            </article>
            
            <form name="frm" id="frm" method="post" action="<c:url value="/lms/crcl/homeworkTestUpdate.do"/>">
            	<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
				<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
				<input type="hidden" name="step" value="${searchVO.step}"/>
				<input type="hidden" name="thirdtabstep" value="${searchVO.thirdtabstep}"/>
				<input type="hidden" name="searchClassCnt" value="${searchVO.searchClassCnt}"/>
					
	            <article class="content-wrap">
	              <div class="content-header">
	                <div class="feedback-title-wrap">
	                  <select class="select2 selClass" data-select="style3" data-placeholder="반 선택">
	                    <option value=""></option>
	                    <option value="0">전체</option>
	                    <c:forEach var="result" items="${selectGroupList}">
	                    	<option value="<c:out value="${result.classCnt}"/>" <c:if test="${result.classCnt eq searchVO.searchClassCnt}">selected="selected"</c:if>><c:out value="${result.classCnt}"/>반</option>
	                    </c:forEach>
	                  </select>
	                  <%-- 
	                  <a href="#" class="btn-outline btn-md btnModalOpen" data-modal-type="excel_register">엑셀일괄등록</a>
	                  <a href="#" class="btn-outline btn-md">성적다운로드</a>
	                   --%>
	                </div>
	                <%-- <p class="notice">* 성적의 최초 등록은 엑셀일괄등록만 가능합니다.</p> --%>
	              </div>
	              <div class="tbl">
	                <div class="tbl-left">
	                  <!-- 테이블영역-->
	                  <table class="common-table-wrap p-5">
	                    <colgroup>
	                      <col>
	                      <col>
	                      <col>
	                      <col>
	                      <col class='bg-point-light'>
	                      <col class='bg-point-light'>
	                      <col class='bg-point-light'>
	                    </colgroup>
	                    <thead>
	                      <tr class='bg-gray font-700'>
	                        <th scope='colgroup' rowspan='3' colspan='4'>학생정보</th>
	                        <th scope='colgroup' colspan='3'>과제마감일</th>
	                      </tr>
	                      <tr class='bg-gray font-700'>
	                        <th scope='colgroup' colspan='3'>과제명</th>
	                      </tr>
	                      <tr class='bg-gray font-700'>
	                        <th scope='colgroup' colspan='3'>성적반영 <input type='text' class='table-inside-input' value='0' /></th>
	                      </tr>
	                      <tr class='bg-gray font-700'>
	                        <th scope='col'>소속</th>
	                        <th scope='col'>생년월일</th>
	                        <th scope='col'>이름</th>
	                        <th scope='col'>조</th>
	                        <th scope='col'>변환점수</th>
	                        <th scope='col'>총점</th>
	                        <th scope='col'>석차</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    	<c:forEach var="result" items="${selectStudentList}" varStatus="stauts">
	                    		<tr class="">
			                        <td scope='row' class='title'>
			                          <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.mngDeptNm}"/></span></div>
			                        </td>
			                        <td><c:out value="${result.brthdy}"/></td>
			                        <td><c:out value="${result.userNm}"/></td>
			                        <td><c:out value="${result.groupCnt}"/>조</td>
			                        <td><c:out value="${fn:replace(result.chScr,'.0','')}"/></td>
			                        <td><c:out value="${fn:replace(result.totScr,'.0','')}"/></td>
			                        <td><c:out value="${result.rk}"/></td>
			                     </tr>
	                    	</c:forEach>
	                    </tbody>
	                  </table>
	                </div>
	                <div class="tbl-main-wrap">
	                  <div class="tbl-main">
	                    <!-- 테이블영역-->
	                    <table class="common-table-wrap p-5">
	                      <thead>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${homeworkList}" varStatus="status">
	                        		<th scope='colgroup' colspan='2'><c:out value="${result.closeDate}"/></th>
	                        	</c:forEach>
	                        </tr>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${homeworkList}" varStatus="status">
	                        		<th scope='colgroup' class='title' colspan='2'>
			                            <div class="inner-wrap"><span class='text dotdotdot'><c:out value="${result.nttSj}"/></span></div>
			                        </th>
	                        	</c:forEach>
	                        </tr>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${homeworkList}" varStatus="status">
	                        		<th scope='colgroup' colspan='2'>
	                        			<label class='checkbox table-inside-checkbox'>
	                        				<input type='checkbox' name='hwIdList' value="${result.hwId}" <c:if test="${result.scoreApplyAt eq 'Y'}">checked="checked"</c:if>><span class='custom-checked'></span>
	                        			</label>
	                        		</th>
	                        	</c:forEach>
	                        </tr>
	                        <tr class='bg-light-gray font-700'>
	                        	<c:forEach var="result" items="${homeworkList}" varStatus="status">
	                        		<th scope='colgroup'>총점</th>
	                          		<th scope='colgroup'>석차</th>
	                        	</c:forEach>
	                        </tr>
	                      </thead>
	                      <tbody>
	                      	<c:forEach var="stuList" items="${selectStudentList}">
	                      		<tr>
		                      		<c:forEach var="result" items="${scoreList}" varStatus="status">
		                      			<c:if test="${stuList.userId eq result.userId}">
		                      				<td><c:out value="${result.scr}"/></td>
	                          				<td><c:out value="${result.rk}"/></td>
		                      			</c:if>
		                      		</c:forEach>
	                      		</tr>
	                      	</c:forEach>
	                      </tbody>
	                    </table>
	                  </div>
	                </div>
	              </div>
	            </article>
            
	            <div class="page-btn-wrap mt-50">
	              <a href="#" id="btn-reg" class="btn-xl btn-point">저장</a>
	            </div>
            </form>
          </section>
        </div>

</div>
</div>
</div>

    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>