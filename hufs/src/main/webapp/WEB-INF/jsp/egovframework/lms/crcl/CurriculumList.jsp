<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
	<c:if test="${not empty searchVO.searchDivision}"><c:param name="searchDivision" value="${searchVO.searchDivision}" /></c:if>
	<c:if test="${not empty searchVO.searchControl}"><c:param name="searchControl" value="${searchVO.searchControl}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchProjectAt}"><c:param name="searchProjectAt" value="${searchVO.searchProjectAt}" /></c:if>
	<c:if test="${not empty searchVO.searchAprvalAt}"><c:param name="searchAprvalAt" value="${searchVO.searchAprvalAt}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
	<c:if test="${not empty searchVO.searchHostCode}"><c:param name="searchHostCode" value="${searchVO.searchHostCode}" /></c:if>
	<c:if test="${not empty searchVO.searchUserNm}"><c:param name="searchUserNm" value="${searchVO.searchUserNm}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclYear}"><c:param name="searchCrclYear" value="${searchVO.searchCrclYear}" /></c:if>
	<c:if test="${not empty searchVO.searchCrclTerm}"><c:param name="searchCrclTerm" value="${searchVO.searchCrclTerm}" /></c:if>
	<c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
	<c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
	<c:if test="${not empty searchVO.searchTargetDetail}"><c:param name="searchTargetDetail" value="${searchVO.searchTargetDetail}" /></c:if>
	<c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
	<c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
	<c:if test="${not empty searchVO.myCurriculumPageFlag}"><c:param name="myCurriculumPageFlag" value="${searchVO.myCurriculumPageFlag}" /></c:if>
	<c:if test="${not empty searchVO.searchTotalTimeAt}"><c:param name="searchTotalTimeAt" value="${searchVO.searchTotalTimeAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
	//과정체계
	var acaDepth2 = [],
		acaDepth3 = [];
	
	<c:forEach var="result" items="${sysCodeList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			acaDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
		<c:if test="${result.ctgryLevel eq '3'}">
			acaDepth3.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>
	
	$("#ctgryId1").change(function(){
		var ctgryId1 = $(this).val(),
			option = "<option value=''>중분류</option>",
			option2 = "<option value=''>소분류</option>";
			
		$("#ctgryId2").html(option);
		$("#ctgryId3").html(option2);
		for(i = 0; i < acaDepth2.length; i++){
			if(acaDepth2[i].upperCtgryId == ctgryId1){
				var searchSysCode2 = "${searchVO.searchSysCode2}";
				if(searchSysCode2 == acaDepth2[i].ctgryId){
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"' selected='selected'>"+acaDepth2[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId2").append("<option value='"+acaDepth2[i].ctgryId+"'>"+acaDepth2[i].ctgryNm+"</option>");
				}
				
			}
		}
		$("#ctgryId2").change();
	});
	
	$("#ctgryId2").change(function(){
		var ctgryId2 = $(this).val(),
			option = "<option value=''>소분류</option>";
			
		$("#ctgryId3").html(option);
		for(i = 0; i < acaDepth3.length; i++){
			if(acaDepth3[i].upperCtgryId == ctgryId2){
				var searchSysCode3 = "${searchVO.searchSysCode3}";
				if(searchSysCode3 == acaDepth3[i].ctgryId){
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"' selected='selected'>"+acaDepth3[i].ctgryNm+"</option>");
				}else{
					$("#ctgryId3").append("<option value='"+acaDepth3[i].ctgryId+"'>"+acaDepth3[i].ctgryNm+"</option>");
				}
			}
		}
		$("#ctgryId3").change();
	});
	
	$("#ctgryId3").change(function(){
		var sysCode3 = $(this).val(),
			optionHtml = "<option value=''>기본과정명</option>";
		
		$.ajax({
			url : "/mng/lms/crclb/CurriculumbaseList.json"
			, type : "post"
			, dataType : "json"
			, data : {searchSysCode3 : sysCode3}
			, success : function(data){
				var searchcrclbId = "${searchVO.searchcrclbId}";
				if(data.successYn == "Y"){
					$.each(data.items, function(i){
						if(data.items[i].crclbId == searchcrclbId){
							optionHtml += "<option value='"+data.items[i].crclbId+"' selected='selected'>"+data.items[i].crclbNm+"</option>"
						}else{
							optionHtml += "<option value='"+data.items[i].crclbId+"'>"+data.items[i].crclbNm+"</option>"
						}
						
					});
					
					$("#searchcrclbId").html(optionHtml);
				}
			}, error : function(){
				alert("error");
			}
		});
		
	});
	
	$("#ctgryId1").change();
});
</script>

<section class="page-content-body">
			<form name="frm" method="post" action="<c:url value="/lms/crcl/CurriculumList.do"/>">
				<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
				<article class="content-wrap">
	              <div class="box-wrap mb-40">
	                <h3 class="title-subhead">과정관리 검색</h3>
	                <div class="flex-row-ten">
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                    	<select id="searchCrclYear" name="searchCrclYear" class="select2" data-select="style3" data-placeholder="년도">
	                    		<option value="">선택</option>
								<c:forEach var="result" items="${yearList}" varStatus="status">
					  				<option value="${yearList[status.index] }" <c:if test="${yearList[status.index] eq searchVO.searchCrclYear}">selected="selected"</c:if>>${yearList[status.index] }</option>
					  			</c:forEach>
							</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="searchCrclTerm" name="searchCrclTerm" class="select2" data-select="style3" data-placeholder="학기">
								<option value="">학기 전체</option>
								<c:forEach var="result" items="${crclTermList}" varStatus="status">
									<c:if test="${result.ctgryLevel eq '1'}">
										<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchCrclTerm}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
									</c:if>
								</c:forEach>
							</select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 mb-20">
	                    <div class="desc">
	                      <input type="text" name="searchStartDate" value="${searchVO.searchStartDate}" id="searchStartDate" class="ell date datepicker type2" placeholder="교육 시작일" readonly="readonly"/>
	                      <i>~</i>
	                      <input type="text" name="searchEndDate" value="${searchVO.searchEndDate}" id="searchEndDate" class="ell date datepicker type2" placeholder="교육 종료일" readonly="readonly"/>
	                    </div>
	
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <select id="searchProcessSttusCodeDate" name="searchProcessSttusCodeDate" class="select2" data-select="style3" data-placeholder="과정상태 전체">
							<option value="">과정상태 전체</option>
							<c:forEach var="result" items="${statusComCode}" varStatus="status">
								<option value="${result.code}" <c:if test="${result.code eq searchVO.searchProcessSttusCodeDate}">selected="selected"</c:if>>${result.codeNm}</option>
							</c:forEach>
						  </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3 mb-20">
	                    <div class="ell">
	                      <input type="text" name="searchUserNm" value="${searchVO.searchUserNm}" placeholder="책임교수 명">
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 mb-20">
	                    <div class="ell">
	                      <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" placeholder="과정명">
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select id="targetType" name="searchTargetType" class="select2" data-select="style3" data-placeholder="대상 전체">
	                        <option value="">대상 선택</option>
							<option value="Y" <c:if test="${'Y' eq searchVO.searchTargetType}">selected="selected"</c:if>>본교생</option>
							<option value="N" <c:if test="${'N' eq searchVO.searchTargetType}">selected="selected"</c:if>>일반</option>
	                      </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-3">
	                    <div class="ell">
	                      <select name="searchTargetDetail" id="searchTargetDetail" class="select2" data-select="style3" data-placeholder="과정유형 전체">
	                        <option value="">선택</option>
	                        <c:forEach var="result" items="${targetList}" varStatus="status">
								<c:if test="${result.ctgryLevel eq '1'}">
									<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq searchVO.searchTargetDetail}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
								</c:if>
							</c:forEach>
	                      </select>
	                    </div>
	                  </div>
	                  <div class="flex-ten-col-4 flex align-items-center">
	                    <label class="checkbox">
	                      <input type="checkbox" name="searchProjectAt" value="Y" <c:if test="${searchVO.searchProjectAt eq 'Y'}">checked="checked"</c:if>/>
	                      <span class="custom-checked"></span>
	                      <span class="text">프로젝트 과정</span>
	                    </label>
	                    <label class="checkbox">
	                      <input type="checkbox" name="searchTotalTimeAt" value="Y" <c:if test="${searchVO.searchTotalTimeAt eq 'Y'}">checked="checked"</c:if>/>
	                      <span class="custom-checked"></span>
	                      <span class="text">총 시수 과정</span>
	                    </label>
	                    <label class="checkbox">
	                      <input type="checkbox" name="myCurriculumPageFlag" value="teacher" <c:if test="${searchVO.myCurriculumPageFlag eq 'teacher'}">checked="checked"</c:if>>
	                      <span class="custom-checked"></span>
	                      <span class="text">나의 과정</span>
	                    </label>
	                  </div>
	                </div>
	                
	                <button type="submit" class="btn-sm font-400 btn-point mt-20">검색</button>
	                <button type="reset" class="btn-sm font-400 btn-outline mt-20">초기화</button>
	              </div>
	            </article>
            </form>
            
            <article class="content-wrap">
              <!-- 테이블영역-->
              <table class="common-table-wrap table-type-board">
                <colgroup>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <col style='width:12%'>
                  <col style='width:12%'>
                  <col style='width:25%'>
                  <col style='width:8%'>
                  <col style='width:8%'>
                  <col style='width:7%'>
                  <col style='width:7%'>
                  <%-- <col style='width:7%'> --%>
                </colgroup>
                <thead>
                  <tr class='bg-light-gray font-700 keep-all'>
                    <th scope='col'>No</th>
                    <th scope='col'>년도</th>
                    <th scope='col'>학기</th>
                    <th scope='col'>과정상태</th>
                    <th scope='col'>교육기간</th>
                    <th scope='col'>과정명</th>
                    <th scope='col' colspan='2'>대상</th>
                    <th scope='col' class='keep-all'>프로젝트 과정</th>
                    <th scope='col' class='keep-all'>총 시수 과정</th>
                    <!-- <th scope='col' class='keep-all'>사업비 (원)</th> -->
                  </tr>
                </thead>
                <tbody>
                	<c:forEach var="result" items="${resultList}" varStatus="status">
			    		<c:url var="viewUrl" value="/lms/crcl/selectCurriculum.do${_BASE_PARAM}">
							<c:param name="crclId" value="${result.crclId}"/>
							<c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
						</c:url>
			    		<tr onclick="location.href='${viewUrl}'">
			    			<c:choose>
                                <c:when test="${result.processSttusCodeDate eq 2 }"><c:set var="processSttusColor" value="font-green"/></c:when>
                                <c:when test="${result.processSttusCodeDate eq 3 or result.processSttusCodeDate eq 6 }"><c:set var="processSttusColor" value="font-orange"/></c:when>
                                <c:when test="${result.processSttusCodeDate eq 7 }"><c:set var="processSttusColor" value="font-blue"/></c:when>
                                <c:otherwise><c:set var="processSttusColor" value=""/></c:otherwise>
                            </c:choose>
			    			<td><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
			    			<td><c:out value="${result.crclYear}"/></td>
			    			<td><c:out value="${result.crclTermNm}"/></td>
			    			<td class="${processSttusColor }">
			    				<c:forEach var="statusCode" items="${statusComCode}" varStatus="status">
			    					<c:if test="${statusCode.code eq result.processSttusCodeDate}"><c:out value="${statusCode.codeNm}"/></c:if>
								</c:forEach>
			    			</td>
			    			<td><c:out value="${result.startDate}"/> ~ <br/> <c:out value="${result.endDate}"/></td>
			    			
			    			<td><c:out value="${result.crclNm}"/></td>
			    			<td>
			    				<c:choose>
			    					<c:when test="${result.targetType eq 'Y'}">본교생</c:when>
			    					<c:otherwise>일반</c:otherwise>
			    				</c:choose>
			    			</td>
			    			<td><c:out value="${result.targetDetailNm}"/></td>
			    			<td>
		                    	<c:choose>
		                    		<c:when test="${result.projectAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
		                    </td>
		                    <td>
								<c:choose>
		                    		<c:when test="${result.totalTimeAt eq 'Y'}">
		                    			<img class='vertical-mid' src='/template/lms/imgs/common/icon_table_circle.png'>
		                    		</c:when>
		                    		<c:otherwise>-</c:otherwise>
		                    	</c:choose>
							</td>
			    			<%-- 
			    			<td>
			    				<c:choose>
			    					<c:when test="${result.totAmount eq '0' or empty result.totAmount}">무료</c:when>
			    					<c:otherwise><fmt:formatNumber type="number" maxFractionDigits="3" value="${result.totAmount}" />원</c:otherwise>
			    				</c:choose>
			    			</td>
			    			 --%>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(resultList) == 0}">
						<tr>
				        	<td class="listCenter" colspan="10"><spring:message code="common.nodata.msg" /></td>
				      	</tr>
				    </c:if>
                </tbody>
              </table>
              <div class="pagination center-align mt-60">
				<div class="pagination-inner-wrap overflow-hidden inline-block">
					<c:url var="startUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="1" />
	   				</c:url>
	               	<button class="start goPage" data-url="${startUrl}"></button>
	               
	               	<c:url var="prevUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
						<c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
					</c:url>
	               	<button class="prev goPage" data-url="${prevUrl}"></button>
	               
	               	<ul class="paginate-list f-l overflow-hidden">
	                 	<c:url var="pageUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}"/>
						<c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
						<ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	               	</ul>
	               
	               	<c:url var="nextUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	   				</c:url>
	               	<button class="next goPage" data-url="${nextUrl}"></button>
	               
	               	<c:url var="endUrl" value="/lms/crcl/CurriculumList.do${_BASE_PARAM}">
	  					<c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	   				</c:url>
	               	<button class="end goPage" data-url="${endUrl}"></button>
				</div>
			</div>
            </article>
          </section>
        </div>

</div>
</div>
</div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>