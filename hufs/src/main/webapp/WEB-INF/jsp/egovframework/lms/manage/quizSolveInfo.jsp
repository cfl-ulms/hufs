<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizRegView.do"/>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
</c:url>
<% /*URL 정의*/ %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>CFL 특수외국어교육진흥사업 한국외국어대학교</title>
	    
	    <meta name="title" content="CFL 특수외국어교육진흥사업 한국외국어대학교">
	    <meta name="description" content="CFL 특수외국어교육진흥사업 한국외국어대학교">
	    <meta name="keywords" content="CFL 특수외국어교육진흥사업 한국외국어대학교">
	    
	    <meta property="og:type" content="website">
	    <meta property="og:title" content="">
	    <meta property="og:description" content="">
	    <meta property="og:image" content="">
	    <meta property="og:url" content="">
	    
	    <!--=================================================
      	파비콘
  		==================================================-->
 	 	<link rel="shortcut icon" href="">

        <!--=================================================
              공통 스타일시트
        ==================================================-->
        <link href="/template/lms/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
        <link rel="stylesheet" href="/template/lms/lib/select2/select2.min.css"><!-- select2 -->
        <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
        <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
        
        <link rel="stylesheet" href="/template/lms/css/quiz/base.css?v=3">
   	    <!--=================================================
	       페이지별 스타일시트
	    ==================================================-->
	    <c:choose>
			<c:when test="${USER_INFO.userSeCode eq '08' }">
		      	<link rel="stylesheet" href="/template/lms/css/quiz/common_staff.css?v=3">
		        <link rel="stylesheet" href="/template/lms/css/common/board_staff.css?v=3">
				<link rel="stylesheet" href="/template/lms/css/quiz/quiz_staff.css?v=3">
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="/template/lms/css/quiz/common.css?v=3">
		        <link rel="stylesheet" href="/template/lms/css/quiz/board.css?v=3">
				<link rel="stylesheet" href="/template/lms/css/quiz/quiz.css?v=3">
			</c:otherwise>
		</c:choose>
		
		  <!--=================================================
		          공통 스크립트
		  ==================================================-->
		  <script src="/template/lms/lib/sly-master/sly.min.js"></script>
		  
		  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
		  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
		  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
		  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
		  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
		  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
		  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
		  <!--daterangepicker -->
		
		  <script src="/template/lms/js/common.js?v=1"></script>
		  <script src="/template/common/js/common.js"></script>
		  <!--sly -->
		
			<script>
				$(document).ready(function(){
					$("#notSubCnt").hide();
					
					<c:if test="${closeYn eq 'Y'}">
						fn_closeWindow();
					</c:if>
				});
				
				function fn_pre_quiz(){
					var quizStatus = "${resultList[0].quizEnd}",
						userSe = "${USER_INFO.userSeCode}";
					
					if(quizStatus == "P" || userSe == "08"){
						$("#quizStart").attr("action", "/lms/quiz/PreQuizList.do");
						$("#quizStart").submit();
					}else{
						alert("퀴즈가 아직 시작되지 않았습니다.");
					}
				}
				
				//이전으로 - 교원
				function fn_prev_page(){
					var quizSort = $("#quizSort").val() - 2;
					
			        $("#quizSort").val(quizSort);
					$("#quizInfo").attr("action", "/lms/quiz/PreQuizList.do");
					$("#quizInfo").submit();
				}
				
				function fn_next_page(){
			        //답안선택한 값 지정 - 복수로 선택가능
			        var radioVal = "",
			        	leng = $('input[name="answer"]:checked').length;
			        
			        <c:choose>
						<c:when test="${USER_INFO.userSeCode eq '08' }">
							$("#quizInfo").attr("action", "/lms/quiz/PreQuizList.do");
							$("#quizInfo").submit();
						</c:when>
						<c:otherwise>
							$('input[name="answer"]:checked').each(function(i){
					        	radioVal += "," + $(this).val();
					        	
					        	if(leng == (i + 1)){
					        		radioVal = radioVal.replace(",","");
					        		$("#answerNum").val(radioVal);
					        		
					        		//정답여부지정
							        if(radioVal == '${quiz.answerNum}'){
							        	$("#answerAt").val("Y");
							        }else{
							        	$("#answerAt").val("N");
							        }
							        
									$("#quizInfo").attr("action", "/lms/quiz/PreQuizList.do");
									$("#quizInfo").submit();
					        	}
					        });
						</c:otherwise>
					</c:choose>
			        
				} 
				
				function fn_submit(quizSort){
					//답안선택한 값 지정
					var radioVal = $('input[name="answer"]:checked').val();
			        $("#answerNum").val(radioVal);
			        
			        //정답여부지정
			        if(radioVal == '${quiz.answerNum}'){
			        	$("#answerAt").val("Y");
			        }else{
			        	$("#answerAt").val("N");
			        }
					
					
					$("#quizSort").val(quizSort);
					$("#submitAt").val("Y");
					$("#quizInfo").attr("action", "/lms/quiz/PreQuizList.do");
					$("#quizInfo").submit();
				}
				
				function fn_closeWindow() {
					window.close();
			    }  
				
				function fn_doNotSubmit(){
					$("#unsubmitted_modal").css("display", "block");
				}
				
				function fn_subList(arg){
					if(arg == "1"){
						$("#subTab2").removeClass("tab-item active");
						$("#subTab2").addClass("tab-item");
						$("#subTab1").removeClass("tab-item");
						$("#subTab1").addClass("tab-item active");

						$("#subCnt").show();
						$("#notSubCnt").hide();
					}else{
						$("#subTab1").removeClass("tab-item active");
						$("#subTab1").addClass("tab-item");
						$("#subTab2").removeClass("tab-item");
						$("#subTab2").addClass("tab-item active");
						
						$("#notSubCnt").show();
						$("#subCnt").hide();
					}
					
				}
				
				function fn_selectAnswerView(){
					$("#answerDiv1").css("display", "none");
					$("#answerDiv2").css("display", "block");
				}
				
				
				function fn_end(){
					<c:choose>
						<c:when test="${USER_INFO.userSeCode eq '08' }">
							if(confirm("퀴즈를 종료하시겟습니까?\n결시자가 있는 경우 해당 학생은 퀴즈를 풀 수 없으며 0점 처리됩니다.")){
								window.opener.location.reload();
								
								$("#quizInfo").attr("action", "/lms/quiz/quizEnd.do");
								$("#quizInfo").submit();
							}
						</c:when>
						<c:otherwise>
							//window.opener.location.reload();
							$("#quizInfo").attr("action", "/lms/quiz/quizEnd.do");
							$("#quizInfo").submit();
						</c:otherwise>
					</c:choose>
				}
			</script>
</head>
<body class="depth2">
  <header class="header">
    <div class="header-bar ">
      <div class="header-title"><h1 class="title">평가</h1></div>
      <c:if test="${searchVO.quizSort > 1 and status eq 'ing'}">
      	<div class="left-area"><a href="javascript:history.back();" class="btn-header-back"></a></div>
      </c:if>
      <div class="right-area">
      </div>
    </div>
  </header>
  <section class="page-content-wrap">
    <div class="page-content-header">
      <article class="content-wrap-md center-align">
        <div class="area">
          <div class="content-header">
            <div class="common-title-wrap">
              <p class="sub-title font-point">총 ${totCnt }문항</p>
              <c:forEach var="result" items="${resultList}" varStatus="status" begin="1" end="1" step="1">
              	<c:set var="plId" value="${result.plId }"/>
              	<c:set var="crclId" value="${result.crclId }"/>
              	<c:set var="quizId" value="${result.quizId }"/>
              	<h1 class="title-md font-800">${result.crclNm}- ${result.studySubject} 퀴즈 ${result.crclLangNm } 총 ${totCnt}문제</h1>
              </c:forEach>
            </div>
          </div>
        </div>
      </article>
    </div>
    <div class="page-content-body h-full">
      <article class="content-wrap pt-0">
        <div class="content-group">
          <button class="btn-sm-full btn-outline-gray-light bg-white" onclick="location.reload();"><i class="mr-10 icon-refresh"></i>새로고침</button>
        </div>
        <c:choose>
        	<c:when test="${status eq 'start' }">
        		<form:form commandName="quizVO" name="quizInfo" id="quizInfo" method="post" action="">
        			<input type="hidden" name="plId" value="${searchVO.plId }"/>
        			<input type="hidden" name="QuizType" value="${searchVO.quizType }"/>
					<input type="hidden" name="crclId" value="${searchVO.crclId }"/>
					<input type="hidden" id="quizEnd" name="quizEnd" value="Y"/>
        		</form:form>
        		<form:form commandName="quizVO" name="quizStart" id="quizStart" method="post" action="">
        			<input type="hidden" name="quizSort" value="1"/>
					<input type="hidden" name="QuizType" value="${searchVO.quizType }"/>
					<input type="hidden" name="plId" value="${searchVO.plId }"/>
					<input type="hidden" name="crclId" value="${searchVO.crclId }"/>
					<div id="quizIntro" class="area">
	        		<c:if test="${USER_INFO.userSeCode eq '08'}">
			          <div class="content-group">
			            <div class="content-header">
			              <div class="common-title-wrap">
			                <h1 class="title-md">응시자 현황</h1>
			              </div>
			            </div>
			            <div class="content-body">
			              <div class="flex-box-wrap">
			                <dl class="box-item">
			                  <dt class="title">전체 학생수</dt>
			                  <dd class="num"><c:out value="${quizVO.totExamineeCnt}" default="0"/></dd>
			                </dl>
			                <dl class="box-item font-red">
			                  <dt class="title font-700">결시</dt>
			                  <fmt:parseNumber var="num1" value="${quizVO.totExamineeCnt-quizVO.examineeCnt}" integerOnly="true" />
			                  <dd class="num">${num1}</dd>
			                </dl>
			              </div>
			            </div>
			          </div>
					</c:if>
			          <div id="quizStart" class="content-group">
			            <div class="btn-wrap">
			              <ul class="flex-row gutter-5">
			              	<c:choose>
			              		<c:when test="${USER_INFO.userSeCode eq '08'}">
			              			<li class="col-6">
					                  <button type="button" class="btn-md btn-point" onclick="fn_pre_quiz();">퀴즈 제출 결과 조회하기</button>
					                </li>
					                <li class="col-6">
					                  <button type="button" class="btn-md btn-point windowClose" onclick="fn_end();">퀴즈 종료하기</button>
					                </li>
			              		</c:when>
			              		<c:otherwise>
			              			<li class="col-12">
					                  <a href="#" class="btn-md btn-point" onclick="fn_pre_quiz();">퀴즈시작</a>
					                </li>
			              		</c:otherwise>
			              	</c:choose>
			              </ul>
			            </div>
			          </div>
			      </div>
			  </form:form>
        	</c:when>
        	<c:when test="${status eq 'ing' }">
        		<form:form commandName="quizVO" name="quizInfo" id="quizInfo" method="post" action="">
					  <c:if test="${USER_INFO.userSeCode ne '08'}">
					  	<input type="hidden" name="pageDiv" value="stu_exam"/>
					  </c:if>
					  <c:set var="quizSort" value="${searchVO.quizSort+1}" />
					  <input type="hidden" id="quizSort" name="quizSort" value="${quizSort}"/>
					  <input type="hidden" name="QuizType" value="${searchVO.quizType }"/>
					  <input type="hidden" name="plId" value="${searchVO.plId }"/>
					  <input type="hidden" name="crclId" value="${searchVO.crclId }"/>
					  <input type="hidden" name="quizId" value="${quiz.quizId }"/>
					  <input type="hidden" name="answerAt" id="answerAt" value="N"/>
					  <input type="hidden" id="answerNum" name="answerNum" value=""/>
					  <input type="hidden" id="submitAt" name="submitAt" value="N"/>
					  
					  <c:if test="${USER_INFO.userSeCode eq '08'}">
						  <div class="area">
						  	  <div class="content-group">
						      	<a href="#" class="btn-md btn-outline" onclick="fn_selectAnswerView();">문제 결과 확인하기</a>
						      </div>
					          <div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap">
					                <h1 class="title-md">응시자 현황</h1>
					              </div>
					            </div>
					            <div class="content-body">
					              <div class="flex-box-wrap">
					                <dl class="box-item">
					                  <dt class="title">전체 학생수</dt>
					                  <dd class="num"><c:out value="${quizVO.totExamineeCnt }" default="0"/></dd>
					                </dl>
					                <dl class="box-item font-red">
					                  <dt class="title title">결시</dt>
					                  <fmt:parseNumber var="num1" value="${quizVO.totExamineeCnt-quizVO.examineeCnt}" integerOnly="true" />
					                  <dd class="num">${num1 }</dd>
					                </dl>
					                <dl class="box-item">
					                  <dt class="title">답안 제출자</dt>
					                  <dd class="num"><c:out value="${quizVO.examineeCnt }" default="0"/></dd>
					                </dl>
					              </div>
					            </div>
					          </div>
					          <div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap f-l">
					                <h1 class="title-md">제출 현황</h1>
					              </div>
					              <button class="btn-link f-r btnModalOpen" data-modal-type="unsubmitted" onclick="fn_doNotSubmit();" style="cursor: pointer;">미제출자 확인하기<i class="icon-arrow-right ml-5"></i></button>
					            </div>
					            <div class="content-body">
					              <div class="flex-box-wrap">
					                <dl class="box-item font-point">
					                  <dt class="title">정답자</dt>
					                  <dd class="num"><c:out value="${quizVO.quizAnswerYCnt }" default="0"/></dd>
					                </dl>
					                <dl class="box-item font-red">
					                  <dt class="title">오답자</dt>
					                  <fmt:parseNumber var="num2" value="${quizVO.quizAnswerNCnt}" integerOnly="true" />
					                  <dd class="num"><c:out value="${num2}" default="0"/></dd>
					                </dl>
					                <dl class="box-item font-gray">
					                  <dt class="title">미제출자</dt>
					                  <fmt:parseNumber var="num3" value="${quizVO.totExamineeCnt-quizVO.examineeCnt}" integerOnly="true" />
					                  <dd class="num">${num3 }</dd>
					                </dl>
					              </div>
					            </div>
					          </div>
					        </div>
				      </c:if>
				        
				      <div id="quizPage" class="area">
				          <div class="content-group">
				            <div class="content-header">
				              <div class="form-title-wrap">
				              	<c:choose>
				              		<c:when test="${searchVO.quizType eq 'ON'}">
						                <h1 id="quizCn" class="title">${quiz.quizSort}. ${quiz.quizNm}</h1>
				              		</c:when>
				              		<c:otherwise>
				              			<h1 id="quizCn" class="title">${quiz.quizSort}번 문제입니다. 답안을 골라주세요.</h1>
				              		</c:otherwise>
				              	</c:choose>
				              </div>
				            </div>
				            <div class="content-body">
				              <c:if test="${not empty quiz.atchFileId }">
					              <div class="img-box-wrap mb-15">
					                <img src="/imgs/common/img_temp_02.jpg" class="block" alt="임시이미지">
					              </div>
				              </c:if>
				              <div class="box-wrap-sm">
				              <div id="answerDiv1" class="checkbox-num-wrap" style="display: block;">
				                  <ul class="flex-row gutter-5">
				                    <li class="col-12">
				                      <label class="checkbox-num">
				                        <input type="checkbox" name="answer" value="1">
				                        <span class="custom-checked" id="spanAnswer1"></span>
				                        <span id="answer1" class="text">
				                        	<c:choose>
							              		<c:when test="${searchVO.quizType eq 'ON'}">
									                ${quiz.answer1}
							              		</c:when>
							              		<c:otherwise>
							              			정답 1번입니다.
							              		</c:otherwise>
							              	</c:choose>
				                        </span>
				                      </label>
				                    </li>
				                    <li class="col-12">
				                      <label class="checkbox-num">
				                        <input type="checkbox" name="answer" value="2">
				                        <span class="custom-checked" id="spanAnswer2"></span>
				                        <span id="answer2" class="text">
				                        	<c:choose>
							              		<c:when test="${searchVO.quizType eq 'ON'}">
									                ${quiz.answer2}
							              		</c:when>
							              		<c:otherwise>
							              			정답 2번입니다.
							              		</c:otherwise>
							              	</c:choose>
				                        </span>
				                      </label>
				                    </li>
				                    <li class="col-12">
				                      <label class="checkbox-num">
				                        <input type="checkbox" name="answer" value="3">
				                        <span class="custom-checked" id="spanAnswer3"></span>
				                        <span id="answer3" class="text">
				                        	<c:choose>
							              		<c:when test="${searchVO.quizType eq 'ON'}">
									                ${quiz.answer3}
							              		</c:when>
							              		<c:otherwise>
							              			정답 3번입니다.
							              		</c:otherwise>
							              	</c:choose>
				                        </span>
				                      </label>
				                    </li>
				                    <li class="col-12">
				                      <label class="checkbox-num">
				                        <input type="checkbox" name="answer" value="4">
				                        <span class="custom-checked" id="spanAnswer4"></span>
				                        <span id="answer4" class="text">
				                        	<c:choose>
							              		<c:when test="${searchVO.quizType eq 'ON'}">
									                ${quiz.answer4}
							              		</c:when>
							              		<c:otherwise>
							              			정답 4번입니다.
							              		</c:otherwise>
							              	</c:choose>
				                        </span>
				                      </label>
				                    </li>
				                    <li class="col-12">
				                      <label class="checkbox-num">
				                        <input type="checkbox" name="answer" value="5">
				                        <span class="custom-checked" id="spanAnswer5"></span>
				                        <span id="answer5" class="text">
				                        	<c:choose>
							              		<c:when test="${searchVO.quizType eq 'ON'}">
									                ${quiz.answer5}
							              		</c:when>
							              		<c:otherwise>
							              			정답 5번입니다.
							              		</c:otherwise>
							              	</c:choose>
				                        </span>
				                      </label>
				                    </li>
				                  </ul>
				                </div>
				                
				                <div id="answerDiv2" class="checkbox-num-wrap read-only" style="display: none;">
				                  <ul class="flex-row">
				                    <li class="col-12">
				                      <div class="quiz-result-wrap">
					                      <label class="checkbox-num">
					                        <input type="checkbox" name="answer" value="1">
					                        <span class="custom-checked" id="spanAnswer1"></span>
					                        <span id="answer1" class="text">
					                        	<c:choose>
								              		<c:when test="${searchVO.quizType eq 'ON'}">
										                ${quiz.answer1}
								              		</c:when>
								              		<c:otherwise>
								              			정답 1번입니다.
								              		</c:otherwise>
								              	</c:choose>
					                        </span>
					                      </label>
					                      <p class="quiz-result">선택 수 : <b>${quiz.subAnswer1}명 / ${quiz.subAnswer1p}%</b></p>
				                      </div>
				                    </li>
				                    <li class="col-12">
				                      <div class="quiz-result-wrap">
					                      <label class="checkbox-num">
					                        <input type="checkbox" name="answer" value="2">
					                        <span class="custom-checked" id="spanAnswer2"></span>
					                        <span id="answer2" class="text">
					                        	<c:choose>
								              		<c:when test="${searchVO.quizType eq 'ON'}">
										                ${quiz.answer2}
								              		</c:when>
								              		<c:otherwise>
								              			정답 2번입니다.
								              		</c:otherwise>
								              	</c:choose>
					                        </span>
					                      </label>
				                          <p class="quiz-result">선택 수 : <b>${quiz.subAnswer2}명 / ${quiz.subAnswer2p}%</b></p>
				                      </div>
				                    </li>
				                    <li class="col-12">
				                      <div class="quiz-result-wrap">
					                      <label class="checkbox-num">
					                        <input type="checkbox" name="answer" value="3">
					                        <span class="custom-checked" id="spanAnswer3"></span>
					                        <span id="answer3" class="text">
					                        	<c:choose>
								              		<c:when test="${searchVO.quizType eq 'ON'}">
										                ${quiz.answer3}
								              		</c:when>
								              		<c:otherwise>
								              			정답 3번입니다.
								              		</c:otherwise>
								              	</c:choose>
					                        </span>
					                      </label>
				                          <p class="quiz-result">선택 수 : <b>${quiz.subAnswer3}명 / ${quiz.subAnswer3p}%</b></p>
				                      </div>
				                    </li>
				                    <li class="col-12">
				                      <div class="quiz-result-wrap">
					                      <label class="checkbox-num">
					                        <input type="checkbox" name="answer" value="4">
					                        <span class="custom-checked" id="spanAnswer4"></span>
					                        <span id="answer4" class="text">
					                        	<c:choose>
								              		<c:when test="${searchVO.quizType eq 'ON'}">
										                ${quiz.answer4}
								              		</c:when>
								              		<c:otherwise>
								              			정답 4번입니다.
								              		</c:otherwise>
								              	</c:choose>
					                        </span>
					                      </label>
				                          <p class="quiz-result">선택 수 : <b>${quiz.subAnswer4}명 / ${quiz.subAnswer4p}%</b></p>
				                      </div>
				                    </li>
				                    <li class="col-12">
				                      <div class="quiz-result-wrap">
					                      <label class="checkbox-num">
					                        <input type="checkbox" name="answer" value="5">
					                        <span class="custom-checked" id="spanAnswer5"></span>
					                        <span id="answer5" class="text">
					                        	<c:choose>
								              		<c:when test="${searchVO.quizType eq 'ON'}">
										                ${quiz.answer5}
								              		</c:when>
								              		<c:otherwise>
								              			정답 5번입니다.
								              		</c:otherwise>
								              	</c:choose>
					                        </span>
					                      </label>
				                          <p class="quiz-result">선택 수 : <b>${quiz.subAnswer5}명 / ${quiz.subAnswer5p}%</b></p>
				                      </div>
				                    </li>
				                  </ul>
				                </div>
				                
				              </div>
				            </div>
				          </div>
				          
				          <c:if test="${USER_INFO.userSeCode ne '08'}">
					          <div class="content-group">
					         	 <div class="info-box-wrap">답안은 한번 제출하면 다시 풀 수 없습니다.</div>
					          </div>
          				  </c:if>
          				  
				          <div class="content-group">
				            <div class="box-wrap-sm">
				              <div class="box-list-wrap">
				                <div class="box-hader">
				                  <!-- <h1 class="title">내가 선택한 답안</h1>
				                  <div id="selAnswer" class="font-point ml-auto">1번</div> -->
				                </div>
				                <c:choose>
				                	<c:when test="${searchVO.quizSort eq totCnt }">
				                		<c:choose>
					                  		<c:when test="${USER_INFO.userSeCode ne '08'}">
					                  			<div class="box-body mt-20">
								                  <button class="btn-md btn-point" onclick="fn_submit('${searchVO.quizSort}');">제출하기</button>
								                </div>
					                  		</c:when>
					                  		<c:otherwise>
					                  			<ul class="flex-row gutter-5">
									                <li class="col-6">
									                  <button type="button" id="prevBtn" class="btn-md btn-outline-deep-gray" onclick="fn_prev_page();">이전문제</button>
									                </li>
									                <li class="col-6">
									                  <button type="button" class="btn-md btn-point" onclick="fn_submit('${searchVO.quizSort}');">마지막 문제입니다.</button>
									                </li>
									            </ul>
					                  		</c:otherwise>
					                  	</c:choose>
				                	</c:when>
				                	<c:when test="${USER_INFO.userSeCode eq '08' and searchVO.quizSort > 1}">
				                		<ul class="flex-row gutter-5">
							                <li class="col-6">
							                  <button type="button" id="prevBtn" class="btn-md btn-outline-deep-gray" onclick="fn_prev_page();">이전문제</button>
							                </li>
							                <li class="col-6">
							                  <button type="button" id="nextBtn" class="btn-md btn-point" onclick="fn_next_page();">다음문제</button>
							                </li>
							            </ul>
				                	</c:when>
				                	<c:otherwise>
				                		<div class="box-body mt-10">
						                  <button type="button" id="nextBtn" class="btn-md btn-point" onclick="fn_next_page();">다음문제</button>
						                </div>
				                	</c:otherwise>
				                </c:choose>
				              </div>
				            </div>
				          </div>
				        </div>
		        </form:form>
        	</c:when>
        	<c:when test="${status eq 'end' }">
        		<form:form commandName="quizVO" name="quizInfo" id="quizInfo" method="post" action="">
					  <c:if test="${USER_INFO.userSeCode ne '08'}">
					  	<input type="hidden" name="pageDiv" value="stu_exam"/>
					  </c:if>
					  <c:set var="quizSort" value="${searchVO.quizSort}" />
					  <input type="hidden" name="quizSort" value="${quizSort}"/>
					  <input type="hidden" name="QuizType" value="${searchVO.quizType }"/>
					  <input type="hidden" name="plId" value="${searchVO.plId }"/>
					  <input type="hidden" name="crclId" value="${searchVO.crclId }"/>
					  <input type="hidden" name="quizId" value="${quiz.quizId }"/>
					  <input type="hidden" name="answerAt" id="answerAt" value="N"/>
					  <input type="hidden" id="answerNum" name="answerNum" value=""/>
					  <input type="hidden" id="submitAt" name="submitAt" value="N"/>
					  <input type="hidden" id="quizEnd" name="quizEnd" value="Y"/>
					  <div class="area">
			          <c:choose>
			          	<c:when test="${USER_INFO.userSeCode ne '08'}">
			          		<div class="content-group">
					            <div class="box-wrap-md center-align">
					              <div class="common-title-wrap">
					                <p class="desc mb-10">평가가 <span class="font-700">완료</span>되었습니다.</p>
					                <h1 class="title-md font-800">나의 점수 <span class="font-point">${quizVO.myScore}점</span></h1>
					              </div>
					            </div>
					        </div>
			          		<div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap">
					                <h1 class="title-md">점수 확인</h1>
					              </div>
					            </div>
					            <div class="content-body">
					              <div class="box-wrap-sm">
					                <ul class="box-list-wrap">
					                  <li class="box-list">
					                    <div class="box-hader">
					                      <h2 class="title">나의 점수</h2>
					                      <div class="rating">${quizVO.myScore}/${quizVO.totCnt} 점</div>
					                    </div>
					                    <div class="box-body">
					                      <div class="bar-chart-wrap">
					                      	<fmt:parseNumber var="grap1" value="${quizVO.myScore/quizVO.totCnt*100}" integerOnly="true" />
					                        <div class="bar bg-point" style="width:${grap1}%"></div>
					                      </div>
					                    </div>
					                  </li>
					                  <li class="box-list">
					                    <div class="box-hader">
					                      <h2 class="title">평균 점수</h2>
					                      <div class="rating">${quizVO.avgScoreCnt}/${quizVO.avgTotCnt} 점</div>
					                    </div>
					                    <div class="box-body">
					                      <div class="bar-chart-wrap">
					                      	<fmt:parseNumber var="grap2" value="${quizVO.avgScoreCnt/quizVO.avgTotCnt*100}" integerOnly="true" />
					                        <div class="bar bg-yellow-green" style="width:${grap2}%"></div>
					                      </div>
					                    </div>
					                  </li>
					                </ul>
					              </div>
					            </div>
					          </div>
			          	</c:when>
			          	<c:otherwise>
			          		<fmt:parseNumber var="rankingTmp" value="0" integerOnly="true" />
			          		<fmt:parseNumber var="minScore" value="0" integerOnly="true" />
			          		<fmt:parseNumber var="maxScore" value="0" integerOnly="true" />
			          		<c:forEach var="result" items="${quizResult}" varStatus="status">
			          			<fmt:parseNumber var="ranking" value="${result.ranking }" integerOnly="true" />
			          			<c:if test="${ranking eq '1' and result.notSubCnt > 0}">
					            	<fmt:parseNumber var="totCnt" value="${result.totCnt }" integerOnly="true" />
					            	<fmt:parseNumber var="maxScore" value="${result.myScore}" integerOnly="true" />
					            </c:if>
					            <c:if test="${rankingTmp < result.ranking and result.notSubCnt > 0}">
					            	<fmt:parseNumber var="minScore" value="${result.myScore}" integerOnly="true" />
					            </c:if>
				          	</c:forEach>
				          	
			          		<div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap">
					                <h1 class="title-md">점수 확인</h1>
					              </div>
					            </div>
					            <div class="content-body">
					              <div class="box-wrap-sm">
					                <ul class="box-list-wrap">
					                  <li class="box-list">
					                    <div class="box-hader">
					                      <h2 class="title">최고점</h2>
					                      <div class="rating">${maxScore }/${totCnt} 점</div>
					                    </div>
					                    <div class="box-body">
					                      <div class="bar-chart-wrap">
					                      	<c:choose>
					                      		<c:when test="${maxScore eq '0'}">
					                      			<fmt:parseNumber var="grap3" value="0" integerOnly="true" />
					                      		</c:when>
					                      		<c:otherwise>
					                      			<fmt:parseNumber var="grap3" value="${maxScore/totCnt*100}" integerOnly="true" />
					                      		</c:otherwise>
					                      	</c:choose>
					                        <div class="bar bg-point" style="width:${grap3}%"></div>
					                      </div>
					                    </div>
					                  </li>
					                  <li class="box-list">
					                    <div class="box-hader">
					                      <h2 class="title">최저점</h2>
					                      <div class="rating">${minScore }/${totCnt } 점</div>
					                    </div>
					                    <div class="box-body">
					                      <div class="bar-chart-wrap">
					                      	<c:choose>
					                      		<c:when test="${minScore eq '0'}">
					                      			<fmt:parseNumber var="grap3" value="0" integerOnly="true" />
					                      		</c:when>
					                      		<c:otherwise>
					                      			<fmt:parseNumber var="grap4" value="${minScore/totCnt*100}" integerOnly="true" />
					                      		</c:otherwise>
					                      	</c:choose>
					                        <div class="bar bg-yellow-green" style="width:${grap4}%"></div>
					                      </div>
					                    </div>
					                  </li>
									<li class="box-list">
					                    <div class="box-hader">
					                      <h2 class="title">평균 점수</h2>
					                      <div class="rating">${quizVO.avgScoreCnt}/${quizVO.avgTotCnt} 점</div>
					                    </div>
					                    <div class="box-body">
					                      <div class="bar-chart-wrap">
					                      	<c:choose>
					                      		<c:when test="${quizVO.avgScoreCnt eq '0'}">
					                      			<fmt:parseNumber var="grap5" value="0" integerOnly="true" />
					                      		</c:when>
					                      		<c:otherwise>
					                      			<fmt:parseNumber var="grap5" value="${quizVO.avgScoreCnt/quizVO.avgTotCnt*100}" integerOnly="true" />
					                      		</c:otherwise>
					                      	</c:choose>
					                        <div class="bar" style="width:${grap5}%"></div>
					                      </div>
					                    </div>
					                  </li>			                  
					                </ul>
					              </div>
					            </div>
					          </div>
					          
					          
					          <div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap">
					                <h1 class="title-md">응시자 현황</h1>
					              </div>
					            </div>
					            <div class="content-body">
					              <div class="flex-box-wrap">
					                <dl class="box-item">
					                  <dt class="title">전체 학생수</dt>
					                  <dd class="num">${quizVO.totExamineeCnt }</dd>
					                </dl>
					                <dl class="box-item font-red">
					                  <dt class="title font-700">결시</dt>
					                  <fmt:parseNumber var="num1" value="${quizVO.totExamineeCnt-quizVO.examineeCnt}" integerOnly="true" />
					                  <dd class="num">${num1 }</dd>
					                </dl>
					              </div>
					            </div>
					          </div>
					          
					          
					          <div class="content-group">
					            <div class="content-header">
					              <div class="common-title-wrap">
					                <h1 class="title-md">응시자 명단</h1>
					              </div>
					            </div>
					            <div class="content-body">
					              <article class="nav-tab-wrap box-style mb-10">
					                <div class="tab-wrap">
					                  <div class="inner">
					                    <div id="subTab1" class="tab-item active"><a href="#" onclick="fn_subList('1');"> 응시자</a></div>
					                    <div id="subTab2" class="tab-item "><a href="#" onclick="fn_subList('2');"> 결시</a></div>
					                  </div>
					                </div>
					              </article>
					              <ul id="subCnt" class="user-list-wrap">
					              	<c:forEach var="result" items="${quizResult}" varStatus="status">
						          		<fmt:parseNumber var="notSubCnt" value="${result.notSubCnt}" integerOnly="true" />
						          		<c:if test="${notSubCnt > 0}">
							          		<li class="user">
							                  <div class="img-frame" style="background-image: url(/template/lms/imgs/common/icon_user_name.svg);"></div>
							                  <span class="name">${result.userNm }</span>
							                </li>
						          		</c:if>
						          	</c:forEach>
					              </ul>
					              <ul id="notSubCnt" class="user-list-wrap">
					              	<c:forEach var="result" items="${quizResult}" varStatus="status">
						          		<fmt:parseNumber var="notSubCnt" value="${result.notSubCnt}" integerOnly="true" />
						          		<c:if test="${notSubCnt == 0}">
							          		<li class="user">
							                  <div class="img-frame" style="background-image: url(/template/lms/imgs/common/icon_user_name.svg);"></div>
							                  <span class="name">${result.userNm }</span>
							                </li>
						          		</c:if>
						          	</c:forEach>
					              </ul>
					            </div>
					          </div>
			          		</c:otherwise>
			          </c:choose>
			          <div class="content-group">
			            <div class="btn-wrap">
			              <a href="#" class="btn-md btn-point windowClose" onclick="fn_end();">퀴즈 종료하기</a>
			            </div>
			          </div>
			        </div>
				</form:form>
	        </c:when>
        </c:choose>
      </article>
    </div>
    
    <div id="unsubmitted_modal" class="alert-modal" style="display: none;">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">미제출자 확인</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text center-align">
          	<c:forEach var="result" items="${quizResult}" varStatus="status">
	            <fmt:parseNumber var="notSubCntTmp" value="${result.notSubCnt }" integerOnly="true" />
          		<c:if test="${notSubCntTmp == 0}">
          			<fmt:parseNumber var="notSubCnt" value="${notSubCnt+1}" integerOnly="true" />
          		</c:if>
          	</c:forEach>
            <span class="font-point">${notSubCnt}</span>명이 문제를 제출하지 않았습니다.
          </p>
          <ul class="user-list-wrap mt-20">
          	<c:forEach var="result" items="${quizResult}" varStatus="status">
          		<fmt:parseNumber var="notSubCnt" value="${result.notSubCnt}" integerOnly="true" />
          		<c:if test="${notSubCnt == 0}">
	          		<li class="user">
		              <div class="img-frame" style="background-image: url(/template/lms/imgs/common/icon_user_name.svg);"></div>
		              <span class="name">${result.userNm }</span>
	            	</li>	
          		</c:if>
          	</c:forEach>
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-md btn-point btnModalConfirm">확인</button>
        </div>
      </div>
    </div>
  </div>
  </section>
  

  <c:import url="/lms/quiz/QuizSolveFooter.do" />
</body>

</html>