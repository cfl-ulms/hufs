<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizReg.do"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId}"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>
  <link rel="stylesheet" href="/template/lms/css/quiz/modal.css">
  <link rel="stylesheet" href="/template/lms/css/quiz/staff_class.css">
  <script src="/template/lms/js/quiz/staff_class_online_quiz.js"></script>
  <script src="/template/lms/js/quiz/jquery.nestable.js"></script>
<script>
	$(document).ready(function(){
		<c:if test="${not empty quizVO.answerNum}">
			var temp = "${quizVO.answerNum}";
			var arr = temp.split(",");
			for(var i in arr){
				$("#spanAnswer"+arr[i]).trigger("click");
				/*
				if(arr[i] == "1"){
					$("#spanAnswer1").trgger("click");	
				}else if(arr[i] == "2"){
					$("#spanAnswer2").trgger("click");
				}else if(arr[i] == "3"){
					$("#spanAnswer3").trgger("click");
				}else if(arr[i] == "4"){
					$("#spanAnswer4").trgger("click");
				}else if(arr[i] == "5"){
					$("#spanAnswer5").trgger("click");
				}
				*/
			}
		</c:if>
		
		$('#btnReg').click(function(){
			$('#no_contents_quiz_modal').css("display", "block");
		});
		
		$(".btn_file1").click(function(){
			var tempHostCode = '${quizVO.atchFileId}';
			window.open("/cmm/fms/studyFileList.do?hostCode="+tempHostCode, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=900");
			return false;
		});
		
		//저장
		$("#btn_save").click(function(){
			//validation
			vali();
			//답지선택된 내역 input 담기
			answer();
			
			if(!valbool){
				return false;
			}
			$("#detailForm").submit();
		});
		
		$("#btn_del_all").click(function(){
			if(confirm("해당 퀴즈 문제를 전체 삭제 하시겠습니까?")){
				$("#detailForm").attr("action", "/lms/quiz/QuizDel.do");
				$("#delDiv").val("ALL");
				$("#detailForm").submit();
			}
		});
		
		$("#quizAllSave").click(function(){
			if(confirm("해당 퀴즈 순서를 현재 기준으로 저장하시겠습니까?")){
				$("#subForm").attr("action", "/lms/quiz/QuizSortBatch.do${_BASE_PARAM}");
				$("#subForm").submit();
			}
		});
		
	});
	
	
	//레이어 닫기
	$(document).on("click", ".btnModalClose", function(){
		var id = $(this).parents(".alert-modal").attr("id");
		
		if(id == "no_contents_quiz_modal"){
			$("#no_contents_quiz_modal").hide();
		}else if(id == "online_quiz_thumbnail_modal"){
			$("#online_quiz_thumbnail_modal").hide();
		}else{
			$(this).parents(".alert-modal").remove();	
		}
	});
	
	
	function vali(){
		if(!$("#quizNm").val()){
			alert("퀴즈 문제 작성하십시오..");
			return false;
		}
		if($("#answer1").val().length>300){		
			alert("1번 답지 길이 초과 하였습니다.");
			return false;
		}
		if($("#answer2").val().length>300){		
			alert("2번 답지 길이 초과 하였습니다.");
			return false;
		}
		if($("#answer3").val().length>300){		
			alert("3번 답지 길이 초과 하였습니다.");
			return false;
		}
		if($("#answer4").val().length>300){		
			alert("4번 답지 길이 초과 하였습니다.");
			return false;
		}
		if($("#answer5").val().length>300){		
			alert("5번 답지 길이 초과 하였습니다.");
			return false;
		}
		return;
	}
	
	function answer(){
		var answer1 = $("#spanAnswer1").css("background-color");
		var answerNum1 = $("#spanNumber1").text();
		var answer2 = $("#spanAnswer2").css("background-color");
		var answerNum2 = $("#spanNumber2").text();
		var answer3 = $("#spanAnswer3").css("background-color");
		var answerNum3 = $("#spanNumber3").text();
		var answer4 = $("#spanAnswer4").css("background-color");
		var answerNum4 = $("#spanNumber4").text();
		var answer5 = $("#spanAnswer5").css("background-color");
		var answerNum5 = $("#spanNumber5").text();
		var sumAnswer = "";
		if(answer1 == "rgb(6, 132, 103)"){
			sumAnswer += answerNum1;
		}
		if(answer2 == "rgb(6, 132, 103)"){
			if(sumAnswer.length>0){sumAnswer += ","+answerNum2;
			}else{sumAnswer += answerNum2;}
		}
		if(answer3 == "rgb(6, 132, 103)"){
			if(sumAnswer.length>0){sumAnswer += ","+answerNum3;
			}else{sumAnswer += answerNum3;}
		}
		if(answer4 == "rgb(6, 132, 103)"){
			if(sumAnswer.length>0){sumAnswer += ","+answerNum4;
			}else{sumAnswer += answerNum4;}
		}
		if(answer5 == "rgb(6, 132, 103)"){
			if(sumAnswer.length>0){sumAnswer += ","+answerNum5;
			}else{sumAnswer += answerNum5;}
		}
		if(sumAnswer == ""){
			alert("정답을 선택하십시오.");
			return false;
			valbool = false;
		}else{
			$("#answerNum").val(sumAnswer);
			valbool = true;
		}
		return;
		
	}
	
	
	function fn_del_s(quizId, quizNm){
		if(confirm("\""+quizNm+"\" 삭제 하시겠습니까?")){
			$("#detailForm").attr("action", "/lms/quiz/QuizDel.do?quizId="+quizId);
			$("#delDiv").val("");
			$("#detailForm").submit();
		}
	}
	
	
	function fn_pre_quiz(plId, quizSort, quizType){
		var preNum = 0;
		var nextNum = 0;
		
		$.ajax({
			url : "/lms/quiz/PreQuizListAjax.json"
				, type : "post"
				, dataType : "json"
				, data : {"plId" : plId, "quizSort" : quizSort, "quizType" : quizType}
				, success : function(data){
					if(data.successYn == "Y"){
						var preQuizList = data.preQuizList;
						if(preQuizList[0].quizType == "ON"){
							$("#online_quiz_thumbnail_modal").css("display", "block");
							$("#online_quiz_modal").css("display", "none");
							$("#totalCnt").text(preQuizList[0].quizSort+" / "+preQuizList[0].quizCnt);
							$("#quizCn").text("Q. "+preQuizList[0].quizNm);
							$(".answer1").text(preQuizList[0].answer1);
							$(".answer2").text(preQuizList[0].answer2);
							$(".answer3").text(preQuizList[0].answer3);
							$(".answer4").text(preQuizList[0].answer4);
							$(".answer5").text(preQuizList[0].answer5);
							if(preQuizList[0].quizSort == "1"){
								$("#preBtn").attr("disabled", true);
								$("#nextBtn").attr("disabled", false);
							}else if(preQuizList[0].quizSort == preQuizList[0].quizCnt){
								$("#preBtn").attr("disabled", false);
								$("#nextBtn").attr("disabled", true);
							}else{
								$("#preBtn").attr("disabled", false);
								$("#nextBtn").attr("disabled", false);
							}
							
							preNum = preQuizList[0].quizSort-1;			
							nextNum = preQuizList[0].quizSort+1;
							
							$("#preBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+preNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#nextBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+nextNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#updMove").attr("onclick", "fn_upd_move('"+preQuizList[0].quizId+"', '"+preQuizList[0].plId+"', '"+preQuizList[0].quizType+"'); return false;");
							
						}else{
							$("#online_quiz_thumbnail_modal").css("display", "none");
							$("#online_quiz_modal").css("display", "block");
							$("#totalCnt2").text(preQuizList[0].quizSort+" / "+preQuizList[0].quizCnt);
							$("#quizCn2").text("Q. "+preQuizList[0].quizSort+"번 문제입니다. 답안을 골라주세요.");
							if(preQuizList[0].quizSort == "1"){
								$("#preBtn2").attr("disabled", true);
								$("#nextBtn2").attr("disabled", false);
							}else if(preQuizList[0].quizSort == preQuizList[0].quizCnt){
								$("#preBtn2").attr("disabled", false);
								$("#nextBtn2").attr("disabled", true);
							}else{
								$("#preBtn2").attr("disabled", false);
								$("#nextBtn2").attr("disabled", false);
							}
							
							preNum = preQuizList[0].quizSort-1;			
							nextNum = preQuizList[0].quizSort+1;
							
							$("#preBtn2").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+preNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#nextBtn2").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+nextNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#updMove2").attr("onclick", "fn_upd_move('"+preQuizList[0].quizId+"', '"+preQuizList[0].plId+"', '"+preQuizList[0].quizType+"'); return false;");
						}
					}
				}, error : function(){
					alert("error");
				}
		});
	}
	
	function fn_upd_move(quizId, plId){
		$("#detailForm").attr("action", "/lms/quiz/QuizRegView.do${_BASE_PARAM2}&quizId="+quizId+"&plId="+plId);
		$("#detailForm").submit();
	}
	
	function fn_init(url){
		if(confirm("초기화 하시겠습니까?")){
			//$("#detailForm").attr("action", url);
			//$("#detailForm").submit();
			$(location).attr('href', url);
		}
	}
	
	function fn_end(){
		alert("퀴즈가 종료되어 문제 추가/삭제를 할 수 없습니다.\n답안 수정만 가능합니다.");
		return false;
	}
</script>

<div class="page-content-header">
	<c:choose>
		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
			<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
		</c:when>
		<c:otherwise>
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
		</c:otherwise>
	</c:choose>
</div>
<section class="page-content-body">
	<article class="content-wrap">
		<!-- tab style -->
		<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="4"/>
			<c:param name="crclId" value="${searchVO.crclId}"/>
			<c:param name="menuId" value="${searchVO.menuId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
    </article>
    <form:form commandName="quizVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data">
    	<input type="hidden" name=menuId value="${searchVO.menuId}"/>
        <input type="hidden" name=plId value="${searchVO.plId}"/>
		<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
		<input type="hidden" name="examType" value="1"/>
		<input type="hidden" name="atchFileId" value="<c:out value='${searchVO.atchFileId}'/>"/>
		<input type="hidden" id="delDiv" name="delDiv" value=""/>
		<c:if test="${not empty quizVO.quizSort && quizVO.quizSort != '0'}"><input type="hidden" id="quizSort" name="quizSort" value="${quizVO.quizSort}"/></c:if>
		<c:if test="${not empty quizVO.quizId}"><input type="hidden" id="quizId" name="quizId" value="${quizVO.quizId}"/></c:if>
		<input type="hidden" id="answerNum" name="answerNum" value="${quizVO.answerNum}"/>
		<input type="hidden" id="quizType" name="quizType" value="ON"/>
		<input type="hidden" id="pageDiv" name="pageDiv" value="ON"/>
		
		<input type="hidden" name="step" value="${searchVO.step}"/>
		<input type="hidden" name="tabType" value="${searchVO.tabType}"/>
		<input type="hidden" name="subtabstep" value="${searchVO.subtabstep}"/>
		<input type="hidden" name="tabStep" value="${searchVO.tabStep}"/>
		<input type="hidden" name="quizEnd" value="${searchVO.quizEnd }"/>
		
		<article class="content-wrap" style="margin-bottom: 40px;">
           <div class="content-header">
             <div class="title-wrap">
               <div class="title">온라인 퀴즈 [문제 등록형]</div>
             </div>
           </div>
           <div class="content-body">
             <div class="quiz-write-wrap">
               <div class="quiz-text-title">문제 작성</div>
               <textarea class="class-textarea" name="quizNm" id="quizNm" cols="" rows="" placeholder="문제 내용을 입력해주세요.">${quizVO.quizNm }</textarea>
               <!-- 첨부파일 -->
               <%-- 
               <div class="file-attachment-wrap">
               		<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
						<c:param name="editorId" value="${_EDITOR_ID}"/>
						<c:param name="estnAt" value="N" />
				    	<c:param name="param_atchFileId" value="${searchVO.atchFileId}" />
				    	<c:param name="imagePath" value="${_IMG }"/>
				    	<c:param name="regAt" value="Y"/>
				    	<c:param name="commonAt" value="Y"/>
				    	<c:param name="crclId" value="${searchVO.crclId}"/>
					</c:import>
					<noscript>
						<input name="file_1" id="egovComFileUploader" type="file" class="inp" />
					</noscript>
               </div>
                --%>
               <div class="quiz-text-title mt-40">답가지 (총 5개)
                 <div class="class-tab-wrap">
                   <!-- 
                   <a class="title on">텍스트</a>
                   <a class="title">이미지</a>
                   <a class="title">동영상</a>
                    -->
                 </div>
               </div>
               <p class="desc-sm">번호를 클릭하여 정답을 지정해주세요.</p>
               <div <c:if test="${searchVO.quizEnd eq 'N'}" >class="dd" </c:if>>
                 <ol class="dd-list">
                   <li class="dd-item">
                     <div class="dd-handle"></div>
                     <div class="dd-content">
                       <label class="checkbox-num circle">
                         <input type="checkbox">
                         <span class="custom-checked" id="spanAnswer1"></span>
                         <span class="checkboxNum text" id="spanNumber1">1</span>
                       </label>
                       <input type="text" name="answer" id="answer1" placeholder="내용을 입력해 주세요." value="${quizVO.answer1}" maxlength="20">
                     </div>
                   </li>
                   <li class="dd-item">
                     <div class="dd-handle"></div>
                     <div class="dd-content">
                       <label class="checkbox-num circle">
                         <input type="checkbox">
                         <span class="custom-checked" id="spanAnswer2"></span>
                         <span class="checkboxNum text" id="spanNumber2">2</span>
                       </label>
                       <input type="text" name="answer" id="answer2" placeholder="내용을 입력해 주세요." value="${quizVO.answer2}" maxlength="20">
                     </div>
                   </li>
                   <li class="dd-item">
                     <div class="dd-handle"></div>
                     <div class="dd-content">
                       <label class="checkbox-num circle">
                         <input type="checkbox">
                         <span class="custom-checked" id="spanAnswer3"></span>
                         <span class="checkboxNum text" id="spanNumber3">3</span>
                       </label>
                       <input type="text" name="answer" id="answer3" placeholder="내용을 입력해 주세요."value="${quizVO.answer3}" maxlength="20">
                     </div>
                   </li>
                   <li class="dd-item">
                     <div class="dd-handle"></div>
                     <div class="dd-content">
                       <label class="checkbox-num circle">
                         <input type="checkbox">
                         <span class="custom-checked" id="spanAnswer4"></span>
                         <span class="checkboxNum text" id="spanNumber4">4</span>
                       </label>
                       <input type="text" name="answer" id="answer4" placeholder="내용을 입력해 주세요." value="${quizVO.answer4}" maxlength="20">
                     </div>
                   </li>
                   <li class="dd-item">
                     <div class="dd-handle"></div>
                     <div class="dd-content">
                       <label class="checkbox-num circle">
                         <input type="checkbox">
                         <span class="custom-checked" id="spanAnswer5"></span>
                         <span class="checkboxNum text" id="spanNumber5">5</span>
                       </label>
                       <input type="text" name="answer" id="answer5" placeholder="내용을 입력해 주세요." value="${quizVO.answer5 }" maxlength="20">
                     </div>
                   </li>
                 </ol>
               </div>
               <div class="mt-30 center-align">
               	 <c:url var="viewUrl" value="/lms/quiz/QuizRegView.do${_BASE_PARAM}">
 					<c:param name="quizType" value="ON"/>              	 	
               	 </c:url>
                 <a href="#" id="" class="btn-xl btn-outline-gray" onclick="fn_init('${viewUrl}');">작성 초기화</a>
                 <a href="#" id="btn_save" class="btn-xl btn-point">문제 저장</a>
               </div>
             </div>
           </div>
         </article>
	</form:form>


	<c:if test="${!empty resultList}">
	<hr class="line-hr mb-35">
		<article class="content-wrap">
			<div class="content-header">
				<div class="title-wrap">
					<div class="title">
						저장된 문제 총 <span class="font-point">${fn:length(resultList)} </span>개
					</div>
				</div>
				<c:choose>
					<c:when test="${searchVO.quizEnd eq 'Y' }">
						<button type="button" class="btn-sm btn-outline-gray" onclick="fn_end();">문제 전체삭제</button>
					</c:when>
					<c:otherwise>
						<button type="button" id="btn_del_all" class="btn-sm btn-outline-gray">문제 전체삭제</button>					
					</c:otherwise>
				</c:choose>
			</div>
			<div class="content-body">
				<form:form commandName="quizVO" name="subForm" id="subForm" method="post" action="${_ACTION}">
					
					<div class="quiz-save-wrap">
						<div class="dd">
							<ol class="dd-list">
								<c:forEach var="result" items="${resultList}" varStatus="status">
									<c:url var="viewUrl3" value="/lms/quiz/QuizRegView.do${_BASE_PARAM}">
										<c:param name="quizId" value="${result.quizId }" />
										<c:param name="quizType" value="ON"></c:param>
									</c:url>
									<li class="dd-item">
										<div class="dd-handle"></div>
										<div class="dd-content">
											<div class="quiz-save-text">
												<a href="${viewUrl3 }"><span class="quiz-save-num quizSaveNum">${fn:length(resultList)+1-status.count}. </span> ${result.quizNm }</a>
											</div>
											<div class="quiz-flex-wrap">
												<p class="quiz-save-answer">정답:${result.answerNum }번</p>
												<input type="hidden" name="quizListSort" value="${result.quizId}"/>
												<c:choose>
													<c:when test="${searchVO.quizEnd eq 'Y' }">
														<a href="#" onclick="fn_end();"><img class="vertical-top" src="/template/lms/imgs/common/icon_close_gray.png" alt="삭제"></a>
													</c:when>
													<c:otherwise>
														<a href="#" onclick="fn_del_s('${result.quizId}', '${status.count}.${result.quizNm }'); return;"><img class="vertical-top" src="/template/lms/imgs/common/icon_close_gray.png" alt="삭제"></a>					
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</li>
								</c:forEach>
							</ol>
						</div>
					</div>
				</form:form>
			</div>
		</article>
		
		<div class="page-btn-wrap mt-50">
			<c:choose>
				<c:when test="${totCnt > 0 }">
					<c:url var="viewUrl2" value="/lms/quiz/QuizEvalList.do${_BASE_PARAM}" />
				</c:when>
				<c:otherwise>
					<c:url var="viewUrl2" value="/lms/quiz/QuizList.do${_BASE_PARAM}" />
				</c:otherwise>
			</c:choose>
	        <a href="${viewUrl2 }" class="btn-xl btn-outline-gray">취소</a>
	        <a href="#" class="btn-xl btn-outline btnModalOpen" data-modal-type="online_quiz_thumbnail" onclick="fn_pre_quiz('${searchVO.plId}', '1', '${searchVO.quizType}'); return false;">문제 미리보기</a>
	        <c:choose>
				<c:when test="${searchVO.quizEnd eq 'Y' }">
					<a href="#" class="btn-xl btn-point" onclick="fn_end();">퀴즈 저장</a>
				</c:when>
				<c:otherwise>
					<a href="#" id="quizAllSave" class="btn-xl btn-point">퀴즈 저장</a>					
				</c:otherwise>
			</c:choose>
      	</div>
	</c:if>
	
	<div id="online_quiz_thumbnail_modal" class="alert-modal" style="display: none;">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">온라인 퀴즈(문제 등록형)</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body online-quiz-modal-wrap">
          <p class="quiz-test-num" id="totalCnt"></p>
          <p class="quiz-test-title" id="quizCn"></p>
          <%--<div class="quiz-text-img" style="background-image:url(../imgs/common/img_temporarily_05.jpg);"></div>--%>
          <ul>
          	<c:forEach begin="1" end="5" varStatus="status">
          		<li class="quiz-test-items">
	              <label class="checkbox-num circle">
	                <input type="checkbox">
	                <span id="temp${status.count }" class="custom-checked"></span>
	                <span class="checkboxNum text">${status.count}</span>
	              </label>
	              <span id="answer${status.count }" class="answer${status.count}">정답 ${status.count}번입니다.</span>
	            </li>
          	</c:forEach>
          </ul>
        
        <div class="modal-footer quiz-text-btn-wrap">
          <div>
            <button type="button" id="preBtn" class="btn-md-auto btn-outline-gray popup-icon-arrow-gray left">이전문제</button>
            <button type="button" id="nextBtn" class="btn-md-auto btn-outline-gray popup-icon-arrow-gray">다음문제</button>
          </div>
          <button type="button" id="updMove" class="btn-sm btn-outline">수정하기</button>
        
      
    
  </div>
</section>
<c:import url="/template/bottom.do" charEncoding="utf-8"/>