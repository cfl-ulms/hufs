<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>

<c:if test="${!empty param.param_atchFileId }">
	<table class="common-table-wrap size-sm left-align">
	  <colgroup>
	    <col width="20%" class="bg-light-blue">
	    <col width="*">
	  </colgroup>
	  <tbody>
	    <c:forEach var="fileVO" items="${fileList}" varStatus="status">
	    	<c:url var="downLoad" value="/cmm/fms/FileDown.do">
				<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
				<c:param name="fileSn" value="${fileVO.fileSn}"/>
				<c:choose>
					<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
					<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
					<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
					<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
				</c:choose>
				<c:choose>
					<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
				</c:choose>
			</c:url>
			
			
			<c:choose>
				<c:when test="${status.index eq 0 }"><c:set var="file_lable" value="취소 신청서"/></c:when>
				<c:when test="${status.index eq 1 }"><c:set var="file_lable" value="환불 신청서"/></c:when>
			</c:choose>
		    <tr>
		      <td scope="row" class="table-title font-700">${file_lable }</td>
		      <td class="ell">
		        <a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
					<c:out value="${fileVO.orignlFileNm}"/>
				</a>
		      </td>
		    </tr>
		</c:forEach>
	  </tbody>
	</table>
</c:if>

<input type="hidden" id="fileGroupId_<c:out value="${param.editorId}"/>" name="fileGroupId_<c:out value="${param.editorId}"/>" value="<c:out value="${param.param_atchFileId}"/>"/>
<input type="hidden" id="fileCurrCount_<c:out value="${param.editorId}"/>" name="fileCurrCount_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
<input type="hidden" id="fileCurrSize_<c:out value="${param.editorId}"/>" name="fileCurrSize_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>
          