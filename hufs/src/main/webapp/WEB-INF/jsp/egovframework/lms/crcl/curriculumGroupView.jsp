<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:if test="${not empty searchVO.menuId}"><c:param name="menuId" value="${searchVO.menuId}" /></c:if>
    <c:if test="${not empty searchVO.crclId}"><c:param name="crclId" value="${searchVO.crclId}" /></c:if>
    <c:if test="${not empty searchVO.mngAt}"><c:param name="mngAt" value="${searchVO.mngAt}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
    <c:param name="contTitleAt">Y</c:param>
</c:import>

<script>
$(document).ready(function() {
});
</script>

          <!-- 콘텐츠헤더 -->
          <div class="page-content-header">
            <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
                <c:param name="dateFlag" value="curriculumrequest"/>
                <c:param name="curriculumManageBtnFlag" value="N"/>
            </c:import>
          </div>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <c:if test="${curriculumVO.processSttusCode > 0}">
                <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
                    <c:param name="step" value="2"/>
                    <c:param name="crclId" value="${curriculumVO.crclId}"/>
                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
                </c:import>
            </c:if>

            <!-- 세번째 tab -->
            <c:import url="/lms/curriculumtabmenu.do" charEncoding="utf-8">
                <c:param name="menu" value="crclstudent"/>
                <c:param name="tabstep" value="${param.tabstep }"/>
                <c:param name="menuId" value="${param.menuId }"/>
                <c:param name="crclId" value="${param.crclId}"/>
                <c:param name="crclbId" value="${param.crclbId}"/>
            </c:import>
            <article class="content-wrap">
              <div class="board-tab-wrap">
                <div class="slyWrap">
                  <ul class="board-tab-lists clear">
                    <c:forEach var="group" items="${selectGroupList}" varStatus="status">
                        <c:choose>
                            <c:when test="${group.groupCnt eq 0}"><c:set var="groupCntTab" value="조없음"/></c:when>
                            <c:otherwise><c:set var="groupCntTab" value="${group.groupCnt}개조"/></c:otherwise>
                        </c:choose>

                        <li class="<c:if test="${group.classCnt eq param.classCnt }">active</c:if>">
                            <a style="padding:0px;" href="/lms/crcl/curriculumGroupView.do?crclId=${curriculumVO.crclId}&crclbId=${curriculumVO.crclbId}&crclbId=${curriculumVO.crclbId}&classCnt=${group.classCnt }&menuId=${param.menuId }&subtabstep=${param.subtabstep}&thirdtabstep=2">${group.classCnt }반(${groupCntTab })</a>
                        </li>
                    </c:forEach>
                  </ul>
                </div>
                <div class="scrollbar">
                  <div class="handle"></div>
                </div>
              </div>
            </article>
            <article class="content-wrap">
              <c:forEach var="groupCnt" items="${groupCntList}" varStatus="status">
                  <c:set var="studentCnt" value="0"/>

                  <%-- 조원 명수 계산 --%>
                  <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
                      <c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
                          <c:set var="studentCnt" value="${studentCnt + 1 }"/>
                      </c:if>
                  </c:forEach>
	              <table class="common-table-wrap mb-2">
	                <tbody>
	                  <tr>
	                    <th class="w-25 font-point font-700">1조</th>
	                    <td>총 ${studentCnt }명</td>
	                  </tr>
	                  <tr>
	                    <td colspan="2" class="left-align wide-spacing p-20">
	                        <div>조장 :
		                        <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
			                        <c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
			                            <c:if test="${pickStudent.groupLeaderAt eq 'Y'}">
			                                ${pickStudent.userNm }
			                            </c:if>
			                        </c:if>
		                        </c:forEach>
	                        </div>
	                        <div>조원 :
                                <c:forEach var="pickStudent" items="${pickStudentList}" varStatus="status">
	                                <c:if test="${pickStudent.groupCnt eq groupCnt.groupCnt }">
	                                    <c:if test="${pickStudent.groupLeaderAt ne 'Y'}">
	                                        ${pickStudent.userNm }
	                                    </c:if>
	                                </c:if>
                                </c:forEach>
                            </div>
	                    </td>
	                  </tr>
	                </tbody>
	              </table>
	            </c:forEach>
	            <c:if test="${fn:length(groupCntList) eq '0' }">
	              <div class="no-content-wrap">
	                <p class="sub-title">조배정이 필요하신가요?<br> 조배정이 필요한 과정만 조배정 서비스를 이용하시면 됩니다.<br>
	                  <a href="/lms/crm/curriculumGroupView.do?menuId=MNU_0000000000000070&crclId=${param.crclId }&crclbId=${param.crclbId }&thirdtabstep=2&subtabstep=4&classCnt=1" class="btn-xl btn-point mt-35">조배정 진행하기</a>
	                </p>
	                <p class="sub-title mt-70">분반이 필요한 경우, 분반을 먼저 진행 후 조배정을 진행해주세요.<br>
	                  <a href="/lms/crm/curriculumClassView.do?menuId=MNU_0000000000000070&crclId=${param.crclId }&crclbId=${param.crclbId }&thirdtabstep=3&subtabstep=4" class="btn-xl btn-outline mt-35">(선택) 분반 진행하기</a>
	                </p>
	              </div>
                </c:if> 
            </article>
          </section>
        </div>
      </div>
    </div>
  </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
    <c:param name="modalAt" value="Y"/>
    <c:param name="shareAt" value="Y"/>
    <c:param name="curriculumRequestAt" value="Y"/>
</c:import>