<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_JS" value="/template/manage/js"/>
<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/manage/studyPlanUpt.do"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${curriculumVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty param.plId}"><c:param name="plId" value="${param.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<link rel="stylesheet" href="/lms/manage/css/common/table_staff.css?v=2">
<link rel="stylesheet" href="/lms/manage/css/common/modal.css?v=2">

<c:set var="_C_IMG" value="/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_C_LIB" value="/lib"/>
<c:set var="_C_HTML" value="/template/common/html"/>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_EDITOR_ID" value="spCn"/>

<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="${_C_LIB}/tinymce/js/tinymce/tinymce.min.js"></script>


<script type="text/javascript" src="${_C_LIB}/upload/upload.js" ></script>
<script type="text/javascript" src="${_C_JS}/board.js" ></script>

<script src="${_C_LIB}/jquery/jquery.ui.widget.js"></script>
<script src="${_C_LIB}/upload/jquery.iframe-transport.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-process.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-image.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-audio.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-video.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-validate.js"></script>
<script src="${_C_LIB}/upload/jquery.fileupload-ui.js"></script>

<script>
$(document).ready(function(){
	var adfile_config = {
			siteId:"<c:out value='${siteInfo.siteId}'/>",
			pathKey:"Study",
			appendPath:"<c:out value='${scheduleMngVO.plId}'/>",
			editorId:"${_EDITOR_ID}",
			fileAtchPosblAt:"Y",
			maxMegaFileSize:"1024",
			atchFileId:"${scheduleMngVO.atchFileId}"
		};

	fn_egov_bbs_editor(adfile_config);

	//수업방법 값 초기에 있는지 확인
	<c:if test="${not empty scheduleMngVO.courseId}">
		$(".online, .offline").hide();
		$(".spType").attr("disabled", true);
	</c:if>

	//평가 값 초기에 있는지 확인
	<c:if test="${fn:length(evtResultList) > 0}">
		$(".evtAt").attr("disabled", true);
	</c:if>

	//수업방법
	$(".spType").click(function(){
		var val = $(this).val();

		if(val == "Y"){
			$(".offline").hide();
			$(".online").show();
		}else{
			$(".offline").show();
			$(".online").hide();
		}
	});

	//수업방법 선택
	$("#offline, #online").change(function(){
		var val = $(this).val(),
			name = $(this).children("option:selected").text();

		if(val){
			$(".offline, .online").hide();
			$(".spType").attr("disabled", true);
			$(".box_spType").html("<li class='item bg-blue-light'>" + name + "<input type='hidden' name='courseId' value='"+val+"'/><button type='button' class='btn-remove btn_deltype' title='삭제'></button></li>");
		}

		//평가선택 시
		if(val == "CTG_0000000000000054"){
			$(".evtTr").show();
		}else{
			$(".evtTr").hide();
		}
	});

	//평가 선택
	$("#evtId").change(function(){
		var val = $(this).val(),
			name = $(this).children("option:selected").text();

		if($(".evtAt:checked").val() == "Y"){
			$(".box_evt").append("<li class='item bg-blue-light'>" + name + "<input type='hidden' name='evtIdList' value='"+ val +"'/><button type='button' class='btn-remove btn_delevt' title='삭제'></button></li>");
			$(".evtAt").attr("disabled", true);
		}else{
			$(".box_evt").html("");
		}
	});

	//평가 아니오 선택
	$(".evtAt").click(function(){
		if($(this).val() != "Y"){
			$(".box_evt").html("");
		}
	});

	//수업자료
	$(".btn_file1").click(function(){
		var tempHostCode = '${curriculumVO.hostCode }';
		window.open("/cmm/fms/studyFileList.do?hostCode="+tempHostCode, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=660,height=900");
		return false;
	});

	//수업영역
	var levelDepth2 = [];
	<c:forEach var="result" items="${levelList}" varStatus="status">
		<c:if test="${result.ctgryLevel eq '2'}">
			levelDepth2.push({upperCtgryId : '${result.upperCtgryId}', ctgryId : '${result.ctgryId}', ctgryNm : '${result.ctgryNm}'});
		</c:if>
	</c:forEach>

	$("#spLevel").change();
	$("#spLevel").change(function(){
		var ctgryId1 = $(this).val();

		$("#spLevelNum").html("");
		for(i = 0; i < levelDepth2.length; i++){
			if(levelDepth2[i].upperCtgryId == ctgryId1){
				var spLevel = "${scheduleMngVO.spLevelNum}";

				if(ctgryId1 == levelDepth2[i].ctgryId){
					$("#spLevelNum").append("<option value='"+levelDepth2[i].ctgryId+"' selected='selected'>"+levelDepth2[i].ctgryNm+"</option>");
				}else{
					$("#spLevelNum").append("<option value='"+levelDepth2[i].ctgryId+"'>"+levelDepth2[i].ctgryNm+"</option>");
				}

			}
		}
	});

	//저장
	$("#btn_save").click(function(){
		$("#detailForm").submit();
	});
});

//교수명 삭제
$(document).on("click", ".btn_delfac", function(){
	$(this).parents("li").remove();
	return false;
});

//달력
$(document).on("focus", ".btn_calendar", function(){
	$(this).datepicker({
	    dateFormat: "yy-mm-dd"
	});
});

//수업방법 삭제
$(document).on("click", ".btn_deltype", function(){
	$(this).parents(".box_spType").html("");
	$(".spType").attr("disabled", false);

	var val = $(".spType:checked").val();

	if(val == "Y"){
		$(".offline").hide();
		$(".online").show();
	}else{
		$(".offline").show();
		$(".online").hide();
	}

	return false;
});

//평가 삭제
$(document).on("click", ".btn_delevt", function(){
	$(this).parents("li").remove();
	$(".evtAt").attr("disabled", false);
	return false;
});

//참고자료 삭제
$(document).on("click",".refFile",function(){
	$(this).remove();
	$("#fileDelChk").val("Y");
	return false;
});

function vali(){
	$(".spType").attr("disabled", false);

	//파일
	$('#fileGroupId').val($('#fileGroupId_${_EDITOR_ID}').val());
	
	if(!$("#studySubject").val()){
		alert("수업 주제를 입력해주세요.");
		return false;
	}
	if(!$("input[name=startDt]").val()){
		alert("수업일을 선택해주세요.");
		return false;
	}
	/*
	if(!$("select[name=startTime]").val()){
		alert("수업 시작 시간을 선택해주세요.");
		return false;
	}
	if(!$("select[name=endTime]").val()){
		alert("수업 종료 시간을 선택해주세요.");
		return false;
	}
	*/
	if(!$("select[name=spLevel]").val()){
		alert("수업영역을 선택해주세요.");
		return false;
	}
	if(!$("select[name=spLevelNum]").val()){
		alert("수업영역을 선택해주세요.");
		return false;
	}
	if(!$("input[name=courseId]").val()){
		alert("수업방법을 선택해주세요.");
		return false;
	}
	if($("input[name=courseId]").val() == "CTG_0000000000000054" && $("input[name=evtAt]:checked").val() == "Y"){
		if(!$("input[name=evtIdList]").val()){
			alert("평가를 선택해주세요.");
			return false;
		}
	}
	if(!$("#spGoal").val()){
		alert("학습성과를 입력해주세요.");
		return false;
	}
	
	return;
}

//양식관리 파일
function file(id, name, strName){
	var inp = "",
		dpChk= "";

	for(i = 0; i < id.length; i++){
		dpChk = 0;
		$("input[class=refeFileList]").each(function(){
			if($(this).val() == id[i]){
				alert("이미 "+name[i]+"파일은 참고자료로 등록하셨습니다.");
				dpChk++;
			}
		});

		if(dpChk == 0){
			inp = "<a href='#' class='attachment icon-file font-gray refFile'><span class='text'>"+name[i]+"&nbsp;&nbsp;&nbsp;&nbsp;</span><span class='file-remove'>-</span><input type='hidden' value='"+id[i]+"' class='refeFileList' name='refeFileList'/></a>";
			$("#refeFile_box").append(inp);
			$("#fileDelChk").val("Y");
		}

		$("#refTr").show();
	}
}

//교원변경
function fnFacSave(){
	var html = "";

	$("#box_fac").html("");
	$("input[name=facCheck]:checked").each(function(i){
		var facId = $(this).val(),
			userNm = $(this).data("nm");
		//alert(facId);
		if(i > 0){
			html = ", ";
		}
		$("#box_fac").append(html + userNm + "<input type='hidden' name='facIdList' value='"+ facId +"'/>");
	});

	$("input[name=changeFacAt]").val("Y");
}
</script>
          <div class="page-content-header">
            <c:choose>
         		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
         			<c:import url="/lms/claHeader.do" charEncoding="utf-8">
         			</c:import>
         		</c:when>
         		<c:otherwise>
         			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
						<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
					</c:import>
         		</c:otherwise>
         	</c:choose>
			<!--
			<div class="util-wrap">
		      <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
		    </div>
		     -->
          </div>
          <form:form commandName="scheduleMngVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}" enctype="multipart/form-data" onsubmit="return vali();">
         	<input type="hidden" name=menuId value="${searchVO.menuId}"/>
         	<input type="hidden" name=plId value="${scheduleMngVO.plId}"/>
			<input type="hidden" name="crclId" value="${scheduleMngVO.crclId}"/>
			<input type="hidden" id="fileGroupId" name="fileGroupId" value="${scheduleMngVO.atchFileId}"/>
			<input type="hidden" id="fileDelChk" name="fileDelChk"/> <!-- 수업자료 삭제 체크 -->
			<input type="hidden" name="changeFacAt" value="N"/>
			
			<c:if test="${not empty param.step}"><input type="hidden" name="step" value="${param.step}"/></c:if>
			<c:if test="${not empty param.tabType}"><input type="hidden" name="tabType" value="${param.tabType}"/></c:if>
			<c:if test="${not empty param.subtabstep}"><input type="hidden" name="subtabstep" value="${param.subtabstep}"/></c:if>
			<c:if test="${not empty param.tabStep}"><input type="hidden" name="tabStep" value="${param.tabStep}"/></c:if>
			
			<form:hidden path="atchFileId"/>

	          <section class="page-content-body">
	            <article class="content-wrap">
	              <!-- tab style -->
	              <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
					<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
						<c:param name="step" value="5"/>
						<c:param name="crclId" value="${curriculumVO.crclId}"/>
						<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
						<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
					</c:import>
				  </c:if>
	        	</article>
	            <article class="content-wrap">
	              <!-- 1. 기본정보 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">기본정보</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <table class="common-table-wrap table-style2 ">
	                  <tbody>
	                    <tr>
	                      <th class="title">수업 주제 </th>
	                      <td>
	                      	<div class="flex-row">
	                          <div class="flex-col-8">
	                            <form:input path="studySubject" cssClass="table-input" required="true"/>
	                          </div>
	                        </div>
	                        <div class="flex-row">
	                          <div class="flex-col-auto">
	                            <div class="table-data">

	                             </div>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr>
	                      <th class="title">교원 명 </th>
	                      <td>
	                        <div class="flex-row">
	                          <div class="flex-col">
	                            <div id="box_fac" class="table-data">
	                              <c:forEach var="result" items="${facPlList}" varStatus="status">
									<c:out value="${result.userNm}"/>
									<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
									(${result.mngDeptNm})
									<input type='hidden' name='facIdList' value='${result.facId}'/>
								  </c:forEach>
								</div>
	                          </div>
	                          <div class="flex-col-auto">
	                            <button type='button' class='btn-sm-110 btn-outline-gray btnModalOpen' data-modal-type='staff_select'>교원변경</button> </div>
	                        </div>
	                      </td>
	                    </tr>
	                  </tbody>
	                </table>
	              </div>
	            </article>
	            <article class="content-wrap">
	              <!-- 2. 수업정보 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">수업정보</div>
	                </div>
	              </div>
	              <div class="content-body">
	                <table class="common-table-wrap table-style2">
	                  <tbody>
	                    <tr>
	                      <th class="title">수업일 </th>
	                      <td>
	                        <div class="flex-row">
	                          <div class="flex-col-4">
	                            <input type="text" name="startDt" class="ell date datepicker" value="${scheduleMngVO.startDt}" autocomplete="off" readonly="readonly" placeholder="수업일"/>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr>
	                      <th class="title">수업시간 </th>
	                      <td>
	                        <div class="flex-row">
	                          	<c:choose>
									<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
										<div class="flex-col-2">
											<select class="table-select select2 startTimeHH required" name="startTimeHH" data-select='style1'>
												<option value="">선택</option>
												<c:forEach var="result" begin="6" end="24" step="1">
													<c:set var="hour">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.startTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
												</c:forEach>
											</select>
										</div>
										<div class="flex-col-auto">
	                            		: </div>
										<div class="flex-col-2">
											<select class="table-select select2 startTimeMM required" name="startTimeMM" data-select='style1'>
												<option value="">선택</option>
												<c:forEach var="result" begin="0" end="55" step="5">
													<c:set var="minute">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.startTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
												</c:forEach>
											</select>
										</div>
									</c:when>
									<c:otherwise>
										<div class="flex-col-4">
											<select class="startTime table-select select2" name="startTime" data-select="style1">
												<c:forEach var="result" items="${camSchList}">
													<option value="${result.startTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.startTime eq result.startTime}">selected="selected"</c:if>>
														<c:out value="${result.period}"/>교시
														<c:out value="${fn:substring(result.startTime,0,2)}"/>:<c:out value="${fn:substring(result.startTime,2,4)}"/>
													</option>
												</c:forEach>
											</select>
										</div>
									</c:otherwise>
								</c:choose>
	                          	<div class="flex-col-auto">
	                            ~ </div>
	                          	<c:choose>
									<c:when test="${curriculumVO.campustimeUseAt eq 'N'}">
										<div class="flex-col-2">
											<select class="table-select select2 endTimeHH required" name="endTimeHH" data-select='style1'>
												<option value="">선택</option>
												<c:forEach var="result" begin="6" end="24" step="1">
													<c:set var="hour">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${hour}" <c:if test="${fn:substring(scheduleMngVO.endTime,0,2) eq hour}"> selected="selected"</c:if>>${hour}</option>
												</c:forEach>
											</select>
										</div>
										<div class="flex-col-auto">
	                            		: </div>
	                            		<div class="flex-col-2">
											<select class="table-select select2 endTimeMM required" name="endTimeMM" data-select='style1'>
												<option value="">선택</option>
												<c:forEach var="result" begin="0" end="55" step="5">
													<c:set var="minute">
														<c:choose>
															<c:when test="${result < 10}">0${result}</c:when>
															<c:otherwise>${result}</c:otherwise>
														</c:choose>
													</c:set>
													<option value="${minute}" <c:if test="${fn:substring(scheduleMngVO.endTime,2,4) eq minute}"> selected="selected"</c:if>>${minute}</option>
												</c:forEach>
											</select>
										</div>
									</c:when>
									<c:otherwise>
										<div class="flex-col-4">
											<select class="endTime table-select select2" name="endTime" data-select="style1">
												<c:forEach var="result" items="${camSchList}">
													<option value="${result.endTime}" data-period="${result.period}" <c:if test="${scheduleMngVO.endTime eq result.endTime}">selected="selected"</c:if>>
														<c:out value="${result.period}"/>교시
														<c:out value="${fn:substring(result.endTime,0,2)}"/>:<c:out value="${fn:substring(result.endTime,2,4)}"/>
													</option>
												</c:forEach>
											</select>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
	                      </td>
	                    </tr>
	                    <tr>
	                      <th class="title">수업 진행 장소 </th>
	                      <td>
	                        <div class="flex-row">
	                          <div class="flex-col-4">
	                            <select id="campusId" name="campusId" class="table-select select2" data-select="style1" required>
									<option value="">캠퍼스 선택</option>
									<c:forEach var="result" items="${campusList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq curriculumVO.campusId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
	                          </div>
	                          <div class="flex-col-4">
	                            <input type="text" class="table-input" name="placeDetail" value="${empty scheduleMngVO.placeDetail ? curriculumVO.campusPlace : scheduleMngVO.placeDetail}" placeholder="상세장소 입력"/>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr>
	                      <th class="title">수업영역 </th>
	                      <td>
	                        <div class="flex-row">
	                          <c:set var="lang" value="${empty scheduleMngVO.crclLang ? curriculumVO.crclLang : scheduleMngVO.crclLang}"/>
	                          <input type="hidden" name="crclLang" value="${lang}"/>
	                          <%--
	                          <div class="flex-col-4">
					  			<select id="crclLang" name="crclLang" class="autoTxt table-select select2" data-select="style1" required>
									<c:forEach var="result" items="${langList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq lang}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
	                          </div>
	                          --%>
	                          <div class="flex-col-4">
	                            <select id="spLevel" name="spLevel" class="table-select select2" data-select="style1" required>
									<c:forEach var="result" items="${levelList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.spLevel}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
	                          </div>
	                          <div class="flex-col-4">
	                          	<c:set var="spLevel" value="${empty scheduleMngVO.spLevel ? 'CTG_0000000000000128' : scheduleMngVO.spLevel}"/>
	                            <select id="spLevelNum" name="spLevelNum" class="table-select select2" data-select="style1" required>
									<c:forEach var="result" items="${levelList}" varStatus="status">
										<c:if test="${result.upperCtgryId eq spLevel and result.ctgryLevel eq '2'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.spLevelNum}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
								</select>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr>
	                      <th class="title">수업방법 </th>
	                      <td>
	                        <div class="flex-row">
	                          <div class="flex-col-auto">
	                            <label class="checkbox circle">
	                              <input type="radio" class="spType table-checkbox" name="spType" value="Y" <c:if test="${scheduleMngVO.spType ne 'N'}">checked="checked"</c:if>/>
	                              <span class="custom-checked"></span>
	                              <span class="text">강의식</span>
	                            </label>
	                          </div>
	                          <div class="flex-col-auto">
	                            <label class="checkbox circle">
	                              <input type="radio" class="spType table-checkbox" name="spType" value="N" <c:if test="${scheduleMngVO.spType eq 'N'}">checked="checked"</c:if>/>
	                              <span class="custom-checked"></span>
	                              <span class="text">비강의식</span>
	                            </label>
	                          </div>
	                          <div class="flex-col-3 online" <c:if test="${scheduleMngVO.spType eq 'N'}">style="display:none;"</c:if>>
	                            <select id="online" name="online" class="table-select select2 " data-select="style1" data-placeholder="선택">
				  					<option value="">선택</option>
				  					<c:forEach var="result" items="${onList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.courseId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
				  				</select>
	                          </div>
	                          <div class="flex-col-3 offline" <c:if test="${scheduleMngVO.spType ne 'N'}">style="display:none;"</c:if>>
	                          	<select id="offline" name="offline" class="table-select select2 " data-select="style1" data-placeholder="선택">
				  					<option value="">선택</option>
				  					<c:forEach var="result" items="${offList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}" <c:if test="${result.ctgryId eq scheduleMngVO.courseId}">selected="selected"</c:if>><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
				  				</select>
	                          </div>
	                          <div class="flex-col-12">
	                            <ul class="selected-items-wrap box_spType">
	                            	<c:if test="${not empty scheduleMngVO.courseId}">
	                              		<li class="item bg-blue-light">
	                              			<c:out value="${scheduleMngVO.courseNm}"/>
	                              			<input type='hidden' name='courseId' value='${scheduleMngVO.courseId}'/>
	                              			<button type="button" class="btn-remove btn_deltype" title="삭제"></button>
	                              		</li>
	                              	</c:if>
	                            </ul>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                    <tr class="evtTr" <c:if test="${scheduleMngVO.courseId ne 'CTG_0000000000000054'}">style="display:none;"</c:if>>
	                      <th class="title">평가 </th>
	                      <td>
	                        <div class="flex-row">
	                          <div class="flex-col-auto">
	                            <label class="checkbox circle">
	                              <input type="radio" class="evtAt table-checkbox" name="evtAt" value="N" <c:if test="${scheduleMngVO.evtAt eq 'N'}">checked="checked"</c:if>/>
	                              <span class="custom-checked"></span>
	                              <span class="text">아니오</span>
	                            </label>
	                          </div>
	                          <div class="flex-col-auto">
	                            <label class="checkbox circle">
	                              <input type="radio" class="evtAt table-checkbox" name="evtAt" value="Y" <c:if test="${scheduleMngVO.evtAt ne 'N'}">checked="checked"</c:if>/>
	                              <span class="custom-checked"></span>
	                              <span class="text">예</span>
	                            </label>
	                          </div>
	                          <div class="flex-col-3">
	                            <select id="evtId" name="evtId" class="table-select select2" data-select="style1" data-placeholder="선택">
				  					<option value="">선택</option>
				  					<c:forEach var="result" items="${evtList}" varStatus="status">
										<c:if test="${result.ctgryLevel eq '1'}">
											<option value="${result.ctgryId}"><c:out value="${result.ctgryNm}"/></option>
										</c:if>
									</c:forEach>
				  				</select>
	                          </div>
	                          <div class="flex-col-12">
	                            <ul class="selected-items-wrap box_evt">
	                            	<c:forEach var="result" items="${evtResultList}">
					  					<li class="item bg-blue-light">
						  					<c:out value="${result.evtNm}"/>
											<input type='hidden' name='evtIdList' value='${result.evtId}'/>
											<button type="button" class="btn-remove btn_delevt" title="삭제"></button>
										</li>
									</c:forEach>
	                            </ul>
	                          </div>
	                        </div>
	                      </td>
	                    </tr>
	                  </tbody>
	                </table>
	              </div>
	            </article>
	            <article class="content-wrap">
	              <!-- 수업내용 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">수업내용</div>
	                </div>
	                <!--
	                <div class="btn-group-wrap">
	                  <div class="right-area">
	                    <a href="#" class="btn-md-auto btn-outline btnModalOpen" data-modal-type="before_class">이전 수업계획서
	                      불러오기</a>
	                  </div>
	                </div>
	                 -->
	              </div>
	              <div class="content-body">
	                <div class="mb-20">
	                	<textarea id="spCn" name="spCn" rows="30" style="width:99%"><c:out value="${scheduleMngVO.spCn}"/></textarea>
	                </div>
	                
	                <p class="notice font-700" style="text-align:right;">※ 온라인 수업의 경우 수업자료를 반드시 등록해 주세요.</p>
	                <!-- 첨부파일 -->
	                <div class="file-attachment-wrap">
	                	<c:import url="/cmm/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
							<c:param name="editorId" value="${_EDITOR_ID}"/>
							<c:param name="estnAt" value="N" />
					    	<c:param name="param_atchFileId" value="${scheduleMngVO.atchFileId}" />
					    	<c:param name="imagePath" value="${_IMG }"/>
					    	<c:param name="regAt" value="Y"/>
					    	<c:param name="commonAt" value="Y"/>
					    	<c:param name="crclId" value="${scheduleMngVO.crclId}"/>
						</c:import>
	                </div>
	              </div>
	            </article>

	            <article class="content-wrap">
	              <table class="common-table-wrap table-style2 border-all-gray">
	                <tbody>
	                  <tr>
	                    <td>
	                      <div class="flex-row">
	                        <div class="flex-col">
	                          <input type="text" id="refe" class="table-input btn_file1" data-target="refe" placeholder="수업자료(이전 등록된 수업자료 추가)" readonly="readonly"/>
	                        </div>
	                        <div class="flex-col-auto">
	                          <button type='button' class='btn-sm-110 btn-outline-gray btn_file1'>추가</button> </div>
	                      </div>
	                    </td>
	                  </tr>
	                  <tr id="refTr" <c:if test="${fn:length(fileList) == 0}">style="display:none;"</c:if>>
	                  	<td>
	                  		<div class="file-attachment-wrap">
							  <div class="file-attachment-view">
							    <div id="refeFile_box" class="inner-area">
							    	<c:forEach var="fileVO" items="${fileList}" varStatus="status">
					              		<c:choose>
				            				<c:when test="${fn:toLowerCase(fileVO.fileExtsn) eq 'jpg' or fn:toLowerCase(fileVO.fileExtsn) eq 'jpeg' or fn:toLowerCase(fileVO.fileExtsn) eq 'png'}">
					             				<c:set var="ext" value="img"/>
					             			</c:when>
					             			<c:otherwise>
					             				<c:set var="ext" value="file"/>
					             			</c:otherwise>
					             		</c:choose>
								  		<a href="#" class="attachment icon-${ext} font-gray refFile">
					                    	<span class="text"><c:out value="${fileVO.orignlFileNm}"/>&nbsp;&nbsp;&nbsp;&nbsp;</span>
					                    	<span class="file-remove">-</span>
					                    	<input type='hidden' value='${fileVO.atchFileId}_${fileVO.fileSn}' class='refeFileList' name='refeFileList'/>
					                  	</a>
						        	</c:forEach>
							    </div>
							  </div>
						  </div>
						  <%--
	                      <ul id="refeFile_box">
								<c:forEach var="result" items="${fileList}" varStatus="status">
									<li class='refFileList'>
										<input type="hidden" value="${result.streFileNm}" class="refeFileList">
										<img src='/template/manage/images/ico_file.gif' alt='파일'/>
										${result.orignlFileNm}
										<a href='#' class='refFile'><img src='/template/manage/images/btn_sdelete.gif'/></a>
									</li>
								</c:forEach>
							</ul>
							 --%>
	                  	</td>
	                  </tr>
	                </tbody>
	              </table>
	            </article>

	            <article class="content-wrap">
	              <!-- 학습성과 -->
	              <div class="content-header">
	                <div class="title-wrap">
	                  <div class="title">학습성과</div>
	                </div>
	              </div>
	              <div class="content-body">
	              	<textarea id="spGoal" name="spGoal" rows="5" style="width:100%" class="content-textarea onlyText" placeholder="학습성과를 적어주세요."><c:out value="${scheduleMngVO.spGoal}"/></textarea>
	              </div>
	            </article>
	            <div class="page-btn-wrap mt-50">
				    <c:url var="viewUrl" value="/lms/manage/studyPlanView.do${_BASE_PARAM}">
				    	<c:param name="plId" value="${scheduleMngVO.plId}"/>
				    </c:url>
		            <a href="${viewUrl}" class="btn-xl btn-outline-gray">취소</a>
	              	<a href="#" id="btn_save" class="btn-xl btn-point">저장</a>
	            </div>
	          </section>
          </form:form>
      </div>

    </div>
  </div>
  </div>
</div>



<div id="before_class_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">이전 수업계획서 불러오기</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body before-quiz-wrap">
          <p class="modal-text">총괄평가 기준을 선택해주세요</p>
          <div class="box-wrap left-align mb-30">
            <div class="flex-row-ten">
              <div class="flex-ten-col">
                <div class="ell">
                  <select name="" id="" class="select2 select2-hidden-accessible" data-select="style3" data-placeholder="년도선택" tabindex="-1" aria-hidden="true">
                    <option value=""></option>
                    <option value="0">년도선택</option>
                  </select><span class="select2 select2-container select2-container--style3" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2--container"><span class="select2-selection__rendered" id="select2--container"><span class="select2-selection__placeholder">년도선택</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
              </div>
              <div class="flex-ten-auto">
                <button class="btn-sm font-400 btn-point">검색</button>
              </div>
            </div>
          </div>
          <table class="common-table-wrap size-sm">
            <colgroup>
              <col style="width:70px;">
              <col>
              <col style="width:138px">
            </colgroup>
            <thead>
              <tr class="bg-gray-light font-700">
                <th colspan="3">2018년 몽골어 초급과정 (2018-00-00 ~2018-00-00)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2018-00-00</td>
              </tr>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2018-00-00</td>
              </tr>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2018-00-00</td>
              </tr>
            </tbody>
          </table>
          <table class="common-table-wrap size-sm">
            <colgroup>
              <col style="width:70px;">
              <col>
              <col style="width:138px">
            </colgroup>
            <thead>
              <tr class="bg-gray-light font-700">
                <th colspan="3">2017년 몽골어 초급과정 (2017-00-00 ~2017-00-00)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2017-00-00</td>
              </tr>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2017-00-00</td>
              </tr>
              <tr>
                <td>
                  <label class="checkbox circle">
                    <input type="radio" name="name3" disabled>
                    <span class="custom-checked"></span>
                  </label>
                </td>
                <td class="left-align">
                  몽골어 자음/ 모음 퀴즈
                </td>
                <td>2017-00-00</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button class="btn-xl btn-point btnModalConfirm">선택한 수업계획서 불러오기</button>
        </div>
      </div>
    </div>
  </div>
  <div id="before_data_modal" class="alert-modal">
    <div class="modal-dialog modal-top">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">이전 수업자료 불러오기</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body before-quiz-wrap">
          <p class="modal-text">이전 수업자료를 선택해주세요</p>
          <div class="box-wrap left-align mb-30">
            <div class="flex-row-ten">
              <div class="flex-ten-col-5 mb-20">
                <div class="ell">
                  <select name="" id="" class="select2" data-select="style3" data-placeholder="과정명">
                    <option value=""></option>
                    <option value="0">과정명</option>
                  </select>
                </div>
              </div>
              <div class="flex-ten-col-5 mb-20">
                <div class="ell">
                  <input type="text" placeholder="파일등록자">
                </div>
              </div>
              <div class="flex-ten-col-12">
                <div class="ell">
                  <input type="text" placeholder="자료명">
                </div>
              </div>
              <div class="flex-ten-col-12 flex align-items-center">
                <label class="checkbox">
                  <input type="checkbox">
                  <span class="custom-checked"></span>
                  <span class="text">파일</span>
                </label>
                <label class="checkbox">
                  <input type="checkbox">
                  <span class="custom-checked"></span>
                  <span class="text">사진</span>
                </label>
                <label class="checkbox">
                  <input type="checkbox">
                  <span class="custom-checked"></span>
                  <span class="text">동영상</span>
                </label>
                <label class="checkbox">
                  <input type="checkbox">
                  <span class="custom-checked"></span>
                  <span class="text">나의 수업자료</span>
                </label>
              </div>
            </div>

            <button class="btn-sm font-400 btn-point mt-20">검색</button>

            <button class="btn-sm font-400 btn-outline mt-20">초기화</button>
          </div>
          <table class="common-table-wrap size-sm">
            <colgroup>
              <col style="width:7%;">
              <col style="width:7%;">
              <col style="width:9%;">
              <col style="width:21%;">
              <col style="width:21%;">
              <col style="width:9%;">
            </colgroup>
            <thead>
              <tr class="bg-gray-light font-700">
                <th>선택</th>
                <th>구분</th>
                <th>미리보기</th>
                <th>파일명</th>
                <th>과정명</th>
                <th>등록자</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><label class="checkbox"><input type="checkbox" class="checkList" checked><span class="custom-checked"></span></label></td>
                <td>파일</td>
                <td>
                  <div class="board-gallery-lists size-sm" style="background-image:url(../imgs/common/img_download_thumbnail_sm.jpg);"></div>
                </td>
                <td class="left-align">00 몽골어 중급회화.word</td>
                <td class="left-align">000 탄뎀과정</td>
                <td>홍길동</td>
              </tr>
              <tr>
                <td><label class="checkbox"><input type="checkbox" class="checkList"><span class="custom-checked"></span></label></td>
                <td>파일</td>
                <td>
                  <div class="board-gallery-lists size-sm" style="background-image:url(../imgs/common/img_download_thumbnail_sm.jpg);"></div>
                </td>
                <td class="left-align">00 몽골어 중급회화.word</td>
                <td class="left-align">000 탄뎀과정</td>
                <td>홍길동</td>
              </tr>
              <tr>
                <td><label class="checkbox"><input type="checkbox" class="checkList"><span class="custom-checked"></span></label></td>
                <td>파일</td>
                <td>
                  <div class="board-gallery-lists size-sm" style="background-image:url(../imgs/common/img_download_thumbnail_sm.jpg);"></div>
                </td>
                <td class="left-align">00 몽골어 중급회화.word</td>
                <td class="left-align">000 탄뎀과정</td>
                <td>홍길동</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button class="btn-xl btn-point btnModalConfirm">선택한 수업자료 등록</button>
        </div>
      </div>
    </div>
  </div>
  <div id="staff_select_modal" class="alert-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">교원 선택</h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <p class="modal-text">수업을 진행 할 교원을 선택해주세요</p>
          <table class="common-table-wrap size-sm">
            <colgroup>
              <col style="width:70px">
              <col>
            </colgroup>
            <thead>
              <tr class="bg-gray-light font-700">
                <th>선택</th>
                <th>교원</th>
              </tr>
            </thead>
            <tbody>
            	<c:forEach var="result" items="${facList}">
	              <tr>
	                <td>
	                  <label class="checkbox circle">
                   		<input type="checkbox" name="facCheck" <c:if test="${result.facId eq result2.facId}">checked</c:if> value="${result.facId}" data-nm="${result.facNm}">
	                    <span class="custom-checked"></span>
	                  </label>
	                </td>
	                <td><c:out value="${result.facNm}"/></td>
	              </tr>
	             </c:forEach>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
          <button class="btn-xl btn-point btnModalConfirm" onclick="fnFacSave();">선택</button>
        </div>
      </div>
    </div>
  </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>