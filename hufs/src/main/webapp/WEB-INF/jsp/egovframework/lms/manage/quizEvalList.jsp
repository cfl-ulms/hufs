<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value="/lms/quiz/QuizRegView.do"/>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
	<c:if test="${not empty param.step}"><c:param name="step" value="${param.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty param.tabType}"><c:param name="tabType" value="${param.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty param.tabStep}"><c:param name="tabStep" value="${param.tabStep}"></c:param></c:if>
</c:url>
<c:url var="_BASE_PARAM2" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId }"></c:param>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>
<c:choose>
	<c:when test="${USER_INFO.userSeCode eq '08' }">
		<link rel="stylesheet" href="/template/lms/css/quiz/staff_class.css">
		<link rel="stylesheet" href="/template/lms/css/quiz/modal_staff.css">
	</c:when>
	<c:otherwise>
		 <link rel="stylesheet" href="/template/lms/css/quiz/class.4.2.css?v=2">
	</c:otherwise>
</c:choose>
<script>
	$(document).ready(function(){
		$("#btn_start").click(function(){
			var plId = $(this).data("id"),
				quizType = $(this).data("type");
			
			$.ajax({
				url : "/lms/quiz/quizStart.do"
					, type : "post"
					, dataType : "json"
					, data : {"plId" : plId, "quizType" : quizType}
					, success : function(data){
						if(data.successYn == "Y"){
							$("#btn_start").text("퀴즈 진행중");
						}
					}, error : function(){
						alert("error");
					}
			});
		});
	});
	
	//레이어 닫기
	$(document).on("click", ".btnModalClose", function(){
		var id = $(this).parents(".alert-modal").attr("id");
		if(id == "no_contents_quiz_modal"){
			$("#no_contents_quiz_modal").hide();
		}else if(id == "online_quiz_thumbnail_modal"){
			$("#online_quiz_thumbnail_modal").hide();
		}else{
			$(this).parents(".alert-modal").remove();	
		}
	});
	

	function fn_pre_quiz(plId, quizSort, quizType){
		var preNum = 0;
		var nextNum = 0;
		$.ajax({
			url : "/lms/quiz/PreQuizListAjax.json"
				, type : "post"
				, dataType : "json"
				, data : {"plId" : plId, "quizSort" : quizSort, "quizType" : quizType}
				, success : function(data){
					if(data.successYn == "Y"){
						var preQuizList = data.preQuizList;
						if(preQuizList[0].quizType == "ON"){
							$("#online_quiz_thumbnail_modal").css("display", "block");
							$("#online_quiz_modal").css("display", "none");
							$("#totalCnt").text(preQuizList[0].quizSort+" / "+preQuizList[0].quizCnt);
							$("#quizCn").text("Q. "+preQuizList[0].quizNm);
							$("#answer1").text(preQuizList[0].answer1);
							$("#answer2").text(preQuizList[0].answer2);
							$("#answer3").text(preQuizList[0].answer3);
							$("#answer4").text(preQuizList[0].answer4);
							$("#answer5").text(preQuizList[0].answer5);
							if(preQuizList[0].quizSort == "1"){
								$("#preBtn").attr("disabled", true);
								$("#nextBtn").attr("disabled", false);
							}else if(preQuizList[0].quizSort == preQuizList[0].quizCnt){
								$("#preBtn").attr("disabled", false);
								$("#nextBtn").attr("disabled", true);
							}else{
								$("#preBtn").attr("disabled", false);
								$("#nextBtn").attr("disabled", false);
							}
							
							preNum = preQuizList[0].quizSort-1;			
							nextNum = preQuizList[0].quizSort+1;
							
							$("#preBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+preNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#nextBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+nextNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#updMove").attr("onclick", "fn_upd_move('"+preQuizList[0].quizId+"', '"+preQuizList[0].plId+"', '"+preQuizList[0].quizType+"'); return false;");
							
						}else{
							$("#online_quiz_thumbnail_modal").css("display", "none");
							$("#online_quiz_modal").css("display", "block");
							$("#totalCnt2").text(preQuizList[0].quizSort+" / "+preQuizList[0].quizCnt);
							$("#quizCn2").text("Q. "+preQuizList[0].quizSort+"번 문제입니다. 답안을 골라주세요.");
							if(preQuizList[0].quizSort == "1"){
								$("#preBtn2").attr("disabled", true);
								$("#nextBtn2").attr("disabled", false);
							}else if(preQuizList[0].quizSort == preQuizList[0].quizCnt){
								$("#preBtn2").attr("disabled", false);
								$("#nextBtn2").attr("disabled", true);
							}else{
								$("#preBtn2").attr("disabled", false);
								$("#nextBtn2").attr("disabled", false);
							}
							
							preNum = preQuizList[0].quizSort-1;			
							nextNum = preQuizList[0].quizSort+1;
							
							$("#preBtn2").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+preNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#nextBtn2").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+nextNum+"', '"+preQuizList[0].quizType+"'); return false;");
							$("#updMove2").attr("onclick", "fn_upd_move('"+preQuizList[0].quizId+"', '"+preQuizList[0].plId+"', '"+preQuizList[0].quizType+"'); return false;");
						}
					}
				}, error : function(){
					alert("error");
				}
		});
	}
	
	function fn_upd_move(quizId, plId, quizType){
		if(quizType == "ON"){
			$("#detailForm").attr("action", "/lms/quiz/QuizRegView.do${_BASE_PARAM2}&quizId="+quizId+"&plId="+plId+"&quizType="+quizType);
			$("#detailForm").submit();
		}else{
			$("#detailForm").attr("action", "/lms/quiz/QuizAnswerRegView.do${_BASE_PARAM2}&quizId="+quizId+"&plId="+plId+"&quizType="+quizType);
			$("#detailForm").submit();
		}
	}
	
	function del_all(quizType){
		if(confirm("퀴즈를 삭제하시겠습니까?\n삭제한 뒤에는 복귀 되지 않으며\n다시 등록을 하셔야 합니다.")){
			$("#detailForm").attr("action", "/lms/quiz/QuizDel.do${_BASE_PARAM}&quizType="+quizType);
			$("#delDiv").val("ALL");
			$("#pageDiv").val("eval");
			$("#detailForm").submit();	
		}
	}
	
	function fn_popup(url){
		window.open(url,'title','height='+screen.availHeight + ',width=' + screen.availWidth + 'fullscreen=yes');
	}
</script>

<div class="page-content-header">
	<c:choose>
		<c:when test="${not empty searchVO.plId}">
			<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
		</c:when>
		<c:otherwise>
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
		</c:otherwise>
	</c:choose>
</div>
<section class="page-content-body">
	<article class="content-wrap">
		<!-- tab style -->
		<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="4"/>
			<c:param name="crclId" value="${searchVO.crclId}"/>
			<c:param name="menuId" value="${searchVO.menuId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
    </article>
		<article class="content-wrap">
              <!-- 사용자 정보 -->
              <div class="card-user-wrap">
                <div class="user-icon"><img src="/template/lms/imgs/common/icon_user_name.svg" alt="사람 아이콘"></div>
                <div class="user-info">
                  <p class="title">담당교수 <b>
                  <c:forEach var="result" items="${facPlList}" varStatus="status">
						<c:out value="${result.userNm}"/>
						<c:if test="${result.manageCode eq '04'}">(부교원)</c:if>
						(${result.mngDeptNm})
						<c:set var="userMail" value="${result.facId }"/>
				  </c:forEach></b></p>
          <p class="sub-title">문의: ${userMail }</p>
        </div>
      </div>
    </article>
            
	<article class="content-wrap">
		<c:if test="${USER_INFO.userSeCode eq '08' or (USER_INFO.userSeCode ne '08' and beforeQuizList[0].quizEnd ne 'Y')}">
		
		   <!-- 퀴즈 -->
		   <c:forEach var="result" items="${beforeQuizList}" varStatus="status">
		       <div class="quiz-box-wrap">
		         <div class="quiz-flex-wrap">
		           <div class="quiz-flex-wrap">
		             <div class="quiz-text-wrap">
		             	<c:set var="schDt1" value="${fn:substring(result.plStartDt, '5', '7')}"/>
		             	<c:set var="schDt2" value="${fn:substring(result.plStartDt, '8', '11')}"/>
		               <p class="date">${schDt1}월 ${schDt2}일 ${result.dayOfWeek } ${result.periodTxt }교시</p>
		               <p class="title">[온라인퀴즈 /
		               		<c:choose>
		               			<c:when test="${result.quizType eq 'ON' }">문제등록형</c:when>
								<c:otherwise>답안등록형</c:otherwise>
							</c:choose>총 ${result.quizCnt}문제] ${result.studySubject} 퀴즈</p>
							<c:choose>
								<c:when test="${result.quizEnd eq 'N'}">
									<p class="desc">
										퀴즈가 아직 시작되지 않았습니다. 
										<%-- 해당 수업시간에 교수님의 안내에 따라 학생 휴대폰으로 퀴즈를 참여하실 수 있습니다. --%>
									</p>
								</c:when>
								<c:when test="${result.quizEnd eq 'P'}">
									<p class="desc">퀴즈가 진행중입니다. 퀴즈 시작하기 버튼을 클릭하여 퀴즈를 진행하세요.</p>
								</c:when>
								<c:otherwise>
									<p class="desc">퀴즈가 완료되었으며, [평가결과조회] 버튼을 누르시면 학생의 평가결과를 확인할 수 있습니다. 답안이 잘못 체크된 경우가 있으셨다면 [답안 수정] 으로 문제의 답을 수정하여 평가결과를 다시 확인할 수 있습니다.</p>
								</c:otherwise>
							</c:choose>
		             </div>
		           </div>
		           <ul class="quiz-btn-wrap">
					 <c:choose>
					 	<c:when test="${USER_INFO.userSeCode eq '08'}">
					 		 <c:choose>
					 		 	 <c:when test="${result.quizEnd eq 'N' or result.quizEnd eq 'P'}">
					 		 	 	 <li class="quiz-start">
					 		 	 	 	<a href="/lms/quiz/QuizSolveInfo.do${_BASE_PARAM}&quizType=${result.quizType}" id="btn_start" target="_blank" data-id="${result.plId}" data-type="${result.quizType}">
					 		 	 	 		<c:choose>
					 		 	 	 			<c:when test="${result.quizEnd eq 'P'}">퀴즈 진행중</c:when>
					 		 	 	 			<c:otherwise>퀴즈 시작하기</c:otherwise>
					 		 	 	 		</c:choose>
					 		 	 	 	</a>
					 		 	 	 </li>
						             <li class="btn-outline-gray"><a href="#" class="btnModalOpen" data-modal-type="online_quiz_thumbnail" onclick="fn_pre_quiz('${result.plId}', '1', '${result.quizType}'); return false;">문제 미리보기</a></li>
						             <c:choose>
					              		<c:when test="${result.quizType eq 'ON'}">
					              			<c:url var="detailViewUrl" value="/lms/quiz/QuizRegView.do${_BASE_PARAM}">
					              				<c:param name="quizType" value="${result.quizType }"></c:param>
					              				<c:param name="quizEnd" value="${result.quizEnd }"></c:param>
					              			</c:url>
					              		</c:when>
										<c:otherwise>
											<c:url var="detailViewUrl" value="/lms/quiz/QuizAnswerRegView.do${_BASE_PARAM}">
					              				<c:param name="quizType" value="${result.quizType }"></c:param>
					              				<c:param name="quizEnd" value="${result.quizEnd }"></c:param>
					              			</c:url>
										</c:otherwise>
									 </c:choose>
									 <li class="btn-outline-gray"><a href="${detailViewUrl }">문제 수정</a></li>
						             <li class="btn-outline-gray"><a href="#" class="btnModalOpen" data-modal-type="quiz_delete" onclick="del_all('${result.quizType }'); return false;" >퀴즈 삭제</a></li>
					 		 	 </c:when>
					 		 	 <c:otherwise>
					 		 	 	 <li class="quiz-start"><a href="/lms/quiz/QuiResultList.do${_BASE_PARAM}&quizType=${result.quizType}">평가결과조회</a></li>
				                     <li class="btn-outline-gray"><a href="#" class="btnModalOpen" data-modal-type="online_quiz_thumbnail" onclick="fn_pre_quiz('${result.plId}', '1', '${result.quizType}'); return false;">문제 미리보기</a></li>
				                     <c:choose>
				                     	<c:when test="${result.quizType eq 'OFF' }">
											<c:url var="answerUpdateViewUrl" value="/lms/quiz/QuizAnswerRegView.do${_BASE_PARAM}">
					              			 	<c:param name="quizType" value="${result.quizType }"></c:param>
					              			 	<c:param name="quizEnd" value="Y"></c:param>
					              			 </c:url>			                     	
				                     	</c:when>
				                     	<c:otherwise>
				                     		<c:url var="answerUpdateViewUrl" value="/lms/quiz/QuizRegView.do${_BASE_PARAM}">
					              			 	<c:param name="quizType" value="${result.quizType }"></c:param>
					              			 	<c:param name="quizEnd" value="Y"></c:param>
					              			 </c:url>
				                     	</c:otherwise>
				                     </c:choose>
				                     <li class="btn-outline-gray"><a href="${ answerUpdateViewUrl}">답안 수정</a></li>
					 		 	 </c:otherwise>
					 		 </c:choose>
					 	</c:when>
					 	<c:otherwise>
					 		<c:if test="${result.quizEnd eq 'P'}">
						 		<li class="quiz-start">
						 			<a href="/lms/quiz/QuizSolveInfo.do${_BASE_PARAM}&quizType=${result.quizType}" target="_blank">퀴즈 시작하기</a>
						 		</li>
					 		</c:if>
					 	</c:otherwise>
					 </c:choose>	           	 
		           </ul>
		         </div>
		       </div>
	       </c:forEach>
       </c:if>
       
       <!-- 퀴즈 평가결과 조회-->
       <c:if test="${USER_INFO.userSeCode eq '08' and beforeQuizList[0].quizEnd eq 'Y'}">
			<c:import url="/lms/quiz/QuiResultList.do" charEncoding="utf-8">
				<%-- 
				<c:param name="crclId" value="${searchVO.crclId}"/>
				<c:param name="plId" value="${searchVO.plId}"/>
				 --%>
				<c:param name="quizType" value="${beforeQuizList[0].quizType}"/>
				<c:param name="iptAt" value="Y"/>
			</c:import>
		</c:if>
		<c:if test="${USER_INFO.userSeCode ne '08' and beforeQuizList[0].quizEnd eq 'Y'}">
			<c:import url="/lms/quiz/QuiResultList.do" charEncoding="utf-8">
				<%-- 
				<c:param name="crclId" value="${searchVO.crclId}"/>
				<c:param name="plId" value="${searchVO.plId}"/>
				 --%>
				<c:param name="quizType" value="${beforeQuizList[0].quizType}"/>
				<c:param name="iptAt" value="Y"/>
			</c:import>
		</c:if>
     </article>
     
     <!-- 문제미리보기 영역 Start-->
     <!-- 문제등록형 -->
     <div id="online_quiz_thumbnail_modal" class="alert-modal" style="display: none;">
	    <div class="modal-dialog modal-top">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h4 class="modal-title">온라인 퀴즈(문제 등록형)</h4>
	          <button type="button" class="btn-modal-close btnModalClose"></button>
	        </div>
	        <div class="modal-body online-quiz-modal-wrap">
	          <p class="quiz-test-num" id="totalCnt"></p>
	          <p class="quiz-test-title" id="quizCn"></p>
	          <%-- <div class="quiz-text-img" style="background-image:url(../imgs/common/img_temporarily_05.jpg);"></div> --%>
	          <ul>
	          	<c:forEach begin="1" end="5" varStatus="status">
	          		<li class="quiz-test-items">
		              <label class="checkbox-num circle">
		                <input type="checkbox">
		                <span id="temp${status.count }" class="custom-checked"></span>
		                <span class="checkboxNum text">${status.count}</span>
		              </label>
		              <span id="answer${status.count }">정답 ${status.count}번입니다.</span>
		            </li>
	          	</c:forEach>
	          </ul>
	        </div>
	        <div class="modal-footer quiz-text-btn-wrap">
	          <div>
	            <button type="button" id="preBtn" class="btn-md-auto btn-outline-gray popup-icon-arrow-gray left">이전문제</button>
	            <button type="button" id="nextBtn" class="btn-md-auto btn-outline-gray popup-icon-arrow-gray">다음문제</button>
	          </div>
	          <button type="button" id="updMove" class="btn-sm btn-outline">수정하기</button>
	        </div>
	      </div>
	    </div>
	  </div>
	  
	  <!-- 답안등록형 -->
	  <div id='online_quiz_modal' class='alert-modal' style="display: none;">
		   <div class='modal-dialog modal-top'>
		     <div class='modal-content'>
		       <div class='modal-header'>
		         <h4 class='modal-title'>온라인 퀴즈(답안 등록형)</h4>
		         <button type='button' class='btn-modal-close btnModalClose'></button>
		       </div>
		       <div class='modal-body online-quiz-modal-wrap'>
		         <p class='quiz-test-num' id="totalCnt2"></p>
		         <p class='quiz-test-title' id="quizCn2"></p>
		         <ul>
		         	<c:forEach begin="1" end="5" varStatus="status">
		         		<li class='quiz-test-items'>
			             <label class='checkbox-num circle'>
			               <input type='checkbox'>
			               <span class='custom-checked'></span>
			               <span class='checkboxNum text'>${status.count}</span>
			             </label>
			             <span>정답 ${status.count}번입니다.</span>
			           </li>
		         	</c:forEach>
		         </ul>
		       </div>
		       <div class='modal-footer quiz-text-btn-wrap'>
		         <div>
		           <button type='button' id="preBtn2" class='btn-md-auto btn-outline-gray popup-icon-arrow-gray left' disabled=''>이전문제</button>
		           <button type='button' id="nextBtn2" class='btn-md-auto btn-outline-gray popup-icon-arrow-gray'>다음문제</button>
		         </div>
		         <button type='button' id="updMove2"class='btn-sm btn-outline'>수정하기</button>
		       </div>
		     </div>
		   </div>
		 </div>
	<!-- 문제미리보기 영역 End-->

	  <form:form commandName="quizVO" name="detailForm" id="detailForm" method="post" action="" enctype="multipart/form-data">
	  	<input type="hidden" id="delDiv" name="delDiv" value=""/>
	  	<input type="hidden" id="pageDiv" name="pageDiv" value=""/>
	  </form:form>
</section>
      
<c:import url="/template/bottom.do" charEncoding="utf-8"/>