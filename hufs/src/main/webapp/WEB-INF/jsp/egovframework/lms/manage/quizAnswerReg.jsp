<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_ACTION" value="/lms/quiz/QuizAnswerReg.do"/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"></c:param>
	<c:param name="crclId" value="${searchVO.crclId}"></c:param>
	<c:if test="${not empty searchVO.step}"><c:param name="step" value="${searchVO.step}"></c:param></c:if>
	<c:if test="${not empty searchVO.plId}"><c:param name="plId" value="${searchVO.plId}"></c:param></c:if>
	<c:if test="${not empty searchVO.tabType}"><c:param name="tabType" value="${searchVO.tabType}"></c:param></c:if>
	<c:if test="${not empty param.subtabstep}"><c:param name="subtabstep" value="${param.subtabstep}"></c:param></c:if>
	<c:if test="${not empty searchVO.tabStep}"><c:param name="tabStep" value="${searchVO.tabStep}"></c:param></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>
  <link rel="stylesheet" href="/template/lms/css/quiz/modal.css">
  <link rel="stylesheet" href="/template/lms/css/quiz/staff_class.css">
  <script src="${_C_JS}/quiz/staff_class_online_quiz.js"></script>
  <script src="${_C_JS}/quiz/jquery.nestable.js"></script>
<script>
	$(document).ready(function(){
		
		//저장
		$("#btn_save").click(function(){
			
			// validation
			var grpl = $("input[name=answerNum]").length;
			var grparr = new Array(grpl);
			var grparr2 = "";
			var answer = 0;
			
			for(var i=0; i<grpl; i++){
				grparr[i] = $("input[name=answerNum]").eq(i).val();
				answer = 0;
				if(grparr[i].indexOf(",") != -1){
					grparr2 = grparr[i].split(",");
					for(var j=0; j<grparr2.length; j++){
						answer = parseInt( grparr2[j] );
						if(!isNumber(answer)){
							alert("정답은 1 ~ 5 까지 숫자만 입력 가능 합니다.");
							return false;
						}
						if( answer < 1 || answer > 5 ){
							alert("정답은 1 ~ 5 까지 숫자만 입력 가능 합니다.");
							return false;
						}
					}
				}else{
					answer = grparr[i];
					if(!isNumber(answer)){
						alert("정답은 1 ~ 5 까지 숫자만 입력 가능 합니다.");
						return false;
					}
					
					if( answer < 1 || answer > 5 ){
						alert("정답은 1 ~ 5 까지 숫자만 입력 가능 합니다.");
						return false;
					}
				}	        
		    }
			$("#detailForm").attr("action", "${_ACTION}");
			$("#detailForm").submit();
		});
	});

//삭제
$(document).on("click", ".btn_del", function(){
	$(this).parents(".box-quiz").remove();
	
	$(".box-quiz").each(function(i){
		$(this).find(".quiz-num").text(i + 1 + "번");
	});
	
	return false;
});
	
	function fn_reg_view_move(){
		document.detailForm.action = "/lms/quiz/QuizRegView.do";
		document.detailForm.submit();
	}
	
	function isNumber(s) {
	  s += ''; // 문자열로 변환
	  s = s.replace(/^\s*|\s*$/g, ''); // 좌우 공백 제거
	  if (s == '' || isNaN(s)) return false;
	  return true;
	}
	
	function fn_reg(){
		var ansCnt =  $('input[name=answerNum]').length+1;
		$(".flex-row").append("<div class='box-quiz flex-col-4 mb-20'>"
								+"	<div class='quiz-online-wrap2'>"
								+"		<p class='quiz-num'>"+ansCnt+"번</p>"
								+"		<input type='text' name='answerNum' value='' maxlength='20'> <a href='#' class='btn_del'><img src='/template/lms/imgs/common/icon_close_gray.png' alt='삭제'></a>"
								+"	</div>"
								+"</div>"
							);
	}

	function fn_del_s(count){
		$("#result"+count).html("");
	}
	
	
	function fn_pre_quiz(plId, quizSort){
		var preNum = 0;
		var nextNum = 0;
		$.ajax({
			url : "/lms/quiz/PreQuizListAjax.json"
				, type : "post"
				, dataType : "json"
				, data : {"plId" : plId, "quizSort" : quizSort, "quizType" : "OFF"}
				, success : function(data){
					if(data.successYn == "Y"){
						var preQuizList = data.preQuizList;
						$("#totalCnt").text(preQuizList[0].quizSort+" / "+preQuizList[0].quizCnt);
						$("#quizCn").text("Q. "+preQuizList[0].quizSort+"번 문제입니다. 답안을 골라주세요.");
						if(preQuizList[0].quizSort == "1"){
							$("#preBtn").attr("disabled", true);
							$("#nextBtn").attr("disabled", false);
						}else if(preQuizList[0].quizSort == preQuizList[0].quizCnt){
							$("#preBtn").attr("disabled", false);
							$("#nextBtn").attr("disabled", true);
						}else{
							$("#preBtn").attr("disabled", false);
							$("#nextBtn").attr("disabled", false);
						}
						
						preNum = preQuizList[0].quizSort-1;			
						nextNum = preQuizList[0].quizSort+1;
						$("#preBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+preNum+"'); return false;");
						$("#nextBtn").attr("onclick", "fn_pre_quiz('"+preQuizList[0].plId+"', '"+nextNum+"'); return false;");
					}
				}, error : function(){
					alert("error");
				}
		})
	}
	
	function fn_end(){
		alert("퀴즈가 종료 및 진행 중 상태여서 문제 추가/삭제를 할 수 없습니다.\n답안 수정만 가능합니다.");
		return false;
	}
</script>

<div class="page-content-header">
	<c:choose>
		<c:when test="${not empty param.plId and param.tabType eq 'T'}">
			<c:import url="/lms/claHeader.do" charEncoding="utf-8"></c:import>
		</c:when>
		<c:otherwise>
			<c:import url="/lms/crclHeader.do" charEncoding="utf-8">
			<%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
			</c:import>
		</c:otherwise>
	</c:choose>
</div>

<section class="page-content-body">
	<article class="content-wrap">
		<!-- tab style -->
		<c:import url="/lms/tabmenu.do" charEncoding="utf-8">
			<c:param name="step" value="4"/>
			<c:param name="crclId" value="${searchVO.crclId}"/>
			<c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
			<c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
		</c:import>
    </article>
    <form:form commandName="quizVO" name="detailForm" id="detailForm" method="post" action="${_ACTION}">
    	<input type="hidden" name=menuId value="${searchVO.menuId}"/>
        <input type="hidden" name=plId value="${searchVO.plId}"/>
		<input type="hidden" name="crclId" value="${searchVO.crclId}"/>
		<input type="hidden" name="quizType" value="OFF"/>
		<input type="hidden" name="quizEnd" value="${searchVO.quizEnd }"/>
		
		<input type="hidden" name="step" value="${searchVO.step }"/>
		<input type="hidden" name="tabType" value="${searchVO.tabType }"/>
		<input type="hidden" name="subtabstep" value="${param.subtabstep }"/>
		<input type="hidden" name="tabStep" value="${searchVO.tabStep }"/>
		
		<article class="content-wrap">
			<!-- 온라인 퀴즈 [답안 등록형] -->
			<div class="content-header">
				<div class="title-wrap">
					<div class="title">온라인 퀴즈 [답안 등록형]</div>
					<div class="desc-sm">
						답가지 선택형 형성평가는 5지선다형으로만 진행이 가능합니다. (정답은 1번부터 5번까지)<br> 정답이 2개인
						경우 콤마(,)로 구분하여 입력해 주셔야 합니다.
					</div>
				</div>
				<c:choose>
					<c:when test="${searchVO.quizEnd eq 'Y' or searchVO.quizEnd eq 'P'}">
						<button type="button" class="btn-sm btn-point" onclick="fn_end(); return;">답안등록하기</button>
					</c:when>
					<c:otherwise>
						<button type="button" class="btn-sm btn-point" onclick="fn_reg(); return;">답안등록하기</button>
					</c:otherwise>
				</c:choose>
			</div>
			<hr class="line-hr mb-20">
			<div class="content-body">
				<div class="flex-row">
					<c:forEach var="result" items="${resultList}" varStatus="status">
						<c:if test="${searchVO.quizEnd eq 'Y' or searchVO.quizEnd eq 'P'}">
							<input type="hidden" name="quizIdArray" value="${result.quizId }">
						</c:if>
						<div class="box-quiz flex-col-4 mb-20" id="result${status.count }">
							<div class="quiz-online-wrap2">
								<p class="quiz-num">${status.count}번</p>
								<input type="text" name="answerNum" value="${result.answerNum }" maxlength="20">
								<c:choose>
									<c:when test="${searchVO.quizEnd eq 'Y' or searchVO.quizEnd eq 'P'}">
										<a href="#" onclick="fn_end(); return;">
									</c:when>
									<c:otherwise>
										<a href="#" class="btn_del">
									</c:otherwise>
								</c:choose>
									<img src="/template/lms/imgs/common/icon_close_gray.png" alt="삭제">
								</a>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</article>

		<div class="page-btn-wrap mt-50">
			<c:choose>
				<c:when test="${totCnt > 0 }">
					<c:url var="viewUrl" value="/lms/quiz/QuizEvalList.do${_BASE_PARAM}" />
				</c:when>
				<c:otherwise>
					<c:url var="viewUrl" value="/lms/quiz/QuizList.do${_BASE_PARAM}" />
				</c:otherwise>
			</c:choose>
			<a href="${viewUrl}" class="btn-xl btn-outline-gray">취소</a> 
			<a href="#" class="btn-xl btn-outline btnModalOpen" data-modal-type="online_quiz" onclick="fn_pre_quiz('${searchVO.plId}', '1'); return false;">퀴즈 미리보기</a> 
			<a href="#" id="btn_save" class="btn-xl btn-point">
			<c:choose>
				<c:when test="${searchVO.quizEnd eq 'Y' or searchVO.quizEnd eq 'P'}">수정</c:when>
				<c:otherwise>등록</c:otherwise>
			</c:choose>
			</a>
		</div>
	</form:form>
		<div id='online_quiz_modal' class='alert-modal'>
		   <div class='modal-dialog modal-top'>
		     <div class='modal-content'>
		       <div class='modal-header'>
		         <h4 class='modal-title'>온라인 퀴즈(답안 등록형)</h4>
		         <button type='button' class='btn-modal-close btnModalClose'></button>
		       </div>
		       <div class='modal-body online-quiz-modal-wrap'>
		         <p class='quiz-test-num' id="totalCnt"></p>
		         <p class='quiz-test-title' id="quizCn"></p>
		         <ul>
		         	<c:forEach begin="1" end="5" varStatus="status">
		         		<li class='quiz-test-items'>
			             <label class='checkbox-num circle'>
			               <input type='checkbox'>
			               <span class='custom-checked'></span>
			               <span class='checkboxNum text'>${status.count}</span>
			             </label>
			             <span>정답 ${status.count}번입니다.</span>
			           </li>
		         	</c:forEach>
		         </ul>
		       </div>
		       <div class='modal-footer quiz-text-btn-wrap'>
		         <div>
		           <button type='button' id="preBtn" class='btn-md-auto btn-outline-gray popup-icon-arrow-gray left' disabled=''>이전문제</button>
		           <button type='button' id="nextBtn" class='btn-md-auto btn-outline-gray popup-icon-arrow-gray'>다음문제</button>
		         </div>
		         <!-- <button type='button' class='btn-sm btn-outline'>수정하기</button> -->
		       </div>
		     </div>
		   </div>
		 </div>

</section>

<c:import url="/template/bottom.do" charEncoding="utf-8"/>