<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>

<% /*날짜 정의*/ %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmm" var="nowDate" /> 

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${searchVO.menuId}"/>
    <c:param name="crclId" value="${searchVO.crclId}"/>
    <c:param name="step" value="${searchVO.step}"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import>

<script>
$(document).ready(function(){
	//제출대기 학생
	$(document).on("click", ".submitWaitingList, .testWaitingList", function() {
		$(".modal-title").text("제출 대기 학생");
		$("#waitingMemberList").html("");

		$.ajax({
            type:"post",
            dataType:"html",
            url:"/lms/crcl/homeworkWaitingMemberListAjax.do",
            data:{crclId:"${param.crclId}", hwId:$(this).data("hwid"), hwType:$(this).data("hwtype"), hwWaitingType:$(this).data("hwwaitingtype")},
            success: function(data) {
                $("#waitingMemberList").html(data);
            }.bind(this),
            error:function() {
                alert("관리자에게 문의 바랍니다.");
            }
        });
    });
	
	//평가대기 학생
    $(document).on("click", ".testWaitingList", function() {
    	$(".modal-title").text("평가 대기 학생");
    });
});
</script>

<div class="page-content-header">
  <c:import url="/lms/crclHeader.do" charEncoding="utf-8">
      <%-- TODO : ywkim 작업해야함 --%>
      <%-- <c:param name="crclId" value="${curriculumVO.crclId}"/> --%>
  </c:import>
  <!-- TODO : ywkim 작업해야함
  <div class="util-wrap">
    <button type="button" class="btn-md btn-outline-gray">PDF 다운로드</button>
  </div>
   -->
</div>
<section class="page-content-body">
            <article class="content-wrap">
	            <c:if test="${curriculumVO.processSttusCode > 0 and managerAt eq 'Y'}">
	                <c:import url="/lms/tabmenu.do" charEncoding="utf-8">
	                    <c:param name="step" value="${searchVO.step }"/>
	                    <c:param name="crclId" value="${curriculumVO.crclId}"/>
	                    <c:param name="totalTimeAt" value="${curriculumVO.totalTimeAt}"/>
	                    <c:param name="processSttusCodeDate" value="${curriculumVO.processSttusCodeDate}"/>
	                </c:import>
	            </c:if>
            </article>
            <article class="content-wrap">
              <!-- 페이지 탭메뉴 -->
              <div class="content-header">
                <div class="class-tab-wrap">
                  <a href="/lms/crcl/homeworkList.do?menuId=${param.menuId }&crclId=<c:out value="${param.crclId}"/>&step=8&thirdtabstep=1" class="title <c:if test="${param.thirdtabstep eq '1' }">on</c:if>">과제</a>
                  <a href="/lms/crcl/homeworkTestList.do?menuId=${param.menuId }&crclId=<c:out value="${param.crclId}"/>&step=8&thirdtabstep=2" class="title <c:if test="${param.thirdtabstep eq '2' }">on</c:if>">과제평가</a>
                </div>
                <div class="btn-group-wrap">
                  <div class="right-area">
                    <%-- 과정 시작일 이후에 과제가 등록 되도록 처리하기 위한 날짜 --%>
                    <fmt:parseDate var="startDate_D"  value="${curriculumVO.startDate }" pattern="yyyy-MM-dd"/>
                    <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="nowDateHyphen3"/>
                    <fmt:parseDate value="${nowDateHyphen3}" pattern="yyyy-MM-dd" var="now_D" />
                    
                    <fmt:parseNumber var="startDate_N" value="${startDate_D.time / (1000*60*60*24)}" integerOnly="true" />
                    <fmt:parseNumber var="now_N" value="${now_D.time / (1000*60*60*24)}" integerOnly="true" />

                    <c:if test="${managerAt eq 'Y' and startDate_N <= now_N}">
                        <a href="/lms/crcl/homeworkRegister.do?registAction=regist&mode=curriculum&menuId=${param.menuId }&crclId=${param.crclId }&step=${param.step }" class="btn-md btn-outline">과제등록</a>
                    </c:if>
                  </div>
                </div>
              </div>
              <div class="content-body">
                <!-- 테이블영역-->
                <table class="common-table-wrap table-type-board">
                  <colgroup>
                    <col style='width:5%'>
                    <col style='width:7%'>
                    <col style='width:7%'>
                    <col style='width:10%'>
                    <col style='width:20%'>
                    <col style='width:8%'>
                    <col style='width:100px'>
                    <col style='width:7%'>
                    <col style='width:7%'>
                    <col style='width:5.5%'>
                    <col style='width:7%'>
                  </colgroup>
                  <thead>
                    <tr class='bg-light-gray font-700'>
                      <th scope='col'>No</th>
                      <th scope='col' colspan='2'>구분</th>
                      <th scope='col'>수업주제</th>
                      <th scope='col'>과제명</th>
                      <th scope='col'>교원명</th>
                      <th scope='col'>제출기간</th>
                      <th scope='col'>마감일</th>
                      <th scope='col'>과제상태</th>
                      <th scope='col'>학생<span class='inline-block'>공개</span></th>
                      <th scope='col'>제출현황</th>
                      <th scope='col'>평가현황</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach var="result" items="${selectHomeworkList}" varStatus="status">
                        <c:url var="viewUrl" value="/lms/crcl/selectHomeworkArticle.do${_BASE_PARAM}">
                            <c:if test="${not empty param.pageIndex}"><c:param name="pageIndex" value="${param.pageIndex}" /></c:if>
                            <c:param name="boardType" value="list" />
                            <c:param name="hwId" value="${result.hwId }" />
                        </c:url>
	                    <tr class="">
	                      <td onclick="location.href='${viewUrl}'" scope='row'><c:out value="${paginationInfo.totalRecordCount - ((searchVO.pageIndex-1) * searchVO.pageUnit) - (status.count - 1)}" /></td>
	                      <td onclick="location.href='${viewUrl}'" class='<c:if test="${result.hwCode eq 1 }">font-point</c:if>'>${result.hwCodeNm}</td>
	                      <td onclick="location.href='${viewUrl}'">${result.hwTypeNm}</td>
	                      <td onclick="location.href='${viewUrl}'" class='left-align'>${result.studySubject}</td>
	                      <td onclick="location.href='${viewUrl}'" class='left-align keep-all'>${result.nttSj}</td>
	                      <td onclick="location.href='${viewUrl}'">${result.ntcrNm}</td>
	                      <td onclick="location.href='${viewUrl}'">${result.openDate}<br>~<br>${result.closeDate}</td>
	                      <td onclick="location.href='${viewUrl}'" class='keep-all'>${result.closeDate}</td>
	                      
	                      <%-- 날짜 비교를 위한 데이터 컨버팅 --%>
	                      <c:set var="openDate" value="${fn:replace(result.openDate, '-', '')}${result.openTime }"/>
	                      <fmt:parseDate value="${openDate}" var="openParseDate" pattern="yyyyMMddHHmm"/>
	                      <fmt:formatDate value="${openParseDate}" pattern="yyyyMMddHHmm" var="openDateForm"/>
	                      
	                      <c:set var="closeDate" value="${fn:replace(result.closeDate, '-', '')}${result.closeTime }"/>
                          <fmt:parseDate value="${closeDate}" var="closeParseDate" pattern="yyyyMMddHHmm"/>
                          <fmt:formatDate value="${closeParseDate}" pattern="yyyyMMddHHmm" var="closeDateForm"/>

	                      <td>
                              <c:choose>
                                  <c:when test="${openDateForm > nowDate }">등록대기</c:when>
                                  <c:when test="${closeDateForm < nowDate or result.stuOpenAt eq 'Y'}">마감</c:when>
                                  <c:otherwise>제출중</c:otherwise>
                              </c:choose>
                          </td>
	                      <td>
                              <c:choose>
                                  <c:when test="${result.stuOpenAt eq 'Y' }">공개</c:when>
                                  <c:otherwise>대기</c:otherwise>
                              </c:choose>
                          </td>
	                      <td>
	                          <c:choose>
	                              <c:when test="${result.homeworkSubmitCnt eq result.memberCnt }">전원제출</c:when>
	                              <c:otherwise>
	                                  <a href="#" class="underline btnModalOpen submitWaitingList"  data-modal-type="waiting_list" data-hwid="${result.hwId }" data-hwtype="${result.hwType }" data-hwwaitingtype="1">
		                                  ${result.homeworkSubmitCnt}/${result.memberCnt }
		                              </a>
	                              </c:otherwise>
	                          </c:choose>
	                      </td>
	                      <td>
	                          <c:choose>
		                          <c:when test="${result.homeworkScrCnt eq result.memberCnt }">평가완료</c:when>
	                              <c:otherwise>
	                                  <a href="#" class="underline btnModalOpen testWaitingList"  data-modal-type="waiting_list" data-hwid="${result.hwId }" data-hwtype="${result.hwType }" data-hwwaitingtype="2">
		                                  ${result.homeworkScrCnt}/${result.memberCnt }
		                              </a>
	                              </c:otherwise>
                              </c:choose>
        	              </td>
	                    </tr>
                    </c:forEach>
                    <c:if test="${fn:length(selectHomeworkList) == 0}">
	                    <tr>
	                        <td class="listCenter" colspan="12"><spring:message code="common.nodata.msg" /></td>
	                    </tr>
	                </c:if>
                  </tbody>
                </table>
                <div class="pagination center-align mt-60">
	                <div class="pagination-inner-wrap overflow-hidden inline-block">
	                    <c:url var="startUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="1" />
	                    </c:url>
	                    <button class="start goPage" data-url="${startUrl}"></button>
	                   
	                    <c:url var="prevUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${searchVO.pageIndex > 1 ? searchVO.pageIndex - 1 : 1}"/>
	                    </c:url>
	                    <button class="prev goPage" data-url="${prevUrl}"></button>
	                   
	                    <ul class="paginate-list f-l overflow-hidden">
	                        <c:url var="pageUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM}"/>
	                        <c:set var="pagingParam"><c:out value="${pageUrl}"/></c:set>
	                        <ui:pagination paginationInfo="${paginationInfo}" type="smart_school" jsFunction="${pagingParam}" />
	                    </ul>
	                   
	                    <c:url var="nextUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${searchVO.pageIndex < paginationInfo.totalPageCount ? searchVO.pageIndex + 1 : searchVO.pageIndex}" />
	                    </c:url>
	                    <button class="next goPage" data-url="${nextUrl}"></button>
	                   
	                    <c:url var="endUrl" value="/lms/crcl/homeworkList.do${_BASE_PARAM}">
	                        <c:param name="pageIndex" value="${paginationInfo.totalPageCount}"/>
	                    </c:url>
	                    <button class="end goPage" data-url="${endUrl}"></button>
	                </div>
	            </div>
              </div>
            </article>
          </section>
        </div>

</div>
</div>
</div>

  <div id="waiting_list_modal" class="alert-modal">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="btn-modal-close btnModalClose"></button>
        </div>
        <div class="modal-body">
          <div class="modal-scroll-wrap">
            <table class="common-table-wrap font-700 left-align">
              <tbody id="waitingMemberList">
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button class="btn-xl btn-point btnModalConfirm">확인</button>
          </div>
        </div>
      </div>
    </div>
    
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>