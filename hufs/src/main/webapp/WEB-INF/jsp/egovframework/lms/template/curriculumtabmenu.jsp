<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>

	<c:choose>
	    <%-- 나의 교육과정 --%>
		<c:when test="${param.menu eq 'my' }">
			<c:url var="tabUrl" value="/lms/crm/myCurriculumView.do">
				<c:param name="crclId" value="${param.crclId}"/>
				<c:param name="crclbId" value="${param.crclbId}"/>
				<c:param name="menuId" value="${param.menuId}"/>
				<c:param name="tabStep" value="${param.tabStep}"/>
				<c:param name="subtabstep" value="1"/>
			</c:url>
			<article class="content-wrap">
              <!-- tab style -->
              <ul class="tab-wrap sub-tab-style">
                <li class="tab-list <c:if test="${param.subtabstep eq 1 }">on</c:if>"><a href="${tabUrl}">과정계획</a></li>

                <c:if test="${param.totalTimeAt eq 'Y' }">
                    <%-- <li class="tab-list <c:if test="${param.subtabstep eq 2 }">on</c:if>"><a href="#">수업계획</a></li> --%>
                    <%-- <li class="tab-list <c:if test="${param.subtabstep eq 2 }">on</c:if>"><a href="#">평가</a></li> --%>
                </c:if>
                
                <li class="tab-list <c:if test="${param.subtabstep eq 3 }">on</c:if>"><a href="/lms/manage/homeworkList.do?crclId=${param.crclId }&crclbId=${param.crclbId }&subtabstep=3&tabStep=${param.tabStep}&menuId=${param.menuId}&menu=my">과제</a></li>
                <li class="tab-list <c:if test="${param.subtabstep eq 4 }">on</c:if>"><a href="/lms/cla/curriculumBoardList.do?menuId=${param.menuId}&crclId=${param.crclId }&subtabstep=4&menu=my">게시판</a></li>
                <li class="tab-list <c:if test="${param.subtabstep eq 5 }">on</c:if>"><a href="/lms/cla/myClassSurveyList.do?menuId=${param.menuId}&crclId=${param.crclId }&subtabstep=5&menu=my">설문</a></li>
              </ul>
            </article>
		</c:when>
		<%-- 나의 교육과정 시간표 --%>
		<c:when test="${param.menu eq 'myschedule' }">
			<c:url var="tabUrl" value="/lms/crm/myCurriculumView.do">
				<c:param name="crclId" value="${param.crclId}"/>
				<c:param name="crclbId" value="${param.crclbId}"/>
				<c:param name="menuId" value="${param.menuId}"/>
				<c:param name="tabStep" value="${param.tabStep}"/>
			</c:url>
			<c:url var="fileUrl" value="/lms/cla/classFileList.do">
				<c:param name="crclId" value="${param.crclId}"/>
				<c:param name="crclbId" value="${param.crclbId}"/>
				<c:param name="menuId" value="${param.menuId}"/>
				<c:param name="tabStep" value="${param.tabStep}"/>
			</c:url>
			<article class="content-wrap">
              <!-- tab style -->
              <ul class="tab-wrap sub-tab-style">
                <li class="tab-list <c:if test="${param.subtabstep eq 1 }">on</c:if>"><a href="${tabUrl }&subtabstep=1">전체</a></li>
                <li class="tab-list <c:if test="${param.subtabstep eq 2 }">on</c:if>"><a href="${tabUrl }&subtabstep=2&todayFlag=Y">오늘 수업</a></li>
               	<li class="tab-list <c:if test="${param.subtabstep eq 3 }">on</c:if>"><a href="${fileUrl}&subtabstep=3&addAt=Y">수업자료</a></li>
              </ul>
            </article>
		</c:when>
		<%-- 교원 수강신청 --%>
        <c:when test="${param.menu eq 'curriculumrequest' }">
            <article class="content-wrap">
                <c:url var="tabUrl" value="?">
				    <c:param name="menuId" value="${param.menuId }"/>
				    <c:param name="crclId" value="${param.crclId}"/>
	                <c:param name="crclbId" value="${param.crclbId}"/>
				</c:url>
                <!-- tab style -->
                <ul class="tab-wrap sub-tab-style">
                  <li class="tab-list <c:if test="${param.subtabstep eq 1 }">on</c:if>"><a href="/lms/crm/curriculumManage.do${tabUrl }&subtabstep=1">과정계획서</a></li>
                  <c:if test="${param.managerAt eq 'Y' }">
	                  <li class="tab-list <c:if test="${param.subtabstep eq 2 }">on</c:if>"><a href="/lms/crm/curriculumRegister.do${tabUrl }&subtabstep=2">수강신청 화면</a></li>
	                  <li class="tab-list <c:if test="${param.subtabstep eq 3 }">on</c:if>"><a href="/lms/crm/curriculumAccept.do${tabUrl }&subtabstep=3">수강신청 승인</a></li>
	                  <li class="tab-list <c:if test="${param.subtabstep eq 4 }">on</c:if>"><a href="/lms/crm/curriculumStudent.do${tabUrl }subtabstep=4&thirdtabstep=1">수강대상자 확정</a></li>
                  </c:if>
                </ul>
            </article>
        </c:when>
        <%-- 교원 수강신청 > 수강대상자 확정 --%>
        <c:when test="${param.menu eq 'curriculumstudent' }">
            <article class="content-wrap">
                <c:url var="thirdTabUrl" value="?">
                    <c:param name="menuId" value="${param.menuId }"/>
                    <c:param name="crclId" value="${param.crclId}"/>
                    <c:param name="crclbId" value="${param.crclbId}"/>
                </c:url>
                <!-- tab style -->
                <div class="class-tab-wrap">
                  <a href="/lms/crm/curriculumStudent.do${thirdTabUrl }&thirdtabstep=1&subtabstep=4" class="title <c:if test="${param.thirdtabstep eq 1 }">on</c:if>">전체명단</a>
                  <a href="/lms/crm/curriculumGroupView.do${thirdTabUrl }&thirdtabstep=2&subtabstep=4&classCnt=1" class="title <c:if test="${param.thirdtabstep eq 2 }">on</c:if>">조배정</a>
                  <a href="/lms/crm/curriculumClassView.do${thirdTabUrl }&thirdtabstep=3&subtabstep=4" class="title <c:if test="${param.thirdtabstep eq 3 }">on</c:if>">분반</a>
                </div>
            </article>
        </c:when>
        <%-- 과정관리 > 학생  --%>
        <c:when test="${param.menu eq 'crclstudent' }">
        <article class="content-wrap" style="margin-bottom:0px;">
            <div class="content-header">
                <c:url var="thirdTabUrl" value="?">
                    <c:param name="menuId" value="${param.menuId }"/>
                    <c:param name="crclId" value="${param.crclId}"/>
                    <c:param name="crclbId" value="${param.crclbId}"/>
                </c:url>
                <!-- tab style -->
                <div class="class-tab-wrap">
                  <a href="/lms/crcl/curriculumStudent.do${thirdTabUrl }&thirdtabstep=1&subtabstep=4" class="title <c:if test="${param.thirdtabstep eq 1 }">on</c:if>">전체명단</a>
                  <a href="/lms/crcl/curriculumGroupView.do${thirdTabUrl }&thirdtabstep=2&subtabstep=4&classCnt=1" class="title <c:if test="${param.thirdtabstep eq 2 }">on</c:if>">조배정</a>
                  <a href="/lms/crcl/curriculumClassView.do${thirdTabUrl }&thirdtabstep=3&subtabstep=4" class="title <c:if test="${param.thirdtabstep eq 3 }">on</c:if>">분반</a>
                </div>
                <div class="btn-group-wrap">
                  <div class="right-area">
                    <a href="/lms/crm/curriculumStudent.do?menuId=MNU_0000000000000070&crclId=${param.crclId }&crclbId=${param.crclbId }&subtabstep=4&thirdtabstep=1" class="btn-md-auto btn-outline icon-arrow">수강대상자 확정 메뉴로 이동</a>
                  </div>
                </div>
            </div>
        </article>
        </c:when>
	</c:choose>
