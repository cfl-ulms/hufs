<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="${param.tmplatCours}"/>
<c:set var="CML" value="/template/lms"/>
<c:set var="_PREFIX" value="/cop/bbs"/>
<c:set var="SE_CODE" value="01" />
<c:if test="${not empty USER_INFO.id}">
	<c:set var="SE_CODE" value="${USER_INFO.userSe}" />
</c:if>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="bbsId" value="${searchVO.bbsId}" />
	<c:if test="${fn:length(searchVO.searchCateList) ne 0}">
		<c:forEach var="searchCate" items="${searchVO.searchCateList}" varStatus="statusCate">
			<c:if test="${not empty searchCate}">
				<c:param name="searchCateList" value="${searchCate}" />
			</c:if>
		</c:forEach>
	</c:if>
  	<c:if test="${not empty searchVO.searchCate}"><c:param name="searchCate" value="${searchVO.searchCate}" /></c:if>
  	<c:if test="${not empty searchVO.searchCnd}"><c:param name="searchCnd" value="${searchVO.searchCnd}" /></c:if>
	<c:if test="${not empty searchVO.searchWrd}"><c:param name="searchWrd" value="${searchVO.searchWrd}" /></c:if>
	<c:if test="${not empty searchVO.tmplatImportAt}"><c:param name="tmplatImportAt" value="${searchVO.tmplatImportAt}"/></c:if>
</c:url>
<% /*URL 정의*/ %>

<c:choose>
	<c:when test="${searchVO.bbsId eq 'BBSMSTR_000000000005'}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
        	<c:url var="viewUrl" value="${_PREFIX}/selectBoardArticle.do${_BASE_PARAM}">
			  	<c:param name="nttNo" value="${result.nttNo}" />
			  	<c:param name="pageIndex" value="${searchVO.pageIndex}" />
		    </c:url>
	        <div class="textbook-contents-view">
	          <div class="left-area">
	            <div class="textbook-img" style="background-image:url(<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${brdMstrVO.siteId}"/>&amp;appendPath=<c:out value="${searchVO.bbsId}"/>&amp;atchFileNm=<c:out value="${result.atchFileNm}"/>);"></div>
	          </div>
	          <div class="right-area">
	            <div class="textbook-inner">
	              <a href="${viewUrl}">
	                <div class="textbook-flag">
					  <c:set var="imgSrc">
						<c:import url="/lms/common/flag.do" charEncoding="utf-8">
							<c:param name="ctgryId" value="${result.ctgryId}"/>
						</c:import>
	          		  </c:set>
	                  <span class="flag-img"><img src="${CML}/imgs/common/flag_lg/${imgSrc}" alt="국기"></span>
	                  <span class="text"><c:out value="${result.ctgryNm}"/> 교재</span>
	                </div>
	                <div class="book-name">
	                	<c:if test="${result.tmp03 eq 'Y'}">
	                		<span class="type">e-book</span>
	                	</c:if>
	                	<c:out value="${result.nttSj}"/>
	                </div>
	                <dl class="book-info">
	                  <dt class="info-title">출판사</dt>
	                  <dd class="info-name"><c:out value="${result.tmp01}"/></dd>
	                </dl>
	                <dl class="book-info">
	                  <dt class="info-title">저자</dt>
	                  <dd class="info-name"><c:out value="${result.tmp02}"/></dd>
	                </dl>
	                <div class="desc dotdotdot">
	                	 <c:out value="${result.nttCn}" escapeXml="false"/>
	                </div>
	              </a>
	            </div>
	            <div class="textbook-button">
	              <a href="${viewUrl}" class="btn-full btn-outline">상세보기</a>
	              <%-- <a href="<c:out value="${result.tmp05}"/>" class="btn-full btn-point" target="_blank">구매하기</a> --%>
	              <div class="util-wrap">
	                <button class="btn-share btnModalOpen" data-modal-type="share" title="공유하기"></button>
	                <c:choose>
	                	<c:when test="${not empty USER_INFO.id}">
	                		<c:set var="wishAt" value=""/>
	                		<c:forEach var="wishList" items="${wishList}" varStatus="status">
	                			<c:if test="${wishList.trgetId eq result.nttNo}">
	                				<c:set var="wishAt" value="on"/>
	                			</c:if>
	                		</c:forEach>
	                		<button class="btn-wishlist on" title="관심수강 담기"></button>
	                	</c:when>
	                	<c:otherwise>
	                		<button class="btn-wishlist btnModalOpen"
				                      data-modal-type="confirm"
				                      data-modal-header="알림"
				                      data-modal-text="회원가입이 필요한 서비스입니다."
				                      data-modal-subtext="로그인 후 이용이 가능합니다. 회원가입 화면으로 이동하시겠습니까?"
				                      data-modal-rightbtn="확인"
				                      onclick="location.href='/uss/umt/cmm/EgovStplatCnfirmMber.do'"
				                      title="관심수강 담기">
				              </button>
	                	</c:otherwise>
	                </c:choose>
	              </div>
	            </div>
	          </div>
	        </div>
        </c:forEach>
	</c:when>
	<c:otherwise>
	
	</c:otherwise>
</c:choose>
  