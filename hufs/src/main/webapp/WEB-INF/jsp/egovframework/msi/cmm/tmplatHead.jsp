<%@page import="egovframework.com.cmm.service.Globals"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title><c:out value="${siteInfo.siteNm}"/></title>

  <meta name="title" content="<c:out value="${siteInfo.siteNm}"/>">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="<c:out value="${siteInfo.siteNm}"/>">
  <meta property="og:description" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/common.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board.css?v=1">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>
  <script src="${CM}/js/common.js"></script>
  
  <!--=================================================
        페이지별 스타일시트, 스크립트
  ==================================================-->
  <c:choose>
	<c:when test="${param.menu eq 'join' and param.type ne 'mailC'}">
		<link rel="stylesheet" href="${CML}/css/join/join.css?v=2">
		<c:if test="${param.step eq '1'}"><script src="${CML}/js/join/join_step01.js?v=1"></script></c:if>
		<c:if test="${param.step eq '2'}"><script src="${CML}/js/join/join_step02.js?v=1"></script></c:if>
	</c:when>
	<c:when test="${param.menu eq 'login' or param.menu eq 'idSearch' or param.menu eq 'pwSearch' or param.type eq 'mailC' or param.menu eq 'pwComplete'}">
		<link rel="stylesheet" href="${CML}/css/login/login.css?v=2">
	</c:when>
	<c:otherwise></c:otherwise>
  </c:choose>
  <link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
  <script>
  	<c:if test="${not empty message}">
		alert("${message}");
	</c:if>
  </script>
</head>
<body>

  <header class="gnb">
    <div class="top-header area">
      <h1>
        <a href="/index.do" class="flex">
          <img src="${SiteFileStoreWebPath}${siteInfo.siteId}/${siteInfo.upendLogoFileNm}" alt="<c:out value="${siteInfo.siteNm}"/>">
        </a>
      </h1>
      <ul class="user-info-wrap">
      	<li class="list">
          <a href="http://www.hufs.ac.kr/" target="_blank" class="link-text">HUFS HOME</a>
        </li>
        <li class="list">
          <a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>">JOIN</a>
        </li>
        <li class="list">
          <a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>">LOGIN</a>
        </li>
      </ul><!-- user-info-wrap -->
    </div><!-- top-header -->

    <!-- nav -->
    <nav class="nav-wrap">
      <div class="nav-wrap-inner area">
        <ul class="nav-list-wrap">
          <li class="nav-list depth-1">
            <a href="/index.do" class="link-text inline-block">진흥원 소개</a>
          </li>
          <c:import url="/msi/ctn/menuService.do" charEncoding="utf-8">
			<c:param name="menuTarget" value="Main"/>
			<c:param name="menuType" value="Simple2Depth"/>
		  </c:import>
        </ul>
        <%-- 
        <div class="btn-menu-trigger">
          <a href="#" class="block">
            <img src="${CML}/imgs/common/icon_lnb.svg" alt="메뉴">
          </a>
        </div>
         --%>
      </div><!-- nav-wrap-inner -->
      <div class="nav-bg"></div>
    </nav><!-- nav -->
  </header>
  
  <!-- bread crumb -->
  <div class="area">
    <ul class="main-bread-crumb">
      <li class="item">HOME</li>
      <c:if test="${not empty param.menu}">
      	<li class="item">
      		<c:choose>
        		<c:when test="${param.menu eq 'join'}">회원가입</c:when>
        		<c:when test="${param.menu eq 'login'}">로그인</c:when>
        		<c:when test="${param.menu eq 'idSearch'}">아이디찾기</c:when>
        		<c:when test="${param.menu eq 'pwSearch'}">비밀번호찾기</c:when>
        		<c:otherwise>-</c:otherwise>
        	</c:choose>
      	</li>
      </c:if>
    </ul>
  </div>
  <section class="page-content-wrap">
    <c:if test="${param.areaAt ne 'N'}">
    	<div class="area">
    </c:if>
      <div class="page-common-title">
      	<c:choose>
       		<c:when test="${param.menu eq 'join'}">
       			<h3 class="title center-align">회원가입</h3>
       			<c:if test="${param.type eq 'mailC'}">
       				<p class="sub-title">회원가입 인증이 완료되었습니다.</p>
       			</c:if>
       		</c:when>
       		<c:when test="${param.menu eq 'login'}">
       			<h2 class="title center-align">로그인</h2>
       			<c:if test="${param.type eq 't'}"><p class="sub-title">Teacher & Staff Login</p></c:if>
       		</c:when>
       		<c:when test="${param.menu eq 'idSearch'}">
       			<h2 class="title center-align">아이디찾기</h2>
       			<c:choose>
	       			<c:when test="${param.type eq 'before'}">
	       				<p class="sub-title">회원가입 시 등록한 이름과 이메일을 입력해주세요</p>
	       			</c:when>
	       			<c:otherwise>
	       				<p class="sub-title">
							아이디 정보 전체를 이메일로 발송해 드렸습니다 <span>(${param.email})<span><br>
							회원님의 계정을 확인하실 수 없는 경우 관리자에게 문의해 주세요
						</p>
	       			</c:otherwise>
       			</c:choose>
       		</c:when>
       		<c:when test="${param.menu eq 'pwSearch'}">
       			<h2 class="title center-align">비밀번호 찾기</h2>
       			<c:choose>
	       			<c:when test="${param.type eq 'before'}">
	       				<p class="sub-title">회원가입시 등록 한 아이디와 이메일, 이름을 입력해주세요<br> 비밀번호 재발급 절차가 진행됩니다</p>
	       			</c:when>
	       			<c:when test="${param.type eq 'mailC'}">
	       				<p class="sub-title">
        					인증이 정상적으로 완료되었습니다. 새로운 비밀번호를 등록해주세요
        				</p>
	       			</c:when>
	       			<c:otherwise>
	       				<p class="sub-title">
							비밀번호 재설정 메일을 발송해 드렸습니다 <span>(${param.email})<span><br>
            				회원님의 계정을 확인하실 수 없는 경우 관리자에게 문의해 주세요
						</p>
	       			</c:otherwise>
       			</c:choose>
       		</c:when>
       		<c:when test="${param.menu eq 'pwComplete'}">
			      <h2 class="title center-align">비밀번호 재설정</h2>
			      <p class="sub-title">비밀번호 재설정이 완료 되었습니다.</p>
       		</c:when>
       		<c:otherwise>-</c:otherwise>
       	</c:choose>
      <c:if test="${param.areaAt ne 'N'}"></div></c:if>
      
      <c:if test="${param.menu eq 'join' and param.stepAt ne 'N'}">
	      <ul class="join-step-wrap">
	        <li class="flex-col-4 join-step <c:if test="${param.step eq '1'}">on</c:if>"><span class="step-item">STEP01.</span>약관동의</li>
	        <li class="flex-col-4 join-step <c:if test="${param.step eq '2'}">on</c:if>"><span class="step-item">STEP02.</span>회원정보입력</li>
	        <li class="flex-col-4 join-step <c:if test="${param.step eq '3'}">on</c:if>"><span class="step-item">STEP03.</span>가입완료</li>
	      </ul>
      </c:if>