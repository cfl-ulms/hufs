<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="seCode" value="${param.seCode}"/>
<c:choose>
	<c:when test="${seCode eq '02'}">
		<c:set var="seCode" value="04"/>
	</c:when>
	<c:when test="${seCode eq '01' or seCode eq '99'}">
		<c:set var="seCode" value="02"/>
	</c:when>
</c:choose>

<c:choose>
	<c:when test="${param.menuTarget eq 'Main'}">
		<c:choose>
			<c:when test="${param.menuType eq 'Simple2Depth'}">
				<c:forEach var="mpmDepth01" items="${mpmList}" varStatus="status">
				<c:if test="${mpmDepth01.menuLevel eq 1 and mpmDepth01.expsrUseAt eq 'Y' and ((seCode eq '08' and mpmDepth01.profsrUseAt eq 'Y') or (seCode eq '06' and mpmDepth01.stdnprntUseAt eq 'Y') or (seCode eq '04' and mpmDepth01.stdntUseAt eq 'Y') or (seCode eq '02' and mpmDepth01.generalUseAt eq 'Y'))}">
					<li class="nav-list depth-1">
						<a href="<c:out value="${mpmDepth01.menuWebPath}"/>" <c:if test="${mpmDepth01.nwdAt eq 'Y'}">target="_blank"</c:if> class="link-text">
							<c:out value="${mpmDepth01.menuNm}"/>
						</a>
						<ul class="sub-menu-list-wrap clear">
							<c:forEach var="mpmDepth02" items="${mpmList}">
							<c:set var="mpmDepth02Count" value="1"/>
							<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, mpmDepth01.menuId) and mpmDepth02.expsrUseAt eq 'Y' and ((seCode eq '08' and mpmDepth02.profsrUseAt eq 'Y') or (seCode eq '06' and mpmDepth02.stdnprntUseAt eq 'Y') or (seCode eq '04' and mpmDepth02.stdntUseAt eq 'Y') or (seCode eq '02' and mpmDepth02.generalUseAt eq 'Y'))}">
								<li class="nav-list depth-2">
									<a href="<c:out value="${mpmDepth02.menuWebPath}"/>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if> class="link-text">
										<c:out value="${mpmDepth02.menuNm}"/>
									</a>
								</li>
								<c:set var="mpmDepth02Count" value="${mpmDepth02Count + 1 }"/>
							</c:if>
							</c:forEach>
						</ul>
					</li>
				</c:if>
				<c:if test="${fn:length(mpmList) eq 0}"><li class="alt_msg">메뉴를 생성하세요</li></c:if>
				</c:forEach>
			</c:when>
		</c:choose>
	</c:when>
	<c:when test="${param.menuTarget eq 'Sub'}">
		<c:forEach var="mpmDepth02" items="${mpmList}" varStatus="status">
			<c:if test="${mpmDepth02.menuLevel eq 2 and fn:contains(mpmDepth02.menuPathById, currRootMpm.menuId) and mpmDepth02.expsrUseAt eq 'Y' and ((seCode eq '08' and mpmDepth02.profsrUseAt eq 'Y') or (seCode eq '06' and mpmDepth02.stdnprntUseAt eq 'Y') or (seCode eq '04' and mpmDepth02.stdntUseAt eq 'Y') or (seCode eq '02' and mpmDepth02.generalUseAt eq 'Y'))}">
				<c:choose>
					<c:when test="${mpmDepth02.menuLastNodeAt eq 'Y'}">
						<li class="list depth-1 <c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">active</c:if>">
							<a href="<c:out value="${mpmDepth02.menuWebPath}"/>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth02.menuNm}"/></a>
						</li>
					</c:when>
					<c:otherwise>
						<li class="list depth-1 has-sub <c:if test="${fn:contains(currMpm.menuPathById, mpmDepth02.menuId)}">active</c:if>">
							<a href="<c:out value="${mpmDepth02.menuWebPath}"/>" <c:if test="${mpmDepth02.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth02.menuNm}"/></a>
							<ul class="sub-menu-group">
								<c:forEach var="mpmDepth03" items="${mpmList}">
									<c:if test="${mpmDepth03.menuLevel eq 3 and fn:contains(mpmDepth03.menuPathById, mpmDepth02.menuId) and mpmDepth03.expsrUseAt eq 'Y' and ((seCode eq '08' and mpmDepth03.profsrUseAt eq 'Y') or (seCode eq '06' and mpmDepth03.stdnprntUseAt eq 'Y') or (seCode eq '04' and mpmDepth03.stdntUseAt eq 'Y') or (seCode eq '02' and mpmDepth03.generalUseAt eq 'Y'))}">
										<li class="list depth-2 <c:if test="${fn:contains(currMpm.menuPathById, mpmDepth03.menuId)}">active</c:if>"><a href="<c:out value="${mpmDepth03.menuWebPath}"/>" <c:if test="${mpmDepth03.nwdAt eq 'Y'}">target="_blank"</c:if>><c:out value="${mpmDepth03.menuNm}"/></a></li>
									</c:if>
								</c:forEach> 
							</ul>
						</li>
					</c:otherwise>
				</c:choose>
			</c:if>
		</c:forEach>
	</c:when>
</c:choose>