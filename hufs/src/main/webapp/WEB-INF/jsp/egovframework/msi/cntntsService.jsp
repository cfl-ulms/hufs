<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="CML" value="/template/lms"/>
<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}"/>
		</c:import>
	</c:when>
	<c:otherwise>
		<!DOCTYPE html>
		<html lang="ko">
		<head>
		  <meta charset="UTF-8">
		  <meta http-equiv="X-UA-Compatible" content="ie=edge">
		  <title>
		  	<c:choose>
	    		<c:when test="${fn:indexOf(ImportUrl,'indvdlInfoPolicy') > -1}">개인정보처리방침</c:when>
	    		<c:when test="${fn:indexOf(ImportUrl,'useStplat') > -1}">이용약관</c:when>
	    		<c:when test="${fn:indexOf(ImportUrl,'cpyrhtSttemntSvc') > -1}">저작권 보호 정책</c:when>
	    	</c:choose>
		  </title>
		  <link rel="stylesheet" href="${CML}/css/common/base.css">
		  <link rel="stylesheet" href="${CML}/css/common/common.css">
		  <link rel="stylesheet" href="${CML}/css/common/table.css">
		  <link rel="stylesheet" href="${CML}/font/font.css">
		  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script>
		  <script src="${CML}/js/common.js"></script>
		</head>
		<body class="popup-body">
		  <div class="window-popup-wrap">
		    <div class="modal-header">
		    	<c:choose>
		    		<c:when test="${fn:indexOf(ImportUrl,'indvdlInfoPolicy') > -1}">
		    			<h4 class="modal-title">개인정보처리방침</h4>
      					<p class="modal-subtitle">개인정보수집∙이용에 따른 약관동의</p>
		    		</c:when>
		    		<c:when test="${fn:indexOf(ImportUrl,'useStplat') > -1}">
		    			<h4 class="modal-title">이용약관</h4>
		      			<p class="modal-subtitle">홈페이지 이용에 따른 약관동의</p>
		    		</c:when>
		    		<c:when test="${fn:indexOf(ImportUrl,'cpyrhtSttemntSvc') > -1}">
		    			<h4 class="modal-title">저작권 보호 정책</h4>
      					<p class="modal-subtitle">저작권 이용에 따른 약관동의</p>
		    		</c:when>
		    	</c:choose>
		    </div>
		    <div class="modal-body">
		      <div class="policy-wrap">
	</c:otherwise>
</c:choose>

<c:if test="${not empty ImportUrl}">
	<c:import url="/EgovPageLink.do?link=${ImportUrl}" charEncoding="utf-8" var="html"/>
	<c:set var="html" value="${fn:replace(html, '$SITE_NM$', siteInfo.siteNm)}"/>
	<c:set var="html" value="${fn:replace(html, '$PHONE_NO$', siteInfo.tlphonNo)}"/>
	<c:set var="html" value="${fn:replace(html, '$FAX_NO$', siteInfo.faxNo)}"/>
	<c:out value="${html}" escapeXml="false"/>
</c:if>


<c:choose>
	<c:when test="${param.tmplatImportAt ne 'N'}">
		<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8">
			<c:param name="isMain" value="${isMain}"/>
		</c:import>
	</c:when>
	<c:otherwise>
		</div>
		    <div class="modal-footer">
		      <button type="button" class="btn-xl btn-point btnWindowClose">확인</button>
		    </div>
		  </div>
		</body>
		</html>
	</c:otherwise>
</c:choose>
