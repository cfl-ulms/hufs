<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

</div><!-- area-->
  </section>
<!-- footer -->
  <footer id="footer">
    <div class="footer-top-wrap">
      <div class="footer-top">
        <div class="footer-term">
          <a class="privacy btnWindowOpen" href="/msi/indvdlInfoPolicy.do?tmplatImportAt=N" data-width="868" data-height="860">
            개인정보처리방침
          </a>
          <a class="btnWindowOpen" href="/msi/useStplat.do?tmplatImportAt=N" data-width="868" data-height="full">
            이용약관
          </a>
          <a class="btnWindowOpen" href="/msi/cpyrhtSttemntSvc.do?tmplatImportAt=N" data-width="868" data-height="650">
            저작권 보호 정책
          </a>
        </div>
        <select class="select2" name="" id="" data-select="style2" data-link="true" data-placeholder="패밀리사이트">
          <option value=""></option>
          <c:import url="/msi/ctn/linkSiteService.do" charEncoding="utf-8">
			<c:param name="tmplatCours" value="${_IMG}"/>
		  </c:import>
        </select>
      </div>
    </div>
    <div class="area">
      <div class="company-info-wrap">
        <h2 class="footer-logo">
          <img src="${SiteFileStoreWebPath}${siteInfo.siteId}/${siteInfo.lptLogoFileNm}" alt="<c:out value="${siteInfo.siteNm}"/>">
        </h2>
        <div class="company-info">
          <p>${siteInfo.adresReplcText}</p>
          <p class="copyright">© 2019 Hankuk University of Foreign Studies. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>

</body>

</html>