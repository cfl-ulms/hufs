<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="/template/web/${siteInfo.tmplatCours}/image"/>
<c:set var="_CSS" value="/template/web/${siteInfo.tmplatCours}/css"/>
<c:set var="C_JS" value="/template/common/js"/>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>알립니다.</title>
<link rel="stylesheet" type="text/css" href="${_CSS}/styles.css" />
<script type="text/javascript" src="${C_JS}/common.js"></script>
<script type="text/javaScript" language="javascript">

/* ********************************************************
* 체크버튼 클릭시
******************************************************** */
$(document).ready(function(){
	$("#chkPopup").change(function(){
		if($(this).is(":checked")){
			fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 1);
		}else{
			fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 0);
		}
	});
});
function fnPopupCheck() {

	var chk = document.getElementById("chkPopup");
	if(chk && chk.checked) {
		fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 1);
	}
	window.close();
}
</script>
<style type="text/css"> 
	html{height:100%; }
	/*  popup_list 20120529  */
	#wrap_pop{width:400px;background:url('${_IMG}/common/plist_top.gif') 0 top no-repeat;padding-top:123px;}
	#wrap_pop .pop_cont{width:350px;background:url('${_IMG}/common/plist_center_bg.gif') 0 bottom repeat-y;padding:25px;margin:0;}
	#wrap_pop .pop_list{margin:0;padding:0;}
	#wrap_pop .pop_list li{list-style:none;background:url('${_IMG}/common/plist_bul.gif') 0 center no-repeat;padding:5px 0 5px 15px;border-bottom:1px solid #e7e7e7;}
</style>
</head>
<body>
	<div id="wrap_pop">	
		<div class="pop_cont">
			<ul class="pop_list">
				<c:forEach items="${popupMainLIst}" var="resultInfo" varStatus="status">
					<li><a href="#" onclick="fn_egov_popupOpen_PopupManage('${resultInfo.popupId}','${resultInfo.fileUrl}','${resultInfo.popupWsize}','${resultInfo.popupHsize}','${resultInfo.popupHlc}','${resultInfo.popupWlc}','${resultInfo.stopVewAt}');return false;">
					<c:choose><c:when test="${fn:length(resultInfo.popupTitleNm) > 30}"><c:out value='${fn:substring(resultInfo.popupTitleNm, 0, 30)}'/>...</c:when><c:otherwise><c:out value="${resultInfo.popupTitleNm}"/></c:otherwise></c:choose></a></li>				
				</c:forEach>
			</ul>
		</div>
		<img src="${_IMG}/common/plist_bottom.gif" alt="" />
	</div>

	
	<div id="popmainboxdn">
		<label for="chkPopup"><input type="checkbox" name="chkPopup" id="chkPopup"/>24시간동안 이 창 열지 않기</label>
		<a href="#" class="btn_close_pop" onclick="fnPopupCheck()" title="팝업창닫기"><img src="${_IMG}/common/popbox_close.gif" alt="닫기" /></a>
	</div>
		
</body>
</html>