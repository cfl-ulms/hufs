<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="_IMG" value="/template/common/images/popup"/>
<c:set var="_CSS" value="/template/web/smart_001/css"/>
<c:set var="C_JS" value="/template/common/js"/>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="<c:out value="${siteInfo.siteNm}"/>" http-equiv="keywords"/>
<meta content="<c:out value="${siteInfo.siteNm}"/>" name="subject"/>
<meta content="여기는 <c:out value="${siteInfo.siteNm}"/>입니다." name="description"/>

<meta content="no" http-equiv="imagetoolbar"/>
<meta content="onmakers" name="author"/>
<title><c:out value='${popupManageVO.popupTitleNm}'/></title>
<link rel="stylesheet" type="text/css" href="${_CSS}/styles.css" />
<script type="text/javascript" src="${C_JS}/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${C_JS}/common.js"></script>
<script type="text/javaScript" language="javascript">

/* ********************************************************
* 체크버튼 클릭시
******************************************************** */
$(document).ready(function(){
	$("#chkPopup").change(function(){
		if($(this).is(":checked")){
			fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 1);
		}else{
			fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 0);
		}
	});
});
function fnPopupCheck() {

	var chk = document.getElementById("chkPopup");
	if(chk && chk.checked) {
		fnSetCookiePopup( "${popupManageVO.popupId}", "done" , 1);
	}
	window.close();
}
</script>
<style type="text/css"> 
	html{height:100%; }
</style>
</head>
<body style="background:none;">	
	<div style="width:365px; padding:8px 40px 150px; font-size:12px; background:url(${_IMG}/popuptem053_445_bg.jpg) no-repeat left bottom;">		
		<p style="margin:-8px -40px 0;">
			<img src="${_IMG}/popuptem053_445_img.jpg" alt="배경"/>
		</p>
		<h1 style="margin-bottom:20px; color:#7c3b04; font-size:14px;"><c:out value="${popupManageVO.popupTitleNm}"/></h1>
		<div style="width:360px;">
			<c:out value="${popupManageVO.popupCn}" escapeXml="false" />
		</div>		
	</div>
	
	<c:if test="${popupManageVO.stopVewAt eq 'Y'}">	
		<div id="popmainboxdn">
			<label for="chkPopup"><input type="checkbox" name="chkPopup" id="chkPopup"/>24시간동안 이 창 열지 않기</label>
			<a href="#" class="btn_close_pop" onclick="fnPopupCheck()" title="팝업창닫기"><img src="${_IMG}/common/popbox_close.gif" alt="닫기" /></a>
		</div>
	</c:if>
		
</body>
</html>