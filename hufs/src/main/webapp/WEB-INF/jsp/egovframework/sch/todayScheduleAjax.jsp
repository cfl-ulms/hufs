<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ page import="java.util.Date" %>
<c:set var="_L_IMG" value="${pageContext.request.contextPath}/template/lms/imgs"/>
<c:set var="CML" value="/template/lms"/>

<c:choose>
	<c:when test="${pageFlag eq 'student' }">
		<link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
		<link rel="stylesheet" href="${CML}/css/class/class.css?v=2">
	</c:when>
	<c:when test="${pageFlag eq 'teacher' }">
		<link rel="stylesheet" href="${CML}/css/common/table_staff.css?v=2">
		<link rel="stylesheet" href="${CML}/css/class/staff_class.css?v=2">
	</c:when>
</c:choose>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
    <c:param name="menuId" value="${param.menuId }"/>
    <c:if test="${not empty searchVO.searchCrclLang}"><c:param name="searchCrclLang" value="${searchVO.searchCrclLang}" /></c:if>
    <c:if test="${not empty searchVO.searchCrclNm}"><c:param name="searchCrclNm" value="${searchVO.searchCrclNm}" /></c:if>
    <c:if test="${not empty searchVO.searchStartDate}"><c:param name="searchStartDate" value="${searchVO.searchStartDate}" /></c:if>
    <c:if test="${not empty searchVO.searchEndDate}"><c:param name="searchEndDate" value="${searchVO.searchEndDate}" /></c:if>
    <c:if test="${not empty searchVO.searchProcessSttusCodeDate}"><c:param name="searchProcessSttusCodeDate" value="${searchVO.searchProcessSttusCodeDate}" /></c:if>
    <c:if test="${not empty searchVO.searchTargetType}"><c:param name="searchTargetType" value="${searchVO.searchTargetType}" /></c:if>
    <c:if test="${not empty searchVO.searchCampusId}"><c:param name="searchCampusId" value="${searchVO.searchCampusId}" /></c:if>
</c:url>
<% /*URL 정의*/ %>

<%-- <c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
    <c:param name="isMainSite">Y</c:param>
    <c:param name="siteId" value="SITE_000000000000001"/>
</c:import> --%>




        	<jsp:useBean id="now" class="java.util.Date" />
        	<fmt:formatDate value="${now}" pattern="HHmmss" var="currTime" />


          <!-- 날짜선택 -->
          <div class="date-select-wrap">
            <!-- <button class="btn-prev" disabled> -->
            <button class="btn-prev btn-list-prev2">
              <i class="icon-arrow" data-id="${yesterDate}"></i>
              <p class="title left-align">이전수업<span class="date" id="prevDt2">${yesterDate}</span></p>
            </button>
            <div class="selected-date">
              <h3 class="title" id="current">${today}(${dayNm})</h3>
              <p class="sub-title">현재 총 ${fn:length(myCurriculumList)} 개의 특수외국어 수업이 진행 중입니다.</p>
            </div>
            <button class="btn-next btn-list-next2">
              <p class="title right-align">다음수업<span class="date" id="afterDt2">${tomorrowDate}</span></p>
              <i class="icon-arrow" data-id="${tomorrowDate}"></i>
            </button>
          </div>
          <!-- 테이블영역-->
          <table class="common-table-wrap table-type-board">
            <colgroup>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
              <col style='width:10%'>
            </colgroup>
            <thead>
              <tr class='bg-light-gray font-700'>
                <th scope='col'>수업일</th>
                <th scope='col'>시작시간</th>
                <th scope='col'>종료시간</th>
                <th scope='col'>교시</th>
                <th scope='col'>과정명</th>
                <th scope='col'>수업주제</th>
                <th scope='col'>교수명</th>
                <th scope='col'>강의실</th>
                <th scope='col'>출석</th>
              </tr>
            </thead>
            <tbody>
            	<c:if test="${fn:length(myCurriculumList) eq 0}">
					<tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
						<td colspan="9">수업이 없는 날입니다.</td>
					</tr>
				</c:if>
            	<c:forEach var="result" items="${myCurriculumList}" varStatus="status">
				<!-- 학생 > 수업계획으로 고정(테스트) -->
            	<c:url var="viewUrl" value="/lms/cla/studyPlanView.do${_AJAX_PARAM}">
			                    <c:param name="crclId" value="${result.crclId}"/>
			                    <c:param name="plId" value="${result.plId}"/>
			                    <c:param name="menuId" value="${searchMenuId}"/>
			                </c:url>
	             <tr onclick="location.href='${viewUrl}'" class=" cursor-pointer">
	                <td scope='row' class='font-point'>
	                ${crclDay}
	                <c:if test="${dayFlag eq 'TODAY' }">[오늘]</c:if>
	                </td>
	                <td>${fn:substring(result.startTime, 0, 2)}:${fn:substring(result.startTime, 2, 4)}</td>
	                <td>${fn:substring(result.endTime, 0, 2)}:${fn:substring(result.endTime, 2, 4)}</td>
	                <td>${result.sisu}</td>
	                <td class='left-align'>${result.crclNm}</td>
	                <td>${result.studySubject}</td>
	                <td>
	                	<c:set var="comma" value=","/>
						<c:set var="facCnt" value="1"/>
						<c:forEach var="list" items="${myCurriculumList}" varStatus="status">
							<c:if test="${result.crclId eq list.crclId and result.plId eq list.plId}">
								<c:forEach var="fac" items="${list.facPlList}" varStatus="status">
									<c:if test="${fac.plId eq list.plId}">
										<c:if test="${!status.last}">
											<c:out value="${fac.userNm}"/>
											<c:if test="${facCnt ne 1}">(부교원)</c:if>${comma}
										</c:if>
										<c:if test="${status.last}">
											<c:out value="${fac.userNm}"/>
											<c:if test="${facCnt ne 1}">(부교원)</c:if>교수
										</c:if>
										<c:set var="facCnt" value="${facCnt + 1}"/>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
	                </td>
	                <td>${result.placeDetail}</td>
	                <td>
	                	<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />
    					<fmt:formatDate value="${now}" pattern="HHmm" var="todayTime" />

    					<c:choose>
    						<c:when test="${USER_INFO.userSeCode eq '08'}">
    							<%-- <c:set var="startDt" value="${fn:replace(result.startDt,'-','')}"/> --%>
    							<c:set var="startDt" value="${result.startDt}"/>
								<c:choose>
									<c:when test="${today >= startDt}">
										<c:choose>
				                      		<c:when test="${not empty result.attTCnt and result.attTCnt == result.attYCnt}">전원출석</c:when>
				                      		<c:otherwise>
				                      			<%-- <a href='#' class='atd underline btnModalOpen' data-modal-type='attend_view' data-id="${result.plId}" data-nm="${result.studySubject}" data-crclid="${result.crclId}"> --%>
					                      			<c:if test="${result.attLCnt > 0}">지각 <c:out value="${result.attLCnt}"/></c:if>
					                      			<c:if test="${result.attLCnt > 0 and result.attNCnt > 0}">,</c:if>
					                      			<c:if test="${result.attNCnt > 0}">결석 <c:out value="${result.attNCnt}"/></c:if>
				                      			<%-- </a> --%>
				                      		</c:otherwise>
				                      	</c:choose>
									</c:when>
									<c:otherwise>
										대기
									</c:otherwise>
								</c:choose>
    						</c:when>
			            	<c:when test="${empty result.attentionType}">
			            		<c:choose>
			            			<c:when test="${today eq crclDay && todayTime > result.startTime && todayTime <= result.endTime}">
			            				진행중
			            			</c:when>
			            			<c:when test="${today > crclDay}">
			            				결석
			            			</c:when>
			            			<c:otherwise>
			            				대기
			            			</c:otherwise>
			            		</c:choose>
			            	</c:when>
			            	<c:when test="${result.attentionType eq 'Y'}">
			            		출석
			            	</c:when>
			            	<c:when test="${result.attentionType eq 'L'}">
			            		지각
			            	</c:when>
			            	<c:when test="${result.attentionType eq 'N'}">
			            		결석
			            	</c:when>
			            	<c:otherwise>
			            		-
			            	</c:otherwise>
			            </c:choose>
	                </td>
	              </tr>
              	</c:forEach>
            </tbody>
          </table>
          
<script>
	$(document).ready(function(){
		var tempMenuId = "${param.menuId }";
		// 오늘의 수업 (이전수업 클릭)
		$('.btn-list-prev2').off().on("click", function(event) {
			var date = "${yesterDate}";
			
			console.log("ffffff");
			
	        $.ajax({
	            url:'/sch/todayScheduleAjax.do',
	            type:'post',
	            dataType:'html',
	            data:{"startDt" : date, "menuId" : tempMenuId},
	            beforeSend : function(){
	            	$('#todayCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
	            }, success:function(data){
	            	$('#todayCard').html(data);
	            }
	        });
		});

		// 오늘의 수업 (다음수업 클릭)
		$('.btn-list-next2').off().on("click", function() {
			var date = "${tomorrowDate}";
			
	        $.ajax({
	        	url:'/sch/todayScheduleAjax.do',
	            type:'post',
	            dataType:'html',
	            data:{"startDt" : date, "menuId" : tempMenuId},
	            beforeSend : function(){
	            	$('#todayCard').html("<img src='/template/common/images/temp/loading.gif' style='margin-left:900px;'/>");
	            }, success:function(data){
	            	$('#todayCard').html(data);
	            }
	        });
		});
	});
</script>