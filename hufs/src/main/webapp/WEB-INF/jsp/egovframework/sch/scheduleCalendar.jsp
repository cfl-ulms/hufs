<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/manage/images"/>
<c:set var="_JS" value="${pageContext.request.contextPath}/template/manage/js"/>
<c:set var="_C_IMG" value="${pageContext.request.contextPath}/template/common/images"/>
<c:set var="_C_JS" value="/template/common/js"/>
<c:set var="_MODE" value=""/>
<c:set var="_PREFIX" value="/lms/crcl"/>
<c:set var="_ACTION" value=""/>

<% /*URL 정의*/ %>
<c:url var="_BASE_PARAM" value="">
	<c:param name="menuId" value="${searchVO.menuId}"/>
	<c:param name="crclId" value="${curriculumVO.crclId }"/>
</c:url>
<% /*URL 정의*/ %>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="isMainSite">Y</c:param>
	<c:param name="siteId" value="SITE_000000000000001"/>
</c:import>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
  
  <link rel="shortcut icon" href="">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/table_staff.css?v=2">
  
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/core/locales-all.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/interaction/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/daygrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/timegrid/main.min.js?v=1"></script>
  <script src="/template/lms/lib/fullcalendar-4.3.1/packages/list/main.min.js?v=1"></script>
  <script src="/template/lms/js/class/fullCalendar_custom.js?v=1"></script>
  <script>
	$(document).ready(function(){
		
		$('#btnRegist').click(function(){
			$('#layerPopup').find("input, textarea").val("");
			$('#startTimeHH').find("option:eq(0)").prop("selected", true);
			$('#startTimeMM').find("option:eq(0)").prop("selected", true);
			$('#endTimeHH').find("option:eq(0)").prop("selected", true);
			$('#endTimeMM').find("option:eq(0)").prop("selected", true);
			$('#campusId').find("option:eq(0)").prop("selected", true);
			
			$('#flag').val("regist");
			$('#plType').val("ind");
			
			Lms.common_ui.init();
			
			$('#layerPopup').show();
		});
		
		/* var acaDepth2 = [];
	
		<c:forEach var="result" items="${curriculumList}" varStatus="status">
			
			acaDepth2.push({crclLang : '${result.crclLang}', crclLangNm : '${result.crclLangNm}', crclId : '${result.crclId}', crclNm : '${result.crclNm}'});
			
		</c:forEach>
		
		 $("#ctgryId1").change(function(){
			var ctgryId1 = $(this).val(),
				option = "<option value=''>교육과정</option>";
				
			$("#ctgryId2").html(option);
			for(i = 0; i < acaDepth2.length; i++){
				if(acaDepth2[i].crclLang == ctgryId1){
					var searchSysCode2 = "${searchVO.searchSysCode2}";
					if(searchSysCode2 == acaDepth2[i].crclId){
						$("#ctgryId2").append("<option value='"+acaDepth2[i].crclId+"' selected='selected'>"+acaDepth2[i].crclNm+"</option>");
					}else{
						$("#ctgryId2").append("<option value='"+acaDepth2[i].crclId+"'>"+acaDepth2[i].crclNm+"</option>");
					}
					
				}
			}
		});
		
		$("#ctgryId1").change(); */

		var calendarEl = document.getElementById('calendar2');

		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
			header: {
			      left: 'prev,next today',
			      center: 'title',
			      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth',
			    },
			    locale: 'ko',
			    navLinks: true, // can click day/week names to navigate views
			    editable: true,
			    eventLimit: true, // allow "more" link when too many events
			    events: [
			    	<c:forEach var="result" items="${schoolSchList}" varStatus="status">
			    		<c:set var="mySchYn" value="N"/>
			    		<c:set var="plCn" value="N"/>
			    		<c:set var="startTime" value="${fn:substring(result.startTime, 0, 2)}:${fn:substring(result.startTime, 2, 4)}"/>
			    		<c:set var="endTime" value="${fn:substring(result.endTime, 0, 2)}:${fn:substring(result.endTime, 2, 4)}"/>
			    		
			    		<c:choose>
			    		<c:when test="${result.plType eq 'crcl'}">
			    			<c:choose>
				    			<c:when test="${searchVO.manageCode eq 'teacher'}">
					    			<c:forEach var="list" items="${facPlList}" varStatus="status">
							    		<c:if test="${list.plId eq result.plId}">
							    			<c:set var="mySchYn" value="Y"/>
							    		</c:if>
					    			</c:forEach>
				    			</c:when>
				    			<c:otherwise>
				    				<c:if test="${result.studentId eq searchVO.frstRegisterId}">
				    					<c:set var="mySchYn" value="Y"/>
				    				</c:if>
				    			</c:otherwise>
			    			</c:choose>
			    		</c:when>
			    		<c:otherwise>
			    		<c:set var="mySchYn" value="Y"/>
				    	</c:otherwise>
				    	</c:choose>
			    		
			    		<c:if test="${mySchYn eq 'Y'}">
				    		{
				            title: <c:choose><c:when test="${result.plType eq 'all'}">'[학교]${result.title}'</c:when><c:when test="${result.plType eq 'ind'}">'[개인]${result.title}'</c:when><c:when test="${result.plType eq 'crcl'}">'[수업]${result.studySubject}'</c:when><c:otherwise>'${result.title}'</c:otherwise></c:choose>,
				            start: new Date(<c:out value='${fn:substring(result.startDt, 0,4)}'/>, <c:out value='${fn:substring(result.startDt, 5, 7)}'/>-1, <c:out value='${fn:substring(result.startDt, 8,10)}'/>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">,<c:out value="${fn:substring(result.startTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.startTime != null and result.startTime != ''}">, <c:out value="${fn:substring(result.startTime, 2, 4)}"/></c:if>
				            		),
				            end: new Date(<c:out value='${fn:substring(result.endDt, 0,4)}'/>, <c:out value='${fn:substring(result.endDt, 5,7)}'/>-1, <c:out value='${fn:substring(result.endDt, 8,10)}'/>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">,<c:out value="${fn:substring(result.endTime, 0, 2)}"/></c:if>
				            		<c:if test="${result.endTime != null and result.endTime != ''}">, <c:out value="${fn:substring(result.endTime, 2, 4)}"/></c:if>
				            		),
				            constraint: 'crclSchedule',
				            textColor: '#ffffff',
				            //color: <c:choose><c:when test="${mySchYn eq 'Y'}">'#b7ca10',</c:when><c:otherwise>'#ff0000',</c:otherwise></c:choose>		
				            <c:choose><c:when test="${result.plType eq 'ind'}">color :'#76828e',</c:when><c:when test="${result.plType eq 'all'}">color: '#b7ca10',</c:when><c:when test="${result.plType eq 'all'}">color: '#068467',</c:when></c:choose>
				            <c:if test="${result.allDayAt eq 'Y'}">allDay : true,</c:if>
				            extendedProps: {
				            	plId: '${result.plId}',
				            	plType: '${result.plType}',
			            		startDt: '${result.startDt}',
			            		endDt: '${result.endDt}',
			            		plCn: '${result.plCn}',
			            		startTime: '${startTime}',
			            		endTime: '${endTime}',
			            		placeDetail: '${result.placeDetail}',
			            		userNm: '${result.userNm}',
			            		facId: '${result.facId}',
			            		periodTxt: '${result.periodTxt}',
			            		crclId: '${result.crclId}',
			            		dayofweek: '${result.dayofweek}',
			            		spLevelNm: '${result.spLevelNm}',
			            		crclLangNm: '${result.crclLangNm}',
			            		studySubject: '${result.studySubject}'
				            }
				          }<c:if test="${not status.last}">, </c:if>
				          </c:if>
			    	</c:forEach>
			    ],
			    eventClick: function (arg) {
			    	var source1 = "";
			    	var source2 = "";
			    	if('${USER_INFO.id}' == arg.event.extendedProps.facId){
			    		source2 = "<button type=\"button\" class=\"btn-xl btn-outline-gray\" onclick=\"fn_updateLink('"+arg.event.extendedProps.crclId+"','"+arg.event.extendedProps.plId+"');\">수정</button>";
			    	}
			    	if(arg.event.extendedProps.crclLangNm == "" && arg.event.extendedProps.spLevelNm == "" && arg.event.extendedProps.studySubject == ""){
			    		source1 = "<span class='title-date'>"+arg.event.title+"(기간 : "+arg.event.extendedProps.startDt+" ~ "+arg.event.extendedProps.endDt+")</span>";
			    	}else{
			    		source1 = "<span class='title-date'>"+arg.event.extendedProps.crclLangNm+" "+arg.event.extendedProps.spLevelNm+" "+arg.event.extendedProps.studySubject+"(기간 : "+arg.event.extendedProps.startDt+" ~ "+arg.event.extendedProps.endDt+")</span>";
			    	}
			    	
			    	if(arg.event.extendedProps.plType == "crcl"){
				    	$(".fc-day-grid").append("                                                                                                       "
					    		+"	<div id='layerPopup_info' class='alert-modal' style='display:block;'>                                                "
							    +"    <div class='modal-dialog'>                                                                                         "
								+"     <div class='modal-content'>                                                                                       "
								+"       <div class='modal-header'>                                                                                      "
								+"         <h4 class='modal-title'>과정시간표</h4>                                                                        "
								+"         <button type='button' class='btn-modal-close btnModalClose' id='btnModalClose'></button>     "
								+"       </div>                                                                                                          "
								+"       <div class='modal-body'>                                                                                        "
								+"         <div class='modal-text'>                                                                                      "
								+"           <div class='modal-table-title'>                                                                             "
								/* +"             <span class='title-date'>"+arg.event.extendedProps.crclLangNm+" "+arg.event.extendedProps.spLevelNm+" "+arg.event.extendedProps.studySubject+"(기간 : "+arg.event.extendedProps.startDt+" ~ "+arg.event.extendedProps.endDt+")</span>                 	 " */
								+source1
								+"             "+arg.event.extendedProps.plCn+"                                                                          "
								+               arg.event.title
								//+"             <div class='title-desc'>"+arg.event.title+"</div>                                                         "
								+"           </div>                                                                                                      "
								+"         </div>                                                                                                        "
								+"         <table class='modal-table-wrap size-sm'>                                                                      "
								+"           <colgroup>                                                                                                  "
								+"             <col class='bg-gray' width='20%'>                                                                         "
								+"             <col width='*'>                                                                                           "
								+"           </colgroup>                                                                                                 "
								+"           <tbody>                                                                                                     "
								+"             <tr>                                                                                                      "
								+"               <td class='font-700'>담당교수</td>                                                                       "
								+"               <td>"+arg.event.extendedProps.userNm+"</td>                                                             "
								+"             </tr>                                                                                                     "
								+"             <tr>                                                                                                      "
								+"               <td class='font-700'>날짜</td>                                                                          "
								+"               <td>"+arg.event.extendedProps.startDt+"("+arg.event.extendedProps.dayofweek+")</td>                                                                                 "
								+"             </tr>                                                                                                     "
								+"             <tr>                                                                                                      "
								+"               <td class='font-700'>시간</td>                                                                          "
								+"               <td>"+arg.event.extendedProps.startTime+" ~ "+arg.event.extendedProps.endTime+"("+arg.event.extendedProps.periodTxt+"교시)</td>                                                                        "
								+"             </tr>                                                                                                     "
								+"             <tr>                                                                                                      "
								+"               <td class='font-700'>장소</td>                                                                          "
								+"               <td>"+arg.event.extendedProps.placeDetail+"</td>                                                   "
								+"             </tr>                                                                                                     "
								+"           </tbody>                                                                                                    "
								+"         </table>                                                                                                      "
								+"       </div>                                                                                                          "
								+"       <div class='modal-footer'>                                                                                      "
								+"         <button type=\"button\" class=\"btn-xl btn-outline popup-icon-arrow btnModalConfirm\" onclick=\"fn_DetailLink('"+arg.event.extendedProps.crclId+"','"+arg.event.extendedProps.plId+"');\" >수업계획서 확인</button>    "
								+source2 
								+"       </div>                                                                                                          "
								+"     </div>                                                                                                            "
								+"   </div>                                                                                                              "
								+"  </div>");
				    	}else if(arg.event.extendedProps.plType == "ind" || arg.event.extendedProps.plType == "all"){
				    		$.ajax({
				    			url : "/sch/selectIndScheduleDetail.json"
				    			, type : "get"
				    			, dataType : "json"
				    			, data : {plId : arg.event.extendedProps.plId}
				    			, success : function(data){
				    				$('#title').val(data.schVO.title);
				    				$('#startDt').val(data.schVO.startDt);
				    				$('#endDt').val(data.schVO.endDt);
				    				$('#startTimeHH').val(data.schVO.startTime.substring(0, 2)).attr("selected", "selected");
				    				$('#startTimeMM').val(data.schVO.startTime.substring(2, 4)).attr("selected", "selected");
				    				$('#endTimeHH').val(data.schVO.endTime.substring(0, 2)).attr("selected", "selected");
				    				$('#endTimeMM').val(data.schVO.endTime.substring(2, 4)).attr("selected", "selected");
				    				if(data.schVO.allDayAt == "Y"){
				    					$('input[name=allDayAt]').val(data.schVO.allDayAt).attr("checked", "checked");
				    				}else{
				    					$('input[name=allDayAt]').attr("checked", false);
				    				}
				    				
				    				$('#campusId').val(data.schVO.campusId).attr("selected", "selected");
				    				$('#placeDetail').val(data.schVO.placeDetail);
				    				$('#plCn').val(data.schVO.plCn);

				    				Lms.common_ui.init();
				    				
				    				$('#flag').val("update");
				    				$('#plType').val("ind");
				    				$('#plId').val(data.schVO.plId);
				    				$("#layerPopup").show();
				    				
				    				if(arg.event.extendedProps.plType == "all"){
				    					$('.btn-point').hide();
				    				}else{
				    					$('.btn-point').show();
				    				}
				    				
				    			}, error : function(){
				    				alert("error");
				    			}
				    		});
				    	}
			    }
			}); 
		   
		calendar.render();

	});
	
	function checkForm(form) {	
		if($('#title').val() == "" || $('#title').val() == null){
			alert("일정명을 입력하세요.");
			$('#title').focus();
			$('#layerPopup').show();
			return false;
			
		}else{
			<c:choose>
				<c:when test="${registerFlag eq '수정'}">
					if(confirm('<spring:message code="common.update.msg" />')) {
				</c:when>
				<c:otherwise>
					if(confirm('<spring:message code="common.regist.msg" />')) {
						$('#schduleForm').submit();
				</c:otherwise>
			</c:choose>
				return true;
			}else {
				return false;
			}
		}
	}
	
	function fn_updateLink(crclId, plId){
		location.href = "/lms/manage/studyPlanReg.do?menuId=${searchVO.menuId}&crclId="+crclId+"&plId="+plId+"&tabType=T";	
	}
	
	function fn_DetailLink(crclId, plId){
		location.href = "/lms/manage/studyPlanView.do?plId="+plId+"&menuId=${searchVO.menuId}&crclId="+crclId+"&tabType=T";	
	}
	
	//레이어 닫기
	$(document).on("click", ".btnModalClose", function(){
		var id = $(this).parents(".alert-modal").attr("id");
		
		if(id == "layerPopup"){
			$("#layerPopup").hide();
		}else{
			$(this).parents(".alert-modal").remove();	
		}
	});
	</script>
	
	<div class="page-content-wrap pt-0">
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <article class="content-wrap">
              <div class="box-wrap mb-40">
              <form name="frm" method="post" action="<c:url value="/sch/scheduleCalendar.do"/>">
              	<input type="hidden" name="menuId" value="${searchVO.menuId}"/>
              	
                <h3 class="title-subhead">교육과정 검색</h3>
                <div class="flex-row-ten">
                  <div class="flex-ten-col-3">
                    <div class="ell">
                    	<c:set var="tmpLang" value=""/>
                      <select id="ctgryId1" name="searchSysCode1" class="select2" data-select="style3" data-placeholder="언어 선택">
                      	
                        <c:forEach var="result" items="${curriculumList}" varStatus="status">
                        	<option value="">언어선택</option>
                        	<c:if test="${tmpLang ne result.crclLang}">
								<option value="${result.crclLang}" <c:if test="${result.crclLang eq searchVO.searchSysCode1}">selected="selected"</c:if>><c:out value="${result.crclLangNm}"/></option>
							</c:if>
							<c:set var="tmpLang" value="${result.crclLang}"/>
						</c:forEach>
                      </select>
                    </div>
                  </div>
                  <!-- <div class="flex-ten-col-3">
                    <div class="ell">
                      <select id="ctgryId2" name="searchSysCode2" class="select2" data-select="style3" data-placeholder="전체 교육과정">
						  <option value="">전체</option>
					  </select>
                    </div>
                  </div> -->
                  <div class="flex-ten-col-4">
                    <div class="ell">
                      <input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm}" placeholder="교육과정명">
                    </div>
                  </div>
                  <div class="flex-ten-col-3">
                    <div class="ell">
                      <input type="text" name="searchFacNm" value="${searchVO.searchFacNm}" placeholder="교수명">
                    </div>
                  </div>
                </div>

                <button class="btn-sm font-400 btn-point mt-20">검색</button>
                </form>
              </div>
            </article>
            <article class="content-wrap">
              <div id='calendar2'></div>
              <%-- 
              <div class="right-align mt-20">
                <a href="#" class="btn-point btn-sm" id="btnRegist">일정 등록</a>
              </div>
               --%>
            </article>
          </section>
        </div>
        
        <c:set var="now" value="<%=new java.util.Date() %>"/>
		<c:set var="today"><fmt:formatDate value="${now }" pattern="yyyy-MM-dd"/></c:set>
        <!-- 팝업 -->
        <div id="layerPopup" class="alert-modal" style="display:none;">
		    <div class="modal-dialog modal-top">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">${plTypeNm }일정 등록</h4>
		          <button type="button" class="btn-modal-close btnModalClose"></button>
		        </div>
		        <form:form commandName="scheduleMngVO" id="schduleForm" name="scheduleMngVO" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/sch/insertIndSchedule.do">
		        <form:hidden path="plType" value="ind"/>
		        <form:hidden path="plId" value=""/>
		        <form:hidden path="flag" value="regist"/>
		        <form:hidden path="crclLang" value="${curriculumVO.crclLang }"/>
		        <div class="modal-body">
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정명 <span class="terms-necessary">*</span></dt>
		            <dd>
		              <form:input path="title" placeholder="일정명을 입력하세요."/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">일정 <span class="terms-necessary">*</span></dt>
		            <dd class="form-schedule-wrap">
		              <%-- <form:input path="startDt" value=""  class="ell date datepicker type2"/>
		              <i>~</i>
		              <form:input path="endDt" value="" class="ell date datepicker type2"/> --%>
		              <form:input  path="startDt" value="${today }"  class="ell date datepicker type2"/>
		              <i>~</i>
		              <form:input path="endDt" value="${today }" class="ell date datepicker type2"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">시간</dt>
		            <dd class="form-schedule-wrap schedule-time">
		              <form:select path="startTimeHH" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              :
		               <form:select path="startTimeMM" class="select2" data-select="style1" data-placeholder="시작시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              <i>~</i>
		              <form:select path="endTimeHH" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="1" end="24" step="1">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,0, 2) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              :
		               <form:select path="endTimeMM" class="select2" data-select="style1" data-placeholder="종료시간">
		                <option value="00">00</option>
		                <c:forEach var="i" begin="10" end="60" step="10">
							<option value="<c:if test="${i<10}">0</c:if>${i}" <c:if test="${fn:substring(searchVO.startTime ,2, 4) eq i}">selected="selected"</c:if>><c:if test="${i<10}">0</c:if>${i}</option>
						</c:forEach>
		              </form:select>
		              <label class="checkbox-img">
		                <form:checkbox path="allDayAt" value="Y"/>
		                <span class="custom-checked"></span>
		                <span class="text">종일</span>
		              </label>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap">
		            <dt class="form-title">장소</dt>
		            <dd class="form-schedule-wrap schedule-place">
		              <form:select path="campusId" class="select2" data-select="style1" data-placeholder="캠퍼스 선택">
		                <option value="">전체</option>
						<c:forEach var="result" items="${sysCodeList}" varStatus="status">
							<c:if test="${result.ctgryNm ne '대분류'}">
								<option value="${result.ctgryId}" <c:if test="${'' eq result.ctgryId}">selected="selected"</c:if>>${result.ctgryNm }</option>
							</c:if>
						</c:forEach>
		              </form:select>
		              <form:input path="placeDetail" placeholder="상세위치정보"/>
		            </dd>
		          </dl>
		          <dl class="modal-form-wrap schedule-contents">
		            <dt class="form-title">
		              내용
		              <div class="text-typing"><span class="font-point textCount">0</span>/100</div>
		            </dt>
		            <dd class="form-schedule-wrap">
		              <form:textarea path="plCn" class="textTyping" maxlength="100" placeholder="내용을 입력해주세요."></form:textarea>
		            </dd>
		          </dl>
		        </div>
		        
		        <div class="modal-footer">
		          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
		          <button type="button" class="btn-xl btn-point " onclick="return checkForm(document.scheduleMngVO);">확인</button>
		        </div>
		        </form:form>
		      </div>
		    </div>
		  </div>
        
        
<c:import url="/template/bottom.do" charEncoding="utf-8"/>