<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="egovframework.com.cmm.service.EgovProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<c:set var="_IMG" value="/template/manage/images"/>
<c:set var="_C_IMG" value="/template/common/images"/>

<!doctype html>
<html lang="ko">
<head>
<!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>CFL 특수외국어교육진흥사업 한국외국어대학교</title>

  <meta name="title" content="CFL 특수외국어교육진흥사업 한국외국어대학교">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="CFL 특수외국어교육진흥사업 한국외국어대학교">
  <meta property="og:description" content="">
  <meta property="og:image" content="/template/lms/imgs/common/og.jpg">
  <meta property="og:url" content="">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="/template/lms/imgs/common/favicon.png">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="/template/lms/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/template/lms/lib/froala_editor/froala_editor.pkgd.min.css"><!-- froala_editor -->
  <link rel="stylesheet" href="/template/lms/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="/template/lms/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="/template/lms/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

	<link rel="stylesheet" href="/template/lms/css/common/base.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/common/common_staff.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/common/board_staff.css?v=1">
	<link rel="stylesheet" href="/template/lms/css/main/staff_main_common.css?v=2">
	<link rel="stylesheet" href="/template/lms/css/main/staff_main_login.css?v=2">
	
	<!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="/template/lms/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="/template/lms/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="/template/lms/lib/slick/slick.js"></script><!-- slick -->
  <script src="/template/lms/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="/template/lms/lib/jquery_ui/jquery-ui.js"></script>
  <script src="/template/lms/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="/template/lms/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="/template/lms/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="/template/lms/js/common.js?v=1"></script>
  <script src="/template/common/js/common.js"></script>
  
  <link rel="stylesheet" href="/template/lms/css/common/modal.css?v=2">
  <link rel="stylesheet" href="/str/cre/lyt/tmplat/sit/LYTTMP_0000000000001/style.css">
  <link rel="stylesheet" href="/template/lms/css/common/table_staff.css?v=2">
<script>
$(document).ready(function(){
	$(".btn_ok").click(function(){
		var idList = [],
			nameList = [];

		if($(".chk:checked").length < 1){
			alert("파일을 한개만 선택해 주세요.");
		}else{
			$(".chk").each(function(){
				if($(this).is(":checked")){
					idList.push($(this).val());
					nameList.push($(this).data("nm"));
				}
			});

			window.opener.file(idList, nameList);
			window.close();
		}

		return false;
	});


	$("input[name=searchScope]").click(function(){
		if("Y" == $(this).val()){
			$("#searchRegisterNm").prop("readonly", false);
		}else{
			$("#searchRegisterNm").val("");
			$("#searchRegisterNm").prop("readonly", true);
		}
	});
	
	$(".btnModalCancel, .btnModalClose").click(function(){
		window.close();
	});
});

function fnReset(){
	$("input:radio[id='searchScopeY']").prop("checked",true);
	$("#searchRegisterNm").val("");
	$("#searchRegisterNm").prop("readonly", true);
	$('#searchCrclNm').val("");
	$('#searchFileNm').val("");
	$("input:checkbox").attr("checked", false);
}
</script>
<style>
#before_data_modal{width:634px;padding:0 15px;}
.modal-dialog .modal-header {
  position: relative;
  height: 80px;
  line-height: 80px;
  padding: 0 30px;
  border-bottom: 1px solid #ddd;
  font-size: 27px;
  font-weight: 700;
  text-align: left;
}
.modal-dialog .modal-header .btn-modal-close {
  position: absolute;
  top: 50%;
  right: 30px;
  width: 20px;
  height: 20px;
  border: 0;
  background-color: transparent;
  background-image: url(/template/lms/imgs/common/btn_alert_close.svg);
  background-repeat: no-repeat;
  background-position: center;
  font-size: 0;
  transform: translateY(-50%);
  cursor: pointer;
}
.modal-footer{text-align:center;margin:20px 0;}
</style>
</head>
<body style="overflow-x:hidden;">
<!-- <div id="before_data_modal" class="alert-modal" style="display:block;"> -->
<div id="before_data_modal">
	<form id="listForm" method="post" action="/cmm/fms/studyFileList.do">
		<input type="hidden" name="hostCode" value="${searchVO.hostCode}"/>
		<input type="hidden" name="searchTarget"/>
	    <div class="modal-dialog modal-top">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h4 class="modal-title">이전 수업자료 불러오기</h4>
	          <button type="button" class="btn-modal-close btnModalClose"></button>
	        </div>
	        <div class="modal-body before-quiz-wrap">
	          <div class="box-wrap left-align mb-30">
	            <div class="flex-row-ten">
	              <div class="flex-ten-col-5 mb-20">
	              	<input type="text" name="searchCrclNm" value="${searchVO.searchCrclNm }" id="searchCrclNm" placeholder="과정명"/>
	              </div>
	              <div class="flex-ten-col-5 mb-20">
	                <div class="ell">
	                  <input type="text" name="searchRegisterNm" id="searchRegisterNm" value="${searchVO.searchRegisterNm }"  ${searchVO.searchScope eq 'N' ? 'readonly': '' } placeholder="파일등록자"/>
	                </div>
	              </div>
	              <div class="flex-ten-col-12">
	                <div class="ell">
	                  <input type="text" name="searchFileNm" value="${searchVO.searchFileNm }" id="searchFileNm" placeholder="자료명"/>
	                </div>
	              </div>
	              <div class="flex-ten-col-12 flex align-items-center">
	                <label class="checkbox">
	                  <input type="checkbox" name="fileExtOther" value="Y" ${param.fileExtOther eq 'Y' ? 'checked' : '' }/>
	                  <span class="custom-checked"></span>
	                  <span class="text">파일</span>
	                </label>
	                <label class="checkbox">
	                  <input type="checkbox" name="fileExtImg" value="Y" ${param.fileExtImg eq 'Y' ? 'checked' : '' }/>
	                  <span class="custom-checked"></span>
	                  <span class="text">사진</span>
	                </label>
	                <label class="checkbox">
	                  <input type="checkbox" name="fileExtMov" value="Y" ${param.fileExtMov eq 'Y' ? 'checked' : '' }/>
	                  <span class="custom-checked"></span>
	                  <span class="text">동영상</span>
	                </label>
	                <label class="checkbox">
	                  <input type="radio" name="searchScope" id="searchScopeN" value="N" ${searchVO.searchScope eq 'N' ? 'checked' : '' }/>
	                  <span class="custom-checked"></span>
	                  <span class="text">나의 수업자료</span>
	                </label>
	              </div>
	            </div>
			
	            <button class="btn-sm font-400 btn-point mt-20" type="submit">검색</button>
	            <button class="btn-sm font-400 btn-outline mt-20" onclick="fnReset();">초기화</button>
	          </div>
	          <table class="common-table-wrap size-sm">
	            <colgroup>
	              <col style="width:7%;">
	              <col style="width:7%;">
	              <col style="width:9%;">
	              <col style="width:21%;">
	              <col style="width:21%;">
	              <col style="width:9%;">
	            </colgroup>
	            <thead>
	              <tr class="bg-gray-light font-700">
	                <th>선택</th>
	                <th>구분</th>
	                <th>미리보기</th>
	                <th>파일명</th>
	                <th>과정명</th>
	                <th>등록자</th>
	              </tr>
	            </thead>
	            <tbody>
	            	<c:choose>
			    		<c:when test="${not empty resultList }">
			    			<c:forEach var="result" items="${resultList}" varStatus="status">
					    		<c:set var="depthNm" value="${fn:split(result.ctgryPathByName,'>')}"/>
								<tr>
					                <td>
					                	<label class="checkbox">
					 						<input type="checkbox" class="chk checkList" value="${result.streFileNm}" data-nm="<c:out value="${result.orignlFileNm}"/>" />
					 						<span class="custom-checked"></span>
					 					</label>
					                </td>
					                <td>${result.fileExtsn}</td>
					                <td>
					                	<c:url var="downLoad" value="/cmm/fms/FileDown.do">
										<c:param name="atchFileId" value="${result.atchFileId}"/>
											<c:param name="fileSn" value="${result.fileSn}"/>
							          	</c:url>
					              		<a href="<c:out value='${downLoad}'/>" class="attachment icon-file font-gray test2" onclick="fn_egov_downFile(this.href);return false;">
								        	<div class="board-gallery-lists size-sm" style="background-image:url(/template/lms/imgs/common/img_download_thumbnail_sm.jpg);"></div>
								        </a>
					                </td>
					                <td class="left-align">${result.orignlFileNm}</td>
					                <td class="left-align">${result.crclNm }</td>
					                <td>${result.userNm }</td>
					            </tr>
							</c:forEach>
			    		</c:when>
			    		<c:otherwise>
			    			<tr>
					        	<td class="listCenter" colspan="6"><spring:message code="common.nodata.msg" /></td>
					      	</tr>
			    		</c:otherwise>
			    	</c:choose>
	            </tbody>
	          </table>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
	          <button class="btn-xl btn-point btnModalConfirm btn_ok" data-target="${param.target}">선택한 수업자료 등록</button>
	        </div>
	      </div>
	    </div>
	</form>
</div>
</body>
</html>