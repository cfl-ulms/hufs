<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="CML" value="/template/lms"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="step" value="2"/>
	<c:param name="menu" value="join"/>
</c:import>

<script>
$(document).ready(function(){
	
	var emailStr = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/;
	$("#email1 , #email3").keyup(function(){
		
		var str1 = $(this).val();
		var str_id ='case1';
		var str_msg= '';
		if($(this).attr("id")  == 'email3'){
			str_id ='case2';
			str_msg= '본인확인 이메일';
		}
		
		if(emailStr.test(str1) == true){
			if(str_id == 'case2'){
				alert("본인확인을 위한 이메일을 입력해주세요.");
			} else {
				alert("이메일 아이디를 입력해주세요.");
			}
			infoCase(4,str_id,str_msg);
			return false;
		} else if(str1 == ''|| str1 == ' '){
			infoCase(4,str_id,str_msg);
		} else if(str1.indexOf('@') != -1){
			alert("특수기호 @ 는 사용하실 수  없습니다.");
			infoCase(1,str_id,str_msg);
			return false;
		} else if(str1.indexOf('.com') != -1){
			alert(str_msg+" 입력하는 곳에 .com 이 들어간 아이디는 \n사용하실 수  없습니다.");
			infoCase(2,str_id,str_msg);
			return false;
		} else if(str1.indexOf('.net') != -1){
			alert(str_msg+" 입력하는 곳에 .net 이 들어간 아이디는 \n사용하실 수  없습니다.");
			infoCase(3,str_id,str_msg);
			return false;
		} else {
			infoCase('',str_id,str_msg);
		}
	});
	
	$("#email2,#email4").keyup(function(){
		var email_str = $(this).val();
		var index = 0;
		var email_id = 'email';

		if($(this).attr("id") == 'email4'){
			email_id = 'email5';			
		}
		
		$("#"+email_id).find("option").each(function(){
			
			if(email_str == $(this).val()){
				$(this).attr("selected","selected");
				index++;
			} 
		});
		
		if(index == 0){
			$("#"+email_id).val("etc").prop("selected",true);
			$("#select2-"+email_id+"-container").text("기타(직접입력)");
			$("#select2-"+email_id+"-container").attr("title","기타(직접입력)");
		}
	});
	
	
	$("#btn_join").click(function(){
		var userSeCode = $("input[name=userSeCode]:checked").val(),
			stNumber = $("input[name=stNumber]").val(),
			authAt = $("input[name=authAt]").val();
		
	  	// 이메일 아이디 입력 유효값 체크
	  	var str3 = '';
		str3 = $("#email1").val();
	  
		if(emailStr.test(str3) == true || str3 == ''  ){
			alert("이메일 아이디를 입력해 주세요.");
			$("#email1").focus();
			infoCase(4,'case1');
			return false;
		} else if(str3.indexOf('@') != -1){
			alert("특수기호 @ 는 사용하실 수 없습니다.");
			infoCase(1,'case1');
			return false;
		} else if(str3.indexOf('.com') != -1){
			alert("이메일 아이디 입력하는 곳에 .com 이 들어간 아이디는 \n사용하실 수  없습니다.");
			infoCase(2,'case1');
			return false;
		} else if(str3.indexOf('.net') != -1){
			alert("이메일 아이디 입력하는 곳에 .net 이 들어간 아이디는 \n사용하실 수  없습니다.");
			infoCase(3,'case1');
			return false;
		}
		  
	  	// 본인확인 이메일  입력 유효값 체크
	  	var str4 = '';
		str4 = $("#email3").val();
		if(emailStr.test(str4) == true || str4 == ''  ){
			alert("본인확인을 위한 이메일을 입력해 주세요.");
			$("#email3").focus();
			infoCase(4,'case2');
			return false;
		} else if(str4.indexOf('@') != -1){
			alert("특수기호 @ 는 사용하실 수 없습니다.");
			infoCase(1,'case2');
			return false;
		} else if(str4.indexOf('.com') != -1){
			alert("본인확인 이메일 입력하는 곳에 .com 이 들어간 아이디는 \n사용하실 수 없습니다.");
			infoCase(2,'case2');
			return false;
		} else if(str4.indexOf('.net') != -1){
			alert("본인확인 이메일 입력하는 곳에 .net 이 들어간 아이디는 \n사용하실 수 없습니다.");
			infoCase(3,'case2');
			return false;
		}
		
		if($("#password").data("check") != 1){
			alert("비밀번호를 확인해주세요.");
			return false;
		}else if($("#password_chk").data("check") != 1){
			alert("비밀번호 확인을 확인해주세요.");
			return false;
		}else{
			$(".required").each(function(i){
				var leng = $(".required").length - 1;
				if(!$(this).val()){
					alert($(this).attr("title") + "을 입력(선택)해 주세요.");
					return false;
				}
					
				if(leng == i){
					//외대 학생 인증
					if(userSeCode == "06" && stNumber){
						if(authAt != "Y"){
							if(stNumber){
								hufsStu(stNumber);
							}
						}
					}else if(userSeCode == "06" && !stNumber){
						alert("학번을 입력해 주세요.");
					}else{
						$("#member").submit();
					}
				}
			});
		}
			
		return false;
	});
	
	//메일주소
	$("#email").change(function(){
		if($(this).val() == 'etc'){
			$("#email2").val("");
		} else {
			$("#email2").val($(this).val());
		}
	});
	  
	//메일주소2
	$("#email5").change(function(){
		if($(this).val() == 'etc'){
			$("#email4").val("");
		} else{
			$("#email4").val($(this).val());
		}
	});
	  
	//비밀번호
	$("#password").bind("input",function(){
		var pw = $(this).val(),
			message = "",
			passwordRules = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,30}$/;
		 if(pw.length < 8){
		  message = "8자 이상";
		 }
	 
		 if(!passwordRules.test(pw)){
		  message += "영문, 숫자, 특수문자 포함하여";
		 }
	 
		if(message.length > 0){
			$("#pw_desc").text(message + " 입력해주세요.");
		}else{
			$("#pw_desc").text(message);
			$(this).data("check","1");
	 	}
	});
	  
	//비밀번호 확인
	$("#password_chk").bind("input",function(){
		var pw = $("#password").val(),
			pw_chk = $(this).val(),
			message = "";
	 
		if(pw != pw_chk){
			message = "비밀번호를 확인해 주세요.";
		}
		$("#pwchk_desc").text(message);
		if(message.length == 0){
			$(this).data("check","1");
		}
	});
	  
	//숫자만 입력
	$(".onlyNum").bind("input",function(event){
	 	var inpVal = $(this).val().replace(/[^0-9]/g,'');
		$(this).val(inpVal);
	});
	  
	//소속학교
	$("#school").change(function(){
		$("#groupNm01").val($(this).val());
	});
	  
	//세부전공
	$("#majorSel").change(function(){
		$("#major").val($(this).val());
	});
});

//안내멘트 숨김 처리
function infoCase(case_no,case_id,str_msg){
	
	if(case_no == ''){
		$("#"+case_id).hide();
	} else {
		$("#"+case_id).show();
				
		if(case_no == 1){
			$("#"+case_id).text("특수기호 @ 는 사용하실 수 없습니다.");
		} else if(case_no == 2){
			$("#"+case_id).text(str_msg+" 입력하는 곳에 .com 은 사용하실 수 없습니다.");
		} else if(case_no == 3){
			$("#"+case_id).text(str_msg+" 입력하는 곳에 .net 은 사용하실 수 없습니다.");
		} else if(case_no == 4){
			if(case_id == 'case2'){
				$("#"+case_id).text("본인확인을 위한 이메일을 입력해주세요.");
			} else {
				$("#"+case_id).text("이메일 아이디를 입력해주세요.");
			}
		} else {
			$("#"+case_id).hide();
		}
	}
}

//외대학생 인증
function hufsStu(stNumber){
	var brthdy01 = $("input[name=brthdy01]").val(),
		brthdy02 = $("#sel_month").val(),
		brthdy03 = $("#sel_date").val(),
		birthday = brthdy01.substring(2,4) + brthdy02 + brthdy03
		name = $("input[name=userNm]").val(),
		sex = $("input[name=sexdstn]:checked").val() == "M" ? "1" : "2";
	
	$.ajax({
		url : "/crypto/ariaEn.do",
		dataType : "json",
		type : "POST",
		data : {data : stNumber},
		success : function(data){
			//console.log(encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data));
			$.ajax({
		  		url:encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data),
		  		contentType: "application/javascript",
		  		dataType: 'jsonp',
		  		type: "POST",
		  		jsonp: "callback",
		  		async: false,
		  		success: function(data){
		   			//console.log(data);
		   			if( data[0].SuccessFg == 'N' ){
		    			alert("인증과정오류 : "+data[0].rtnToken);
		   			}else if(data[0].birthday.substring(0,6) != birthday || data[0].name != name || data[0].sex != sex || data[0].student_yn != "Y"){
		   				alert("사용자 정보가 존재하지 않거나 한국외국어대학교 학번 정보와 일치하지 않습니다 . 본인의 성명, 생년월일, 성별 및 학번이 정확하게 입력되었는지 다시 한 번 확인해 주시기 바랍니다. 본교생 인증의 경우 가입일자 기준 재학생에 한하여 가능합니다.");
		   			}else{
		    			$("input[name=stGrade]").val(data[0].grade);
		    			$("input[name=stClass]").val(data[0].major);
		    			$("input[name=authAt]").val("Y");
		    			$("#member").submit();
		   			}
		  		}, error:function(jqxhr, status, error){
		  			//alert("구성원 인증 중 오류 발생!\n[code:"+jqxhr.status+"\n"+"message:"+jqxhr.responseText+"\n]");
		  			//console.log("error : " + encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data));
		  			alert("사용자 정보가 존재하지 않거나 한국외국어대학교 학번 정보와 일치하지 않습니다 . 본인의 성명, 생년월일, 성별 및 학번이 정확하게 입력되었는지 다시 한 번 확인해 주시기 바랍니다. 본교생 인증의 경우 가입일자 기준 재학생에 한하여 가능합니다.");
		  		}
		 	});
		}, error : function(){
			alert("error");
		}
	});
}

function check(frm){
	return true;
}

</script>
<%-- 학번 확인 용 --%>
<%-- 
<button type="button" class="<c:out value="aa"/>" onclick="hufsStu(201901570);">확인</button>
 --%>
<form action="<c:url value='/uss/umt/user/EgovUserInsert.do'/>" id="member" name="member" method="post" onsubmit="return check(this)">

	<div class="join-contents-wrap step02">
        <p class="title">필수항목</p>
        <div class="mb-50">
          <dl class="join-form-con">
            <dt class="join-form-title">아이디(이메일 주소 입력)</dt>
            <dd class="join-form-body group-sort-wrap">
            	<div style="width: 100%; margin-top: 9px;">
            		<label>
		              <input type="text" id="email1" class="required" name="email1" placeholder="이메일 아이디를 입력해주세요." required="required" title="이메일 아이디"  style="margin-top: -1px;">
		              <i class="icon-email">@</i>
		              <input type="text" id="email2" class="required" name="email2" required="required" title="이메일 주소" value="naver.com" style="margin-top: -1px;">
		              <select id="email" class="select2" data-select="style1">
		                <option value="naver.com">네이버(naver.com)</option>
		                <option value="hanmail.net">다음(hanmail.net)</option>
		                <option value="gmail.com">구글(gmail.com)</option>
		                <option value="etc">기타(직접입력)</option>
		              </select>
            		</label>
            	</div>
            	<div style="margin-bottom: 5px;">
            		<label style="color: red;" id="case1" >이메일 아이디를 입력해주세요</label>
            	</div>
            </dd>
          </dl>
          <dl>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">비밀번호</dt>
            <dd class="join-form-body">
              <input type="password" id="password" class="required" name="password" placeholder="비밀번호를 입력해주세요." required="required" title="비밀번호">
              <p id="pw_desc" class="join-form-desc">8자 이상 영문, 숫자, 특수문자 포함하여 입력해주세요.</p>
            </dd>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">비밀번호 확인</dt>
            <dd class="join-form-body">
              <input type="password" id="password_chk" class="required" placeholder="비밀번호를 한 번 더 입력해주세요." required="required" title="비밀번호 확인">
              <p id="pwchk_desc" class="join-form-desc">비밀번호를 확인해 주세요.</p>
            </dd>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">이름</dt>
            <dd class="join-form-body">
              <input type="text" name="userNm" class="required" placeholder="이름을 입력해주세요." required="required" title="이름">
            </dd>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">생년월일</dt>
            <dd class="join-form-body three-boxes" style="margin-top: 9px; margin-bottom: 9px;">
              <!-- <input type="text" class="onlyNum required" name="brthdy01" placeholder="년(4자)" required="required" title="생년월일"> -->
              <select id="sel_month" class="select2 required" name="brthdy01" data-select="style1" required="required" title="생년월일" >
           		<option value="">년</option>
           		<c:set var="nowYear" value="2022"/>
              	<c:forEach begin="1900" end="2022" var="year">
              		<option value="${nowYear - year + 1900}">${nowYear - year + 1900}년</option>
              	</c:forEach>
              </select>
              <select id="sel_month" class="select2 required" name="brthdy02" data-select="style1" required="required" title="생년월일">
                <option value="">월</option>
                <option value="01">1월</option>
                <option value="02">2월</option>
                <option value="03">3월</option>
                <option value="04">4월</option>
                <option value="05">5월</option>
                <option value="06">6월</option>
                <option value="07">7월</option>
                <option value="08">8월</option>
                <option value="09">9월</option>
                <option value="10">10월</option>
                <option value="11">11월</option>
                <option value="12">12월</option>
              </select>
              <select id="sel_date" class="select2 required" name="brthdy03" data-select="style1" required="required" title="생년월일">
                <option value="">일</option>
                <option value="01">1일</option>
                <option value="02">2일</option>
                <option value="03">3일</option>
                <option value="04">4일</option>
                <option value="05">5일</option>
                <option value="06">6일</option>
                <option value="07">7일</option>
                <option value="08">8일</option>
                <option value="09">9일</option>
                <option value="10">10일</option>
                <option value="11">11일</option>
                <option value="12">12일</option>
                <option value="13">13일</option>
                <option value="14">14일</option>
                <option value="15">15일</option>
                <option value="16">16일</option>
                <option value="17">17일</option>
                <option value="18">18일</option>
                <option value="19">19일</option>
                <option value="20">20일</option>
                <option value="21">21일</option>
                <option value="22">22일</option>
                <option value="23">23일</option>
                <option value="24">24일</option>
                <option value="25">25일</option>
                <option value="26">26일</option>
                <option value="27">27일</option>
                <option value="28">28일</option>
                <option value="29">29일</option>
                <option value="30">30일</option>
                <option value="31">31일</option>
              </select>
            </dd>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">성별</dt>
            <dd class="join-form-body radio-btn">
              <label class="checkbox circle">
                <input type="radio" name="sexdstn" checked="checked" value="M">
                <span class="custom-checked"></span>
                <span class="text">남성</span>
              </label>
              <label class="checkbox circle">
                <input type="radio" name="sexdstn" value="W">
                <span class="custom-checked"></span>
                <span class="text">여성</span>
              </label>
            </dd>
          </dl>
          
          <dl class="join-form-con">
            <dt class="join-form-title">본인확인 이메일</dt>
              <dd class="join-form-body group-sort-wrap">
              	<div style="width: 100%; margin-top: 9px;">
	              <input type="text" id="email3" name="email3" placeholder="본인확인을 위한 이메일을 입력해주세요." style="margin-top: -1px;">
	              <i class="icon-email">@</i>
	              <input type="text" id="email4" name="email4" style="margin-top: -1px;">
	              <select id="email5" class="select2" data-select="style1">
	                <option value="etc">기타(직접입력)</option>
	                <option value="naver.com">네이버(naver.com)</option>
	                <option value="daum.net">다음(daum.net)</option>
	                <option value="gmail.com">구글(gmail.com)</option>
	              </select>
              	</div>
               	<div style="margin-bottom: 5px;">
            		<label style="color: red;" id="case2" >본인확인을 위한 이메일을 입력해주세요</label>
            	</div>
            </dd>
          </dl>
          
          <dl class="join-form-con">
            <dt class="join-form-title">휴대전화번호</dt>
            <dd class="join-form-body">
              <select class="select2" name="geocode" data-select="style1">
              		<option value="82">대한민국 +82</option>
	              	<c:forEach var="result" items="${telNumList}" varStatus="status">
	              		<c:if test="${result.code ne '82'}">
	              			<option value="${result.code}">${result.codeNm} +${result.code}</option>
	              		</c:if>
	              	</c:forEach>
              </select>
              <input type="text" class="onlyNum required" name="moblphonNo" placeholder="전화번호를 입력해주세요." required="required" title="휴대전화번호">
              <p class="join-form-desc">(-)를 제외하고 번호만 입력해주세요.</p>
            </dd>
          </dl>
          <dl class="join-form-con">
            <dt class="join-form-title">소속</dt>
            <dd class="join-form-body group-sort-wrap">
              <div class="group-sort-lables">
                <label class="checkbox circle">
                  <input type="radio" class="test" data-group=".groupUni" name="userSeCode" checked value="06">
                  <span class="custom-checked"></span>
                  <span class="text">한국외국어대학교</span>
                </label>
                <label class="checkbox circle">
                  <input type="radio" class="test" data-group=".groupEtc" name="userSeCode" value="04">
                  <span class="custom-checked"></span>
                  <span class="text">기타 대학</span>
                </label>
                <label class="checkbox circle">
                  <input type="radio" class="test" data-group=".groupGeneral" name="userSeCode" value="02">
                  <span class="custom-checked"></span>
                  <span class="text">일반</span>
                </label>
              </div>
              <div class="group-sort-form groupUni open">
                <div class="join-group-inner">
                  <input type="text" class="group-uni" name="stNumber" placeholder="학번을 입력해 주세요 (한국외국어대학교 학생 인증)">
                  <input type="hidden" name="stGrade">
                  <input type="hidden" name="stClass">
                  <input type="hidden" name="authAt" value="N">
                </div>
              </div>
              <div class="group-sort-form groupEtc">
                <div class="join-group-inner">
                  <input type="text" id="groupNm01" name="groupNm01" placeholder="소속명(대학명)을 입력해주세요.">
                  <select id="school" class="select2" data-select="style1">
                    <option value="">기타(직접입력)</option>
                    <c:forEach var="result" items="${schoolList}" varStatus="status">
	              		<option value="${result.codeNm}">${result.codeNm}</option>
	              	</c:forEach>
                  </select>
                  <p class="join-form-desc">소속명은 특수외국어 과정 수료 시 수료증에 기재되는 정보로 정확히 입력해 주세요.</p>
                </div>
                <div class="join-group-inner">
                  <input type="text" id="major" name="major" placeholder="세부전공을 선택 입력해주세요.">
                  <select id="majorSel" class="select2" data-select="style1">
                    <option value="">기타(직접입력)</option>
                    <c:forEach var="result" items="${languageList}" varStatus="status">
		          		<c:if test="${not empty result.upperCtgryId}">
		          			<option value="${result.ctgryNm}">${result.ctgryNm}</option>
		            	</c:if>
					</c:forEach>
                  </select>
                </div>
              </div>
              <div class="group-sort-form groupGeneral">
                <div class="join-group-inner">
                  <input type="text" class="group-uni" name="groupNm02" placeholder="소속명을 입력해 주세요">
                  <p class="join-form-desc">소속명은 특수외국어 과정 수료 시 수료증에 기재되는 정보로 정확히 입력해 주세요.</p>
                </div>
              </div>
            </dd>
          </dl>
          <p style="color:red;margin-top:5px;">* 한국외국어대학교 학생 인증은 가입일 기준 재학생에 한하여 가능하며, 반드시 소속을 한국외국어대학교로 선택하여야 합니다. 기타대학이나 일반을 선택 후 학번을 입력할 경우 한국외국어대학교 학생 인증이 되지 않으며, 개별 학과에서 운영하는 본교생 대상 프로그램에 지원하실 수 없습니다.</p>
        </div>
        <p class="title">선택항목</p>
        <div>
          <p class="mt-20 mb-15">관심 특수외국어 (최대 3개 선택)</p>
          <!-- tab style -->
          <ul class="tab-wrap box-style">
          	<c:forEach var="result" items="${languageList}" varStatus="status">
          		<c:if test="${not empty result.upperCtgryId and result.ctgryId ne 'ALL'}">
	          		<c:set var="imgSrc">
		          		<c:import url="/lms/common/flag.do" charEncoding="utf-8">
							<c:param name="ctgryId" value="${result.ctgryId}"/>
						</c:import>
	          		</c:set>
	          		<li class="tab-list">
	              		<img src="${CML}/imgs/common/flag/${imgSrc}" alt="${result.ctgryNm} 국기" data-id="${result.ctgryId}"> ${result.ctgryNm}
	            	</li>
            	</c:if>
			</c:forEach>
          </ul>
          <div id="lang_box">
          	<input type="hidden" name="trgetIdList"/>
          </div>
        </div>
        <div class="center-align mt-50">
          <a href="/uss/umt/cmm/EgovStplatCnfirmMber.do" class="btn-xl btn-outline-gray">이전으로</a>
          <a href="#" id="btn_join" class="btn-xl btn-point">가입하기</a>
        </div>
      </div><!-- join-contents-wrap -->
</form>
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>