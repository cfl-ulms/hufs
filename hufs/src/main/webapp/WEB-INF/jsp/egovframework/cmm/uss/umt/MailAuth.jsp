<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="CML" value="/template/lms"/>
<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="join"/>
	<c:param name="type" value="mailC"/>
	<c:param name="stepAt" value="N"/>
	<c:param name="areaAt" value="N"/>
</c:import>
	<div class="area">
      <article class="login-wrap">
        <div class="login-btn-wrap mt-60">
          <a href="/index.do" class="login-btn btn-full btn-point">확인</a>
        </div>
      </article>
    </div><!-- area-->

<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>
