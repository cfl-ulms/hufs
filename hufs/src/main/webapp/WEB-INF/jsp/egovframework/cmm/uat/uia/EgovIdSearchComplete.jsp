<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="idSearch"/>
	<c:param name="type" value="after"/>
</c:import>

	<script type="text/javascript">

		function fnShowId() {
			$('#idLabel').text('<c:out value="${searchVO.id}"/>');
		}
	</script>

	<article class="login-wrap">
		<div class="login-find-content">
			<p class="desc">
				<b>${searchVO.name}</b>님의 아이디는 <span class="font-blue">${serachVO.email }</span> 입니다
			</p>
		</div>
		<div class="login-btn-wrap">
			<a href="/uat/uia/egovLoginUsr.do" class="login-btn btn-full btn-point">로그인</a> 
			<a href="/index.do" class="login-btn btn-full btn-outline-light-gray">메인으로</a>
		</div>
	</article>
	<!-- area-->
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>