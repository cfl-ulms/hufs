<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<ul id="box_images">
<c:forEach var="fileVO" items="${fileList}" varStatus="status">
	<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
		<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
		<c:param name="fileSn" value="${fileVO.fileSn}"/>
		<c:param name="returnUrl" value="${CURR_URL}"/>
	</c:url>
	<li>
		<img src='<c:url value='/cmm/fms/getImage.do'/>?siteId=<c:out value="${param.siteId}"/>&amp;appendPath=<c:out value="${param.bbsId}"/>&amp;atchFileNm=${fileVO.streFileNm}.${fileVO.fileExtsn}' <c:if test="${not empty param.width}">width="${param.width}px"</c:if> <c:if test="${not empty param.height}">height="${param.height}px"</c:if>/>
		<c:if test="${param.mngAt eq 'Y'}">
			<a href="<c:out value="${delUrl}"/>">
				<img src="${CMMN_IMG }/btn_sdelete.gif"/>
			</a>
		</c:if>
	</li>
</c:forEach>
</ul>