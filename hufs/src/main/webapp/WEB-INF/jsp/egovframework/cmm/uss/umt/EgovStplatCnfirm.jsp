<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>
<c:choose>
<c:when test="${param.popupAt eq 'Y'}">
<c:set var="CML" value="/template/lms"/>
<c:set var="CM" value="/template/common"/>

<!DOCTYPE html>

<html lang="ko">

<head>
  <!--=================================================
            메타 태그
  ==================================================-->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>약관 및 개인정보취급방침 동의 안내</title>

  <meta name="title" content="약관 및 개인정보취급방침 동의 안내">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <meta property="og:type" content="website">
  <meta property="og:title" content="약관 및 개인정보취급방침 동의 안내">
  <meta property="og:description" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">

  <!--=================================================
        파비콘
  ==================================================-->
  <link rel="shortcut icon" href="">

  <!--=================================================
        공통 스타일시트
  ==================================================-->
  <link href="${CML}/font/font.css" rel="stylesheet"><!-- 나눔스퀘어 -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet"> <!-- Poppins -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="${CML}/lib/slick/slick.css"><!-- slick -->
  <link rel="stylesheet" href="${CML}/lib/jquery_ui/jquery-ui.css">
  <link rel="stylesheet" href="${CML}/lib/daterangepicker/daterangepicker.css">
  <!--daterangepicker -->

  <link rel="stylesheet" href="${CML}/css/common/base.css?v=2">
  <link rel="stylesheet" href="${CML}/css/common/common.css?v=1">
  <link rel="stylesheet" href="${CML}/css/common/board.css?v=1">

  <!--=================================================
          공통 스크립트
  ==================================================-->
  <script src="${CML}/lib/jquery-3.3.1/jquery-3.3.1.min.js"></script><!-- 제이쿼리 -->
  <script src="${CML}/lib/select2/select2.min.js"></script><!-- select2 -->
  <script src="${CML}/lib/slick/slick.js"></script><!-- slick -->
  <script src="${CML}/lib/slick/slick.min.js"></script><!-- slick -->
  <script src="${CML}/lib/jquery_ui/jquery-ui.js"></script>
  <script src="${CML}/lib/froala_editor/froala_editor.pkgd.min.js"></script><!-- froala_editor -->
  <script src="${CML}/lib/dotdotdot/jquery.dotdotdot.min.js"></script><!-- dotdotdot(말줄임) -->
  <script src="${CML}/lib/daterangepicker/daterangepicker.js"></script>
  <!--daterangepicker -->

  <script src="${CML}/js/common.js?v=1"></script>
  <script src="${CM}/js/common.js"></script>
  
  <!--=================================================
        페이지별 스타일시트, 스크립트
  ==================================================-->
	<link rel="stylesheet" href="${CML}/css/join/join.css?v=2">
	<c:if test="${param.step eq '1'}"><script src="${CML}/js/join/join_step01.js?v=1"></script></c:if>
	<c:if test="${param.step eq '2'}"><script src="${CML}/js/join/join_step02.js?v=1"></script></c:if>
  <link rel="stylesheet" href="${CML}/css/common/table.css?v=2">
  <style>
  .join-inner-wrap .aco-content .text{height:auto;}
  </style>
</head>
<body>

</c:when>
<c:otherwise>
	<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
		<c:param name="step" value="1"/>
		<c:param name="menu" value="join"/>
	</c:import>
</c:otherwise>
</c:choose>

<script>
$(document).ready(function(){
	$("#btn_agr").click(function(){
		if (!$('input[id=agree01]:checked').is(':checked')) {
			alert('특수외국어교육진흥사업 약관에 동의하지 않으셨습니다.');
		     return false;
		} 
		if (!$('input[id=agree02]:checked').is(':checked')) {
			alert('특수외국어교육진흥사업 개인정보취급방침에 동의하지 않으셨습니다.');
		     return false;
		}
		
		return;
	});
	
	//인쇄하기
	<c:if test="${param.popupAt eq 'Y'}">
		window.print();
	</c:if>
});
</script>


	<div class="join-contents-wrap">
		<c:choose>
			<c:when test="${param.popupAt eq 'Y'}">
				<!-- <a href="#" id="btn-print" class="btn-xl btn-outline-gray" style="float:right;">인쇄하기</a> -->
				<div class="title" style="padding:20px 0;text-align:center;">교육과정 수강신청 시 약관 및 개인정보취급방침 동의서</div>
			</c:when>
			<c:otherwise>
				<div class="title"></div>
			</c:otherwise>
		</c:choose>
        <ul class="join-inner-wrap">
          <li class="innerList">
            <div class="aco-head">
              <label class="checkbox">
                <input type="checkbox" id="agree01" class="infoCheck" >
                <span class="custom-checked"></span>
                <span class="text">특수외국어교육진흥사업 약관동의(필수)</span>
              </label>
              <div class="aco-arrow acoArrow open"></div>
            </div>
            <div class="aco-content" style="display:block;">
            	<c:catch var="ex">
					<c:import url="/EgovPageLink.do?link=${UseStplatUrl}" charEncoding="utf-8" var="html"/>
					<c:out value="${html}" escapeXml="false"/>	
				</c:catch>
				<c:if test="${ex != null}">publishing not found</c:if>
            </div>
          </li>
          <li class="innerList">
            <div class="aco-head">
              <label class="checkbox">
                <input type="checkbox" id="agree02" class="infoCheck">
                <span class="custom-checked"></span>
                <span class="text">특수외국어교육진흥사업 개인정보취급방침 (필수)</span>
              </label>
              <div class="aco-arrow acoArrow open"></div>
            </div>
            <div class="aco-content" style="display:block;">
              	<c:catch var="ex">
					<c:import url="/EgovPageLink.do?link=${IndvdlInfoPolicyUrl}&joinAt=Y" charEncoding="utf-8" var="html"/>
					<c:out value="${html}" escapeXml="false"/>	
				</c:catch>
				<c:if test="${ex != null}">publishing not found</c:if>
            </div>
          </li>
        </ul>
        <c:if test="${param.popupAt ne 'Y'}">
	        <div class="agree-check">
	          <label class="checkbox">
	            <input type="checkbox" class="allAgree">
	            <span class="custom-checked"></span>
	            <span class="text">사이트 이용을 위한 전체 약관에 모두 동의합니다.</span>
	          </label>
	        </div>
	        <div class="center-align mt-50">
	          <a href="/index.do" class="btn-xl btn-outline-gray">비동의</a>
	          <a href="/uss/umt/user/EgovUserInsertView.do" id="btn_agr" class="btn-xl btn-point">동의</a>
	        </div>
        </c:if>
      </div>			
<c:choose>
<c:when test="${param.popupAt eq 'Y'}">
</body>
</html>
</c:when>
<c:otherwise>
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>
</c:otherwise>
</c:choose>
