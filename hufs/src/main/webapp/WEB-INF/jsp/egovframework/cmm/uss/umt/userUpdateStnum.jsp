<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="CML" value="/template/lms"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="step" value="2"/>
	<c:param name="menu" value="join"/>
</c:import>

<script>
$(document).ready(function(){
	$("#stulist tr").click(function(){
	//$("#stulist tr").each(function(){ 한 방에 처리할려고 하니 에러나네...
		var info = $(this).data("info"),
			userNm = info.userNm,
			userId = info.userId,
			stNumber = info.stNumber,
			stMajor = info.major,
			stGrade = info.stGrade,
			tr = $(this);
		
		$.ajax({
			url : "/crypto/ariaEn.do",
			dataType : "json",
			type : "POST",
			data : {data : stNumber},
			success : function(data){
				$.ajax({
			  		url:encodeURI("${authUri}?service_id=${serviceId}&sfl_colreg_no_enc="+data.data),
			  		contentType: "application/javascript",
			  		dataType: 'jsonp',
			  		type: "POST",
			  		jsonp: "callback",
			  		async: false,
			  		success: function(data){
			   			console.log(data);
			   			if( data[0].SuccessFg == 'N' ){
			    			alert("인증과정오류 : "+data[0].rtnToken);
			   			}else{
			   				var grade = data[0].grade,
			   					major = data[0].major;
			   				
			   				$.ajax({
			   					url : "/uss/umt/user/updateStuFUser.json",
			   					dataType : "json",
			   					type : "POST",
			   					data : {userId : userId, major : data[0].major, stGrade : data[0].grade},
			   					success : function(data){
			   						if(data.updateCnt == 1){
			   							tr.css("color","blue");
			   							tr.find(".grade").text(grade);
			   							if(stGrade != grade){
			   								tr.find(".grade").css("color", "red");
			   							}
			   							
			   							tr.find(".major").text(major);
			   							if(stMajor != major){
			   								tr.find(".major").css("color", "red");
			   							}
			   							tr.find(".result").text("Y");
			   							tr.next().click();
			   						}
			   					}, error:function(jqxhr, status, error){
						  			alert("update error");
						  		}
			   				});
			   			}
			  		}, error:function(jqxhr, status, error){
			  			alert("view error");
			  		}
			 	});
			}, error : function(){
				alert("error");
			}
		});
	});
});

</script>
<style>
td, th{border:1px solid #000;height:30px;}
</style>
<table>
	<colgroup>
		<col width="25%"/>
		<col width="30%"/>
		<col width="5%"/>
		<col width="20%"/>
		<col width="15%"/>
		<col width="5%"/>
	</colgroup>
	<thead>
		<tr>
			<th>이름</th>
			<th>아이디</th>
			<th>학년</th>
			<th>전공</th>
			<th>학번</th>
			<th>업데이트 여부</th>
		</tr>
	</thead>
	<tbody id="stulist">
		<c:forEach var="result" items="${resultList}" varStatus="status">
			<tr data-info='{"userNm":"${result.userNm}", "userId":"${result.userId}", "stNumber":"${result.stNumber}", "stGrade":"${result.stGrade}", "major":"${result.major}"}'>
				<td><c:out value="${result.userNm}"/></td>
				<td><c:out value="${result.userId}"/></td>
				<td class="grade"><c:out value="${result.stGrade}"/></td>
				<td class="major"><c:out value="${result.major}"/></td>
				<td><c:out value="${result.stNumber}"/></td>
				<td class="result">N</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>