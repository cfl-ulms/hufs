<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="idSearch"/>
	<c:param name="type" value="before"/>
</c:import>

	<script type="text/javascript">

		<c:if test="${not empty message}">
			alert("<c:out value="${message}"/>");
		</c:if>

		function fnSearchId(form) {
			
			if ($('#email').val() =="") {
				alert("이메일을 입력하세요.");
				$("#email").focus();
				return false;
			}
			
			if ($('#name').val() =="") {
				alert("이름을 입력하세요.");
				$("#name").focus();
				return false;
			}
		}
	</script>
	
	<article class="login-wrap">
		<form id="idSearch" name="idSearch" action="/uat/uia/egovIdSearch.do" method="post">
			<fieldset class="confirm-form">
				<legend class="text-hide">본인확인</h3></legend>
				<input type="email" id="email" name="email" class="form-input" placeholder="본인확인을 위해 등록한 이메일 주소를 입력해 주세요" autocomplete="username email" required>
				<input type="text"  id="name"  name="name"  class="form-input" placeholder="이름을 입력해주세요" required>
				<input type="hidden" name="findSe" value="emart" />
				
				<div class="login-btn-wrap">
					<button type="submit" class="login-btn btn-full btn-point" onclick="fnSearchId();">아이디 찾기</button>
					<a href="/index.do" class="login-btn btn-full btn-outline-light-gray">메인으로</a>
				</div>
			</fieldset>
		</form>
	</article>
	<!-- footer -->
			
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>