<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="mypage" value="Y"/>
</c:import>

  <link rel="stylesheet" href="/template/lms/css/common/table.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/common/modal.css?v=2">
  <link rel="stylesheet" href="/template/lms/css/my/my.css?v=2">
  
<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="userManageVO" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript">
	$(document).ready(function(){
		//alert($(".chgPassword").data("modal-text"));
		
		$('.chgPassword').click(function(){ // 비밀번호 변경 확인 버튼 클릭
			var org_password = $('#password').val();
			var chg_password = $('#password2').val();
			var chk_chg_password = $('#chk_password2').val();
			
			if(chg_password === chk_chg_password){ // 새로운 비밀번호 입력이 동일하다면
				$.ajax({
					type:"post",
					dataType:"json",
					url:"/uss/umt/cmm/changePassword.json",
					data:"password="+org_password+"&password2="+chg_password,
					success: function(data) {
						console.log(data.successYn);
						if(data.successYn == "Y"){
							$("#openModal").hide();
							$("#confirmModal").show();
						}else{
							$("#confirmModal").hide();
							$("#openModal").show();
						}
						$(".modal-text").text(data.msg);
					},
					error:function() {
						//alert("error");
					}
				});
			}else{
				$(".modal-text").text("새로운 비밀번호 입력이 동일하지 않습니다.");
				$("#confirmModal").hide();
				$("#openModal").show();
			}
		});
		
		
		$('.btnLeave').click(function(){
			if( !($('#chk_agree1').is(':checked')&& $('#chk_agree2').is(':checked')) ){
				$(".modal-text").text("두 개의 체크박스에 모두 체크해주세요.");
				$("#openLeaveModal").show();
			}else{
				$.ajax({
					type:"post",
					dataType:"json",
					url:"/uss/umt/cmm/leaveUser.json",
					data:"",
					success: function(data) {
						$(".modal-text").text("탈퇴처리되었습니다.");
						$("#openLeaveModal").hide();
						$("#leaveOut").show();
					},
					error:function() {
						//alert("error");
					}
				});
			}
		});
	});
	
<c:if test="${result}">
	alert("<c:out value="${message}"/>");
</c:if>

	</script>
          <!-- 콘텐츠바디 -->
          <section class="page-content-body">
            <article class="content-wrap">
              <table class="common-table-wrap table-style2 ">
                <tbody>
                  <tr>
                    <th class="title vertical-mid">프로필 사진 </th>
                    <td>
                      <div class="profile-wrap flex-row">
                        <div class="flex-col-auto">
						  <c:choose>
								<c:when test="${empty userManageVO.photoStreFileNm}"><div class="img-frame" style="background-image: url(/template/lms/imgs/common/icon_user_name.svg);"></div></c:when>
								<c:otherwise>
								<div class="img-frame" style="background-image: url(${MembersFileStoreWebPath}<c:out value="${userManageVO.photoStreFileNm}"/>);"></div>
								</c:otherwise>
							</c:choose>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">소속 </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:choose>
								<c:when test="${userManageVO.userSeCode eq '02'}"><!-- 일반 -->
									<c:out value="${userManageVO.groupCode }"/>
								</c:when>
								<c:when test="${userManageVO.userSeCode eq '04'}"><!-- 일반학생(기타대학) -->
									<c:out value="${userManageVO.groupCode }(${userManageVO.major})"/>
								</c:when>
								<c:when test="${userManageVO.userSeCode eq '06'}"><!-- 외대학생 -->
									<c:out value="한국외국어대학교"/>
								</c:when>
							</c:choose>
						  </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">이름 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:out value="${userManageVO.userNm }"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">생년월일 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:out value="${fn:substring(userManageVO.brthdy, 0, 4)}"/>-<c:out value="${fn:substring(userManageVO.brthdy, 4, 6)}"/>-<c:out value="${fn:substring(userManageVO.brthdy, 6, 8)}"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">성별 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:out value="${userManageVO.sexdstn eq 'M' ? '남' : '여' }"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">휴대전화 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <div class="form-data">(+${userManageVO.geocode }) ${fn:substring(userManageVO.moblphonNo, 0, 2) }-${fn:substring(userManageVO.moblphonNo, 2, 6) }-${fn:substring(userManageVO.moblphonNo, 6, 10) }</div>
						  </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">아이디 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            ${userManageVO.userId }
							<c:if test="${userManageVO.confmAt ne 'Y'}"><span class='font-point ml-5'>(이메일 승인 대기중)</span></c:if> 
						  </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">본인확인 이메일 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            ${userManageVO.emailAdres } </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">비밀번호 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-6">
                          <button class="btn-sm btn-outline-gray btnModalOpen" data-modal-type="pwchange">비밀번호 변경</button>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="right-align">
                <button href="#" class="btn-leave btnModalOpen" data-modal-type="leave_id">회원탈퇴</button>
              </div>
            </article>
            <div class="page-btn-wrap mt-50">
              <a href="/uss/umt/cmm/EgovUserUpdateForm.do?menuId=MNU_0000000000000044" class="btn-xl btn-point">수정</a>
            </div>
          </section>
         </div></div></div></div>

<div id="alert_modal" class="alert-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <p class="modal-text"></p>
        <p class="modal-subtext"></p>
      </div>
      <div class="modal-footer">
      	<button type="button" id="confirmModal" style="display:none;" class="btn-xl btn-point btnModalConfirm">확인</button>
      	<button type="button" id="openModal" style="display:none;" class="btn-xl btn-point btnModalOpen" data-modal-type="pwchange">확인</button>
      	<button type="button" id="leaveOut" style="display:none;" class="btn-xl btn-point btnModalConfirm" onclick="location.href='/uat/uia/actionLogout.do'">확인</button>
      	<button type="button" id="openLeaveModal" style="display:none;" class="btn-xl btn-point btnModalOpen" data-modal-type="leave_id">확인</button>
      </div>
    </div>
  </div>
</div>
<div id="pwchange_modal" class="alert-modal">
  <div class="modal-dialog modal-top">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">비밀번호 변경</h4>
        <button type="button" class="btn-modal-close btnModalClose"></button>
      </div>
      <div class="modal-body">
        <div class="modal-text">새로운 비밀번호를 등록해주세요.</div>
        <div class="pwchange-form-wrap">
	       <input type="text" id="password" placeholder="현재 비밀번호 입력"/>
	       <input type="text" id="password2" placeholder="새로운 비밀번호 입력"/>
	       <input type="text" id="chk_password2" placeholder="새로운 비밀번호 확인"/>
	    </div>
        <p class="pwchange-desc mt-15">* 비밀번호에 영어 대/소문자, 숫자, 특수문자를 조합하시면 비밀번호 안전도가 높아져 도용의 위험이 줄어듭니다.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
        <button class="btn-xl btn-point btnModalOpen chgPassword" data-modal-type="alert" data-modal-size="modal-sm" data-modal-header="비밀번호 변경" data-modal-text="">확인</button>
      </div>
    </div>
  </div>
</div>
<div id="leave_id_modal" class="alert-modal">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title">회원탈퇴</h4>
         <button type="button" class="btn-modal-close btnModalClose"></button>
       </div>
       <div class="modal-body">
         <div class="modal-text">회원탈퇴를 진행하시겠습니까?</div>
         <div class="modal-subtext">
           회원을 탈퇴하실 경우 지금까지 진행하신 과정의 성적 이수 내역 및<br>
           특수외국어진흥사업 LMS 서비스 전체 이용내역이 모두 삭제됩니다.
         </div>
         <ul class="term-wrap mt-20">
           <li class="innerList">
             <div class="term-head">
               <label class="checkbox">
                 <input type="checkbox" id="chk_agree1" class="info Check">
                 <span class="custom-checked"></span>
                 <span class="text">네 탈퇴합니다.</span>
               </label>
             </div>
           </li>
           <hr class="division-line">
           <li class="innerList">
             <div class="term-head">
               <label class="checkbox">
                 <input type="checkbox" id="chk_agree2" class="infoCheck">
                 <span class="custom-checked"></span>
                 <span class="text">탈퇴안내를 확인했으며, 취소불가 및 안내사항에 동의합니다.</span>
               </label>
             </div>
           </li>
         </ul>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn-xl btn-outline-gray btnModalCancel">취소</button>
         <!-- <button type="button" class="btn-xl btn-point btnModalConfirm btnLeave">확인</button> -->
         <button class="btn-xl btn-point btnModalOpen btnLeave" data-modal-type="alert" data-modal-size="modal-sm" data-modal-header="회원탈퇴" data-modal-text="">탈퇴합니다</button>
       </div>
     </div>
   </div>
 </div>

<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>