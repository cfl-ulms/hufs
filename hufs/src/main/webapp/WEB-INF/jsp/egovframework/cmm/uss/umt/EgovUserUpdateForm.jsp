<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>

<c:set var="USER_INFO" value="<%=EgovUserDetailsHelper.getAuthenticatedUser(request, response) %>" />
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<link rel="stylesheet" href="/template/lms/css/common/table.css?v=2">

<c:import url="/msi/tmplatHead.do" charEncoding="utf-8">
	<c:param name="mypage" value="Y"/>
</c:import>

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<validator:javascript formName="userManageVO" staticJavascript="false" xhtml="true" cdata="false"/>

<script>
function inputDirectEmailDns(val){
 	document.getElementById('email2').value = val;
}

function inputDirectTxtGroup(val){
 	document.getElementById('showTxtGroup').value = val;
}

function inputDirectTxtMajor(val){
 	document.getElementById('showTxtMajor').value = val;
}

$(document).ready(function(){
	$('.btnSave').click(function(){
		$(".required").each(function(){
			if(!$(this).val()){
				alert($(this).attr("title") + "을 입력(선택)해 주세요.");
				retVal = false;
				return false;
			}
		});
		if($('#password').val() == "" || $('#password').val() == null){
			alert("비밀번호 확인을 입력해 주세요.");
			$('#password').focus();
			return false;
		}else{
			// 비밀번호 일치 확인
			$.ajax({
				type:"post",
				url:"/uss/umt/cmm/chkUserPassword.json",
				data:"password="+$('#password').val(),
				success: function(data) {
					if(data.equalYn == "N"){
						alert("비밀번호를 확인해주세요.");
						return false;
					}else{
						$("#member").submit();
					}
				},
				error:function() {
					alert("error");
				}
			});
		}
	});
	
	$('#btn-upload').click(function(e){
		e.preventDefault();
		$('#file').click();
	});
});

function fnShowModify(userSeCode){
	$('#showGroup').hide();
	$('#btnShowModify').hide();
	if(userSeCode == '02'){ // 일반
		$('#showTxtGroup').show();
		$('#btnGroupModify').show();
	}else if(userSeCode == '04'){ // 기타대
		$('#showGroupAll').show();
	}

}

function fnGroupModify02(){
	$.ajax({
		type:"post",
		url:"/uss/umt/cmm/changeGroup.json",
		data:"groupCode="+$('#showTxtGroup').val(),
		success: function(data) {
			alert("수정하였습니다.");
			$('#showGroup').show();
			$('#showTxtGroup').hide();
			$('#btnShowModify').show();
			$('#btnGroupModify').hide();
			$('#showGroup').text($('#showTxtGroup').val());
		},
		error:function() {
			alert("error");
		}
	});
}

function fnGroupModify04(){
	$.ajax({
		type:"post",
		url:"/uss/umt/cmm/changeGroup.json",
		data:"groupCode="+$('#showTxtGroup').val()+"&major="+$('#showTxtMajor').val(),
		success: function(data) {
			alert("수정하였습니다.");
			$('#showGroup').show();
			$('#showGroupAll').hide();
			$('#btnShowModify').show();
			$('#showGroup').text($('#showTxtGroup').val() + "(" + $('#showTxtMajor').val() + ")");
		},
		error:function() {
			alert("error");
		}
	});
}

function fnUpdateImgPath(){
	$('#file').val("");
	$('.img-frame').attr("style", "background-image: url(/template/lms/imgs/common/icon_user_name.svg)")
}
</script>

<style type="text/css">
#file{display:none;}
</style>

<!-- 콘텐츠바디 -->
<form action="<c:url value='/uss/umt/cmm/EgovUserUpdate.do'/>" id="member" name="member" method="post" enctype="multipart/form-data">
	
          <section class="page-content-body">
            <article class="content-wrap">
              <table class="common-table-wrap table-style2 ">
                <tbody>
                  <tr>
                    <th class="title vertical-mid">프로필 사진 </th>
                    <td>
                      <div class="profile-wrap flex-row">
                        <div class="flex-col-auto">
							<input type="hidden" id="imgPath" value=""/>
							  <c:choose>
								<c:when test="${empty userManageVO.photoStreFileNm}">
									<c:set var="imgPath" value="/template/lms/imgs/common/icon_user_name.svg"/>
								</c:when>
								<c:otherwise>
									<c:set var="imgPath" value="${MembersFileStoreWebPath}${userManageVO.photoStreFileNm}"/>
								</c:otherwise>
							</c:choose>
							<div class='img-frame' style='background-image: url(${imgPath});'></div>
                        </div>
                        <div class="flex-col-auto">
							<input type="file" id="file" name="userPicFile" onchange="changeValue(this)"/>
                          <button class="btn-sm-90 btn-outline-gray" id="btn-upload" type="button">사진 변경</button>
                        </div>
                        <div class="flex-col-auto">
                          <button class="btn-sm-90 btn-outline-gray" onclick="javascript:fnUpdateImgPath();" type="button">사진 삭제</button>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">소속 </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:choose>
								<c:when test="${userManageVO.userSeCode eq '02'}"><!-- 일반 -->
									<span id="showGroup"><c:out value="${userManageVO.groupCode }"/></span>
									<input type="text" id="showTxtGroup" value="${userManageVO.groupCode }" style="display:none;width:300px;"/>
									<button class="btn-sm-90 btn-outline-gray ml-15" id="btnGroupModify" onclick="javascript:fnGroupModify02();" style="display:none;">완료</button>
								</c:when>
								<c:when test="${userManageVO.userSeCode eq '04'}"><!-- 일반학생(기타대학) -->
									<span id="showGroup"><c:out value="${userManageVO.groupCode }(${userManageVO.major})"/></span>
								</c:when>
								<c:when test="${userManageVO.userSeCode eq '06'}"><!-- 외대학생 -->
									<c:out value="한국외국어대학교"/>(<c:out value="${userManageVO.major}"/>)
								</c:when>
							</c:choose>
                        </div>
                        <div class="flex-col-auto">
						  <c:if test="${userManageVO.userSeCode ne '06'}">
								<button class="btn-sm-90 btn-outline-gray ml-15" id="btnShowModify" onclick="javascript:fnShowModify('${userManageVO.userSeCode}');">수정</button>
							</c:if> 
                        </div>
                      </div>
					  <c:if test="${userManageVO.userSeCode eq '04'}">
						<span id="showGroupAll" style="display:none;">
							<div class="form-group">
		                      <input type="text" id="showTxtGroup" value="${userManageVO.groupCode}">
		                      <select class="select2" data-select="style1" onchange='inputDirectTxtGroup(this.value);'>
			                      <c:forEach var="result" items="${schoolList}" varStatus="status">
				              		<option value="${result.codeNm}" <c:if test="${userManageVO.groupCode eq result.codeNm}"> selected="selected"</c:if>>${result.codeNm}</option>
				              	  </c:forEach>
			              	  </select>
		                    </div>
		                    <div class="form-group has-btn">
		                      <input type="text" id="showTxtMajor" value="${userManageVO.major}">
		                      <select class="select2" data-select="style1" onchange='inputDirectTxtMajor(this.value);'>
			                      <c:forEach var="result" items="${languageList}" varStatus="status">
					          		<c:if test="${not empty result.upperCtgryId}">
					          			<option value="${result.ctgryNm}" <c:if test="${userManageVO.major eq result.ctgryNm}"> selected="selected"</c:if>>${result.ctgryNm}</option>
					            	</c:if>
								  </c:forEach>
							  </select>
		                      <button class="btn-sm-90 btn-outline-gray ml-10" onclick="javascript:fnGroupModify04();">완료</button>
		                    </div>
		                    <p class="mypage-form-desc">소속명은 특수외국어 과정 수료 시 수료증에 기재되는 정보로 정확히 입력해주세요.</p>
		                </span>
	                    </c:if>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">이름 </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:out value="${userManageVO.userNm }"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">생년월일 </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                           <c:out value="${fn:substring(userManageVO.brthdy, 0, 4)}"/>-<c:out value="${fn:substring(userManageVO.brthdy, 4, 6)}"/>-<c:out value="${fn:substring(userManageVO.brthdy, 6, 8)}"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">성별 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            <c:out value="${userManageVO.sexdstn eq 'M' ? '남' : '여' }"/> </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">휴대전화 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row items-start">
                        <div class="flex-col-4">
						  <select class="table-select select2" name="geocode" data-select="style1">
								<option value="82">대한민국 +82</option>
								<c:forEach var="result" items="${telNumList}" varStatus="status">
									<c:if test="${result.code ne '82'}">
										<option value="${result.code}">${result.codeNm} +${result.code}</option>
									</c:if>
								</c:forEach>
						  </select>
                        </div>
                        <div class="flex-col-4">
						  <input type="text" required="required" class="table-input onlyNum required" name="moblphonNo" value="${userManageVO.moblphonNo}" title="휴대전화번호">
                        </div>
                      </div>
                      <div class="flex-row">
                        <div class="flex-col-8 ml-auto">
                          <p class="table-guide"><span class="right-area">(-)를 제외하고 번호만 입력해주세요.</span></p>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">아이디 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            ${userManageVO.userId } <c:if test="${userManageVO.confmAt ne 'Y'}"><span class='font-point ml-5'>(이메일 승인 대기중)</span></c:if>  </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">본인확인 이메일 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-auto">
                          <div class="table-data">
                            ${userManageVO.emailAdres } </div>
                        </div>
                      </div>
					  <c:set var="tmpMail" value="${fn:split(userManageVO.emailAdres, '@') }"/>
                      <div class="flex-row flex-nowrap">
                        <div class="flex-ten-col-3">
                          <input type="text" class="table-input" id="email1" name="email1" value="${tmpMail[0]}" placeholder="">
                        </div>
                        <div class="flex-col-auto">
                          <div class="table-data">
                            @ </div>
                        </div>
                        <div class="flex-ten-col-3">
                          <input type="text" class="table-input" id="email2" name="email2" type="text" value="${tmpMail[1]}" placeholder="">
                        </div>
                        <div class="flex-ten-col-3">
                          <select class="table-select select2" data-select="style1" onchange='inputDirectEmailDns(this.value);'>
	                        <option value="">기타(직접입력)</option>
			                <option value="naver.com"	<c:if test="${tmpMail[1] eq 'naver.com'}"> selected="selected"</c:if>>네이버(naver.com)</option>
			                <option value="hanmail.net"	<c:if test="${tmpMail[1] eq 'hanmail.net'}"> selected="selected"</c:if>>다음</option>
			                <option value="gmail.com"	<c:if test="${tmpMail[1] eq 'gmail.com'}"> selected="selected"</c:if>>G메일(gmail.com)</option>
							<option value="nate.com"	<c:if test="${tmpMail[1] eq 'nate.com'}"> selected="selected"</c:if>>네이트(nate.com)</option>
	                      </select>
                        </div>
                        <div class="flex-col-auto">
                          <button class="btn-sm-90 btn-outline-gray">완료</button>
                        </div>
                      </div>
                      <div class="flex-row">
                        <div class="flex-col-12">
                          <p class="table-guide">* 본인확인 이메일을 변경하시는 경우, 변경된 이메일 주소로 인증 메일이 재발송 됩니다.</p>
                          <p class="table-guide">* 재발송된 메일로 로그인하여 인증을 완료해야 로그인이 가능합니다.</p>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">비밀번호 확인 <i class="font-point font-400">*</i> </th>
                    <td>
                      <div class="flex-row">
                        <div class="flex-col-8">
                          <input type="text" class="table-input" class="required" required="required" id="password" name="password" placeholder="">
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </article>
            <div class="page-btn-wrap mt-50">
               <a href="/uss/umt/cmm/EgovUserUpdateView.do" class="btn-xl btn-outline-gray">취소</a>
               <a href="" class="btn-xl btn-point btnSave">저장</a>
            </div>
          </section>
</form>
 </div></div></div></div>
<c:import url="/msi/tmplatBottom.do" charEncoding="utf-8"/>