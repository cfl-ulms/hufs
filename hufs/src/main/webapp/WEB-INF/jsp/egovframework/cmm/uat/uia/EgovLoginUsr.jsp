<%@ page contentType="text/html; charset=utf-8"%>
<%@ page import="egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="IS_MOBILE"><%=egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)%></c:set>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:set var="type">
	<c:choose>
		<c:when test="${not empty param.type}"><c:out value="${param.type}"/></c:when>
		<c:otherwise>s</c:otherwise>
	</c:choose>
</c:set>
		
<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="login"/>
	<c:param name="type" value="${type}"/>
</c:import>

<script>
	/* 
	<c:if test='${not empty message}'>
		alert("${message}");
	</c:if>
	 */
	function checkGnrlLogin(frm) {
   		if(frm.user_id.value == "") {
			alert("아이디를  입력해주세요");
			frm.user_id.focus();
			return false;
		}

   		if(frm.password.value == "") {
			alert("비밀번호를  입력해주세요");
			frm.password.focus();
			return false;
		}
   		
   		//아이디 저장
   		var type = $("#btn_idsave").data("type");
   		if($("#btn_idsave").is(":checked")){
			fnSetCookieValue("id_" + type, $("#user_id").val());
		}else{
			fnSetCookieValue("id_" + type, "");
		}
	}
	
	$(document).ready(function(){
		if(fnGetCookie("id_${type}")){
			$("#user_id").val(fnGetCookie("id_${type}"));
			$("#btn_idsave").prop("checked",true);
		}
	});
	
</script>
	
	<article class="login-wrap">
        <form action="<c:url value='/uat/uia/actionLogin.do'/>" name="frmGnrlLogin" method="post" onsubmit="return checkGnrlLogin(this)">
          <fieldset class="login-form">
            <legend class="text-hide">로그인 정보</legend>
            <c:choose>
            	<c:when test="${param.type eq 't'}">
					<input type="text" id="user_id" name="id" class="form-input" placeholder="아이디" autocomplete="username" required>            		
            	</c:when>
            	<c:otherwise>
            		<input type="email" id="user_id" name="id" class="form-input" placeholder="아이디 (이메일 주소)" autocomplete="username email" required>
            	</c:otherwise>
            </c:choose>
            <input type="password" name="password" class="form-input" placeholder="비밀번호" autocomplete="new-password" required>
            <div class="login-help-wrap">
              <div class="login-check">
                <label class="checkbox">
                  <input type="checkbox" id="btn_idsave" data-type="${type}">
                  <span class="custom-checked"></span>
                  <span class="text">아이디 저장</span>
                </label>
              </div>
              <div class="login-find">
                <a href="<%=EgovUserDetailsHelper.getRedirectFindIdUrl()%>">아이디 찾기</a> /
                <a href="<%=EgovUserDetailsHelper.getRedirectFindPasswordUrl()%>">비밀번호 찾기</a>
              </div>
            </div>
            <div class="login-btn-wrap">
              <button type="submit" class="login-btn btn-full btn-point">로그인</button>
              <a href="<%=EgovUserDetailsHelper.getRedirectRegistUrl()%>" class="login-btn btn-full btn-outline-light-gray">회원가입</a>
              <c:choose>
            	<c:when test="${param.type eq 't'}">
					<a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>" class="login-btn btn-link">Student Login</a>
            	</c:when>
            	<c:otherwise>
            		<a href="<%=EgovUserDetailsHelper.getRedirectLoginUrl()%>?type=t" class="login-btn btn-link">Teacher & Staff Login</a>
            	</c:otherwise>
	          </c:choose>
            </div>
          </fieldset>
        </form>
      </article>

<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>
