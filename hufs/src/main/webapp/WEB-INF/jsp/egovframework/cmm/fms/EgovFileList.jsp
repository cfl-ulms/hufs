<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
org.springframework.web.util.UrlPathHelper helper = new org.springframework.web.util.UrlPathHelper();
String currentUrl = helper.getOriginatingRequestUri(request) + ((helper.getOriginatingQueryString(request) != null) ? "?" + helper.getOriginatingQueryString(request) : "");
%>
<c:set var="CURR_URL" value="<%=currentUrl%>"/>
<c:set var="CMMN_IMG" value="/template/manage/images"/>

<c:set var="_FILE_CURR_COUNT" value="0"/>
<c:set var="_FILE_CURR_SIZE" value="0"/>

<c:choose>
		<c:when test="${param.report eq 'Y'}">
			<c:forEach var="fileVO" items="${fileList}" varStatus="status">
				<c:url var="downLoad" value="/cmm/fms/FileDown.do">
					<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
					<c:param name="fileSn" value="${fileVO.fileSn}"/>
					<c:choose>
						<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
						<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
						<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
						<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
					</c:choose>
					<c:choose>
						<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
					</c:choose>
				</c:url>
				<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;">
					<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
				</a>
	        </c:forEach>
		</c:when>

		<c:when test="${param.type eq 'mov'}">
			<c:set var="movCnt" value="0"/>
			<c:forEach var="fileVO" items="${fileList}" varStatus="status">
				<c:if test="${movCnt == 0 and fn:toLowerCase(fileVO.fileExtsn) eq 'mp4'}">
					<div>
						<c:import url="/multiview/main/popup/mediaPlayer.do" charEncoding="utf-8">
							<c:param name="upPath" value="${fn:replace(fileVO.fileStreCours, webPath, '')}/${fileVO.streFileNm}" />
							<c:param name="type" value="bbs"/>
						</c:import>
					</div>
					<%--
					<iframe src="/multiview/main/popup/mediaPlayer.do?upPath=${fn:replace(fileVO.fileStreCours, webPath, '')}/${fileVO.streFileNm}&type=bbs" style="width:100%;height:485px;overflow:auto;border:none;"></iframe>
					 --%>
					<c:set var="movCnt" value="1"/>
				</c:if>
	        </c:forEach>
		</c:when>

		<c:when test="${param.mngAt eq 'Y'}">
			<div class="file_box">
				<c:if test="${updateFlag=='Y'}">
					<div class="file_top">

						<input id="fileupload" class="fileupload <c:out value="${param.editorId}"/>" type="file" name="files" multiple style="display:none; opacity: 0; filter:alpha(opacity: 0);">
						<label class="file_btn" data-target="<c:out value="${param.editorId}"/>" id="btnAddFile_<c:out value="${param.editorId}"/>" for="fileupload"><img src="${CMMN_IMG }/btn_add_file.gif" alt="파일첨부"/></label>
						<p class="file_info">
							<strong class="blue">갯수 : </strong><span id="lblCurrCount_<c:out value="${param.editorId}"/>">0</span>/<span id="lblMaxCount_<c:out value="${param.editorId}"/>">0</span> ,
							<strong class="blue">크기 : </strong><span id="lblCurrSize_<c:out value="${param.editorId}"/>">0MB</span>/<span id="lblMaxSize_<c:out value="${param.editorId}"/>">0MB</span>
						</p>
					</div>
				</c:if>
				<div id="progress">
					<span class="progress-bar" style="width:0%"></span>
				</div>
				<table class="file_list_chart" summary="첨부파일 목록을 나타낸표입니다">
					<caption>첨부파일목록</caption>
					<colgroup>
							<col width="*" />
							<col width="150" />
							<col width="30" />
					</colgroup>
					<thead>
						<tr>
							<th>파일명</th>
							<c:choose>
								<c:when test="${updateFlag=='Y'}">
									<th class="size">크기</th>
									<th class="del">삭제</th>
								</c:when>
								<c:otherwise>
									<th class="size" colspan="2">크기</th>
								</c:otherwise>
							</c:choose>
						</tr>
					</thead>
					<tbody id="multiFileList_<c:out value="${param.editorId}"/>">
					<tr id="tr_file_empty_<c:out value="${param.editorId}"/>" <c:if test="${fn:length(fileList) ne '0'}">style="display:none"</c:if>>
						<c:choose>
							<c:when test="${updateFlag=='Y'}">
								<td colspan="3" align="center">첨부된 파일이 없습니다.</td>
							</c:when>
							<c:otherwise>
								<td colspan="2" align="center">첨부된 파일이 없습니다.</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<c:forEach var="fileVO" items="${fileList}" varStatus="status">
						<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
							<c:choose>
				  		       <c:when test="${updateFlag=='Y'}">
					  		       	<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
					  		       		<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
										<c:param name="fileSn" value="${fileVO.fileSn}"/>
										<c:param name="returnUrl" value="${CURR_URL}"/>
					  		       	</c:url>
									<td><img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
										<c:if test="${not empty fileVO.la and not empty fileVO.lo }">
											(<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.la }"/>,<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.lo }"/>)
										</c:if>
									</td>
									<td class="size"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
									<td class="del"><a href="<c:out value="${delUrl}"/>" onclick="fn_egov_editor_file_del('<c:out value="${param.editorId}"/>', '<c:out value="${param.estnAt}"/>', '<c:out value="${param.bbsId}"/>', '<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');return false;">
											<img src="${CMMN_IMG }/btn_sdelete.gif"/>
										</a>
									</td>
									<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
									<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
				  		       </c:when>
				  		       <c:otherwise>
				  			       <c:url var="downLoad" value="/cmm/fms/FileDown.do">
										<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
										<c:param name="fileSn" value="${fileVO.fileSn}"/>
										<c:choose>
											<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
											<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
											<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
											<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
										</c:choose>
										<c:choose>
											<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
										</c:choose>
				  			       </c:url>
									<td>
										<a href="<c:out value='${downLoad}'/>" onclick="fn_egov_downFile(this.href);return false;" class="test333">
											<img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
											<c:if test="${not empty fileVO.la and not empty fileVO.lo }">
												(<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.la }"/>,<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.lo }"/>)
											</c:if>
										</a>
									</td>
									<td class="size" colspan="2"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
				  		       </c:otherwise>
							</c:choose>
						</tr>
			        </c:forEach>
					</tbody>
				</table>
			</div>
		</c:when>
		
		<c:when test="${param.commonAt eq 'Y'}">
			<!-- 첨부파일 -->
			<input type="hidden" id="staffPage" value="common"/>
            <div class="file-attachment-wrap">
              <div class="file-attachment-write">
                <div class="clear">
                  <p class="title">
                    파일을 업로드 해주세요.
                    <span>(jpg, jpeg, png, docx, pptx, pdf)</span>
                  </p>
                  <input id="fileupload" class="fileupload <c:out value="${param.editorId}"/>" type="file" name="files" multiple style="display:none; opacity: 0; filter:alpha(opacity: 0);">
                  <label class="file_btn" data-target="<c:out value="${param.editorId}"/>" id="btnAddFile_<c:out value="${param.editorId}"/>" for="fileupload"><button class="btn-file-attach f-r">불러오기</button></label>
                </div>
                <div id="multiFileList_<c:out value="${param.editorId}"/>" class="inner-area">
                	<c:forEach var="fileVO" items="${fileList}" varStatus="status">
                		<c:choose>
               				<c:when test="${fn:toLowerCase(fileVO.fileExtsn) eq 'jpg' or fn:toLowerCase(fileVO.fileExtsn) eq 'jpeg' or fn:toLowerCase(fileVO.fileExtsn) eq 'png'}">
               					<c:set var="ext" value="img"/>
               				</c:when>
               				<c:otherwise>
               					<c:set var="ext" value="file"/>
               				</c:otherwise>
               			</c:choose>
               			<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
		  		       		<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
							<c:param name="fileSn" value="${fileVO.fileSn}"/>
							<c:param name="returnUrl" value="${CURR_URL}"/>
		  		       	</c:url>
					  		       	
                		<a href="${delUrl}" id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="attachment icon-${ext} font-gray" onclick="fn_egov_editor_file_del('<c:out value="${param.editorId}"/>', '<c:out value="${param.estnAt}"/>', '<c:out value="${param.bbsId}"/>', '<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');return false;">
	                    	<span class="text"><c:out value="${fileVO.orignlFileNm}"/></span>
	                    	<span class="file-remove">-</span>
	                  	</a>
			        </c:forEach>
                </div>
              </div>
            </div>
		</c:when>
		<c:when test="${param.commonViewAt eq 'Y'}">
			<div class="file-attachment-wrap test">
			  <div class="file-attachment-view">
			    <div class="inner-area">
			    	<c:forEach var="fileVO" items="${fileList}" varStatus="status">
	              		<c:choose>
            				<c:when test="${fn:toLowerCase(fileVO.fileExtsn) eq 'jpg' or fn:toLowerCase(fileVO.fileExtsn) eq 'jpeg' or fn:toLowerCase(fileVO.fileExtsn) eq 'png'}">
	             				<c:set var="ext" value="img"/>
	             			</c:when>
	             			<c:otherwise>
	             				<c:set var="ext" value="file"/>
	             			</c:otherwise>
	             		</c:choose>
	             		<c:url var="downLoad" value="/cmm/fms/FileDown.do">
						<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
							<c:param name="fileSn" value="${fileVO.fileSn}"/>
							<c:choose>
								<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
								<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
								<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
								<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
							</c:choose>
							<c:choose>
								<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
							</c:choose>
			          	</c:url>
				  		       	
	              		<a href="<c:out value='${downLoad}'/>" class="attachment icon-file font-gray test2" onclick="fn_egov_downFile(this.href);return false;">
				        	<span class="text"><c:out value="${fileVO.orignlFileNm}"/></span>
				      	</a>
		        	</c:forEach>
			    </div>
			  </div>
			</div>
		</c:when>
		<c:when test="${param.addYn eq 'Y'}">
	    	<c:forEach var="fileVO" items="${fileList}" varStatus="status">
             		<c:choose>
          				<c:when test="${fn:toLowerCase(fileVO.fileExtsn) eq 'jpg' or fn:toLowerCase(fileVO.fileExtsn) eq 'jpeg' or fn:toLowerCase(fileVO.fileExtsn) eq 'png'}">
            				<c:set var="ext" value="img"/>
            			</c:when>
            			<c:otherwise>
            				<c:set var="ext" value="file"/>
            			</c:otherwise>
            		</c:choose>
            		<c:url var="downLoad" value="/cmm/fms/FileDown.do">
				<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
					<c:param name="fileSn" value="${fileVO.fileSn}"/>
					<c:choose>
						<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
						<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
						<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
						<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
					</c:choose>
					<c:choose>
						<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
					</c:choose>
	          	</c:url>
		  		       	
             		<a href="<c:out value='${downLoad}'/>" class="attachment icon-file font-gray test2" onclick="fn_egov_downFile(this.href);return false;">
		        	<span class="text"><c:out value="${fileVO.orignlFileNm}"/></span>
		      	</a>
        	</c:forEach>
		</c:when>
		
		
		<c:otherwise>
			<c:forEach var="fileVO" items="${fileList}" varStatus="status">
				<tr id="<c:out value="${fileVO.atchFileId}_${fileVO.fileSn}"/>" class="db">
					<c:choose>
		  		       <c:when test="${updateFlag=='Y'}">
			  		       	<c:url var="delUrl" value='/cmm/fms/deleteFileInfs.do'>
			  		       		<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
								<c:param name="fileSn" value="${fileVO.fileSn}"/>
								<c:param name="returnUrl" value="${CURR_URL}"/>
			  		       	</c:url>
							<td><img src='${CMMN_IMG }/ico_file.gif' alt='파일'/> <c:out value="${fileVO.orignlFileNm}"/>
								<c:if test="${not empty fileVO.la and not empty fileVO.lo }">
									(<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.la }"/>,<fmt:formatNumber type="number" pattern="###.#####" value="${fileVO.lo }"/>)
								</c:if>
							</td>
							<td class="size"><c:out value="${fileVO.fileMgByByteConvert}"/></td>
							<td class="del"><a href="<c:out value="${delUrl}"/>" onclick="fn_egov_editor_file_del('<c:out value="${param.editorId}"/>', '<c:out value="${param.estnAt}"/>', '<c:out value="${param.bbsId}"/>', '<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');return false;">
									<img src="${CMMN_IMG }/btn_sdelete.gif"/>
								</a>
							</td>
							<c:set var="_FILE_CURR_COUNT" value="${_FILE_CURR_COUNT + 1}"/>
							<c:set var="_FILE_CURR_SIZE" value="${_FILE_CURR_SIZE + fileVO.fileMg}"/>
		  		       </c:when>
		  		       <c:otherwise>
		  			       <div class="file-attachment-wrap test">
							  <div class="file-attachment-view">
							    <div class="inner-area">
							     <c:url var="downLoad" value="/cmm/fms/FileDown.do">
										<c:param name="atchFileId" value="${fileVO.atchFileId}"/>
										<c:param name="fileSn" value="${fileVO.fileSn}"/>
										<c:choose>
											<c:when test="${not empty param.bbsId}"><c:param name="bbsId" value="${param.bbsId}"/></c:when>
											<c:otherwise><c:param name="bbsId" value="00000000000000000000"/></c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${not empty param.trgetId}"><c:param name="trgetId" value="${param.trgetId}"/></c:when>
											<c:when test="${not empty param.nttNo and not empty param.menuId}"><c:param name="trgetId" value="SYSTEM_DEFAULT_BOARD"/></c:when>
											<c:when test="${not empty param.nttNo and empty param.menuId}"><c:param name="trgetId" value="MMAMVP_SERVICE_BOARD"/></c:when>
										</c:choose>
										<c:choose>
											<c:when test="${not empty param.nttNo}"><c:param name="nttId" value="${param.nttNo}"/></c:when>
										</c:choose>
						          </c:url>
							      <a href="<c:out value='${downLoad}'/>" class="attachment icon-file font-gray test2" onclick="fn_egov_downFile(this.href);return false;">
							        <span class="text"><c:out value="${fileVO.orignlFileNm}"/></span>
							      </a>
							    </div>
							  </div>
							</div>
		  		       </c:otherwise>
					</c:choose>
				</tr>
	        </c:forEach>
	</c:otherwise>
</c:choose>

<input type="hidden" id="fileGroupId_<c:out value="${param.editorId}"/>" name="fileGroupId_<c:out value="${param.editorId}"/>" value="<c:out value="${param.param_atchFileId}"/>"/>
<input type="hidden" id="fileCurrCount_<c:out value="${param.editorId}"/>" name="fileCurrCount_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_COUNT}"/>"/>
<input type="hidden" id="fileCurrSize_<c:out value="${param.editorId}"/>" name="fileCurrSize_<c:out value="${param.editorId}"/>" value="<c:out value="${_FILE_CURR_SIZE}"/>"/>
