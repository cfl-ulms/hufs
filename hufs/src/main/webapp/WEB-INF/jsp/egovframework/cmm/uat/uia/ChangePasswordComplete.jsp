<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="pwComplete"/>
</c:import>
	
	      <article class="login-wrap">
	        <div class="login-btn-wrap mt-60">
	          <a href="/index.do" class="login-btn btn-full btn-point">확인</a>
	        </div>
	      </article>
	
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>