<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="pwSearch"/>
	<c:param name="type" value="after"/>
	<c:param name="email" value="${resultInfo.email}"/>
</c:import>
	
	<article class="login-wrap">
		<div class="login-btn-wrap">
			<a href="/uat/uia/egovLoginUsr.do" class="login-btn btn-full btn-point">로그인</a> 
			<a href="/index.do" class="login-btn btn-full btn-outline-light-gray">메인으로</a>
		</div>
	</article>
	
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>