<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="CML" value="/template/lms"/>
<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="step" value="3"/>
	<c:param name="menu" value="join"/>
</c:import>

		<div class="join-complete-wrap">
	        <img width="75" src="${CML}/imgs/join/icon_join_step03_on.svg" alt="회원가입이 완료되었습니다.">
	        <p class="title">회원가입이 완료되었습니다.<br>
		        <c:if test="${confmAt eq 'N'}">
			          가입승인 안내 메일을 확인해주세요.
		      	</c:if>
	        </p>
	      	<c:if test="${confmAt eq 'N'}">
	        	<p class="desc">
	        		본인확인 이메일 
	        		<span class="font-point">(<c:out value="${emailAdres}"/>)</span>로 가입신청 이메일을 전송하였습니다.<br>
	          		메일을 확인하신 후 <strong>가입승인</strong> 버튼을 체크하시면 최종 가입이 완료됩니다.
	        	</p>
	        </c:if>
	        <div class="center-align mt-50">
	          <a href="/index.do" class="btn-xl btn-point">확인</a>
	        </div>
	      </div>


<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>