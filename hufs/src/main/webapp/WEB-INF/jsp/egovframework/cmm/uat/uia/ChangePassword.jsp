<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://egovframework.gov/ctl/ui" prefix="ui" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="_IMG" value="${pageContext.request.contextPath}/template/member/images"/>

<c:import url="/msi/cmm/tmplatHead.do" charEncoding="utf-8">
	<c:param name="menu" value="pwSearch"/>
	<c:param name="type" value="mailC"/>
</c:import>
	
      <article class="login-wrap">
        <form id="pw" name="pw" action="/uat/uia/changePw.do" method="post">
          <fieldset class="login-form">
            <legend class="text-hide">본인확인</h3>
            </legend>
            <input type="hidden" name="url" value="${url}"/>
            <input type="password" name="password" class="form-input" placeholder="새로운 비밀번호를 입력해주세요" autocomplete="new-password" required>
            <input type="password" class="form-input" placeholder="비밀번호를 한 번 더 입력해주세요" autocomplete="new-password" required>
            <div class="login-btn-wrap">
              <button type="submit" class="login-btn btn-full btn-point">확인</button>
              <a href="/uat/uia/egovLoginUsr.do" class="login-btn btn-full btn-outline-light-gray">로그인</a>
            </div>
          </fieldset>
        </form>
      </article>
	
<c:import url="/msi/cmm/tmplatBottom.do" charEncoding="utf-8"/>