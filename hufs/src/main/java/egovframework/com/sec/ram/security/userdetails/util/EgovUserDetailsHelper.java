package egovframework.com.sec.ram.security.userdetails.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.cmm.service.Globals;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.string.EgovObjectUtil;
import egovframework.com.cmm.service.EgovUserDetailsService;

/**
 * EgovUserDetails Helper 클래스
 * 
 * @author sjyoon
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2009.03.10  sjyoon    최초 생성
 *
 * </pre>
 */

public class EgovUserDetailsHelper {
	
	static EgovUserDetailsService egovUserDetailsService;
	static EgovSiteManageService siteInfoService;
	static EgovLoginService loginService;
	
	public EgovUserDetailsService getEgovUserDetailsService() {
		return egovUserDetailsService;
	}

	public void setEgovUserDetailsService(EgovUserDetailsService egovUserDetailsService) {
		EgovUserDetailsHelper.egovUserDetailsService = egovUserDetailsService;
	}
	
	public void setSiteInfoService(EgovSiteManageService siteInfoService) {
		EgovUserDetailsHelper.siteInfoService = siteInfoService;
	}
	
	public void setLoginService(EgovLoginService loginService) {
		EgovUserDetailsHelper.loginService = loginService;
	}
	
	/**
	 * 인증된 사용자객체를 VO형식으로 가져온다.
	 * @return Object - 사용자 ValueObject
	 */
	public static LoginVO getAuthenticatedUser() {
		
		Object obj = egovUserDetailsService.getAuthenticatedUser();
		if(obj != null) {
			return (LoginVO)obj;
		}
		return null;
	}
	
	/**
	 * 인증된 사용자객체를 VO형식으로 가져온다.
	 * @return Object - 사용자 ValueObject
	 */
	public static LoginVO getAuthenticatedUser(HttpServletRequest request, HttpServletResponse response) {
		
		Object obj = egovUserDetailsService.getAuthenticatedUser();
		if(obj != null) {
			LoginVO loginVO = (LoginVO)obj;
			/*
			try {
				SiteManageVO siteVO = siteInfoService.selectSiteServiceInfo(request);
				String reqURL = request.getRequestURI();
				int SE_CODE = Integer.parseInt(loginVO.getUserSeCode());
				
				if(!reqURL.startsWith("/mng") && SE_CODE != 99 && reqURL.endsWith(".do") && siteVO != null && !"SITE_000000000000001".equals(siteVO.getSiteId()) && "Y".equals(siteVO.getCrtfcUseAt())) {
					boolean findFlag = false;
					if(loginVO.getSbscrbSite() != null) {
						for(int i=0; i<loginVO.getSbscrbSite().length; i++) {
							if(siteVO.getSiteId().equals(loginVO.getSbscrbSite()[i])) {
								findFlag = true;
								break;
							}
						}
					}
					
					if(!findFlag) {
						return null;
					}
				}
				
			} catch(Exception ex) {ex.printStackTrace();}
			*/
			return loginVO;
		}
		return null;
	}
	
	/**
	 * 인증된 사용자객체를 VO형식으로 가져온다.
	 * @return Object - 사용자 ValueObject
	 */
	/*
	public static LoginVO getAuthenticatedUser(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		
		if (EgovObjectUtil.isNull(authentication)) {
			return null;
		}
		
		String username = authentication.getName();
		if (username.equals("roleAnonymous")) {
			return null;
		}
		
		Object principal = authentication.getPrincipal();
		if(EgovObjectUtil.isNull(principal)) {
			return null;
		}
		
		EgovUserDetails details = (EgovUserDetails) principal;			
		
		return (LoginVO)details.getEgovUserVO();
		
	}
	*/
	
	/**
	 * 인증된 사용자의 권한 정보를 가져온다.
	 *
	 * @return List - 사용자 권한정보 목록
	 */
	public static List<String> getAuthorities() {
		return egovUserDetailsService.getAuthorities();
	}

	/**
	 * 인증된 사용자 여부를 체크한다.
	 * @return Boolean - 인증된 사용자 여부(TRUE / FALSE)
	 */
	public static Boolean isAuthenticated() {
		return egovUserDetailsService.isAuthenticated();
	}
	
	public static Boolean isAuthenticated(HttpServletRequest request, HttpServletResponse response) {
		
		Object obj = egovUserDetailsService.getAuthenticatedUser();
		if(obj != null) {
			/*
			LoginVO loginVO = (LoginVO)obj;
			
			try {
				SiteManageVO siteVO = siteInfoService.selectSiteServiceInfo(request);
				if(siteVO != null) {
					if("Y".equals(siteVO.getCrtfcUseAt()) && loginVO.getSbscrbSite() != null && "SITE_000000000000001".equals(siteVO.getSiteId())) {
						boolean findFlag = false;
						for(int i=0; i<loginVO.getSbscrbSite().length; i++) {
							if(siteVO.getSiteId().equals(loginVO.getSbscrbSite()[i])) {
								findFlag = true;
								break;
							}
						}
						
						if(!findFlag) {
							return false;
						}
						
					}
				}
			} catch(Exception ex) {}
			*/
			return true;
		}
		return false;
		
		//return egovUserDetailsService.isAuthenticated();
	}
				
		//로그인 URL
		public static String getRedirectLoginUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uat/uia/egovLoginUsr.do";
			return "/uat/uia/egovLoginUsr.do";
		}

		//로그아웃 URL
		public static String getRedirectLogoutUrl() throws Exception {
			
			return "/uat/uia/actionLogout.do";
		}
		
		//회원가입 URL
		public static String getRedirectRegistUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uss/umt/cmm/EgovSelectMber.do";
			//return "/uss/umt/cmm/EgovSelectMber.do";
			return "/uss/umt/cmm/EgovStplatCnfirmMber.do";
		}
		
		//아이디찾기 URL
		public static String getRedirectFindIdUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uat/uia/egovIdSearchView.do";
			return "/uat/uia/egovIdSearchView.do";
		}
		
		//비밀번호재발급 URL
		public static String getRedirectFindPasswordUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uat/uia/egovPasswordSearchView.do";
			//return "/uat/uia/egovPasswordSearchView.do";
			return "/uat/uia/egovPasswordSearch.do";
		}
		
		//정보수정 URL
		public static String getRedirectUserUpdateUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=update";
			return "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=update";
		}
		
		//비밀번호변경 URL
		public static String getRedirectUserPasswordUpdateUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=password";
			return "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=password";
		}
		
		//회원탈퇴 URL
		public static String getRedirectUserDeleteUrl() throws Exception {
			
			//return Globals.DOMAIN  + "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=secsn";
			return "/uss/umt/cmm/EgovUserConfirmView.do?trgtPge=secsn";
		}

}
