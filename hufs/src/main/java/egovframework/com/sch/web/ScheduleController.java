package egovframework.com.sch.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.sms.service.EgovSmsInfoService;
import egovframework.com.evt.service.ComtnschdulinfoService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import net.sf.json.JSONObject;

/******************************************************
 * @Class Name : ComtnschdulinfoMngController.java
 * @Program name : egovframework.com.mng.evt
 * @Descriptopn : 충청남도교육연구정보원 스마트충남 기능 개선 구축
 * @version : 1.0.0
 * @author : 이호영
 * @created date : 2012. 2. 14.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2012. 2. 14.        이호영             first generated
*********************************************************/

@Controller
public class ScheduleController {

	/** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    /** ComtnschdulinfoService */
    @Resource(name = "comtnschdulinfoService")
    private ComtnschdulinfoService comtnschdulinfoService;

    @Autowired
	private DefaultBeanValidator beanValidator;

    /** EgovFileMngUtil */
    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService            fileMngService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "EgovSmsInfoService")
    private EgovSmsInfoService egovSmsInfoService;

    @Resource(name = "egovMessageSource")
	private EgovMessageSource egovMessageSource;

    @Resource(name = "EgovBBSCtgryService")
	private EgovBBSCtgryService egovBBSCtgryService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

	/**
	 * 학교별 시간표 조회
	 * @param searchVO - 조회할 정보가 담긴 ScheduleMngVO
	 * @return "/sch/selectCampusSchedule"
	 * @exception Exception
	 */
    @RequestMapping("/sch/selectCampusSchedule.do")
    public String selectCampusSchedule(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	//캠퍼스 카테고리 리스트
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		model.addAttribute("resultList", scheduleMngService.selectCampusSchedule(searchVO));
  		model.addAttribute("campusVO", searchVO);

    	return "/sch/campusSchedule";
    }

    @RequestMapping(value="/sch/insertCampusSchedule.json")
    public void insertCampusSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "N";

        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

  		int delCnt = scheduleMngService.deleteCampusSchedule(searchVO);
  		scheduleMngService.insertCampusSchedule(searchVO);

		successYn = "Y";

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    /**
	 * 수업 시간표 조회
	 * @param searchVO - 조회할 정보가 담긴 ScheduleMngVO
	 * @return "/sch/selectClassSchedule"
	 * @exception Exception
	 */
    @RequestMapping("/sch/selectClassSchedule.do")
    public String selectClassSchedule(ScheduleMngVO searchVO, @RequestParam String plType, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	Ctgry ctgry = new Ctgry();

    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

	   	// 소속 리스트
    	/*ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS50");
    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));*/

    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//searchVO.setPlType("all");
    	if(!EgovStringUtil.isEmpty(plType)){
    		searchVO.setPlType(plType);
    	}

    	//과정정보
    	/*CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));*/

    	//과정 시간표
    	model.addAttribute("crclList", scheduleMngService.selectCrclSchedule(searchVO));
    	// 학교일정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectClassSchedule(searchVO));
    	model.addAttribute("plType", plType);

    	return "/sch/classSchedule";
    }

    @RequestMapping(value="/sch/insertClassSchedule.do")
    public String insertClassSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setFrstRegisterId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.insertClassSchedule(searchVO);

  		return "redirect:/sch/selectClassSchedule.do?plType="+searchVO.getPlType();

    }
    
    @RequestMapping(value="/sch/insertIndSchedule.do")
    public String insertIndSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setFrstRegisterId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());
  		
  		if("regist".equals(searchVO.getFlag())){
  			scheduleMngService.insertClassSchedule(searchVO);
  		}else if("update".equals(searchVO.getFlag())){
  			scheduleMngService.updateClassSchedule(searchVO, request, response);
  		}

  		

  		return "redirect:/sch/scheduleCalendar.do?menuId=MNU_0000000000000084";
    }

    /**
	 * 수업 시간표 상세
	 */
    @RequestMapping("/sch/selectClassScheduleView.do")
    public String selectClassScheduleView(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	Ctgry ctgry = new Ctgry();

    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	// 소속 리스트
    	/*ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS50");
    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));*/

    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		if(loginVO != null){
  			searchVO.setFrstRegisterId(loginVO.getId());
  		}

    	//searchVO.setPlType("all");
    	model.addAttribute("plType", searchVO.getPlType());
    	model.addAttribute("resultList", scheduleMngService.selectClassSchedule(searchVO));
    	model.addAttribute("scheduleMngVO", scheduleMngService.selectClassScheduleView(searchVO));

    	return "/sch/classScheduleView";
    }

    @RequestMapping(value="/sch/updateClassSchedule.do")
    public String updateClassSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setLastUpdusrId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.updateClassSchedule(searchVO, request, response);

  		return "redirect:/sch/selectClassSchedule.do?plType="+searchVO.getPlType();

    }

    @RequestMapping(value="/sch/deleteClassSchedule.do")
    public String deleteCampusSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	scheduleMngService.deleteClassSchedule(searchVO);

    	return "redirect:/sch/selectClassSchedule.do?plType="+searchVO.getPlType();
    }

    /**
	 * 수업시간표
	 */
    @RequestMapping("/sch/scheduleCalendar.do")
    public String scheduleCalendar(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	
    	Ctgry ctgry = new Ctgry();
    	/*
    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));*/
    	
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
  		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		String userSeCodeNm = "";
    	if("02".equals(loginVO.getUserSeCode()) || "04".equals(loginVO.getUserSeCode()) || "06".equals(loginVO.getUserSeCode())) {
    		userSeCodeNm = "student";
    		searchVO.setFrstRegisterId(loginVO.getId());
  		} else { //교원
  			userSeCodeNm = "teacher";
  		}
    	searchVO.setManageCode(userSeCodeNm); // managecode에 userSeCodeNm 값 세팅함
    	
    	// 학교일정 시간표
    	model.addAttribute("schoolSchList", scheduleMngService.selectClassSchedule(searchVO));
    	
    	
  		if(loginVO != null){
  			searchVO.setFacId(loginVO.getId());
  		}
  		
  		//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);
    	
    	CurriculumVO currVO = new CurriculumVO();
    	currVO.setUserId(searchVO.getFacId());
    	currVO.setSearchSchAt("Y");
    	
    	currVO.setMyCurriculumPageFlag(userSeCodeNm);
    	currVO.setSelectLangAt("Y");
    	List<?> curriculumList = curriculumService.selectCurriculumList(currVO, request, response);
        model.addAttribute("curriculumList", curriculumList);
        
        searchVO.setFrstRegisterId(loginVO.getId());
        model.addAttribute("searchVO", searchVO);

    	/*searchVO.setPlType("crcl");

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	Ctgry ctgry = new Ctgry();
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));
    	model.addAttribute("crclId", searchVO.getCrclId());*/

    	return "/sch/scheduleCalendar";
    }
    
    /**
	 * 수업시간표 - 상세(수정)
	 */
    @RequestMapping("/sch/scheduleCalendarView.do")
    public String scheduleCalendarView(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	
    	Ctgry ctgry = new Ctgry();
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
    	// 학교일정 시간표
    	model.addAttribute("schoolSchList", scheduleMngService.selectClassSchedule(searchVO));
    	
    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		if(loginVO != null){
  			searchVO.setFacId(loginVO.getId());
  		}
  		
  		//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);
    	
    	CurriculumVO currVO = new CurriculumVO();
    	currVO.setUserId(searchVO.getFacId());
    	currVO.setSearchSchAt("Y");
    	String userSeCodeNm = "";
    	if("02".equals(loginVO.getUserSeCode()) || "04".equals(loginVO.getUserSeCode()) || "06".equals(loginVO.getUserSeCode())) {
    		userSeCodeNm = "student";
  		} else { //교원
  			userSeCodeNm = "teacher";
  		}
    	currVO.setMyCurriculumPageFlag(userSeCodeNm);
    	List<?> curriculumList = curriculumService.selectCurriculumList(currVO, request, response);
        model.addAttribute("curriculumList", curriculumList);
        
        model.addAttribute("searchVO", searchVO);

    	return "/sch/scheduleCalendar";
    }

    /**
	 * 오늘의 수업
	 */
    @RequestMapping("/sch/selectTodayCrclList.do")
    public String selectTodayCrclList(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	model.addAttribute("USER_INFO", user);
    	
		SimpleDateFormat sDate = new SimpleDateFormat("yyyy-MM-dd");

  		CurriculumVO myCurriculumVO = new CurriculumVO();
	  	myCurriculumVO.setFacId(user.getId());
	  	myCurriculumVO.setCrclId(searchVO.getCrclId());
	  	
	  	String pageFlag = "";
	  	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	  		pageFlag = "student";
  		} else { //교원
  			pageFlag = "teacher";
  		}
	  	model.addAttribute("pageFlag", pageFlag);
	  	String tempStartDt = "";
	  	if("".equals(searchVO.getStartDt())){
	  		tempStartDt = sDate.format(new Date());
	  	}else{
	  		tempStartDt = searchVO.getStartDt();
	  	}
	  	model.addAttribute("crclDay", tempStartDt); // 오늘 날짜

	  	myCurriculumVO.setStartDate(tempStartDt);

	  	SimpleDateFormat sDate2 = new SimpleDateFormat("yyyy년 MM월 dd일");

	  	if("".equals(searchVO.getStartDt())){
	  		tempStartDt = sDate2.format(new Date());
	  	}else{
	  		tempStartDt = sDate2.format(sDate.parse(searchVO.getStartDt()));
	  	}
	  	model.addAttribute("today", tempStartDt); // 오늘 날짜
	  	String dayNm = EgovDateUtil.getDateDay(EgovDateUtil.getToday());
	  	model.addAttribute("dayNm", dayNm); // 요일
	  	
	  	model.addAttribute("dayFlag", "TODAY");
	  	
	  	String nextDay = EgovDateUtil.formatDate(EgovDateUtil.addDay(EgovDateUtil.getToday(), 1), "-");
	  	String prevDay = EgovDateUtil.formatDate(EgovDateUtil.addDay(EgovDateUtil.getToday(), -1), "-");
	  	model.addAttribute("nextDay", nextDay);
	  	model.addAttribute("prevDay", prevDay);

	  	myCurriculumVO.setIsMainFlag("N");
	  	myCurriculumVO.setMyCurriculumPageFlag(pageFlag);
	  	List<?> tmpTodayCrclList = curriculumService.selectTodayCrclList(myCurriculumVO);
	  	List<EgovMap> todayCrclList = (List<EgovMap>) tmpTodayCrclList;
	  		for(int i = 0; i < tmpTodayCrclList.size(); i++){
				EgovMap map = (EgovMap) tmpTodayCrclList.get(i);
				String crclId = map.get("crclId").toString();
				String plId = map.get("plId").toString();
				ScheduleMngVO sVO = new ScheduleMngVO();
				sVO.setCrclId(crclId);
				sVO.setPlId(plId);

				todayCrclList.get(i).put("facPlList", scheduleMngService.selectFacultyList(sVO));
			}
		  model.addAttribute("myCurriculumList", todayCrclList);

    	return "/sch/todaySchedule";
    }

    /**
	 * 오늘의 수업 - 이전, 다음 수업
	 */
    @RequestMapping("/sch/todayScheduleAjax.do")
    public String todayScheduleAjax(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	model.addAttribute("USER_INFO", user);
    	
    	SimpleDateFormat sDate = new SimpleDateFormat("yyyy-MM-dd");
    	model.addAttribute("crclDay", searchVO.getStartDt());

    	CurriculumVO myCurriculumVO = new CurriculumVO();
    	myCurriculumVO.setCrclId(searchVO.getCrclId());
    	
    	String pageFlag = "";

    	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	  		pageFlag = "student";
  		} else { //교원
  			pageFlag = "teacher";
  		}

    	model.addAttribute("pageFlag", pageFlag);
    	myCurriculumVO.setFacId(user.getId());

		myCurriculumVO.setStartDate(searchVO.getStartDt());

		myCurriculumVO.setIsMainFlag("N");
		myCurriculumVO.setMyCurriculumPageFlag(pageFlag);
		List<?> tmpTodayCrclList = curriculumService.selectTodayCrclList(myCurriculumVO);
		List<EgovMap> todayCrclList = (List<EgovMap>) tmpTodayCrclList;
		for(int i = 0; i < tmpTodayCrclList.size(); i++){
			EgovMap map = (EgovMap) tmpTodayCrclList.get(i);
			String crclId = map.get("crclId").toString();
			String plId = map.get("plId").toString();
			ScheduleMngVO sVO = new ScheduleMngVO();
			sVO.setCrclId(crclId);
			sVO.setPlId(plId);

			todayCrclList.get(i).put("facPlList", scheduleMngService.selectFacultyList(sVO));
		}

		model.addAttribute("myCurriculumList", todayCrclList);

		String realToday = sDate.format(new Date());
		if(realToday.equals(myCurriculumVO.getStartDate())){
			model.addAttribute("dayFlag", "TODAY");
		}else if(realToday.compareTo(myCurriculumVO.getStartDate()) > 0){
			model.addAttribute("dayFlag", "YESTERDAY");
		}else if(realToday.compareTo(myCurriculumVO.getStartDate()) < 0){
			model.addAttribute("dayFlag", "TOMORROW");
		}

		model.addAttribute("yesterDate", EgovDateUtil.convertDate(EgovDateUtil.addDay(myCurriculumVO.getStartDate(), -1), "0000", "yyyy-MM-dd"));
		model.addAttribute("tomorrowDate", EgovDateUtil.convertDate(EgovDateUtil.addDay(myCurriculumVO.getStartDate(), 1), "0000", "yyyy-MM-dd"));
		model.addAttribute("today", EgovDateUtil.convertDate(myCurriculumVO.getStartDate().replaceAll("-", ""), "0000", "yyyy년 MM월 dd일"));
		String dayNm = EgovDateUtil.getDateDay(myCurriculumVO.getStartDate().replaceAll("-", ""));
		model.addAttribute("dayNm", dayNm); // 요일
		model.addAttribute("searchMenuId", searchVO.getMenuId());

    	return "/sch/todayScheduleAjax";
    }
    
    @RequestMapping(value="/sch/selectIndScheduleDetail.json")
    public void selectIndScheduleDetail(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "N";

        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

  		ScheduleMngVO schVO = scheduleMngService.selectClassScheduleView(searchVO);
  		
		jo.put("schVO", schVO);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }


}
