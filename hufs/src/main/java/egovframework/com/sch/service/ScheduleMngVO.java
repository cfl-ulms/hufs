package egovframework.com.sch.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import egovframework.com.cmm.ComDefaultVO;
import egovframework.com.evt.service.ComtneventcnsrVO;

/**
 * @Class Name : ScheduleMngVO.java
 * @Description : schedule VO class
 * @Modification Information
 *
 *  Copyright (C)  All right reserved.
 */
@SuppressWarnings("serial")
public class ScheduleMngVO  extends ComDefaultVO{

	/** 사이트ID */
    private String siteId = "";

    /** 캠퍼스 ID */
    private String campusId = "";

    //캠퍼스 명
    private String campusNm = "";

    /** 교시 */
    private String period = "";

    /** 시작일자 */
    private String startDt = "";

    /** 종료일자 */
    private String endDt = "";

    /** 시작시간 */
    private String startTime = "";

    private String startTimeHH = "";
    private String startTimeMM = "";
    private List<String> startTimeHHList;
    private List<String> startTimeMMList;
    
    /** 종료시간 */
    private String endTime = "";
    private String endTimeHH = "";
    private String endTimeMM = "";
    private List<String> endTimeHHList;
    private List<String> endTimeMMList;

    private String title = "";

    private String plId = "";

    private String plType = "";

    private String allDayAt = "";

    private String placeDetail = "";

    private String plCn = "";

    private String useAt = "";

    private String frstRegisterId = "";
    private String frstRegisterPnttm = "";
    private String lastUpdusrId = "";
    private String lastUpdusrPnttm = "";

    //과정ID
    private java.lang.String crclId;

    //시수
    private java.lang.String sisu;

    //수업주제
    private java.lang.String studySubject;

    //교원ID
    private java.lang.String facId;
    private List<String> facIdList;

    //권한
    private java.lang.String manageCode;

    //교시글자(과정시간표 교시 param)
    private java.lang.String periodTxt;

  	//비교
    private java.lang.String cpr;

    //언어
    private java.lang.String crclLang;

    //언어명
    private java.lang.String crclLangNm;

    //수업레벨
    private java.lang.String spLevel;

    //수업레벨명
    private java.lang.String spLevelNm;

    //수업레벨넘버
    private java.lang.String spLevelNum;
    
    //수업레벨명
    private java.lang.String spLevelNumNm;
    
    //수업방법
    private java.lang.String spType;

    //평가여부
    private java.lang.String evtAt;

    //첨부파일ID
    private java.lang.String atchFileId;
    private java.lang.String fileGroupId;

    //수업내용
    private java.lang.String spCn;

    //수업목표
    private java.lang.String spGoal;

    //교원명 검색
    private java.lang.String searchFacNm;

    //강의유형ID
    private java.lang.String courseId;

    //강의유형 명
    private java.lang.String courseNm;

    //온라인
    private java.lang.String online;

    //오프라인
    private java.lang.String offline;

    //교원변경여부
    private java.lang.String changeFacAt;

    //평가Id
    private java.lang.String evtId;
    private List<String> evtIdList;

    //평가명
    private java.lang.String evtNm;

    private String searchStrDt;

    private String searchEndDt;
    
    private String searchCrclNm;
    
    //대분류검색
    private java.lang.String searchSysCode1;
    
    //중분류검색
    private java.lang.String searchSysCode2;

    private List arrTime = ListUtils.lazyList(new ArrayList(), new Factory() {
	    public Object create() {
	      return new ScheduleMngVO();
	    }
	});

    private List<String> refeFileList;

    private String streFileNm;

    private String fileDelChk;

    private String searchCtgryId;

    private String searchCrclId;

    private String searchCrclbId;

    private String searchStartDate;

    private String searchEndDate;
    
    private String searchUserId;
    
    private String studentId;
    private String dayofweek;
    
    private java.lang.String flag;
    
    private String facCnt;
    
    private String sisuSum;
    
    public String getDayofweek() {
		return dayofweek;
	}

	public void setDayofweek(String dayofweek) {
		this.dayofweek = dayofweek;
	}

	//탭 스타일
    private String tabType;
    
    public List getArrTime() {
    	return arrTime;
	}

	public void setArrTime(List arrTime) {
		this.arrTime = arrTime;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getCampusId() {
		return campusId;
	}

	public void setCampusId(String campusId) {
		this.campusId = campusId;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStartTimeHH() {
		return startTimeHH;
	}

	public void setStartTimeHH(String startTimeHH) {
		this.startTimeHH = startTimeHH;
	}

	public String getStartTimeMM() {
		return startTimeMM;
	}

	public void setStartTimeMM(String startTimeMM) {
		this.startTimeMM = startTimeMM;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEndTimeHH() {
		return endTimeHH;
	}

	public void setEndTimeHH(String endTimeHH) {
		this.endTimeHH = endTimeHH;
	}

	public String getEndTimeMM() {
		return endTimeMM;
	}

	public void setEndTimeMM(String endTimeMM) {
		this.endTimeMM = endTimeMM;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPlId() {
		return plId;
	}

	public void setPlId(String plId) {
		this.plId = plId;
	}

	public String getPlType() {
		return plType;
	}

	public void setPlType(String plType) {
		this.plType = plType;
	}

	public String getAllDayAt() {
		return allDayAt;
	}

	public void setAllDayAt(String allDayAt) {
		this.allDayAt = allDayAt;
	}

	public String getPlaceDetail() {
		return placeDetail;
	}

	public void setPlaceDetail(String placeDetail) {
		this.placeDetail = placeDetail;
	}

	public String getPlCn() {
		return plCn;
	}

	public void setPlCn(String plCn) {
		this.plCn = plCn;
	}

	public String getUseAt() {
		return useAt;
	}

	public void setUseAt(String useAt) {
		this.useAt = useAt;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public String getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(String frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public String getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(String lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public java.lang.String getCrclId() {
		return crclId;
	}

	public void setCrclId(java.lang.String crclId) {
		this.crclId = crclId;
	}

	public java.lang.String getStudySubject() {
		return studySubject;
	}

	public void setStudySubject(java.lang.String studySubject) {
		this.studySubject = studySubject;
	}

	public java.lang.String getFacId() {
		return facId;
	}

	public void setFacId(java.lang.String facId) {
		this.facId = facId;
	}

	public List<String> getFacIdList() {
		return facIdList;
	}

	public void setFacIdList(List<String> facIdList) {
		this.facIdList = facIdList;
	}

	public java.lang.String getManageCode() {
		return manageCode;
	}

	public void setManageCode(java.lang.String manageCode) {
		this.manageCode = manageCode;
	}

	public java.lang.String getSisu() {
		return sisu;
	}

	public void setSisu(java.lang.String sisu) {
		this.sisu = sisu;
	}

	public java.lang.String getPeriodTxt() {
		return periodTxt;
	}

	public void setPeriodTxt(java.lang.String periodTxt) {
		this.periodTxt = periodTxt;
	}

	public java.lang.String getCpr() {
		return cpr;
	}

	public void setCpr(java.lang.String cpr) {
		this.cpr = cpr;
	}

	public java.lang.String getCrclLang() {
		return crclLang;
	}

	public void setCrclLang(java.lang.String crclLang) {
		this.crclLang = crclLang;
	}

	public java.lang.String getCrclLangNm() {
		return crclLangNm;
	}

	public void setCrclLangNm(java.lang.String crclLangNm) {
		this.crclLangNm = crclLangNm;
	}

	public java.lang.String getSpLevel() {
		return spLevel;
	}

	public void setSpLevel(java.lang.String spLevel) {
		this.spLevel = spLevel;
	}

	public java.lang.String getSpType() {
		return spType;
	}

	public void setSpType(java.lang.String spType) {
		this.spType = spType;
	}

	public java.lang.String getEvtAt() {
		return evtAt;
	}

	public void setEvtAt(java.lang.String evtAt) {
		this.evtAt = evtAt;
	}

	public java.lang.String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(java.lang.String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public java.lang.String getSpCn() {
		return spCn;
	}

	public void setSpCn(java.lang.String spCn) {
		this.spCn = spCn;
	}

	public java.lang.String getSpGoal() {
		return spGoal;
	}

	public void setSpGoal(java.lang.String spGoal) {
		this.spGoal = spGoal;
	}

	public java.lang.String getSearchFacNm() {
		return searchFacNm;
	}

	public void setSearchFacNm(java.lang.String searchFacNm) {
		this.searchFacNm = searchFacNm;
	}

	public java.lang.String getCourseId() {
		return courseId;
	}

	public void setCourseId(java.lang.String courseId) {
		this.courseId = courseId;
	}

	public java.lang.String getOnline() {
		return online;
	}

	public void setOnline(java.lang.String online) {
		this.online = online;
	}

	public java.lang.String getOffline() {
		return offline;
	}

	public void setOffline(java.lang.String offline) {
		this.offline = offline;
	}

	public java.lang.String getCourseNm() {
		return courseNm;
	}

	public void setCourseNm(java.lang.String courseNm) {
		this.courseNm = courseNm;
	}

	public java.lang.String getChangeFacAt() {
		return changeFacAt;
	}

	public void setChangeFacAt(java.lang.String changeFacAt) {
		this.changeFacAt = changeFacAt;
	}

	public java.lang.String getEvtId() {
		return evtId;
	}

	public void setEvtId(java.lang.String evtId) {
		this.evtId = evtId;
	}

	public List<String> getEvtIdList() {
		return evtIdList;
	}

	public void setEvtIdList(List<String> evtIdList) {
		this.evtIdList = evtIdList;
	}

	public java.lang.String getEvtNm() {
		return evtNm;
	}

	public void setEvtNm(java.lang.String evtNm) {
		this.evtNm = evtNm;
	}

	public java.lang.String getFileGroupId() {
		return fileGroupId;
	}

	public void setFileGroupId(java.lang.String fileGroupId) {
		this.fileGroupId = fileGroupId;
	}

	public java.lang.String getSpLevelNm() {
		return spLevelNm;
	}

	public void setSpLevelNm(java.lang.String spLevelNm) {
		this.spLevelNm = spLevelNm;
	}

	public String getCampusNm() {
		return campusNm;
	}

	public void setCampusNm(String campusNm) {
		this.campusNm = campusNm;
	}

	public List<String> getRefeFileList() {
		return refeFileList;
	}

	public void setRefeFileList(List<String> refeFileList) {
		this.refeFileList = refeFileList;
	}

	public String getStreFileNm() {
		return streFileNm;
	}

	public void setStreFileNm(String streFileNm) {
		this.streFileNm = streFileNm;
	}

	public String getFileDelChk() {
		return fileDelChk;
	}

	public void setFileDelChk(String fileDelChk) {
		this.fileDelChk = fileDelChk;
	}

	public String getSearchStrDt() {
		return searchStrDt;
	}

	public void setSearchStrDt(String searchStrDt) {
		this.searchStrDt = searchStrDt;
	}

	public String getSearchEndDt() {
		return searchEndDt;
	}

	public void setSearchEndDt(String searchEndDt) {
		this.searchEndDt = searchEndDt;
	}

	public String getSearchCtgryId() {
		return searchCtgryId;
	}

	public void setSearchCtgryId(String searchCtgryId) {
		this.searchCtgryId = searchCtgryId;
	}

	public String getSearchCrclId() {
		return searchCrclId;
	}

	public void setSearchCrclId(String searchCrclId) {
		this.searchCrclId = searchCrclId;
	}

	public String getSearchCrclbId() {
		return searchCrclbId;
	}

	public void setSearchCrclbId(String searchCrclbId) {
		this.searchCrclbId = searchCrclbId;
	}

	public String getSearchStartDate() {
		return searchStartDate;
	}

	public void setSearchStartDate(String searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	public String getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTabType() {
		return tabType;
	}

	public void setTabType(String tabType) {
		this.tabType = tabType;
	}

	public java.lang.String getSpLevelNum() {
		return spLevelNum;
	}

	public void setSpLevelNum(java.lang.String spLevelNum) {
		this.spLevelNum = spLevelNum;
	}

	public java.lang.String getSpLevelNumNm() {
		return spLevelNumNm;
	}

	public void setSpLevelNumNm(java.lang.String spLevelNumNm) {
		this.spLevelNumNm = spLevelNumNm;
	}

	public java.lang.String getSearchSysCode1() {
		return searchSysCode1;
	}

	public void setSearchSysCode1(java.lang.String searchSysCode1) {
		this.searchSysCode1 = searchSysCode1;
	}

	public java.lang.String getSearchSysCode2() {
		return searchSysCode2;
	}

	public void setSearchSysCode2(java.lang.String searchSysCode2) {
		this.searchSysCode2 = searchSysCode2;
	}

	public java.lang.String getFlag() {
		return flag;
	}

	public void setFlag(java.lang.String flag) {
		this.flag = flag;
	}

	public String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public String getFacCnt() {
		return facCnt;
	}

	public void setFacCnt(String facCnt) {
		this.facCnt = facCnt;
	}

	public String getSisuSum() {
		return sisuSum;
	}

	public void setSisuSum(String sisuSum) {
		this.sisuSum = sisuSum;
	}

	public List<String> getStartTimeHHList() {
		return startTimeHHList;
	}

	public void setStartTimeHHList(List<String> startTimeHHList) {
		this.startTimeHHList = startTimeHHList;
	}

	public List<String> getStartTimeMMList() {
		return startTimeMMList;
	}

	public void setStartTimeMMList(List<String> startTimeMMList) {
		this.startTimeMMList = startTimeMMList;
	}

	public List<String> getEndTimeHHList() {
		return endTimeHHList;
	}

	public void setEndTimeHHList(List<String> endTimeHHList) {
		this.endTimeHHList = endTimeHHList;
	}

	public List<String> getEndTimeMMList() {
		return endTimeMMList;
	}

	public void setEndTimeMMList(List<String> endTimeMMList) {
		this.endTimeMMList = endTimeMMList;
	}

	public String getSearchUserId() {
		return searchUserId;
	}

	public void setSearchUserId(String searchUserId) {
		this.searchUserId = searchUserId;
	}

}
