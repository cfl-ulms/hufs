package egovframework.com.sch.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : SchedulebaseDAO.java
 * @Description : Schedulebase DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Repository("schedulebaseDAO")
public class SchedulebaseDAO extends EgovAbstractDAO {

    /**
	 * 캠퍼스 시간표 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return 목록
	 * @exception Exception
	 */
    public List<?> selectCampusSchedule(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectCampusSchedule", searchVO);
    }

    /**
	 * 캠퍼스 시간표 저장
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return 목록
	 * @exception Exception
	 */
    public void insertCampusSchedule(ScheduleMngVO vo) throws Exception {

    	EgovMap paramMap = new EgovMap();
    	paramMap.put("campusId", vo.getCampusId());

    	EgovMap schMap;
    	List<EgovMap> schList = new ArrayList<EgovMap>();
    	String stTime = vo.getStartTime().replace(":", "");
    	String edTime = vo.getEndTime().replace(":", "");

    	for(int i=1; i<=12; i++){
    		schMap = new EgovMap();

    		if(i == 1){
    			schMap.put("period", i);
    			if(Integer.parseInt(stTime) < 1000){
    				stTime = "0" + Integer.parseInt(stTime);
    			}
    			if(Integer.parseInt(edTime) < 1000){
    				edTime = "0" + Integer.parseInt(edTime);
    			}
        		schMap.put("startTime", stTime);
        		schMap.put("endTime", edTime);
    		}else{
    			stTime = Integer.toString(Integer.parseInt(stTime) + 100);
    			edTime = Integer.toString(Integer.parseInt(edTime) + 100);
    			if(Integer.parseInt(stTime) < 1000){
    				stTime = "0" + Integer.parseInt(stTime);
    			}
    			if(Integer.parseInt(edTime) < 1000){
    				edTime = "0" + Integer.parseInt(edTime);
    			}

    			schMap.put("period", i);
        		schMap.put("startTime", stTime);
        		schMap.put("endTime", edTime);
    		}
    		schList.add(schMap);
    	}
    	paramMap.put("schList", schList);

        insert("schedulebaseDAO.insertCampusSchedule", paramMap);

    }
    
    //저장
    public void insertCampusScheduleAll(EgovMap paramMap) throws Exception {
    	insert("schedulebaseDAO.insertCampusSchedule", paramMap);
    }
    
    public int deleteCampusSchedule(ScheduleMngVO searchVO) throws Exception {
        return delete("schedulebaseDAO.deleteCampusSchedule", searchVO);
    }

    public List<?> selectClassSchedule(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectClassSchedule", searchVO);
    }

    /**
	 * 수업 시간표 저장
	 */
    public void insertClassSchedule(ScheduleMngVO vo) throws Exception {
        insert("schedulebaseDAO.insertClassSchedule", vo);
    }

    public ScheduleMngVO selectClassScheduleView(ScheduleMngVO searchVO) throws Exception {
        return (ScheduleMngVO) select("schedulebaseDAO.selectClassScheduleView", searchVO);
    }

    public void updateClassSchedule(ScheduleMngVO vo) throws Exception {

        update("schedulebaseDAO.updateClassSchedule", vo);

    }

    public void deleteClassSchedule(ScheduleMngVO vo) throws Exception {
        update("schedulebaseDAO.deleteClassSchedule", vo);
    }

    //과정시간표 교원 삭제
    public void deleteFaculty(ScheduleMngVO vo) throws Exception {
        delete("schedulebaseDAO.deleteFaculty", vo);
    }

	//과정시간표 교원 저장
    public void insertFaculty(ScheduleMngVO vo) throws Exception {
        insert("schedulebaseDAO.insertFaculty", vo);
    }

    //시간표 담당교원
    public List selectFacultyList(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectFacultyList", searchVO);
    }
    
 	//과정 담당교원
    public List selectCrclFacultyList(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectCrclFacultyList", searchVO);
    }
    
    //과정 담당교원 카운트
    public List selectCrclFacultyListCnt(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectCrclFacultyListCnt", searchVO);
    }

    //과정시간표
    public List selectCrclSchedule(ScheduleMngVO searchVO) throws Exception {
        return list("schedulebaseDAO.selectCrclSchedule", searchVO);
    }

    //과정시간표 총 개수
    public int selectCrclScheduleTotCnt(ScheduleMngVO searchVO) {
        return (Integer)select("schedulebaseDAO.selectCrclScheduleTotCnt_S", searchVO);
    }

    public void updateCalendarSchedule(ScheduleMngVO vo) throws Exception {

        update("schedulebaseDAO.updateCalendarSchedule", vo);

    }

    public ScheduleMngVO selectCalendarScheduleView(ScheduleMngVO searchVO) throws Exception {
        return (ScheduleMngVO) select("schedulebaseDAO.selectCalendarScheduleView", searchVO);
    }

    //수업평가 등록
    public void insertEvaluation(ScheduleMngVO vo) throws Exception {
        insert("schedulebaseDAO.insertEvaluation", vo);
    }

    //수업평가 삭제
    public void deleteEvaluation(ScheduleMngVO vo) throws Exception {
        delete("schedulebaseDAO.deleteEvaluation", vo);
    }

    //등록 된 평가 목록
    public List selectStudyEvtList(ScheduleMngVO vo) throws Exception{
    	return list("schedulebaseDAO.selectStudyEvtList", vo);
    }

    //등록 된 수업자료
    public List selectStudyFileList(ScheduleMngVO vo) throws Exception{
    	return list("schedulebaseDAO.selectStudyFileList", vo);
    }

    //수업자료 등록
    public void insertStudyFile(ScheduleMngVO vo) throws Exception {
        insert("schedulebaseDAO.insertStudyFile", vo);
    }

    //등록된 수업자료 삭제
    public void deleteStudyFile(ScheduleMngVO vo) throws Exception {
        delete("schedulebaseDAO.deleteStudyFile", vo);
    }

    public EgovMap selectScheduleAttention(ScheduleMngVO vo) throws Exception{
    	return (EgovMap) select("schedulebaseDAO.selectScheduleAttention", vo);
    }

}
