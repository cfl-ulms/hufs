package egovframework.com.sch.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.evt.service.ComtnschdulinfoVO;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;


/**
 * @Class Name : ScheduleMngService.java
 * @Description : ScheduleMng Business class
 * @Modification Information
 *
 *  Copyright (C)  All right reserved.
 */
public interface ScheduleMngService {

	/**
	 * 캠퍼스 시간표 조회
	 * @param vo ScheduleMngVO
	 * @return List
	 * @exception Exception
	 */
	public List selectCampusSchedule(ScheduleMngVO vo) throws Exception ;

	/**
	 * 캠퍼스 시간표 저장
	 * @param vo ScheduleMngVO
	 * @return List
	 * @exception Exception
	 */
	public void insertCampusSchedule(ScheduleMngVO vo) throws Exception ;
	
	//캠퍼스 일괄 저장
	public void insertCampusScheduleAll(ScheduleMngVO vo) throws Exception;
		
	/**
	 * 캠퍼스 시간표 삭제
	 * @param vo ScheduleMngVO
	 * @return List
	 * @exception Exception
	 */
	public int deleteCampusSchedule(ScheduleMngVO vo) throws Exception ;

	/**
	 * 수업 시간표 조회
	 */
	public List selectClassSchedule(ScheduleMngVO vo) throws Exception ;

	/**
	 * 수업 시간표 저장
	 */
	public void insertClassSchedule(ScheduleMngVO vo) throws Exception ;

	/**
	 * 수업 시간표
	 */
	public ScheduleMngVO selectClassScheduleView(ScheduleMngVO vo) throws Exception ;

	public void updateClassSchedule(ScheduleMngVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception ;

	public void deleteClassSchedule(ScheduleMngVO vo) throws Exception ;

	//담당교원목록
	public List selectFacultyList(ScheduleMngVO vo) throws Exception;
	
	//과정 담당교원목록
	public List selectCrclFacultyList(ScheduleMngVO vo) throws Exception;
	
	//과정 담당교원목록 카운트
	public List selectCrclFacultyListCnt(ScheduleMngVO vo) throws Exception;

	//과정시간표
    public List selectCrclSchedule(ScheduleMngVO vo) throws Exception;

	//과정시간표 총 개수
    int selectCrclScheduleTotCnt(ScheduleMngVO searchVO);

    public void updateCalendarSchedule(ScheduleMngVO vo) throws Exception ;

    public ScheduleMngVO selectCalendarScheduleView(ScheduleMngVO vo) throws Exception ;

    //과정수업 수정
    public void updateStudyPlan(ScheduleMngVO vo) throws Exception ;

    //등록 된 평가 목록
    public List selectStudyEvtList(ScheduleMngVO vo) throws Exception;

    public List selectStudyFileList(ScheduleMngVO vo) throws Exception;

    public EgovMap selectScheduleAttention(ScheduleMngVO vo) throws Exception;

}
