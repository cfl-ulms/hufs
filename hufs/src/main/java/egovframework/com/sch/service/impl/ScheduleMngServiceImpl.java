package egovframework.com.sch.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.evt.service.ComtnschdulinfoVO;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : ScheduleMngServiceImpl.java
 * @Description : Schedule Business Implement class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Service("ScheduleMngService")
public class ScheduleMngServiceImpl extends EgovAbstractServiceImpl implements ScheduleMngService {

    @Resource(name="schedulebaseDAO")
    private SchedulebaseDAO schedulebaseDAO;

    /** ID Generation */
    @Resource(name="plIdGnrService")
    private EgovIdGnrService egovIdGnrService;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	public List selectCampusSchedule(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.selectCampusSchedule(vo);
	}

	public void insertCampusSchedule(ScheduleMngVO vo) throws Exception {
		schedulebaseDAO.insertCampusSchedule(vo);
	}
	
	//캠퍼스시간표 일괄저장
	public void insertCampusScheduleAll(ScheduleMngVO vo) throws Exception{
		//초기화
		schedulebaseDAO.deleteCampusSchedule(vo);
		
		EgovMap paramMap = new EgovMap();
    	paramMap.put("campusId", vo.getCampusId());

    	EgovMap schMap;
    	List<EgovMap> schList = new ArrayList<EgovMap>();

    	for(int i=1; i<=12; i++){
    		schMap = new EgovMap();
    		schMap.put("period", i);
    		schMap.put("startTime", vo.getStartTimeHHList().get(i-1) + "" + vo.getStartTimeMMList().get(i-1));
    		schMap.put("endTime", vo.getEndTimeHHList().get(i-1) + "" + vo.getEndTimeMMList().get(i-1));
    		
    		schList.add(schMap);
    	}
    	paramMap.put("schList", schList);
    	
    	schedulebaseDAO.insertCampusScheduleAll(paramMap);
	}
	
	public int deleteCampusSchedule(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.deleteCampusSchedule(vo);
	}

	public List selectClassSchedule(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.selectClassSchedule(vo);
	}

	public void insertClassSchedule(ScheduleMngVO vo) throws Exception {
		/** ID Generation Service */
    	String id = egovIdGnrService.getNextStringId();
    	vo.setPlId(id);

    	//교육과정 시간표
    	if(!EgovStringUtil.isEmpty(vo.getCrclId())){
    		vo.setEndDt(vo.getStartDt());
    	}

		schedulebaseDAO.insertClassSchedule(vo);

		//교육과정 시간표 - 교원
    	if(!EgovStringUtil.isEmpty(vo.getCrclId())){

    		int cnt = 0;
    		//초기화
    		schedulebaseDAO.deleteFaculty(vo);
    		if(vo.getFacIdList() != null){
    			for(int i = 0; i < vo.getFacIdList().size(); i++){
    				vo.setFacId(vo.getFacIdList().get(i));
        			if(cnt == 0){
        				//첫번째가 담당교원
        				vo.setManageCode("06");
        				cnt++;
        			}else{
        				vo.setManageCode("04");
        			}
        			schedulebaseDAO.insertFaculty(vo);
    			}
    		}
    	}
	}

	public ScheduleMngVO selectClassScheduleView(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.selectClassScheduleView(vo);
	}

	public void updateClassSchedule(ScheduleMngVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//교육과정 시간표
    	if(!EgovStringUtil.isEmpty(vo.getCrclId())){
    		vo.setEndDt(vo.getStartDt());
    	}

    	//첨부파일
    	FileVO fvo = new FileVO();
    	fvo.setAtchFileId(vo.getAtchFileId());
		fvo.setFileGroupId(vo.getFileGroupId());
		String atchFileId = fileMngService.insertFileInfsByTemp(fvo, request, response).getAtchFileId();
		vo.setAtchFileId(atchFileId);

		//참고자료
		if("Y".equals(vo.getFileDelChk())){
			schedulebaseDAO.deleteStudyFile(vo);
		}
    	if(vo.getRefeFileList() != null && vo.getRefeFileList().size() > 0){
    		for(int i = 0; i < vo.getRefeFileList().size(); i++){
        		vo.setStreFileNm(vo.getRefeFileList().get(i));
        		schedulebaseDAO.insertStudyFile(vo);
        	}
    	}

		schedulebaseDAO.updateClassSchedule(vo);

		//교육과정 시간표 - 교원
    	if("Y".equals(vo.getChangeFacAt())){

    		int cnt = 0;
    		//초기화
    		schedulebaseDAO.deleteFaculty(vo);
    		if(vo.getFacIdList() != null){
    			//중복제거
    			List<ScheduleMngVO> mngList = new ArrayList<ScheduleMngVO>();
    			
    			
    			for(int i = 0; i < vo.getFacIdList().size(); i++){
    				vo.setFacId(vo.getFacIdList().get(i));
        			if(cnt == 0){
        				//첫번째가 담당교원
        				vo.setManageCode("06");
        				cnt++;
        			}else{
        				vo.setManageCode("04");
        			}
        			schedulebaseDAO.insertFaculty(vo);
    			}
    		}
    	}

    	//비강의식 - 평가 선택 시 등록
    	if("CTG_0000000000000054".equals(vo.getCourseId())){
    		//초기화
    		schedulebaseDAO.deleteEvaluation(vo);

    		if(vo.getEvtIdList() != null){
    			for(int i = 0; i < vo.getEvtIdList().size(); i++){
    				vo.setEvtId(vo.getEvtIdList().get(i));
        			schedulebaseDAO.insertEvaluation(vo);
    			}
    		}
    	}
	}

	public void deleteClassSchedule(ScheduleMngVO vo) throws Exception {
		//교육과정 시간표 - 교원
    	if(!EgovStringUtil.isEmpty(vo.getCrclId())){
    		schedulebaseDAO.deleteFaculty(vo);
    	}

		schedulebaseDAO.deleteClassSchedule(vo);
	}

	//시간표 교원 삭제
	public void deleteFaculty(ScheduleMngVO vo) throws Exception {
		schedulebaseDAO.deleteFaculty(vo);
	}

	//담당교원목록
	public List selectFacultyList(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.selectFacultyList(vo);
	}
	
	//과정 담당교원
    public List selectCrclFacultyList(ScheduleMngVO vo) throws Exception {
        return schedulebaseDAO.selectCrclFacultyList(vo);
    }
    
    //과정 담당교원 카운트
    public List selectCrclFacultyListCnt(ScheduleMngVO vo) throws Exception {
        return schedulebaseDAO.selectCrclFacultyListCnt(vo);
    }

	//과정시간표
    public List selectCrclSchedule(ScheduleMngVO vo) throws Exception {
    	return schedulebaseDAO.selectCrclSchedule(vo);
    }

    //과정시간표 총 개수
    public int selectCrclScheduleTotCnt(ScheduleMngVO searchVO) {
		return schedulebaseDAO.selectCrclScheduleTotCnt(searchVO);
	}

    public void updateCalendarSchedule(ScheduleMngVO vo) throws Exception {
    	schedulebaseDAO.updateCalendarSchedule(vo);
	}

    public ScheduleMngVO selectCalendarScheduleView(ScheduleMngVO vo) throws Exception {
		return schedulebaseDAO.selectCalendarScheduleView(vo);
	}

    //과정수업 수정
    public void updateStudyPlan(ScheduleMngVO vo) throws Exception{
    	//교원변경 시
    	if("Y".equals(vo.getChangeFacAt())){
    		int cnt = 0;
    		//초기화
    		schedulebaseDAO.deleteFaculty(vo);
    		if(vo.getFacIdList() != null){
    			for(int i = 0; i < vo.getFacIdList().size(); i++){
    				vo.setFacId(vo.getFacIdList().get(i));
        			if(cnt == 0){
        				//첫번째가 담당교원
        				vo.setManageCode("06");
        				cnt++;
        			}else{
        				vo.setManageCode("04");
        			}
        			schedulebaseDAO.insertFaculty(vo);
    			}
    		}
    	}

    	//수업평가
		schedulebaseDAO.deleteEvaluation(vo);
		if(vo.getEvtIdList() != null){
			for(int i = 0; i < vo.getEvtIdList().size(); i++){
				vo.setEvtId(vo.getEvtIdList().get(i));
    			schedulebaseDAO.insertEvaluation(vo);
			}
		}

    	schedulebaseDAO.updateClassSchedule(vo);
    }

    //등록 된 평가 목록
    public List selectStudyEvtList(ScheduleMngVO vo) throws Exception{
    	return schedulebaseDAO.selectStudyEvtList(vo);
    }

    //등록 된 평가 목록
    public List selectStudyFileList(ScheduleMngVO vo) throws Exception{
    	return schedulebaseDAO.selectStudyFileList(vo);
    }

    //수업 출석 정보 조회
    public EgovMap selectScheduleAttention(ScheduleMngVO vo) throws Exception{
    	return schedulebaseDAO.selectScheduleAttention(vo);
    }
}
