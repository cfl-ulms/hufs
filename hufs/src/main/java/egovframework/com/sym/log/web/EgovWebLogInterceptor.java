package egovframework.com.sym.log.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import egovframework.com.cmm.service.Globals;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.log.service.EgovLogManageService;
import egovframework.com.sym.log.service.WebLog;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name : EgovWebLogInterceptor.java
 * @Description : 웹로그 생성을 위한 인터셉터 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 9.   이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 9.
 * @version
 * @see
 *
 */
public class EgovWebLogInterceptor extends HandlerInterceptorAdapter {

	@Resource(name="EgovLogManageService")
	private EgovLogManageService logManageService;
		
	@Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	String[] disableUrlList = new String[]
	            {
					"/mng/",
					"/validator.do", 
					"/msi/tmplatHead.do",
					"/msi/tmplatBottom.do",
					"/cmm/fms/",
					"/cop/sns/",
					"/uss/ion/pwm/openPopupManage.do"
				};
	
	String[] sslRedirectList = new String[]
			{
					
				"/uat/uia/egovLoginUsr.do",
				"/uss/umt/cmm/EgovSelectMber.do",
				"/uss/umt/cmm/EgovStplatCnfirmMber.do",
				"/uat/uia/egovIdSearchView.do",
				"/uat/uia/egovPasswordSearchView.do",
				"/uss/umt/cmm/EgovUserConfirmView.do",
				"/uss/umt/cmm/EgovUserConfirmView.do",
				"/rsv/"
			};
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		
		try {
			boolean accessAlrow = true;
			String messageCode = "fail.auth.login";
			String reqURL = request.getRequestURI();
			if(reqURL.endsWith(".do")) {
			
				int SE_CODE = -1;
				SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
				LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
				if(user != null) {
					//SE_CODE = Integer.parseInt(user.getUserSeCode());
					SE_CODE = Integer.parseInt(user.getUserSe());
					
					if(SE_CODE <= 10) {
						
						//사이트 관리자이면...
						if(SE_CODE == 10) {
							user.setUserSe(siteVO.getSiteId().equals(user.getSiteId())? user.getUserSeCode() : "02");
						}
						/*
						//교사이면...
						if(SE_CODE == 8) {
							//인증서 등록이 없으면 일반인..
							if(EgovStringUtil.isEmpty(user.getDn())) {
								user.setUserSe("02");
								SE_CODE = 2;
							}
						}
						*/
					}
				}
				
				if(reqURL.startsWith("/cop/cmy")) {
					if(user == null) {
						accessAlrow = false;
					}
				} else if(reqURL.startsWith("/mng")) {
					accessAlrow = false;
					messageCode = "fail.auth.nomatch";
					//String message = "권한이 없습니다.";
					if(user != null) {
						if(SE_CODE >= 10) {
							/*
							if(user.getAccesAppnIp().indexOf(EgovClntInfo.getClntIP(request)) == -1) {
								message = "접근할수 있는 IP가 아닙니다.";
							} else {
								accessAlrow = true;
							}*/
							accessAlrow = true;
						}
					}
				}
			}
			
			if(!accessAlrow) {
				response.sendRedirect("/?messageCode=" + messageCode);
				return false;
			}
			
			if("Y".equals(Globals.SSL_AT)) {
				if("http".equals(request.getScheme()) && isExistsSsl(reqURL)) {
					String queryString = "";
					if(!EgovStringUtil.isEmpty(request.getQueryString())) {
						queryString = "?" + request.getQueryString();
					}
					response.sendRedirect("https://" + request.getServerName() + reqURL + queryString);
					return false;
				}
			}
			
			return true;

		} catch(NumberFormatException ex) {
			return false;
		} catch(Exception ex) {
			return false;
		}
		
	}
	
	/**
	 * 웹 로그정보를 생성한다.
	 * 
	 * @param HttpServletRequest request, HttpServletResponse response, Object handler 
	 * @return 
	 * @throws Exception 
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler, ModelAndView modeAndView) throws Exception {
		
		String reqURL = request.getRequestURI();
		if(reqURL.endsWith(".do")) {
		
			SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
			LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
			
			try {
				String webLogWrite = (String)EgovSessionCookieUtil.getSessionAttribute(request, "webLogWrite");
				if(!"Y".equals(webLogWrite)) {
					String menuId = request.getParameter("menuId");
					String cmmntyId = request.getParameter("cmmntyId");
					
					if(EgovStringUtil.isEmpty(menuId)) {
						menuId = cmmntyId;
						
						if("/index.do".equals(reqURL)) {
							menuId = "99999999990000000000";
						}
					}
								
					//if(("/".equals(reqURL) || reqURL.endsWith(".do")) && !isExistsDeny(reqURL)) {
					if(!EgovStringUtil.isEmpty(menuId)) {
						
						String siteId = request.getParameter("siteId");
						String sysTyCode = "";			
						
						if(EgovStringUtil.isEmpty(siteId)) {
							siteId = siteVO.getSiteId();
							sysTyCode = siteVO.getSysTyCode();
						}
						
						String userId = "";
						String mobileAt = "N";
						
						WebLog webLog = new WebLog();
						
				    	if(user != null) {
							userId = user.getId();
				    	} else {
				    		userId = request.getSession().getId();
				    	}
				
				    	if(egovframework.com.utl.fcc.service.EgovHttpUtil.getIsMobile(request)) {
				    		//reqURL = EgovStringUtil.replace(reqURL, "/hpg/", "/mbl/");
				    		mobileAt = "Y";
				    		
				    	}
				    	
				    	webLog.setSiteId(siteId);
				    	webLog.setSysTyCode(sysTyCode);
				    	webLog.setMenuId(menuId);
						webLog.setUrl(reqURL);
						webLog.setRqesterId(userId);
						webLog.setRqesterIp(EgovClntInfo.getClntIP(request));
						webLog.setMobileAt(mobileAt);
						
						logManageService.logInsertWebLog(webLog);
						
						EgovSessionCookieUtil.setSessionAttribute(request, "webLogWrite", "Y");
					}
				}
			
			} catch(NumberFormatException ex) {
			} catch(Exception ex) {
				
			}
		}
		
		
	}
	
	/*
	private boolean isExistsDeny(String url) {
		boolean isExists = false;
		for(int i=0; i < disableUrlList.length; i++) {
			if(url.startsWith(disableUrlList[i])) {
				isExists = true;
				break;
			}
		}
		
		return isExists;
	}
	*/
	private boolean isExistsSsl(String url) {
		boolean isExists = false;
		for(int i=0; i < sslRedirectList.length; i++) {
			if(url.startsWith(sslRedirectList[i])) {
				isExists = true;
				break;
			}
		}
		
		return isExists;
	}
}
