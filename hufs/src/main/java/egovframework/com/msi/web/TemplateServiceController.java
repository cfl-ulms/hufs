package egovframework.com.msi.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.service.Globals;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.mpm.service.Mpm;
import egovframework.com.sym.mpm.service.MpmVO;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.ion.bnr.service.BannerVO;
import egovframework.com.uss.ion.bnr.service.EgovBannerService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/******************************************************
 * @Class Name : EmtMainTemplateController.java
 * @Program name : egovframework.com.msi.web
 * @Descriptopn :
 * @version : 1.0.0
 * @author : 이호영
 * @created date : 2011. 7. 26.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2012. 7. 26.        이호영             first generated
 * 2012. 9. 01.        문동열
*********************************************************/

@Controller
public class TemplateServiceController {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovMpmService")
	private EgovMpmService egovMpmService;

	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "curriculumService")
    private CurriculumService curriculumService;

	@Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;

	@Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
	
	@Resource(name = "egovBannerService")
	EgovBannerService egovBannerService;
	
	@RequestMapping(value = "/index.do")
	public String index(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user != null){
			int SE_CODE = Integer.parseInt(user.getUserSe());
			if(SE_CODE > 8){
				return "redirect:/mng/index.do";
			}
		}
		//사진경로
		model.addAttribute("MembersFileStoreWebPath", propertyService.getString("Members.fileStoreWebPath"));

		//메인페이지 여부
		model.addAttribute("isMain", "Y");

		//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//배너 수
  		BannerVO bannerVO = new BannerVO();
	    bannerVO.setSiteId(siteVO.getSiteId());
	    bannerVO.setSysTyCode(siteVO.getSysTyCode());
	    bannerVO.setServiceAt("Y");
  		int bannerServiceCnt = egovBannerService.selectBannerListTotCnt(bannerVO);
  		model.addAttribute("bannerServiceCnt", bannerServiceCnt);
		return "msi/cntntsService";
	}

	@RequestMapping(value = "/msi/tmplatHead.do")
	public String tmplatUpper(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);

	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    //개인파일 웹경로
	    model.addAttribute("MembersFileStoreWebPath", propertyService.getString("Members.fileStoreWebPath"));

		model.addAttribute("contentLineAt", request.getParameter("contentLineAt"));

	    /** EgovPropertyService.SiteList */
		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(request, response, siteVO, mnuVO, model, true, true);

		if("Y".equals(request.getParameter("isMain"))){
			//나의 교육과정 개수 조회
	        int myCurriculumCnt = 0;
			CurriculumVO currVO = new CurriculumVO();
			//currVO.setStudentPageAt("Y");
			currVO.setStudentPageAt("curriculumAllList");
			currVO.setFirstIndex(0);
			currVO.setLastIndex(8);
			currVO.setRecordCountPerPage(8);

			LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
			if(user != null){
				if("08".equals(user.getUserSeCode())) {
					currVO.setMyCurriculumPageFlag("teacher");
					currVO.setUserId(user.getId());
					currVO.setStudentPageAt("N");
					currVO.setLastIndex(6);
					currVO.setRecordCountPerPage(6);
				}
				
					// 학습자료
					 BoardVO boardVO = new BoardVO();
					  boardVO.setFirstIndex(0);
					  boardVO.setRecordCountPerPage(4);
					  boardVO.setCrclId(null);
					  boardVO.setIsMainFlag("Y");
					  boardVO.setSysTyCode("ALL");
					  boardVO.setBbsAttrbCode("BBSA02");

					  List<BoardVO> resultList = bbsMngService.selectBoardArticles(boardVO);


					  model.addAttribute("crsBbsList", resultList);

					  // 나의 교육과정
					  SimpleDateFormat sDate = new SimpleDateFormat("yyyy-MM-dd");
					  String today = sDate.format(new Date());
					  CurriculumVO myCurriculumVO = new CurriculumVO();
					  myCurriculumVO.setFacId(user.getId());
					  myCurriculumVO.setStartDate(today);
					  myCurriculumVO.setIsMainFlag("Y");
					  String pageFlag = "";
					  	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
					  		pageFlag = "student";
				  		} else { //교원
				  			pageFlag = "teacher";
				  		}
					  myCurriculumVO.setMyCurriculumPageFlag(pageFlag);
					  List<?> tmpTodayCrclList = curriculumService.selectTodayCrclList(myCurriculumVO);
					  List<EgovMap> todayCrclList = (List<EgovMap>) tmpTodayCrclList;
						for(int i = 0; i < tmpTodayCrclList.size(); i++){
							EgovMap map = (EgovMap) tmpTodayCrclList.get(i);
							String crclId = map.get("crclId").toString();
							String plId = map.get("plId").toString();
							ScheduleMngVO sVO = new ScheduleMngVO();
							sVO.setCrclId(crclId);
							sVO.setPlId(plId);

							todayCrclList.get(i).put("facPlList", scheduleMngService.selectFacultyList(sVO));
						}

					  model.addAttribute("myCurriculumList", todayCrclList);
					  model.addAttribute("crclDay", myCurriculumVO.getStartDate());
					  
					  SimpleDateFormat sDate2 = new SimpleDateFormat("yyyy년 MM월 dd일");
					  today = sDate2.format(new Date());
					  model.addAttribute("today", today); // 오늘 날짜
				      String dayNm = EgovDateUtil.getDateDay(EgovDateUtil.getToday());
				      model.addAttribute("dayNm", dayNm); // 요일
			}

			List<?> curriculumList = curriculumService.selectCurriculumList(currVO, request, response);
			List<EgovMap> crclList = (List<EgovMap>) curriculumList;
			for(int i = 0; i < curriculumList.size(); i++){
				EgovMap map = (EgovMap) curriculumList.get(i);
				String crclId = map.get("crclId").toString();
				currVO.setCrclId(crclId);

				crclList.get(i).put("facList", curriculumService.selectCurriculumFacultyDp(currVO));
			}
	        model.addAttribute("resultList", crclList);

	        currVO.setSearchProcessSttusCodeDate("7");
	        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(currVO, request, response);

	        model.addAttribute("myCurriculumCnt", myCurriculumCnt);

	        //캠퍼스
	        Ctgry ctgry = new Ctgry();
	        ctgry.setCtgrymasterId("CTGMST_0000000000017");
	        model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

	        ctgry.setCtgrymasterId("CTGMST_0000000000002");
			model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
			
			//과제 조회
			if(user != null) {
				CurriculumVO curriculumVO = new CurriculumVO();
				curriculumVO.setUserId(user.getId());
				curriculumVO.setFirstIndex(0);
				curriculumVO.setRecordCountPerPage(3);
				curriculumVO.setIsMainFlag("Y");
				
				//학생일 경우 자신의 과정에 과제만 나와야 하므로 sql 분기를 위한 처리
				if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
					curriculumVO.setMyCurriculumPageFlag("student");
		  		} else {
		  			curriculumVO.setMyCurriculumPageFlag("teacher");
		  		}

		  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(curriculumVO);
		  		model.addAttribute("selectHomeworkList", selectHomeworkList);
			}
		}
		return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile")
				+ "sit/"
				+ siteVO.getLytSourcId()
				+ "/sourcHead"
				+ (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");
	}


	@RequestMapping(value = "/msi/tmplatBottom.do")
	public String tmplatBottom(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(request, response, siteVO, mnuVO, model, true, false);

	    return propertyService.getString("publish.sourc.lyt.fileStoreWebPathByJspFile")
	    		+ "sit/"
	    		+ siteVO.getLytSourcId()
	    		+ "/sourcBottom"
	    		+ (EgovHttpUtil.getIsMobile(request) && "Y".equals(siteVO.getMobileUseAt()) ? Globals.PUBLISH_MOBILE_APPEND_FREFIX : "");
	}

	@RequestMapping(value = "/msi/cntntsService.do")
	public String cntnsService(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/cntntsService";
	}


	@RequestMapping(value = "/msi/siteMap.do")
	public String siteMap(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    mnuVO.setSiteId(siteVO.getSiteId());
		List<Mpm> mpmList = egovMpmService.selectMpmServiceList(mnuVO);

		model.addAttribute("mnMnuList", mpmList);

		return "msi/siteMap";

	}

	@RequestMapping(value = "/msi/indvdlInfoPolicy.do")
	public String indvdlInfoPolicy(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/indvdlInfoPolicy");

		return "msi/cntntsService";

	}

	@RequestMapping(value = "/msi/useStplat.do")
	public String useStplat(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/useStplat");

	    return "msi/cntntsService";

	}

	@RequestMapping(value = "/msi/emailColctPolicy.do")
	public String emailColctPolicy(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/emailColctPolicy");

		return "msi/cntntsService";

	}

	@RequestMapping(value = "/msi/cpyrhtSttemntSvc.do")
	public String cpyrhtSttemntSvc(MpmVO mnuVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    model.addAttribute("ImportUrl", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/cpyrhtSttemntSvc");

		return "msi/cntntsService";

	}
	
	//수강신청 약관 내용
    @RequestMapping("/msi/terms.do")
    public String terms(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);

		return "/str/mnu/" + siteVO.getSiteId() + "/terms";
    }
	
	private void modelMpmDataBinding(HttpServletRequest request, HttpServletResponse response, SiteManageVO siteVO, MpmVO mnuVO, ModelMap model, boolean publishBinding, boolean mainContentsBinding) throws Exception {
		List<Mpm> mpmList = egovMpmService.selectMpmServiceList(mnuVO);
		model.addAttribute("mpmList", mpmList);

		Mpm currMpm = egovMpmService.selectMpmCurrent(mpmList, mnuVO);

		//미리보기일 경우
		if("Y".equals(mnuVO.getPreviewYn())) {
			if(currMpm != null) {
				mnuVO.setMenuPathByName(currMpm.getMenuPathByName());
				mnuVO.setMenuPathById(currMpm.getMenuPathById());
				mnuVO.setMenuLevel(currMpm.getMenuLevel());
				mnuVO.setMenuLastNodeAt(currMpm.getMenuLastNodeAt());
			} else {
				Mpm uppperMpm = egovMpmService.selectMpmFind(mpmList, mnuVO.getUpperMenuId());
				if(uppperMpm != null) {
					mnuVO.setMenuPathByName(uppperMpm.getMenuPathByName() + ">" + mnuVO.getMenuNm());
					mnuVO.setMenuPathById(uppperMpm.getMenuPathById() + ">" + mnuVO.getMenuId());
					mnuVO.setMenuLevel(uppperMpm.getMenuLevel() + 1);
					mnuVO.setMenuLastNodeAt("Y");
				}
			}
			currMpm = mnuVO;
		}

		if(mainContentsBinding && "Y".equals(mnuVO.getIsMain())) {
	    	if(siteVO.getMainContentsList() != null) {
	    		Mpm progrmMpm = null;
		    	for(int i = 0; i < siteVO.getMainContentsList().size(); i ++) {
		    		progrmMpm = egovMpmService.selectMpmProgram(mpmList, siteVO.getMainContentsList().get(i).getProgrmId());
		    		if(progrmMpm != null) {
		    			siteVO.getMainContentsList().get(i).setMenuId(progrmMpm.getMenuId());
		    		}
		    	}
	    	}

	    }

	    model.addAttribute("currMpm", currMpm);

	    Mpm rootMpm = egovMpmService.selectMpmCurrentRoot(mpmList, currMpm);
	    model.addAttribute("currRootMpm", rootMpm);

	    if(rootMpm != null){
	    	if("MNU_0000000000000082".equals(rootMpm.getMenuId())){
	    		  SimpleDateFormat sDate = new SimpleDateFormat("yyyy-MM-dd");
				  String today = sDate.format(new Date());
				  ScheduleMngVO sVO = new ScheduleMngVO();
				  sVO.setStartDt(today);
				  model.addAttribute("scheduleCnt", scheduleMngService.selectCrclScheduleTotCnt(sVO)); // 오늘 수업 건수

				  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
				  CurriculumVO myCurriculumVO = new CurriculumVO();

			      	//학생, 교원  페이지 분기 처리
		  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
		  			myCurriculumVO.setStudentPageAt("Y");
		  			myCurriculumVO.setMyCurriculumPageFlag("student");
		  			myCurriculumVO.setSttus("1");
		  		} else { //교원
		  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
		  		}

		  		//나의 교육과정 개수 조회
		  		myCurriculumVO.setSearchProcessSttusCodeDate("7");
		      	myCurriculumVO.setUserId(user.getId());
		        int myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
		        model.addAttribute("myCurriculumCnt", myCurriculumCnt);
			}
	    }

	    if(publishBinding) {
	    	model.addAttribute("MenuFileStoreWebPath", propertyService.getString("Menu.fileStoreWebPath"));

		    model.addAttribute("MnuFileStoreWebPathByWebFile", propertyService.getString("publish.mnu.fileStoreWebPathByWebFile"));
		    model.addAttribute("MnuFileStoreWebPathByJspFile", propertyService.getString("publish.mnu.fileStoreWebPathByJspFile"));

		    model.addAttribute("LytFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.lyt.fileStoreWebPathByWebFile"));
		    model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));
	    }
	}

	@RequestMapping(value = "/msi/cmm/tmplatHead.do")
	public String cmmTmplatHead(UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		//SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		//model.addAttribute("siteInfo", siteVO);
		SiteManageVO siteVO = null;
    	if(EgovStringUtil.isEmpty(userManageVO.getSiteId())) {
        	siteVO = siteManageService.selectSiteServiceInfo(request);
        } else  {
        	siteVO = siteManageService.selectSiteServiceInfoBySiteId(userManageVO.getSiteId());
        }

    	if(siteVO != null) {
    		model.addAttribute("siteInfo", siteVO);
    	}

    	MpmVO mnuVO = new MpmVO();
    	//사이트설정정보
	    /** EgovPropertyService.SiteList */
		mnuVO.setSiteId(siteVO.getSiteId());
		this.modelMpmDataBinding(request, response, siteVO, mnuVO, model, true, true);

	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/cmm/tmplatHead";
	}


	@RequestMapping(value = "/msi/cmm/tmplatBottom.do")
	public String cmmTmplatBottom(UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		//SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    //model.addAttribute("siteInfo", siteVO);
		SiteManageVO siteVO = null;
    	if(EgovStringUtil.isEmpty(userManageVO.getSiteId())) {
        	siteVO = siteManageService.selectSiteServiceInfo(request);
        } else  {
        	siteVO = siteManageService.selectSiteServiceInfoBySiteId(userManageVO.getSiteId());
        }

    	if(siteVO != null) {
    		model.addAttribute("siteInfo", siteVO);
    	}
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/cmm/tmplatBottom";
	}

	@RequestMapping(value = "/msi/sch/tmplatHead.do")
	public String schTmplatHead(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/sch/tmplatHead";
	}


	@RequestMapping(value = "/msi/sch/tmplatBottom.do")
	public String schTmplatBottom(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
	    model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

		return "msi/sch/tmplatBottom";
	}
	
	@RequestMapping(value = "/msi/introduce/introduce.do")
	public String introduce(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		//사이트설정정보
		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
		model.addAttribute("siteInfo", siteVO);
	    //사이트 설정 웹경로.
	    model.addAttribute("SiteFileStoreWebPath", propertyService.getString("Site.fileStoreWebPath"));

	    //개인파일 웹경로
	    model.addAttribute("MembersFileStoreWebPath", propertyService.getString("Members.fileStoreWebPath"));
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		model.addAttribute("USER_INFO", user);
		
		String url ="";
		url = request.getParameter("menuId");
		
		return "str/mnu/SITE_000000000000001/"+url;
	}
}
