package egovframework.com.auth.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.google.gson.Gson;

import egovframework.com.auth.service.NaverLoginService;
import egovframework.com.auth.service.NaverUser;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import net.sf.json.JSONObject;


@Controller
public class AuthController {

	@Resource(name = "naverLoginService")
    private NaverLoginService naverLoginService;

    @Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;

    protected static final Log LOG = LogFactory.getLog(AuthController.class);
    
    //네이버웨일 return auth
  	@RequestMapping(value="/auth/naverLogin.do")
  	public String naverLogin(@ModelAttribute("loginVO") LoginVO loginVO,HttpServletRequest request,HttpServletResponse response,ModelMap model, HttpSession session)throws Exception {
  		/* 네이버아이디로 인증 URL을 생성하기 위하여 getAuthorizationUrl메소드 호출 */
        String naverAuthUrl = naverLoginService.getAuthorizationUrl(session);
        
        //네이버 
        model.addAttribute("url", naverAuthUrl);
        
        //return "dataeum/front/test/naverLogin";
        return "redirect:"+naverAuthUrl;
  	}
    
    //네이버웨일 return auth
	@RequestMapping(value="/auth/login.do")
	public String login(@ModelAttribute("loginVO") LoginVO loginVO, @RequestParam String code, @RequestParam String state, HttpSession session, HttpServletRequest request,HttpServletResponse response,ModelMap model)throws Exception {
		System.out.println("여기는 callback");
        OAuth2AccessToken oauthToken;
        oauthToken = naverLoginService.getAccessToken(session, code, state);
        
        //로그인 사용자 정보를 읽어온다.
        String apiResult = naverLoginService.getUserProfile(oauthToken);
        System.out.println(apiResult);

        Gson gson = new Gson();
        
        NaverUser user = gson.fromJson(apiResult, NaverUser.class);
        if(user.getPrimaryEmail() != null) {
			System.out.println("1. sessionId = " + session.getId());
			System.out.println("result =====> " + user.toString());
			
			session.setAttribute("naverUser", user);
			model.addAttribute("token", oauthToken.getAccessToken());
        }
        
        model.addAttribute("result", apiResult);
        
        return "auth/naverSuccess";
	}
	
	//네이버웨일 로그인 url생성
	@RequestMapping(value="/auth/whaleAuth.do")
	public void whaleAuth(@ModelAttribute("loginVO") LoginVO loginVO,HttpServletRequest request,HttpServletResponse response,ModelMap model, HttpSession session)throws Exception {
		//response.setHeader("Access-Control-Allow-Origin", "*");
		
		String successYn = "Y";
		String naverAuthUrl = naverLoginService.getAuthorizationUrl(session);
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("url", naverAuthUrl);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	@RequestMapping(value = "/auth/myinfo.do")
	public void myinfo(String token, HttpServletRequest request,HttpServletResponse response) throws Exception {
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		OAuth2AccessToken searchToken = new OAuth2AccessToken(token);
		String apiResult = naverLoginService.getUserProfile(searchToken);
		
		String successYn = "Y";
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
  		jo.put("successYn", successYn);
		jo.put("items", apiResult);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
		
	}
	
	@RequestMapping(value = "/auth/myinfo2.do")
	public void myinfo(HttpServletRequest request,HttpServletResponse response) throws Exception {
		String successYn = "Y";
		String apiResult = "{\"primaryEmail\":\"tea01@gne.go.kr\",\"sid\":\"Ybzi5lxu7sa8AMhfPUm9Yu2urfxIfT4ZsVaPFvK6QEoP91u2jOn7RU6DKfpdaZu2\",\"userType\":\"tea\",\"name\":{\"givenName\":\"선생\",\"familyName\":\"고\",\"fullName\":\"선생 고\"},\"thumbnailPhotoUrl\":null,\"space\":{\"name\":\"한컴초등학교\",\"code\":\"HANCOM\"}}";
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("items", apiResult);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//네이버웨일 return auth
  	@RequestMapping(value="/auth/test.do")
  	public String test(@ModelAttribute("loginVO") LoginVO loginVO,HttpServletRequest request,HttpServletResponse response,ModelMap model, HttpSession session)throws Exception {
  		
        return "/auth/test";
  	}
	
}