package egovframework.com.auth.service;

public class NaverUserName {

	private String givenName;
	
	private String familyName;
	
	private String fullName;

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "NaverUserName [givenName=" + givenName + ", familyName=" + familyName + ", fullName=" + fullName + "]";
	}
	
	
	
}
