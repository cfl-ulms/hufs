package egovframework.com.auth.service;

public class NaverSpace {

	String name;
	
	String code;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "NaverSpace [name=" + name + ", code=" + code + "]";
	}
	
	
}
