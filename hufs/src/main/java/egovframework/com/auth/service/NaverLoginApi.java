package egovframework.com.auth.service;

import com.github.scribejava.core.builder.api.DefaultApi20;

public class NaverLoginApi extends DefaultApi20 {
	protected NaverLoginApi(){
    }

    private static class InstanceHolder{
        private static final NaverLoginApi INSTANCE = new NaverLoginApi();
    }

    public static NaverLoginApi instance(){
        return InstanceHolder.INSTANCE;
    }
    
    @Override
    public String getAccessTokenEndpoint() {
        //return "https://nid.naver.com/oauth2.0/token?grant_type=authorization_code";
    	return "https://auth.whalespace.io/oauth2/v1/token?grant_type=authorization_code";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        // TODO Auto-generated method stub
        //return"https://nid.naver.com/oauth2.0/authorize";
    	return"https://auth.whalespace.io/oauth2/v1/authorize";
    }
    
}
