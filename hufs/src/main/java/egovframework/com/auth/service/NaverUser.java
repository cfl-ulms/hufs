package egovframework.com.auth.service;

public class NaverUser {
	
	private String primaryEmail;
	
	private String sid;
	
	private String userType;
	
	private NaverUserName name;
	
	private String thumbnailPhotoUrl;
	
	private NaverSpace space;

	public String getPrimaryEmail() {
		return primaryEmail;
	}

	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public NaverUserName getName() {
		return name;
	}

	public void setName(NaverUserName name) {
		this.name = name;
	}

	public String getThumbnailPhotoUrl() {
		return thumbnailPhotoUrl;
	}

	public void setThumbnailPhotoUrl(String thumbnailPhotoUrl) {
		this.thumbnailPhotoUrl = thumbnailPhotoUrl;
	}

	public NaverSpace getSpace() {
		return space;
	}

	public void setSpace(NaverSpace space) {
		this.space = space;
	}

	@Override
	public String toString() {
		return "NaverUser [primaryEmail=" + primaryEmail + ", sid=" + sid + ", userType=" + userType + ", name=" + name.toString()
				+ ", thumbnailPhotoUrl=" + thumbnailPhotoUrl + ", space=" + space.toString() + "]";
	}
	
	
}
