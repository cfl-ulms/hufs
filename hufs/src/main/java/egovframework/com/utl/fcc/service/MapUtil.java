package egovframework.com.utl.fcc.service;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

public class MapUtil {

	public static int maxKeyLength(Map<?, ?> map) {
		String key = null;
		Set keySet = map.keySet();
		Iterator keyIter = keySet.iterator();
		int preLength = 0;
		boolean curLength = false;

		while(keyIter.hasNext()) {
			key = keyIter.next().toString();
			int curLength1 = key.length();
			if(preLength < curLength1) {
				preLength = curLength1;
			}
		}

		return preLength;
	}

	public static void mergeMap(Map targetMap, Map addMap) {
		if(targetMap != null && addMap != null) {
			Iterator iter = null;
			Map.Entry entry = null;

			
			iter = addMap.entrySet().iterator();

			while(iter.hasNext()) {
				entry = (Map.Entry)iter.next();
				targetMap.put(entry.getKey(), entry.getValue());
			}
			

		}
	}
	
	public static String isMapNullCheck(Map<String,Object> paramMap,String key){
		String returnValue = "";
		if(paramMap == null || paramMap.get(key) == null){
			returnValue =  "";
		}else{
			returnValue =paramMap.get(key).toString();
		}
		return returnValue;
	}
	
	public static String isEgovMapNullCheck(EgovMap paramMap, String key){
		String returnValue = "";		
		if(paramMap == null || paramMap.get(key) == null) {
			returnValue =  "";
		}else {
			returnValue = paramMap.get(key).toString();
		}
		return returnValue;
	}
	
	public static String[] stringConvert(Map<String,Object> paramMap,String key){
		String[] returnValue;
		if(paramMap.get(key) instanceof String[]){
			returnValue = (String[]) paramMap.get(key);
		}else{
			returnValue = new String[1];
			returnValue[0] = isMapNullCheck(paramMap, key);
		}
		return returnValue;
	}
	
	
	/*public static Map<String,Object> sessionInfo(HttpServletRequest request,Map<String,Object> paramMap){
		
		HttpSession session = request.getSession();
		EgovMap sessionMap = (EgovMap)session.getAttribute("SESSION_MAP");
		if(sessionMap != null){
			Set<Entry<String, Object>> entrySet = sessionMap.entrySet();
			Iterator<Entry<String, Object>> iterator = entrySet.iterator();
			while(iterator.hasNext()) {
				Map.Entry<java.lang.String, java.lang.Object> entry = (Map.Entry<java.lang.String, java.lang.Object>)iterator.next();
				// sessionMap에 담긴 key가 null 값을 가질 때 공백("") 처리
				// paramMap.put(entry.getKey(),entry.getValue().toString());
				String entryKey = entry.getKey();
				Object entryValue = entry.getValue();
				paramMap.put(entryKey, entryValue != null ? entryValue.toString() : "");
			}			
		}
		return paramMap;
	}*/
	
	public static Map<String,Object> sessionInfo(HttpServletRequest request,HttpServletResponse response, Map<String,Object> paramMap){
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		/*
		if(user != null){
			paramMap.put("sessionMid", user.getId());
			paramMap.put("schoolno", user.getSchulId());	
			paramMap.put("amemschool", user.getSchulNm());
			paramMap.put("gradecheck", user.getStGrade());
			paramMap.put("amemgrade2", user.getStClass());		
			
			paramMap.put("creativeCheck", user.getCreativeCheck());			
			paramMap.put("creativeGrade1", user.getCreativeGrade1());
			paramMap.put("creativeGrade2", user.getCreativeGrade2());
			paramMap.put("creativeGrade3", user.getCreativeGrade3());
			paramMap.put("creativeGrade4", user.getCreativeGrade4());
			paramMap.put("creativeGrade5", user.getCreativeGrade5());
			paramMap.put("creativeGrade6", user.getCreativeGrade6());
			
			
		}
		*/
		return paramMap;
	}
	
	
	
	public static String sessionInfoValue(HttpServletRequest request,HttpServletResponse response, Map<String,Object> paramMap,String value){
		Map<String,Object> sessionMap = sessionInfo(request,response,paramMap);
		return sessionMap.get(value).toString();
	}
	
	public static EgovMap unicodeConvert(EgovMap returnMap){
		if(returnMap != null){
			Set<EgovMap> entrySet = returnMap.entrySet();
			Iterator<EgovMap> iterator = entrySet.iterator();
			while(iterator.hasNext()) {
				Map.Entry<java.lang.String, java.lang.Object> entry = (Map.Entry<java.lang.String, java.lang.Object>)iterator.next();
				String valueConvert = entry.getValue().toString();
				String keyConvert = entry.getKey().toString();
				returnMap.put(keyConvert,valueConvert);
			}			
		}
		return returnMap;
	}
}

