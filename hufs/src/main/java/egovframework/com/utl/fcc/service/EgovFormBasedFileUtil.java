package egovframework.com.utl.fcc.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

/**
 * @Class Name  : EgovFormBasedFileUtil.java
 * @Description : Form-based File Upload 유틸리티
 * @Modification Information
 * 
 *     수정일         수정자                   수정내용
 *     -------          --------        ---------------------------
 *   2009.08.26       한성곤                  최초 생성
 *
 * @author 공통컴포넌트 개발팀 한성곤
 * @since 2009.08.26
 * @version 1.0
 * @see 
 */
public class EgovFormBasedFileUtil {
    /** Buffer size */
    public static final int BUFFER_SIZE = 8192;
    
    public static final String seperator = File.separator;
    /**
     * 오늘 날짜 문자열 취득. 
     * ex) 20090101
     * @return
     */
    public static String getTodayString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		return format.format(new Date());
    }
    
    /**
     * 물리적 파일명 생성.
     * @return
     */
    public static String getPhysicalFileName() {
    	return EgovFormBasedUUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }
    
    /**
     * 파일명 변환.
     * @param filename String 
     * @return
     * @throws Exception
     */
    protected static String convert(String filename) throws Exception {
		//return java.net.URLEncoder.encode(filename, "utf-8");
		return filename;
    }
    
    /**
     * 파일을 읽음.
     * @param String filePath
     * @throws IOException
     */
    public static String readFile(String filePath) throws Exception {
    
    	File file = null;
	    file = new File(filePath);
	    if(!file.exists()) {
	    	return "";
	    }
		
	    StringBuffer buff = new StringBuffer();
		Scanner sc = null;
		
		try {
			sc = new Scanner(file, "UTF-8");
		    while(sc.hasNextLine()) {
		    	buff.append(sc.nextLine());
		    	buff.append("\n");
		    }
		    
		} finally {
			if (sc != null) {
				sc.close();
			}
		}
	    
	    return buff.toString();
    }
    
    /**
     * 파일을 저장함.
     * @param String filePath
     * @param String cn
     * @throws IOException
     */
    public static long saveFile(String filePath, String cn) throws Exception {
    
    	if(cn == null)
    		return 0;
    	
	    File file = null;
	    file = new File(filePath);
	    
		// 디렉토리 생성
		if (! file.getParentFile().exists()) {
		    file.getParentFile().mkdirs();
		}
		
		OutputStream os = null;
		
		try {
			
		    os = new FileOutputStream(file);	    
		    os.write(cn.getBytes("UTF-8"));
		} finally {
		    if (os != null) {
		    	os.close();
		    }
		}
		
		return file.length();
    }
    
    /**
     * 파일을 삭제함.
     * @param String filePath
     * @param String cn
     * @throws IOException
     */
    public static boolean deleteFile(String filePath) throws Exception {
    
	    File file = null;
	    file = new File(filePath);
	    
	    if(file.exists()) {
	    	return file.delete();
	    } else {
	    	return false;
	    }
    
    }
    
    /**
     * 파일명을 변경.
     * @param String filePath
     * @param String cn
     * @throws IOException
     */
    public static boolean renameFile(String filePath, String fileName, String fileReame) throws Exception {
    
	    File file = null;
	    file = new File(filePath, fileName);
	    
	    if(file.exists()) {
	    	return file.renameTo(new File(filePath, fileReame));
	    } else {
	    	return false;
	    }
    
    }
    
    /**
     * Stream으로부터 파일을 저장함.
     * @param is InputStream
     * @param file File
     * @throws IOException
     */
    public static long saveFile(InputStream is, File file, boolean append) throws Exception {
		// 디렉토리 생성
		if (! file.getParentFile().exists()) {
		    file.getParentFile().mkdirs();
		}
			
		OutputStream os = null;
		long size = 0L;
		
		try {
		    os = new FileOutputStream(file, append);
		    
		    int bytesRead = 0;
		    byte[] buffer = new byte[BUFFER_SIZE];
		    
		    while ((bytesRead = is.read(buffer, 0, BUFFER_SIZE)) != -1) {
			size += bytesRead;
			os.write(buffer, 0, bytesRead);
		    }
		} finally {
		    if (os != null) {
			os.close();
		    }
		}
		
		return size;
    }
    
    
    
    
    
    /**
     * 이미지에 대한 미리보기 기능을 제공한다.
     * 
     * mimeType의 경우는 JSP 상에서 다음과 같이 얻을 수 있다.
     * getServletConfig().getServletContext().getMimeType(name);
     * 
     * @param response
     * @param where
     * @param serverSubPath
     * @param physicalName
     * @param mimeType
     * @throws Exception
     */
    public static void viewFile(HttpServletResponse response, String where, String serverSubPath, String physicalName, String mimeType) throws Exception {
		String downFileName = where + seperator + serverSubPath + seperator + physicalName;
		    
		File file = new File(downFileName);
	
		if (!file.exists()) {
		    throw new FileNotFoundException(downFileName);
		}
	
		if (!file.isFile()) {
		    throw new FileNotFoundException(downFileName);
		}
	
		byte[] b = new byte[BUFFER_SIZE];
	
		if (mimeType == null) {
		    mimeType = "application/octet-stream;";
		}
	
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", "filename=image;");
	
		BufferedInputStream fin = null;
		BufferedOutputStream outs = null;
		
		try {
		    fin = new BufferedInputStream(new FileInputStream(file));
		    outs = new BufferedOutputStream(response.getOutputStream());
		    
		    int read = 0;
	
		    while ((read = fin.read(b)) != -1) {
			outs.write(b, 0, read);
		    }
		} finally {
		    if (outs != null) {
			outs.close();
		    }
		    
		    if (fin != null) {
			fin.close();
		    }
		}
    }
}
