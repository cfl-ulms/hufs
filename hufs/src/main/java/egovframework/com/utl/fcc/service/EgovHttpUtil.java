package egovframework.com.utl.fcc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import egovframework.rte.fdl.property.EgovPropertyService;

@Component("EgovHttpUtil")
public class EgovHttpUtil {

	@Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
		

	public static void setIsMobile(HttpServletRequest request) {
		request.getSession().setAttribute("IS_MOBILE", true);
	}
	
	public static void removeIsMobile(HttpServletRequest request) {
		if(request.getSession().getAttribute("IS_MOBILE") != null) {
			request.getSession().removeAttribute("IS_MOBILE");
		}
	}
	
	public static void setIsMobileCancled(HttpServletRequest request) {
		request.getSession().setAttribute("IS_MOBILE_CANCLE", true);
	}
	
	public static void removeIsMobileCancled(HttpServletRequest request) {
		if(request.getSession().getAttribute("IS_MOBILE_CANCLE") != null) {
			request.getSession().removeAttribute("IS_MOBILE_CANCLE");
		}
	}
	
	public static boolean getIsMobile(HttpServletRequest request) {
		boolean isMobile = false;
		if(request.getSession().getAttribute("IS_MOBILE") != null) {
			isMobile = true;//(Boolean)request.getSession().getAttribute("IS_MOBILE");
		}
		if(!isMobile) {
			if(checkMobileHeader(request) && request.getSession().getAttribute("IS_MOBILE_CANCLE") == null) {
				isMobile = true;
			}
		}
	
		return isMobile;		
	}
	
	public static boolean checkMobileHeader(HttpServletRequest request) {
		boolean isMobile = false;
		String[] chkList = {"IPHONE","IPOD","IPAD","ITOUCH","MOBILE","IEMOBILE"};//,"ANDROID"};
		String user_agent = request.getHeader("user-agent");
		if(user_agent != null) {
			String br_info = user_agent.toUpperCase();
			
			for(int i=0; i<chkList.length; i++) {
				if(br_info.indexOf(chkList[i]) != -1) {
					isMobile = true;
					break;
				}
			}
		}
		
		return isMobile;
	}
	
	
	/**
	 * 모바일 종류를 리턴 by moon 2011.12.13
	 * @param request
	 * @return
	 */
	public static String getMobileType(HttpServletRequest request) {
		String result = "NONE";
		String[] chkList = {"IPHONE","IPOD","IPAD","ITOUCH","MOBILE","IEMOBILE"};//,"ANDROID"};
		String user_agent = request.getHeader("user-agent");
		if(user_agent != null) {
			String br_info = user_agent.toUpperCase();
			for(int i=0; i<chkList.length; i++) {
				if(br_info.indexOf(chkList[i]) != -1) {
					result = chkList[i];
					break;
				}
			}
		}
		return result;
	}
	
	
	
	public static String getEstnParseData(HttpServletRequest request) {
		Gson gson = new Gson();
    	JsonObject jObj = new JsonObject();
	    Enumeration en = request.getParameterNames();
	    while(en.hasMoreElements()) {
	    	String str = (String)en.nextElement();
	    	if(str.startsWith("estn")) {
	    		jObj.addProperty(str, request.getParameter(str));
	    	}
	    }
	    if(!jObj.isJsonNull()) {
	    	return gson.toJson(jObj);
	    }
	    return null;
	}
}
