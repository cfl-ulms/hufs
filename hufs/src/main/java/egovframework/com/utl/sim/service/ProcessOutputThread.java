package egovframework.com.utl.sim.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class ProcessOutputThread extends Thread {

	Logger log = Logger.getLogger(this.getClass());
	
	private InputStream is;
	private StringBuffer msg;
	
	public ProcessOutputThread(InputStream is, StringBuffer msg) {
		this.is = is;
		this.msg = msg;
	}
	
	public void run() {
		msg.append(getStreamString(is));
	}
	
	private String getStreamString(InputStream is) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(is));
			StringBuffer out = new StringBuffer();
			String stdLine;
			while((stdLine = reader.readLine()) != null) {
				out.append(stdLine);
			}
			return out.toString().trim();
		} catch(IOException e) {
			log.debug("IOException: "+ e.getMessage());
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException ignore) {
					log.debug("IGNORED: "+ ignore.getMessage());
				}
			}
			if(is != null) {
				try {
					is.close();
				} catch(IOException ignore) {
					log.debug("IGNORED: "+ ignore.getMessage());
				}
			}
		}
		
		return "";
	}
}
