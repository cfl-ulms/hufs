package egovframework.com.uat.uia.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.annotation.Resource;









import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.adobe.xmp.impl.Base64;
import com.oreilly.servlet.Base64Encoder;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.mpm.service.Mpm;
import egovframework.com.sym.mpm.service.MpmVO;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageDefaultVO;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.com.utl.fcc.service.EgovSsoSecureEncryptUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovFileScrty;
import egovframework.com.cmm.service.Globals;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.cmy.service.EgovCommunityManageService;
import egovframework.com.ems.service.DirectMailService;
import egovframework.com.ems.service.MailMessageVO;


/**
 * 일반 로그인, 인증서 로그인을 처리하는 컨트롤러 클래스
 * @author 공통서비스 개발팀 박지욱
 * @since 2009.03.06
 * @version 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  2009.03.06  박지욱          최초 생성 
 *  
 *  </pre>
 */
@Controller
public class EgovLoginController {

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	@Resource(name = "loginService")
    private EgovLoginService loginService;

    @Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;

    @Resource(name = "userManageService")
    private EgovUserManageService userManageService;
    
    @Resource(name = "EgovMpmService")
	private EgovMpmService egovMpmService;
    
    @Resource(name = "EgovCommunityManageService")
    private EgovCommunityManageService 	cmmntyService;
    
    @Resource(name = "directMailService")
	private DirectMailService directMailService;
    
    protected static final Log LOG = LogFactory.getLog(EgovLoginController.class);
    
    /**
	 * 로그인 화면으로 들어간다
	 * @param vo - 로그인후 이동할 URL이 담긴 LoginVO
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/egovLoginUsr.do")
	public String loginUsrView(@ModelAttribute("loginVO") LoginVO loginVO,
			HttpServletRequest request,
			HttpServletResponse response,
			ModelMap model) 
			throws Exception {
		
    	if(loginVO.getUrl() != null){
    		EgovSessionCookieUtil.setSessionAttribute(request, "returnUrl", loginVO.getUrl());
    	}
    	EgovSessionCookieUtil.setSessionAttribute(request, "returnUrl", loginVO.getUrl());
    	return "cmm/uat/uia/EgovLoginUsr";
    	
	}

    /**
	 * 일반 로그인을 처리한다
	 * @param vo - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
    @RequestMapping(value="/uat/uia/actionLogin.do")
    public String actionLogin(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, ModelMap model)throws Exception {
    	
    	// 접속IP
    	//String userIp = EgovClntInfo.getClntIP(request);
    	boolean loginSuccess = false;
    	LoginVO resultVO = null;
    	if (loginVO.getId() != null && loginVO.getPassword() != null){
    		// 1. 일반 로그인 처리    		
        	UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
        	if(userVO != null) {
				resultVO = loginService.actionLogin(loginVO);
				if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {	//로그인 성공
					loginSuccess = true;
				}    			
    		}
    	}

        if (loginSuccess) {
        	resultVO.setUserSeCode(resultVO.getUserSe());
        	EgovSessionCookieUtil.setSessionAttribute(request, "isMasterLogin", "Y");
        	EgovSessionCookieUtil.setSessionAttribute(request, "loginVO", resultVO);
        	return "redirect:/uat/uia/actionMain.do?siteId=" + loginVO.getSiteId();
        	
        	// 2. spring security 연동
            //return "redirect:/j_spring_security_check?j_username=" + resultVO.getId() + "&j_password=" + resultVO.getPassword();
        } else {
        	model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	model.addAttribute("SessionID", request.getSession().getId());
        	
        	return "cmm/uat/uia/EgovLoginUsr";
        }
    }
    
    /**
	 * 모바일앱 로그인 사전처리를한다
	 * @param vo - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
    /*
    @RequestMapping(value="/uat/uia/appLogin.do")
    public String appLogin(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, ModelMap model)throws Exception {
    	
    	// 접속IP
    	//String userIp = EgovClntInfo.getClntIP(request);

    	boolean loginSuccess = false;
    	LoginVO resultVO = null;

    	if (loginVO.getId() != null && loginVO.getPassword() != null){
    		// 1. 일반 로그인 처리
        	UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
        	if(userVO != null) {

				resultVO = loginService.actionLogin(loginVO);
	
				if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {	//로그인 성공
					loginSuccess = true;
				}    			
    		}
    	}

    	model.addAttribute("AuthStep", "LOGIN_BEFORE");
        if (loginSuccess) {        	
        	SiteManageDefaultVO siteVO = new SiteManageDefaultVO();
        	siteVO.setSiteId(resultVO.getSiteId());
        	model.addAttribute("siteInfo", siteManageService.selectSiteSimpleInfo(siteVO));        	
        	//model.addAttribute("token", EgovSsoSecureEncryptUtil.generateEncyptToken(EgovSsoSecureEncryptUtil.generateEncyptKey(), resultVO.getId()));
        	model.addAttribute("StepResult", "Y");
        	return "cop/svc/EgovAppLoginResultXml";
        } else {
        	model.addAttribute("StepResult", "N");
        	return "cop/svc/EgovAppLoginResultXml";
        }
    }
    */
    
    /**
	 * 모바일앱 로그인을 처리한다
	 * @param vo - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
    /*
    @RequestMapping(value="/uat/uia/actionAppLogin.do")
    public String actionAppLogin(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, ModelMap model)throws Exception {
    	
    	// 접속IP
    	//String userIp = EgovClntInfo.getClntIP(request);

    	boolean loginSuccess = false;
    	LoginVO resultVO = null;

    	if (loginVO.getId() != null && loginVO.getPassword() != null){
    		// 1. 일반 로그인 처리
        	UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
        	if(userVO != null) {

				resultVO = loginService.actionLogin(loginVO);
	
				if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {	//로그인 성공
					loginSuccess = true;
				}    			
    		}
    	}

        if (loginSuccess) {
    	  	// 2. spring security 연동
        	EgovSessionCookieUtil.setSessionAttribute(request, "isAppLogin", "Y");
            return "redirect:/j_spring_security_check?j_username=" + resultVO.getId() + "&j_password=" + EgovFileScrty.encode(resultVO.getPassword());
        } else {
        	model.addAttribute("AuthStep", "LOGIN");
        	model.addAttribute("StepResult", "N");
        	return "cop/svc/EgovAppLoginResultXml";
        }
    }
     */
    /**
	 * 인증서 로그인을 처리한다
	 * @param vo - 주민번호가 담긴 LoginVO
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
    @RequestMapping(value="/uat/uia/actionCrtfctLogin.do")
    public String actionCrtfctLogin(@ModelAttribute("loginVO") LoginVO loginVO, 
    		HttpServletRequest request,
    		HttpServletResponse response,
			ModelMap model)
            throws Exception {
    	
    	LoginVO resultVO = null;

    	loginVO.setDn(null);    	
    	resultVO = loginService.actionCrtfctLogin(loginVO);
		    	
		if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {

        	//System.out.println("login ok ------- spring go :" + "redirect:/j_spring_security_check?j_username=" + resultVO.getId() + "&j_password=" + EgovFileScrty.encode(resultVO.getPassword()));
            // 2. spring security 연동
			EgovSessionCookieUtil.setSessionAttribute(request, "isMasterLogin", "Y");
            return "redirect:/j_spring_security_check?j_username=" + resultVO.getId() + "&j_password=" + resultVO.getPassword();
    		
        } else {
        	//System.out.println("login fail : " + loginVO.getId() + "/" + EgovFileScrty.encode(loginVO.getPassword()));
        	
        	String sessionID 		= request.getSession().getId();
    		String strServerCert 	= "";
    		
    		model.addAttribute("serverCert", strServerCert);
    		model.addAttribute("sessionId", sessionID);
    		
        	model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	model.addAttribute("isDevelopmentMode", propertyService.getBoolean("Globals.isDevelopmentMode"));
        	return "cmm/uat/uia/EgovLoginUsr";
        }
    }
    
    /**
	 * 로그인 후 메인화면으로 들어간다
	 * @param 
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/actionMain.do")
	public String actionMain(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user == null) {
      	  	model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "forward:/uat/uia/egovLoginUsr.do";
  	  	}
		/*
    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(request, response);
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "forward:/uat/uia/egovLoginUsr.do";
    	}
    	*/
		
    	String returnUrl = (String)EgovSessionCookieUtil.getSessionAttribute(request, "returnUrl");
    	String main_page = Globals.MAIN_PAGE;
    	
    	/*SiteManageVO siteVO = siteManageService.selectSiteServiceInfoBySiteId(user.getSiteId());*/
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    	if(siteVO != null) {
    		main_page = "http://" + siteVO.getSiteUrl() + "/";
    	}
    	//main_page = main_page + "?token=" + user.getSsoToken();//Globals.MAIN_PAGE;
    	
    	String modelUrl = main_page;
    	//관리자 로그인 시 관리자 페이지로 이동
    	if(Integer.parseInt(user.getUserSeCode()) >= 10){
    		modelUrl =  "redirect:" + main_page + "mng/index.do";
    	}else if(returnUrl == null || "".equals(returnUrl)){
    		modelUrl =  "redirect:" + main_page;
    	}else{
    		modelUrl = "redirect:" + returnUrl;
    	}
    	
    	EgovSessionCookieUtil.removeSessionAttribute(request, "returnUrl");
    	
    	return modelUrl;
	}
    
    /**
	 * 로그아웃한다.
	 * @return String
	 * @exception Exception
	 */
    @RequestMapping(value="/uat/uia/actionLogout.do")
	public String actionLogout(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	request.getSession().invalidate();
    	return "redirect:/";
    	// 1. Security 연동
    	//return "redirect:/j_spring_security_logout";
    }
    
    /**
	 * 로그아웃한다.
	 * @return String
	 * @exception Exception
	 */
    @RequestMapping(value="/uat/uia/ssoActionLogout.do")
	public void ssoActionLogout(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	
    	//request.getSession().invalidate();
    }
    
    /**
	 * 로그아웃 후 메인화면으로 들어간다
	 * @param 
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/actionLogoutMain.do")
	public String actionLogoutMain(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    	
    	String returnUrl = (String)EgovSessionCookieUtil.getSessionAttribute(request, "returnUrl");
    	String main_page = Globals.MAIN_PAGE;

    	String modelUrl = main_page;
    	if(returnUrl == null || "".equals(returnUrl)){
    		if (main_page.startsWith("/")) {
    			modelUrl =  "redirect:" + main_page;
    		}
    	}else{
    		modelUrl = "redirect:" + returnUrl;
    	}
    	
    	EgovSessionCookieUtil.removeSessionAttribute(request, "returnUrl");
    	
    	return modelUrl;
	}
	
	/**
	 * 로그인 정보를 조회한다.
	 * @param 
	 * @exception Exception
	 */
	@RequestMapping(value = "/uat/uia/selectLoginInfo.do")
	  public void manageArticle(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String token = "";
		String loginYn = "N";
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user != null) {
			token = user.getSsoToken();
			loginYn = "Y";
		}
		JSONObject jo = new JSONObject();
		jo.put("token", token);
		jo.put("loginYn", loginYn);
		  
		response.setContentType("text/javascript; charset=utf-8");
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	  }
	
	/**
	 * 인증서안내 화면으로 들어간다
	 * @return 인증서안내 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/egovGpkiIssu.do")
	public String gpkiIssuView(ModelMap model) throws Exception {
		return "cmm/uat/uia/EgovGpkiIssu";
	}
	
	/**
	 * 아이디 찾기 인증화면
	 * @param 
	 * @return 아이디/비밀번호 찾기 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/egovIdSearchView.do")
	public String egovIdSearch(HttpServletRequest request, ModelMap model) throws Exception {

		return "cmm/uat/uia/EgovIdSearch";
	}
		
	/**
	 * 아이디를 찾는다.
	 * @param 
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/egovIdSearch.do")
	public String egovIdSearch(@ModelAttribute("searchVO") LoginVO loginVO, HttpServletRequest request, ModelMap model) throws Exception {

        LoginVO resultVO = loginService.searchId(loginVO);

        if (resultVO != null && resultVO.getId() != null && !resultVO.getId().equals("")) {
        	loginVO.setId(resultVO.getId());
        	loginVO.setName(resultVO.getName());
        	loginVO.setMobileNo(resultVO.getMobileNo());
        	loginVO.setEmail(resultVO.getEmail());
        	
        	return "cmm/uat/uia/EgovIdSearchComplete";
        } else {
        	model.addAttribute("message", egovMessageSource.getMessage("fail.common.idsearch"));
        	
        	return "cmm/uat/uia/EgovIdSearchComplete";
        }
	}


	/**
	 * 비밀번호 찾기 인증화면
	 * @param 
	 * @return 비밀번호 찾기 페이지
	 * @exception Exception
	 */
	@RequestMapping(value="/uat/uia/egovPasswordSearchView.do")
	public String egovPasswordSearch(@ModelAttribute("searchVO") LoginVO loginVO, HttpServletRequest request, ModelMap model) throws Exception {
		
		if(!EgovStringUtil.isEmpty(loginVO.getUrl())) {
			String userId = Base64.decode(Base64.decode(Base64.decode(loginVO.getUrl())));
			loginVO.setId(userId);
		}
		
		if(!EgovStringUtil.isEmpty(loginVO.getId())) {
			UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
	    	if(userVO != null) {
	    		model.addAttribute("result", userVO);
	    		model.addAttribute("url", loginVO.getUrl());
	    	} else {
	    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.pwsearch"));
	    	}
		}
		return "cmm/uat/uia/ChangePassword";
	}
	
	//비밀번호 설정완료
	@RequestMapping(value="/uat/uia/changePw.do")
	public String changePw(@ModelAttribute("searchVO") LoginVO loginVO, HttpServletRequest request, ModelMap model) throws Exception {
		
		String userId = Base64.decode(Base64.decode(Base64.decode(loginVO.getUrl())));
		loginVO.setId(userId);
		
		if(!EgovStringUtil.isEmpty(loginVO.getId())) {
			UserManageVO userVO = userManageService.selectLoingUser(loginVO.getId());
	    	if(userVO != null) {
	    		userVO.setPassword(loginVO.getPassword());
	    		int cnt = userManageService.updatePassword(userVO);
	    		model.addAttribute("result", userVO);
	    	} else {
	    		//model.addAttribute("message", egovMessageSource.getMessage("fail.common.idsearch"));
	    	}
		}
		return "cmm/uat/uia/ChangePasswordComplete";
	}
	
    /**
	 * 비밀번호를 찾는다.
	 * @param vo - 아이디, 이름, 주민등록번호가 담긴 LoginVO
	 * @return result - 임시비밀번호전송결과
	 * @exception Exception
	 */
    @RequestMapping(value="/uat/uia/egovPasswordSearch.do")
    public String egovPasswordSearch(@ModelAttribute("loginVO") LoginVO loginVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	/*
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	
    	if(user == null){
    		return "cmm/uat/uia/EgovPasswordSearch";
    	}
    	*/
    	// 1. 비밀번호 찾기
    	Map<String, Object> resultList = loginService.searchPassword(loginVO, true);
        boolean result = (Boolean)resultList.get("result");
        
        // 2. 결과 리턴
        if (result) {
        	LoginVO resultVO = (LoginVO)resultList.get("resultVO");
        	//boolean sendResult = (Boolean)resultList.get("sendResult");
        	
        	//메일 인증
            MailMessageVO mailVo = new MailMessageVO();
            mailVo.setSenderName(resultVO.getName());
            mailVo.setSenderEmail(resultVO.getEmail());
            mailVo.setSubject("[한국외국어대학교] 비밀번호 재설정 메일");
            
            String url = Base64Encoder.encode(Base64Encoder.encode(Base64Encoder.encode(resultVO.getId())));
            String contUrl = Globals.DOMAIN + "/uat/uia/egovPasswordSearchView.do?url=" + url;
        	String html = "<table border='0' cellpadding='0' cellspacing='0' style='background-color:#f5f5f5; width:100%; margin:0 auto'>" +
		        		    "<tr>" +
		        		      "<td align='center'>" +
		        		        "<div style='max-width: 600px; padding-top:30px; padding-right:10px; padding-bottom:30px; padding-left:10px; margin:0 auto'>" +
		        		          "<table border='0' cellpadding='0' cellspacing='0' width='100%' style='width:100%; margin:0; padding:0; background-color:#ffffff; color:#333333; -webkit-text-size-adjust:100%;text-align:left; letter-spacing: -0.015em;'>" +
		        		            "<tr>" +
		        		              "<td style=\"padding-top:25px; padding-right:30px; padding-bottom:20px; padding-left:30px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; text-align: center;\" colspan='4'>" +
		        		                "<img src='http://cfl.ac.kr/template/lms/imgs/common/img_header_logo.png' width='206px' height='28px' alt='특수외국어교육진흥사업 한국외국어대학교'>" +
		        		              "</td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td colspan='2' style=\"line-height:1.25; padding-top:35px; padding-bottom:7px; font-family: 'Noto Sans KR', sans-serif; letter-spacing: -0.01em;\">" +
		        		                "<b style='font-size:20px;color:#000000'>비밀번호 재설정</b>" +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td colspan='2' style=\"line-height:1.56; padding-bottom:22px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; font-size:16px; letter-spacing: -0.015em;\">" +
		        		                "특수외국어진흥사업 - 한국외국어대학교 <span style='color:#0038a9'>비밀번호 재설정 안내 입니다.</span>" +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td colspan='2' style=\"padding-top:20px; padding-bottom:22px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; line-height:1.64;\">" +
		        		                "아래 ‘<span style='color:#0038a9'>비밀번호 재설정</span>’ 버튼을 클릭하여 계정의 비밀번호를 재설정 해주세요." +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td width='18' valign='top' style='padding-top:3px;'>" +
		        		                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
		        		              "</td>" +
		        		              "<td style=\"line-height:1.64; padding-bottom:6px; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999\" valign='center'>" +
		        		                "위 링크를 클릭해도 시작되지 않으면 URL을 복사하여 새 브라우저 창에 붙여 넣으세요." +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td width='18' valign='top' style='padding-top:4px;'>" +
		        		                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
		        		              "</td>" +
		        		              "<td style=\"line-height:1.64; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999\" valign='center'>" +
		        		                "요청하지 않았는데 본 메일이 수신되었다면 비밀번호를 재설정하려는 다른 사용자가 이메일 주소를 잘못 입력했을 수 있습니다. 이 경우 본 메일을 무시하시면 됩니다." +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td colspan='2' style='padding-top:36px; text-align: center;'>" +
		        		                "<a href='" + contUrl + "' style=\"display:inline-block; padding-top:13px; padding-right: 65px; padding-bottom:13px; padding-left:65px; background-color:#0038a9; font-family: 'Noto Sans KR', sans-serif; font-size:16px; color:#ffffff; text-decoration:none; text-align:center;\">" +
		        		                  "비밀번호 재설정" +
		        		                "</a>" +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		            "<tr>" +
		        		              "<td width='30'></td>" +
		        		              "<td colspan='2' style=\"line-height:1.53; padding-top:25px; padding-bottom:35px; font-family: 'Noto Sans KR', sans-serif; font-size:13px; color:#666666; text-align: center;\">" +
		        		                "© 2019 Hankuk University of Foreign Studies. All Rights Reserved." +
		        		              "</td>" +
		        		              "<td width='30'></td>" +
		        		            "</tr>" +
		        		          "</table>" +
		        		        "</div>" +
		        		      "</td>" +
		        		    "</tr>" +
		        		  "</table>";
        	
    		mailVo.setContent(html);
            directMailService.sendGMail(mailVo);
            
    		model.addAttribute("resultInfo", resultVO);
        	return "cmm/uat/uia/EgovPasswordSearchComplete";
        } else {
        	//model.addAttribute("message", egovMessageSource.getMessage("fail.common.pwsearch"));
        	return "cmm/uat/uia/EgovPasswordSearch";
        }
    }

}