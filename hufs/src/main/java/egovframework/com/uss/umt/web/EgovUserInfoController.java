package egovframework.com.uss.umt.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.Globals;
import egovframework.com.cmm.util.CryptoARIAUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.EgovLoginService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.com.utl.sim.service.EgovCrypTo;

import org.springmodules.validation.commons.DefaultBeanValidator;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.adobe.xmp.impl.Base64;
import com.oreilly.servlet.Base64Encoder;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.ems.service.DirectMailService;
import egovframework.com.ems.service.MailMessageVO;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;

/******************************************************
 * @Class Name : EgovUserManageController.java
 * @Program name : egovframework.com.uss.umt.web
 * @Descriptopn : 
 * @version : 1.0.0
 * @author : 이호영
 * @created date : 2011. 7. 26.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2011. 7. 26.        이호영             first generated
*********************************************************/

@Controller
public class EgovUserInfoController {

	@Resource(name = "SiteManageService")
	private EgovSiteManageService siteManageService;
	
	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
	
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
    @Resource(name = "userManageService")
    private EgovUserManageService userManageService;
    
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    	
	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
	
	@Resource(name = "directMailService")
	private DirectMailService directMailService;

    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
	private DefaultBeanValidator beanValidator;
    
    @Resource(name = "EgovCmmUseService")
	private EgovCmmUseService     cmmUseService;
    
    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
    @Resource(name = "loginService")
    private EgovLoginService loginService;
    
    @Resource(name = "CryptoARIAUtil")
	private CryptoARIAUtil cryptoARIAUtil;
    
    /**
     * 회원구분 선택
     * @param model 화면모델
     * @return cmm/uss/umt/EgovSelectMber
     * @throws Exception
     */
    @RequestMapping("/uss/umt/cmm/EgovSelectMber.do")
    public String selectMber(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model)
            throws Exception {

        return "cmm/uss/umt/EgovSelectMber";
    }
    
    /**
     * 약관확인
     * @param model 화면모델
     * @return cmm/uss/umt/EgovStplatCnfirm
     * @throws Exception
     */
    @RequestMapping("/uss/umt/cmm/EgovStplatCnfirmMber.do")
    public String stplatCnfirmMber(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model)
            throws Exception {

    	SiteManageVO siteVO = null;    	
    	if(EgovStringUtil.isEmpty(userManageVO.getSiteId())) {
        	siteVO = siteManageService.selectSiteServiceInfo(request);
        } else  {
        	siteVO = siteManageService.selectSiteServiceInfoBySiteId(userManageVO.getSiteId());
        }
    	if(siteVO != null) {
    		model.addAttribute("UseStplatUrl", propertiesService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/useStplat");
    		model.addAttribute("IndvdlInfoPolicyUrl", propertiesService.getString("publish.mnu.fileStoreWebPathByJspFile") + siteVO.getSiteId() + "/indvdlInfoPolicy");
    	}
    	
        return "cmm/uss/umt/EgovStplatCnfirm";
    }
    
    /**
     * 실명인증
     * @param model 화면모델
     * @return cmm/uss/umt/EgovCertificate
     * @throws Exception
     */
    @RequestMapping("/uss/umt/cmm/EgovCertificate.do")
    public String certificate(@ModelAttribute("searchVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model)
            throws Exception {
    	
        return "cmm/uss/umt/EgovCertificate";
    }
    
    /**
     * 사용자등록화면으로 이동한다.
     * @param searchVO 검색조건정보
     * @param request 사용자초기화정보
     * @param model 화면모델
     * @return cmm/uss/umt/EgovUserInsert
     * @throws Exception
     */
	@RequestMapping("/uss/umt/user/EgovUserInsertView.do")
     public String insertUserView(@ModelAttribute("userManageVO") UserManageVO userManageVO, HttpServletRequest request, ModelMap model)throws Exception {
		
		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
		//학교목록
		codeVO.setCodeId("COM041");
		model.addAttribute("schoolList", cmmUseService.selectCmmCodeDetail(codeVO));
		
		//국제전화번호
		codeVO.setCodeId("COM040");
		model.addAttribute("telNumList", cmmUseService.selectCmmCodeDetail(codeVO));
    	
		//인증API서비스ID
		model.addAttribute("serviceId", propertiesService.getString("hufs.user.serviceid"));
		model.addAttribute("authUri", propertiesService.getString("hufs.user.authUri"));
		
        return "cmm/uss/umt/EgovUserInsert";
    }
    
    /**
     * 사용자등록처리후 메인화면으로 이동한다.
     * @param userManageVO 사용자등록정보
     * @param bindingResult 입력값검증용 bindingResult
     * @param model 화면모델
     * @return forward:/uss/umt/user/EgovUserManage.do
     * @throws Exception
     */
	@RequestMapping("/uss/umt/user/EgovUserInsert.do")
    public String insertUser(@ModelAttribute("userManageVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request)throws Exception {
    	//학생 아이디
    	userManageVO.setUserId(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
    	if(!EgovStringUtil.isEmpty(userManageVO.getEmail3()) && !EgovStringUtil.isEmpty(userManageVO.getEmail4())) {
        	userManageVO.setEmailAdres(userManageVO.getEmail3() + "@" + userManageVO.getEmail4());
        }
    	if (userManageService.checkIdDplct(userManageVO.getUserId()) > 0) {
    		model.addAttribute("message", egovMessageSource.getMessage("common.isExist.msg"));
    		return "forward:/uss/umt/user/EgovUserInsertView.do";
    	} else {
            if(EgovStringUtil.isEmpty(userManageVO.getSiteId())) {
            	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
            	userManageVO.setSiteId(siteVO.getSiteId());
            }
            
            //외대학생
            if("06".equals(userManageVO.getUserSeCode())){
            	userManageVO.setMajor(userManageVO.getStClass());
            }
            
            // 이메일 인증을 받고나면 Y
            // @police.qo.kr  유형의 메일은 자동 인증되도록 요청
            if(userManageVO.getEmail2().equals("police.go.kr")){
            	userManageVO.setConfmAt("Y");
            } else {
            	userManageVO.setConfmAt("N");
            }
            
            if(userManageVO.getConfmAt().equals("N")) {
            
	            //메일 인증
	            MailMessageVO mailVo = new MailMessageVO();
	            mailVo.setSenderName(userManageVO.getUserNm());
	            mailVo.setSenderEmail(userManageVO.getEmailAdres());
	            mailVo.setSubject("[한국외국어대학교] 회원가입 메일 인증");
	            
	            String url = Base64Encoder.encode(Base64Encoder.encode(Base64Encoder.encode(userManageVO.getUserId())));
	            String contUrl = Globals.DOMAIN + "/uss/umt/mailAuth.do?url=" + url;
	            String html = "<table style='background-color:#f5f5f5; width:100%; margin:0 auto'>" +
	            					"<tr>" +
	            						"<td align='center'>" +
	            							"<div style='max-width: 600px; padding-top:30px; padding-right:10px; padding-bottom:30px; padding-left:10px; margin:0 auto'>" +
	            								"<table border='0' cellpadding='0' cellspacing='0' width='100%' style='width:100%; background-color:#ffffff; color:#333333; -webkit-text-size-adjust:100%;text-align:left; letter-spacing: -0.015em;'>" +
	            									"<tr>" +
	            										"<td style='padding-top:25px; padding-right:30px; padding-bottom:20px; padding-left:30px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; text-align: center;' colspan='4'>" +
	            											"<img src='http://cfl.ac.kr/template/lms/imgs/common/img_header_logo.png' width='206px' height='28px' alt='특수외국어교육진흥사업 한국외국어대학교'>" +
	            										"</td>" +
	            									"</tr>" +
	            									"<tr>" +
	            										"<td width='30'></td>" +
		            									"<td colspan='2' style='line-height:1.25; padding-top:35px; padding-bottom:7px; font-family: 'Noto Sans KR', sans-serif; letter-spacing: -0.01em;'>" +
		            										"<b style='font-size:20px;color:#000000'>회원가입</b>" +
		            									"</td>" +
		            									"<td width='30'></td>" +
	            									"</tr>" +
		            								"<tr>" +
		            								"<td width='30'></td>" +
		            					              "<td colspan='2' style='line-height:1.56; padding-bottom:22px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; font-size:16px; letter-spacing: -0.015em;'>" +
		            					                "특수외국어진흥사업 - 한국외국어대학교 <span style='color:#0038a9'>회원가입 인증 안내 입니다.</span>" +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
		            					            "<tr>" +
		            					              "<td width='30'></td>" +
		            					              "<td colspan='2' style='padding-top:20px; padding-bottom:22px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; line-height:1.64;'>" +
		            					               " 아래 ‘<span style='color:#0038a9'>회원가입 인증완료</span>’ 버튼을 클릭하여 회원가입을 완료해 주세요." +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
		            					            "<tr>" +
		            					              "<td width='30'></td>" +
		            					              "<td width='18' valign='top' style='padding-top:3px;'>" +
		            					                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
		            					              "</td>" +
		            					              "<td style='line-height:1.64; padding-bottom:6px; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999' valign='center'>" +
		            					                "위 링크를 클릭해도 시작되지 않으면 URL을 복사하여 새 브라우저 창에 붙여 넣으세요." +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
		            					            "<tr>" +
		            					              "<td width='30'></td>" +
		            					              "<td width='18' valign='top' style='padding-top:4px;'>" +
		            					                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
		            					              "</td>" +
		            					              "<td style='line-height:1.64; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999' valign='center'>" +
		            					                "요청하지 않았는데 본 메일이 수신되었다면 다른 사용자가 이메일 주소를 잘못 입력했을 수 있습니다. 이 경우 본 메일을 무시하시면 됩니다." +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
		            					            "<tr>" +
		            					              "<td width='30'></td>" +
		            					              "<td colspan='2' style='padding-top:36px; text-align: center;'>" +
		            					                "<a href='" + contUrl + "' style=\"display:inline-block; padding-top:13px; padding-right: 65px; padding-bottom:13px; padding-left:65px; background-color:#0038a9; font-family:'Noto Sans KR', sans-serif; font-size:16px; color:#ffffff; text-decoration:none; text-align:center;\">회원가입 인증완료</a>" +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
		            					            "<tr>" +
		            					              "<td width='30'></td>" +
		            					              "<td colspan='2' style='line-height:1.53; padding-top:25px; padding-bottom:35px; font-family: 'Noto Sans KR', sans-serif; font-size:13px; color:#666666; text-align: center;'>" +
		            					                "© 2019 Hankuk University of Foreign Studies. All Rights Reserved." +
		            					              "</td>" +
		            					              "<td width='30'></td>" +
		            					            "</tr>" +
	            					            "</table>" +
	            					        "</div>" +
		            					"</td>" +
		            				"</tr>" +
		            			"</table>";
		            									
	            //mailVo.setContent("메일 인증 URL : <a href="+contUrl+" target='_blank'>" + contUrl + "</a>");
	            mailVo.setContent(html);
	            
	             
	            directMailService.sendGMail(mailVo);
            }
            userManageService.insertUser(userManageVO);
            
    		model.addAttribute("emailAdres", userManageVO.getEmailAdres());
    		model.addAttribute("confmAt", userManageVO.getConfmAt());

    		return "cmm/uss/umt/EgovUserInsertComplete";
    	}
    }
    
    /**
     * 입력한 사용자아이디의 중복확인화면 이동
     * @param model 화면모델
     * @return cmm/uss/umt/EgovIdDplctCnfirm
     * @throws Exception
     */
    @RequestMapping(value="/uss/umt/EgovIdDplctCnfirmView.do")
    public String checkIdDplctCnfirmView(ModelMap model)
            throws Exception {
        model.addAttribute("checkId", "");
        model.addAttribute("usedCnt", "-1");
        return "cmm/uss/umt/EgovIdDplctCnfirm";
    }
    
    /**
     * 입력한 사용자아이디의 중복여부를 체크하여 사용가능여부를 확인
     * @param commandMap 파라메터전달용 commandMap
     * @param model 화면모델
     * @return cmm/uss/umt/EgovIdDplctCnfirm
     * @throws Exception
     */
    @RequestMapping(value="/uss/umt/cmm/EgovIdDplctCnfirm.do")
    public String checkIdDplctCnfirm(
    		Map<String, Object> commandMap,
            ModelMap model
            )throws Exception {
        
    	String checkId = (String)commandMap.get("checkId");
    	checkId =  new String(checkId.getBytes("ISO-8859-1"), "UTF-8");
        
    	if (checkId==null || checkId.equals("")) return "forward:/uss/umt/EgovIdDplctCnfirmView.do";
        
        int usedCnt = userManageService.checkIdDplct(checkId);
        model.addAttribute("usedCnt", usedCnt);
        model.addAttribute("checkId", checkId);
        
        return "cmm/uss/umt/EgovIdDplctCnfirm";
    }
    
    /** 마이페이지 > 비밀번호 변경 */
    @RequestMapping("/uss/umt/cmm/changePassword.json")
    public void changePassword(@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	String successYn = "";
        String msg = "";
        
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
        LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		userManageVO.setUserId(loginVO.getId());

        int chkCnt = userManageService.selectCheckPassword(userManageVO);
        if(chkCnt > 0) {
        	userManageVO.setPassword(userManageVO.getPassword2());
        	if(userManageService.updatePassword(userManageVO) > 0){
        		successYn = "Y";
        		msg = "비밀번호가 변경되었습니다.";
        	}else{
        		successYn = "Y";
        		msg = "변경된 데이터 없습니다.";
        	}
        }else{
        	successYn = "N";
        	msg = "현재 비밀번호를 확인해주세요.";
        }

        jo.put("successYn", successYn);
		jo.put("msg", msg);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
    /** 마이페이지 > 회원탈퇴 */
    @RequestMapping("/uss/umt/cmm/leaveUser.json")
    public void leaveUser(@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
        LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		userManageVO.setUserId(loginVO.getId());
		userManageVO.setLastUpdusrId(loginVO.getId());

    	userManageService.deleteUser(userManageVO);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
    /** 마이페이지 > 비밀번호 일치 여부 체크 */
    @RequestMapping("/uss/umt/cmm/chkUserPassword.json")
    public void chkUserPassword(@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	String equalYn = "N";
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
        LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		userManageVO.setUserId(loginVO.getId());
		userManageVO.setPassword(EgovCrypTo.encryptPassword(userManageVO.getPassword()));
		
		if(userManageVO.getPassword().equals(loginVO.getPassword())){
			equalYn = "Y";
		}
		
		jo.put("equalYn", equalYn);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
    /** 마이페이지 > 소속변경  */
    @RequestMapping("/uss/umt/cmm/changeGroup.json")
    public void changeGroup(@ModelAttribute("searchVO") UserDefaultVO searchVO, 
		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
        LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		userManageVO.setUserId(loginVO.getId());
		userManageVO.setLastUpdusrId(loginVO.getId());
		
		userManageService.updateUserGroup(userManageVO);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    /**
     * 개인정보 보호를 위한 페스워드 인증하면으로 이동한다.
     * @param model 화면모델
     * @return cmm/uss/umt/EgovUserConfirm
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserConfirmView.do")
    public String userConfirmView(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

    	LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
    	userManageVO.setUserId(loginVO.getId());
    	
    	model.addAttribute("userManageVO", userManageVO);
      return "cmm/uss/umt/EgovUserConfirm";
    }

	/**
     * 개인정보 보호를 위한 페스워드를 받아 확인한다.
     * @param model 화면모델
     * @return "cmm/uss/umt/EgovUserConfirm"
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserConfirm.do")
    public String userConfirm(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
		
    	//입력한 정보가 맞는지 체크
    	if (userManageService.selectCheckPassword(userManageVO) > 0) {
    		if("secsn".equals(searchVO.getTrgtPge())){
    			//탈퇴화면으로 이동
    			return "forward:/uss/umt/cmm/EgovUserSecsnView.do";
    		}else if("update".equals(searchVO.getTrgtPge())){
    			//사용자정보 수정화면으로 이동
    			return "forward:/uss/umt/cmm/EgovUserUpdateView.do";
    		}else if("password".equals(searchVO.getTrgtPge())){
    			//비밀번호 변경화면으로 이동
    			return "forward:/uss/umt/cmm/EgovUserPasswordUpdateView.do";
    		}
    	}else{
    		model.addAttribute("userManageVO", userManageVO);
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.idpw"));
    		return "cmm/uss/umt/EgovUserConfirm";
    	}
    	
    	return "forward:" + Globals.MAIN_PAGE;
    }

	
	/**
     * 사용자정보 수정 화면으로 이동한다.
     * @param model 화면모델
     * @return cmm/uss/umt/EgovUserUpdate
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserUpdateView.do")
    public String EgovUserSelectUpdtView(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
    	userManageVO.setUserId(loginVO.getId());
    	userManageVO.setUserSeCode(loginVO.getUserSeCode());
    	userManageVO = userManageService.selectUser(userManageVO);
    	
    	model.addAttribute("resultList", userManageService.selectSchool());    	
    	model.addAttribute("userManageVO", userManageVO);
    	
    	//사진경로
		model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

		if("08".equals(loginVO.getUserSeCode())){
			return "redirect:/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000044&userId="+loginVO.getId()+"&userSeCode="+loginVO.getUserSeCode()+"&type=mypage";
		}else{
			return "cmm/uss/umt/EgovUserUpdate";
		}
		
    }
	
	/**
     * 사용자정보 수정 폼 화면으로 이동한다.
     * @param model 화면모델
     * @return cmm/uss/umt/EgovUserUpdate
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserUpdateForm.do")
    public String EgovUserUpdateForm(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
    	userManageVO.setUserId(loginVO.getId());
    	userManageVO.setUserSeCode(loginVO.getUserSeCode());
    	userManageVO = userManageService.selectUser(userManageVO);
    	
    	model.addAttribute("resultList", userManageService.selectSchool());    	
    	model.addAttribute("userManageVO", userManageVO);
    	
    	//국제전화번호
    	ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
		codeVO.setCodeId("COM040");
		model.addAttribute("telNumList", cmmUseService.selectCmmCodeDetail(codeVO));

		//학교목록
		codeVO.setCodeId("COM041");
		model.addAttribute("schoolList", cmmUseService.selectCmmCodeDetail(codeVO));
		
		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//사진경로
		model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
    	
      return "cmm/uss/umt/EgovUserUpdateForm";
    }
	
	/**
     * 사용자정보 수정 처리 한다.
     * @param model 화면모델
     * @return forward:/
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserUpdate.do")
    public String EgovUserUpdate(
    		final MultipartHttpServletRequest multiRequest, 
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, 
    		BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
		
    	List<FileVO> result = null;
        
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
          if(result != null) {
  	    	for(int index=0; index < result.size(); index++) {
  	    		FileVO file = result.get(index);
  	    		if(file.getFormNm().startsWith("user")) {
  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
  	    		}
  	    	}
      	  }
          
          LoginVO resultVO = null;
          loginVO.setFlag("Y");
          resultVO = loginService.actionLogin(loginVO);
          resultVO.setPhotoStreFileNm(userManageVO.getPhotoStreFileNm());
          EgovSessionCookieUtil.setSessionAttribute(request, "loginVO", resultVO);
        }
        
		/*beanValidator.validate(userManageVO, bindingResult);
    	if (bindingResult.hasErrors()){

    		userManageVO.setUserId(loginVO.getId());
    		userManageVO.setUserSeCode(loginVO.getUserSeCode());
    		userManageVO = userManageService.selectUser(userManageVO);
    		
        	return "cmm/uss/umt/EgovUserUpdate";
  		}*/
        
    	
        if(!EgovStringUtil.isEmpty(userManageVO.getEmail1()) && !EgovStringUtil.isEmpty(userManageVO.getEmail2())) {
        	userManageVO.setEmailAdres(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
        }
        
    	userManageVO.setUserId(loginVO.getId());
        userManageVO.setLastUpdusrId(loginVO.getId());
        userManageVO.setUserSeCode(loginVO.getUserSeCode());
		if(userManageService.updateStudent(userManageVO) > 0) {
			model.addAttribute("userManageVO", userManageVO);
			model.addAttribute("result", true);
			model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		}

		return "redirect:/uss/umt/cmm/EgovUserUpdateView.do?menuId=MNU_0000000000000044";
    }
	
	/**
     * 회원탈퇴 회면으로 이동한다.
     * @param model 화면모델
     * @return ivp/mpe/ComtnmberSecsn
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserSecsnView.do")
    public String selectComtnmberSecsn(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
      return "cmm/uss/umt/EgovUserSecsn";
    }
	
	/**
     * 회원탈퇴를 처리 한다.
     * @param model 화면모델
     * @return forward:/uat/uia/actionLogout.do
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserSecsn.do")
    public String updateComtnmberSecsn(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
		userManageVO.setUserId(loginVO.getId());

		if(userManageService.deleteUser(userManageVO) > 0) {
			model.addAttribute("result", true);
			model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		}
		
		return "cmm/uss/umt/EgovUserSecsn";
    }
	
	/**
     * 비밀번호 변경화면으로 이동한다.
     * @param model 화면모델
     * @return cmm/uss/umt/EgovUserPasswordUpdate
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserPasswordUpdateView.do")
    public String userPasswordUpdateView(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
    	return "cmm/uss/umt/EgovUserPasswordUpdate";
    }
	
	/**
     * 비밀번호 변경 한다.
     * @param model 화면모델
     * @return forward:/
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovUserPasswordUpdate.do")
    public String userPasswordUpdate(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		LoginVO loginVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(loginVO == null) {
    		return "redirect:" + EgovUserDetailsHelper.getRedirectLoginUrl();
  	  	}
    	
		userManageVO.setUserId(loginVO.getId());

		if(userManageService.updatePassword(userManageVO) > 0) {
			model.addAttribute("result", true);
			model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		}
		return "cmm/uss/umt/EgovUserPasswordUpdate";
    }
	
	@RequestMapping(value="/uss/umt/cmm/selectSchool")
	public String selectSchool(@RequestParam("stTyCode") String stTyCode, Model model ) throws Exception{
		
		ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
		
		if("SCH01".equals(stTyCode)) {
    		codeVO.setCodeId("CA0014");    	  	
    	}else if("SCH02".equals(stTyCode)) {
    		codeVO.setCodeId("CA0015");    	  	
    	}else if("SCH03".equals(stTyCode)) {
    		codeVO.setCodeId("CA0016");    	  	
    	}
				
		model.addAttribute("schoolList", cmmUseService.selectCmmCodeDetail(codeVO));
		
		return "prg/dat/schoolList";
	}
	
	/**
     * 교원 목록을 조회
     * @param model 화면모델
     * @return mng/usr/EgovMemberList
     * @throws Exception
     */
	@RequestMapping(value = "/uss/umt/cmm/EgovMberManage.do")
	  public String selectEgovMberManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			userManageVO.setSiteId(loginVO.getSiteId());
		}
		
		userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		userManageVO.setPageSize(propertiesService.getInt("pageSize"));
        
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
        paginationInfo.setPageSize(userManageVO.getPageSize());
        
        userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
        userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
        
        userManageVO.setUserSeCode("08");
        
        //팝업 검색
        if("N".equals(userManageVO.getTemplateAt())){
        	 userManageVO.setRecordCountPerPage(Integer.MAX_VALUE);
        	 List resultList = new ArrayList();
        	 //검색어가 있을 시에만 조회가 됨
        	 if(!EgovStringUtil.isEmpty(userManageVO.getSearchUserNm())){
        		 userManageVO.setSearchConfirmAt("Y");
        		 resultList = userManageService.selectUserList(userManageVO);
        	 }
        	 model.addAttribute("resultList", resultList);
        }else{
        	model.addAttribute("resultList", userManageService.selectUserList(userManageVO));
            
            int totCnt = userManageService.selectUserListTotCnt(userManageVO);
            paginationInfo.setTotalRecordCount(totCnt);
            model.addAttribute("paginationInfo", paginationInfo);
            
            ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
    	   	voComCode = new ComDefaultCodeVO();
    	   	// 소속 리스트
        	voComCode.setCodeId("LMS50");
        	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));
        	// 직위
        	voComCode.setCodeId("LMS70");
        	model.addAttribute("positionList", cmmUseService.selectCmmCodeDetail(voComCode));
        	// 재직상태
        	voComCode.setCodeId("LMS40");
        	model.addAttribute("workStatusList", cmmUseService.selectCmmCodeDetail(voComCode));
        	
            // 주관기관
        	Ctgry ctgry = new Ctgry();
            ctgry.setCtgrymasterId("CTGMST_0000000000009");
          	model.addAttribute("deptList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        }
       
		return "usr/EgovMemberList";
	}
	
	/**
	 * 사용자 뷰
	 */
	
	 @RequestMapping(value = "/uss/umt/cmm/EgovMberView.do")
	public String EgovMberView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO,
    		UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{
		 	
		 	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
		 	model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));
		 	
		 	//과정진행상태 - 공통코드(LMS30)
		 	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS30");
        	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("statusComCode", statusComCode);
        	
		 	model.addAttribute("curriculumList", userManageService.selectUserCurriculumList(userManageVO));
		 	
		 	model.addAttribute("mypageFlag", request.getParameter("type"));
		 	
		 	// 세부전공 (언어)
		 	Ctgry ctgry = new Ctgry();
 			ctgry.setCtgrymasterId("CTGMST_0000000000002");
 			model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
 
		 return "usr/EgovMemberView";
	}
	 
	 /**
	 * 내 정보 수정 페이지
	 */
	
	 @RequestMapping(value = "/uss/umt/cmm/EgovMberModify.do")
	public String EgovMberModify(
			@ModelAttribute("searchVO") UserDefaultVO searchVO,
			final MultipartHttpServletRequest multiRequest, 
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		 
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		userManageVO.setLastUpdusrId(user.getId());
    		userManageVO.setUserSeCode(user.getUserSeCode());
    	}
    	
    	userManageVO.setMoblphonNo(userManageVO.getMoblphonNo().replaceAll("-", ""));
    	
    	List<FileVO> result = null;
    	
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
          if(result != null) {
  	    	for(int index=0; index < result.size(); index++) {
  	    		FileVO file = result.get(index);
  	    		if(file.getFormNm().startsWith("user")) {
  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
  	    		}
  	    	}
      	  }
          
          LoginVO resultVO = null;
          user.setFlag("Y");
          resultVO = loginService.actionLogin(user);
          resultVO.setPhotoStreFileNm(userManageVO.getPhotoStreFileNm());
          resultVO.setUserSeCode(resultVO.getUserSe());
          EgovSessionCookieUtil.setSessionAttribute(request, "loginVO", resultVO);
        }
		 
		 userManageService.updateTeacherInfo(userManageVO);
		 
		 return "redirect:/uss/umt/cmm/EgovMberView.do?menuId=MNU_0000000000000044&userId="+userManageVO.getUserId()+"&userSeCode="+userManageVO.getUserSeCode()+"&type=mypage";
	}
	 
	 /**
     * 비밀번호 초기화
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uss/umt/cmm/EgovResetPwd.do")
    public String EgovResetPwd(UserManageVO userManageVO)throws Exception {
			userManageService.updateResetPwd(userManageVO);

	      return "forward:/uss/umt/cmm/EgovMberView.do";
    }
	
	//메일 인증
	@RequestMapping("/uss/umt/mailAuth.do")
    public String mailAuth(@ModelAttribute("userManageVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request)throws Exception {
    	String successYn = "N";
		if(EgovStringUtil.isEmpty(userManageVO.getUrl())){
			model.addAttribute("message", "잘못 된 접근입니다.");
		}else{
			String userId = Base64.decode(Base64.decode(Base64.decode(userManageVO.getUrl())));
			userManageVO.setUserId(userId);
			//승인 처리
			userManageService.updateUserRelis(userManageVO);
			
			successYn = "Y";
		}
		model.addAttribute("successYn", successYn);
		
		return "/cmm/uss/umt/MailAuth";
		
    }
	
	/**
	 * My > 교육과정이력 조회
	 */
	
	 @RequestMapping(value = "/uss/umt/cmm/selectEduHistory.do")
	public String selectEduHistory(
			@ModelAttribute("searchVO") UserDefaultVO searchVO,
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		 CurriculumVO curriculumVO = new CurriculumVO();
		 
		 LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	    	if(user != null){
	    		curriculumVO.setUserId(user.getId());
	    	}
	    
	    List<CurriculumVO> resultList = curriculumService.selectMyCurriculumHistoryList(curriculumVO);
	    model.addAttribute("resultList", resultList);
	    
	    
	    model.addAttribute("resultCnt", curriculumService.selectMyCurriculumHistoryCnt(curriculumVO));
	    
	    List<CurriculumVO> resultListCount = curriculumService.selectMyCurriculumHistoryListCount(curriculumVO);
	    model.addAttribute("resultListCount", resultListCount);
	    
	    //과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
      	voComCode.setCodeId("LMS30");
      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
      	model.addAttribute("statusComCode", statusComCode);
	    	
		 
 
		 return "usr/eduHistory";
	}
	 
	 //이메일 인증메일 발송
	@RequestMapping("/uss/umt/user/authMail.json")
    public void authMail(@ModelAttribute("userManageVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

        //메일 인증
        MailMessageVO mailVo = new MailMessageVO();
        mailVo.setSenderName(userManageVO.getUserNm());
        mailVo.setSenderEmail(userManageVO.getEmailAdres());
        mailVo.setSubject("[한국외국어대학교] 회원가입 메일 인증");
        
        String url = Base64Encoder.encode(Base64Encoder.encode(Base64Encoder.encode(userManageVO.getUserId())));
        String contUrl = Globals.DOMAIN + "/uss/umt/mailAuth.do?url=" + url;
        String html = "<table style='background-color:#f5f5f5; width:100%; margin:0 auto'>" +
        					"<tr>" +
        						"<td align='center'>" +
        							"<div style='max-width: 600px; padding-top:30px; padding-right:10px; padding-bottom:30px; padding-left:10px; margin:0 auto'>" +
        								"<table border='0' cellpadding='0' cellspacing='0' width='100%' style='width:100%; background-color:#ffffff; color:#333333; -webkit-text-size-adjust:100%;text-align:left; letter-spacing: -0.015em;'>" +
        									"<tr>" +
        										"<td style='padding-top:25px; padding-right:30px; padding-bottom:20px; padding-left:30px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; text-align: center;' colspan='4'>" +
        											"<img src='http://cfl.ac.kr/template/lms/imgs/common/img_header_logo.png' width='206px' height='28px' alt='특수외국어교육진흥사업 한국외국어대학교'>" +
        										"</td>" +
        									"</tr>" +
        									"<tr>" +
        										"<td width='30'></td>" +
            									"<td colspan='2' style='line-height:1.25; padding-top:35px; padding-bottom:7px; font-family: 'Noto Sans KR', sans-serif; letter-spacing: -0.01em;'>" +
            										"<b style='font-size:20px;color:#000000'>회원가입</b>" +
            									"</td>" +
            									"<td width='30'></td>" +
        									"</tr>" +
            								"<tr>" +
            								"<td width='30'></td>" +
            					              "<td colspan='2' style='line-height:1.56; padding-bottom:22px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color: #ddd; font-family: 'Noto Sans KR', sans-serif; font-size:16px; letter-spacing: -0.015em;'>" +
            					                "특수외국어진흥사업 - 한국외국어대학교 <span style='color:#0038a9'>회원가입 인증 안내 입니다.</span>" +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
            					            "<tr>" +
            					              "<td width='30'></td>" +
            					              "<td colspan='2' style='padding-top:20px; padding-bottom:22px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; line-height:1.64;'>" +
            					               " 아래 ‘<span style='color:#0038a9'>회원가입 인증완료</span>’ 버튼을 클릭하여 회원가입을 완료해 주세요." +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
            					            "<tr>" +
            					              "<td width='30'></td>" +
            					              "<td width='18' valign='top' style='padding-top:3px;'>" +
            					                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
            					              "</td>" +
            					              "<td style='line-height:1.64; padding-bottom:6px; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999' valign='center'>" +
            					                "위 링크를 클릭해도 시작되지 않으면 URL을 복사하여 새 브라우저 창에 붙여 넣으세요." +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
            					            "<tr>" +
            					              "<td width='30'></td>" +
            					              "<td width='18' valign='top' style='padding-top:4px;'>" +
            					                "<img src='http://cfl.ac.kr/template/lms/imgs/common/email_icon_info.png' width='18px' height='18px' alt='' style='vertical-align: top;'>" +
            					              "</td>" +
            					              "<td style='line-height:1.64; padding-left:10px; font-family: 'Noto Sans KR', sans-serif; font-size:14px; color:#999999' valign='center'>" +
            					                "요청하지 않았는데 본 메일이 수신되었다면 다른 사용자가 이메일 주소를 잘못 입력했을 수 있습니다. 이 경우 본 메일을 무시하시면 됩니다." +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
            					            "<tr>" +
            					              "<td width='30'></td>" +
            					              "<td colspan='2' style='padding-top:36px; text-align: center;'>" +
            					                "<a href='" + contUrl + "' style=\"display:inline-block; padding-top:13px; padding-right: 65px; padding-bottom:13px; padding-left:65px; background-color:#0038a9; font-family:'Noto Sans KR', sans-serif; font-size:16px; color:#ffffff; text-decoration:none; text-align:center;\">회원가입 인증완료</a>" +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
            					            "<tr>" +
            					              "<td width='30'></td>" +
            					              "<td colspan='2' style='line-height:1.53; padding-top:25px; padding-bottom:35px; font-family: 'Noto Sans KR', sans-serif; font-size:13px; color:#666666; text-align: center;'>" +
            					                "© 2019 Hankuk University of Foreign Studies. All Rights Reserved." +
            					              "</td>" +
            					              "<td width='30'></td>" +
            					            "</tr>" +
        					            "</table>" +
        					        "</div>" +
            					"</td>" +
            				"</tr>" +
            			"</table>";
            									
        mailVo.setContent(html);
        
        directMailService.sendGMail(mailVo);
        
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
  		jo.put("successYn", "Y");
  		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
	
	//외대 학생 일괄 업데이트
	@RequestMapping("/uss/umt/user/userUpdateStnum.do")
    public String userUpdateStnum(@ModelAttribute("userManageVO") UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception {
		//인증API서비스ID
		model.addAttribute("serviceId", propertiesService.getString("hufs.user.serviceid"));
		model.addAttribute("authUri", propertiesService.getString("hufs.user.authUri"));
		
		List<EgovMap> resultList = userManageService.selectStuUserList(userManageVO);
		model.addAttribute("resultList", resultList);
		
		return "/cmm/uss/umt/userUpdateStnum";
   }
	
	//외대 학생 일괄 업데이트
	@RequestMapping("/uss/umt/user/updateStuFUser.json")
    public void updateStuFUser(@ModelAttribute("userManageVO") UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception {
		int updateCnt = userManageService.updateStuFUser(userManageVO);
		
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  		
  		jo.put("successYn", "Y");
  		jo.put("updateCnt", updateCnt);
  		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
   }
	
}
