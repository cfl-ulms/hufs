package egovframework.com.uss.umt.service;

import java.io.Serializable;
import java.util.List;
/******************************************************
 * @Class Name : UserManageVO.java
 * @Program name : egovframework.com.uss.umt.service
 * @Descriptopn : 
 * @version : 1.0.0
 * @author : 이호영 
 * @created date : 2011. 7. 25. 
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2011. 7. 25.        이호영             first generated
*********************************************************/

@SuppressWarnings("serial")
public class UserManageVO extends UserDefaultVO implements Serializable {
    
    /** USER_ID */
    private String userId;
    
    /** CREDT_ID */
    private String credtId;
    
    /** USER_SE_CODE */
    private String userSeCode;
    
    /** POSITION_CODE */
    private String positionCode;
    
    /** USER_SE_CODE_NM */
    private String positionCodeNm;
    
    /** PASSWORD */
    private String password;
    
    /** PASSWORD RE */
    private String password2;
    
    /** USER_NM */
    private String userNm;
    
    /** EMAIL_ADRES */
    private String emailAdres;
    
    /** TLPHON_NO */
    private String tlphonNo;
    
    /** MOBLPHON_NO */
    private String moblphonNo;
    
    /** ZIP */
    private String zip;
    
    /** ADRES */
    private String adres;
    
    /** ADRES_DETAIL */
    private String adresDetail;
        
    /** DELETE_AT */
    private String deleteAt;
    
    /** DELETE_PNTTM */
    private java.util.Date deletePnttm;
    
    /** BRTHDY */
    private String brthdy;
    
    /** BRTHDY */
    private String brthdy01;
    
    /** BRTHDY */
    private String brthdy02;
    
    /** BRTHDY */
    private String brthdy03;
    
    /** SLRCLD_LRR_CODE */
    private String slrcldLrrCode;
    
    /** SEXDSTN */
    private String sexdstn;
    
    /** EMAIL_RECPTN_AT */
    private String emailRecptnAt;
    
    /** MOBLPHON_RECPTN_AT */
    private String moblphonRecptnAt;
    
    /** PHOTO_ORIGINAL_FILE_NM */
    private String photoOriginalFileNm;
    
    /** PHOTO_STRE_FILE_NM */
    private String photoStreFileNm;
    
    /** FRST_REGIST_PNTTM */
    private java.util.Date frstRegistPnttm;
    
    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;
        
    /** LAST_UPDUSR_ID */
    private String lastUpdusrId;
    
    /** CONFM_PNTTM */
    private java.util.Date confmPnttm;
    
    /** CONFM_AT */
    private String confmAt;
    
    /** DELETE_RESN */
    private String deleteResn;
    
    /** USER_IHIDNUM */
    private String userIhidnum;
    
    /** SITE_ID */
    private String siteId;
    
    /** MESSAGE */
    private String message;
    
    /** 일련번호 */
    private int no;
        
    
    /** 집전화 앞번호 */
    private String tel1;
    
    /** 집전화 중간번호 */
    private String tel2;
    
    /** 집전화 끝번호 */
    private String tel3;
    
    /** 휴대전화 앞번호 */
    private String phone1;
    
    /** 휴대전화 중간번호 */
    private String phone2;
    
    /** 휴대전화 끝번호 */
    private String phone3;
    
    /** 이메일 계정 */
    private String email1;
    
    /** 이메일 도메인 */
    private String email2;
    
    /** 본인확인email계정 */
    private String email3;
    
    /** 본인확인email 도메인 */
    private String email4;
    
    /** 코드 이름 */
    private String codeNm;
    
    /** 코드 */
    private String code;

    /** 학년 */
    private String stGrade;
    
    /** 반 */
    private String stClass;
    
    /** 번호 */
    private String stNumber;
    
    /* 학교 코드*/
    private String stCode;
 
    /* 학교구분코드 (초,중,고) */
    private String stTyCodeStaff;
    
    /* 교직원 학교 코드*/
    private String stCodeStaff;
    
    /* 학교구분코드 (초,중,고) */
    private String stTyCode;
    
    /* 학교명 */
    private String stName;
    
    /** 강사 학력 */
    private String lecAcdmcr;
    
    /** 강의 경력 */
    private String lecCareer;
    
    /** 강사 소개 */
    private String lecInfo;
    
    /** 회원권한 */  
    private String grade;
    
    /* 관심과목 */
    private List<String> trgetIdList;
    
    /** 소속코드 */  
    private String groupCode;
    
    /** 소속명 */  
    private String groupCodeNm;
    
    //소속명
    private String groupNm;
    
    /** 소속명(타대학) */  
    private String groupNm01;
    
    /** 소속명(일반) */  
    private String groupNm02;
    
    /** 세부전공 코드 */  
    private String major;
    
    /* 세부전공명 */
    private String majorNm;
    
    /** 재직상태 */  
    private String workStatusCode;
    
    /** 재직상태명 */  
    private String workStatusCodeNm;
    
    /** 사번 */  
    private String userSchNo;
    
    /** 주관기관 */  
    private String mngDeptCode;
    
    /** 주관기관명 */  
    private String mngDeptCodeNm;
    
    /** 계정사용여부 */  
    private String useYn;
    
    /** 교육과정이력 */  
    private String currNm;
    
    /** 교육과정이력 개수 */  
    private int currCnt;
    
    /* 검색조건 - 직위*/
    private String searchPositionCode;
    
    /* 검색조건 - 주관기관*/
    private String searchDept;
    
    /* 검색조건 - 성별*/
    private String searchGender;
    
    /* 검색조건 - 소속 */
    private String searchGroup;
    
    /* 검색조건 - 재직상태 */
    private String searchWork;
    
    /* 검색조건 - 이름 */
    private String searchUserNm;
    
    /* 검색조건 - 생년월일 */
    private String searchBrthdy;
    
    /* 검색조건 - 연락처 */
    private String searchMoblphonNo;
    
    /* 검색조건 - 아이디 */
    private String searchUserId;
    
    /* 검색조건 - 이메일 */
    private String searchEmailAdres;
    
    /* LMS 사용 여부 */
    private String confirmAt;
    private String searchConfirmAt;
    
    /* 템플릿 여부 */
    private String templateAt;
    
    /* 위치 */
    private String position;
    
    /** 국가번호 */
    private String geocode;
    
    //메뉴ID
    private java.lang.String menuId;
    
  	//URL
    private String url;
    
    //과정ID
    private java.lang.String crclId;
    
	public String getLecAcdmcr() {
		return lecAcdmcr;
	}

	public void setLecAcdmcr(String lecAcdmcr) {
		this.lecAcdmcr = lecAcdmcr;
	}

	public String getLecInfo() {
		return lecInfo;
	}

	public void setLecInfo(String lecInfo) {
		this.lecInfo = lecInfo;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getLecCareer() {
		return lecCareer;
	}

	public void setLecCareer(String lecCareer) {
		this.lecCareer = lecCareer;
	}

	public String getStTyCodeStaff() {
		return stTyCodeStaff;
	}

	public void setStTyCodeStaff(String stTyCodeStaff) {
		this.stTyCodeStaff = stTyCodeStaff;
	}

	public String getStCodeStaff() {
		return stCodeStaff;
	}

	public void setStCodeStaff(String stCodeStaff) {
		this.stCodeStaff = stCodeStaff;
	}

	public String getCodeNm() {
		return codeNm;
	}

	public void setCodeNm(String codeNm) {
		this.codeNm = codeNm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUserId() {
        return this.userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getCredtId() {
        return this.credtId;
    }
    
    public void setCredtId(String credtId) {
        this.credtId = credtId;
    }
        
    public String getUserSeCode() {
		return userSeCode;
	}

	public void setUserSeCode(String userSeCode) {
		this.userSeCode = userSeCode;
	}
	
	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}
	
	public String getPositionCodeNm() {
		return positionCodeNm;
	}

	public void setPositionCodeNm(String positionCodeNm) {
		this.positionCodeNm = positionCodeNm;
	}

	public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword2() {
        return this.password2;
    }
    
    public void setPassword2(String password2) {
        this.password2 = password2;
    }
    
    public String getUserNm() {
        return this.userNm;
    }
    
    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }
    
    public String getEmailAdres() {
        return this.emailAdres;
    }
    
    public void setEmailAdres(String emailAdres) {
        this.emailAdres = emailAdres;
    }
    
    public String getTlphonNo() {
        return this.tlphonNo;
    }
    
    public void setTlphonNo(String tlphonNo) {
        this.tlphonNo = tlphonNo;
    }
    
    public String getMoblphonNo() {
        return this.moblphonNo;
    }
    
    public void setMoblphonNo(String moblphonNo) {
        this.moblphonNo = moblphonNo;
    }
    
    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
        this.zip = zip;
    }
    
    public String getAdres() {
        return this.adres;
    }
    
    public void setAdres(String adres) {
        this.adres = adres;
    }
    
    public String getAdresDetail() {
        return this.adresDetail;
    }
    
    public void setAdresDetail(String adresDetail) {
        this.adresDetail = adresDetail;
    }
    
    public String getDeleteAt() {
        return this.deleteAt;
    }
    
    public void setDeleteAt(String deleteAt) {
        this.deleteAt = deleteAt;
    }
    
    public java.util.Date getDeletePnttm() {
        return this.deletePnttm;
    }
    
    public void setDeletePnttm(java.util.Date deletePnttm) {
        this.deletePnttm = deletePnttm;
    }
    
    public String getBrthdy() {
        return this.brthdy;
    }
    
    public void setBrthdy(String brthdy) {
        this.brthdy = brthdy;
    }
    
    public String getSlrcldLrrCode() {
		return slrcldLrrCode;
	}

	public void setSlrcldLrrCode(String slrcldLrrCode) {
		this.slrcldLrrCode = slrcldLrrCode;
	}

	public String getSexdstn() {
        return this.sexdstn;
    }
    
    public void setSexdstn(String sexdstn) {
        this.sexdstn = sexdstn;
    }
    
    public String getEmailRecptnAt() {
        return this.emailRecptnAt;
    }
    
    public void setEmailRecptnAt(String emailRecptnAt) {
        this.emailRecptnAt = emailRecptnAt;
    }
    
    public String getMoblphonRecptnAt() {
        return this.moblphonRecptnAt;
    }
    
    public void setMoblphonRecptnAt(String moblphonRecptnAt) {
        this.moblphonRecptnAt = moblphonRecptnAt;
    }
    
    public String getPhotoOriginalFileNm() {
        return this.photoOriginalFileNm;
    }
    
    public void setPhotoOriginalFileNm(String photoOriginalFileNm) {
        this.photoOriginalFileNm = photoOriginalFileNm;
    }
    
    public String getPhotoStreFileNm() {
        return this.photoStreFileNm;
    }
    
    public void setPhotoStreFileNm(String photoStreFileNm) {
        this.photoStreFileNm = photoStreFileNm;
    }
    
    public java.util.Date getFrstRegistPnttm() {
        return this.frstRegistPnttm;
    }
    
    public void setFrstRegistPnttm(java.util.Date frstRegistPnttm) {
        this.frstRegistPnttm = frstRegistPnttm;
    }
    
    public java.util.Date getLastUpdusrPnttm() {
        return this.lastUpdusrPnttm;
    }
    
    public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
        this.lastUpdusrPnttm = lastUpdusrPnttm;
    }
        
    public String getLastUpdusrId() {
        return this.lastUpdusrId;
    }
    
    public void setLastUpdusrId(String lastUpdusrId) {
        this.lastUpdusrId = lastUpdusrId;
    }
    
    public java.util.Date getConfmPnttm() {
        return this.confmPnttm;
    }
    
    public void setConfmPnttm(java.util.Date confmPnttm) {
        this.confmPnttm = confmPnttm;
    }
    
    public String getConfmAt() {
        return this.confmAt;
    }
    
    public void setConfmAt(String confmAt) {
        this.confmAt = confmAt;
    }
    
    public String getDeleteResn() {
        return this.deleteResn;
    }
    
    public void setDeleteResn(String deleteResn) {
        this.deleteResn = deleteResn;
    }    
    
    public String getPrtctorNm() {
        return this.userIhidnum;
    }
    
    public void setUserIhidnum(String userIhidnum) {
        this.userIhidnum = userIhidnum;
    }

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStGrade() {
		return stGrade;
	}

	public void setStGrade(String stGrade) {
		this.stGrade = stGrade;
	}

	public String getStClass() {
		return stClass;
	}

	public void setStClass(String stClass) {
		this.stClass = stClass;
	}

	public String getStNumber() {
		return stNumber;
	}

	public void setStNumber(String stNumber) {
		this.stNumber = stNumber;
	}
	
	public String getStCode() {
		return stCode;
	}
	
	public String getStName() {
		return stName;
	}

	public void setStName(String stName) {
		this.stName = stName;
	}

	public void setStCode(String stCode) {
		this.stCode = stCode;
	}

	public String getStTyCode() {
		return stTyCode;
	}

	public void setStTyCode(String stTyCode) {
		this.stTyCode = stTyCode;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getTel3() {
		return tel3;
	}

	public void setTel3(String tel3) {
		this.tel3 = tel3;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public List<String> getTrgetIdList() {
		return trgetIdList;
	}

	public void setTrgetIdList(List<String> trgetIdList) {
		this.trgetIdList = trgetIdList;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	
	public String getGroupCodeNm() {
		return groupCodeNm;
	}

	public void setGroupCodeNm(String groupCodeNm) {
		this.groupCodeNm = groupCodeNm;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getBrthdy01() {
		return brthdy01;
	}

	public void setBrthdy01(String brthdy01) {
		this.brthdy01 = brthdy01;
	}

	public String getBrthdy02() {
		return brthdy02;
	}

	public void setBrthdy02(String brthdy02) {
		this.brthdy02 = brthdy02;
	}

	public String getBrthdy03() {
		return brthdy03;
	}

	public void setBrthdy03(String brthdy03) {
		this.brthdy03 = brthdy03;
	}

	public String getGroupNm01() {
		return groupNm01;
	}

	public void setGroupNm01(String groupNm01) {
		this.groupNm01 = groupNm01;
	}

	public String getGroupNm02() {
		return groupNm02;
	}

	public void setGroupNm02(String groupNm02) {
		this.groupNm02 = groupNm02;
	}

	public String getEmail3() {
		return email3;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public String getEmail4() {
		return email4;
	}

	public void setEmail4(String email4) {
		this.email4 = email4;
	}
	
	public String getWorkStatusCode() {
		return workStatusCode;
	}

	public void setWorkStatusCode(String workStatusCode) {
		this.workStatusCode = workStatusCode;
	}
	
	public String getWorkStatusCodeNm() {
		return workStatusCodeNm;
	}

	public void setWorkStatusCodeNm(String workStatusCodeNm) {
		this.workStatusCodeNm = workStatusCodeNm;
	}
	
	public String getUserSchNo() {
		return userSchNo;
	}

	public void setUserSchNo(String userSchNo) {
		this.userSchNo = userSchNo;
	}
	
	public String getMngDeptCode() {
		return mngDeptCode;
	}

	public void setMngDeptCode(String mngDeptCode) {
		this.mngDeptCode = mngDeptCode;
	}
	

	public String getMajorNm() {
		return majorNm;
	}

	public void setMajorNm(String majorNm) {
		this.majorNm = majorNm;
	}

	public String getMngDeptCodeNm() {
		return mngDeptCodeNm;
	}

	public void setMngDeptCodeNm(String mngDeptCodeNm) {
		this.mngDeptCodeNm = mngDeptCodeNm;
	}
	
	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	
	public String getCurrNm() {
		return currNm;
	}

	public void setCurrNm(String currNm) {
		this.currNm = currNm;
	}
	
	public int getCurrCnt() {
		return currCnt;
	}

	public void setCurrCnt(int currCnt) {
		this.currCnt = currCnt;
	}
	
	public String getSearchPositionCode() {
		return searchPositionCode;
	}

	public void setSearchPositionCode(String searchPositionCode) {
		this.searchPositionCode = searchPositionCode;
	}
	
	public String getSearchDept() {
		return searchDept;
	}

	public void setSearchDept(String searchDept) {
		this.searchDept = searchDept;
	}
	
	public String getSearchGender() {
		return searchGender;
	}

	public void setSearchGender(String searchGender) {
		this.searchGender = searchGender;
	}
	
	public String getSearchGroup() {
		return searchGroup;
	}

	public void setSearchGroup(String searchGroup) {
		this.searchGroup = searchGroup;
	}
	
	public String getSearchWork() {
		return searchWork;
	}

	public void setSearchWork(String searchWork) {
		this.searchWork = searchWork;
	}
	
	public String getSearchUserNm() {
		return searchUserNm;
	}

	public void setSearchUserNm(String searchUserNm) {
		this.searchUserNm = searchUserNm;
	}
	
	public String getSearchBrthdy() {
		return searchBrthdy;
	}

	public void setSearchBrthdy(String searchBrthdy) {
		this.searchBrthdy = searchBrthdy;
	}
	
	public String getSearchMoblphonNo() {
		return searchMoblphonNo;
	}

	public void setSearchMoblphonNo(String searchMoblphonNo) {
		this.searchMoblphonNo = searchMoblphonNo;
	}
	
	public String getSearchUserId() {
		return searchUserId;
	}

	public void setSearchUserId(String searchUserId) {
		this.searchUserId = searchUserId;
	}

	public String getSearchEmailAdres() {
		return searchEmailAdres;
	}

	public void setSearchEmailAdres(String searchEmailAdres) {
		this.searchEmailAdres = searchEmailAdres;
	}

	public String getConfirmAt() {
		return confirmAt;
	}

	public void setConfirmAt(String confirmAt) {
		this.confirmAt = confirmAt;
	}

	public String getTemplateAt() {
		return templateAt;
	}

	public void setTemplateAt(String templateAt) {
		this.templateAt = templateAt;
	}

	public String getGeocode() {
		return geocode;
	}

	public void setGeocode(String geocode) {
		this.geocode = geocode;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public java.lang.String getMenuId() {
		return menuId;
	}

	public void setMenuId(java.lang.String menuId) {
		this.menuId = menuId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getGroupNm() {
		return groupNm;
	}

	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}

	public java.lang.String getCrclId() {
		return crclId;
	}

	public void setCrclId(java.lang.String crclId) {
		this.crclId = crclId;
	}

	public String getSearchConfirmAt() {
		return searchConfirmAt;
	}

	public void setSearchConfirmAt(String searchConfirmAt) {
		this.searchConfirmAt = searchConfirmAt;
	}
	
}
