package egovframework.com.uss.ivp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.sym.mpm.service.MpmVO;
import egovframework.com.uss.ivp.service.EgovIndvdlestbsService;
import egovframework.com.uss.ivp.service.IndvdlestbsVO;
import egovframework.com.uss.ivp.service.impl.IndvdlestbsDAO;

@Service("IndvdlestbsService")
public class EgovIndvdlestbsServiceImpl extends EgovAbstractServiceImpl implements
EgovIndvdlestbsService {

    @Resource(name="IndvdlestbsDAO")
    private IndvdlestbsDAO indvdlestbsDAO;
    
	/**
	 * 개인 설정정보
	 */
    public String insertIndvdlestbs(IndvdlestbsVO vo) throws Exception {
    	
    	indvdlestbsDAO.insertIndvdlestbs(vo);
        return null;
    }

    /**
	 * COMTNINDVDLESTBS을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 IndvdlestbsVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteIndvdlestbs(IndvdlestbsVO vo) throws Exception {
        indvdlestbsDAO.deleteIndvdlestbs(vo);
    }
    
    /**
	 * 개인설정  메뉴목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return COMTNINDVDLESTBS 목록
	 * @exception Exception
	 */
    public List<MpmVO> selectMyMenuList(IndvdlestbsVO searchVO) throws Exception {
        return indvdlestbsDAO.selectMyMenuList(searchVO);
    }
    
    public List<EgovMap> selectIvpList(IndvdlestbsVO vo) throws Exception {
    	return indvdlestbsDAO.selectIvpList(vo);
    }
    
}
