package egovframework.com.cmm.view;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.web.servlet.view.document.AbstractExcelView;

/**
 * <pre>
 * Class Name  : ExcelView.java
 * Description : 엑셀 뷰 부모 클래스 
 * Modification Information  
 * 
 *    수정일　　　 　　  수정자　　　     수정내용
 *    ────────────   ─────────   ───────────────────────────────
 *    2011. 1. 29.   이상훈              최초생성
 * </pre>
 *
 * @author 이상훈
 * @since 2011. 1. 29.
 * @version 1.0
 * 
 */
public abstract class ExcelView extends AbstractExcelView {
    protected HSSFCellStyle documentTitleStyle;
    protected HSSFCellStyle titleStyle;
    protected HSSFCellStyle titleTextStyle;
    protected HSSFCellStyle columnTextStyle;
    protected HSSFCellStyle columnTextCenterStyle;
    protected HSSFCellStyle columnTextRightStyle;
    
    /**
     * getStyleDocumentTitle
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleDocumentTitle(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)20);
        font.setColor((short) HSSFColor.BLACK.index);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        return cellStyle;
    }
    
    /**
     * getStyleTitle
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleTitle(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)12);
        font.setColor((short) HSSFColor.BLACK.index);
           font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.GREY_25_PERCENT.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        return cellStyle;
    }

    /**
     * getStyleTitleText
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleTitleText(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)12);
        font.setColor((short) HSSFColor.BLACK.index);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        return cellStyle;
    }

    /**
     * getStyleColumnText
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleColumnText(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)9);
        font.setColor((short) HSSFColor.BLACK.index);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
           //개행적용
           cellStyle.setWrapText(true);
        return cellStyle;
    }
    
    /**
     * getStyleColumnTextCenter
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleColumnTextCenter(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)9);
        font.setColor((short) HSSFColor.BLACK.index);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        return cellStyle;
    }
    
    /**
     * getStyleColumnTextRight
     * 
     * @param paramMap
     * @return
     * @throws Exception
     * @author 
     */
    protected HSSFCellStyle getStyleColumnTextRight(HSSFCellStyle cellStyle, HSSFFont font) {
        font.setFontName("맑은 고딕");
        font.setFontHeightInPoints((short)9);
        font.setColor((short) HSSFColor.BLACK.index);
           
           cellStyle.setFont(font);
           cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

        //set border style
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
           cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
           cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        //set color
           cellStyle.setFillBackgroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillForegroundColor((short) HSSFColor.WHITE.index);
           cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

           cellStyle.setLocked(true);
           cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        return cellStyle;
    }
}
