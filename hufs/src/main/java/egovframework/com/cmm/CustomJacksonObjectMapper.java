package egovframework.com.cmm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import egovframework.com.cmm.NullSerializer;


@SuppressWarnings("serial")
public class CustomJacksonObjectMapper extends ObjectMapper {

	public CustomJacksonObjectMapper() {
		super();

		DefaultSerializerProvider.Impl sp = new DefaultSerializerProvider.Impl();
		sp.setNullValueSerializer(new NullSerializer());
		this.setSerializerProvider(sp);
	}
}
