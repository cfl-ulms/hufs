package egovframework.com.cmm.util;

import javax.annotation.Resource;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

import egovframework.rte.fdl.cryptography.EgovPasswordEncoder;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.cryptography.impl.EgovARIACryptoServiceImpl;

@Component("CryptoARIAUtil")
public class CryptoARIAUtil{

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;
    
    //암호화
    public byte[] encryptData(String data){
        EgovPasswordEncoder egovPasswordEncoder = new EgovPasswordEncoder();
        EgovARIACryptoServiceImpl egovARIACryptoServiceImpl = new EgovARIACryptoServiceImpl();
        
        String key = propertyService.getString("crypto.hashed.password");
        String hasedPassword = egovPasswordEncoder.encryptPassword(key);
        egovPasswordEncoder.setHashedPassword(hasedPassword);
        egovPasswordEncoder.setAlgorithm("SHA-256");
        egovARIACryptoServiceImpl.setPasswordEncoder(egovPasswordEncoder);
        egovARIACryptoServiceImpl.setBlockSize(1025);
        
        byte[] encrypted = egovARIACryptoServiceImpl.encrypt(data.getBytes(), key);
        return encrypted;
    }
    
    //복호화
    public byte[] decryptedData(String data){
        
        EgovPasswordEncoder egovPasswordEncoder = new EgovPasswordEncoder();
        EgovARIACryptoServiceImpl egovARIACryptoServiceImpl = new EgovARIACryptoServiceImpl();
        
        String key = propertyService.getString("crypto.hashed.password");
        String hasedPassword = egovPasswordEncoder.encryptPassword(key);
        egovPasswordEncoder.setHashedPassword(hasedPassword);
        egovPasswordEncoder.setAlgorithm("SHA-256");
        egovARIACryptoServiceImpl.setPasswordEncoder(egovPasswordEncoder);
        egovARIACryptoServiceImpl.setBlockSize(1025);
        
        byte[] decrypted = egovARIACryptoServiceImpl.decrypt(Base64.decodeBase64(data.getBytes()), key);
        return decrypted;
    }
    
    
}
