package egovframework.com.cmm.web;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FileVO implements Serializable {
    /**
	 * 파일확장자
	 */
    public String fileExtsn = "";
    /**
	 * 파일크기
	 */
    public String fileMg = "";
    /**
	 * 파일저장경로
	 */
    public String fileStreCours = "";
    /**
	 * 원파일명
	 */
    public String orignlFileNm = "";
    /**
	 * 저장파일명
	 */
    public String streFileNm = "";
    
    /**
	 * 임시파일ID
	 */
    public String tmprFileId = "";
    
    /**
	 * fileExtsn attribute를 리턴한다.
	 * @return  the fileExtsn
	 */
    public String getFileExtsn() {
	return fileExtsn;
    }

    /**
	 * fileExtsn attribute 값을 설정한다.
	 * @param fileExtsn  the fileExtsn to set
	 */
    public void setFileExtsn(String fileExtsn) {
	this.fileExtsn = fileExtsn;
    }

    /**
	 * fileMg attribute를 리턴한다.
	 * @return  the fileMg
	 */
    public String getFileMg() {
	return fileMg;
    }

    /**
	 * fileMg attribute 값을 설정한다.
	 * @param fileMg  the fileMg to set
	 */
    public void setFileMg(String fileMg) {
	this.fileMg = fileMg;
    }

    /**
	 * fileStreCours attribute를 리턴한다.
	 * @return  the fileStreCours
	 */
    public String getFileStreCours() {
	return fileStreCours;
    }

    /**
	 * fileStreCours attribute 값을 설정한다.
	 * @param fileStreCours  the fileStreCours to set
	 */
    public void setFileStreCours(String fileStreCours) {
	this.fileStreCours = fileStreCours;
    }

    /**
	 * orignlFileNm attribute를 리턴한다.
	 * @return  the orignlFileNm
	 */
    public String getOrignlFileNm() {
	return orignlFileNm;
    }

    /**
	 * orignlFileNm attribute 값을 설정한다.
	 * @param orignlFileNm  the orignlFileNm to set
	 */
    public void setOrignlFileNm(String orignlFileNm) {
	this.orignlFileNm = orignlFileNm;
    }

    /**
	 * streFileNm attribute를 리턴한다.
	 * @return  the streFileNm
	 */
    public String getStreFileNm() {
	return streFileNm;
    }

    /**
	 * streFileNm attribute 값을 설정한다.
	 * @param streFileNm  the streFileNm to set
	 */
    public void setStreFileNm(String streFileNm) {
	this.streFileNm = streFileNm;
    }
    
	public String getTmprFileId() {
		return tmprFileId;
	}

	public void setTmprFileId(String tmprFileId) {
		this.tmprFileId = tmprFileId;
	}
}
