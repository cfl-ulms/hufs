package egovframework.com.cmm.web;

import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import egovframework.com.cmm.util.CryptoARIAUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import net.sf.json.JSONObject;

@Controller
public class CryptoARIAController {
	
	/** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    @Resource(name = "CryptoARIAUtil")
	private CryptoARIAUtil cryptoARIAUtil;
    
    @RequestMapping("/crypto/ariaEn.do")
    public void ariaEn(@RequestParam("data") String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	
    	byte[] dataAriaByte = cryptoARIAUtil.encryptData(data);
    	String dataAria = new String(Base64.encodeBase64(dataAriaByte));
    	dataAria = dataAria.replaceAll("\\+", "%2B");
    	dataAria = dataAria.replaceAll("\\&", "%26");
    	
    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("data", dataAria);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    	
    	
    }
    
    
    
}
