package egovframework.com.cmm.web;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.util.EgovWebUtil;
import egovframework.com.utl.fcc.service.EgovNumberUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name : EgovImageProcessController.java
 * @Description :
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 4. 2.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 4. 2.
 * @version
 * @see
 *
 */
@Controller
public class EgovImageProcessController {

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    Logger log = Logger.getLogger(this.getClass());

    /**
     * 게시판의 첨부된 이미지에 대한(썸네일) 미리보기 기능을 제공한다.
     *
     * @param atchFileId
     * @param fileSn
     * @param sessionVO
     * @param model
     * @param response
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/getImage.do")
    public void getImage(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	    String fileStorePath = EgovStringUtil.isEmpty(request.getParameter("fileStorePath")) ? "Board.fileStorePath" : request.getParameter("fileStorePath");
    	String siteId = request.getParameter("siteId");
		String appendPath = EgovWebUtil.filePathBlackList(request.getParameter("appendPath"));
		String atchFileNm = EgovWebUtil.filePathBlackList(request.getParameter("atchFileNm"));
		String thumbYn = request.getParameter("thumbYn");
		String fileExt = "";
	    int index = atchFileNm.lastIndexOf(".");
	    if(index != -1) {
	    	fileExt = atchFileNm.substring(index + 1);
	    	atchFileNm = atchFileNm.substring(0, index);
	    }

	    String resFilePath = propertiesService.getString(fileStorePath) + "/" + siteId+ "/" + appendPath;
	    File file = null;
	    if("Y".equals(thumbYn)) {
			String strWidth = request.getParameter("width");
			String strHeight = request.getParameter("height");
			int width = (EgovNumberUtil.getNumberValidCheck(strWidth)) ? EgovStringUtil.zeroConvert(strWidth) : propertiesService.getInt("photoThumbWidth");
			int height = (EgovNumberUtil.getNumberValidCheck(strHeight)) ? EgovStringUtil.zeroConvert(strHeight) : propertiesService.getInt("photoThumbHeight");

			file = new File(resFilePath, atchFileNm + "_THUMB." + fileExt);
			if(!file.exists()) {
				width = 500;
				height = 500;
				Thumbnails.of(new File(resFilePath, atchFileNm))
					.size(width, height)
					.toFile(file)
					;
			}
	    } else {
	    	file = new File(resFilePath, atchFileNm);
	    }

	    if(file.exists()) {
			FileInputStream fis = null;
			BufferedInputStream in = null;
			ByteArrayOutputStream bStream = null;

			try {

				fis = new FileInputStream(file);
				in = new BufferedInputStream(fis);
				bStream = new ByteArrayOutputStream();

				int imgByte;
				while ((imgByte = in.read()) != -1) {
				    bStream.write(imgByte);
				}

				String type = "";

				if (fileExt != null && !"".equals(fileExt)) {
				    if ("jpg".equals(EgovStringUtil.lowerCase(fileExt))) {
					type = "image/jpeg";
				    } else {
					type = "image/" + EgovStringUtil.lowerCase(fileExt);
				    }

				} else {
				    log.debug("Image fileType is null.");
				}

				response.setHeader("Content-Type", type);
				response.setContentLength(bStream.size());

				bStream.writeTo(response.getOutputStream());

				response.getOutputStream().flush();
			} catch (FileNotFoundException fnfe) {
				log.debug("/cmm/fms/getImage.do -- stream error : " + atchFileNm);
			} catch (IOException ioe) {
				log.debug("/cmm/fms/getImage.do -- stream error : " + atchFileNm);
			} catch(Exception e) {
				log.debug("/cmm/fms/getImage.do -- stream error : " + atchFileNm);
			} finally {
				try {response.getOutputStream().close();}catch(Exception ex){log.info(ex);}
				if(bStream != null) {
					try {bStream.close();}catch(IOException ex){log.info(ex);}
				}
				if(in != null) {
					try {in.close();}catch(IOException ex){log.info(ex);}
				}
				if(fis != null) {
					try {fis.close();}catch(IOException ex){log.info(ex);}
				}
			}
	    }
    }

}
