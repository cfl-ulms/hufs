package egovframework.com.cmm.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.JsonResponse;
import egovframework.com.lms.service.ClassManageService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * 파일 조회, 삭제, 다운로드 처리를 위한 컨트롤러 클래스
 * @author 공통서비스개발팀 이삼섭
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.3.25  이삼섭          최초 생성
 *
 * </pre>
 */
@Controller
public class EgovFileMngController {

    @Resource(name = "EgovFileMngService")
    private EgovFileMngService fileService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "egovFileIdGnrService")
    private EgovIdGnrService fileIdgenService;

    @Resource(name = "classManageService")
    private ClassManageService classManageService;

    Logger log = Logger.getLogger(this.getClass());

    /**
     * 첨부파일에 대한 목록을 조회한다.
     *
     * @param fileVO
     * @param atchFileId
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/selectFileInfs.do")
    public String selectFileInfs(HttpServletRequest request, @ModelAttribute("searchVO") FileVO fileVO, @RequestParam("param_atchFileId") String param_atchFileId, ModelMap model) throws Exception {
	String returnUrl = "cmm/fms/EgovFileList";
	String atchFileId = param_atchFileId;
	String viewDivision = request.getParameter("viewDivision") == null ? "" : request.getParameter("viewDivision").toString();
	if("staff".equals(viewDivision)){
		returnUrl = "cmm/fms/EgovStaffFileList";
	}else{
		model.addAttribute("updateFlag", "N");
	}

	fileVO.setAtchFileId(atchFileId);
	List<FileVO> result = fileService.selectFileInfs(fileVO);


	model.addAttribute("fileList", result);
	model.addAttribute("fileListCnt", result.size());
	model.addAttribute("atchFileId", atchFileId);
	model.addAttribute("webPath", propertiesService.getString("web.path"));

	return "cmm/fms/EgovFileList";
    }

    /**
     * 자료요청 완료 첨부파일에 대한 목록을 조회한다.
     *
     * @param fileVO
     * @param atchFileId
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/selectDtaResFileInfs.do")
    public String selectDtaResFileInfs(@ModelAttribute("searchVO") FileVO fileVO, @RequestParam("param_atchFileId") String param_atchFileId, ModelMap model) throws Exception {
	String atchFileId = param_atchFileId;

	fileVO.setAtchFileId(atchFileId);
	List<FileVO> result = fileService.selectFileInfs(fileVO);

	model.addAttribute("fileList", result);
	model.addAttribute("updateFlag", "N");
	model.addAttribute("fileListCnt", result.size());
	model.addAttribute("atchFileId", atchFileId);

	return "cmm/fms/EgovDtaResFileList";
    }

    /**
     * 첨부파일 변경을 위한 수정페이지로 이동한다.
     *
     * @param fileVO
     * @param atchFileId
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/selectFileInfsForUpdate.do")
    public String selectFileInfsForUpdate(HttpServletRequest request,
    	    HttpServletResponse response, @ModelAttribute("searchVO") FileVO fileVO, @RequestParam("param_atchFileId") String param_atchFileId,
	    //SessionVO sessionVO,
	    ModelMap model) throws Exception {

	String atchFileId = param_atchFileId;
	String returnUrl = "cmm/fms/EgovFileList";
	String viewDivision = request.getParameter("viewDivision") == null ? "" : request.getParameter("viewDivision").toString();
	if("staff".equals(viewDivision)){
		returnUrl = "cmm/fms/EgovStaffFileList";
	}

	if(!EgovStringUtil.isEmpty(atchFileId)) {
		fileVO.setAtchFileId(atchFileId);

		List<FileVO> result = fileService.selectFileInfs(fileVO);

		model.addAttribute("fileList", result);
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);
	}

	model.addAttribute("updateFlag", "Y");

	return returnUrl;
    }

    /**
     * 첨부파일에 대한 삭제를 처리한다.
     *
     * @param fileVO
     * @param returnUrl
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/deleteFileInfs.do")
    public String deleteFileInf(@ModelAttribute("searchVO") FileVO fileVO, @RequestParam("returnUrl") String returnUrl,
	    HttpServletRequest request,
	    HttpServletResponse response,
	    ModelMap model) throws Exception {

	Boolean isAuthenticated = true;//EgovUserDetailsHelper.isAuthenticated(request, response);

	if (isAuthenticated) {
	    fileService.deleteFileInf(fileVO);
	}


	if (!EgovStringUtil.isEmpty(returnUrl)) {
	    return "redirect:" + returnUrl;
	} else {
	    return "redirect:/";
	}

    }

    /**
     * 첨부파일에 대한 삭제를 처리한다.
     *
     * @param fileVO
     * @param returnUrl
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/deleteFileInfByAjax.do")
    public void deleteFileInfByJson(@ModelAttribute("searchVO") FileVO fileVO,
	    HttpServletRequest request,
	    HttpServletResponse response,
	    ModelMap model) throws Exception {

    	int iCount = fileService.deleteFileInf(fileVO);

    	Gson gson = new Gson();
    	JsonObject jObj = new JsonObject();

    	if(iCount != 0) {
    		jObj.addProperty("delCount", iCount);
	    	jObj.addProperty("atchFileId", fileVO.getAtchFileId());
	    	jObj.addProperty("fileSn", fileVO.getFileSn());
    	} else {
    		jObj.addProperty("delCount", "0");
    	}

    	FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(fileVO);
    	jObj.addProperty("totalFileMg", totalInfoVO.getTotalFileMg());
    	jObj.addProperty("totalFileCount", totalInfoVO.getTotalFileCount());

  	  	response.setContentType("text/javascript; charset=utf-8");
  	  	PrintWriter printwriter = response.getWriter();
  	  	printwriter.write(gson.toJson(jObj));
  	  	printwriter.flush();
  		printwriter.close();

    }

    /**
     * 이미지 첨부파일에 대한 목록을 조회한다.
     *
     * @param fileVO
     * @param atchFileId
     * @param sessionVO
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/selectImageFileInfs.do")
    public String selectImageFileInfs(@ModelAttribute("searchVO") FileVO fileVO,
	    //SessionVO sessionVO,
	    ModelMap model) throws Exception {

	String atchFileId = fileVO.getAtchFileId();

	fileVO.setAtchFileId(atchFileId);
	List<FileVO> result = fileService.selectImageFileList(fileVO);

	model.addAttribute("fileList", result);

	return "cmm/fms/EgovImgFileList";
    }

    /**
     * 대용량파일을 Upload 처리한다.
     *
     * @param fileVO
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/cmm/fms/uploadStreamLongFiles.do")
    public void uploadStreamLongFiles(@ModelAttribute("searchVO") FileVO fileVO, @CommandMap Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	String siteId = (String)commandMap.get("siteId");
    	if(EgovStringUtil.isEmpty(siteId)) {
    		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    		siteId = siteVO.getSiteId();
    	}

    	String strErrorMessage = "";
    	boolean isSaveOk = false;

    	//String uploadType = (String)commandMap.get("uploadType");
    	String pathKey = (String)commandMap.get("pathKey");
    	String appendPath = (String)commandMap.get("appendPath");
    	String first = (String)commandMap.get("first");
    	String last = (String)commandMap.get("last");

    	String isEdit = (String)commandMap.get("isEdit");
    	String isTempEdit = (String)commandMap.get("isTempEdit");

    	boolean firstChunk = Boolean.parseBoolean(first);
    	boolean lastChunk = Boolean.parseBoolean(last);
    	boolean edit = Boolean.parseBoolean(isEdit);
    	boolean tempEdit = Boolean.parseBoolean(isTempEdit);

    	fileVO.setFileStreCours(propertiesService.getString(pathKey) + File.separator + siteId + File.separator + appendPath);

    	FileVO resultVO = fileUtil.uploadStreamLongFiles(request.getInputStream(), fileVO, firstChunk, lastChunk);

    	if(resultVO == null) {
    		strErrorMessage = "등록할 수 없는 파일입니다.";
    	} else {
	    	if(lastChunk) {

	    		File file = new File(fileVO.getFileStreCours() + File.separator + fileVO.getStreFileNm());
	    		// =================================== 웹필터 적용 시작 ======================================================================
	    		String wfServerAddress = propertiesService.getString("WebFilterIP"); // 웹필터 아이피 세팅
	    		//String wfFileName = file.getFileName(); // 실제 파일명 세팅
	    		String wfFileName = file.getName();

	    		Socket s = null;
	    		boolean byPass = false;
	    		boolean denyFlag = false;

	    		int pos = wfFileName.lastIndexOf( "." );
	    		String ext = wfFileName.substring( pos + 1 );
	    		if(!"jpg".equals(ext) && !"gif".equals(ext) && !"bmp".equals(ext) && !"png".equals(ext) && !"psd".equals(ext) && !"pdd".equals(ext) && !"tif".equals(ext) && !"raw".equals(ext) && !"ai".equals(ext) && !"eps".equals(ext) && !"pcx".equals(ext) && !"pct".equals(ext) && !"pic".equals(ext) && !"pxr".equals(ext) && !"tga".equals(ext) &&
	    			!"wav".equals(ext) && !"ogg".equals(ext) && !"mp3".equals(ext) && !"wma".equals(ext) && !"au".equals(ext) && !"mid".equals(ext) &&
	    			!"avi".equals(ext) && !"mpg".equals(ext) && !"mpeg".equals(ext) && !"mpe".equals(ext) && !"wmv".equals(ext) && !"asf".equals(ext) && !"asx".equals(ext) && !"flv".equals(ext) && !"rm".equals(ext) && !"mov".equals(ext) && !"dat".equals(ext) &&
	    			!"vob".equals(ext) && !"m2v".equals(ext) && !"mkv".equals(ext)  && !"mp4".equals(ext)){

	    			try{
	    				s = new Socket();
	    				SocketAddress remoteAddr = new InetSocketAddress(wfServerAddress, 80);
	    				s.connect(remoteAddr, 5000);
	    			}catch(Exception e){
	    				byPass = true;
	    			}

	    			if(!byPass){
	    				// 웹필터 처리
	    				try{
	    					URL targetURL = new URL("http://"+wfServerAddress+"/webfilterSubmitAction.do?serverDomain_="+request.getServerName()+"&serverPort_=&writeFormName_=fileForm&serverProtocol_=http:&WFcharSet_=euc-kr&WFOrgAction_=/fileUpload.do&WFlistUrl_=/board/record/list.do");
	    					HttpURLConnection conn = (HttpURLConnection)targetURL.openConnection();

	    					String delimeter = "------------ei4gL6Ij5ei4GI3Ij5Ij5gL6cH2Ef1";

	    					byte[] newLineBytes = "\r\n".getBytes();
	    					byte[] delimeterBytes = delimeter.getBytes();
	    					byte[] dispositionBytes = "Content-Disposition: form-data; name=".getBytes();
	    					byte[] nameBytes = "filename".getBytes();
	    					byte[] fileBytes = "file".getBytes();
	    					byte[] quotationBytes = "\"".getBytes();
	    					byte[] contentTypeBytes = "Content-Type: application/octet-stream".getBytes();
	    					byte[] fileNameBytes = "; filename=".getBytes();
	    					byte[] twoDashBytes = "--".getBytes();

	    					conn.setRequestMethod("POST");
	    					conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+delimeter);
	    					conn.setDoInput(true);
	    					conn.setDoOutput(true);
	    					conn.setUseCaches(false);

	    					BufferedOutputStream bos = null;
	    					BufferedInputStream bis = null;
	    					try{
	    						bos = new BufferedOutputStream(conn.getOutputStream());

	    						bos.write(twoDashBytes);
	    						bos.write(delimeterBytes);
	    						bos.write(newLineBytes);
	    						bos.write(dispositionBytes);
	    						bos.write(quotationBytes);
	    						bos.write(nameBytes);
	    						bos.write(quotationBytes);
	    						bos.write(newLineBytes);
	    						bos.write(newLineBytes);
	    						bos.write(fileBytes);
	    						bos.write(newLineBytes);

	    						bos.write(twoDashBytes);
	    						bos.write(delimeterBytes);
	    						bos.write(newLineBytes);

	    						bos.write(dispositionBytes);
	    						bos.write(quotationBytes);
	    						bos.write(fileNameBytes);
	    						bos.write(quotationBytes);
	    						bos.write(wfFileName.getBytes());
	    						bos.write(quotationBytes);
	    						bos.write(newLineBytes);
	    						bos.write(contentTypeBytes);
	    						bos.write(newLineBytes);
	    						bos.write(newLineBytes);

	    						bis = new BufferedInputStream(new FileInputStream(file)); //실제파일 read 부분
	    						byte[] fileBuffer = new byte[1024 * 8];
	    						int len = -1;
	    						while((len = bis.read(fileBuffer)) != -1){
	    							bos.write(fileBuffer, 0, len);
	    						}
	    						bos.write(newLineBytes);
	    						bos.write(twoDashBytes);
	    						bos.write(delimeterBytes);
	    						bos.write(twoDashBytes);
	    						bos.write(newLineBytes);
	    						bos.flush();

	    					}catch(Exception e){

	    					}finally{
	    						if(bos != null) bos.close();
	    						if(bis != null) bis.close();
	    					}

	    					BufferedInputStream bisConn = null;
	    					try {
		    					bisConn = new BufferedInputStream(conn.getInputStream());
		    					StringBuffer sb = new StringBuffer();
		    					int iChar;

		    					while((iChar = bisConn.read()) > -1){
		    						sb.append((char)iChar);
		    					}

		    					if(sb.toString().indexOf("/getCleanBoardErrorFile") > -1){
		    						// 웹필터 차단
		    						denyFlag = true;
		    					}
	    					}catch(Exception e){

	    					}finally{
	    						if(bisConn != null) bisConn.close();
	    					}

	    				}catch(Exception e){
	    				}

	    			}

	    			if(s!=null) {
	    				try {
	    					s.close();
	    				} catch(Exception ex) {

	    				}finally{
    						if(s != null) s.close();
    					}
	    			}

	    		}
	    		// =================================== 웹필터 적용 끝 ======================================================================

	    		if(!denyFlag){  // 웹필터 차단여부 확인

	    			//정상부분처리
	    			isSaveOk = true;

	    		}else{ // 웹필터 if else

	    			// 차단 파일 처리(응답부분 수정 필요)
	    			strErrorMessage = "개인정보가 유출될 수 있는 파일입니다.";

	    		}

			}
    	}

    	if(isSaveOk) {
    		FileVO dbVO = fileMngService.insertTempFileInf(resultVO);

    		if(edit) {
    			if(tempEdit) {
    				FileVO delVO = fileMngService.selectTempFileInfByAtchFileIdAndFileSn(resultVO);
	    			if(delVO != null) {
	    				fileMngService.deleteTempFileInf(delVO);
	    			}
    			} else {
    				fileMngService.deleteFileInf(resultVO);
    			}
			}

    		Gson gson = new Gson();
        	JsonObject jObj = new JsonObject();

    		jObj.addProperty("SiteId", siteId);
    		jObj.addProperty("AppendPath", appendPath);
        	jObj.addProperty("AtchFileId", dbVO.getAtchFileId());
        	jObj.addProperty("TmprFileId", dbVO.getTmprFileId());
        	jObj.addProperty("FileName", dbVO.getOrignlFileNm());
        	jObj.addProperty("StreFileNm", dbVO.getStreFileNm() + "." + dbVO.getFileExtsn());
        	jObj.addProperty("FileSn", dbVO.getFileSn());
        	jObj.addProperty("OriginFileSn", resultVO.getFileSn());
        	jObj.addProperty("FileSize", dbVO.getFileMg());
        	jObj.addProperty("IsEdit", isEdit);
        	jObj.addProperty("IsTempEdit", isTempEdit);
        	FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(dbVO);
        	jObj.addProperty("TotalFileMg", totalInfoVO.getTotalFileMg());
	    	jObj.addProperty("TotalFileCount", totalInfoVO.getTotalFileCount());

	    	jObj.addProperty("ErrorMessage", "");

        	response.setContentType("text/javascript; charset=utf-8");
		    PrintWriter printwriter = response.getWriter();
    		printwriter.write(gson.toJson(jObj));
    		printwriter.flush();
    		printwriter.close();
    	} else if(!EgovStringUtil.isEmpty(strErrorMessage)) {
    		Gson gson = new Gson();
        	JsonObject jObj = new JsonObject();
    		jObj.addProperty("SiteId", siteId);
    		jObj.addProperty("AppendPath", appendPath);
        	jObj.addProperty("AtchFileId", resultVO.getAtchFileId());
        	jObj.addProperty("TmprFileId", resultVO.getTmprFileId());
        	jObj.addProperty("FileName", resultVO.getOrignlFileNm());
        	jObj.addProperty("StreFileNm", resultVO.getStreFileNm() + "." + resultVO.getFileExtsn());
        	jObj.addProperty("FileSn", resultVO.getFileSn());
        	jObj.addProperty("OriginFileSn", resultVO.getFileSn());
        	jObj.addProperty("FileSize", resultVO.getFileMg());
        	jObj.addProperty("IsEdit", isEdit);
        	jObj.addProperty("IsTempEdit", isTempEdit);
        	jObj.addProperty("TotalFileMg", "0");
	    	jObj.addProperty("TotalFileCount", "0");

	    	jObj.addProperty("ErrorMessage", strErrorMessage);

        	response.setContentType("text/javascript; charset=utf-8");
		    PrintWriter printwriter = response.getWriter();
    		printwriter.write(gson.toJson(jObj));
    		printwriter.flush();
    		printwriter.close();
    	}
    }*/


    /*
    @RequestMapping(value = "/cmm/fms/upload.do", method = RequestMethod.POST)
    public void upload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") FileVO searchVO, Map<String, Object> commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	String range = multiRequest.getHeader("Content-Range");
        long fileFullLength = -1;
        long chunkFrom = -1;
        long chunkTo = -1;
        if (range != null) {
            if (!range.startsWith("bytes "))
                throw new Exception("Unexpected range format: " + range);
            String[] fromToAndLength = range.substring(6).split(Pattern.quote("/"));
            fileFullLength = Long.parseLong(fromToAndLength[1]);
            String[] fromAndTo = fromToAndLength[0].split(Pattern.quote("-"));
            chunkFrom = Long.parseLong(fromAndTo[0]);
            chunkTo = Long.parseLong(fromAndTo[1]);
        }



    	String siteId = searchVO.getSiteId();
    	if(EgovStringUtil.isEmpty(siteId)) {
    		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    		siteId = siteVO.getSiteId();
    	}

    	if(EgovStringUtil.isEmpty(searchVO.getAtchFileId())) {
    		searchVO.setAtchFileId(fileIdgenService.getNextStringId());
    	}

		JsonResponse res = new JsonResponse();

		if(!EgovStringUtil.isEmpty(siteId) && !EgovStringUtil.isEmpty(searchVO.getPathKey())) {
			String appendPath = searchVO.getSiteId();
			if(!EgovStringUtil.isEmpty(searchVO.getAppendPath())) {
				appendPath = appendPath + "/" + searchVO.getAppendPath();
	    	}

			ArrayList<FileVO> result = new ArrayList<FileVO>();

		    final Map<String, MultipartFile> files = multiRequest.getFileMap();
		    if(!files.isEmpty()) {
		    	List<FileVO> fileList = fileUtil.parseFileInfV1(searchVO.getMaxMegaFileSize() * 1024 * 1024, files, "", 0, searchVO.getAtchFileId(), searchVO.getPathKey() + ".fileStorePath", searchVO.getPathKey() + ".fileStoreWebPath", appendPath);
		    	if(!"Y".equals(searchVO.getIsNoTempFile())) {
			    	for(int i=0; i<fileList.size(); i++) {
				    	FileVO dbVO = fileMngService.insertTempFileInf(fileList.get(i));
				    	result.add(dbVO);
				    }
		    	} else {
		    		int maxSn = fileMngService.getMaxFileSN(searchVO);
		    		for(int i=0; i<fileList.size(); i++) {
		    			fileList.get(i).setFileSn(String.valueOf(maxSn + i));
		    		}
		    		fileMngService.updateFileInfs(fileList);
		    		result.addAll(fileList);
		    	}
		    }

		    res.setFiles(result);

		    HashMap<String, Object> wireData = new HashMap<String, Object>();
		    wireData.put("editorId", searchVO.getEditorId());
		    if(result.size() > 0) {

			    FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(result.get(0));
			    wireData.put("totalFileMg", totalInfoVO.getTotalFileMg());
			    wireData.put("totalFileCount", totalInfoVO.getTotalFileCount());
		    }

		    res.setWireData(wireData);
		    res.setSuccess(true);
		}

	    Gson gson = new Gson();

    	response.setContentType("text/javascript; charset=utf-8");
	    PrintWriter printwriter = response.getWriter();
		printwriter.write(gson.toJson(res));
		printwriter.flush();
		printwriter.close();

    }
    */

    @RequestMapping(value = "/cmm/fms/upload.do", method = RequestMethod.POST)
    public void upload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") FileVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	/*
    	String siteId = searchVO.getSiteId();
    	if(EgovStringUtil.isEmpty(siteId)) {
    		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    		siteId = siteVO.getSiteId();
    	}

    	if(EgovStringUtil.isEmpty(searchVO.getAtchFileId())) {
    		searchVO.setAtchFileId(fileIdgenService.getNextStringId());
    	}

    	String appendPath = searchVO.getSiteId();
		if(!EgovStringUtil.isEmpty(searchVO.getAppendPath())) {
			appendPath = appendPath + "/" + searchVO.getAppendPath();
    	}


		String storePathString = "";
		String storeWebPathString = "";
		String atchFileIdString = "";

		String storePath = searchVO.getPathKey() + ".fileStorePath";
		String storeWebPath = searchVO.getPathKey() + ".fileStoreWebPath";
		if ("".equals(storePath) || storePath == null) {
		    storePathString = propertiesService.getString("Globals.fileStorePath");
		    storeWebPathString = propertiesService.getString("Globals.fileStoreWebPath");
		} else {
		    storePathString = propertiesService.getString(storePath);
		    storeWebPathString = propertiesService.getString(storeWebPath);
		}

		if (!("".equals(appendPath) || appendPath == null)) {
		    storePathString = storePathString + "/" + appendPath;
		    storeWebPathString = storeWebPathString + "/" + appendPath;
		}

    	String range = multiRequest.getHeader("Content-Range");
        long fileFullLength = -1;
        long chunkFrom = -1;
        long chunkTo = -1;
        if (range != null) {
            if (!range.startsWith("bytes "))
                throw new ServletException("Unexpected range format: " + range);
            String[] fromToAndLength = range.substring(6).split(Pattern.quote("/"));
            fileFullLength = Long.parseLong(fromToAndLength[1]);
            String[] fromAndTo = fromToAndLength[0].split(Pattern.quote("-"));
            chunkFrom = Long.parseLong(fromAndTo[0]);
            chunkTo = Long.parseLong(fromAndTo[1]);
        }



        File tempDir = new File(storePathString);//new File(System.getProperty("java.io.tmpdir"));  // Configure according
        File storageDir = tempDir;                                      // project server environment.

        final Map<String, MultipartFile> files = multiRequest.getFileMap();
        Iterator<Entry<String, MultipartFile>> it = files.entrySet().iterator();
		MultipartFile item;
		List<Map<String, Object>> ret = new ArrayList<Map<String,Object>>();
		while (it.hasNext()) {
		    Entry<String, MultipartFile> entry = it.next();

		    item = entry.getValue();

		    String fileId = searchVO.getAtchFileId() + "_" + String.valueOf(item.getOriginalFilename().hashCode());

		    Map<String, Object> fileInfo = new LinkedHashMap<String, Object>();
            File assembledFile = null;
            fileInfo.put("name", item.getName());
            fileInfo.put("type", item.getContentType());
            File dir = new File(storageDir, fileId);
            if (!dir.exists())
                dir.mkdir();
            if (fileFullLength < 0) {  // File is not chunked
                fileInfo.put("size", item.getSize());
                assembledFile = new File(dir, item.getName());
                item.transferTo(assembledFile);
            } else {  // File is chunked
                byte[] bytes = item.getBytes();
                if (chunkFrom + bytes.length != chunkTo + 1)
                    throw new ServletException("Unexpected length of chunk: " + bytes.length +
                            " != " + (chunkTo + 1) + " - " + chunkFrom);
                saveChunk(dir, item.getName(), chunkFrom, bytes, fileFullLength);
                TreeMap<Long, Long> chunkStartsToLengths = getChunkStartsToLengths(dir, item.getName());
                long lengthSoFar = getCommonLength(chunkStartsToLengths);
                fileInfo.put("size", lengthSoFar);
                if (lengthSoFar == fileFullLength) {
                    assembledFile = assembleAndDeleteChunks(dir, item.getName(),
                            new ArrayList<Long>(chunkStartsToLengths.keySet()));
                }
            }
            if (assembledFile != null) {
                fileInfo.put("complete", true);
                fileInfo.put("serverPath", assembledFile.getAbsolutePath());
                // Here you can do something with fully assembled file.
            }
            ret.add(fileInfo);
		};
        */


    	int imgOrientation = 0;

    	String range = multiRequest.getHeader("Content-Range");
    	long maxChunkSize = searchVO.getMaxChunkSize();
        long fileFullLength = -1;
        long chunkFrom = -1;
        long chunkTo = -1;
        if (range != null) {
            if (!range.startsWith("bytes "))
                throw new Exception("Unexpected range format: " + range);
            String[] fromToAndLength = range.substring(6).split(Pattern.quote("/"));
            fileFullLength = Long.parseLong(fromToAndLength[1]);
            String[] fromAndTo = fromToAndLength[0].split(Pattern.quote("-"));
            chunkFrom = Long.parseLong(fromAndTo[0]);
            chunkTo = Long.parseLong(fromAndTo[1]);
        }



    	String siteId = searchVO.getSiteId();
    	if(EgovStringUtil.isEmpty(siteId)) {
    		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    		siteId = siteVO.getSiteId();
    	}

    	if(EgovStringUtil.isEmpty(searchVO.getAtchFileId())) {
    		searchVO.setAtchFileId(fileIdgenService.getNextStringId());
    	}

		JsonResponse res = new JsonResponse();

		if(!EgovStringUtil.isEmpty(siteId) && !EgovStringUtil.isEmpty(searchVO.getPathKey())) {
			String appendPath = searchVO.getSiteId();
			if(!EgovStringUtil.isEmpty(searchVO.getAppendPath())) {
				appendPath = appendPath + "/" + searchVO.getAppendPath();
	    	}

			ArrayList<FileVO> result = new ArrayList<FileVO>();

		    final Map<String, MultipartFile> files = multiRequest.getFileMap();

		    if(!files.isEmpty()) {
		    	//이미지일 경우, orientation 확인 - i클래스
		    	if(files.get("filesx") != null){
		    		File uploadImg = new File(files.get("filesx").getOriginalFilename());
		    		uploadImg.createNewFile();
		    		FileOutputStream fos = new FileOutputStream(uploadImg);
		    		fos.write(files.get("filesx").getBytes());
		    		fos.close();

		    		String fileType = uploadImg.getPath();
		    		String[] array = fileType.split("\\.");

		    		if("jpg".equals(array[1]) || "jpeg".equals(array[1]) || "png".equals(array[1]) || "gif".equals(array[1])){
		    			imgOrientation = getOrientation(uploadImg);
		    		}
		    	}

		    	List<FileVO> fileList = fileUtil.parseFileInfV2(searchVO.getMaxMegaFileSize() * 1024 * 1024, files, "", 0, searchVO.getAtchFileId(), searchVO.getPathKey() + ".fileStorePath", searchVO.getPathKey() + ".fileStoreWebPath", appendPath, maxChunkSize, fileFullLength, chunkFrom, chunkTo);
		    	if(!"Y".equals(searchVO.getIsNoTempFile())) {
			    	for(int i=0; i<fileList.size(); i++) {
			    		fileList.get(i).setOrientation(imgOrientation);
				    	FileVO dbVO = fileMngService.insertTempFileInf(fileList.get(i));
				    	result.add(dbVO);
				    }
		    	} else {
		    		int maxSn = fileMngService.getMaxFileSN(searchVO);
		    		for(int i=0; i<fileList.size(); i++) {
		    			fileList.get(i).setFileSn(String.valueOf(maxSn + i));
		    		}
		    		fileMngService.updateFileInfs(fileList);
		    		result.addAll(fileList);
		    	}
		    }

		    res.setFiles(result);

		    HashMap<String, Object> wireData = new HashMap<String, Object>();
		    wireData.put("editorId", searchVO.getEditorId());
		    if(result.size() > 0) {

			    FileVO totalInfoVO = fileMngService.selectFileDetailTotalInfo(result.get(0));
			    wireData.put("totalFileMg", totalInfoVO.getTotalFileMg());
			    wireData.put("totalFileCount", totalInfoVO.getTotalFileCount());
		    }

		    res.setWireData(wireData);
		    res.setSuccess(true);
		}

	    Gson gson = new Gson();

    	response.setContentType("text/javascript; charset=utf-8");
	    PrintWriter printwriter = response.getWriter();
		printwriter.write(gson.toJson(res));
		printwriter.flush();
		printwriter.close();

    }

    public static int getOrientation(File in) throws IOException {

		int orientation = 1;
		Metadata metadata;
		Directory directory;

		try {
			metadata = ImageMetadataReader.readMetadata(in);
			directory = metadata.getDirectory(ExifIFD0Directory.class);
			if(directory != null){
				orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
			}
		} catch (ImageProcessingException e) {
			System.err.println("[ImgUtil] could not process image");
			e.printStackTrace();
		} catch (MetadataException e) {
			System.err.println("[ImgUtil] could not get orientation from image");
			e.printStackTrace();
		}

		return orientation;
	}

    private static void saveChunk(File dir, String fileName,
            long from, byte[] bytes, long fileFullLength) throws IOException {
        File target = new File(dir, fileName + "." + from + ".chunk");
        OutputStream os = new FileOutputStream(target);
        try {
            os.write(bytes);
        } finally {
            os.close();
        }
    }

    private static TreeMap<Long, Long> getChunkStartsToLengths(File dir,
            String fileName) throws IOException {
        TreeMap<Long, Long> chunkStartsToLengths = new TreeMap<Long, Long>();
        for (File f : dir.listFiles()) {
            String chunkFileName = f.getName();
            if (chunkFileName.startsWith(fileName + ".") &&
                    chunkFileName.endsWith(".chunk")) {
                chunkStartsToLengths.put(Long.parseLong(chunkFileName.substring(
                        fileName.length() + 1, chunkFileName.length() - 6)), f.length());
            }
        }
        return chunkStartsToLengths;
    }

    private static long getCommonLength(TreeMap<Long, Long> chunkStartsToLengths) {
        long ret = 0;
        for (long len : chunkStartsToLengths.values())
            ret += len;
        return ret;
    }

    private static File assembleAndDeleteChunks(File dir, String fileName,
            List<Long> chunkStarts) throws IOException {
        File assembledFile = new File(dir, fileName);
        if (assembledFile.exists()) // In case chunks come in concurrent way
            return assembledFile;
        OutputStream assembledOs = new FileOutputStream(assembledFile);
        byte[] buf = new byte[100000];
        try {
            for (long chunkFrom : chunkStarts) {
                File chunkFile = new File(dir, fileName + "." + chunkFrom + ".chunk");
                InputStream is = new FileInputStream(chunkFile);
                try {
                    while (true) {
                        int r = is.read(buf);
                        if (r == -1)
                            break;
                        if (r > 0)
                            assembledOs.write(buf, 0, r);
                    }
                } finally {
                    is.close();
                }
                chunkFile.delete();
            }
        } finally {
            assembledOs.close();
        }
        return assembledFile;
    }

    /**
     * 대용량파일을 삭제 처리한다.
     *
     * @param fileVO
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/deleteStreamLongFileByAjax.do")
    public void deleteStreamLongFileByAjax(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	FileVO dbVO = fileMngService.deleteTempFileInf(fileVO);

    	Gson gson = new Gson();
    	JsonObject jObj = new JsonObject();

    	if(dbVO != null) {
    		jObj.addProperty("delCount", "1");
	    	jObj.addProperty("atchFileId", dbVO.getAtchFileId());
	    	jObj.addProperty("fileSn", dbVO.getFileSn());

	    	FileVO totalInfoVO = fileService.selectFileDetailTotalInfo(dbVO);
	    	jObj.addProperty("totalFileMg", totalInfoVO.getTotalFileMg());
	    	jObj.addProperty("totalFileCount", totalInfoVO.getTotalFileCount());
    	} else {
    		jObj.addProperty("delCount", "0");
    	}

  	  	response.setContentType("text/javascript; charset=utf-8");
  	  	PrintWriter printwriter = response.getWriter();
  	  	printwriter.write(gson.toJson(jObj));
  	  	printwriter.flush();
  		printwriter.close();
    }

    /**
     * 파일ID를 생성환다.
     *
     * @param fileVO
     * @return
     * @throws Exception
     */
    @RequestMapping("/cmm/fms/selectFileIdByAjax.do")
    public void selectFileIdByAjax(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	Gson gson = new Gson();
    	JsonObject jObj = new JsonObject();
    	jObj.addProperty("atchFileId", fileIdgenService.getNextStringId());

  	  	response.setContentType("text/javascript; charset=utf-8");
  	  	PrintWriter printwriter = response.getWriter();
  	  	printwriter.write(gson.toJson(jObj));
  	  	printwriter.flush();
  		printwriter.close();
    }

    //과정수업 첨부파일 목록
    @RequestMapping("/cmm/fms/studyFileList.do")
    public String studyFileList(@ModelAttribute("searchVO") FileVO fileVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getId())) {
			fileVO.setFrstRegisterId(loginVO.getId());
		}
		
		List<String> fileExt = new ArrayList<String>();
		//이미지 확장자
		if("Y".equals(fileVO.getFileExtImg())){
			fileExt.add("jpg");
			fileExt.add("jpeg");
			fileExt.add("gif");
			fileExt.add("png");
			fileExt.add("bmp");
		}
		//동영상 확장자
		if("Y".equals(fileVO.getFileExtImg())){
			fileExt.add("mkv");
			fileExt.add("avi");
			fileExt.add("mp4");
			fileExt.add("mpg");
			fileExt.add("flv");
			fileExt.add("wmv");
			fileExt.add("mov");
		}
		//기타 확장자
		if("Y".equals(fileVO.getFileExtImg())){
			fileExt.add("hwp");
			fileExt.add("doc");
			fileExt.add("docx");
			fileExt.add("ppt");
			fileExt.add("pptx");
			fileExt.add("xls");
			fileExt.add("xlsx");
		}
		fileVO.setSearchFileExtsn(fileExt);
		
		model.addAttribute("resultList", this.classManageService.selectSearchStudyFileList(fileVO, request));
		model.addAttribute("webPath", propertiesService.getString("Study.fileStoreWebPath"));

		return "cmm/fms/StudyFileList";
    }
    
    @RequestMapping("/cmm/fms/updateFilePublic.do")
    public void updateFilePublic(@ModelAttribute("searchVO") FileVO fileVO,HttpServletRequest request,HttpServletResponse response,ModelMap model) throws Exception {

    	Gson gson = new Gson();
    	JsonObject jObj = new JsonObject();

    	fileService.updateFilePublic(fileVO);
    	jObj.addProperty("successYn", "Y");

  	  	response.setContentType("text/javascript; charset=utf-8");
  	  	PrintWriter printwriter = response.getWriter();
  	  	printwriter.write(gson.toJson(jObj));
  	  	printwriter.flush();
  		printwriter.close();

    }
}
