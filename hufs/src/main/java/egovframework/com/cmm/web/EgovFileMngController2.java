package egovframework.com.cmm.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.JsonResponse;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.utl.fcc.service.EgovStringUtil;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
public class EgovFileMngController2 {
    
	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
	
	@RequestMapping(value = "/fileUpload.do", method = RequestMethod.POST)
	@SuppressWarnings("unchecked")
    public void upload(final MultipartHttpServletRequest request, @ModelAttribute("searchVO") FileVO searchVO, 
                  HttpServletResponse response) throws Exception {

		JsonResponse res = new JsonResponse();
		ArrayList<FileVO> result = new ArrayList<FileVO>();
	      
	    final Map<String, MultipartFile> files = request.getFileMap();
	    if(!files.isEmpty()) {
	    	List<FileVO> fileList = fileUtil.directParseFileInf(files, "D_", 0, "Board.fileStorePath", "Board.fileStoreWebPath", fileUtil.selectFileBassCours());
	    	result.addAll(fileList);
	    }
	    
	    res.setFiles(result);
	    res.setSuccess(true);
	    
	    Gson gson = new Gson();
	    
    	response.setContentType("text/javascript; charset=utf-8");
	    PrintWriter printwriter = response.getWriter();
		printwriter.write(gson.toJson(res));
		printwriter.flush();
		printwriter.close();
	    
    }
	/*
	@RequestMapping(value = "/mltmdFileUpload.do", method = RequestMethod.POST)
	@SuppressWarnings("unchecked")
	public void mltmdFileUpload(final MultipartHttpServletRequest request, @ModelAttribute("searchVO") FileVO searchVO, 
            HttpServletResponse response) throws Exception {

		JsonResponse res = new JsonResponse();
		ArrayList<MltmdFileDetailVO> result = null;
	    
	  final Map<String, MultipartFile> files = request.getFileMap();
	  if(!files.isEmpty()) {
	  	result = fileUtil.directParseMvpFileInf(files, searchVO.getMltmdClCode()+ "_", 0, searchVO.getMltmdClCode() + ".fileStorePath", searchVO.getFileStreCours());
	  }
	  res.setData(result);
	  res.setSuccess(true);
	  
	  Gson gson = new Gson();
	  
		response.setContentType("text/javascript; charset=utf-8");
	  PrintWriter printwriter = response.getWriter();
		printwriter.write(gson.toJson(res));
		printwriter.flush();
		printwriter.close();
	  
	}
	*/
    
}


