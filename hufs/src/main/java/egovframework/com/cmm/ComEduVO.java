package egovframework.com.cmm;

import egovframework.rte.fdl.string.EgovDateUtil;

public class ComEduVO extends ComDefaultVO {
	
	private static final long serialVersionUID = 1L;
	
	/** EDU_YEAR */
    private java.lang.String searchEduYear;
    
    /** SCHUL_ID */
    private java.lang.String searchSchulId;
    
    private java.lang.String searchSchulGradeCode;    
    
    /** GRADE_CODE */
    private java.lang.String searchGradeCode = "1";
    
    /** CLAS_CODE */
    private java.lang.String searchClasCode;
    
    private java.lang.String searchSemstrCode;
    
    private java.lang.String searchSbjectId;
    
    private java.lang.String searchWeekCode;
    
    private java.lang.String searchDfkCode;
    
    private java.lang.String searchPeriodCode;
    
    private java.lang.String searchClasNm;
    
    private java.lang.String searchTimeSeCode;
    
    private java.lang.String searchClasTimeAppyAt;
    
    private java.lang.String searchTimeTableAppyAt;
    
    private java.lang.String searchEcshgAt;
    
    private java.lang.String searchSbjectIncludeEcshgAt = "N";
    
    private java.lang.String searchCrsdsgnAt;
    
    //이전 년도/학년 여부.
    private java.lang.String yearGradeType;
    
    private java.lang.String searchSchulTypeCode;
    private java.lang.String searchSchulSeCode;
    private java.lang.String searchOfcdcCode;    
    
    private java.lang.String searchDate;
    
    private java.lang.String[] searchArrAuthorGradeChkCode;
    
    private java.lang.String searchProgAt;
    private java.lang.String searchUpperSbjectId;
    private java.lang.String searchSbjectNm;
    
	public java.lang.String getSearchEduYear() {
		return searchEduYear == null ? EgovDateUtil.getCurrentYearAsString() : searchEduYear;
	}

	public void setSearchEduYear(java.lang.String searchEduYear) {
		this.searchEduYear = searchEduYear;
	}

	public java.lang.String getSearchSchulId() {
		return searchSchulId;
	}

	public void setSearchSchulId(java.lang.String searchSchulId) {
		this.searchSchulId = searchSchulId;
	}

	public java.lang.String getSearchSchulGradeCode() {
		return searchSchulGradeCode;
	}

	public void setSearchSchulGradeCode(java.lang.String searchSchulGradeCode) {
		this.searchSchulGradeCode = searchSchulGradeCode;
	}

	public java.lang.String getSearchGradeCode() {
		return searchGradeCode;
	}

	public void setSearchGradeCode(java.lang.String searchGradeCode) {
		this.searchGradeCode = searchGradeCode;
	}

	public java.lang.String getSearchClasCode() {
		return searchClasCode;
	}

	public void setSearchClasCode(java.lang.String searchClasCode) {
		this.searchClasCode = searchClasCode;
	}
	
	public java.lang.String getSearchSemstrCode() {
		return searchSemstrCode;
	}

	public void setSearchSemstrCode(java.lang.String searchSemstrCode) {
		this.searchSemstrCode = searchSemstrCode;
	}

	public java.lang.String getSearchSbjectId() {
		return searchSbjectId;
	}

	public void setSearchSbjectId(java.lang.String searchSbjectId) {
		this.searchSbjectId = searchSbjectId;
	}

	public java.lang.String getSearchWeekCode() {
		return searchWeekCode;
	}

	public void setSearchWeekCode(java.lang.String searchWeekCode) {
		this.searchWeekCode = searchWeekCode;
	}

	public java.lang.String getSearchDfkCode() {
		return searchDfkCode;
	}

	public void setSearchDfkCode(java.lang.String searchDfkCode) {
		this.searchDfkCode = searchDfkCode;
	}

	public java.lang.String getSearchPeriodCode() {
		return searchPeriodCode;
	}

	public void setSearchPeriodCode(java.lang.String searchPeriodCode) {
		this.searchPeriodCode = searchPeriodCode;
	}

	public java.lang.String getSearchClasNm() {
		return searchClasNm;
	}

	public void setSearchClasNm(java.lang.String searchClasNm) {
		this.searchClasNm = searchClasNm;
	}

	public java.lang.String getSearchClasTimeAppyAt() {
		return searchClasTimeAppyAt;
	}

	public void setSearchClasTimeAppyAt(java.lang.String searchClasTimeAppyAt) {
		this.searchClasTimeAppyAt = searchClasTimeAppyAt;
	}

	public java.lang.String getSearchTimeTableAppyAt() {
		return searchTimeTableAppyAt;
	}

	public void setSearchTimeTableAppyAt(java.lang.String searchTimeTableAppyAt) {
		this.searchTimeTableAppyAt = searchTimeTableAppyAt;
	}

	public java.lang.String getSearchTimeSeCode() {
		return searchTimeSeCode;
	}

	public void setSearchTimeSeCode(java.lang.String searchTimeSeCode) {
		this.searchTimeSeCode = searchTimeSeCode;
	}

	public java.lang.String getSearchEcshgAt() {
		return searchEcshgAt;
	}

	public void setSearchEcshgAt(java.lang.String searchEcshgAt) {
		this.searchEcshgAt = searchEcshgAt;
	}

	public java.lang.String getSearchSbjectIncludeEcshgAt() {
		return searchSbjectIncludeEcshgAt;
	}

	public void setSearchSbjectIncludeEcshgAt(java.lang.String searchSbjectIncludeEcshgAt) {
		this.searchSbjectIncludeEcshgAt = searchSbjectIncludeEcshgAt;
	}

	public java.lang.String getSearchSchulTypeCode() {
		return searchSchulTypeCode;
	}

	public void setSearchSchulTypeCode(java.lang.String searchSchulTypeCode) {
		this.searchSchulTypeCode = searchSchulTypeCode;
	}

	public java.lang.String getSearchSchulSeCode() {
		return searchSchulSeCode;
	}

	public void setSearchSchulSeCode(java.lang.String searchSchulSeCode) {
		this.searchSchulSeCode = searchSchulSeCode;
	}

	public java.lang.String getSearchOfcdcCode() {
		return searchOfcdcCode;
	}

	public void setSearchOfcdcCode(java.lang.String searchOfcdcCode) {
		this.searchOfcdcCode = searchOfcdcCode;
	}

	public java.lang.String getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(java.lang.String searchDate) {
		this.searchDate = searchDate;
	}

	public java.lang.String getSearchCrsdsgnAt() {
		return searchCrsdsgnAt;
	}

	public void setSearchCrsdsgnAt(java.lang.String searchCrsdsgnAt) {
		this.searchCrsdsgnAt = searchCrsdsgnAt;
	}

	public java.lang.String getYearGradeType() {
		return yearGradeType;
	}

	public void setYearGradeType(java.lang.String yearGradeType) {
		this.yearGradeType = yearGradeType;
	}

	public java.lang.String[] getSearchArrAuthorGradeChkCode() {
		return searchArrAuthorGradeChkCode;
	}

	public void setSearchArrAuthorGradeChkCode(java.lang.String[] searchArrAuthorGradeChkCode) {
		this.searchArrAuthorGradeChkCode = searchArrAuthorGradeChkCode;
	}

	public java.lang.String getSearchProgAt() {
		return searchProgAt;
	}

	public void setSearchProgAt(java.lang.String searchProgAt) {
		this.searchProgAt = searchProgAt;
	}

	public java.lang.String getSearchUpperSbjectId() {
		return searchUpperSbjectId;
	}

	public void setSearchUpperSbjectId(java.lang.String searchUpperSbjectId) {
		this.searchUpperSbjectId = searchUpperSbjectId;
	}

	public java.lang.String getSearchSbjectNm() {
		return searchSbjectNm;
	}

	public void setSearchSbjectNm(java.lang.String searchSbjectNm) {
		this.searchSbjectNm = searchSbjectNm;
	}


	
    
}
