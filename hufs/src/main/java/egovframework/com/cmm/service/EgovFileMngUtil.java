package egovframework.com.cmm.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import egovframework.com.utl.fcc.service.EgovFormBasedFileUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;

/**
 * @Class Name  : EgovFileMngUtil.java
 * @Description : 메시지 처리 관련 유틸리티
 * @Modification Information
 * 
 *     수정일         수정자                   수정내용
 *     -------          --------        ---------------------------
 *   2009.02.13       이삼섭                  최초 생성
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 02. 13
 * @version 1.0
 * @see 
 * 
 */
@Component("EgovFileMngUtil")
public class EgovFileMngUtil {

    public static final int BUFF_SIZE = 4096;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertyService;

    @Resource(name = "egovFileIdGnrService")
    private EgovIdGnrService idgenService;

    Logger log = Logger.getLogger(this.getClass());
    
    public static String[] denyFileExtentionList = new String[]{"sh", "exe", "jsp", "asp", "php", "js", "jar", "jspx"};
    public static String[] isImageList = new String[] {"jpg", "jpeg", "gif", "png", "bmp"};

    /**
     * 다이렉트 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> directParseFileInf(Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String storePath, String appendPath)  {
	int fileKey = fileKeyParam;
	
	String storePathString = "";
	String atchFileIdString = "";

	if ("".equals(storePath) || storePath == null) {
	    storePathString = propertyService.getString("Globals.fileStorePath");
	} else {
	    storePathString = propertyService.getString(storePath);
	}
	
	if (!("".equals(appendPath) || appendPath == null)) {
	    storePathString = storePathString + "/" + appendPath;
	}

	File saveFolder = new File(storePathString);
	
	if (!saveFolder.exists() || saveFolder.isFile()) {
	    saveFolder.mkdirs();
	}

	List<FileVO> result  = new ArrayList<FileVO>();
	
	try {
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;
		String filePath = "";
		
		FileVO fvo;
	
		boolean isDenyed;
		while (itr.hasNext()) {
		    Entry<String, MultipartFile> entry = itr.next();
	
		    file = entry.getValue();
		    String orginFileName = file.getOriginalFilename();
		    
		    //--------------------------------------
		    // 원 파일명이 없는 경우 처리
		    // (첨부가 되지 않은 input file type)
		    //--------------------------------------
		    if ("".equals(orginFileName)) {
			continue;
		    }
		    ////------------------------------------
	
		    String newName = "";
		    String fileExt = "";
		    int index = orginFileName.lastIndexOf(".");
		    if(index != -1) {
		    	fileExt = orginFileName.substring(index + 1);
		    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey + "." + fileExt;
		    } else {
		    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
		    }
		    
		    isDenyed = false;
		    for(int i=0; i < denyFileExtentionList.length; i++) {
		    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
		    		isDenyed = true;
		    		break;
		    	}
		    }	    
		    if (isDenyed) {
				continue;
			}
		    
		    long _size = file.getSize();
	
		    if (!"".equals(orginFileName)) {
		    	filePath = storePathString + "/" + newName;
		    	file.transferTo(new File(filePath));
		    }
		    
		    fvo = new FileVO();
		    boolean isImage = false;
		    for(int i=0; i < isImageList.length; i++) {
		    	if(fileExt.toLowerCase().equals(isImageList[i])) {
		    		isImage = true;
		    		break;
		    	}
		    }
		    if(isImage) {
		    	BufferedImage image = ImageIO.read(file.getInputStream());
		    	int width = image.getWidth();
		    	int height = image.getHeight();
		    	fvo.setFileImgWidth(width);
		    	fvo.setFileImgHeight(height);
		    }
		    
		    fvo.setFileExtsn(fileExt);
		    fvo.setFileStreCours(storePathString);
		    fvo.setFileMg(Long.toString(_size));
		    fvo.setOrignlFileNm(orginFileName);
		    fvo.setStreFileNm(newName);
		    fvo.setAtchFileId(atchFileIdString);
		    fvo.setFileSn(String.valueOf(fileKey));
		    fvo.setFormNm(entry.getKey());
	
		    //writeFile(file, newName, storePathString);
		    result.add(fvo);
		    
		    fileKey++;
		} 
	} catch(IllegalStateException e) {
		log.debug("IllegalStateException: "+ e.getMessage());
	} catch(IOException e) {
		log.debug("IOException: "+ e.getMessage());
	} catch(Exception e) {
		log.debug("Exception: "+ e.getMessage());
	}
	

	return result;
    }
    
    /**
     * Not Separater
     * @param files
     * @param KeyStr
     * @param fileKeyParam
     * @param storePath
     * @param appendPath
     * @return
     * @throws Exception
     */
    public List<FileVO> directParseFileInfNoSeparator(Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String storePath, String appendPath) throws Exception {
    	int fileKey = fileKeyParam;
    	
    	String storePathString = "";
    	String atchFileIdString = "";

    	if ("".equals(storePath) || storePath == null) {
    	    storePathString = propertyService.getString("Globals.fileStorePath");
    	} else {
    	    storePathString = propertyService.getString(storePath);
    	}
    	
    	if (!("".equals(appendPath) || appendPath == null)) {
    	    storePathString = storePathString + appendPath;
    	}

    	File saveFolder = new File(storePathString);
    	
    	if (!saveFolder.exists() || saveFolder.isFile()) {
    	    saveFolder.mkdirs();
    	}

    	List<FileVO> result  = new ArrayList<FileVO>();
    	try {
	    	Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
	    	MultipartFile file;
	    	String filePath = "";
	    	
	    	FileVO fvo;
	
	    	boolean isDenyed;
	    	while (itr.hasNext()) {
	    	    Entry<String, MultipartFile> entry = itr.next();
	
	    	    file = entry.getValue();
	    	    String orginFileName = file.getOriginalFilename();
	    	    
	    	    //--------------------------------------
	    	    // 원 파일명이 없는 경우 처리
	    	    // (첨부가 되지 않은 input file type)
	    	    //--------------------------------------
	    	    if ("".equals(orginFileName)) {
	    		continue;
	    	    }
	    	    ////------------------------------------
	
	    	    String newName = "";
	    	    String fileExt = "";
	    	    int index = orginFileName.lastIndexOf(".");
	    	    if(index != -1) {
	    	    	fileExt = orginFileName.substring(index + 1);
	    	    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey + "." + fileExt;
	    	    } else {
	    	    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
	    	    }
	    	    
	    	    isDenyed = false;
	    	    for(int i=0; i < denyFileExtentionList.length; i++) {
	    	    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
	    	    		isDenyed = true;
	    	    		break;
	    	    	}
	    	    }	    
	    	    if (isDenyed) {
	    			continue;
	    		}
	    	    
	    	    long _size = file.getSize();
	
	    	    if (!"".equals(orginFileName)) {
	    	    	filePath = storePathString + "/" + newName;
	    	    	file.transferTo(new File(filePath));
	    	    }
	    	    fvo = new FileVO();
	    	    fvo.setFileExtsn(fileExt);
	    	    fvo.setFileStreCours(storePathString);
	    	    fvo.setFileMg(Long.toString(_size));
	    	    fvo.setOrignlFileNm(orginFileName);
	    	    fvo.setStreFileNm(newName);
	    	    fvo.setAtchFileId(atchFileIdString);
	    	    fvo.setFileSn(String.valueOf(fileKey));
	    	    fvo.setFormNm(entry.getKey());
	
	    	    //writeFile(file, newName, storePathString);
	    	    result.add(fvo);
	    	    
	    	    fileKey++;
	    	}
    	
    	} catch(IllegalStateException e) {
    		log.debug("IllegalStateException: "+ e.getMessage());
    	} catch(IOException e) {
    		log.debug("IOException: "+ e.getMessage());
    	} catch(Exception e) {
    		log.debug("Exception: "+ e.getMessage());
    	}

    	return result;
        }
    
    /**
     * 일반 게시판 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> parseBoardFileInf(long maxFileSize, Map<String, MultipartFile> files, int fileKeyParam, String atchFileId, String aspCode, String bbsId, String la, String lo) throws Exception {
    	
    	return parseFileInf(maxFileSize, files, "BBS_", fileKeyParam, atchFileId, "Board.fileStorePath", aspCode + "/" + bbsId, la, lo);
    }
    
    /**
     * 양식 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> parseBaseFileInf(long maxFileSize, Map<String, MultipartFile> files, int fileKeyParam, String atchFileId, String aspCode, String la, String lo) throws Exception {
    	
    	return parseFileInf(maxFileSize, files, "BASE_", fileKeyParam, atchFileId, "Base.fileStorePath", "", la, lo);
    }
    
    /**
     * 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> parseFileInf(long maxFileSize, Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String atchFileId, String storePath, String appendPath, String la, String lo) throws Exception {
	int fileKey = fileKeyParam;
	
	String storePathString = "";
	String atchFileIdString = "";

	if ("".equals(storePath) || storePath == null) {
	    storePathString = propertyService.getString("Globals.fileStorePath");
	} else {
	    storePathString = propertyService.getString(storePath);
	}
	
	if (!("".equals(appendPath) || appendPath == null)) {
	    storePathString = storePathString + "/" + appendPath;
	}

	if ("".equals(atchFileId) || atchFileId == null) {
	    atchFileIdString = idgenService.getNextStringId();
	} else {
	    atchFileIdString = atchFileId;
	}

	File saveFolder = new File(storePathString);

	if (!saveFolder.exists() || saveFolder.isFile()) {
	    saveFolder.mkdirs();
	}
	
	List<FileVO> result  = new ArrayList<FileVO>();
	
	try {
		
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;
		String filePath = "";
		
		FileVO fvo;
	
		boolean isDenyed;
		while (itr.hasNext()) {
		    Entry<String, MultipartFile> entry = itr.next();
	
		    file = entry.getValue();
		    String orginFileName = file.getOriginalFilename();
		    
		    //--------------------------------------
		    // 원 파일명이 없는 경우 처리
		    // (첨부가 되지 않은 input file type)
		    //--------------------------------------
		    if ("".equals(orginFileName)) {
			continue;
		    }
		    
		    ////------------------------------------
	
		    String fileExt = "";
		    int index = orginFileName.lastIndexOf(".");
		    if(index != -1) {
		    	fileExt = orginFileName.substring(index + 1);
		    } 
		    
		    isDenyed = false;
		    for(int i=0; i < denyFileExtentionList.length; i++) {
		    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
		    		isDenyed = true;
		    		break;
		    	}
		    }	    
		    if (isDenyed) {
				continue;
			}
		    
		    String newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
		    long _size = file.getSize();
		    if(_size > maxFileSize) {
		    	continue;
		    }
	
		    if (!"".equals(orginFileName)) {
		    	filePath = storePathString + "/" + newName;
		    	file.transferTo(new File(filePath));
		    }
		    
		    fvo = new FileVO();
		    fvo.setFileExtsn(fileExt);
		    fvo.setFileStreCours(storePathString);
		    fvo.setFileMg(Long.toString(_size));
		    fvo.setOrignlFileNm(orginFileName);
		    fvo.setStreFileNm(newName);
		    fvo.setAtchFileId(atchFileIdString);
		    fvo.setFileSn(String.valueOf(fileKey));
		    fvo.setFormNm(entry.getKey());
		    fvo.setLa(la);
		    fvo.setLo(lo);
	
		    //writeFile(file, newName, storePathString);
		    result.add(fvo);
		    
		    fileKey++;
		}
	} catch(IllegalStateException e) {
		log.debug("IllegalStateException: "+ e.getMessage());
	} catch(IOException e) {
		log.debug("IOException: "+ e.getMessage());
	} catch(Exception e) {
		log.debug("Exception: "+ e.getMessage());
	}

	return result;
    }

    
    
    /**
     * 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> parseFileInfV1(long maxFileSize, Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String atchFileId, String storePath, String storeWebPath, String appendPath) throws Exception {
	int fileKey = fileKeyParam;
	
	String storePathString = "";
	String storeWebPathString = "";
	String atchFileIdString = "";

	if ("".equals(storePath) || storePath == null) {
	    storePathString = propertyService.getString("Globals.fileStorePath");
	    storeWebPathString = propertyService.getString("Globals.fileStoreWebPath");	    
	} else {
	    storePathString = propertyService.getString(storePath);
	    storeWebPathString = propertyService.getString(storeWebPath);
	}
	
	if (!("".equals(appendPath) || appendPath == null)) {
	    storePathString = storePathString + "/" + appendPath;
	    storeWebPathString = storeWebPathString + "/" + appendPath;
	}
	

	if ("".equals(atchFileId) || atchFileId == null) {
	    atchFileIdString = idgenService.getNextStringId();
	} else {
	    atchFileIdString = atchFileId;
	}

	File saveFolder = new File(storePathString);
	
	if (!saveFolder.exists() || saveFolder.isFile()) {
	    saveFolder.mkdirs();
	}

	List<FileVO> result  = new ArrayList<FileVO>();
	
	try {
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;
		String filePath = "";
		
		FileVO fvo;
	
		boolean isDenyed;
		while (itr.hasNext()) {
		    Entry<String, MultipartFile> entry = itr.next();
	
		    file = entry.getValue();
		    String orginFileName = file.getOriginalFilename();
		    
		    //--------------------------------------
		    // 원 파일명이 없는 경우 처리
		    // (첨부가 되지 않은 input file type)
		    //--------------------------------------
		    if ("".equals(orginFileName)) {
			continue;
		    }
		    
		    ////------------------------------------
	
		    String fileExt = "";
		    int index = orginFileName.lastIndexOf(".");
		    if(index != -1) {
		    	fileExt = orginFileName.substring(index + 1);
		    } 
		    
		    isDenyed = false;
		    for(int i=0; i < denyFileExtentionList.length; i++) {
		    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
		    		isDenyed = true;
		    		break;
		    	}
		    }	    
		    if (isDenyed) {
				continue;
			}
		    
		    String newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
		    long _size = file.getSize();
		    if(_size > maxFileSize) {
		    	continue;
		    }
	
		    if (!"".equals(orginFileName)) {
		    	filePath = storePathString + "/" + newName;
		    	file.transferTo(new File(filePath));
		    }
		    
		    fvo = new FileVO();
		    fvo.setFileExtsn(fileExt);
		    fvo.setFileStreCours(storePathString);
		    fvo.setFileMg(Long.toString(_size));
		    fvo.setOrignlFileNm(orginFileName);
		    fvo.setStreFileNm(newName);
		    fvo.setAtchFileId(atchFileIdString);
		    fvo.setFileSn(String.valueOf(fileKey));
		    fvo.setFormNm(entry.getKey());
		    fvo.setFileStreWebCours(storeWebPathString);
	
		    //writeFile(file, newName, storePathString);
		    result.add(fvo);
		    
		    fileKey++;
		}
	} catch(IllegalStateException e) {
		log.debug("IllegalStateException: "+ e.getMessage());
	} catch(IOException e) {
		log.debug("IOException: "+ e.getMessage());
	} catch(Exception e) {
		log.debug("Exception: "+ e.getMessage());
	}

	return result;
    }
    
    
    /**
     * 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> parseFileInfV2(long maxFileSize, Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String atchFileId, String storePath, String storeWebPath, String appendPath, long maxChunkSize, long fileFullLength, long chunkFrom, long chunkTo) throws Exception {
		int fileKey = fileKeyParam;
		
		String storePathString = "";
		String storeWebPathString = "";
		String atchFileIdString = "";
	
		if ("".equals(storePath) || storePath == null) {
		    storePathString = propertyService.getString("Globals.fileStorePath");
		    storeWebPathString = propertyService.getString("Globals.fileStoreWebPath");	    
		} else {
		    storePathString = propertyService.getString(storePath);
		    storeWebPathString = propertyService.getString(storeWebPath);
		}
		
		if (!("".equals(appendPath) || appendPath == null)) {
		    storePathString = storePathString + "/" + appendPath;
		    storeWebPathString = storeWebPathString + "/" + appendPath;
		}
		
	
		if ("".equals(atchFileId) || atchFileId == null) {
		    atchFileIdString = idgenService.getNextStringId();
		} else {
		    atchFileIdString = atchFileId;
		}
	
		File saveFolder = new File(storePathString);
		
		if (!saveFolder.exists() || saveFolder.isFile()) {
		    saveFolder.mkdirs();
		}
	
		List<FileVO> result  = new ArrayList<FileVO>();
		
		try {
			Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
			MultipartFile file;
			
			FileVO fvo;
		
			boolean isDenyed;
			while (itr.hasNext()) {
			    Entry<String, MultipartFile> entry = itr.next();
		
			    file = entry.getValue();
			    String orginFileName = file.getOriginalFilename();
			    
			    //--------------------------------------
			    // 원 파일명이 없는 경우 처리
			    // (첨부가 되지 않은 input file type)
			    //--------------------------------------
			    if ("".equals(orginFileName)) {
			    	continue;
			    }
			    
			    ////------------------------------------
		
			    String fileExt = "";
			    int index = orginFileName.lastIndexOf(".");
			    if(index != -1) {
			    	fileExt = orginFileName.substring(index + 1);
			    } 
			    
			    isDenyed = false;
			    for(int i=0; i < denyFileExtentionList.length; i++) {
			    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
			    		isDenyed = true;
			    		break;
			    	}
			    }	    
			    if (isDenyed) {
					continue;
				}
			    
			    String newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
			    String filePath = storePathString + "/" + newName;
			    
			    String tmpFileName = atchFileId + "_" + String.valueOf(orginFileName.hashCode());
			    long _size = 0;
			    
			    boolean isChunk = fileFullLength < 0 ? false : true;
			    boolean isLastChunk = chunkTo == fileFullLength-1;
			    
			    if(isChunk) {
			    	File saveFile = new File(storePathString + "/" + tmpFileName);
			    	EgovFormBasedFileUtil.saveFile(file.getInputStream(), saveFile, true);
					
					if(isLastChunk) {
						_size = saveFile.length();
						EgovFormBasedFileUtil.renameFile(storePathString, tmpFileName, newName);
					} else {
						continue;
					}
			    } else {
			    	_size = file.getSize();
			    	file.transferTo(new File(filePath));
			    }
			    
			    if(_size > maxFileSize) {
			    	File delFile = new File(filePath);
			    	if(delFile.exists() && delFile.isFile()) {
			    		delFile.delete();
			    	}
			    	continue;
			    }
			    
			    fvo = new FileVO();
			    fvo.setFileExtsn(fileExt);
			    fvo.setFileStreCours(storePathString);
			    fvo.setFileMg(Long.toString(_size));
			    fvo.setOrignlFileNm(orginFileName);
			    fvo.setStreFileNm(newName);
			    fvo.setAtchFileId(atchFileIdString);
			    fvo.setFileSn(String.valueOf(fileKey));
			    fvo.setFormNm(entry.getKey());
			    fvo.setFileStreWebCours(storeWebPathString);
		
			    //writeFile(file, newName, storePathString);
			    result.add(fvo);
			    
			    fileKey++;
			}
		} catch(IllegalStateException e) {
			log.debug("IllegalStateException: "+ e.getMessage());
		} catch(IOException e) {
			log.debug("IOException: "+ e.getMessage());
		} catch(Exception e) {
			log.debug("Exception: "+ e.getMessage());
		}

	return result;
    }
    
    /**
     * 첨부파일을 서버에 저장한다.
     * 
     * @param file
     * @param newName
     * @param stordFilePath
     * @throws Exception
     */
    protected void writeUploadedFile(MultipartFile file, String newName, String stordFilePath) throws Exception {
	InputStream stream = null;
	OutputStream bos = null;
	
	try {
	    stream = file.getInputStream();
	    File cFile = new File(stordFilePath);

	    if (!cFile.isDirectory()) {
		boolean _flag = cFile.mkdir();
		if (!_flag) {
		    throw new IOException("Directory creation Failed ");
		}
	    }

	    bos = new FileOutputStream(stordFilePath + "/" + newName);

	    int bytesRead = 0;
	    byte[] buffer = new byte[BUFF_SIZE];

	    while ((bytesRead = stream.read(buffer, 0, BUFF_SIZE)) != -1) {
		bos.write(buffer, 0, bytesRead);
	    }
	} catch (FileNotFoundException fnfe) {
		log.error(fnfe);
	} catch (IOException ioe) {
		log.error(ioe);
	} catch (Exception e) {
		log.error(e);
	} finally {
	    if (bos != null) {
		try {
		    bos.close();
		} catch (IOException ignore) {
		    log.debug("IGNORED: " + ignore.getMessage());
		}
	    }
	    if (stream != null) {
		try {
		    stream.close();
		} catch (IOException ignore) {
		    log.debug("IGNORED: " + ignore.getMessage());
		}
	    }
	}
    }	
	
    /**
     * 대용량파일을 Upload 처리한다.
     * 
     * @param request
     * @param where
     * @param maxFileSize
     * @return
     * @throws Exception
     */
    public FileVO uploadStreamLongFiles(InputStream is, FileVO fileVO, boolean firstChunk, boolean lastChunk) throws Exception {
    	
    	File file = new File(fileVO.getFileStreCours() + "/" + fileVO.getStreFileNm());
    	
    	if(firstChunk) {
    		String fileExt = "";
		    int index = fileVO.getOrignlFileNm().lastIndexOf(".");
		    if(index != -1) {
		    	fileExt = fileVO.getOrignlFileNm().substring(index + 1);
		    } 
    		boolean isDenyed = false;
    	    for(int i=0; i < denyFileExtentionList.length; i++) {
    	    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
    	    		isDenyed = true;
    	    		break;
    	    	}
    	    }	    
    	    if (isDenyed) {
    			//throw new IOException("등록할 수 없는 파일 종류입니다.");
    	    	return null;
    		}
    	    
	    	if (! file.getParentFile().exists()) {
			    file.getParentFile().mkdirs();
			}
    	}
    	
    	Exception proEx = null;
    	OutputStream os = null;		
		try {
		    os = new FileOutputStream(file, true);
		    
		    int bytesRead = 0;
		    byte[] buffer = new byte[BUFF_SIZE];
		    
		    while ((bytesRead = is.read(buffer, 0, BUFF_SIZE)) != -1) {
		    	os.write(buffer, 0, bytesRead);
		    }
		} catch (FileNotFoundException ex) {
			proEx = ex;
		} catch (IOException ex) {
			proEx = ex;
		} catch(Exception ex) {
			proEx = ex;
		} finally {
		    if (os != null) {
		    	os.close();
		    }
		}
		
		if(proEx != null) {
			throw proEx;
		}
		
		if(lastChunk) {
			
			String fileExt = "";
		    int index = fileVO.getOrignlFileNm().lastIndexOf(".");
		    if(index != -1) {
		    	fileExt = fileVO.getOrignlFileNm().substring(index + 1);
		    } 
		   
		    fileVO.setFileExtsn(fileExt);
		    fileVO.setFileMg(Long.toString(file.length()));	
		}
		
        return fileVO;
    }
    
    public String selectFileBassCours() {
		java.util.Calendar cal = java.util.Calendar.getInstance();		
		int iYear = cal.get(java.util.Calendar.YEAR);
		int iMonth = cal.get(java.util.Calendar.MONTH);
		int iDate = cal.get(java.util.Calendar.DATE);		
		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth+1).length() == 1 ? "0" + Integer.toString(iMonth+1) : Integer.toString(iMonth+1); 
		String sDay = Integer.toString(iDate).length() == 1 ? "0" + Integer.toString(iDate) : Integer.toString(iDate); 
		
		return "/" + sYear + "/" + sMonth + "/" + sDay;
    }
    
    /**
     * 다이렉트 첨부파일에 대한 목록 정보를 취득한다.
     * 
     * @param files
     * @return
     * @throws Exception
     */
    public List<FileVO> directParseFileInf(Map<String, MultipartFile> files, String KeyStr, int fileKeyParam, String storePath, String storeWebPath, String appendPath) throws Exception {
	int fileKey = fileKeyParam;
	
	String storePathString = "";
	String storeWebPathString = "";
	String atchFileIdString = "";

	if ("".equals(storePath) || storePath == null) {
	    storePathString = propertyService.getString("Globals.fileStorePath");
	    storeWebPathString = propertyService.getString("Globals.fileStoreWebPath");	    
	} else {
	    storePathString = propertyService.getString(storePath);
	    storeWebPathString = propertyService.getString(storeWebPath);
	}
	
	if (!("".equals(appendPath) || appendPath == null)) {
	    storePathString = storePathString + appendPath;
	    storeWebPathString = storeWebPathString + appendPath;
	}

	File saveFolder = new File(storePathString);
	
	if (!saveFolder.exists() || saveFolder.isFile()) {
	    saveFolder.mkdirs();
	}

	Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
	MultipartFile file;
	String filePath = "";
	List<FileVO> result  = new ArrayList<FileVO>();
	FileVO fvo;

	boolean isDenyed;
	while (itr.hasNext()) {
	    Entry<String, MultipartFile> entry = itr.next();

	    file = entry.getValue();
	    String orginFileName = file.getOriginalFilename();
	    
	    //--------------------------------------
	    // 원 파일명이 없는 경우 처리
	    // (첨부가 되지 않은 input file type)
	    //--------------------------------------
	    if ("".equals(orginFileName)) {
		continue;
	    }
	    ////------------------------------------

	    String newName = "";
	    String fileExt = "";
	    int index = orginFileName.lastIndexOf(".");
	    if(index != -1) {
	    	fileExt = orginFileName.substring(index + 1);
	    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey + "." + fileExt;
	    } else {
	    	newName = KeyStr + EgovStringUtil.getTimeStamp() + fileKey;
	    }
	    
	    isDenyed = false;
	    for(int i=0; i < denyFileExtentionList.length; i++) {
	    	if(fileExt.toLowerCase().equals(denyFileExtentionList[i])) {
	    		isDenyed = true;
	    		break;
	    	}
	    }	    
	    if (isDenyed) {
			continue;
		}
	    
	    long _size = file.getSize();

	    if (!"".equals(orginFileName)) {
	    	filePath = storePathString + "/" + newName;
	    	file.transferTo(new File(filePath));
	    }
	    fvo = new FileVO();
	    fvo.setFileExtsn(fileExt);
	    fvo.setFileStreCours(storeWebPathString);
	    //fvo.setFileStreCours(storePathString);
	    //fvo.setFileStreWebCours(storeWebPathString);
	    fvo.setFileMg(Long.toString(_size));
	    fvo.setOrignlFileNm(orginFileName);
	    fvo.setStreFileNm(newName);
	    fvo.setAtchFileId(atchFileIdString);
	    fvo.setFileSn(String.valueOf(fileKey));
	    fvo.setFormNm(entry.getKey());

	    //writeFile(file, newName, storePathString);
	    result.add(fvo);
	    
	    fileKey++;
	}

	return result;
    }
}
