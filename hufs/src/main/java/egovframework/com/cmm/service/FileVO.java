package egovframework.com.cmm.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import egovframework.com.cmm.ComDefaultVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : FileVO.java
 * @Description : 파일정보 처리를 위한 VO 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 25.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 25.
 * @version
 * @see
 *
 */
@SuppressWarnings("serial")
public class FileVO extends ComDefaultVO {

    /**
	 * 첨부파일 아이디
	 */
    public String atchFileId = "";
    /**
	 * 생성일자
	 */
    public java.util.Date creatDt;
    /**
	 * 파일내용
	 */
    public String fileCn = "";
    /**
	 * 파일확장자
	 */
    public String fileExtsn = "";
    /**
	 * 파일크기
	 */
    public String fileMg = "";
    /**
	 * 파일연번
	 */
    public String fileSn = "";
    /**
	 * 파일저장경로
	 */
    public String fileStreCours = "";
    /**
	 * 원파일명
	 */
    public String orignlFileNm = "";
    /**
	 * 저장파일명
	 */
    public String streFileNm = "";

    /**
	 * 폼명
	 */
    public String formNm = "";

    /**
	 * 임시파일그룹ID
	 */
    public String fileGroupId = "";

    /**
	 * 임시파일ID
	 */
    public String tmprFileId = "";

    /**
	 * 파일갯수
	 */
    public int fileSnCount = 0;

    /**
	 * 프로그램ID
	 */
    public String progrmId = "";

    /**
	 * 사용여부
	 */
    public String useAt = "Y";

    /**
	 * 확장파일여부
	 */
    public String estnAt = "";

    /**
	 * 총파일크기
	 */
    public String totalFileMg = "0";

    /**
	 * 총파일갯수
	 */
    public String totalFileCount = "0";

    /**
	 * 첨부파일아이디리스트
	 */
    private List<String> atchFileIdArr;

    /** 이전 첨부파일아이디 */
    private String oldAtchFileId;

    /** 위도 */
    private String la;

    /** 경도 */
    private String lo;

    /** 이미지 orientation */
    private Integer orientation;


    private String sysTyCode;

    private String siteId;

    private String pathKey;

    private String appendPath;

    private long maxMegaFileSize;

    private String editorId;

    /**
	 * 다운로드건수
	 */
    private BigDecimal downCnt;

    private String mltmdClCode;

    /**
	 * 파일저장웹경로
	 */
    public String fileStreWebCours = "";

    private String isNoTempFile = "N";

    private long maxChunkSize;

    //과정코드ID
    private String crclId;

    //공개여부
    private String publicAt;

    //등록자ID
    private String frstRegisterId;

    //등록시점
    private java.util.Date frstRegisterPnttm;

    private String searchCtgryId;

    private String searchCrclId;

    private String searchFileNm;

    private String searchRegisterNm;

    private List<String> searchFileExtsn;

    private String searchUseCnt;

    private String searchScope;

    private String searchCrclNm;

    private String searchPublicAt;

    private String fileExtNm;

    private String hostCode;

    private String searchFrstRegisterId;

    private String plId;

    //이미지 확장자
    private String fileExtImg;

    //기타(파일) 확장자
    private String fileExtOther;

    //동영상 확장자
    private String fileExtMov;


    //다운로드 타입(arr:파일아이디 여러개, 나머지는 한개)
    private String downLoadType;

    private String viewType;

    private String dateType;

    private String searchCrclLang;

    private String searchPlTitle;

    private String searchTeacher;
    
    //학생여부
    private String studentAt;
    
    //회원ID
    private String userId;
    
    //콘텐츠 사용여부(나의 교육과정)
    private String addAt;
    
    //파일 가로 크기
    private int fileImgWidth;
    
    //파일 세로 크기
    private int fileImgHeight;
    
    /**
	 * atchFileId attribute를 리턴한다.
	 * @return  the atchFileId
	 */
    public String getAtchFileId() {
	return atchFileId;
    }

    /**
	 * atchFileId attribute 값을 설정한다.
	 * @param atchFileId  the atchFileId to set
	 */
    public void setAtchFileId(String atchFileId) {
	this.atchFileId = atchFileId;
    }

    /**
	 * creatDt attribute를 리턴한다.
	 * @return  the creatDt
	 */
    public java.util.Date getCreatDt() {
	return creatDt;
    }

    /**
	 * creatDt attribute 값을 설정한다.
	 * @param creatDt  the creatDt to set
	 */
    public void setCreatDt(java.util.Date creatDt) {
	this.creatDt = creatDt;
    }

    /**
	 * fileCn attribute를 리턴한다.
	 * @return  the fileCn
	 */
    public String getFileCn() {
	return fileCn;
    }

    /**
	 * fileCn attribute 값을 설정한다.
	 * @param fileCn  the fileCn to set
	 */
    public void setFileCn(String fileCn) {
	this.fileCn = fileCn;
    }

    /**
	 * fileExtsn attribute를 리턴한다.
	 * @return  the fileExtsn
	 */
    public String getFileExtsn() {
	return fileExtsn;
    }

    /**
	 * fileExtsn attribute 값을 설정한다.
	 * @param fileExtsn  the fileExtsn to set
	 */
    public void setFileExtsn(String fileExtsn) {
	this.fileExtsn = fileExtsn;
    }

    /**
	 * fileMg attribute를 리턴한다.
	 * @return  the fileMg
	 */
    public String getFileMg() {
	return fileMg;
    }

    /**
	 * fileMg attribute 값을 설정한다.
	 * @param fileMg  the fileMg to set
	 */
    public void setFileMg(String fileMg) {
	this.fileMg = fileMg;
    }

    /**
	 * fileSn attribute를 리턴한다.
	 * @return  the fileSn
	 */
    public String getFileSn() {
	return fileSn;
    }

    /**
	 * fileSn attribute 값을 설정한다.
	 * @param fileSn  the fileSn to set
	 */
    public void setFileSn(String fileSn) {
	this.fileSn = fileSn;
    }

    /**
	 * fileStreCours attribute를 리턴한다.
	 * @return  the fileStreCours
	 */
    public String getFileStreCours() {
	return fileStreCours;
    }

    /**
	 * fileStreCours attribute 값을 설정한다.
	 * @param fileStreCours  the fileStreCours to set
	 */
    public void setFileStreCours(String fileStreCours) {
	this.fileStreCours = fileStreCours;
    }

    /**
	 * orignlFileNm attribute를 리턴한다.
	 * @return  the orignlFileNm
	 */
    public String getOrignlFileNm() {
	return orignlFileNm;
    }

    /**
	 * orignlFileNm attribute 값을 설정한다.
	 * @param orignlFileNm  the orignlFileNm to set
	 */
    public void setOrignlFileNm(String orignlFileNm) {
	this.orignlFileNm = orignlFileNm;
    }

    /**
	 * streFileNm attribute를 리턴한다.
	 * @return  the streFileNm
	 */
    public String getStreFileNm() {
	return streFileNm;
    }

    /**
	 * streFileNm attribute 값을 설정한다.
	 * @param streFileNm  the streFileNm to set
	 */
    public void setStreFileNm(String streFileNm) {
	this.streFileNm = streFileNm;
    }

    /**
	 * formNm attribute를 리턴한다.
	 * @return  the formNm
	 */
    public String getFormNm() {
	return formNm;
    }

    /**
	 * formNm attribute 값을 설정한다.
	 * @param formNm  the formNm to set
	 */
    public void setFormNm(String formNm) {
	this.formNm = formNm;
    }

    public String getFileGroupId() {
		return fileGroupId;
	}

	public void setFileGroupId(String fileGroupId) {
		this.fileGroupId = fileGroupId;
	}

	public String getTmprFileId() {
		return tmprFileId;
	}

	public void setTmprFileId(String tmprFileId) {
		this.tmprFileId = tmprFileId;
	}

	public int getFileSnCount() {
		return fileSnCount;
	}

	public void setFileSnCount(int fileSnCount) {
		this.fileSnCount = fileSnCount;
	}

	public String getProgrmId() {
		return progrmId;
	}

	public void setProgrmId(String progrmId) {
		this.progrmId = progrmId;
	}

	public String getUseAt() {
		return useAt;
	}

	public void setUseAt(String useAt) {
		this.useAt = useAt;
	}

	public String getEstnAt() {
		return estnAt;
	}

	public void setEstnAt(String estnAt) {
		this.estnAt = estnAt;
	}

	public String getTotalFileMg() {
		return totalFileMg;
	}

	public void setTotalFileMg(String totalFileMg) {
		this.totalFileMg = totalFileMg;
	}

	public String getTotalFileCount() {
		return totalFileCount;
	}

	public void setTotalFileCount(String totalFileCount) {
		this.totalFileCount = totalFileCount;
	}

	public String getFileMgByByteConvert() {
		return EgovStringUtil.byteConverter(fileMg);
	}


	public List<String> getAtchFileIdArr() {
		return atchFileIdArr;
	}

	public void setAtchFileIdArr(List<String> atchFileIdArr) {
		this.atchFileIdArr = atchFileIdArr;
	}

	public String getOldAtchFileId() {
		return oldAtchFileId;
	}

	public void setOldAtchFileId(String oldAtchFileId) {
		this.oldAtchFileId = oldAtchFileId;
	}

	public String getLa() {
		return la;
	}

	public void setLa(String la) {
		this.la = la;
	}

	public String getLo() {
		return lo;
	}

	public void setLo(String lo) {
		this.lo = lo;
	}

	/**
     * toString 메소드를 대치한다.
     */
    public String toString() {
    	return ToStringBuilder.reflectionToString(this);
    }

	public String getSysTyCode() {
		return sysTyCode;
	}

	public void setSysTyCode(String sysTyCode) {
		this.sysTyCode = sysTyCode;
	}

	public String getFileStreWebCours() {
		return fileStreWebCours;
	}

	public void setFileStreWebCours(String fileStreWebCours) {
		this.fileStreWebCours = fileStreWebCours;
	}

	public BigDecimal getDownCnt() {
		return downCnt;
	}

	public void setDownCnt(BigDecimal downCnt) {
		this.downCnt = downCnt;
	}

	public String getMltmdClCode() {
		return mltmdClCode;
	}

	public void setMltmdClCode(String mltmdClCode) {
		this.mltmdClCode = mltmdClCode;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public long getMaxMegaFileSize() {
		return maxMegaFileSize;
	}

	public void setMaxMegaFileSize(long maxMegaFileSize) {
		this.maxMegaFileSize = maxMegaFileSize;
	}

	public String getPathKey() {
		return pathKey;
	}

	public void setPathKey(String pathKey) {
		this.pathKey = pathKey;
	}

	public String getAppendPath() {
		return appendPath;
	}

	public void setAppendPath(String appendPath) {
		this.appendPath = appendPath;
	}

	public String getEditorId() {
		return editorId;
	}

	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}

	public String getIsNoTempFile() {
		return isNoTempFile;
	}

	public void setIsNoTempFile(String isNoTempFile) {
		this.isNoTempFile = isNoTempFile;
	}

	public long getMaxChunkSize() {
		return maxChunkSize;
	}

	public void setMaxChunkSize(long maxChunkSize) {
		this.maxChunkSize = maxChunkSize;
	}

	public Integer getOrientation() {
		return orientation;
	}

	public void setOrientation(Integer orientation) {
		this.orientation = orientation;
	}

	public String getCrclId() {
		return crclId;
	}

	public void setCrclId(String crclId) {
		this.crclId = crclId;
	}

	public String getPublicAt() {
		return publicAt;
	}

	public void setPublicAt(String publicAt) {
		this.publicAt = publicAt;
	}

	public String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public String getSearchFileNm() {
		return searchFileNm;
	}

	public void setSearchFileNm(String searchFileNm) {
		this.searchFileNm = searchFileNm;
	}


	public List<String> getSearchFileExtsn() {
		return searchFileExtsn;
	}

	public void setSearchFileExtsn(List<String> searchFileExtsn) {
		this.searchFileExtsn = searchFileExtsn;
	}

	public String getSearchUseCnt() {
		return searchUseCnt;
	}

	public void setSearchUseCnt(String searchUseCnt) {
		this.searchUseCnt = searchUseCnt;
	}

	public String getSearchCtgryId() {
		return searchCtgryId;
	}

	public void setSearchCtgryId(String searchCtgryId) {
		this.searchCtgryId = searchCtgryId;
	}

	public String getSearchCrclId() {
		return searchCrclId;
	}

	public void setSearchCrclId(String searchCrclId) {
		this.searchCrclId = searchCrclId;
	}

	public String getSearchScope() {
		return searchScope;
	}

	public void setSearchScope(String searchScope) {
		this.searchScope = searchScope;
	}

	public String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public String getSearchRegisterNm() {
		return searchRegisterNm;
	}

	public void setSearchRegisterNm(String searchRegisterNm) {
		this.searchRegisterNm = searchRegisterNm;
	}

	public String getFileExtNm() {
		return fileExtNm;
	}

	public void setFileExtNm(String fileExtNm) {
		this.fileExtNm = fileExtNm;
	}

	public String getHostCode() {
		return hostCode;
	}

	public void setHostCode(String hostCode) {
		this.hostCode = hostCode;
	}

	public String getDownLoadType() {
		return downLoadType;
	}

	public void setDownLoadType(String downLoadType) {
		this.downLoadType = downLoadType;
	}

	public String getSearchPublicAt() {
		return searchPublicAt;
	}

	public void setSearchPublicAt(String searchPublicAt) {
		this.searchPublicAt = searchPublicAt;
	}

	public String getSearchFrstRegisterId() {
		return searchFrstRegisterId;
	}

	public void setSearchFrstRegisterId(String searchFrstRegisterId) {
		this.searchFrstRegisterId = searchFrstRegisterId;
	}

	public String getFileExtImg() {
		return fileExtImg;
	}

	public void setFileExtImg(String fileExtImg) {
		this.fileExtImg = fileExtImg;
	}

	public String getFileExtOther() {
		return fileExtOther;
	}

	public void setFileExtOther(String fileExtOther) {
		this.fileExtOther = fileExtOther;
	}

	public String getFileExtMov() {
		return fileExtMov;
	}

	public void setFileExtMov(String fileExtMov) {
		this.fileExtMov = fileExtMov;
	}

	public String getPlId() {
		return plId;
	}

	public void setPlId(String plId) {
		this.plId = plId;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getSearchCrclLang() {
		return searchCrclLang;
	}

	public void setSearchCrclLang(String searchCrclLang) {
		this.searchCrclLang = searchCrclLang;
	}

	public String getSearchPlTitle() {
		return searchPlTitle;
	}

	public void setSearchPlTitle(String searchPlTitle) {
		this.searchPlTitle = searchPlTitle;
	}

	public String getSearchTeacher() {
		return searchTeacher;
	}

	public void setSearchTeacher(String searchTeacher) {
		this.searchTeacher = searchTeacher;
	}

	public String getStudentAt() {
		return studentAt;
	}

	public void setStudentAt(String studentAt) {
		this.studentAt = studentAt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAddAt() {
		return addAt;
	}

	public void setAddAt(String addAt) {
		this.addAt = addAt;
	}

	public int getFileImgWidth() {
		return fileImgWidth;
	}

	public void setFileImgWidth(int fileImgWidth) {
		this.fileImgWidth = fileImgWidth;
	}

	public int getFileImgHeight() {
		return fileImgHeight;
	}

	public void setFileImgHeight(int fileImgHeight) {
		this.fileImgHeight = fileImgHeight;
	}

}
