package egovframework.com.cmm.service;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : FileVO.java
 * @Description : 파일정보 처리를 위한 VO 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 25.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 25.
 * @version
 * @see
 *
 */
@SuppressWarnings("serial")
public class SearchVO extends ComDefaultVO {

	public String searchCtgryId;

    public String searchStartDate;

    public String searchEndDate;

    public String searchRegisterNm;

    public String searchCrclId;

    public String searchCrclNm;

    public String searchCrclbId;

    public String searchStudySubject;

    public String searchClassManage;

    public String searchTodayClass;

    public String searchProcessSttusCode;

    public String searchKeyWord;

    public String[] searchSysType;

    public String[] searchSchdulClCode;

    public String searchSubmitN;

    public String searchSubmitI;

    public String searchSubmitF;

    public String searchCrclLang;

    public String searchPageFlag;

    public String sessionId;

    private String[] searchSubmitArr;

    private String searchSubmit;
    
    private String searchPlType;
    

   public String getSearchCtgryId() {
      return searchCtgryId;
   }

   public void setSearchCtgryId(String searchCtgryId) {
      this.searchCtgryId = searchCtgryId;
   }

   public String getSearchStartDate() {
      return searchStartDate;
   }

   public void setSearchStartDate(String searchStartDate) {
      this.searchStartDate = searchStartDate;
   }

   public String getSearchEndDate() {
      return searchEndDate;
   }

   public void setSearchEndDate(String searchEndDate) {
      this.searchEndDate = searchEndDate;
   }

   public String getSearchRegisterNm() {
      return searchRegisterNm;
   }

   public void setSearchRegisterNm(String searchRegisterNm) {
      this.searchRegisterNm = searchRegisterNm;
   }

   public String getSearchCrclId() {
      return searchCrclId;
   }

   public void setSearchCrclId(String searchCrclId) {
      this.searchCrclId = searchCrclId;
   }

   public String getSearchCrclbId() {
      return searchCrclbId;
   }

   public void setSearchCrclbId(String searchCrclbId) {
      this.searchCrclbId = searchCrclbId;
   }

	public String getSearchStudySubject() {
		return searchStudySubject;
	}

	public void setSearchStudySubject(String searchStudySubject) {
		this.searchStudySubject = searchStudySubject;
	}

	public String getSearchClassManage() {
		return searchClassManage;
	}

	public void setSearchClassManage(String searchClassManage) {
		this.searchClassManage = searchClassManage;
	}

	public String getSearchTodayClass() {
		return searchTodayClass;
	}

	public void setSearchTodayClass(String searchTodayClass) {
		this.searchTodayClass = searchTodayClass;
	}

	public String getSearchProcessSttusCode() {
		return searchProcessSttusCode;
	}

	public void setSearchProcessSttusCode(String searchProcessSttusCode) {
		this.searchProcessSttusCode = searchProcessSttusCode;
	}

	public String getSearchKeyWord() {
		return searchKeyWord;
	}

	public void setSearchKeyWord(String searchKeyWord) {
		this.searchKeyWord = searchKeyWord;
	}

	public String[] getSearchSysType() {
		return searchSysType;
	}

	public void setSearchSysType(String[] searchSysType) {
		this.searchSysType = searchSysType;
	}

	public String[] getSearchSchdulClCode() {
		return searchSchdulClCode;
	}

	public void setSearchSchdulClCode(String[] searchSchdulClCode) {
		this.searchSchdulClCode = searchSchdulClCode;
	}

	public String getSearchSubmitN() {
		return searchSubmitN;
	}

	public void setSearchSubmitN(String searchSubmitN) {
		this.searchSubmitN = searchSubmitN;
	}

	public String getSearchSubmitI() {
		return searchSubmitI;
	}

	public void setSearchSubmitI(String searchSubmitI) {
		this.searchSubmitI = searchSubmitI;
	}

	public String getSearchSubmitF() {
		return searchSubmitF;
	}

	public void setSearchSubmitF(String searchSubmitF) {
		this.searchSubmitF = searchSubmitF;
	}

	public String getSearchCrclLang() {
		return searchCrclLang;
	}

	public void setSearchCrclLang(String searchCrclLang) {
		this.searchCrclLang = searchCrclLang;
	}

	public String getSearchPageFlag() {
		return searchPageFlag;
	}

	public void setSearchPageFlag(String searchPageFlag) {
		this.searchPageFlag = searchPageFlag;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public String[] getSearchSubmitArr() {
		return searchSubmitArr;
	}

	public void setSearchSubmitArr(String[] searchSubmitArr) {
		this.searchSubmitArr = searchSubmitArr;
	}

	public String getSearchSubmit() {
		return searchSubmit;
	}

	public void setSearchSubmit(String searchSubmit) {
		this.searchSubmit = searchSubmit;
	}

	public String getSearchPlType() {
		return searchPlType;
	}

	public void setSearchPlType(String searchPlType) {
		this.searchPlType = searchPlType;
	}

}
