package egovframework.com.mng.sch;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.sms.service.EgovSmsInfoService;
import egovframework.com.evt.service.ComtnschdulinfoService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import net.sf.json.JSONObject;

/******************************************************
 * @Class Name : ComtnschdulinfoMngController.java
 * @Program name : egovframework.com.mng.evt
 * @Descriptopn : 충청남도교육연구정보원 스마트충남 기능 개선 구축
 * @version : 1.0.0
 * @author : 이호영
 * @created date : 2012. 2. 14.
 * Modification log
 * =====================================================
 * date                name             description
 * -----------------------------------------------------
 * 2012. 2. 14.        이호영             first generated
*********************************************************/

@Controller
public class ScheduleMngController {

	/** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;
    
    /** ComtnschdulinfoService */
    @Resource(name = "comtnschdulinfoService")
    private ComtnschdulinfoService comtnschdulinfoService;
    
    @Autowired	
	private DefaultBeanValidator beanValidator;
    
    /** EgovFileMngUtil */
    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
    
    @Resource(name = "EgovFileMngService")
	private EgovFileMngService            fileMngService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "EgovSmsInfoService")
    private EgovSmsInfoService egovSmsInfoService;
    
    @Resource(name = "egovMessageSource")
	private EgovMessageSource egovMessageSource;
    
    @Resource(name = "EgovBBSCtgryService")
	private EgovBBSCtgryService egovBBSCtgryService;
    
    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
    
    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
	/**
	 * 학교별 시간표 조회
	 * @param searchVO - 조회할 정보가 담긴 ScheduleMngVO
	 * @return "/mng/sch/selectCampusSchedule"
	 * @exception Exception
	 */
    @RequestMapping("/mng/sch/selectCampusSchedule.do")
    public String selectCampusSchedule(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	
    	//캠퍼스 카테고리 리스트
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		model.addAttribute("resultList", scheduleMngService.selectCampusSchedule(searchVO));
  		model.addAttribute("campusVO", searchVO);

    	return "/mng/sch/campusSchedule";
    }
    
    @RequestMapping(value="/mng/sch/insertCampusSchedule.json")
    public void insertCampusScheduleJson(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "N";
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

  		int delCnt = scheduleMngService.deleteCampusSchedule(searchVO);
  		scheduleMngService.insertCampusSchedule(searchVO);
		
		successYn = "Y";

		jo.put("successYn", successYn);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    } 
    
    @RequestMapping(value="/mng/sch/insertCampusSchedule.do")
    public String insertCampusSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	
    	scheduleMngService.insertCampusScheduleAll(searchVO);
    	
    	return "forward:/mng/sch/selectCampusSchedule.do";
    }
    
    /**
	 * 수업 시간표 조회
	 * @param searchVO - 조회할 정보가 담긴 ScheduleMngVO
	 * @return "/mng/sch/selectClassSchedule"
	 * @exception Exception
	 */
    @RequestMapping("/mng/sch/selectClassSchedule.do")
    public String selectClassSchedule(ScheduleMngVO searchVO, @RequestParam String plType, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	
    	Ctgry ctgry = new Ctgry();
    	
    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

	   	// 소속 리스트
    	/*ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS50");
    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));*/
    	
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
    	//searchVO.setPlType("all");
    	if(!EgovStringUtil.isEmpty(plType)){
    		searchVO.setPlType(plType);
    	}
    	
    	//과정정보
    	/*CurriculumVO curriculumVO = new CurriculumVO(); 
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));
    	
    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));*/
    	
    	
    	//과정 시간표
    	ScheduleMngVO schVO = new ScheduleMngVO();
    	schVO.setSearchCrclNm(searchVO.getSearchCrclNm());
    	schVO.setSearchSysCode1(searchVO.getSearchSysCode1());
    	schVO.setSearchFacNm(searchVO.getSearchFacNm());
    	model.addAttribute("crclList", scheduleMngService.selectCrclSchedule(schVO));
    	
    	model.addAttribute("plType", plType);
    	
    	model.addAttribute("searchVO", searchVO);
    	
    	// 학교일정 시간표
    	searchVO.setPlType("all");
    	model.addAttribute("resultList", scheduleMngService.selectClassSchedule(searchVO));

    	return "/mng/sch/classSchedule";
    }
    
    @RequestMapping(value="/mng/sch/insertClassSchedule.do")
    public String insertClassSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setFrstRegisterId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.insertClassSchedule(searchVO);
  		
  		//return "redirect:/mng/sch/selectClassSchedule.do?plType="+searchVO.getPlType();
  		String plTye = "";
  		if("ind".equals(searchVO.getPlType())){
  			plTye = "ind";
    	}else{
    		plTye = "all";
    	}
  		
  		return "redirect:/mng/sch/selectClassSchedule.do?plType="+plTye;

    }
    
    /**
	 * 수업 시간표 상세
	 */
    @RequestMapping("/mng/sch/selectClassScheduleView.do")
    public String selectClassScheduleView(ScheduleMngVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

    	Ctgry ctgry = new Ctgry();
    	
    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
    	// 소속 리스트
    	/*ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS50");
    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));*/
    	
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
  		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		if(loginVO != null){
  			searchVO.setFrstRegisterId(loginVO.getId());
  		}
  		
    	//searchVO.setPlType("all");
    	model.addAttribute("plType", searchVO.getPlType());
    	//과정 시간표
    	ScheduleMngVO schVO = new ScheduleMngVO();
    	schVO.setSearchCrclNm(searchVO.getSearchCrclNm());
    	schVO.setSearchSysCode1(searchVO.getSearchSysCode1());
    	schVO.setSearchFacNm(searchVO.getSearchFacNm());
    	model.addAttribute("crclList", scheduleMngService.selectCrclSchedule(schVO));
    	
    	model.addAttribute("scheduleMngVO", scheduleMngService.selectClassScheduleView(searchVO));
    	
    	// 학교일정 시간표
    	searchVO.setPlType("all");
    	model.addAttribute("resultList", scheduleMngService.selectClassSchedule(searchVO));

    	return "/mng/sch/classScheduleView";
    }
    
    @RequestMapping(value="/mng/sch/updateClassSchedule.do")
    public String updateClassSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setLastUpdusrId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.updateClassSchedule(searchVO, request, response);
  		
  		String plTye = "";
  		if("ind".equals(searchVO.getPlType())){
  			plTye = "ind";
    	}else{
    		plTye = "all";
    	}
  		
  		return "redirect:/mng/sch/selectClassSchedule.do?plType="+plTye;

    }
    
    @RequestMapping(value="/mng/sch/deleteClassSchedule.do")
    public String deleteCampusSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	scheduleMngService.deleteClassSchedule(searchVO);
    	
    	String plTye = "";
  		if("ind".equals(searchVO.getPlType())){
  			plTye = "ind";
    	}else{
    		plTye = "all";
    	}
  		
  		return "redirect:/mng/sch/selectClassSchedule.do?plType="+plTye;
    }
}
