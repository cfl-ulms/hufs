package egovframework.com.mng.uss.umt.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import com.adobe.xmp.impl.Base64;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.umt.service.EgovUserManageService;
import egovframework.com.uss.umt.service.UserDefaultVO;
import egovframework.com.uss.umt.service.UserManageVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

@Controller
public class EgovUserManageController {
	
	/** EgovMessageSource */
	@Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
	
	/** EgovUserManageService */
    @Resource(name = "userManageService")
    private EgovUserManageService userManageService;
    
    /** EgovPropertyService */
	@Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
	/** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;
    
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    /** DefaultBeanValidator */
    @Autowired
	private DefaultBeanValidator beanValidator;
    
    /** EgovFileMngUtil */
	@Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
	
	@Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
    /**
     * 교원 목록을 조회
     * @param model 화면모델
     * @return mng/usr/EgovMemberList
     * @throws Exception
     */
	@RequestMapping(value = "/mng/usr/EgovMberManage.do")
	  public String selectEgovMberManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			userManageVO.setSiteId(loginVO.getSiteId());
		}
		
		userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		userManageVO.setPageSize(propertiesService.getInt("pageSize"));
        
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
        paginationInfo.setPageSize(userManageVO.getPageSize());
        
        userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
        userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
        
        userManageVO.setUserSeCode("08");
        
        //팝업 검색
        if("N".equals(userManageVO.getTemplateAt())){
        	 userManageVO.setRecordCountPerPage(Integer.MAX_VALUE);
        	 List resultList = new ArrayList();
        	 //검색어가 있을 시에만 조회가 됨
        	 if(!EgovStringUtil.isEmpty(userManageVO.getSearchUserNm())){
        		 userManageVO.setSearchConfirmAt("Y");
        		 resultList = userManageService.selectUserList(userManageVO);
        	 }
        	 model.addAttribute("resultList", resultList);
        }else{
        	model.addAttribute("resultList", userManageService.selectUserList(userManageVO));
            
            int totCnt = userManageService.selectUserListTotCnt(userManageVO);
            paginationInfo.setTotalRecordCount(totCnt);
            model.addAttribute("paginationInfo", paginationInfo);
            
            ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
    	   	voComCode = new ComDefaultCodeVO();
    	   	// 소속 리스트
        	voComCode.setCodeId("LMS50");
        	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));
        	// 직위
        	voComCode.setCodeId("LMS70");
        	model.addAttribute("positionList", cmmUseService.selectCmmCodeDetail(voComCode));
        	// 재직상태
        	voComCode.setCodeId("LMS40");
        	model.addAttribute("workStatusList", cmmUseService.selectCmmCodeDetail(voComCode));
        	
            // 주관기관
        	Ctgry ctgry = new Ctgry();
            ctgry.setCtgrymasterId("CTGMST_0000000000009");
          	model.addAttribute("deptList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        }
       
		return "mng/usr/EgovMemberList";
	}
	
	@RequestMapping(value ="/mng/usr/EgovMberManage.json")
	public void selectEgovMberManageJson(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String successYn = "Y";
		
		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			userManageVO.setSiteId(loginVO.getSiteId());
		}
		
	    userManageVO.setFirstIndex(0);
	    userManageVO.setRecordCountPerPage(Integer.MAX_VALUE);
	    userManageVO.setSearchConfirmAt("Y");
	    
	    JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("items", userManageService.selectUserList(userManageVO));
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//학생 목록을 조회(팝업)
	@RequestMapping(value = "/mng/usr/EgovStuMberManage.do")
	  public String EgovStuMberManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(Integer.parseInt(loginVO.getUserSe()) > 9){
			List resultList = new ArrayList();
			userManageVO.setFirstIndex(0);
			userManageVO.setRecordCountPerPage(Integer.MAX_VALUE);
			userManageVO.setUserSeCode("04");
	    	 //검색어가 있을 시에만 조회가 됨(기존 아이디만 조회 , 변경 : 이름 및 전화번호도 조회되게 추가 )
	    	 if(!EgovStringUtil.isEmpty(userManageVO.getSearchUserId()) || !EgovStringUtil.isEmpty(userManageVO.getSearchUserNm()) || !EgovStringUtil.isEmpty(userManageVO.getSearchMoblphonNo())){
	    		 resultList = userManageService.selectUserList(userManageVO);
	    	 }
	    	 model.addAttribute("resultList", resultList);
		}
    	 
		return "mng/usr/EgovStuMemberList";
	}
	
	/**
	 * 사용자 뷰
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "/mng/usr/EgovMberView.do"
	 * @throws Exception
	 */
	
	 @RequestMapping(value = "/mng/usr/EgovMberView.do")
	public String EgovMberView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO,
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{
		 
		 	UserManageVO userVO = userManageService.selectUser(userManageVO);
		 
		 	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
		 	model.addAttribute("userManageVO", userVO);
		 	
		 	if("08".equals(userVO.getUserSeCode())){ // 교원
		 		CurriculumVO curriculumVO = new CurriculumVO();
			    
			    curriculumVO.setUserId(userVO.getUserId());
			    List<CurriculumVO> resultList = curriculumService.selectMyCurriculumHistoryList(curriculumVO);
			    model.addAttribute("resultList", resultList);
			    
			    model.addAttribute("resultCnt", curriculumService.selectMyCurriculumHistoryCnt(curriculumVO));
			    
			    List<CurriculumVO> resultListCount = curriculumService.selectMyCurriculumHistoryListCount(curriculumVO);
			    model.addAttribute("resultListCount", resultListCount);
			    
			    //과정진행상태 - 공통코드(LMS30)
		  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
		  	   	voComCode = new ComDefaultCodeVO();
		      	voComCode.setCodeId("LMS30");
		      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
		      	model.addAttribute("statusComCode", statusComCode);
		 	}
 
		 return "mng/usr/EgovMemberView";
	}
	
	/**
	 * 사용자 등록화면으로 이동한다.
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "/mng/usr/EgovMberAddView.do"
	 * @throws Exception
	 */
	
	 @RequestMapping(value = "/mng/usr/EgovMberAddView.do")
	public String EgovMberAddView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO, @RequestParam(value="userId", defaultValue="", required=false) String userId,
    		UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{
		 
		 	if(!EgovStringUtil.isEmpty(userId)){
		 		userManageVO.setUserId(userId);
		 		userManageVO = userManageService.selectUser(userManageVO);
		 		model.addAttribute("userManageVO", userManageVO);
		 	}
		 	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
		   	voComCode = new ComDefaultCodeVO();
		   	// 소속 리스트
	    	voComCode.setCodeId("LMS50");
	    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	// 직위
	    	voComCode.setCodeId("LMS70");
	    	model.addAttribute("positionList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	// 재직상태
	    	voComCode.setCodeId("LMS40");
	    	model.addAttribute("workStatusList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	
	        // 주관기관
	    	Ctgry ctgry = new Ctgry();
	        ctgry.setCtgrymasterId("CTGMST_0000000000009");
	      	model.addAttribute("deptList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
	      	
	      	// 세부전공 (언어)
			ctgry.setCtgrymasterId("CTGMST_0000000000002");
			model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
			 
			//사진경로
			model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
			
		 return "mng/usr/EgovMemberIndt";
	}
	 
	 /**
	 * 직원 등록화면으로 이동한다.
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "/mng/usr/EgovMberAddView.do"
	 * @throws Exception
	 */
	
	 @RequestMapping(value = "/mng/usr/EgovStaffAddView.do")
	public String EgovStaffAddView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO, @RequestParam(value="userId", defaultValue="", required=false) String userId,
    		UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{
		 
		 	if(!EgovStringUtil.isEmpty(userId)){
		 		userManageVO.setUserId(userId);
		 		userManageVO = userManageService.selectUser(userManageVO);
		 		model.addAttribute("userManageVO", userManageVO);
		 	}
		 	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
		   	voComCode = new ComDefaultCodeVO();
		   	
	    	// 재직상태
	    	voComCode.setCodeId("LMS40");
	    	model.addAttribute("workStatusList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	
	    	// 소속 리스트
	    	voComCode.setCodeId("LMS60");
	    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	
	    	//사진경로
			model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

		 return "mng/usr/EgovStaffIndt";
	}


	/**
     * 사용자 정보를 DB에 입력한다.
     * @param model 화면모델
     * @return forward:/mng/usr/EgovUserSelectIndt.do
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovUserSelectIndt.do")
    public String EgovUserSelectIndt(
    		final MultipartHttpServletRequest multiRequest, 
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, 
    		BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
			        
			List<FileVO> result = null;
	        
	    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
	        if(!files.isEmpty()) {
	          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
	          if(result != null) {
	  	    	for(int index=0; index < result.size(); index++) {
	  	    		FileVO file = result.get(index);
	  	    		if(file.getFormNm().startsWith("user")) {
	  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
	  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
	  	    		}
	  	    	}
	      	  }
	        }
			userManageService.insertUser(userManageVO);

	      return "forward:/mng/usr/EgovMberManage.do";
    }
	
	/**
     * 직원 정보를 DB에 입력한다.
     * @param model 화면모델
     * @return forward:/mng/usr/EgovStaffSelectIndt.do
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovStaffSelectIndt.do")
    public String EgovStaffSelectIndt(
    		final MultipartHttpServletRequest multiRequest, 
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, 
    		UserManageVO userManageVO, 
    		BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

			
			List<FileVO> result = null;
	        
	    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
	        if(!files.isEmpty()) {
	          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
	          if(result != null) {
	  	    	for(int index=0; index < result.size(); index++) {
	  	    		FileVO file = result.get(index);
	  	    		if(file.getFormNm().startsWith("user")) {
	  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
	  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
	  	    		}
	  	    	}
	      	  }
	        }
	        
			userManageService.insertStaff(userManageVO);

	      return "forward:/mng/usr/EgovStaffManage.do";
    }
	
	/**
     * 직원 정보 수정 처리 한다.
     * @param model 화면모델
     * @return forward:/mng/usr/EgovStaffSelectUpdt.do
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovStaffSelectUpdt.do")
    public String EgovStaffSelectUpdt(
    		final MultipartHttpServletRequest multiRequest, 
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, @RequestParam(value="targetId", defaultValue="", required=false) String targetId,
    		UserManageVO userManageVO, 
    		BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

		List<FileVO> result = null;
        
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
          if(result != null) {
  	    	for(int index=0; index < result.size(); index++) {
  	    		FileVO file = result.get(index);
  	    		if(file.getFormNm().startsWith("user")) {
  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
  	    		}
  	    	}
      	  }
        }     

        if(!EgovStringUtil.isEmpty(userManageVO.getPhone1()) && !EgovStringUtil.isEmpty(userManageVO.getPhone2()) && !EgovStringUtil.isEmpty(userManageVO.getPhone3())) {
        	userManageVO.setMoblphonNo(userManageVO.getPhone1() + "-" + userManageVO.getPhone2() + "-" + userManageVO.getPhone3());
        }
        if(!EgovStringUtil.isEmpty(userManageVO.getEmail1()) && !EgovStringUtil.isEmpty(userManageVO.getEmail2())) {
        	userManageVO.setEmailAdres(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
        }
        if(!EgovStringUtil.isEmpty(targetId)){
        	userManageVO.setTargetId(targetId);
        }
        
        LoginVO user = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);

        userManageVO.setLastUpdusrId(user.getId());

		userManageService.updateManageStaff(userManageVO);

		return "forward:/mng/usr/EgovStaffManage.do";
    }
	
	/**
     * 교원 삭제
     */
	@RequestMapping(value = "/mng/usr/EgovUserDelete.do")
    public String EgovUserDelete(@ModelAttribute("searchVO") UserDefaultVO searchVO, UserManageVO userManageVO)throws Exception {

			userManageService.deleteUser(userManageVO);

	      return "redirect:/mng/usr/EgovMberManage.do";
    }
	
	/**
     * 비밀번호 초기화
     * @param model 화면모델
     * @return forward:/mng/usr/EgovresetPwd.do
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovResetPwd.do")
    public String EgovResetPwd(UserManageVO userManageVO)throws Exception {
			userManageService.updateResetPwd(userManageVO);

	      return "forward:/mng/usr/EgovMemberView.do";
    }
	 
	 
	
    /**
	 * 사용자목록을 엑셀로 다운받는다.
	 * @param searchVO - 조회할 정보가 담긴 ComtneventprzwnerDefaultVO
	 * @return "/mng/evt/ComtneventprzwnerList"
	 * @exception Exception
	 */
	@RequestMapping(value="/mng/usr/EgovMberManageExcel.do")
    public String EgovMberManageExcel(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model) throws Exception {

        int totCnt = userManageService.selectUserListTotCnt(userManageVO);
        
        userManageVO.setFirstIndex(0);
        userManageVO.setRecordCountPerPage(totCnt);
    	
    	model.addAttribute("resultList", userManageService.selectUserList(userManageVO));

    	return "mng/usr/EgovMemberListExcel";
    }
	
	/**
     * 사용자정보 수정 화면으로 이동한다.
     * @param model 화면모델
     * @return mng/usr/EgovMemberUpdt
     * @throws Exception
     */
	@RequestMapping(value = "/mng/usr/EgovUserSelectUpdtView.do")
    public String EgovUserSelectUpdtView(
    		@ModelAttribute("searchVO") UserDefaultVO searchVO,
    		UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception {
    	
    	model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));
    	
    	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
    	
      return "mng/usr/EgovMemberUpdt";
    }
	
	
	/**
     * 사용자정보 수정 처리 한다.
     * @param model 화면모델
     * @return forward:/mng/usr/EgovUserSelectUpdtView.do
     * @throws Exception
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovUserSelectUpdt.do")
    public String EgovUserSelectUpdt(
    		final MultipartHttpServletRequest multiRequest, 
    		@ModelAttribute("searchVO") UserDefaultVO searchVO, @RequestParam(value="targetId", defaultValue="", required=false) String targetId,
    		UserManageVO userManageVO, 
    		BindingResult bindingResult, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
  	  
		// 아이디 체크
    	List<FileVO> result = null;
        
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
        if(!files.isEmpty()) {
          result = fileUtil.directParseFileInf(files, "MEM_", 0, "Members.fileStorePath", "");
          if(result != null) {
  	    	for(int index=0; index < result.size(); index++) {
  	    		FileVO file = result.get(index);
  	    		if(file.getFormNm().startsWith("user")) {
  	    			userManageVO.setPhotoOriginalFileNm(file.getOrignlFileNm());
  	    			userManageVO.setPhotoStreFileNm(file.getStreFileNm());
  	    		}
  	    	}
      	  }
        }

        if(!EgovStringUtil.isEmpty(userManageVO.getPhone1()) && !EgovStringUtil.isEmpty(userManageVO.getPhone2()) && !EgovStringUtil.isEmpty(userManageVO.getPhone3())) {
        	userManageVO.setMoblphonNo(userManageVO.getPhone1() + "-" + userManageVO.getPhone2() + "-" + userManageVO.getPhone3());
        }
        if(!EgovStringUtil.isEmpty(userManageVO.getEmail1()) && !EgovStringUtil.isEmpty(userManageVO.getEmail2())) {
        	userManageVO.setEmailAdres(userManageVO.getEmail1() + "@" + userManageVO.getEmail2());
        }
        if(!EgovStringUtil.isEmpty(targetId)){
        	userManageVO.setTargetId(targetId);
        }
        
        LoginVO user = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(request, response);

        userManageVO.setLastUpdusrId(user.getId());
        
        if("02".equals(userManageVO.getUserSeCode()) || "04".equals(userManageVO.getUserSeCode()) || "06".equals(userManageVO.getUserSeCode())){
        	if("02".equals(userManageVO.getUserSeCode())){
        		userManageVO.setGroupCode(userManageVO.getGroupNm02());
        	}else if("04".equals(userManageVO.getUserSeCode())){
        		userManageVO.setGroupCode(userManageVO.getGroupNm01());
        	}
        	userManageVO.setCode(userManageVO.getUserSeCode());
        	userManageVO.setUserSeCode("04");
        	
        	if(!EgovStringUtil.isEmpty(userManageVO.getBrthdy01()) && !EgovStringUtil.isEmpty(userManageVO.getBrthdy02()) && !EgovStringUtil.isEmpty(userManageVO.getBrthdy03())) {
            	userManageVO.setBrthdy(userManageVO.getBrthdy01() + userManageVO.getBrthdy02() + userManageVO.getBrthdy03());
            }
        }
        
        // 데이터 수정
		userManageService.updateManageUser(userManageVO);
		//model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));

      //return "forward:/mng/usr/EgovMberManage.do";
		if(EgovStringUtil.isEmpty(userManageVO.getUserId())){
			userManageVO.setUserId(userManageVO.getTargetId());
		}
		String returnUrl = "";
		if("08".equals(userManageVO.getUserSeCode())){
			returnUrl = "EgovMberView";
		}
		if("04".equals(userManageVO.getUserSeCode())){
			returnUrl = "EgovStudentView";
		}
		return "forward:/mng/usr/"+returnUrl+".do?userId="+userManageVO.getUserId()+"&userSeCode="+userManageVO.getUserSeCode();
		
    }
	
	
	/**
	 * 관리자가 패스워드를 임의로 재발급하고 핸드폰으로 전송 한다.
	 * @param userManageVO
	 * @param request
	 * @param commandMap
	 * @param model
	 * @return "forward:/mng/usr/SendPassword.do"
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/usr/SendPassword.do")
	public String SendPassword(
			@ModelAttribute("searchVO") UserManageVO userManageVO, final HttpServletRequest request, Model model) throws Exception {

		model.addAttribute("target", "searchPasswordBySms");	//SMS모드로 전송한다.

		// 1. 비밀번호 찾기
    	Map<String, Object> resultList = userManageService.updateSendPassword(userManageVO);
    	boolean result = (Boolean)resultList.get("sendResult");
    	
    	// 2. 결과 리턴
        if (result) {
    		model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
        } else {
        	model.addAttribute("message", egovMessageSource.getMessage("fail.request.msg"));
        }
        
        return "mng/usr/SendPassword";
	}
	
	/**
	 * 선택한 사용자 목록을 접속금지/해제 처리 한다.
	 * @param userManageVO
	 * @return String
	 * @exception Exception
	 */ 
	@RequestMapping(value="/mng/usr/EgovMberManageConfrm.do")
	public String mberManageConfrm(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model) throws Exception {
		
    	if("N".equals(userManageVO.getConfmAt())) {
    		userManageService.updateUserRhibt(userManageVO);
    	} else if("Y".equals(userManageVO.getConfmAt())) {
    		userManageService.updateUserRelis(userManageVO);
    	}

		//model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		return "forward:/mng/usr/EgovMberManage.do";
	}
	
	/**
	 * 선택한 사용자 목록을 접속금지/해제 처리 한다.
	 * @param userManageVO
	 * @return String
	 * @exception Exception
	 */ 
	@RequestMapping(value="/mng/usr/EgovMberManageDelete.do")
	public String mberManageDelete(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		userManageVO.setLastUpdusrId(user.getId());
		userManageService.deleteUser(userManageVO);

		//model.addAttribute("message", egovMessageSource.getMessage("success.common.process"));
		return "forward:/mng/usr/EgovMberManage.do";
	}
	
	/**
	 * 사용자 엑셀파일을 일괄 업로드한다.
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "mng/usr/EgovMberExcelUpload"
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/usr/EgovMberExcelUploadView.do")
	public String EgovMberExcelUploadView(@ModelAttribute("searchVO") UserManageVO userManageVO, Model model) throws Exception {
		return "mng/usr/EgovMberExcelUpload";
	}
	/**
	 * 사용자 엑셀파일을 일괄 업로드한다.
	 * @param userManageVO
	 * @param request
	 * @param commandMap
	 * @param model
	 * @return "mng/usr/EgovMberExcelUpload"
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/mng/usr/EgovMberExcelUpload.do")
	public String EgovMberExcelUpload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") UserManageVO userManageVO, Model model) throws Exception {

		Map<String, Object> resultList = null;
		
		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		MultipartFile file;

		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();
		
			file = entry.getValue();

			if (!"".equals(file.getOriginalFilename())) {
				int index = file.getOriginalFilename().lastIndexOf(".");
				String fileExt = "";
			    if(index != -1) {
			    	fileExt = file.getOriginalFilename().substring(index + 1).toUpperCase();
			    }
				resultList = userManageService.insertUserExcelUpload(userManageVO, fileExt, file.getInputStream());
				model.addAttribute("message", resultList.get("message"));
				if(resultList.containsKey("dataList")) {
					model.addAttribute("dataList", resultList.get("dataList"));
				}
			}
		}
		
        return "mng/usr/EgovMberExcelUpload";
	}
	
	/**
     * 직원 목록을 조회
     * @param model 화면모델
     * @return mng/usr/EgovMemberList
     * @throws Exception
     */
	@RequestMapping(value = "/mng/usr/EgovStaffManage.do")
	  public String selectEgovStaffManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			userManageVO.setSiteId(loginVO.getSiteId());
		}
		
		userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		userManageVO.setPageSize(propertiesService.getInt("pageSize"));
        
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
        paginationInfo.setPageSize(userManageVO.getPageSize());
        
        userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
        userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
        
        userManageVO.setUserSeCode("10");
        model.addAttribute("resultList", userManageService.selectUserList(userManageVO));
        
        int totCnt = userManageService.selectUserListTotCnt(userManageVO);
        paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

		return "mng/usr/EgovStaffList";
	}
	
	/**
     * 학생 목록을 조회
     * @param model 화면모델
     * @return mng/usr/EgovMemberList
     * @throws Exception
     */
	@RequestMapping(value = "/mng/usr/EgovStudentManage.do")
	  public String EgovStudentManage(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getSiteId())) {		  
			userManageVO.setSiteId(loginVO.getSiteId());
		}
		
		userManageVO.setPageUnit(propertiesService.getInt("pageUnit"));
		userManageVO.setPageSize(propertiesService.getInt("pageSize"));
        
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(userManageVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(userManageVO.getPageUnit());
        paginationInfo.setPageSize(userManageVO.getPageSize());
        
        userManageVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        userManageVO.setLastIndex(paginationInfo.getLastRecordIndex());
        userManageVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

        userManageVO.setUserSeCode("04");
        model.addAttribute("resultList", userManageService.selectUserList(userManageVO));
        
        int totCnt = userManageService.selectUserListTotCnt(userManageVO);
        paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
	   	// 소속 리스트
    	/*voComCode.setCodeId("COM041");
    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));*/
		
		return "mng/usr/EgovStudentList";
	}
	
	/**
	 * 학생 뷰 화면
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "/mng/usr/EgovStudentView.do"
	 * @throws Exception
	 */
	
	 @RequestMapping(value = "/mng/usr/EgovStudentView.do")
	public String EgovStudentView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO,
    		UserManageVO userManageVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		 	CurriculumVO currVO = new CurriculumVO();
		 
		 	if(EgovStringUtil.isEmpty(userManageVO.getUserSeCode()) || "02".equals(userManageVO.getUserSeCode()) || "06".equals(userManageVO.getUserSeCode())){
	 			userManageVO.setUserSeCode("04");
	 			currVO.setStudentPageAt("Y");
	 		}
		 	
		 	model.addAttribute("userManageVO", userManageService.selectUser(userManageVO));

		 	currVO.setFirstIndex(0);
		 	currVO.setLastIndex(10000);
		 	currVO.setRecordCountPerPage(10000);
		 	
			currVO.setUserId(userManageVO.getUserId());
			currVO.setUserSeCode(userManageVO.getUserSeCode());
			currVO.setWishCurriculumPageAt("Y");
			
		 	List<?> curriculumList = curriculumService.selectCurriculumList(currVO, request, response);
		    model.addAttribute("resultList", curriculumList);
		 	
		 	//사진경로
			model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
 
		 return "mng/usr/EgovStudentView";
	}
	
	/**
	 * 학생 등록화면으로 이동한다.
	 * @param userManageVO
	 * @param request
	 * @param model
	 * @return "/mng/usr/EgovStudentAddView.do"
	 * @throws Exception
	 */
	
	 @RequestMapping(value = "/mng/usr/EgovStudentAddView.do")
	public String EgovStudentAddView(
			@ModelAttribute("searchVO") UserDefaultVO searchVO, @RequestParam(value="userId", defaultValue="", required=false) String userId,
    		UserManageVO userManageVO, HttpServletRequest request, ModelMap model) throws Exception{
		 
		 	if(!EgovStringUtil.isEmpty(userId)){
		 		userManageVO.setUserId(userId);
		 		if(EgovStringUtil.isEmpty(userManageVO.getUserSeCode())){
		 			userManageVO.setUserSeCode("04");
		 		}
		 		userManageVO = userManageService.selectUser(userManageVO);
		 		model.addAttribute("userManageVO", userManageVO);
		 	}
		 	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
		   	voComCode = new ComDefaultCodeVO();
		   	// 소속 리스트
	    	voComCode.setCodeId("COM041");
	    	model.addAttribute("groupList", cmmUseService.selectCmmCodeDetail(voComCode));
	    	
	    	//언어코드
			Ctgry ctgry = new Ctgry();
			ctgry.setCtgrymasterId("CTGMST_0000000000002");
			model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
			
			ComDefaultCodeVO codeVO = new ComDefaultCodeVO();
			/*
			//학교목록
			codeVO.setCodeId("COM041");
			model.addAttribute("schoolList", cmmUseService.selectCmmCodeDetail(codeVO));
			*/
			//국제전화번호
			codeVO.setCodeId("COM040");
			model.addAttribute("telNumList", cmmUseService.selectCmmCodeDetail(codeVO));
			
			//사진경로
			model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
	    	
			//인증API서비스ID
			model.addAttribute("serviceId", propertiesService.getString("hufs.user.serviceid"));
			model.addAttribute("authUri", propertiesService.getString("hufs.user.authUri"));
			
		 return "mng/usr/EgovStudentIndt";
	}
	 
	@RequestMapping(value ="/mng/usr/EgovMberEmailDuplChk.json")
	public void selectEgovMberEmailDuplChk(@ModelAttribute("searchVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String successYn = "Y";
		
		if(!EgovStringUtil.isEmpty(userManageVO.getEmailAdres())){
			successYn = userManageService.checkEmailDuplChk(userManageVO.getEmailAdres());
		}
	    
	    JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//가입인증 메일 수동 인증
	@RequestMapping("/mng/usr/mailAuth.do")
    public void mailAuth(@ModelAttribute("userManageVO") UserManageVO userManageVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String successYn = "Y";
		
		//승인 처리
		userManageService.updateUserRelis(userManageVO);
		
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
		
    }
}
