package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

/**
 * @Class Name : CurriculumController.java
 * @Description : Curriculum Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Controller
public class CurriculumMngController {

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;
    
    /** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
    
    @Resource(name = "SiteManageService")
	EgovSiteManageService 				  siteManageService;
    
    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    /**
     * XSS 방지 처리.
     *
     * @param data
     * @return
     */
    protected String unscript(String data) {
      if(data == null || data.trim().equals("")) {
        return "";
      }

      String ret = data;

      ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
      ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

      ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
      ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

      ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
      ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

       ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
       ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

      ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

      return ret;
    }

    //교육과정개설신청관리
    @RequestMapping(value="/mng/lms/crcl/CurriculumMngList.do")
    public String selectCurriculumMngList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//이수구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000003");
  		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//관리구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000005");
  		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

        return "/mng/lms/crcl/CurriculumMngList";
    }

    //과정등록관리
    @RequestMapping(value="/mng/lms/crcl/CurriculumList.do")
    public String selectCurriculumList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String retUrl = "/mng/lms/crcl/CurriculumList";
    	Ctgry ctgry = new Ctgry();

    	if("Y".equals(searchVO.getMngAt())){
    		retUrl = "/mng/lms/crcl/CurriculumMngList";
    		//교수가 확인 및 등록 신청한 것만 확인
    		searchVO.setSearchRegistAt("Y");
    	}else{
    		//과정진행상태 - 공통코드(LMS30)
    		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
    	   	voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS30");
        	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("statusComCode", statusComCode);

        	//학기
        	ctgry.setCtgrymasterId("CTGMST_0000000000016");
    		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//대상
      		ctgry.setCtgrymasterId("CTGMST_0000000000015");
      		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
      		
      		//년도
    		model.addAttribute("yearList", new LmsCommonUtil().getYear());
    		
    		//승인 된 것만 나옴
    		searchVO.setSearchAprvalAt("Y");
    	}

    	//과정체계
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//이수구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000003");
  		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//관리구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000005");
  		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserSeCode(user.getUserSeCode());

    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

        return retUrl;
    }

    @RequestMapping("/mng/lms/crcl/addCurriculumView.do")
    public String addCurriculumView(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//학기
		ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//캠퍼스
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//사업비
		ctgry.setCtgrymasterId("CTGMST_0000000000018");
		model.addAttribute("feeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

		//과정성과 - 공통코드(LMS10)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS10");
    	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("listComCode", listComCode);

    	model.addAttribute("curriculumVO", searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);

        return "/mng/lms/crcl/CurriculumRegister";
    }

    @RequestMapping("/mng/lms/crcl/addCurriculum.do")
    public String addCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/crcl/CurriculumList.do";
		}

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    		if(Integer.parseInt(user.getUserSeCode()) > 8){
    			searchVO.setAprvalAt("Y");
    			searchVO.setRegistAt("Y");
    		}
    	}else{
    		return "forward:/index.do";
    	}

        curriculumService.insertCurriculum(searchVO);

        request.getSession().removeAttribute("sessionVO");

        return "forward:/mng/lms/crcl/CurriculumList.do";
    }

    @RequestMapping("/mng/lms/crcl/updateCurriculumView.do")
    public String updateCurriculumView(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//학기
		ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//캠퍼스
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//사업비
		ctgry.setCtgrymasterId("CTGMST_0000000000018");
		model.addAttribute("feeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//과정성과 - 공통코드(LMS10)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS10");
    	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("listComCode", listComCode);

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));

    	//년도
    	model.addAttribute("yearList", new LmsCommonUtil().getYear());
    	
    	//과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList3", resultMap3.get("resultList"));
		
    	//과정확정 이후 수정 시
    	if(Integer.parseInt(curriculumVO.getProcessSttusCode()) > 0){
    		//과정진행상태 - 공통코드(LMS30)
        	voComCode.setCodeId("LMS30");
        	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("statusComCode", statusComCode);

        	//오늘일자
        	model.addAttribute("today", EgovDateUtil.getToday());

        	//학습내용
        	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

        	//부책임교원
        	CurriculumVO curriculum = new CurriculumVO();
        	curriculum.setCrclId(searchVO.getCrclId());
        	//부책임교원 코드 08
        	curriculum.setManageCode("08");
        	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

        	//성적기준
    		ctgry.setCtgrymasterId("CTGMST_0000000000011");
    		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//총괄평가
    		List evaluationList = curriculumService.selectTotalEvaluation(curriculum);
    		model.addAttribute("evaluationList", evaluationList);
    		//총괄평가 기본 목록
    		if(evaluationList == null || evaluationList.size() == 0){
    			ctgry.setCtgrymasterId("CTGMST_0000000000012");
        		model.addAttribute("evaluationBaseList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    		}else{
    			model.addAttribute("evaluationList", evaluationList);

    			//수업참여도 게시판
    			List attendBbsList = curriculumService.selectAttendbbs(curriculum);
    			model.addAttribute("attendBbsList", attendBbsList);
    		}

    		//교원배정
        	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(searchVO));

        	//과제타입코드
        	voComCode.setCodeId("LMS80");
        	List<CmmnDetailCode> homeworkComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("homeworkComCode", homeworkComCode);

        	//수료기준
    		ctgry.setCtgrymasterId("CTGMST_0000000000010");
    		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        	//교재 및 부교재
        	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
    	}

        request.getSession().setAttribute("sessionVO", searchVO);

        return "/mng/lms/crcl/CurriculumRegister";
    }
    
    // 과정내용
    @RequestMapping("/ajax/mng/lms/crcl/selectCurriculumLesson.json")
    public void selectCurriculumLesson(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	
		//학습내용
    	List<EgovMap> lessonList = new ArrayList<EgovMap>();
    	lessonList = curriculumService.selectCurriculumLesson(searchVO);
    	
    	JSONObject jo = new JSONObject();
    	response.setContentType("application/json;charset=utf-8");
    	
    	if(lessonList.size() > 0 ) {
    		jo.put("lessonList", lessonList);
    		jo.put("status", "200");
    	} else {
    		jo.put("status", "err");
    	}
    	
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
    
    //기본과정명Ajax
   /* @RequestMapping(value="/mng/lms/crclb/CurriculumbaseList.json")
    public void CurriculumbaseListJson(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "Y";
    	
		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(Integer.MAX_VALUE);
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("items", curriculumbaseService.selectCurriculumbaseList(searchVO));
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    } */

    @RequestMapping("/mng/lms/crcl/selectCurriculum.do")
    public String selectCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();

    	//교수가 작성한 교육과정개설신청 확인 시 필요
    	if("Y".equals(searchVO.getMngAt())){
    		//과정성과 - 공통코드(LMS10)
    	   	voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS10");
        	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("listComCode", listComCode);
    	}

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
      	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
      	
      	System.out.println(curriculumVO);
      	
      	//과정만족도
 		SurveyVO surveyVo = new SurveyVO();
 		surveyVo.setSchdulClCode("TYPE_1");
 		surveyVo.setRecordCountPerPage(1000);
 		surveyVo.setFirstIndex(0);
 		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList", resultMap.get("resultList"));
 		surveyVo.setSchdulClCode("TYPE_2");
 		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList2", resultMap2.get("resultList"));
 		surveyVo.setSchdulClCode("TYPE_3");
 		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList3", resultMap3.get("resultList"));
 		
      	//과정확정 이후 수정 시
    	if(Integer.parseInt(curriculumVO.getProcessSttusCode()) > 0){
    		//과정진행상태 - 공통코드(LMS30)
        	voComCode.setCodeId("LMS30");
        	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("statusComCode", statusComCode);

        	//오늘일자
        	model.addAttribute("today", EgovDateUtil.getToday());

        	//학습내용
        	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

        	//부책임교원
        	CurriculumVO curriculum = new CurriculumVO();
        	curriculum.setCrclId(searchVO.getCrclId());
        	//부책임교원 코드 08
        	curriculum.setManageCode("08");
        	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

        	//성적기준
        	Ctgry ctgry = new Ctgry();
    		ctgry.setCtgrymasterId("CTGMST_0000000000011");
    		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//총괄평가
    		List evaluationList = curriculumService.selectTotalEvaluation(curriculum);
    		model.addAttribute("evaluationList", evaluationList);

    		//수업참여도 게시판
			List attendBbsList = curriculumService.selectAttendbbs(curriculum);
			model.addAttribute("attendBbsList", attendBbsList);

    		//교원배정 - 총 시간 입력 시 나옴
			if("Y".equals(curriculumVO.getTotalTimeAt())){
				searchVO.setCpr("Y");//시간표 교원 비교하기 위해서 정렬
				List tempFacList = curriculumService.selectCurriculumFaculty(searchVO);
	        	List<EgovMap> facList = new ArrayList<EgovMap>();
	        	List<String> facIdList = new ArrayList<String>();
	        	if(tempFacList != null && tempFacList.size() > 0){
	        		EgovMap tempMap = new EgovMap();
	        		String prevPosition = "";
	        		String facId = "";
	        		String facNm = "";
	        		String prevSisu = "";
	        		String sisu = "";
	        		for(int i = 0; i < tempFacList.size(); i++){
	        			EgovMap map = (EgovMap) tempFacList.get(i);
	        			if(prevPosition.equals(map.get("position").toString()) || i == 0){
	        				facId += " " + map.get("facId").toString();
	        				facNm += " " + map.get("facNm").toString();

	        			}else{
	        				tempMap.put("facId", facId);
	        				tempMap.put("facNm", facNm);
	        				tempMap.put("sisu", prevSisu);
	        				tempMap.put("sisu", prevSisu);
	        				facList.add(tempMap);
	        				tempMap = new EgovMap();

	        				facId = " " + map.get("facId").toString();
	        				facNm = " " + map.get("facNm").toString();

	        			}

	        			//마지막 부분 담기
	        			if(i == (tempFacList.size() - 1)){
	        				sisu = map.get("sisu").toString();
	        				tempMap.put("facId", facId);
	        				tempMap.put("facNm", facNm);
	        				tempMap.put("sisu", sisu);
	        				facList.add(tempMap);
	        			}

	        			prevPosition = map.get("position").toString();
	        			prevSisu = map.get("sisu").toString();
	        			facIdList.add(map.get("facId").toString());
	        		}
	        	}
	        	model.addAttribute("facList", facList);
	        	model.addAttribute("facIdList", facIdList);

	        	//등록 된 시간표 담당교원
	        	ScheduleMngVO scheduleMngVO = new ScheduleMngVO();
	        	scheduleMngVO.setCrclId(searchVO.getCrclId());
	        	scheduleMngVO.setCpr("Y");//교원 비교하기 위해서 정렬
	        	List tempFacPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
	        	List<EgovMap> facPlList = new ArrayList<EgovMap>();
	        	if(tempFacPlList != null && tempFacPlList.size() > 0){
	        		String prevPlId = "";
	        		String facId = "";
	        		String sisu = "";
	        		String prevSisu = "";
	        		EgovMap tempMap = new EgovMap();
	        		for(int i = 0; i < tempFacPlList.size(); i++){
	        			EgovMap map = (EgovMap) tempFacPlList.get(i);
	        			if(prevPlId.equals(map.get("plId").toString()) || i == 0){
	        				facId += " " + map.get("facId").toString();
	        			}else{
	        				tempMap.put("facId", facId);
	        				tempMap.put("sisu", prevSisu);
	        				facPlList.add(tempMap);
	        				tempMap = new EgovMap();

	        				facId = " " + map.get("facId").toString();
	        			}

	        			//마지막 부분 담기
	        			if(i == (tempFacPlList.size() - 1)){
	        				sisu = map.get("sisu").toString();
	        				tempMap.put("facId", facId);
	        				tempMap.put("sisu", sisu);
	        				facPlList.add(tempMap);
	        			}

	        			prevPlId = map.get("plId").toString();
	        			prevSisu = map.get("sisu").toString();
	        		}
	        	}
	        	model.addAttribute("facPlList", facPlList);
			}

        	//과제타입코드
        	voComCode.setCodeId("LMS80");
        	List<CmmnDetailCode> homeworkComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("homeworkComCode", homeworkComCode);

        	//수료기준
    		ctgry.setCtgrymasterId("CTGMST_0000000000010");
    		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        	//교재 및 부교재
        	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
    	}

      	request.getSession().setAttribute("sessionVO", searchVO);

        return "/mng/lms/crcl/CurriculumView";
    }

    @RequestMapping("/mng/lms/crcl/updateCurriculum.do")
    public String updateCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String retStr = "forward:/mng/lms/crcl/CurriculumList.do";
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
        curriculumService.updateCurriculum(searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

    @RequestMapping("/mng/lms/crcl/updatePsCodeCurriculum.do")
    public String updatePsCodeCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String retStr = "forward:/mng/lms/crcl/CurriculumList.do";
    	/*
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	 */
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
    	if("D".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "승인취소 되었습니다.");
        	searchVO.setProcessSttusCode("0");
        }else if("Y".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "승인 되었습니다.");
        	searchVO.setProcessSttusCode("1");
        	retStr = "forward:/mng/lms/crcl/selectCurriculum.do";
        }else if("N".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "반려 되었습니다.");
        	retStr = "forward:/mng/lms/crcl/selectCurriculum.do";
        }else if("R".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "반려취소 되었습니다.");
        	retStr = "forward:/mng/lms/crcl/selectCurriculum.do";
        }else if(!EgovStringUtil.isEmpty(searchVO.getProcessSttusCode())){
        	if(Integer.parseInt(searchVO.getProcessSttusCode()) > 0){
        		model.addAttribute("message", "과정상태가 변경 되었습니다.");
            	retStr = "forward:/mng/lms/manage/lmsControl.do";
        	}
        }else if("0".equals(searchVO.getProcessSttusCode())){
        	model.addAttribute("message", "확정취소 되었습니다.");
        }else if(!EgovStringUtil.isEmpty(searchVO.getComnt())){
        	retStr = "forward:/mng/lms/crcl/selectCurriculum.do";
        }
    	
        curriculumService.updatePsCodeCurriculum(searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }


    @RequestMapping("/mng/lms/crcl/deleteCurriculum.do")
    public String deleteCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
        curriculumService.deleteCurriculum(searchVO);
        return "forward:/mng/lms/crcl/CurriculumList.do";
    }

    @RequestMapping("/mng/lms/crcl/updateCurriculumPart.do")
    public void updateCurriculumPart(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	//String retStr = "forward:/mng/lms/crcl/CurriculumList.do";

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}

        String successYn = "Y";

		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(Integer.MAX_VALUE);

		curriculumService.updateCurriculumPart(searchVO, request, response);

        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    @RequestMapping("/mng/lms/crcl/selectCurriculumStudent.do")
    public String selectCurriculumStudent(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);

        return "/mng/lms/crcl/CurriculumStudentView";
    }
    
    //과정관리 >  과제 > 과정평가
    @RequestMapping(value = "/mng/lms/crcl/homeworkTestList.do")
  	public String selectHomeworkTestList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	//수강 그룹 조회(반,조)
  		List selectGroupList = curriculumMemberService.selectGroupList(searchVO);
  		model.addAttribute("selectGroupList", selectGroupList);

      	//과제 목록
      	searchVO.setHomeworkScoreAt("Y");
      	searchVO.setPagingFlag("N");
      	List homeworkList = curriculumService.selectHomeworkList(searchVO);
      	model.addAttribute("homeworkList", homeworkList);

      	//수강 대상자 조회
      	if(searchVO.getSearchClassCnt() == null || searchVO.getSearchClassCnt() == 0){
      		searchVO.setSearchClassCnt(0);
      	}
  		List selectStudentList = curriculumService.homeworkScoreSum(searchVO);
  		model.addAttribute("selectStudentList", selectStudentList);

  		//학생 점수 조회
  		List homeworkScoreList = curriculumService.homeworkScoreList(searchVO);
  		model.addAttribute("scoreList", homeworkScoreList);

        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/mng/lms/crcl/HomeworkTestList";
  	}
    
    //과정관리 > 과제 > 과제평가 성적반영
  	@RequestMapping(value = "/mng/lms/crcl/homeworkTestUpdate.do")
  	public String homeworkTestUpdate(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		curriculumService.updateScoreApply(searchVO);

  		return "forward:/mng/lms/crcl/homeworkTestList.do";
  	}
    
    //과정관리 > 과제
  	@RequestMapping(value = "/mng/lms/crcl/homeworkList.do")
  	public String selectHomeworkList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      	//과제 조회
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

  		//과제 총 합계
      	int totCnt = curriculumService.selectHomeworkTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);

  		return "/mng/lms/crcl/HomeworkList";
  	}

  	//과제 상세
  	@RequestMapping("/mng/lms/crcl/selectHomeworkArticle.do")
    public String selectHomeworkArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

  		//과정 담당자 인지 확인
      	/*LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);*/

      	//등록 된 시간표 담당교원
  		ScheduleMngVO scheduleMngVO = new ScheduleMngVO();
      	
      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());

    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

        //수업 담당자 인지 확인
        /*String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);*/

      	CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
      	model.addAttribute("homeworkVO", homeworkVO);

  		if("homeworksubmit".equals(request.getParameter("viewFlag"))) {
  			//과제 제출 상세 조회
  			CurriculumVO homeworkSubmitVO = curriculumService.selectHomeworkSubmitArticle(searchVO);
  	      	model.addAttribute("homeworkSubmitVO", homeworkSubmitVO);

  	      	//조원 조회
  	  		searchVO.setClassCnt(homeworkSubmitVO.getClassCnt());
  	  	    searchVO.setGroupCnt(homeworkSubmitVO.getGroupCnt());
  	  		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
  	  		model.addAttribute("pickStudentList", selectPickStudentList);
  		} else {
  			//과제 제출 목록 조회
  	      	searchVO.setHwType(homeworkVO.getHwType());
  	  		List<?> selectHomeworkSubjectList = curriculumService.selectHomeworkSubjectList(searchVO);
  	  		model.addAttribute("selectHomeworkSubjectList", selectHomeworkSubjectList);
  		}

  		//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
      	
      	//과제 제출 이미지 경로
      	model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));
      	
      	request.getSession().setAttribute("sessionVO", searchVO);

	    return "/mng/lms/crcl/HomeworkArticle";
    }

  	//학생 공개 업데이트
  	@RequestMapping("/mng/lms/crcl/updateStuOpenAt.do")
    public String updateStuOpenAt(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}

  		curriculumService.updateStuOpenAt(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  	//과제 삭제
  	@RequestMapping("/mng/lms/crcl/deleteHomeworkArticle.do")
    public String deleteHomeworkArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(!EgovStringUtil.isEmpty(request.getParameter("menuFlag"))) {
  			forwardUrl = "redirect:/mng/lms/manage/homeworkTotalList.do";
  		} else if("CURRICULUM_MANAGE".equals(request.getParameter("menu"))) {
  			forwardUrl = "forward:/mng/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/mng/lms/manage/homeworkList.do";
  		}

  		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

  		curriculumService.deleteHomeworkArticle(searchVO);

  		request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//후기 선정 업데이트
  	@RequestMapping("/mng/lms/crcl/updateCommentPickAt.do")
    public String updateCommentPickAt(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}

  		curriculumService.updateCommentPickAt(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  	//점수 및 피드백 처리
  	@RequestMapping("/mng/lms/crcl/updateFdb.do")
    public String updateFdb(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");

      	if(request.getSession().getAttribute("sessionVO") == null) {
      		return retStr;
  		}

  		curriculumService.updateFdb(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  	//과제 등록
  	@RequestMapping("/mng/lms/crcl/insertHomeworkArticle.do")
    public String insertHomeworkArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(!EgovStringUtil.isEmpty(request.getParameter("menuFlag"))) {
  			forwardUrl = "redirect:/mng/lms/manage/homeworkTotalList.do";
  		} else if(EgovStringUtil.isEmpty(request.getParameter("plId"))) {
  			forwardUrl = "forward:/mng/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/mng/lms/manage/homeworkList.do";
  		}

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		searchVO.setFrstRegisterId(user.getId());
		searchVO.setNtcrNm(user.getName());

		String atchFileId = "";

	    List<FileVO> result = null;

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	        result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, "",
	        		siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
	        atchFileId = fileMngService.insertFileInfs(result);
	    }

	    searchVO.setAtchFileId(atchFileId);
	    
	    //관리자에서 과정과제를 등록하면
	    String manageId = "";
	    String manageNm = "";

      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		manageId = map.get("userId").toString();
          		manageNm = map.get("userNm").toString();
          		if(i == 0){
          			break;		
          		}
          	}
      	}
	    searchVO.setFrstRegisterId(manageId);
	    searchVO.setNtcrNm(manageNm);
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지
	    searchVO.setOpenTime(searchVO.getOpenTime().replace(":", ""));
	    searchVO.setCloseTime(searchVO.getCloseTime().replace(":", ""));

	    curriculumService.insertHomeworkArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//과제 수정
  	@RequestMapping("/mng/lms/crcl/updateHomeworkArticle.do")
    public String updateHomeworkArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(!EgovStringUtil.isEmpty(request.getParameter("menuFlag"))) {
  			forwardUrl = "redirect:/mng/lms/manage/homeworkTotalList.do";
  		} else if(EgovStringUtil.isEmpty(request.getParameter("plId"))) {
  			forwardUrl = "forward:/mng/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/mng/lms/manage/homeworkList.do";
  		}

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		String atchFileId = searchVO.getAtchFileId();

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
		    if(EgovStringUtil.isEmpty(atchFileId)) {
			  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
			    atchFileId = fileMngService.insertFileInfs(result);
			    searchVO.setAtchFileId(atchFileId);
		    } else {
			    FileVO fvo = new FileVO();
			    fvo.setAtchFileId(atchFileId);
			    //int cnt = fileMngService.getMaxFileSN(fvo);
			    List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getBbsId(), "", "");
			    fileMngService.updateFileInfs(_result);
		    }
	    }

	    searchVO.setLastUpdusrId(user.getId());
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지
	    searchVO.setOpenTime(searchVO.getOpenTime().replace(":", ""));
	    searchVO.setCloseTime(searchVO.getCloseTime().replace(":", ""));

	    curriculumService.updateHomeworkArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }
}
