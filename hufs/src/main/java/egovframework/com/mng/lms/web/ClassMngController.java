package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.SearchVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.ClassManageService;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class ClassMngController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

	@Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;

	@Resource(name = "classManageService")
    private ClassManageService classManageService;

    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;


   @RequestMapping(value="/mng/lms/cla/classFileList.do")
    public String classFileList(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		//** EgovPropertyService.sample */
	    fileVo.setPageUnit(propertyService.getInt("pageUnit"));
	    fileVo.setPageSize(propertyService.getInt("pageSize"));

    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(fileVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(fileVo.getPageUnit());
		paginationInfo.setPageSize(fileVo.getPageSize());

		fileVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		fileVo.setLastIndex(paginationInfo.getLastRecordIndex());
		fileVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//주관기관조회
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

		//과정조회
		/*List<?> curriculumbaseList = this.classManageService.selectClassCurriculumList(fileVo);
        model.addAttribute("curriculumbaseList", curriculumbaseList);*/

		EgovMap resultMap = this.classManageService.selectFileListMap(fileVo,request);
		model.addAttribute("fileList", resultMap.get("resultList"));
		model.addAttribute("fileListCnt", resultMap.get("resultCnt"));
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
        model.addAttribute("paginationInfo", paginationInfo);
        return "/mng/lms/cla/classFileList2";
    }

   @RequestMapping("/mng/lms/cla/classCurriculumList.json")
   public void classCurriculumList(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");

		List<?> curriculumbaseList = classManageService.selectClassCurriculumList(fileVo);
		model.addAttribute("curriculumbaseList", curriculumbaseList);

		jo.put("curriculumbaseList", curriculumbaseList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
   }

   @RequestMapping("/mng/lms/cla/classFileView.do")
   public String classFileView(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
	   model.addAttribute("fileInfo", classManageService.selectFileInfo(fileVo));
	   return "/mng/lms/cla/classFileView";
   }

   @RequestMapping(value="/mng/lms/cla/curriculumStudyList.do")
   public String curriculumStudyList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		//** EgovPropertyService.sample */
	    searchVO.setPageUnit(propertyService.getInt("pageUnit"));
	    searchVO.setPageSize(propertyService.getInt("pageSize"));

	    PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//주관기관조회
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

		//기본과정
		model.addAttribute("curriculumBaseList", this.classManageService.selectCurriculumBaseList(searchVO));

		//과정
		model.addAttribute("curriculumList", this.classManageService.selectCurriculumList(searchVO));

		//과정 수업 스케쥴만 조회
		searchVO.setSearchPlType("crcl");

		EgovMap resultMap = this.classManageService.selectCurriculumStudyMap(searchVO);
		model.addAttribute("studyList", resultMap.get("resultList"));
		model.addAttribute("studyListCnt", resultMap.get("resultCnt"));
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
       model.addAttribute("paginationInfo", paginationInfo);
       return "/mng/lms/cla/curriculumStudyList";
   }

   @RequestMapping("/mng/lms/cla/curriculumList.json")
   public void curriculumList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");
		//기본과정
		jo.put("curriculumBaseList", this.classManageService.selectCurriculumBaseList(searchVO));
		//과정
		jo.put("curriculumList", this.classManageService.selectCurriculumList(searchVO));
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
   }

   @RequestMapping(value="/mng/lms/cla/curriculumBoardList.do")
   public String curriculumBoardList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		//** EgovPropertyService.sample */
	    searchVO.setPageUnit(propertyService.getInt("pageUnit"));
	    searchVO.setPageSize(propertyService.getInt("pageSize"));

	    PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		//주관기관조회
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

		//기본과정
		model.addAttribute("curriculumBaseList", this.classManageService.selectCurriculumBaseList(searchVO));

		//과정
		//model.addAttribute("curriculumList", this.classManageService.selectCurriculumList(searchVO));

		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

		EgovMap resultMap = this.classManageService.selectCurriculumBoardMap(searchVO);
		model.addAttribute("boardList", resultMap.get("resultList"));
		model.addAttribute("boardListCnt", resultMap.get("resultCnt"));
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
       model.addAttribute("paginationInfo", paginationInfo);
       return "/mng/lms/cla/curriculumBoardList";
   }

   @RequestMapping(value="/mng/lms/cla/classSurveyList.do")
   public String classSurveyList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
		//** EgovPropertyService.sample */
	    searchVO.setPageUnit(propertyService.getInt("pageUnit"));
	    searchVO.setPageSize(propertyService.getInt("pageSize"));

	    PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		if(request.getMethod().equals("GET") && searchVO.getSearchProcessSttusCode() != null){
			searchVO.setSearchProcessSttusCode(String.valueOf(searchVO.getSearchProcessSttusCode()));
		}
		//주관기관조회
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

		//과정
		//model.addAttribute("curriculumList", this.classManageService.selectCurriculumList(searchVO));

		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
	   	voComCode.setCodeId("LMS30");
	   	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
	   	model.addAttribute("statusComCode", statusComCode);

		EgovMap resultMap = this.classManageService.selectSurveyMap(searchVO);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		model.addAttribute("surveyListCnt", resultMap.get("resultCnt"));
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
      model.addAttribute("paginationInfo", paginationInfo);
      return "/mng/lms/cla/classSurveyList";
  }
}
