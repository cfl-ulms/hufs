package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Board;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.GradeService;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * @Class Name : CurriculumController.java
 * @Description : Curriculum Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Controller
public class LmsManageMngController {

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

    @Resource(name = "EgovBBSCtgryMasterService")
    private EgovBBSCtgryMasterService egovBBSCtgryMasterService;

    @Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil               fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService            fileMngService;

    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

    /** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;

    @Resource(name = "EgovBBSManageService")
	private EgovBBSManageService          bbsMngService;

    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "gradeService")
	private GradeService gradeService;

    //과정관리
    @RequestMapping(value="/mng/lms/manage/lmsControl.do")
    public String lmsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

    	//게시판 카테고리 목록
		CtgryMaster ctgryMaster = new CtgryMaster();
		ctgryMaster.setSiteId(searchVO.getCrclId());
		List<CtgryMaster> ctgryMasterList = egovBBSCtgryMasterService.selectCtgrymasterList(ctgryMaster);

		List<Ctgry> ctgryList = new ArrayList<Ctgry>();
		for(int i = 0; i < ctgryMasterList.size(); i++){
			Ctgry ctgry = new Ctgry();
			ctgry.setCtgrymasterId(ctgryMasterList.get(i).getCtgrymasterId());
			List<Ctgry> ctgryResultList = egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
			//카테고리 리스트를 하나로 합침
			for(int a = 0; a < ctgryResultList.size(); a++){
				Ctgry ctgryTemp = ctgryResultList.get(a);
				ctgryList.add(ctgryTemp);
			}
		}
		model.addAttribute("ctgryList", ctgryList);

		//과정정보
		CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(curriculumVO));

		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

        return "/mng/lms/manage/lmsControl";
    }

    //과정게시판관리
    @RequestMapping(value="/mng/lms/manage/lmsBbsControl.do")
    public String lmsBbsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

		//과정정보
		CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(curriculumVO));

		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

        return "/mng/lms/manage/lmsBbsControl";
    }

    //과정게시판등록
    @RequestMapping(value="/mng/lms/manage/lmsBbsInsert.do")
    public String lmsBbsInsert(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

    	//게시판 마스터 등록&수정
    	//기본 게시판은 수정처리 안함
    	for(int i = 1; i < searchVO.getBbsIdList().size(); i++){
    		BoardMasterVO boardMasterVO = new BoardMasterVO();
    		boardMasterVO.setCtgrymasterId(searchVO.getCtgrymasterIdList().get(i));
    		boardMasterVO.setSiteId(searchVO.getCrclId());
    		boardMasterVO.setSysTyCode(searchVO.getSysTyCodeList().get(i));
    		boardMasterVO.setBbsId(searchVO.getBbsIdList().get(i));
    		boardMasterVO.setBbsNm(searchVO.getBbsNmList().get(i));
    		boardMasterVO.setCommentUseAt("N");
    		boardMasterVO.setReplyPosblAt("N");
    		boardMasterVO.setFileAtchPosblAt("Y");
    		boardMasterVO.setPosblAtchFileNumber("10");
    		boardMasterVO.setPosblAtchFileSize("1024");
    		boardMasterVO.setInqireAuthor("02");
    		boardMasterVO.setRegistAuthor("10");
    		boardMasterVO.setAnswerAuthor("02");
    		boardMasterVO.setOthbcUseAt("N");
    		boardMasterVO.setSourcId(propertiesService.getString("Crcl.sourcId"));
    		boardMasterVO.setTmplatId(propertiesService.getString("Crcl.tmplatId"));
    		boardMasterVO.setSvcAt("Y");
    		if(EgovStringUtil.isEmpty(searchVO.getBbsIdList().get(i))){
    			boardMasterVO.setFrstRegisterId(user.getId());
    			bbsAttrbService.insertBBSMastetInf(boardMasterVO);
    		}else{
    			boardMasterVO.setLastUpdusrId(user.getId());
    			bbsAttrbService.updateBBSMasterInf(boardMasterVO);
    		}
    	}

        return "forward:/mng/lms/manage/lmsControl.do";
    }

    //과정게시판삭제
    @RequestMapping(value="/mng/lms/manage/lmsBbsDelete.do")
    public void lmsBbsDelete(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

    	//게시판 마스터 삭제
   		BoardMasterVO boardMasterVO = new BoardMasterVO();
   		boardMasterVO.setBbsId(searchVO.getBbsId());
   		boardMasterVO.setSiteId(searchVO.getCrclId());
   		boardMasterVO.setCtgrymasterId(searchVO.getCtgrymasterId());
   		boardMasterVO.setLastUpdusrId(user.getId());

   		bbsAttrbService.deleteBBSMasterInf(boardMasterVO);

   		//카테고리 삭제
		CtgryMaster ctgryMaster = new CtgryMaster();
		ctgryMaster.setCtgrymasterId(searchVO.getCtgrymasterId());
		ctgryMaster.setSiteId(searchVO.getCrclId());
		egovBBSCtgryMasterService.deleteComtnbbsctgrymaster(ctgryMaster);

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    //과정게시판 선택 팝업
    @RequestMapping(value="/mng/lms/manage/lmsBbsPopList.do")
    public String lmsBbsPopList(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

        return "/mng/lms/manage/lmsBbsPopList";
    }

    //총괄평가기준 선택 팝업
    @RequestMapping(value="/mng/lms/manage/lmsEvtPopList.do")
    public String lmsEvtPopList(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000012");
		model.addAttribute("evaluationBaseList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        return "/mng/lms/manage/lmsEvtPopList";
    }

    //과정 시간표 설정
    @RequestMapping(value="/mng/lms/manage/schedule.do")
    public String schedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

        return "/mng/lms/manage/schedule";
    }

    //과정 시간표 등록&수정
    @RequestMapping(value="/mng/lms/manage/scheduleReg.do")
    public String scheduleReg(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정시간코드
    	searchVO.setPlType("crcl");

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
        	model.addAttribute("curriculumVO", curriculumVO);
        	
    		searchVO.setCrclLang(request.getParameter("crclLang"));
    		
    		//시간표 수기 입력
    		if("N".equals(curriculumVO.getCampustimeUseAt())) {
    			searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
    			searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());
    		}
    		
    		//등록
    		if(EgovStringUtil.isEmpty(searchVO.getPlId())){
    			searchVO.setFrstRegisterId(user.getId());
        		scheduleMngService.insertClassSchedule(searchVO);
    		}else{ //수정
    			searchVO.setLastUpdusrId(user.getId());
        		scheduleMngService.updateClassSchedule(searchVO, request, response);
    		}

        	//과정에 등록된 교원
        	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(curriculumVO));

        	//캠퍼스 시간표
        	searchVO.setCampusId(curriculumVO.getCampusId());
        	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

        	//등록 된 시간표
        	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
        	scheduleMngVO.setPeriodTxt(searchVO.getPeriodTxt());
        	model.addAttribute("scheduleMngVO", scheduleMngVO);

        	//등록 된 시간표 담당교원
        	List facPlList = scheduleMngService.selectFacultyList(searchVO);
        	model.addAttribute("facPlList", facPlList);
    	}else{

    	}

		return "/mng/lms/manage/scheduleIstAjax";
    }

    //과정 시간표 삭제
    @RequestMapping(value="/mng/lms/manage/dltSchedule.json")
    public void dltSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";

    	scheduleMngService.deleteClassSchedule(searchVO);

    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    //과정 수업계획
    @RequestMapping(value="/mng/lms/manage/studyPlan.do")
    public String studyPlan(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정 시간표(수업계획)
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(curriculumVO));

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

        return "/mng/lms/manage/studyPlan";
    }

    //과정 시간표 설정
    @RequestMapping(value="/mng/lms/manage/scheduleCalendar.do")
    public String scheduleCalendar(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	searchVO.setPlType("crcl");

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	Ctgry ctgry = new Ctgry();
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));
    	model.addAttribute("crclId", searchVO.getCrclId());


        return "/mng/lms/manage/scheduleCalendar";
    }

    //과정 시간표 상세
    @RequestMapping(value="/mng/lms/manage/scheduleCalendarView.do")
    public String scheduleCalendarView(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	searchVO.setPlType("crcl");

    	Ctgry ctgry = new Ctgry();

    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);
    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));
    	model.addAttribute("scheduleMngVO", scheduleMngService.selectCalendarScheduleView(searchVO));
    	model.addAttribute("crclLang", request.getParameter("crclLang"));

        return "/mng/lms/manage/scheduleCalendarView";
    }

    @RequestMapping(value="/mng/lms/manage/insertCalendarSchedule.do")
    public String insertCalendarSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setLastUpdusrId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.insertClassSchedule(searchVO);

  		return "redirect:/mng/lms/manage/scheduleCalendar.do?crclId="+searchVO.getCrclId()+"&type=cal";

    }

    @RequestMapping(value="/mng/lms/manage/updateCalendarSchedule.do")
    public String updateCalendarSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setLastUpdusrId(loginVO.getId());
  		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
  		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

  		scheduleMngService.updateClassSchedule(searchVO, request, response);

  		return "redirect:/mng/lms/manage/scheduleCalendar.do?crclId="+searchVO.getCrclId()+"&type=cal";

    }

    //과정 수업계획 상세
    @RequestMapping(value="/mng/lms/manage/studyPlanView.do")
    public String studyPlanView(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//수업계획
    	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
    	model.addAttribute("scheduleMngVO", scheduleMngVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));
    	/*
    	//캠퍼스
    	Ctgry ctgry = new Ctgry();
		//ctgry.setCtgrymasterId("CTGMST_0000000000017");
		//model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//레벨
		ctgry.setCtgrymasterId("CTGMST_0000000000014");
		model.addAttribute("levelList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//온라인강의(강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000006");
		model.addAttribute("onList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//오프라인강의(비강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000007");
		model.addAttribute("offList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//평가
		ctgry.setCtgrymasterId("CTGMST_0000000000008");
		model.addAttribute("evtList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		*/
		//과정에 등록된 교원
    	model.addAttribute("evtResultList", scheduleMngService.selectStudyEvtList(searchVO));

    	//등록한 수업자료 리스트
    	model.addAttribute("fileList", scheduleMngService.selectStudyFileList(searchVO));

        return "/mng/lms/manage/studyPlanView";
    }

    //과정 수업계획 등록&수정 페이지
    @RequestMapping(value="/mng/lms/manage/studyPlanReg.do")
    public String studyPlanReg(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//수업계획
    	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
    	model.addAttribute("scheduleMngVO", scheduleMngVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

    	//캠퍼스
    	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//레벨
		ctgry.setCtgrymasterId("CTGMST_0000000000014");
		model.addAttribute("levelList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//온라인강의(강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000006");
		model.addAttribute("onList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//오프라인강의(비강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000007");
		model.addAttribute("offList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//평가
		ctgry.setCtgrymasterId("CTGMST_0000000000008");
		model.addAttribute("evtList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//과정에 등록된 교원
    	model.addAttribute("evtResultList", scheduleMngService.selectStudyEvtList(searchVO));

    	//등록한 수업자료 리스트
    	model.addAttribute("fileList", scheduleMngService.selectStudyFileList(searchVO));

		request.getSession().setAttribute("sessionVO", searchVO);

        return "/mng/lms/manage/studyPlanReg";
    }

    //과정 수업 등록&수정
    @RequestMapping(value="/mng/lms/manage/studyPlanUpt.do")
    public String studyPlanUpt(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/crclb/CurriculumbaseList.do";
		}

    	//과정시간코드
    	searchVO.setPlType("crcl");

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    		
    		//시간표 수기 입력
    		if("N".equals(curriculumVO.getCampustimeUseAt())) {
    			searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
    			searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());
    		}
    		
    		//등록
    		if(EgovStringUtil.isEmpty(searchVO.getPlId())){
    			searchVO.setFrstRegisterId(user.getId());
        		scheduleMngService.insertClassSchedule(searchVO);
    		}else{ //수정
    			searchVO.setLastUpdusrId(user.getId());
    			scheduleMngService.updateClassSchedule(searchVO, request, response);
    		}
    	}else{

    	}

    	request.getSession().removeAttribute("sessionVO");

		return "forward:/mng/lms/manage/studyPlanView.do";
    }

    //과정게시판 목록
    @RequestMapping(value="/mng/lms/manage/lmsBbsList.do")
    public String lmsBbsList(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

    	//bbsid체크
    	if(EgovStringUtil.isEmpty(lmsMngVO.getBbsId())){
    		master = result.get(0);
    	}else{
    		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
    		boardMasterVO.setBbsId(lmsMngVO.getBbsId());

      	  	master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
    	}

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		boardVO.setFrstRegisterId(user.getId());
    		boardVO.setAdminAt("Y");

    		model.addAttribute("sessionUniqId", user.getId());
    	}
    	//과정 반 조회

    	if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selectClassList", selectClassList);
    	}else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selectGroupList", selectGroupList);
    	}

        PaginationInfo paginationInfo = new PaginationInfo();
  	  	if(master != null) {

  		  // 페이징 정보 설정
  		  boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
  		  boardVO.setPageSize(propertiesService.getInt("pageSize"));
  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  /*
  		  //공지게시물 가져오기
  		  BoardVO noticeVO = new BoardVO();
  		  noticeVO.setBbsId(lmsMngVO.getBbsId());
  		  noticeVO.setTmplatId(master.getTmplatId());
  		  noticeVO.setCommentUseAt(master.getCommentUseAt());
  		  noticeVO.setSearchNoticeAt("Y");
  		  noticeVO.setFirstIndex(0);
  		  noticeVO.setRecordCountPerPage(9999);

  		  model.addAttribute("noticeList", bbsMngService.selectBoardArticles(noticeVO));
  		  */
  		  paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
  		  paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
  		  paginationInfo.setPageSize(boardVO.getPageSize());

  		  boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
  		  boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
  		  boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

  		  boardVO.setBbsId(master.getBbsId());
  		  boardVO.setCommentUseAt(master.getCommentUseAt());
  		  boardVO.setTmplatId(master.getTmplatId());
  		  boardVO.setBbsAttrbCode(master.getBbsAttrbCode());

  		  List<BoardVO> resultList = bbsMngService.selectBoardArticles(boardVO);
  		  int totCnt = bbsMngService.selectBoardArticlesCnt(boardVO);

  		  paginationInfo.setTotalRecordCount(totCnt);

  		  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
  			  Ctgry ctgry = new Ctgry();
  			  ctgry.setCtgrymasterId(master.getCtgrymasterId());
  			  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
  		  }

  		  model.addAttribute("resultList", resultList);
  		  model.addAttribute("resultCnt", totCnt);
  		  model.addAttribute("brdMstrVO", master);
        }

  	  	model.addAttribute("paginationInfo", paginationInfo);

        return "/mng/lms/manage/lmsBbsList";
    }

    //과정게시판 등록
    @RequestMapping(value="/mng/lms/manage/lmsBbsAdd.do")
    public String lmsBbsAdd(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

    	BoardMasterVO vo = new BoardMasterVO();
    	vo.setSiteId(lmsMngVO.getCrclId());
	  	vo.setSysTyCode(boardVO.getSysTyCode());
	  	vo.setBbsId(boardVO.getBbsId());
	  	vo.setTrgetId(boardVO.getTrgetId());

	  	BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

	  	if(master != null) {
	  	  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
	  		  Ctgry ctgry = new Ctgry();
	  		  ctgry.setCtgrymasterId(master.getCtgrymasterId());
	  		  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
	  		  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
	  	    }

	  	  model.addAttribute("brdMstrVO", master);

	  	if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selecteResultList", selectClassList);
    	}else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selecteResultList", selectGroupList);
    	}

	  	  Board board = new Board();
	  	  model.addAttribute("board", board);

	  	  request.getSession().setAttribute("sessionVO", boardVO);
	  	}

        return "/mng/lms/manage/lmsBbsAdd";
    }

    //게시판 게시물 상세
    @RequestMapping("/mng/lms/manage/lmsBbsView.do")
    public String lmsBbsView(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

  	  boardVO.setAdminAt("Y");

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(lmsMngVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {

  		  model.addAttribute("brdMstrVO", master);

  		  // 조회수 증가 여부 지정
  		  boardVO.setPlusCount(false);
  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

  		  if(user != null) {
  			  model.addAttribute("sessionUniqId", user.getId());
  		  }
  	  }


  	  return "/mng/lms/manage/lmsBbsView";
    }

    //과정게시판 등록 프로세스
    @RequestMapping("/mng/lms/manage/insertBoardArticle.do")
    public String insertBoardArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO,
  		  Board board, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

  	  if(request.getSession().getAttribute("sessionVO") == null) {
  		  return "forward:/mng/lms/manage/lmsBbsList.do";
  	  }

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  String atchFileId = "";
  		  if(EgovStringUtil.isEmpty(boardVO.getSiteId())){
  			  boardVO.setSiteId(master.getSiteId());
  		  }

  	      List<FileVO> result = null;

  	      final Map<String, MultipartFile> files = multiRequest.getFileMap();
  	      if(!files.isEmpty()) {
  	        result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, "", boardVO.getSiteId(), boardVO.getBbsId(),"","");
  	        atchFileId = fileMngService.insertFileInfs(result);
  	      }


  	      boardVO.setAtchFileId(atchFileId);
  	      boardVO.setFrstRegisterId(user.getId());
  	      boardVO.setNtcrNm(user.getName());
  	      boardVO.setCreatIp(EgovClntInfo.getClntIP(request));
  	      boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));

  	      bbsMngService.insertBoardArticle(boardVO, master);

  	      request.getSession().removeAttribute("sessionVO");
  	  }

  	  return "forward:/mng/lms/manage/lmsBbsList.do";
    }

    //게시판 게시물 수정
    @RequestMapping("/mng/lms/manage/forUpdateBoardArticle.do")
    public String selectBoardArticleForUpdt(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);


  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());
  	  boardVO.setAdminAt("Y");

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
  			  Ctgry ctgry = new Ctgry();
  			  ctgry.setCtgrymasterId(master.getCtgrymasterId());
  			  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
  	      }

  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);

  		  model.addAttribute("brdMstrVO", master);
  		  model.addAttribute("board", dataVO);

  		  request.getSession().setAttribute("sessionVO", boardVO);

  		  if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selecteResultList", selectClassList);
  		  }else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selecteResultList", selectGroupList);
  		  }
  	  }

  	  return "/mng/lms/manage/lmsBbsAdd";
    }

    //게시판 게시 글 수정프로세스
    @RequestMapping("/mng/lms/manage/updateBoardArticle.do")
    public String updateBoardArticle(final MultipartHttpServletRequest multiRequest,
  		  BoardVO board, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

      String returnUrl = "forward:/mng/lms/manage/lmsBbsList.do";
	  if(request.getSession().getAttribute("sessionVO") == null) {
		  if(request.getParameter("depth1") != null ? request.getParameter("depth1").toString() == "CRCL_BOARD" ? true : false : false){
 		   	return  "forward:/mng/lms/cla/curriculumBoardList.do";
  	  	  }else{
  	  	  	return "forward:/mng/lms/manage/lmsBbsList.do";
  	      }
	  }

	  String tempParam = request.getParameter("depth1") != null ? request.getParameter("depth1").toString() : "";
	  if("CRCL_BOARD".equals(tempParam)){
		  returnUrl = "forward:/mng/lms/cla/curriculumBoardList.do";
	  }

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  	  String atchFileId = board.getAtchFileId();

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(board.getCrclId());
  	  //vo.setSysTyCode(board.getSysTyCode());
  	  vo.setBbsId(board.getBbsId());
  	  vo.setTrgetId(board.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  if(EgovStringUtil.isEmpty(board.getSiteId())){
  			  board.setSiteId(master.getSiteId());
  		  }
  		  final Map<String, MultipartFile> files = multiRequest.getFileMap();
  		  if(!files.isEmpty()) {
  			  if(EgovStringUtil.isEmpty(atchFileId)) {
  				  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, atchFileId, board.getSiteId(), board.getBbsId(),"","");
  				  atchFileId = fileMngService.insertFileInfs(result);
  				  board.setAtchFileId(atchFileId);
  			  } else {
  				  FileVO fvo = new FileVO();
  				  fvo.setAtchFileId(atchFileId);
  				  int cnt = fileMngService.getMaxFileSN(fvo);
  				  List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, cnt, atchFileId, board.getSiteId(), board.getBbsId(),"","");
  				  fileMngService.updateFileInfs(_result);
  			  }
  		  }

  		  board.setAdminAt("Y");
  		  board.setLastUpdusrId(user.getId());
  		  board.setEstnData(EgovHttpUtil.getEstnParseData(request));

  		  bbsMngService.updateBoardArticle(board, master, false);

  		  request.getSession().removeAttribute("sessionVO");
      }

      return returnUrl;
    }

    //게시판 게시글 삭제
    @RequestMapping("/mng/lms/manage/deleteBoardArticle.do")
    public String deleteBoardArticle(@ModelAttribute("searchVO") BoardVO boardVO, BoardVO board, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  board.setAdminAt("Y");
  		  board.setLastUpdusrId(user.getId());
  		  bbsMngService.deleteBoardArticle(board, master);
  	  }

  	  return "forward:/mng/lms/manage/lmsBbsList.do";
    }

    //과정 설문
    @RequestMapping(value="/mng/lms/manage/curriculumSurveyList.do")
    public String curriculumSurveyList(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(surveyVo.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//설문리스트
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(surveyVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(surveyVo.getPageUnit());
		paginationInfo.setPageSize(surveyVo.getPageSize());

		surveyVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		surveyVo.setLastIndex(paginationInfo.getLastRecordIndex());
		surveyVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		EgovMap resultMap = this.surveyManageService.selectCurriculumSurvey(surveyVo);
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
		model.addAttribute("surveyCurriculumList", resultMap.get("resultList"));
		model.addAttribute("surveyCurriculumListCnt", resultMap.get("resultCnt"));
        model.addAttribute("paginationInfo", paginationInfo);


        return "/mng/lms/manage/curriculumSurveyList";
    }

    //과정 설문 (미)제출자 확인 - 기본 미제출자 조회
	@RequestMapping(value="/mng/lms/manage/curriculumSurveyMember.do")
	public String curriculumSurveyMember(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<EgovMap> memberList = new ArrayList<EgovMap>();
  		EgovMap surveyMemberSummary = new EgovMap();
  		if("TYPE_3".equals(surveyVo.getSchdulClCode())){
  			memberList = this.surveyManageService.selectSurveyProfessor(surveyVo);
  	  		surveyMemberSummary = this.surveyManageService.selectSurveyProfessorSummary(surveyVo);
  		}else {
  			memberList = this.surveyManageService.selectSurveyMember(surveyVo);
  	  		surveyMemberSummary = this.surveyManageService.selectSurveyMemberSummary(surveyVo);
  		}
  		
		model.addAttribute("memberList", memberList);
		model.addAttribute("surveyMemberSummary", surveyMemberSummary);
		
	    return "/mng/lms/manage/surveyMemberList";
	}

    //과정 설문 상세
    @RequestMapping(value="/mng/lms/manage/curriculumSurveyView.do")
    public String curriculumSurveyView(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(surveyVo.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	surveyVo.setSchdulClCode(request.getParameter("schdulClCode"));
    	
    	//과정 추가 정보
    	model.addAttribute("curriculumAddInfo", this.surveyManageService.selectCurriculumAddInfo(surveyVo));
    	//과정 설문
    	model.addAttribute("surveyAnswer", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVo));

        return "/mng/lms/manage/curriculumSurveyView";
    }

  //과정게시판 통계
    @RequestMapping(value="/mng/lms/manage/lmsBbsStatistics.do")
    public String lmsBbsStatistics(@ModelAttribute("searchVO") BoardVO boardVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	int totalBbsCnt = 0;

    	if(boardVO.getSearchType() == ""){
    		boardVO.setSearchType("student");
    	}
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(boardVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);


    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setSiteId(boardVO.getCrclId());
		boardMasterVO.setBbsId(boardVO.getBbsId());
		master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
		model.addAttribute("masterVo", master);

		boardVO.setCntType(master.getSysTyCode().toLowerCase());

    	//게시판 구분별 수
    	List<EgovMap> pieList = this.bbsMngService.selectBoardStatisticsPie(boardVO);
    	model.addAttribute("pieList", pieList);

    	if(pieList != null){
    		List<EgovMap> barList = new ArrayList<EgovMap>();
    		for(EgovMap tempMap : pieList){
    			boardVO.setCtgryId(tempMap.get("ctgryId").toString());
    			 if("0".equals(tempMap.get("ctgryLevel").toString())){
                     boardVO.setCtgryId(null);
                  }
    			EgovMap barMap = this.bbsMngService.selectBoardStatisticsBar(boardVO);
    			barList.add(barMap);
    			totalBbsCnt += Integer.parseInt(tempMap.get("cnt").toString());
    		}
    		model.addAttribute("barList", barList);
    		model.addAttribute("barMemberList", this.bbsMngService.selectBoardStatisticsBarMemberList(boardVO));
    		model.addAttribute("totalBbsCnt", totalBbsCnt);
    	}


    	PaginationInfo paginationInfo = new PaginationInfo();

  		// 페이징 정보 설정
    	if(boardVO.getSortType() == ""){
    		boardVO.setSortType("sum");
    	}

    	boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	boardVO.setPageSize(propertiesService.getInt("pageSize"));
    	paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
	  	paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
	  	paginationInfo.setPageSize(boardVO.getPageSize());

	  	boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
	  	boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
	  	boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

	  	EgovMap resultMap = this.bbsMngService.selectStatistics(boardVO);
	  	int totCnt = Integer.parseInt(resultMap.get("resultCnt").toString());
	  	paginationInfo.setTotalRecordCount(totCnt);

	  	EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
	  	if(tempCollect != null){
	  		model.addAttribute("attendCollect", tempCollect.get("colect"));
	  	}

	  	model.addAttribute("resultList", resultMap.get("resultList"));
	  	model.addAttribute("resultCnt", totCnt);
  	  	model.addAttribute("paginationInfo", paginationInfo);
        return "/mng/lms/manage/lmsBbsStatistics";
    }

    @RequestMapping(value="/mng/lms/manage/lmsBbsStatisticsBar.json")
    public void lmsBbsStatisticsBar(@ModelAttribute("searchVO") BoardVO boardVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	List<EgovMap> barList = new ArrayList<EgovMap>();
    	List<EgovMap> barMemberList = new ArrayList<EgovMap>();
    	//게시판 구분별 수
    	List<EgovMap> pieList = this.bbsMngService.selectBoardStatisticsPie(boardVO);
    	model.addAttribute("pieList", pieList);

    	if(pieList != null){
    		for(EgovMap tempMap : pieList){
    			boardVO.setCtgryId(tempMap.get("ctgryId").toString());
    			if("0".equals(tempMap.get("ctgryLevel").toString())){
	   				boardVO.setCtgryId(null);
	   			}
    			EgovMap barMap = this.bbsMngService.selectBoardStatisticsBar(boardVO);
				barList.add(barMap);
    		}
    		barMemberList = this.bbsMngService.selectBoardStatisticsBarMemberList(boardVO);
    	}


    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

  		jo.put("barList", barList);
  		jo.put("pieList", pieList);
  		jo.put("barMemberList", barMemberList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    @RequestMapping(value="/mng/lms/manage/selectLmsBbsList.do")
    public String selectLmsBbsList(@ModelAttribute("searchVO") BoardVO boardVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setSiteId(boardVO.getCrclId());
		boardMasterVO.setBbsId(boardVO.getBbsId());
		master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
		model.addAttribute("masterVo", master);

		boardVO.setCntType(master.getSysTyCode().toLowerCase());


    	PaginationInfo paginationInfo = new PaginationInfo();
    	boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	boardVO.setPageSize(propertiesService.getInt("pageSize"));
    	paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
	  	paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
	  	paginationInfo.setPageSize(boardVO.getPageSize());

	  	boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
	  	boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
	  	boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

	  	EgovMap resultMap = this.bbsMngService.selectMemBoard(boardVO);
	  	int totCnt = Integer.parseInt(resultMap.get("resultCnt").toString());
	  	paginationInfo.setTotalRecordCount(totCnt);


		EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
	  	if(tempCollect != null){
	  		model.addAttribute("attendCollect", tempCollect.get("colect"));
	  	}

	  	model.addAttribute("resultList", resultMap.get("resultList"));
	  	model.addAttribute("resultCnt", totCnt);
  	  	model.addAttribute("paginationInfo", paginationInfo);
        return "/mng/lms/manage/lmsMemBbsList";
    }

    //게시판 게시물 상세
    @RequestMapping("/mng/lms/manage/lmsMemBbsView.do")
    public String lmsMemBbsView(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());
  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);
	  model.addAttribute("brdMstrVO", master);
	  // 조회수 증가 여부 지정
	  boardVO.setPlusCount(false);
	  boardVO.setAdminAt("Y");
	  boardVO.setCtgrymasterId(master.getCtgrymasterId());
	  model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));
	  EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
	  	if(tempCollect != null){
	  		model.addAttribute("attendCollect", tempCollect.get("colect"));
	  	}

  	  return "/mng/lms/manage/lmsMemBbsView";
    }

    //수업 > 과제(수업과제)
  	@RequestMapping(value = "/mng/lms/manage/homeworkList.do")
  	public String selectHomeworkList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//과정 조회
      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	//과제 총 합계
      	int totCnt = curriculumService.selectHomeworkTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);

      	//등록 된 시간표 담당교원
      	ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());
    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

    	//과제 조회를 위한 유저 반,조 조회(학생 권한만 사용)
    	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	    	searchVO.setUserId(user.getId());
	      	CurriculumVO curriculumMemberVO = curriculumService.selectCurriculumMemberDetail(searchVO);

	      	searchVO.setClassCnt(curriculumMemberVO.getClassCnt());
	  		searchVO.setGroupCnt(curriculumMemberVO.getGroupCnt());
    	}

  		//과제 조회
    	searchVO.setPagingFlag("N");
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

      	model.addAttribute("USER_INFO", user);

      	//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	request.getSession().removeAttribute("sessionVO");

  		return "/mng/lms/manage/HomeworkList";
  	}

  //수업 > 과제
  	@RequestMapping(value = "/mng/lms/manage/homeworkTotalList.do")
  	public String selectHomeworkTotalList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());

  		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//주관기관조회
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      	//과제 조회
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

  		//과제 총 합계
      	int totCnt = curriculumService.selectHomeworkTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);

  		return "/mng/lms/manage/HomeworkTotalList";
  	}

	@RequestMapping(value = "/mng/lms/manage/manageReportList.do")
  	public String manageReportList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//주관기관조회
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
		model.addAttribute("ctgryList", ctgryList);

		//과정상태
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", this.curriculumService.selectAdminReportList(searchVO));
		int totCnt = this.curriculumService.selectAdminReportListCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("resultListCnt",totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

  		return "/mng/lms/manage/manageReportList";
  	}

  	// 운영보고서
  	@RequestMapping(value = "/mng/lms/manage/manageReport.do")
  	public String selectmanageReport(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());
  		model.addAttribute("USER_INFO", user);
  		
  		CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

  		//과정 담당자 인지 확인
      	String managerAt = "Y";
      	/*
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	*/
      	model.addAttribute("managerAt", managerAt);

      	//등록 된 시간표 담당교원
  		ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());

    	List facPlList = scheduleMngService.selectCrclFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

    	List facPlListCnt = scheduleMngService.selectCrclFacultyListCnt(scheduleMngVO);
    	model.addAttribute("facPlListCnt", facPlListCnt);
    	/*
        //수업 담당자 인지 확인
        String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);
		*/
        //학습내용
    	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

    	//교육과정 통계
  		model.addAttribute("curriculumSts", curriculumService.curriculumSts(searchVO));

    	//성적 목록
  		List gradeList = gradeService.selectGradeList(searchVO);
  		model.addAttribute("gradeList", gradeList);

        //과정 설문
        SurveyVO surveyVo = new SurveyVO();
        surveyVo.setCrclId(searchVO.getCrclId());
        //surveyVo.setSchdulId(this.surveyManageService.selectSurveyId(surveyVo));
        //과정만족도
        surveyVo.setSchdulId(curriculumVO.getSurveySatisfyType());
        if(!"".equals(surveyVo.getSchdulId().toString())){
        	//과정 추가 정보
        	model.addAttribute("curriculumAddInfo", this.surveyManageService.selectCurriculumAddInfo(surveyVo));
        	model.addAttribute("surveyAnswer", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVo));
        	
        	// 과정만족도 답변점수목록
        	model.addAllAttributes(this.surveyManageService.curriculumSurveyAnswerScore(surveyVo));
        	
        }
        //과정만족도 - 교원대상
        //surveyVo.setSchdulId(curriculumVO.getProfessorSatisfyType());
        SurveyVO surveyVO2 = new SurveyVO();
        surveyVO2.setCrclId(surveyVo.getCrclId());
        surveyVO2.setSchdulId(curriculumVO.getProfessorSatisfyType());
        if(!EgovStringUtil.isEmpty(surveyVO2.getSchdulId())){
        	
        	model.addAttribute("curriculumAddInfoType3", this.surveyManageService.selectCurriculumAddInfoType3(surveyVO2));
        	model.addAttribute("surveyAnswerType3", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVO2));
        	
        	// 과정만족도 답변점수목록
        	model.addAllAttributes(this.surveyManageService.curriculumSurveyAnswerScore3(surveyVO2));
        }

		return "/mng/lms/manage/manageReport";
  	}
  	
  	//운영보고서 수정
  	@RequestMapping(value = "/mng/lms/manage/updateReport.do")
  	public String updateReport(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());
  		if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}

  		curriculumService.updateCurriculumPart(searchVO, request, response);

		return "forward:/mng/lms/manage/manageReport.do";
  	}
}