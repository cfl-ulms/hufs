package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.uat.uia.service.LoginVO;

/**
 * @Class Name : CurriculumbaseController.java
 * @Description : Curriculumbase Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class CurriculumbaseMngController {

    @Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;
    
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;
	
    /**
	 * curriculumbase 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return "/mng/lms/crclb/CurriculumbaseList"
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/lms/crclb/CurriculumbaseList.do")
    public String selectCurriculumbaseList(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
        List<?> curriculumbaseList = curriculumbaseService.selectCurriculumbaseList(searchVO);
        model.addAttribute("resultList", curriculumbaseList);
        
        int totCnt = curriculumbaseService.selectCurriculumbaseListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        //과정체계
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000004");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//이수구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000003");
  		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//관리구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000005");
  		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//대상
  		ctgry.setCtgrymasterId("CTGMST_0000000000015");
  		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        
        return "/mng/lms/crclb/CurriculumbaseList";
    } 
    
    //기본과정명Ajax
    @RequestMapping(value="/mng/lms/crclb/CurriculumbaseList.json")
    public void CurriculumbaseListJson(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "Y";
    	
		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(Integer.MAX_VALUE);
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("items", curriculumbaseService.selectCurriculumbaseList(searchVO));
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    } 
    
    @RequestMapping("/mng/lms/crclb/addCurriculumbaseView.do")
    public String addCurriculumbaseView(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	
    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//이수구분
		ctgry.setCtgrymasterId("CTGMST_0000000000003");
		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//관리구분
		ctgry.setCtgrymasterId("CTGMST_0000000000005");
		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//대상
		ctgry.setCtgrymasterId("CTGMST_0000000000015");
		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//성적기준
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		// 과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList3", resultMap3.get("resultList"));
		
        model.addAttribute("curriculumbaseVO", searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/lms/crclb/CurriculumbaseRegister";
    }
    
    @RequestMapping("/mng/lms/crclb/addCurriculumbase.do")
    public String addCurriculumbase(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/crclb/CurriculumbaseList.do";
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
        curriculumbaseService.insertCurriculumbase(searchVO);
        
        request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mng/lms/crclb/CurriculumbaseList.do";
    }
    
    @RequestMapping("/mng/lms/crclb/updateCurriculumbaseView.do")
    public String updateCurriculumbaseView(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//이수구분
		ctgry.setCtgrymasterId("CTGMST_0000000000003");
		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//관리구분
		ctgry.setCtgrymasterId("CTGMST_0000000000005");
		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//대상
		ctgry.setCtgrymasterId("CTGMST_0000000000015");
		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//성적기준
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(searchVO));
		
		//과정학습참고자료
		model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(searchVO));
		request.getSession().setAttribute("sessionVO", searchVO);
		
		// 과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList3", resultMap3.get("resultList"));
		
        return "/mng/lms/crclb/CurriculumbaseRegister";
    }
    
    @RequestMapping("/mng/lms/crclb/selectCurriculumbaseAjax.do")
    public String selectCurriculumbase(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	//과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList3", resultMap3.get("resultList"));
		
    	model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(searchVO));
    	//과정학습참고자료
		model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(searchVO));
    			
        return "/mng/lms/crclb/CurriculumbaseDetailAjax";
    }
    
    @RequestMapping("/mng/lms/crclb/updateCurriculumbase.do")
    public String updateCurriculumbase(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/crclb/CurriculumbaseList.do";
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
        curriculumbaseService.updateCurriculumbase(searchVO);
        
        request.getSession().removeAttribute("sessionVO");
        
        return "forward:/mng/lms/crclb/CurriculumbaseList.do";
    }
    
    @RequestMapping("/mng/lms/crclb/deleteCurriculumbase.do")
    public String deleteCurriculumbase(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/crclb/CurriculumbaseList.do";
		}
    	
        curriculumbaseService.deleteCurriculumbase(searchVO);
        
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "forward:/mng/lms/crclb/CurriculumbaseList.do";
    }

}
