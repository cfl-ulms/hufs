package egovframework.com.mng.lms.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sun.star.io.IOException;
import com.sun.star.util.URL;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.lms.service.impl.ZipUtils;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

/**
 * 
 * @author ywkim 
 * CurseRegMngController.java 
 * CurseRegistManage 수강신청관리         
 */

@Controller
public class CurseRegMngController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovBBSCtgryService")
	private EgovBBSCtgryService egovBBSCtgryService;
	
	@Resource(name = "curriculumService")
    private CurriculumService curriculumService;
	
	@Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;
	
	/** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;
    
    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	//수강신청관리
	@RequestMapping(value = "/mng/lms/crm/curseregManage.do")
	public String curseregManage(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	Ctgry ctgry = new Ctgry();

    	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 안함
		searchVO.setProcessSttusCodeZeroAt("N");

        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);
        
        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        //오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

		return "/mng/lms/crm/curseregManage";
	}
	
	//수강신청관리 > 과정계획서
	@RequestMapping(value = "/mng/lms/crm/curriculumManage.do")
	public String selectCurriculumManage(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	//과정만족도
    	SurveyVO surveyVo = new SurveyVO();
 		surveyVo.setSchdulClCode("TYPE_1");
 		surveyVo.setRecordCountPerPage(1000);
 		surveyVo.setFirstIndex(0);
    	EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList", resultMap.get("resultList"));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
      	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
      	
      	request.getSession().setAttribute("sessionVO", searchVO);
      	
		return "/mng/lms/crm/curseregManageView";
	}
	
	
	//수강신청관리 > 과정계획서
	@RequestMapping(value = "/mng/lms/crm/curriculumRegister.do")
	public String selectCurriculumRegister(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));
    	
    	//학습내용
    	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));
    	
    	//과정학습참고자료 및 기본과정 조회
    	CurriculumbaseVO curriculumbaseVO = new CurriculumbaseVO();
    	curriculumbaseVO.setCrclbId(searchVO.getCrclbId());
    	model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(curriculumbaseVO));
    	model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(curriculumbaseVO));
    	
    	//성적기준
    	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//교재 및 부교재
    	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));

    	model.addAttribute("curriculumVO", curriculumVO);

      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumRegisterView";
	}
	
	//수강신청관리 > 수강신청승인
	@RequestMapping(value = "/mng/lms/crm/curriculumAccept.do")
	public String selectCurriculumAccept(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//수강신청 회원 리스트 조회
		List<?> curriculumMemberList = curriculumMemberService.selectCurriculumMemberList(searchVO);
		model.addAttribute("curriculumMemberList", curriculumMemberList);
		
		//수강신청 회원 총개수
		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
		model.addAttribute("totCnt", totCnt);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumAcceptView";
	}
	
	//수강신청관리 > 수강신청승인에서 승인상태 수정
	@RequestMapping(value = "/mng/lms/crm/updateCurriculumAccept.do")
	public String updateCurriculumAccept(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String retStr = "forward:/mng/lms/crm/curriculumAccept.do";
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
    	curriculumMemberService.updateCurriculumAccept(searchVO);
        
        request.getSession().removeAttribute("sessionVO");
        
        return retStr;
	}
	
	//수강신청관리 > 과정계획서 수강신청 조기마감
	@RequestMapping(value = "/mng/lms/crm/updateProcessSttusCode.do")
	public String updateProcessSttusCode(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		String retStr = "redirect:" + request.getParameter("forwardUrl");
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
    	curriculumService.updateCurriculum(searchVO);
        status.setComplete();
        
        request.getSession().removeAttribute("sessionVO");
        
        return retStr;
	}
	
	//수강신청관리 > 수강대상자 확정 > 전체명단
	@RequestMapping(value = "/mng/lms/crm/curriculumStudent.do")
	public String selectCurriculumStudent(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//수강대상자 통계 조회
    	List<?> curriculumMemberStatsList = curriculumMemberService.curriculumMemberStatsList(searchVO);
		model.addAttribute("curriculumMemberStatsList", curriculumMemberStatsList);

		//수강대상자 총 합계
		searchVO.setSearchSttus("1");
		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
		model.addAttribute("totCnt", totCnt);
		
    	//수강 대상자 조회
		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
		model.addAttribute("selectStudentList", selectStudentList);
		
      	request.getSession().setAttribute("sessionVO", searchVO);
      	
      	String retUrl = "/mng/lms/crm/curriculumStudentView";
      	if("Y".equals(searchVO.getExcelAt())){
      		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
      	  	model.addAttribute("siteInfo", siteVO);
      	  	
      		retUrl = "/lms/crcl/curriculumStudentViewExcel";
      	}
      	
      	if("Y".equals(searchVO.getZipFileAt())){
      		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
      		model.addAttribute("siteInfo", siteVO);
      		
      		retUrl = "forward:/lms/crm/downZipFile.do";
      	}
      	
		return retUrl;
	}
	
	
	

	@RequestMapping("/mng/lms/crm/selectMemberGroupCnt.json")
	public void selectMemberGroupCnt(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception{
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");
		
		List<?> selectMemberGroupCnt = curriculumMemberService.selectMemberGroupCnt(searchVO);
		
		jo.put("selectMemberGroupCnt", selectMemberGroupCnt);
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}

	//수강신청관리 > 수강대상자 확정 > 조배정 뷰 페이지
	@RequestMapping(value = "/mng/lms/crm/curriculumGroupView.do")
	public String selectCurriculumGroupView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
		
    	//수강 그룹 조회(반,조)
		List<?> selectGroupList = curriculumMemberService.selectGroupList(searchVO);
		model.addAttribute("selectGroupList", selectGroupList);

		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);
		
		//조의 개수 리스트를 조회
		List<?>  groupCntList = curriculumMemberService.selectCurriculumGroupCntList(searchVO);
		model.addAttribute("groupCntList", groupCntList);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumGroupView";
	}

	//수강신청관리 > 수강대상자 확정 > 조배정 수정 페이지
	@RequestMapping(value = "/mng/lms/crm/curriculumGroupRegister.do")
	public String selectCurriculumGroupRegister(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
		
    	//수강 그룹 조회(반,조)
		List<?> selectGroupList = curriculumMemberService.selectGroupList(searchVO);
		model.addAttribute("selectGroupList", selectGroupList);
    	
    	//조의 개수 리스트를 조회
		List<?>  groupCntList = curriculumMemberService.selectCurriculumGroupCntList(searchVO);
		model.addAttribute("groupCntList", groupCntList);
		
		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);
		
		//수강 대상자 조회
		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
		model.addAttribute("selectStudentList", selectStudentList);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumGroupRegister";
	}
	
	//수강신청관리 > 수강대상자 확정 > 조배정 확정
	@RequestMapping(value = "/mng/lms/crm/updateCurriculumGroup.do")
	public String updateCurriculumGroup(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		String retStr = "forward:/mng/lms/crm/curriculumGroupView.do";
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
    	curriculumMemberService.updateCurriculumGroup(searchVO);
    	status.setComplete();
        
        request.getSession().removeAttribute("sessionVO");
        
        return retStr;
	}

	//수강신청관리 > 수강대상자 확정 > 반배정 뷰 페이지
	@RequestMapping(value = "/mng/lms/crm/curriculumClassView.do")
	public String selectCurriculumClassView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//반 정보 조회
		List<?> selectCurriculumClassList = curriculumMemberService.selectCurriculumClassList(searchVO);
		model.addAttribute("curriculumClassList", selectCurriculumClassList);

		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);

      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumClassView";
	}

	//수강신청관리 > 수강대상자 확정 > 반배정 수정페이지
	@RequestMapping(value = "/mng/lms/crm/curriculumClassRegister.do")
	public String selectCurriculumClassRegister(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(searchVO));

    	//반 정보 조회
		List<?> selectCurriculumClassList = curriculumMemberService.selectCurriculumClassList(searchVO);
		model.addAttribute("curriculumClassList", selectCurriculumClassList);

    	//반 개수 조회
		int classCnt = curriculumMemberService.selectCurriculumClassTotCnt(searchVO);
		model.addAttribute("classCnt", classCnt);
		
		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curriculumClassRegister";
	}

	//수강신청관리 > 수강대상자 확정 > 반배정 > 학생조회
	@RequestMapping(value ="/mng/lms/crm/studentJson.json")
	public void selectCurriculumClass(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String successYn = "Y";
	    
	    JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("items", curriculumMemberService.selectStudentList(searchVO));
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//수강신청관리 > 수강대상자 확정 > 반배정 확정
	@RequestMapping(value = "/mng/lms/crm/updateCurriculumClass.do")
	public String updateCurriculumClass(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		String retStr = "forward:/mng/lms/crm/curriculumClassView.do";
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}
    	
    	curriculumMemberService.updateCurriculumClass(searchVO);
    	status.setComplete();
        
        request.getSession().removeAttribute("sessionVO");
        
        return retStr;
	}

	//수강신청 승인/취소 내역
	@RequestMapping(value = "/mng/lms/crm/curseregSttusList.do")
	public String curseregManageList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		Ctgry ctgry = new Ctgry();

		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
    	//년도
    	model.addAttribute("yearList", new LmsCommonUtil().getYear());
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 안함
		searchVO.setProcessSttusCodeZeroAt("N");

    	List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);
        
        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

		return "/mng/lms/crm/curseregSttusList";
	}
	
	//수강신청 승인/취소 내역 상세페이지
	@RequestMapping(value = "/mng/lms/crm/curseregSttusView.do")
	public String curseregManageView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//교육과정 수강료 납부 확인/환불 리스트 조회
		List<?> curriculumMemberList = curriculumMemberService.selectCurriculumMemberList(searchVO);
		model.addAttribute("curriculumMemberList", curriculumMemberList);
		
		//교육과정 수강료 납부 확인/환불 총개수
		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
		model.addAttribute("totCnt", totCnt);

		//학생 상태별 개수 조회
		List<?> memberSttusCntList = curriculumMemberService.selectMemberSttusCntList(searchVO);
        model.addAttribute("memberSttusCntList", memberSttusCntList);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

		return "/mng/lms/crm/curseregSttusView";
	}

	//수강대상자 확정 승인
	@RequestMapping(value = "/mng/lms/crm/curseregManageConfirm.do")
	public String curseregManageConfirm(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
		Ctgry ctgry = new Ctgry();

		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
    	//년도
    	model.addAttribute("yearList", new LmsCommonUtil().getYear());
    	
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 안함
		searchVO.setProcessSttusCodeZeroAt("N");

    	List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);
        
        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

		return "/mng/lms/crm/curseregManageConfirm";
	}
	
	//취소/환불 규정 뷰페이지
	@RequestMapping(value = "/mng/lms/crm/curseregManageCancelRule.do")
	public String curseregManageCancelRule(@ModelAttribute("searchVO") Ctgry searchVO, ModelMap model) throws Exception {
		return "/mng/lms/crm/curseregManageCancelRule";
	}
	
	//취소/환불 규정 수정페이지
	@RequestMapping(value = "/mng/lms/crm/curseregManageCancelRuleReg.do")
	public String curseregManageCancelRuleReg(@ModelAttribute("searchVO") Ctgry searchVO, ModelMap model) throws Exception {
		return "/mng/lms/crm/curseregManageCancelRuleReg";
	}

	//학생 반, 조 개수 리스트
	@RequestMapping(value = "/lms/crm/selectMemberCntList.json")
	public void selectMemberCntList(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<?> selectMemberCntList = curriculumMemberService.selectMemberCntList(searchVO);
		
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");

		jo.put("memberCntList", selectMemberCntList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//수강신청 > 선택된 학생 일괄 승인
	@RequestMapping(value = "/ajax/mng/lms/crm/updateSttusCurriculumMember.json")
	public void updateSttusCurriculumMember(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		EgovMap egovMap = new EgovMap();
		egovMap.put("paramUserId", request.getParameter("paramUserId"));
		egovMap.put("crclId", request.getParameter("crclId"));
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user != null){
			egovMap.put("lastUpdusrId", user.getId());
		}
		
		curriculumMemberService.updateSttusCurriculumMember(egovMap);
		
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");
		
		jo.put("result", "Y");
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//수강신청 > 선택된 학생 삭제
	@RequestMapping(value = "/ajax/mng/lms/crm/delCurriculumMember.json")
	public void delCurriculumMember(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		EgovMap egovMap = new EgovMap();
		egovMap.put("paramUserId", request.getParameter("paramUserId"));
		egovMap.put("crclId", request.getParameter("crclId"));
		
		curriculumMemberService.delCurriculumMember(egovMap);
		
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");
		
		jo.put("result", "Y");
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	//수강신청 등록
    @RequestMapping("/mng/lms/crm/insertCurriculumMember.do")
    public String insertCurriculumMember(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, final MultipartHttpServletRequest multiRequest) throws Exception {
    	List<FileVO> result = null;
	    String arrFileExt[] = {"ppt", "pptx", "doc", "docx", "xls", "xlsx", "hwp", "pdf", ""}; //기타파일은 필수가 아니기 떄문에 공백값도 있음.
	    String aplyFileExt  = "";
	    String planFileExt  = "";
	    String etfFileExt   = "";
	    String retUrl = "forward:/mng/usr/EgovStuMberManage.do";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    		searchVO.setSttus("1");
    	}else{
    		return "forward:/index.do";
    	}

    	//파일 업로드
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty() && "Y".equals(searchVO.getStdntAplyAt())) {
	    	result = fileUtil.directParseFileInf(files, "CURRICULUMALL_", 0, "CurriculumAll.fileStorePath", "");

	    	if(result != null) {
		    	for(int index=0; index < result.size(); index++) {
		    		FileVO file = result.get(index);
		    		if(file.getFormNm().startsWith("aplyFile")) {
		    			searchVO.setAplyFile(file.getStreFileNm());
		    			searchVO.setAplyOriFile(file.getOrignlFileNm());
		    			aplyFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("planFile")) {
		    			searchVO.setPlanFile(file.getStreFileNm());
		    			searchVO.setPlanOriFile(file.getOrignlFileNm());
		    			planFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("etfFile")) {
		    			searchVO.setEtfFile(file.getStreFileNm());
		    			searchVO.setEtfOriFile(file.getOrignlFileNm());
		    			etfFileExt = file.getFileExtsn();
		    		}
		    	}
	    	}
		    
	    	//확장자 체크
	    	if(!Arrays.asList(arrFileExt).contains(aplyFileExt)
	        || !Arrays.asList(arrFileExt).contains(planFileExt)
	        || !Arrays.asList(arrFileExt).contains(etfFileExt)) {
	    		model.addAttribute("requestMessage", "첨부하신 파일 확장자는 불가능합니다.");
	    		return retUrl;
	    	}
	    	
	    	curriculumService.insertCurriculumMember(searchVO);
	    } else {
	    	curriculumService.insertCurriculumMember(searchVO);
	    }
	    
	    model.addAttribute("requestMessage", "수강 신청이 완료되었습니다.");
	    
	    //학생 등록으로 반환
		return retUrl;
    }
}