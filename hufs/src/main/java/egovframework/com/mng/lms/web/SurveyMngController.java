package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyQuestionExVO;
import egovframework.com.lms.service.SurveyQuestionVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.string.EgovStringUtil;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class SurveyMngController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;

	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;

	@RequestMapping(value="/mng/lms/sur/surveyManageList.do")
    public String surveyManageList(@ModelAttribute("surveyVo") SurveyVO surveyVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

		/** EgovPropertyService.sample */
    	surveyVo.setPageUnit(propertyService.getInt("pageUnit"));
    	surveyVo.setPageSize(propertyService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(surveyVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(surveyVo.getPageUnit());
		paginationInfo.setPageSize(surveyVo.getPageSize());

		surveyVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		surveyVo.setLastIndex(paginationInfo.getLastRecordIndex());
		surveyVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		EgovMap resultMap = this.surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		model.addAttribute("surveyListCnt", resultMap.get("resultCnt"));
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
        model.addAttribute("paginationInfo", paginationInfo);
        return "/mng/lms/sur/SurveyManageList";
    }

	@RequestMapping(value="/mng/lms/sur/surveyManage.do")
    public String surveyManage(@ModelAttribute("surveyVo") SurveyVO surveyVo, @RequestParam String actionKey,HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{

		String url = "/mng/lms/sur/SurveyManage";
		
		if("update".equals(actionKey)){
			SurveyVO surveyInfo = surveyManageService.selectSurveyInfo(surveyVo);
			for(SurveyQuestionVO queVo : surveyInfo.getQuestionsArray()){
				List<SurveyQuestionExVO> widthExVoList = new ArrayList<SurveyQuestionExVO>();
				List<SurveyQuestionExVO> heigthExVoList = new ArrayList<SurveyQuestionExVO>();

				if("table".equals(queVo.getQesitmTyCode())){
					for(SurveyQuestionExVO tempEx : queVo.getExamples()){
						if("W".equals(tempEx.getExTy())){
							widthExVoList.add(tempEx);
						}else{
							heigthExVoList.add(tempEx);
						}
					}
					queVo.setWidthExamples(widthExVoList);
					queVo.setHeightExamples(heigthExVoList);
				}
			}
			model.addAttribute("surveyInfo", surveyInfo);
		}
		
		if("view".equals(actionKey)){
			SurveyVO surveyInfo = surveyManageService.selectSurveyInfo(surveyVo);
			for(SurveyQuestionVO queVo : surveyInfo.getQuestionsArray()){
				List<SurveyQuestionExVO> widthExVoList = new ArrayList<SurveyQuestionExVO>();
				List<SurveyQuestionExVO> heigthExVoList = new ArrayList<SurveyQuestionExVO>();
				
				if("table".equals(queVo.getQesitmTyCode())){
					for(SurveyQuestionExVO tempEx : queVo.getExamples()){
						if("W".equals(tempEx.getExTy())){
							widthExVoList.add(tempEx);
						}else{
							heigthExVoList.add(tempEx);
						}
					}
					queVo.setWidthExamples(widthExVoList);
					queVo.setHeightExamples(heigthExVoList);
				}
			}
			model.addAttribute("surveyInfo", surveyInfo);
			url = "/mng/lms/sur/SurveyManageView";
		}

		model.addAttribute("actionKey", actionKey);
        return url;
    }

	@ResponseBody
	@RequestMapping(value="/mng/lms/sur/insertSurvey.json")
    public void academicSystemRegist(@RequestParam Map<String, Object> parameters, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SurveyVO surVo = new SurveyVO();
		int result = 0;
		String questionsArray =  (String) parameters.get("questionsArray");
		String action =  (String) parameters.get("action");
		String tempSchdulId = (String) parameters.get("schdulId");

		LoginVO loginVO = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(!EgovStringUtil.isEmpty(loginVO.getId())) {
			surVo.setLastUpdusrId(loginVO.getId());
		}

		surVo.setSchdulNm((String) parameters.get("schdulNm"));
		surVo.setSchdulClCode((String) parameters.get("schdulClCode"));
		ObjectMapper mapper = new ObjectMapper();
		surVo.setQuestionsArray((List<SurveyQuestionVO>) mapper.readValue(questionsArray, new TypeReference<ArrayList<SurveyQuestionVO>>(){}));

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");


		try {
			if("update".equals(action)){
				surVo.setSchdulId(tempSchdulId);
				this.surveyManageService.deleteSurvey(surVo);
			}

			this.surveyManageService.insertSurvey(surVo);
			jo.put("resultCode", "Ex001");


		} catch (Exception e) {
			e.printStackTrace();
			jo.put("resultCode", "Ex009");
		}


		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
	
	@RequestMapping(value="/mng/lms/sur/deleteSurvey.json")
    public void deleteSurvey(@ModelAttribute("surveyVo") SurveyVO surveyVo, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject jo = new JSONObject();
		
		try {
			
	  		response.setContentType("application/json;charset=utf-8");
			this.surveyManageService.deleteSurvey(surveyVo);

			jo.put("resultCode", "Ex001");

		} catch (Exception e) {
			e.printStackTrace();
			jo.put("resultCode", "Ex009");
		}


		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

}
