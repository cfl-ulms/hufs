package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.mysql.fabric.xmlrpc.base.Array;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : CurriculumController.java
 * @Description : Curriculum Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class LmsTemplateMngController {

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    //과정관리
    @RequestMapping(value="/mng/lms/tabmenu.do")
    public String lmsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        return "/mng/lms/template/tabmenu";
    } 
    
    //수강신청
    @RequestMapping(value="/mng/lms/curseregtabmenu.do")
    public String curseregtabmenu(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        return "/mng/lms/template/curseregtabmenu";
    }
    
    //수강신청 > 수강대상자 확정
    @RequestMapping(value="/mng/lms/curriculumStudenttabmenu.do")
    public String curriculumStudenttabmenu(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
        return "/mng/lms/template/curriculumStudenttabmenu";
    }
}
