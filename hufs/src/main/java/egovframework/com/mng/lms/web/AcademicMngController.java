package egovframework.com.mng.lms.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.msi.service.ContentsServiceVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.ion.bnr.service.BannerVO;
import egovframework.com.uss.ion.bnr.service.EgovBannerService;
import egovframework.com.uss.ion.pwm.service.EgovPopupManageService;
import egovframework.com.uss.ion.pwm.service.PopupManageVO;
import egovframework.com.uss.ion.sit.service.EgovLinkSiteManageService;
import egovframework.com.uss.ion.sit.service.LinkSiteManageVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.evt.service.ComtnschdulinfoService;
import egovframework.com.evt.service.ComtnschdulinfoVO;

@Controller
public class AcademicMngController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;
	
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
	
	@RequestMapping(value="/mng/lms/academicSystem.do")
    public String academicSystem(@ModelAttribute("searchVO") Ctgry searchVO, ModelMap model) throws Exception {
		
		//과정체계관리 마스터코드
		searchVO.setCtgrymasterId("CTGMST_0000000000004");
        model.addAttribute("resultList", egovBBSCtgryService.selectComtnbbsctgryList(searchVO));
        
        return "/mng/lms/academicSystem";
    } 
	
	@RequestMapping(value="/mng/lms/academicSystemRegist.do")
    public String academicSystemRegist(@ModelAttribute("searchVO") Ctgry searchVO, ModelMap model, HttpServletRequest request) throws Exception {
		
		model.addAttribute("ctgryList", egovBBSCtgryService.selectComtnbbsctgryList(searchVO));
    	model.addAttribute("ctgry", searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/lms/academicSystemRegist";
    } 
	
	@RequestMapping("/mng/lms/insertAcademicSystem.do")
    public String insertAcademicSystem(Ctgry ctgry, @ModelAttribute("searchVO") Ctgry searchVO, HttpServletRequest request) throws Exception {
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/academicSystem.do";
		}
    	
    	egovBBSCtgryService.insertComtnbbsctgry(ctgry);
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/lms/academicSystem.do";
    }
    
    @RequestMapping("/mng/lms/updateAcademicSystem.do")
    public String updateAcademicSystem(@ModelAttribute("searchVO") Ctgry searchVO, Model model, HttpServletRequest request) throws Exception {
    	
    	model.addAttribute("ctgryList", egovBBSCtgryService.selectComtnbbsctgryList(searchVO));
    	model.addAttribute("ctgry", egovBBSCtgryService.selectComtnbbsctgry(searchVO));
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "/mng/lms/academicSystemRegist";
    }

    @RequestMapping("/mng/lms/updateAcademic.do")
    public String updateAcademic(Ctgry ctgry, @ModelAttribute("searchVO") Ctgry searchVO, HttpServletRequest request) throws Exception {
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/lms/academicSystem.do";
		}
    	
    	egovBBSCtgryService.updateComtnbbsctgry(ctgry);
    	request.getSession().removeAttribute("sessionVO");
    	
        return "forward:/mng/lms/academicSystem.do";
    }
    
    @RequestMapping("/mng/lms/deleteAcademicSystem.do")
    public String deleteAcademicSystem(Ctgry comtnbbsctgryVO, @ModelAttribute("searchVO") Ctgry searchVO) throws Exception {
    	egovBBSCtgryService.deleteComtnbbsctgry(comtnbbsctgryVO);
        return "forward:/mng/lms/academicSystem.do";
    }
	
}
