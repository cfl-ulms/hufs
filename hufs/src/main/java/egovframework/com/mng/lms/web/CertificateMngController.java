package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CertificateService;
import egovframework.com.lms.service.CertificateVO;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;


@Controller
public class CertificateMngController {

	 @Resource(name = "curriculumService")
	 private CurriculumService curriculumService;
	 
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
    
    @Resource(name = "certificateService")
    private CertificateService certificateService;
    
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    //수료증 발급조회
    @RequestMapping("/mng/lms/selectCertificate.do")
    public String selectCertificate(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		
		if(EgovStringUtil.isEmpty(searchVO.getSearchCondition())) {
			//유효 수료증만 검색
			//searchVO.setUseAt("Y");
			searchVO.setSearchCondition("Y");
		}
		
    	List<EgovMap> resultList = certificateService.selectCertificateList(searchVO);
    	model.addAttribute("resultList", resultList);
    	
    	int totCnt = certificateService.selectCertificateListCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
    	
        
        //년도
  		model.addAttribute("yearList", new LmsCommonUtil().getYear());
  		
  		Ctgry ctgry = new Ctgry();
  		//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
    	return "/mng/lms/certificate/certificateList";
    }
    
}
