package egovframework.com.mng.lms.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.BasefileService;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : BasefileController.java
 * @Description : Basefile Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
public class BasefileController {

    @Resource(name = "basefileService")
    private BasefileService basefileService;
    
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;
    
    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
	
    /**
	 * basefile 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 BasefileDefaultVO
	 * @return "/mng/lms/basefile/BasefileList"
	 * @exception Exception
	 */
    @RequestMapping(value="/mng/lms/basefile/BasefileList.do")
    public String selectBasefileList(@ModelAttribute("searchVO") BasefileVO searchVO, ModelMap model, HttpServletRequest request) throws Exception {
    	
        List<?> basefileList = basefileService.selectBasefileList(searchVO);
        model.addAttribute("resultList", basefileList);
        
        return "/mng/lms/basefile/BasefileList";
    } 
    
    @RequestMapping("/mng/lms/basefile/addBasefileView.do")
    public String addBasefileView(@ModelAttribute("searchVO") BasefileVO searchVO, Model model, HttpServletRequest request)throws Exception {
    	
    	//서비스메뉴
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000019");
		model.addAttribute("menuList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
        return "/mng/lms/basefile/BasefileRegister";
    }
    
    @RequestMapping("/mng/lms/basefile/addBasefile.do")
    public String addBasefile(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BasefileVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/cop/bbs/ctg/selectBBSCtgryList.do";
		}
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    	}else{
    		return "forward:/mng/lms/basefile/BasefileList.do";
    	}
    	
    	String atchFileId = "";
	    List<FileVO> result = null;
	      
	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	        result = fileUtil.parseBaseFileInf(1024 * 1024 * 1024, files, 0, "", "","","");
	        atchFileId = fileMngService.insertFileInfs(result);
	    }
	      
	    searchVO.setAtchFileId(atchFileId);
    	
        basefileService.insertBasefile(searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        
        return "forward:/mng/lms/basefile/BasefileList.do";
    }
    
    @RequestMapping("/mng/lms/basefile/updateBasefileView.do")
    public String updateBasefileView(@ModelAttribute("searchVO") BasefileVO searchVO, Model model, HttpServletRequest request) throws Exception {
        model.addAttribute("result", basefileService.selectBasefile(searchVO));
        return "/mng/lms/basefile/BasefileRegister";
    }

    @RequestMapping("/mng/lms/basefile/selectBasefile.do")
    public String selectBasefile(@ModelAttribute("searchVO") BasefileVO searchVO, Model model) throws Exception {
    	model.addAttribute("result", basefileService.selectBasefile(searchVO));
        return "/mng/lms/basefile/BasefileRegister";
    }

    @RequestMapping("/mng/lms/basefile/updateBasefile.do")
    public String updateBasefile(@ModelAttribute("searchVO") BasefileVO searchVO, HttpServletRequest request)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/cop/bbs/ctg/selectBBSCtgryList.do";
		}
    	
        basefileService.updateBasefile(searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        return "forward:/mng/lms/basefile/BasefileList.do";
    }
    
    @RequestMapping("/mng/lms/basefile/deleteBasefile.do")
    public String deleteBasefile(@ModelAttribute("searchVO") BasefileVO searchVO, HttpServletRequest request)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/mng/cop/bbs/ctg/selectBBSCtgryList.do";
		}
    	
        basefileService.deleteBasefile(searchVO);
        request.getSession().setAttribute("sessionVO", searchVO);
        return "forward:/mng/lms/basefile/BasefileList.do";
    }

}
