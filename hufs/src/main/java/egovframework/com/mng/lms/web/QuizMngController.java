package egovframework.com.mng.lms.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.QuizService;
import egovframework.com.lms.service.QuizVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

@Controller
public class QuizMngController {
	
	@Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

	@Resource(name = "quizService")
	private QuizService quizService;
	
	@Resource(name = "EgovFileMngUtil")
 	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
     
    @Resource(name = "EgovBBSAttributeManageService")
    private EgovBBSAttributeManageService bbsAttrbService;
    
    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
	
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
	//퀴즈 평가결과조회
	@RequestMapping(value="/mng/lms/quiz/QuiResultList.do")
    public String selectQuiResultList(@ModelAttribute("searchVO") QuizVO searchVO, ScheduleMngVO scheduleMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String retVal = "/lms/manage/quizResult";
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		
		List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);
    	
        List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(searchVO);
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        
        if(!"08".equals(user.getUserSe())){
        	//전체 성적 지표
        	EgovMap summary = quizService.quizExamSummary(searchVO);
        	model.addAttribute("summary", summary);
        	
        	searchVO.setSearchUserId(user.getId());
        	retVal = "/lms/manage/quizResultStu";
        }
        List<QuizVO> quizListResult = quizService.selectQuizResult_Report(searchVO);
		model.addAttribute("quizResult", quizListResult);
		
        model.addAttribute("search2", searchVO);
        return retVal;
    }
	
	@RequestMapping(value="/mng/lms/quiz/QuizExamList.do")
    public String selectQuizExamList(@ModelAttribute("searchVO") QuizVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		
		model.addAttribute("USER_INFO", user);
		
		List<QuizVO> beforeQuizList = null;
		try {
			beforeQuizList = quizService.selectBeforeQuizList(searchVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
        try {
			model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        CurriculumVO curriculumVO = new CurriculumVO();
        curriculumVO.setSearchSchAt("Y");
        List<?> curriculumList = null;
		try {
			curriculumList = curriculumService.selectCurriculumList(curriculumVO, request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("resultList", curriculumList);
        
        /** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		paginationInfo.setTotalRecordCount(beforeQuizList.size());
        model.addAttribute("paginationInfo", paginationInfo);
        
		return "/mng/lms/manage/quizExamList";
	}
	
}