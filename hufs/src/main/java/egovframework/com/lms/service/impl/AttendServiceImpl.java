package egovframework.com.lms.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.AttendService;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.BasefileService;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.lms.service.impl.BasefileDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : BasefileServiceImpl.java
 * @Description : Basefile Business Implement class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("attendService")
public class AttendServiceImpl extends EgovAbstractServiceImpl implements AttendService {
        
    @Resource(name="attendDAO")
    private AttendDAO attendDAO;
    
	
    //출석수정
    public void updateAttention(AttendVO vo) throws Exception {
    	if(vo.getUserIdList().size() != 0 && vo.getAttentionTypeList().size() != 0){
    		//초기화
    		attendDAO.deleteAttention(vo);
    		
    		EgovMap paramMap = new EgovMap();
    		List<EgovMap> atdList = new ArrayList<EgovMap>();
    		for(int i = 0; i < vo.getUserIdList().size(); i++){
    			EgovMap atdMap = new EgovMap();

    			atdMap.put("plId", vo.getPlId());
    			atdMap.put("userId", vo.getUserIdList().get(i));
    			atdMap.put("attentionType", vo.getAttentionTypeList().get(i));
    			atdMap.put("frstRegisterId", vo.getFrstRegisterId());
    			
    			atdList.add(atdMap);
        	}
        	paramMap.put("atdList", atdList);
        	
        	attendDAO.insertAllAttention(paramMap);
    	}
    }
    
    //출석목록
    public List selectAttendList(AttendVO vo) throws Exception {
    	return attendDAO.selectAttendList(vo);
    }
    
    //과정별 학생 출석 목록(성적)
    public List studentAttendList(AttendVO vo) throws Exception {
    	return attendDAO.studentAttendList(vo);
    }
}
