package egovframework.com.lms.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.SearchVO;
import egovframework.com.cmm.service.impl.FileManageDAO;
import egovframework.com.lms.service.ClassManageService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumbaseService.java
 * @Description : Curriculumbase Business class
 * @Modification Information
 *
 * @author 이정현
 * @since 2019.11.17
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
@Service("classManageService")
public class ClassManageServiceImpl extends EgovAbstractServiceImpl implements ClassManageService {

	/** ID Generation */
    @Resource(name="surveyIdGnrService")
    private EgovIdGnrService surveyIdGnrService;

    @Resource(name="classManageDAO")
    private ClassManageDAO classManageDAO;

    @Resource(name = "FileManageDAO")
    private FileManageDAO fileMngDAO;

    private List<String> FILEEXTS_IMG = Arrays.asList("jpg", "jpeg", "png", "gif", "peg", "bmp");
	private List<String> FILEEXTS_MOV = Arrays.asList("mp4", "avi", "mov", "wmv", "asf", "asx", "mpg", "mpeg", "mpe", "flv", "rm", "mkv");
	private List<String> FILEEXTS_OTHER = Arrays.asList("xlsx", "pptx", "docx", "txt", "doc", "zip", "xls", "ppt", "pdf", "hwp");

	public List selectClassCurriculumList(FileVO fileVo) throws Exception {
		return this.classManageDAO.selectClassCurriculumList(fileVo);
	}

	public EgovMap selectFileListMap(FileVO fileVo, HttpServletRequest request) throws Exception {
		EgovMap resultMap = new EgovMap();

		fileVo.setSearchFileExtsn(selectFileExtnList(request));
		List<EgovMap> fileList = this.classManageDAO.selectClassFileList(fileVo);
		int fileListCnt = this.classManageDAO.selectClassFileListCnt(fileVo);

		if(fileList != null){
			fileList = this.setFileList(fileList);
		}

		resultMap.put("resultList", fileList);
		resultMap.put("resultCnt", fileListCnt);
		return resultMap;
	}

	public EgovMap selectTeacherFileListMap(FileVO fileVo, HttpServletRequest request) throws Exception {
		EgovMap resultMap = new EgovMap();

		fileVo.setSearchFileExtsn(selectFileExtnList(request));
		if("Y".equals(fileVo.getAddAt())){
			fileVo.setRecordCountPerPage(99999);
		}
		List<EgovMap> fileList = this.classManageDAO.selectTeacherClassFileList(fileVo);
		int fileListCnt = this.classManageDAO.selectTeacherClassFileListCnt(fileVo);

		if(fileList != null){
			fileList = this.setFileList(fileList);
		}

		resultMap.put("resultList", fileList);
		resultMap.put("resultCnt", fileListCnt);
		return resultMap;
	}

	public EgovMap selectStudentFileListMap(FileVO fileVo, HttpServletRequest request) throws Exception {
		EgovMap resultMap = new EgovMap();

		fileVo.setSearchFileExtsn(selectFileExtnList(request));
		List<EgovMap> fileList = this.classManageDAO.selectStudentClassFileList(fileVo);
		int fileListCnt = this.classManageDAO.selectStudentClassFileListCnt(fileVo);

		if(fileList != null){
			fileList = this.setFileList(fileList);
		}

		resultMap.put("resultList", fileList);
		resultMap.put("resultCnt", fileListCnt);
		return resultMap;
	}

	public List selectSearchStudyFileList(FileVO fileVo,HttpServletRequest request) throws Exception {
		fileVo.setSearchFileExtsn(selectFileExtnList(request));
		List<EgovMap> fileList = this.classManageDAO.selectSearchStudyFileList(fileVo);
		if(fileList != null){
			fileList = this.setFileList(fileList);
		}
		return fileList;
	}

	private List<String> selectFileExtnList(HttpServletRequest request) throws Exception {
		String extOth = request.getParameter("fileExtOther");
		String extImg = request.getParameter("fileExtImg");
		String extMov = request.getParameter("fileExtMov");
		List<String> searchFileExtsn = new ArrayList<String>();

		if(extOth != null){
			searchFileExtsn.addAll(FILEEXTS_OTHER);
		}
		if(extImg != null){
			searchFileExtsn.addAll(FILEEXTS_IMG);
		}
		if(extMov != null){
			searchFileExtsn.addAll(FILEEXTS_MOV);
		}

		return searchFileExtsn;
	}

	public FileVO selectFileInfo(FileVO fileVo) throws Exception {
		FileVO fvo = fileMngDAO.selectFileInf(fileVo);
		if(FILEEXTS_IMG.indexOf(fvo.getFileExtsn()) != -1){
			fvo.setFileExtNm("이미지");
		}else if(FILEEXTS_MOV.indexOf(fvo.getFileExtsn()) != -1){
			fvo.setFileExtNm("동영상");
		}else{
			fvo.setFileExtNm("파일");
		}
		return fvo;
	}


	public EgovMap selectCurriculumStudyMap(SearchVO searchVo) throws Exception {
		EgovMap resultMap = new EgovMap();

		List<EgovMap> resultList = this.classManageDAO.selectCurriculumStudyList(searchVo);
		int resultCnt = this.classManageDAO.selectCurriculumStudyListCnt(searchVo);

		resultMap.put("resultList", resultList);
		resultMap.put("resultCnt", resultCnt);
		return resultMap;
	}

	public List selectCurriculumList(SearchVO searchVo) throws Exception {
		return this.classManageDAO.selectCurriculumList(searchVo);
	}

	public List selectCurriculumBaseList(SearchVO searchVo) throws Exception {
		return this.classManageDAO.selectCurriculumBaseList(searchVo);
	}

	public EgovMap selectCurriculumBoardMap(SearchVO searchVo) throws Exception {
		EgovMap resultMap = new EgovMap();

		List<EgovMap> resultList = this.classManageDAO.selectCurriculumBoardList(searchVo);
		int resultCnt = this.classManageDAO.selectCurriculumBoardListCnt(searchVo);

		resultMap.put("resultList", resultList);
		resultMap.put("resultCnt", resultCnt);
		return resultMap;
	}

	public EgovMap selectSurveyMap(SearchVO searchVo) throws Exception {
		EgovMap resultMap = new EgovMap();

		List<EgovMap> resultList = this.classManageDAO.selectSurveyList(searchVo);
		int resultCnt = this.classManageDAO.selectSurveyListCnt(searchVo);

		resultMap.put("resultList", resultList);
		resultMap.put("resultCnt", resultCnt);
		return resultMap;
	}

	public EgovMap selectCurriculumUserInfo(EgovMap map) throws Exception {
		return this.classManageDAO.selectCurriculumUserInfo(map);
	}

	public List setFileList(List<EgovMap> fileList) throws Exception {
		for(EgovMap tempVo : fileList){
			if(FILEEXTS_IMG.indexOf(tempVo.get("fileExtsn").toString()) != -1){
				tempVo.put("fileExtNm", "이미지");
			}else if(FILEEXTS_MOV.indexOf(tempVo.get("fileExtsn").toString()) != -1){
				tempVo.put("fileExtNm", "동영상");
			}else{
				tempVo.put("fileExtNm", "파일");
			}
		}
		return fileList;
	}

}
