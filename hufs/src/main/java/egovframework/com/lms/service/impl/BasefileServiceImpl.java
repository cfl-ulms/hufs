package egovframework.com.lms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.com.lms.service.BasefileService;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.lms.service.impl.BasefileDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : BasefileServiceImpl.java
 * @Description : Basefile Business Implement class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("basefileService")
public class BasefileServiceImpl extends EgovAbstractServiceImpl implements
        BasefileService {
        
    @Resource(name="basefileDAO")
    private BasefileDAO basefileDAO;
    
	/**
	 * basefile을 등록한다.
	 * @param vo - 등록할 정보가 담긴 BasefileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertBasefile(BasefileVO vo) throws Exception {
    	String ctgryId = "";
    	for(int i = 0; i < vo.getCtgryIdList().size(); i++){
    		if(!EgovStringUtil.isEmpty(vo.getCtgryIdList().get(i))){
    			ctgryId = vo.getCtgryIdList().get(i);
    		}
    	}
    	vo.setCtgryId(ctgryId);
    	
    	basefileDAO.insertBasefile(vo);
        return null;
    }

    /**
	 * basefile을 수정한다.
	 * @param vo - 수정할 정보가 담긴 BasefileVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateBasefile(BasefileVO vo) throws Exception {
        basefileDAO.updateBasefile(vo);
    }

    /**
	 * basefile을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 BasefileVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteBasefile(BasefileVO vo) throws Exception {
        basefileDAO.deleteBasefile(vo);
    }

    /**
	 * basefile을 조회한다.
	 * @param vo - 조회할 정보가 담긴 BasefileVO
	 * @return 조회한 basefile
	 * @exception Exception
	 */
    public BasefileVO selectBasefile(BasefileVO vo) throws Exception {
        BasefileVO resultVO = basefileDAO.selectBasefile(vo);
        if (resultVO == null)
            throw processException("info.nodata.msg");
        return resultVO;
    }

    /**
	 * basefile 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return basefile 목록
	 * @exception Exception
	 */
    public List<?> selectBasefileList(BasefileVO searchVO) throws Exception {
        return basefileDAO.selectBasefileList(searchVO);
    }

    /**
	 * basefile 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return basefile 총 갯수
	 * @exception
	 */
    public int selectBasefileListTotCnt(BasefileVO searchVO) {
		return basefileDAO.selectBasefileListTotCnt(searchVO);
	}
    
}
