package egovframework.com.lms.service;

import java.util.List;

import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumMemberService.java
 * @Description : Curriculum Business class
 * @Modification Information
 *
 * @author 김용완
 * @since 2019.12.16
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public interface CurriculumMemberService {
	/**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 총 갯수
	 * @exception
	 */
    int selectCurriculumMemberTotCnt(CurriculumVO searchVO);

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 목록
	 * @exception Exception
	 */
    List selectCurriculumMemberList(CurriculumVO searchVO) throws Exception;

    /**
	 * curriculumMember을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    void updateCurriculumAccept(CurriculumVO vo) throws Exception;

    /**
	 * curriculumMember 통계 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 통계 목록
	 * @exception Exception
	 */
    List curriculumMemberStatsList(CurriculumVO searchVO) throws Exception;

    /**
	 * 수강 대상자 통계 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 통계 목록
	 * @exception Exception
	 */
    List selectStudentList(CurriculumVO searchVO) throws Exception;

    /**
	 * 수강 그룹 개수 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 개수
	 * @exception Exception
	 */
    List selectMemberGroupCnt(CurriculumVO searchVO) throws Exception;

    /**
	 * 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    List selectGroupList(CurriculumVO searchVO) throws Exception;

    /**
	 * 조배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    void updateCurriculumGroup(CurriculumVO searchVO) throws Exception;

    /**
	 * 선택한 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    List selectPickStudentList(CurriculumVO searchVO) throws Exception;

    /**
	 * 조의 개수 리스트를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculummember 총 갯수
	 * @exception
	 */
    List selectCurriculumGroupCntList(CurriculumVO searchVO);

    /**
	 * curriculumgroup을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 목록
	 * @exception
	 */
    List selectCurriculumClassList(CurriculumVO searchVO) throws Exception;

    /**
	 * 반의 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 총 갯수
	 * @exception
	 */
    int selectCurriculumClassTotCnt(CurriculumVO searchVO);

    /**
	 * 반배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    void updateCurriculumClass(CurriculumVO searchVO) throws Exception;

    /**
	 * 반 정보 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectCurriculumGroupList(CurriculumVO searchVO) throws Exception;
    
    /**
	 * 학생 상태별 개수 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectMemberSttusCntList(CurriculumVO searchVO) throws Exception;

    /**
	 * 학생 반, 조 개수 리스트
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectMemberCntList(CurriculumVO searchVO) throws Exception;
    
    // 수강신청 인원 일괄 변경
    public void updateSttusCurriculumMember(EgovMap egovMap) throws Exception;
    
    // 수강신청 인원 삭제 
    public void delCurriculumMember(EgovMap egovMap) throws Exception;
}
