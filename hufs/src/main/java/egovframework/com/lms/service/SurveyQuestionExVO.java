package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumVO.java
 * @Description : Curriculum VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public class SurveyQuestionExVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;

    private String exId;
    private String qesitmId;
    private String exSn;
    private String exCn;
    private String exTy;


	public String getExId() {
		return exId;
	}
	public void setExId(String exId) {
		this.exId = exId;
	}
	public String getQesitmId() {
		return qesitmId;
	}
	public void setQesitmId(String qesitmId) {
		this.qesitmId = qesitmId;
	}
	public String getExSn() {
		return exSn;
	}
	public void setExSn(String exSn) {
		this.exSn = exSn;
	}
	public String getExCn() {
		return exCn;
	}
	public void setExCn(String exCn) {
		this.exCn = exCn;
	}
	public String getExTy() {
		return exTy;
	}
	public void setExTy(String exTy) {
		this.exTy = exTy;
	}

}
