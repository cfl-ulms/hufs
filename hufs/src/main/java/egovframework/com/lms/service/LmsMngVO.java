package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumVO.java
 * @Description : Curriculum VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public class LmsMngVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;

    /** CRCL_ID */
    private java.lang.String crclId;

    /** CRCLB_ID */
    private java.lang.String crclbId;

    /** CRCL_NM */
    private java.lang.String crclNm;

    /** CRCL_NM */
    private java.lang.String crclbNm;

    /** SYS_CODE */
    private java.lang.String sysCode;

    private List<String> SysCodeList;

    /** 과정체계코드경로 */
    private java.lang.String sysCodePath;

    /** 과정체계코드명경로 */
    private java.lang.String sysCodeNmPath;

    /** CRCL_YEAR */
    private int crclYear;

    /** CRCL_TERM */
    private java.lang.String crclTerm;

    //학기명
    private java.lang.String crclTermNm;

    /** CRCL_LANG */
    private java.lang.String crclLang;

    /** 언어명 */
    private java.lang.String crclLangNm;

    /** START_DATE */
    private java.lang.String startDate;

    /** END_DATE */
    private java.lang.String endDate;

    /** TOTAL_TIME */
    private int totalTime;

    /** WEEK_NUM */
    private int weekNum;

    /** DAY_TIME */
    private int dayTime;

    /** CAMPUS_ID */
    private java.lang.String campusId;

    //캠퍼스명
    private java.lang.String campusNm;

    /** CAMPUS_PLACE */
    private java.lang.String campusPlace;

    /** HOST_CODE */
    private java.lang.String hostCode;

    //주관기관명
    private java.lang.String hostCodeNm;

    /** CRCL_GOAL */
    private java.lang.String crclGoal;

    /** CRCL_EFFECT */
    private java.lang.String crclEffect;

    /** TARGET_TYPE */
    private java.lang.String targetType;

    //대상상세
    private java.lang.String targetDetail;

    //대상상세명
    private java.lang.String targetDetailNm;

    /** PLAN_START_DATE */
    private java.lang.String planStartDate;

    /** PLAN_END_DATE */
    private java.lang.String planEndDate;

    /** APPLY_START_DATE */
    private java.lang.String applyStartDate;

    /** APPLY_END_DATE */
    private java.lang.String applyEndDate;

    /** APPLY_MAX_CNT */
    private int applyMaxCnt;

    /** ASSIGN_AT */
    private java.lang.String assignAt;

    /** TUITION_AT */
    private java.lang.String tuitionAt;

    /** TUITION_FEES */
    private java.lang.String tuitionFees;

    /** REGISTRATION_FEES */
    private java.lang.String registrationFees;

    /** TUITION_START_DATE */
    private java.lang.String tuitionStartDate;

    /** TUITION_END_DATE */
    private java.lang.String tuitionEndDate;

    /** CRCL_OUTCOME */
    private java.lang.String crclOutcome;

    //과정성과명
    private java.lang.String crclOutcomeNm;

    /** PROCESS_STTUS_CODE */
    private java.lang.String processSttusCode;

    /** USE_AT */
    private java.lang.String useAt;

    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;

    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;

    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;

    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;

    //학점인정여부
    private java.lang.String gradeAt;

    //이수구분
    private java.lang.String division;

    //이수구분명
    private java.lang.String divisionNm;

    //관리구분
    private java.lang.String control;

    //관리구분명
    private java.lang.String controlNm;

    //성적처리기준
    private java.lang.String gradeType;

    //성적처리기준명
    private java.lang.String gradeTypeNm;

    /** PROJECT_AT */
    private java.lang.String projectAt;

    /** TOTAL_TIME_AT */
    private java.lang.String totalTimeAt;

    //책임교원Id
    private List<String> userIdList;

    //지원비 비목ID
    private List<String> expIdList;

    //지원비 비목 코드
    private List<String> typeCodeList;

    //계정 목
    private List<String> accountCodeList;

    //금액
    private List<String> amountList;

    //산출근거
    private List<String> reasonList;

    //비고
    private List<String> etcList;

    //책임교원Id
    private java.lang.String userId;

    //과정권한코드
    private java.lang.String manageCode;

    //지원비 비목ID
    private java.lang.String expId;

    //지원비 비목 코드
    private java.lang.String typeCode;

    //계정 목
    private java.lang.String accountCode;

    //금액
    private java.lang.String amount;

    //산출근거
    private java.lang.String reason;

    //비고
    private java.lang.String etc;

    //대분류검색
    private java.lang.String searchSysCode1;

    //중분류검색
    private java.lang.String searchSysCode2;

    //소분류검색
    private java.lang.String searchSysCode3;

    //이수구분검색
    private java.lang.String searchDivision;

    //관리구분검색
    private java.lang.String searchControl;

    //기본과정명 검색
    private java.lang.String searchcrclbNm;

    //과정명 검색
    private java.lang.String searchCrclNm;

    //기본과정코드검색
    private java.lang.String searchcrclbId;

    //책임교원검색(ID)
    private java.lang.String searchUserId;

    //책임교원검색(이름)
    private java.lang.String searchUserNm;

    //주관기관검색
    private java.lang.String searchHostCode;

    //언어검색
    private java.lang.String searchCrclLang;

    //코멘트
    private java.lang.String comnt;

    //코멘트등록일자
    private java.util.Date comntPnttm;

    //교원명
    private java.lang.String userNm;

    /*승인여부
     *  Y : 승인,
     *  N : 반려,
     *  R : 대기,
     *  D : 승인취소
     */
    private java.lang.String aprvalAt;
    //승인일자
    private java.util.Date aprvalPnttm;

    //승인취소사유
    private java.lang.String aprvalDn;

    /** 승인결과 검색 */
    private java.lang.String searchAprvalAt;

    /** 프로젝트과정 검색 */
    private java.lang.String searchProjectAt;

    /** 시수과정 검색 */
    private java.lang.String searchTotalTimeAt;

    /** 관리여부 */
    private java.lang.String mngAt;

    //연도검색
    private java.lang.String searchCrclYear;

    //학기검색
    private java.lang.String searchCrclTerm;

    //시작일 검색
    private java.lang.String searchStartDate;

    //종료일 검색
    private java.lang.String searchEndDate;

    //대상검색
    private java.lang.String searchTargetType;

    //대상상세검색
    private java.lang.String searchTargetDetail;

    //과정상태
    private java.lang.String searchProcessSttusCode;

    //총사업비
    private java.lang.Integer totAmount;

    //기타사항
    private java.lang.String etcCn;

    //절대평가여부
    private java.lang.String evaluationAt;

    //운영보고서유형
    private java.lang.String reportType;

    /** SURVEY_SATISFY_TYPE */
    private java.lang.String surveySatisfyType;

    /** SURVEY_IND_AT */
    private java.lang.String surveyIndAt;

    /** SURVEY_IND */
    private java.lang.String surveyInd;

    /** STDNT_APLY_AT */
    private java.lang.String stdntAplyAt;

    //지원비명
    private java.lang.String typeCodeNm;

    //계정목명
    private java.lang.String accountCodeNm;

    //카테고리ID리스트
    private List<String> ctgrymasterIdList;

    //게시판명 리스트
    private List<String> bbsNmList;

    //게시판구분(사용자그룹)
    private List<String> sysTyCodeList;

    //게시판Id리스트
    private List<String> bbsIdList;

    //게시판Id
    private java.lang.String bbsId;

    //카테고리Id
    private java.lang.String ctgrymasterId;

    private java.lang.String plId;

    public java.lang.String getCrclId() {
        return this.crclId;
    }

    public void setCrclId(java.lang.String crclId) {
        this.crclId = crclId;
    }

    public java.lang.String getCrclbId() {
        return this.crclbId;
    }

    public void setCrclbId(java.lang.String crclbId) {
        this.crclbId = crclbId;
    }

    public java.lang.String getCrclNm() {
        return this.crclNm;
    }

    public void setCrclNm(java.lang.String crclNm) {
        this.crclNm = crclNm;
    }

    public java.lang.String getSysCode() {
        return this.sysCode;
    }

    public void setSysCode(java.lang.String sysCode) {
        this.sysCode = sysCode;
    }

    public int getCrclYear() {
        return this.crclYear;
    }

    public void setCrclYear(int crclYear) {
        this.crclYear = crclYear;
    }

    public java.lang.String getCrclTerm() {
		return crclTerm;
	}

	public void setCrclTerm(java.lang.String crclTerm) {
		this.crclTerm = crclTerm;
	}

	public java.lang.String getCrclLang() {
        return this.crclLang;
    }

    public void setCrclLang(java.lang.String crclLang) {
        this.crclLang = crclLang;
    }

    public java.lang.String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(java.lang.String startDate) {
        this.startDate = startDate;
    }

    public java.lang.String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }

    public int getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getWeekNum() {
        return this.weekNum;
    }

    public void setWeekNum(int weekNum) {
        this.weekNum = weekNum;
    }

    public int getDayTime() {
        return this.dayTime;
    }

    public void setDayTime(int dayTime) {
        this.dayTime = dayTime;
    }

    public java.lang.String getCampusId() {
        return this.campusId;
    }

    public void setCampusId(java.lang.String campusId) {
        this.campusId = campusId;
    }

    public java.lang.String getCampusPlace() {
        return this.campusPlace;
    }

    public void setCampusPlace(java.lang.String campusPlace) {
        this.campusPlace = campusPlace;
    }

    public java.lang.String getHostCode() {
        return this.hostCode;
    }

    public void setHostCode(java.lang.String hostCode) {
        this.hostCode = hostCode;
    }

    public java.lang.String getCrclGoal() {
        return this.crclGoal;
    }

    public void setCrclGoal(java.lang.String crclGoal) {
        this.crclGoal = crclGoal;
    }

    public java.lang.String getCrclEffect() {
        return this.crclEffect;
    }

    public void setCrclEffect(java.lang.String crclEffect) {
        this.crclEffect = crclEffect;
    }

    public java.lang.String getTargetType() {
        return this.targetType;
    }

    public void setTargetType(java.lang.String targetType) {
        this.targetType = targetType;
    }

    public java.lang.String getPlanStartDate() {
        return this.planStartDate;
    }

    public void setPlanStartDate(java.lang.String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public java.lang.String getPlanEndDate() {
        return this.planEndDate;
    }

    public void setPlanEndDate(java.lang.String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public java.lang.String getApplyStartDate() {
        return this.applyStartDate;
    }

    public void setApplyStartDate(java.lang.String applyStartDate) {
        this.applyStartDate = applyStartDate;
    }

    public java.lang.String getApplyEndDate() {
        return this.applyEndDate;
    }

    public void setApplyEndDate(java.lang.String applyEndDate) {
        this.applyEndDate = applyEndDate;
    }

    public int getApplyMaxCnt() {
        return this.applyMaxCnt;
    }

    public void setApplyMaxCnt(int applyMaxCnt) {
        this.applyMaxCnt = applyMaxCnt;
    }

    public java.lang.String getAssignAt() {
        return this.assignAt;
    }

    public void setAssignAt(java.lang.String assignAt) {
        this.assignAt = assignAt;
    }

    public java.lang.String getTuitionAt() {
        return this.tuitionAt;
    }

    public void setTuitionAt(java.lang.String tuitionAt) {
        this.tuitionAt = tuitionAt;
    }

    public java.lang.String getTuitionFees() {
		return tuitionFees;
	}

	public void setTuitionFees(java.lang.String tuitionFees) {
		this.tuitionFees = tuitionFees;
	}

	public java.lang.String getRegistrationFees() {
		return registrationFees;
	}

	public void setRegistrationFees(java.lang.String registrationFees) {
		this.registrationFees = registrationFees;
	}

	public java.lang.String getTuitionStartDate() {
        return this.tuitionStartDate;
    }

    public void setTuitionStartDate(java.lang.String tuitionStartDate) {
        this.tuitionStartDate = tuitionStartDate;
    }

    public java.lang.String getTuitionEndDate() {
        return this.tuitionEndDate;
    }

    public void setTuitionEndDate(java.lang.String tuitionEndDate) {
        this.tuitionEndDate = tuitionEndDate;
    }

    public java.lang.String getCrclOutcome() {
        return this.crclOutcome;
    }

    public void setCrclOutcome(java.lang.String crclOutcome) {
        this.crclOutcome = crclOutcome;
    }

    public java.lang.String getProcessSttusCode() {
        return this.processSttusCode;
    }

    public void setProcessSttusCode(java.lang.String processSttusCode) {
        this.processSttusCode = processSttusCode;
    }

    public java.lang.String getUseAt() {
        return this.useAt;
    }

    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }

    public java.lang.String getFrstRegisterId() {
        return this.frstRegisterId;
    }

    public void setFrstRegisterId(java.lang.String frstRegisterId) {
        this.frstRegisterId = frstRegisterId;
    }

    public java.util.Date getFrstRegisterPnttm() {
        return this.frstRegisterPnttm;
    }

    public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
        this.frstRegisterPnttm = frstRegisterPnttm;
    }

    public java.lang.String getLastUpdusrId() {
        return this.lastUpdusrId;
    }

    public void setLastUpdusrId(java.lang.String lastUpdusrId) {
        this.lastUpdusrId = lastUpdusrId;
    }

    public java.util.Date getLastUpdusrPnttm() {
        return this.lastUpdusrPnttm;
    }

    public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
        this.lastUpdusrPnttm = lastUpdusrPnttm;
    }

	public List<String> getSysCodeList() {
		return SysCodeList;
	}

	public void setSysCodeList(List<String> sysCodeList) {
		SysCodeList = sysCodeList;
	}

	public List<String> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}

	public List<String> getExpIdList() {
		return expIdList;
	}

	public void setExpIdList(List<String> expIdList) {
		this.expIdList = expIdList;
	}

	public List<String> getAccountCodeList() {
		return accountCodeList;
	}

	public void setAccountCodeList(List<String> accountCodeList) {
		this.accountCodeList = accountCodeList;
	}

	public List<String> getAmountList() {
		return amountList;
	}

	public void setAmountList(List<String> amountList) {
		this.amountList = amountList;
	}

	public List<String> getReasonList() {
		return reasonList;
	}

	public void setReasonList(List<String> reasonList) {
		this.reasonList = reasonList;
	}

	public List<String> getEtcList() {
		return etcList;
	}

	public void setEtcList(List<String> etcList) {
		this.etcList = etcList;
	}

	public java.lang.String getUserId() {
		return userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	public java.lang.String getManageCode() {
		return manageCode;
	}

	public void setManageCode(java.lang.String manageCode) {
		this.manageCode = manageCode;
	}

	public java.lang.String getExpId() {
		return expId;
	}

	public void setExpId(java.lang.String expId) {
		this.expId = expId;
	}

	public java.lang.String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(java.lang.String accountCode) {
		this.accountCode = accountCode;
	}

	public java.lang.String getAmount() {
		return amount;
	}

	public void setAmount(java.lang.String amount) {
		this.amount = amount;
	}

	public java.lang.String getReason() {
		return reason;
	}

	public void setReason(java.lang.String reason) {
		this.reason = reason;
	}

	public java.lang.String getEtc() {
		return etc;
	}

	public void setEtc(java.lang.String etc) {
		this.etc = etc;
	}

	public List<String> getTypeCodeList() {
		return typeCodeList;
	}

	public void setTypeCodeList(List<String> typeCodeList) {
		this.typeCodeList = typeCodeList;
	}

	public java.lang.String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(java.lang.String typeCode) {
		this.typeCode = typeCode;
	}

	public java.lang.String getSearchSysCode1() {
		return searchSysCode1;
	}

	public void setSearchSysCode1(java.lang.String searchSysCode1) {
		this.searchSysCode1 = searchSysCode1;
	}

	public java.lang.String getSearchSysCode2() {
		return searchSysCode2;
	}

	public void setSearchSysCode2(java.lang.String searchSysCode2) {
		this.searchSysCode2 = searchSysCode2;
	}

	public java.lang.String getSearchSysCode3() {
		return searchSysCode3;
	}

	public void setSearchSysCode3(java.lang.String searchSysCode3) {
		this.searchSysCode3 = searchSysCode3;
	}

	public java.lang.String getSearchDivision() {
		return searchDivision;
	}

	public void setSearchDivision(java.lang.String searchDivision) {
		this.searchDivision = searchDivision;
	}

	public java.lang.String getSearchControl() {
		return searchControl;
	}

	public void setSearchControl(java.lang.String searchControl) {
		this.searchControl = searchControl;
	}

	public java.lang.String getSearchcrclbNm() {
		return searchcrclbNm;
	}

	public void setSearchcrclbNm(java.lang.String searchcrclbNm) {
		this.searchcrclbNm = searchcrclbNm;
	}

	public java.lang.String getSearchcrclbId() {
		return searchcrclbId;
	}

	public void setSearchcrclbId(java.lang.String searchcrclbId) {
		this.searchcrclbId = searchcrclbId;
	}

	public java.lang.String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(java.lang.String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public java.lang.String getSearchUserId() {
		return searchUserId;
	}

	public void setSearchUserId(java.lang.String searchUserId) {
		this.searchUserId = searchUserId;
	}

	public java.lang.String getSearchHostCode() {
		return searchHostCode;
	}

	public void setSearchHostCode(java.lang.String searchHostCode) {
		this.searchHostCode = searchHostCode;
	}

	public java.lang.String getSearchCrclLang() {
		return searchCrclLang;
	}

	public void setSearchCrclLang(java.lang.String searchCrclLang) {
		this.searchCrclLang = searchCrclLang;
	}

	public java.lang.String getComnt() {
		return comnt;
	}

	public void setComnt(java.lang.String comnt) {
		this.comnt = comnt;
	}

	public java.lang.String getAprvalAt() {
		return aprvalAt;
	}

	public void setAprvalAt(java.lang.String aprvalAt) {
		this.aprvalAt = aprvalAt;
	}

	public java.lang.String getSysCodePath() {
		return sysCodePath;
	}

	public void setSysCodePath(java.lang.String sysCodePath) {
		this.sysCodePath = sysCodePath;
	}

	public java.lang.String getSysCodeNmPath() {
		return sysCodeNmPath;
	}

	public void setSysCodeNmPath(java.lang.String sysCodeNmPath) {
		this.sysCodeNmPath = sysCodeNmPath;
	}

	public java.lang.String getCrclLangNm() {
		return crclLangNm;
	}

	public void setCrclLangNm(java.lang.String crclLangNm) {
		this.crclLangNm = crclLangNm;
	}

	public java.lang.String getHostCodeNm() {
		return hostCodeNm;
	}

	public void setHostCodeNm(java.lang.String hostCodeNm) {
		this.hostCodeNm = hostCodeNm;
	}

	public java.lang.String getDivisionNm() {
		return divisionNm;
	}

	public void setDivisionNm(java.lang.String divisionNm) {
		this.divisionNm = divisionNm;
	}

	public java.lang.String getControlNm() {
		return controlNm;
	}

	public void setControlNm(java.lang.String controlNm) {
		this.controlNm = controlNm;
	}

	public java.lang.String getProjectAt() {
		return projectAt;
	}

	public void setProjectAt(java.lang.String projectAt) {
		this.projectAt = projectAt;
	}

	public java.lang.String getTotalTimeAt() {
		return totalTimeAt;
	}

	public void setTotalTimeAt(java.lang.String totalTimeAt) {
		this.totalTimeAt = totalTimeAt;
	}

	public java.lang.String getSearchProjectAt() {
		return searchProjectAt;
	}

	public void setSearchProjectAt(java.lang.String searchProjectAt) {
		this.searchProjectAt = searchProjectAt;
	}

	public java.lang.String getSearchTotalTimeAt() {
		return searchTotalTimeAt;
	}

	public void setSearchTotalTimeAt(java.lang.String searchTotalTimeAt) {
		this.searchTotalTimeAt = searchTotalTimeAt;
	}

	public java.lang.String getSearchAprvalAt() {
		return searchAprvalAt;
	}

	public void setSearchAprvalAt(java.lang.String searchAprvalAt) {
		this.searchAprvalAt = searchAprvalAt;
	}

	public java.util.Date getAprvalPnttm() {
		return aprvalPnttm;
	}

	public void setAprvalPnttm(java.util.Date aprvalPnttm) {
		this.aprvalPnttm = aprvalPnttm;
	}

	public java.lang.String getSearchUserNm() {
		return searchUserNm;
	}

	public void setSearchUserNm(java.lang.String searchUserNm) {
		this.searchUserNm = searchUserNm;
	}

	public java.lang.String getUserNm() {
		return userNm;
	}

	public void setUserNm(java.lang.String userNm) {
		this.userNm = userNm;
	}

	public java.lang.String getMngAt() {
		return mngAt;
	}

	public void setMngAt(java.lang.String mngAt) {
		this.mngAt = mngAt;
	}

	public java.lang.String getSearchCrclYear() {
		return searchCrclYear;
	}

	public void setSearchCrclYear(java.lang.String searchCrclYear) {
		this.searchCrclYear = searchCrclYear;
	}

	public java.lang.String getSearchCrclTerm() {
		return searchCrclTerm;
	}

	public void setSearchCrclTerm(java.lang.String searchCrclTerm) {
		this.searchCrclTerm = searchCrclTerm;
	}

	public java.lang.String getSearchStartDate() {
		return searchStartDate;
	}

	public void setSearchStartDate(java.lang.String searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	public java.lang.String getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(java.lang.String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	public java.lang.String getSearchTargetType() {
		return searchTargetType;
	}

	public void setSearchTargetType(java.lang.String searchTargetType) {
		this.searchTargetType = searchTargetType;
	}

	public java.lang.String getSearchTargetDetail() {
		return searchTargetDetail;
	}

	public void setSearchTargetDetail(java.lang.String searchTargetDetail) {
		this.searchTargetDetail = searchTargetDetail;
	}

	public java.lang.String getSearchProcessSttusCode() {
		return searchProcessSttusCode;
	}

	public void setSearchProcessSttusCode(java.lang.String searchProcessSttusCode) {
		this.searchProcessSttusCode = searchProcessSttusCode;
	}

	public java.lang.Integer getTotAmount() {
		return totAmount;
	}

	public void setTotAmount(java.lang.Integer totAmount) {
		this.totAmount = totAmount;
	}

	public java.lang.String getCrclTermNm() {
		return crclTermNm;
	}

	public void setCrclTermNm(java.lang.String crclTermNm) {
		this.crclTermNm = crclTermNm;
	}

	public java.lang.String getEtcCn() {
		return etcCn;
	}

	public void setEtcCn(java.lang.String etcCn) {
		this.etcCn = etcCn;
	}

	public java.lang.String getCrclbNm() {
		return crclbNm;
	}

	public void setCrclbNm(java.lang.String crclbNm) {
		this.crclbNm = crclbNm;
	}

	public java.lang.String getDivision() {
		return division;
	}

	public void setDivision(java.lang.String division) {
		this.division = division;
	}

	public java.lang.String getControl() {
		return control;
	}

	public void setControl(java.lang.String control) {
		this.control = control;
	}

	public java.lang.String getGradeAt() {
		return gradeAt;
	}

	public void setGradeAt(java.lang.String gradeAt) {
		this.gradeAt = gradeAt;
	}

	public java.lang.String getTargetDetail() {
		return targetDetail;
	}

	public void setTargetDetail(java.lang.String targetDetail) {
		this.targetDetail = targetDetail;
	}

	public java.lang.String getTargetDetailNm() {
		return targetDetailNm;
	}

	public void setTargetDetailNm(java.lang.String targetDetailNm) {
		this.targetDetailNm = targetDetailNm;
	}

	public java.lang.String getGradeType() {
		return gradeType;
	}

	public void setGradeType(java.lang.String gradeType) {
		this.gradeType = gradeType;
	}

	public java.lang.String getGradeTypeNm() {
		return gradeTypeNm;
	}

	public void setGradeTypeNm(java.lang.String gradeTypeNm) {
		this.gradeTypeNm = gradeTypeNm;
	}

	public java.lang.String getEvaluationAt() {
		return evaluationAt;
	}

	public void setEvaluationAt(java.lang.String evaluationAt) {
		this.evaluationAt = evaluationAt;
	}

	public java.lang.String getReportType() {
		return reportType;
	}

	public void setReportType(java.lang.String reportType) {
		this.reportType = reportType;
	}

	public java.lang.String getSurveySatisfyType() {
		return surveySatisfyType;
	}

	public void setSurveySatisfyType(java.lang.String surveySatisfyType) {
		this.surveySatisfyType = surveySatisfyType;
	}

	public java.lang.String getSurveyIndAt() {
		return surveyIndAt;
	}

	public void setSurveyIndAt(java.lang.String surveyIndAt) {
		this.surveyIndAt = surveyIndAt;
	}

	public java.lang.String getSurveyInd() {
		return surveyInd;
	}

	public void setSurveyInd(java.lang.String surveyInd) {
		this.surveyInd = surveyInd;
	}

	public java.lang.String getStdntAplyAt() {
		return stdntAplyAt;
	}

	public void setStdntAplyAt(java.lang.String stdntAplyAt) {
		this.stdntAplyAt = stdntAplyAt;
	}

	public java.lang.String getCampusNm() {
		return campusNm;
	}

	public void setCampusNm(java.lang.String campusNm) {
		this.campusNm = campusNm;
	}

	public java.lang.String getTypeCodeNm() {
		return typeCodeNm;
	}

	public void setTypeCodeNm(java.lang.String typeCodeNm) {
		this.typeCodeNm = typeCodeNm;
	}

	public java.lang.String getAccountCodeNm() {
		return accountCodeNm;
	}

	public void setAccountCodeNm(java.lang.String accountCodeNm) {
		this.accountCodeNm = accountCodeNm;
	}

	public java.lang.String getCrclOutcomeNm() {
		return crclOutcomeNm;
	}

	public void setCrclOutcomeNm(java.lang.String crclOutcomeNm) {
		this.crclOutcomeNm = crclOutcomeNm;
	}

	public java.util.Date getComntPnttm() {
		return comntPnttm;
	}

	public void setComntPnttm(java.util.Date comntPnttm) {
		this.comntPnttm = comntPnttm;
	}

	public java.lang.String getAprvalDn() {
		return aprvalDn;
	}

	public void setAprvalDn(java.lang.String aprvalDn) {
		this.aprvalDn = aprvalDn;
	}

	public List<String> getCtgrymasterIdList() {
		return ctgrymasterIdList;
	}

	public void setCtgrymasterIdList(List<String> ctgrymasterIdList) {
		this.ctgrymasterIdList = ctgrymasterIdList;
	}

	public List<String> getBbsNmList() {
		return bbsNmList;
	}

	public void setBbsNmList(List<String> bbsNmList) {
		this.bbsNmList = bbsNmList;
	}

	public List<String> getSysTyCodeList() {
		return sysTyCodeList;
	}

	public void setSysTyCodeList(List<String> sysTyCodeList) {
		this.sysTyCodeList = sysTyCodeList;
	}

	public List<String> getBbsIdList() {
		return bbsIdList;
	}

	public void setBbsIdList(List<String> bbsIdList) {
		this.bbsIdList = bbsIdList;
	}

	public java.lang.String getBbsId() {
		return bbsId;
	}

	public void setBbsId(java.lang.String bbsId) {
		this.bbsId = bbsId;
	}

	public java.lang.String getCtgrymasterId() {
		return ctgrymasterId;
	}

	public void setCtgrymasterId(java.lang.String ctgrymasterId) {
		this.ctgrymasterId = ctgrymasterId;
	}

	public java.lang.String getPlId() {
		return plId;
	}

	public void setPlId(java.lang.String plId) {
		this.plId = plId;
	}

}
