package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.SurveyVO;

/**
 * @Class Name : CurriculumDAO.java
 * @Description : Curriculum DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Repository("curriculumDAO")
public class CurriculumDAO extends EgovAbstractDAO {

	/**
	 * curriculum을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertCurriculum(CurriculumVO vo) throws Exception {
        return (String)insert("curriculumDAO.insertCurriculum_S", vo);
    }

    /**
	 * curriculum을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculum(CurriculumVO vo) throws Exception {
        update("curriculumDAO.updateCurriculum_S", vo);
    }

    //과정 일부 만 수정
    public void updateCurriculumPart(CurriculumVO vo) throws Exception {
        update("curriculumDAO.updateCurriculumPart", vo);
    }

    /**
	 * curriculum을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void deleteCurriculum(CurriculumVO vo) throws Exception {
        update("curriculumDAO.deleteCurriculum_S", vo);
    }

    /**
	 * curriculum을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumVO
	 * @return 조회한 curriculum
	 * @exception Exception
	 */
    public CurriculumVO selectCurriculum(CurriculumVO vo) throws Exception {
        return (CurriculumVO) select("curriculumDAO.selectCurriculum_S", vo);
    }

    /**
	 * curriculum 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculum 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumList(CurriculumVO searchVO) throws Exception {
        //return (List<EgovMap>) list("curriculumDAO.selectCurriculumList_D", searchVO);
    	return list("curriculumDAO.selectCurriculumList_D", searchVO);
    }

    /**
	 * curriculum 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculum 총 갯수
	 * @exception
	 */
    public int selectCurriculumListTotCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumDAO.selectCurriculumListTotCnt_S", searchVO);
    }

    //책임교원등록
    public String insertCurriculumMng(CurriculumVO vo) throws Exception {
        return (String)insert("curriculumDAO.insertCurriculumMng", vo);
    }

    //책임교원삭제
    public void deleteCurriculumMng(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumMng", vo);
    }

    //사업비등록
    public String insertCurriculumExp(CurriculumVO vo) throws Exception {
        return (String)insert("curriculumDAO.insertCurriculumExp", vo);
    }

    //사업비 목록
    public List selectCurriculumExpense(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectCurriculumExpense", vo);
    }

    //프로세스업데이트
    public void updatePsCodeCurriculum(CurriculumVO vo) throws Exception {
        update("curriculumDAO.updatePsCodeCurriculum", vo);
    }

    //사업비삭제
    public void deleteCurriculumExp(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumExp", vo);
    }

    //학습내용등록
    public void insertCurriculumLesson(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertCurriculumLesson", vo);
    }

    //학습내용삭제
    public void deleteCurriculumLesson(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumLesson", vo);
    }

    //학습내용 목록
    public List selectCurriculumLesson(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectCurriculumLesson", vo);
    }

    //과정 사용자(관리자) 목록
    public List selectCurriculumMng(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectCurriculumMng", vo);
    }

    //교원배정등록
    public void insertCurriculumFaculty(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertCurriculumFaculty", vo);
    }

    //교원배정삭제
    public void deleteCurriculumFaculty(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumFaculty", vo);
    }

    //교원배정목록
    public List selectCurriculumFaculty(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectCurriculumFaculty", vo);
    }

    //교원배정(중복제거) 목록
    public List selectCurriculumFacultyDp(CurriculumVO vo) throws Exception {
    	return list("curriculumDAO.selectCurriculumFacultyDp", vo);
    }

    //총괄평가등록
    public void insertTotalEvaluation(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertTotalEvaluation", vo);
    }

    //총괄평가삭제
    public void deleteTotalEvaluation(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteTotalEvaluation", vo);
    }

    //총괄평가목록
    public List selectTotalEvaluation(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectTotalEvaluation", vo);
    }

    //수업참여도게시판목록
    public List selectAttendbbs(CurriculumVO vo) throws Exception {
        return list("curriculumDAO.selectAttendbbs", vo);
    }

    //수업참여게시판
    public void insertAttendbbs(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertAttendbbs", vo);
    }

    //교재 및 부교재 등록
    public void insertBookbbs(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertBookbbs", vo);
    }

    //교재 및 부교재 삭제
    public void deleteBookbbs(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.deleteBookbbs", vo);
    }

    //교재 및 부교재 목록
    public List selectBookbbs(CurriculumVO vo) throws Exception {
    	return list("curriculumDAO.selectBookbbs", vo);
    }

    //과정 그룹 등록
    public void insertCurriculumGroup(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertCurriculumGroup_S", vo);
    }
    
    //과정 그룹 삭제
    public void deleteCurriculumGroup(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumGroup_S", vo);
    }

    //과정 맴버 삭제(한명)
    public void deleteCurriculumMember(CurriculumVO vo) throws Exception {
        delete("curriculumDAO.deleteCurriculumMember", vo);
    }
    
    //과정 맴버 삭제(과정별)
    public void deleteCurriculumMemberList(CurriculumVO vo) throws Exception {
    	delete("curriculumDAO.deleteCurriculumMemberList", vo);
    }

    /**
	 * curriculummember을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public void insertCurriculumMember(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.insertCurriculumMember_S", vo);
    }

    //수강신청 첨부 서류 재처리
    public void updateCurriculumMember(CurriculumVO vo) throws Exception {
        insert("curriculumDAO.updateCurriculumMember_S", vo);
    }


	//중복 수강신청 조회
    public int selectCurriculumDuplicationMemberCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumDAO.selectCurriculumDuplicationMemberCnt_S", searchVO);
    }

    //수강신청 조회
    public CurriculumVO selectCurriculumMemberDetail(CurriculumVO vo) throws Exception {
        return (CurriculumVO) select("curriculumDAO.selectCurriculumMemberDetail_S", vo);
    }
    
   //수강신청 학생 목록
    public List<EgovMap> selectCurriculumMemberList(CurriculumVO vo) throws Exception {
    	return (List<EgovMap>)list("curriculumDAO.selectCurriculumMemberList", vo);
    }
    
       
    // 수정 시 교육과정 수강신청 인원 새로이 저장 
    public void insertCurriculumMemberAdd(CurriculumVO vo) throws Exception {
    	insert("curriculumDAO.insertCurriculumMemberAdd", vo);
    }
    
    //수강신청 학생 이력 저장
    public void insertCurriculumMemberHistory(CurriculumVO vo) throws Exception {
    	insert("curriculumDAO.insertCurriculumMemberHistory", vo);
    }
    
    //수강신청 학생 이력 use_flag = 'N' 처리
    public void updateCurriculumMemberHistory(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateCurriculumMemberHistory", vo);
    }

    // 오늘의 수업
    public List<?> selectTodayCrclList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectTodayCrclList", searchVO);
    }

    /**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 총 갯수
	 * @exception
	 */
    public int selectHomeworkTotCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumDAO.selectHomeworkTotCnt_S", searchVO);
    }

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectHomeworkList_D", searchVO);
    }

    /**
	 * 과제 제출 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkSubmitWaitingList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectHomeworkSubmitWaitingList_D", searchVO);
    }

    /**
	 * 과제 평가 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkTestWaitingList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectHomeworkTestWaitingList_D", searchVO);
    }

    /**
	 * 과제를 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public void insertHomeworkArticle(CurriculumVO searchVO) throws Exception {
    	insert("curriculumMemberDAO.insertHomeworkArticle_S", searchVO);
    }

    /**
	 * 과제 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public CurriculumVO selectHomeworkArticle(CurriculumVO vo) throws Exception {
        return (CurriculumVO) select("curriculumDAO.selectHomeworkArticle_S", vo);
    }

    /**
	 * 과제를 삭제한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 삭제
	 * @exception Exception
	 */
    public void deleteHomeworkArticle(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.deleteHomework", vo);
    }

    /**
	 * 과제를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateHomeworkArticle(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateHomeworkArticle", vo);
    }

    /**
	 * 과제 제출 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkSubjectList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectHomeworkSubjectList_S", searchVO);
    }

    /**
	 * 학생 공개를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateStuOpenAt(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateStuOpenAt", vo);
    }

    /**
	 * 과제 후기 선정을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateCommentPickAt(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateCommentPickAt", vo);
    }

    /**
	 * 과제 피드백을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateFdb(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateFdb", vo);
    }

    /**
	 * 과제 제출 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public CurriculumVO selectHomeworkSubmitArticle(CurriculumVO vo) throws Exception {
        return (CurriculumVO) select("curriculumDAO.selectHomeworkSubmitArticle_S", vo);
    }

    /**
	 * 과제 제출을 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public void insertHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception {
    	insert("curriculumMemberDAO.insertHomeworkSubmitArticle_S", searchVO);
    }
    
    /**
     * 과제 제출을 수정한다.
     * @param searchMap - 조회할 정보가 담긴 Map
     * @return homework 수정
     * @exception Exception
     */
    public void updateHomeworkSubmitArticle(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateHomeworkSubmitArticle", vo);
    }
    
    /**
     * 과정후기를 등록한다.
     * @param searchMap - 조회할 정보가 담긴 Map
     * @return homework 목록
     * @exception Exception
     */
    public void insertCourseReview(CurriculumVO searchVO) throws Exception {
    	insert("curriculumMemberDAO.insertCourseReview", searchVO);
    }
    
    // 과정후기를 수정한다.
    public void updateCourseReview(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateCourseReview", vo);
    }
    
    // 과정후기를 삭제한다.(user_at > N 처리)
    public void deleteCourseReview(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.deleteCourseReview", vo);
    }


    //과제평가
    public List homeworkScoreList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.homeworkScoreList", searchVO);
    }

    //과제평가 - 학생 점수 요약
    public List homeworkScoreSum(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.homeworkScoreSum", searchVO);
    }

    //과제평가 - 성적반영
    public void updateScoreApply(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.updateScoreApply", vo);
    }

    //과제평가 - 성적반영 삭제
    public void deleteScoreApply(CurriculumVO vo) throws Exception {
    	update("curriculumDAO.deleteScoreApply", vo);
    }

    /**
	 * 반 개수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public int selectCurriculumMemberGroupCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumDAO.selectCurriculumMemberGroupCnt_S", searchVO);
    }

    // My > 교육과정이력 조회
    public List<?> selectMyCurriculumHistoryList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectMyCurriculumHistoryList", searchVO);
    }

    // My > 교육과정이력 조회
    public List<?> selectMyCurriculumHistoryListCount(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectMyCurriculumHistoryListCount", searchVO);
    }

    // My > 교육과정이력 - 총 과정수, 과정중 count
    public int selectMyCurriculumHistoryCnt(CurriculumVO searchVO) throws Exception {
        return (Integer) select("curriculumDAO.selectMyCurriculumHistoryCnt", searchVO);
    }

    //학생 과제 공개 개수
    public int selectHomeworkCommentPickAtCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumDAO.selectHomeworkCommentPickAtCnt_S", searchVO);
    }

    /**
	 * 학생 과제 공개 리스트를 조회.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkCommentPickAtList(CurriculumVO searchVO) throws Exception {
        return list("curriculumDAO.selectHomeworkCommentPickAtList_S", searchVO);
    }
    
    // 과정후기 리스트
    public List<?> selectCourseReviewList(CurriculumVO searchVO) throws Exception {
    	return list("curriculumDAO.selectCourseReviewList", searchVO);
    }
    
    // 과정후기 목록 리스트
    public List<?> selectCourseReviewNoticeList(CurriculumVO searchVO) throws Exception {
    	return list("curriculumDAO.selectCourseReviewNoticeList", searchVO);
    }

    // 과정후기 목록 갯수
    public int selectCourseReviewCnt(CurriculumVO searchVO) {
    	return (Integer)select("curriculumDAO.selectCourseReviewCnt", searchVO);
    }
    
    // 과정후기 상세
    public CurriculumVO selectCourseReviewAtView(CurriculumVO vo) throws Exception {
        return (CurriculumVO) select("curriculumDAO.selectCourseReviewAtView", vo);
    }
    
    public List<EgovMap> selectReportList(CurriculumVO vo) throws Exception {
        return (List<EgovMap>) list("curriculumDAO.selectReportList", vo);
    }

    public int selectReportListCnt(CurriculumVO searchVO) throws Exception {
        return (Integer)select("curriculumDAO.selectReportListCnt", searchVO);
    }

    public List<EgovMap> selectAdminReportList(CurriculumVO vo) throws Exception {
        return (List<EgovMap>) list("curriculumDAO.selectAdminReportList", vo);
    }

    public int selectAdminReportListCnt(CurriculumVO searchVO) throws Exception {
        return (Integer)select("curriculumDAO.selectAdminReportListCnt", searchVO);
    }

    //교육과정 통계(운영보고서)
    public EgovMap curriculumSts(CurriculumVO vo) throws Exception{
    	return (EgovMap)select("curriculumDAO.curriculumSts", vo);
    }
    
    //과정 목록 - 학생기준
    public List<EgovMap> selectMyCurriculumList(CurriculumVO vo) throws Exception {
        return (List<EgovMap>) list("curriculumDAO.selectMyCurriculumList", vo);
    }
    
    //과정 목록 수 - 학생기준
    public int selectMyCurriculumListCnt(CurriculumVO vo) throws Exception {
        return (Integer)select("curriculumDAO.selectMyCurriculumListCnt", vo);
    }
    
    //과정 진행 단계 체크
    public EgovMap curriculumStatusCheck(CurriculumVO vo) throws Exception {
    	return (EgovMap) select("curriculumDAO.curriculumStatusCheck", vo);
    }
    
    //설문 참여 체크
    public EgovMap curriculumSurveyCheck(CurriculumVO vo) throws Exception {
    	return (EgovMap) select("curriculumDAO.curriculumSurveyCheck", vo);
    }
}
