package egovframework.com.lms.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.lms.service.CurriculumVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

@Repository("curriculumMemberDAO")
public class CurriculumMemberDAO extends EgovAbstractDAO {
	/**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculum 총 갯수
	 * @exception
	 */
    public int selectCurriculumMemberTotCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumMemberDAO.selectCurriculumMemberTotCnt_S", searchVO);
    }

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculumMember 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumMemberList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectCurriculumMember_D", searchVO);
    }

    /**
	 * curriculumMember을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumAccept(CurriculumVO vo) throws Exception {
        update("curriculumMemberDAO.updateCurriculumAccept_S", vo);
    }

    /**
	 * curriculumMember 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculumMember 목록
	 * @exception Exception
	 */
    public List<?> curriculumMemberStatsList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.curriculumMemberStats_D", searchVO);
    }

    /**
	 * 수강 대상자 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculumMember 목록
	 * @exception Exception
	 */
    public List<?> selectStudentList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectStudentList_D", searchVO);
    }

    /**
	 * 수강 그룹 개수 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 개수
	 * @exception Exception
	 */
    public List<?> selectMemberGroupCnt(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectMemberGroupCnt_S", searchVO);
    }

    /**
	 * 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    public List<?> selectGroupList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectGroupList_D", searchVO);
    }

    /**
	 * 반,조 초기화.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumGroupReset(CurriculumVO vo) throws Exception {
        update("curriculumMemberDAO.updateCurriculumGroupReset_S", vo);
    }

    /**
	 * 조배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumGroup(CurriculumVO vo) throws Exception {
        update("curriculumMemberDAO.updateCurriculumGroup_S", vo);
    }

    /**
	 * 선택한 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    public List<?> selectPickStudentList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectPickStudentList_D", searchVO);
    }

    /**
	 * 조의 개수 리스트를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculummember 총 갯수
	 * @exception
	 */
    public List<?> selectCurriculumGroupCntList(CurriculumVO searchVO) {
        return list("curriculumMemberDAO.selectCurriculumGroupCntList_D", searchVO);
    }

    /**
	 * curriculumgroup을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 목록
	 * @exception
	 */
    public List<?> selectCurriculumClassList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectCurriculumClassList_D", searchVO);
    }

    /**
	 * 반의 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 총 갯수
	 * @exception
	 */
    public int selectCurriculumClassTotCnt(CurriculumVO searchVO) {
        return (Integer)select("curriculumMemberDAO.selectCurriculumClassTotCnt_S", searchVO);
    }

    /**
	 * 조배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumClassReset(CurriculumVO vo) throws Exception {
        update("curriculumMemberDAO.updateCurriculumClassReset_S", vo);
    }

    /**
	 * 반정보 삭제.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void deleteCurriculumManage(CurriculumVO vo) throws Exception {
        delete("curriculumMemberDAO.deleeCurriculumManage_S", vo);
    }

    /**
	 * 반정보 등록.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void insertCurriculumManage(CurriculumVO vo) throws Exception {
        insert("curriculumMemberDAO.insertCurriculumManage_S", vo);
    }

    /**
	 * 학생 정보에 반 정보 수정
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumClass(CurriculumVO vo) throws Exception {
        insert("curriculumMemberDAO.updateCurriculumClass_S", vo);
    }

    /**
	 * 반 정보 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectCurriculumGroupList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectCurriculumGroupList_D", searchVO);
    }
    
    /**
	 * 학생 상태별 개수 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectMemberSttusCntList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectMemberSttusCntList_D", searchVO);
    }
    
    /**
	 * 학생 반, 조 개수 리스트
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public List<?> selectMemberCntList(CurriculumVO searchVO) throws Exception {
        return list("curriculumMemberDAO.selectMemberCntList_D", searchVO);
    }
    
    // 수강신청 인원 일괄 승인
    public int updateSttusCurriculumMember(CurriculumVO vo) throws Exception {
    	return update("curriculumMemberDAO.updateSttusCurriculumMember", vo);
    }
    // 수강신청 인원 삭제
    public int delCurriculumMember(CurriculumVO vo) throws Exception {
    	return delete("curriculumMemberDAO.delCurriculumMember", vo);
    }
}
