package egovframework.com.lms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.impl.CurriculumbaseDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : CurriculumbaseServiceImpl.java
 * @Description : Curriculumbase Business Implement class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Service("curriculumbaseService")
public class CurriculumbaseServiceImpl extends EgovAbstractServiceImpl implements
        CurriculumbaseService {
        
    @Resource(name="curriculumbaseDAO")
    private CurriculumbaseDAO curriculumbaseDAO;
    
    /** ID Generation */
    @Resource(name="crclbIdGnrService")    
    private EgovIdGnrService egovIdGnrService;

	/**
	 * curriculumbase을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumbaseVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertCurriculumbase(CurriculumbaseVO vo) throws Exception {
    	//코드
    	if(EgovStringUtil.isEmpty(vo.getCrclbId())){
    		String id = egovIdGnrService.getNextStringId();
        	vo.setCrclbId(id);
    	}
    	
    	//과정체계코드
    	String sysCode = "";
    	for(int i = 0; i < vo.getSysCodeList().size(); i++){
    		if(!EgovStringUtil.isEmpty(vo.getSysCodeList().get(i))){
    			sysCode = vo.getSysCodeList().get(i);
    		}
    	}
    	vo.setSysCode(sysCode);
    	
    	//학생 수강신청 제출서류 여부
    	if("N".equals(vo.getStdntAplyAt())){
    		vo.setAplyFile("");
    		vo.setPlanFile("");
    		vo.setEtcFile("");
    	}
    	
    	curriculumbaseDAO.insertCurriculumbase(vo);
    	
    	//참고자료
    	if(vo.getRefeFileList() != null && vo.getRefeFileList().size() > 0){
    		for(int i = 0; i < vo.getRefeFileList().size(); i++){
        		vo.setAtchFileId(vo.getRefeFileList().get(i));
        		curriculumbaseDAO.insertCurriculumbasefile(vo);
        	}
    	}
    	
        return null;
    }

    /**
	 * curriculumbase을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumbaseVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumbase(CurriculumbaseVO vo) throws Exception {
    	//과정체계코드
    	String sysCode = "";
    	for(int i = 0; i < vo.getSysCodeList().size(); i++){
    		if(!EgovStringUtil.isEmpty(vo.getSysCodeList().get(i))){
    			sysCode = vo.getSysCodeList().get(i);
    		}
    	}
    	vo.setSysCode(sysCode);
    	
    	//학생 수강신청 제출서류 여부
    	if("N".equals(vo.getStdntAplyAt())){
    		vo.setAplyFile("");
    		vo.setPlanFile("");
    		vo.setEtcFile("");
    	}
    	
    	curriculumbaseDAO.updateCurriculumbase(vo);
    	
    	//참고자료
    	curriculumbaseDAO.deleteCurriculumbasefile(vo);
    	if(vo.getRefeFileList() != null && vo.getRefeFileList().size() > 0){
    		for(int i = 0; i < vo.getRefeFileList().size(); i++){
        		vo.setAtchFileId(vo.getRefeFileList().get(i));
        		curriculumbaseDAO.insertCurriculumbasefile(vo);
        	}
    	}
    }

    /**
	 * curriculumbase을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumbaseVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteCurriculumbase(CurriculumbaseVO vo) throws Exception {
        curriculumbaseDAO.deleteCurriculumbase(vo);
    }

    /**
	 * curriculumbase을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return 조회한 curriculumbase
	 * @exception Exception
	 */
    public CurriculumbaseVO selectCurriculumbase(CurriculumbaseVO vo) throws Exception {
        return curriculumbaseDAO.selectCurriculumbase(vo);
    }

    /**
	 * curriculumbase 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumbase 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumbaseList(CurriculumbaseVO searchVO) throws Exception {
        return curriculumbaseDAO.selectCurriculumbaseList(searchVO);
    }
    
    public List selectCurriculumbaseFile(CurriculumbaseVO vo) throws Exception{
    	return curriculumbaseDAO.selectCurriculumbaseFile(vo);
    }
    
    /**
	 * curriculumbase 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumbase 총 갯수
	 * @exception
	 */
    public int selectCurriculumbaseListTotCnt(CurriculumbaseVO searchVO) {
		return curriculumbaseDAO.selectCurriculumbaseListTotCnt(searchVO);
	}
    
}
