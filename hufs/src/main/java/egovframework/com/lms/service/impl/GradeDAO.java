package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.lms.service.CurriculumVO;

//성적
@Repository("gradeDAO")
public class GradeDAO extends EgovAbstractDAO {

    //성적등록
    public void insertGrade(CurriculumVO vo) throws Exception {
        insert("gradeDAO.insertGrade", vo);
    }
    
    //기타성적등록
    public void insertGradeEtc(CurriculumVO vo) throws Exception {
        insert("gradeDAO.insertGradeEtc", vo);
    }
    
    //성적 삭제
    public void deleteGrade(CurriculumVO vo) throws Exception {
        insert("gradeDAO.deleteGrade", vo);
    }
    
    //기타성적 삭제
    public void deleteGradeEtc(CurriculumVO vo) throws Exception {
        insert("gradeDAO.deleteGradeEtc", vo);
    }
    
    //성적목록
    public List selectGradeList(CurriculumVO vo) throws Exception {
        return list("gradeDao.selectGradeList", vo);
    }
    
    //수료정보취합
    public EgovMap selectGradeSummary(CurriculumVO vo) throws Exception {
        return (EgovMap) select("gradeDao.selectGradeSummary", vo);
    }
    
    
}
