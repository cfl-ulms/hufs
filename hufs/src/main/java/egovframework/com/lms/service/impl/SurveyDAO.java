package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.SearchVO;
import egovframework.com.lms.service.SurveyQuestionExVO;
import egovframework.com.lms.service.SurveyQuestionVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumDAO.java
 * @Description : Curriculum DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Repository("surveyDAO")
public class SurveyDAO extends EgovAbstractDAO {

    public List<?> selectSurveyList(SurveyVO surveyVO) throws Exception {
        return list("surveyDAO.selectSurveyList", surveyVO);
    }

    public int selectSurveyListCnt(SurveyVO surveyVO) {
        return (Integer)select("surveyDAO.selectSurveyListCnt", surveyVO);
    }

    public void insertSurvey(SurveyVO surveyVO) throws Exception {
    	insert("surveyDAO.insertSurvey", surveyVO);
    }

    public void insertSurveyQuestion(SurveyQuestionVO questVo) throws Exception {
        insert("surveyDAO.insertSurveyQuestion", questVo);
    }

    public void insertSurveyExample(SurveyQuestionExVO exVo) throws Exception {
        insert("surveyDAO.insertSurveyExample", exVo);
    }

    public SurveyVO selectSurveyInfo(SurveyVO surveyVO) {
        return (SurveyVO) select("surveyDAO.selectSurveyInfo", surveyVO);
    }

    public void deleteSurveyQuestion(SurveyVO surveyVO) throws Exception {
        update("surveyDAO.deleteSurveyQeustion", surveyVO);
    }

    public List selectCurriculumSurveyList(SurveyVO surveyVO) throws Exception {
        return list("surveyDAO.selectCurriculumSurveyList", surveyVO);
    }

    public int selectCurriculumSurveyListCnt(SurveyVO surveyVO) {
        return (Integer)select("surveyDAO.selectCurriculumSurveyListCnt", surveyVO);
    }

    public List selectSurveyQuestionList(SurveyVO surveyVO) throws Exception {
        return list("surveyDAO.selectSurveyQuestionList", surveyVO);
    }

    public List selectCurriculumSurveyAnswer(SurveyQuestionVO questVo) throws Exception {
        return list("surveyDAO.selectCurriculumSurveyAnswer", questVo);
    }

    public EgovMap selectCurriculumAddSchedule(SurveyVO surveyVO) throws Exception {
        return (EgovMap)select("surveyDAO.selectCurriculumAddSchedule", surveyVO);
    }

    public EgovMap selectCurriculumAddInfo(SurveyVO surveyVO) throws Exception {
        return (EgovMap)select("surveyDAO.selectCurriculumAddInfo", surveyVO);
    }
    
    //과정만족도 - 교원
    public EgovMap selectCurriculumAddInfoType3(SurveyVO surveyVO) throws Exception {
        return (EgovMap)select("surveyDAO.selectCurriculumAddInfoType3", surveyVO);
    }
  
    //설문 제출 현황 목록
    public List selectSurveyMember(SurveyVO surveyVO) throws Exception {
        return list("surveyDAO.selectSurveyMember", surveyVO);
    }
    
    //설문 제출 현황 목록
    public EgovMap selectSurveyMemberSummary(SurveyVO surveyVO) throws Exception {
        return (EgovMap)select("surveyDAO.selectSurveyMemberSummary", surveyVO);
    }
    
    public int selectAnswerSurveyCnt(SurveyVO surveyVO) {
        return (Integer)select("surveyDAO.selectAnswerSurveyCnt", surveyVO);
    }

    public void insertSurveyAnswer(EgovMap map) throws Exception {
        insert("surveyDAO.insertSurveyAnswer", map);
    }

    public List selectMySurveyList(SearchVO searchVo) throws Exception {
        return list("surveyDAO.selectMySurveyList", searchVo);
    }

    public int selectMySurveyListCnt(SearchVO searchVo) {
        return (Integer)select("surveyDAO.selectMySurveyListCnt", searchVo);
    }

    public void insertSurveySubmit(EgovMap map) throws Exception {
        insert("surveyDAO.insertSurveySubmit", map);
    }

    public List selectCurriculumSurveyEssayAnswer(SurveyQuestionVO questVo) throws Exception {
        return list("surveyDAO.selectCurriculumSurveyEssayAnswer", questVo);
    }

    public String selectSurveyId(SurveyVO surveyVO) throws Exception {
        return (String)select("surveyDAO.selectSurveyId", surveyVO);
    }
    
    //과정만족도 - 교원용
    public List selectMyProfessorSurveyList(SearchVO searchVo) throws Exception {
        return list("surveyDAO.selectMyProfessorSurveyList", searchVo);
    }
    
    //과정만족도 - 교원용
    public int selectMyProfessorSurveyCnt(SearchVO searchVo) {
        return (Integer)select("surveyDAO.selectMyProfessorSurveyCnt", searchVo);
    }
    
    //설문 제출자 - 교원
    public List selectSurveyProfessor(SurveyVO surveyVO) throws Exception {
        return list("surveyDAO.selectSurveyProfessor", surveyVO);
    }
    
    //설문 제출 현황 목록 - 교원
    public EgovMap selectSurveyProfessorSummary(SurveyVO surveyVO) throws Exception {
        return (EgovMap)select("surveyDAO.selectSurveyProfessorSummary", surveyVO);
    }
    
    // 과정만족도 목록
    @SuppressWarnings("unchecked")
	public List<EgovMap> curriculumSurveyAnswerScoreList(SurveyQuestionVO surveyQuestionVo) throws Exception {
    	return (List<EgovMap>)list("surveyDAO.curriculumSurveyAnswerScoreList", surveyQuestionVo);
    }
    
    // 과정만족도 목록
    public EgovMap selectMySurveyDetail(SurveyVO surveyVO) throws Exception {
    	return (EgovMap)select("surveyDAO.selectMySurveyDetail", surveyVO);
    }
}
