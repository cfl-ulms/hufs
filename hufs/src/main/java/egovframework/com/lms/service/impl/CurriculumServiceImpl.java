package egovframework.com.lms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.mysql.jdbc.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.lms.service.impl.CurriculumDAO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @Class Name : CurriculumServiceImpl.java
 * @Description : Curriculum Business Implement class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Service("curriculumService")
public class CurriculumServiceImpl extends EgovAbstractServiceImpl implements CurriculumService {

    @Resource(name="curriculumDAO")
    private CurriculumDAO curriculumDAO;

    /** ID Generation */
    @Resource(name="crclIdGnrService")
    private EgovIdGnrService egovIdGnrService;

    @Resource(name="expIdGnrService")
    private EgovIdGnrService expIdGnrService;

    @Resource(name = "EgovBBSCtgryMasterService")
    private EgovBBSCtgryMasterService egovBBSCtgryMasterService;

    @Resource(name = "EgovBBSAttributeManageService")
	private EgovBBSAttributeManageService bbsAttrbService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "homeworklIdGnrService")
    private EgovIdGnrService idgenService;

    @Resource(name = "homeworkSubmitlIdGnrService")
    private EgovIdGnrService idgenServiceHomeworkSubmit;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

	/**
	 * curriculum을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertCurriculum(CurriculumVO vo) throws Exception {
    	/** ID Generation Service */
    	String id = egovIdGnrService.getNextStringId();
    	vo.setCrclId(id);

    	//과정체계코드
    	String sysCode = "";
    	for(int i = 0; i < vo.getSysCodeList().size(); i++){
    		if(!EgovStringUtil.isEmpty(vo.getSysCodeList().get(i))){
    			sysCode = vo.getSysCodeList().get(i);
    		}
    	}
    	vo.setSysCode(sysCode);

    	//금액부분정리
    	vo.setTuitionFees(vo.getTuitionFees().replaceAll(",", ""));
    	vo.setRegistrationFees(vo.getRegistrationFees().replaceAll(",", ""));

    	curriculumDAO.insertCurriculum(vo);

    	//책임권한
    	if(vo.getUserIdList() != null){
    		for(int i = 0; i < vo.getUserIdList().size(); i++){
        		vo.setUserId(vo.getUserIdList().get(i));
        		vo.setManageCode("10");

        		curriculumDAO.insertCurriculumMng(vo);

        		//과정 그룹 등록
        		if(i == 0) {
        	    	curriculumDAO.insertCurriculumGroup(vo);
        		}
        	}
    	}

    	//사업비
    	if(vo.getTypeCodeList() != null){
    		for(int i = 0; i < vo.getTypeCodeList().size(); i++){
        		String expId = expIdGnrService.getNextStringId();
        		vo.setExpId(expId);
        		vo.setTypeCode(vo.getTypeCodeList().get(i));
        		vo.setAccountCode(vo.getAccountCodeList().get(i));
        		vo.setAmount(vo.getAmountList().get(i).replaceAll(",", ""));
        		vo.setReason(vo.getReasonList().get(i));
        		vo.setEtc(vo.getEtcList().get(i));

        		curriculumDAO.insertCurriculumExp(vo);

        	}
    	}

        return id;
    }

    /**
	 * curriculum을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    @SuppressWarnings("unchecked")
	public void updateCurriculum(CurriculumVO vo) throws Exception {
    	
    	//과정체계코드
    	String sysCode = "";
    	if(vo.getSysCodeList() != null){
	    	for(int i = 0; i < vo.getSysCodeList().size(); i++){
	    		if(!EgovStringUtil.isEmpty(vo.getSysCodeList().get(i))){
	    			sysCode = vo.getSysCodeList().get(i);
	    		}
	    	}
	    	vo.setSysCode(sysCode);
    	}

    	//금액부분정리
    	if(vo.getTuitionFees() != null){
    		vo.setTuitionFees(vo.getTuitionFees().replaceAll(",", ""));
    	}
    	if(vo.getRegistrationFees() != null){
    		vo.setRegistrationFees(vo.getRegistrationFees().replaceAll(",", ""));
    	}

    	curriculumDAO.updateCurriculum(vo);

    	
    	//책임권한
    	if(vo.getUserIdList() != null){
    		
    		
    		//초기화 시 
    		// curriculummember -> curriculummanage -> curriculumgroup 순으로 삭제 후 
    		// curriculummanage -> curriculumgroup -> curriculummember 다시 넣어줘야 한다.
    		// curriculummember 가 curriculummanage, curriculumgroup 테이블에 대하여 제약조건이 걸려있음
    		List<EgovMap> list = null;
    		list = curriculumDAO.selectCurriculumMemberList(vo);
    		
    		// 교육과정 수강신청 인원이 있는 경우
    		if(list != null && list.size() > 0) {
    			
    			// 임시테이블에 교육과정 수강신청 멤버 use_flag = 'N'으로 처리
    			curriculumDAO.updateCurriculumMemberHistory(vo);
    			
    			// 임시테이블에 교육과정 수강신청 멤버 저장
    			curriculumDAO.insertCurriculumMemberHistory(vo);
    			
    			// 교육과정 수강신청 멤버 삭제
    			curriculumDAO.deleteCurriculumMemberList(vo);
    		}
    		
    		
    		//초기화 - 책임권한
    		vo.setManageCode("10");
    		curriculumDAO.deleteCurriculumMng(vo);
    		
    		//과정 그룹 삭제
    		curriculumDAO.deleteCurriculumGroup(vo);
    		
    		for(int i = 0; i < vo.getUserIdList().size(); i++){
        		vo.setUserId(vo.getUserIdList().get(i));
        		curriculumDAO.insertCurriculumMng(vo);
        		
        		//과정 그룹 등록
        		if(i == 0) {
        	    	curriculumDAO.insertCurriculumGroup(vo);
        		}
        	}
    		
    		// 과정 수강 인원 삽입
    		if(list != null && list.size() > 0) {
    			curriculumDAO.insertCurriculumMemberAdd(vo);
    		}
    		
    	}

    	//부책임권한
    	if(vo.getUserIdList2() != null){
    		//초기화 - 부책임권한
    		vo.setManageCode("08");
    		curriculumDAO.deleteCurriculumMng(vo);
    		for(int i = 0; i < vo.getUserIdList2().size(); i++){
        		vo.setUserId(vo.getUserIdList2().get(i));
        		curriculumDAO.insertCurriculumMng(vo);
        	}
    	}

    	//사업비
    	if(vo.getTypeCodeList() != null){
    		//초기화
    		curriculumDAO.deleteCurriculumExp(vo);

    		for(int i = 0; i < vo.getTypeCodeList().size(); i++){
        		String expId = expIdGnrService.getNextStringId();
        		vo.setExpId(expId);
        		vo.setTypeCode(vo.getTypeCodeList().get(i));
        		vo.setAccountCode(vo.getAccountCodeList().get(i));
        		vo.setAmount(vo.getAmountList().get(i).replaceAll(",", ""));
        		vo.setReason(vo.getReasonList().get(i));
        		vo.setEtc(vo.getEtcList().get(i));

        		curriculumDAO.insertCurriculumExp(vo);

        	}
    	}

    	//학습내용
/*    	if(vo.getChasiList() != null){
    		//초기화
    		curriculumDAO.deleteCurriculumLesson(vo);

    		for(int i = 0; i < vo.getChasiList().size(); i++){
        		vo.setChasiNm(vo.getChasiList().get(i));
        		vo.setLessonNm(vo.getLessonNmList().get(i));

        		curriculumDAO.insertCurriculumLesson(vo);
        	}
    	}*/
    	
    	String lessonContentList ="";
    	lessonContentList = vo.getLessonContentList();
    	
    	String[] lessonList = null;
    	if(lessonContentList != null && !StringUtils.isEmpty(lessonContentList) && lessonContentList != "") {
    		lessonList = lessonContentList.split("#first#");
    	}
    	
    	CurriculumVO curriculumVO;
    	
    	if(lessonList != null && lessonList.length > 0) {
    		
    		curriculumDAO.deleteCurriculumLesson(vo);
    		for(int i=0 ; i < lessonList.length ; i++) {
    			curriculumVO = new CurriculumVO();
    			curriculumVO.setCrclId(vo.getCrclId());
    			
    			String lessonNm = lessonList[i].split("#second#")[1];
    			int lessonOrder = Integer.parseInt(lessonList[i].split("#second#")[0]);
    			curriculumVO.setLessonNm(lessonNm);
    			curriculumVO.setLessonOrder(lessonOrder);

    			String[] innerChasiNm = lessonList[i].split("#second#")[2].split("#third#");
    			if(innerChasiNm != null && innerChasiNm.length > 0) {
    				for(int j=0 ; j < innerChasiNm.length ; j++) {
    					
    					if(innerChasiNm[j].equals("emptyContentChasiNm")) {
    						curriculumVO.setChasiNm("");
    						curriculumVO.setChasiOrder(j+1);
    						
    					} else {
    						curriculumVO.setChasiNm(innerChasiNm[j]);
    						curriculumVO.setChasiOrder(j+1);
    					}
    					
    					curriculumDAO.insertCurriculumLesson(curriculumVO);
    					
    				}
    			} else {
    				curriculumVO.setChasiNm("");
    				curriculumVO.setChasiOrder(1);
    				curriculumDAO.insertCurriculumLesson(curriculumVO);
    				
    			}
    		}
    		
    	}

    	//교원배정
    	if(vo.getSisuList() != null){
    		//초기화
    		curriculumDAO.deleteCurriculumFaculty(vo);
    		for(int i = 0; i < vo.getSisuList().size(); i++){
        		vo.setFacId(vo.getFacIdList().get(i));
        		vo.setFacNm(vo.getFacNmList().get(i));
        		vo.setSisu(vo.getSisuList().get(i));
        		vo.setPosition(vo.getPositionList().get(i));

        		curriculumDAO.insertCurriculumFaculty(vo);
        	}
    	}

    	//교육과정 총괄평가기준
    	if(vo.getEvtIdList() != null){
    		//초기화
    		curriculumDAO.deleteTotalEvaluation(vo);
    		for(int i = 0; i < vo.getEvtIdList().size(); i++){
    			if(!EgovStringUtil.isEmpty(vo.getEvtValList().get(i)) && !"0".equals(vo.getEvtValList().get(i))){
    				vo.setEvtId(vo.getEvtIdList().get(i));
            		vo.setEvtNm(vo.getEvtNmList().get(i));
            		vo.setEvtVal(vo.getEvtValList().get(i));
            		vo.setEvtStand(vo.getEvtStandList().get(i));
      			  	curriculumDAO.insertTotalEvaluation(vo);

      			  	//수업참여 게시판
      			  	if("CTG_0000000000000117".equals(vo.getEvtIdList().get(i))){
      			  		if(vo.getBbsIdList() != null){
    		  			  	for(int a = 0; a < vo.getBbsIdList().size(); a++){
    		  			  		vo.setBbsId(vo.getBbsIdList().get(a));
    		  			  		vo.setColect(vo.getColectList().get(a));
    		  			  		curriculumDAO.insertAttendbbs(vo);
    		  			  	}
      			  		}
      			  	}
    			}
        	}
    	}

    	//교재 및 부교재
    	if(vo.getNttNoList() != null){
    		//초기화
    		curriculumDAO.deleteBookbbs(vo);
    		for(int i = 0; i < vo.getNttNoList().size(); i++){
        		vo.setNttNo(vo.getNttNoList().get(i));

        		curriculumDAO.insertBookbbs(vo);
        	}
    	}

    }

    /**
	 * curriculum을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void deleteCurriculum(CurriculumVO vo) throws Exception {
        curriculumDAO.deleteCurriculum(vo);
    }

    /**
	 * curriculum을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumVO
	 * @return 조회한 curriculum
	 * @exception Exception
	 */
    public CurriculumVO selectCurriculum(CurriculumVO vo) throws Exception {
        CurriculumVO resultVO = curriculumDAO.selectCurriculum(vo);
        if (resultVO == null)
            throw processException("info.nodata.msg");
        return resultVO;
    }

    /**
	 * curriculum 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		if("Y".equals(searchVO.getSearchMyCulAt())){
    			searchVO.setSearchFrstRegisterId(user.getId());
    		}
    		//searchVO.setUserSeCode(user.getUserSeCode());
    	}
    	/*
    	//본교생이 아닐 경우 학부전공연계과정은 제외
    	if(searchVO.getUserSeCode() == null || Integer.parseInt(searchVO.getUserSeCode()) < 6){
    		searchVO.setSearchTargetType("N");
    	}
    	*/
    	//승인된 교육과정만 불러오도록 처리
    	if(EgovStringUtil.isEmpty(searchVO.getSearchAprvalAt()) && !"Y".equals(searchVO.getMngAt())){
    		searchVO.setSearchAprvalAt("Y");
    	}

        return curriculumDAO.selectCurriculumList(searchVO);
    }

    /**
	 * curriculum 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 총 갯수
	 * @exception
	 */
    public int selectCurriculumListTotCnt(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) {
    	/*LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setUserSeCode(user.getUserSeCode());
    	}

    	//본교생이 아닐 경우 학부전공연계과정은 제외
    	if(searchVO.getUserSeCode() == null || Integer.parseInt(searchVO.getUserSeCode()) < 6){
    		searchVO.setSearchTargetType("N");
    	}*/

    	//승인된 교육과정만 불러오도록 처리
    	if(EgovStringUtil.isEmpty(searchVO.getSearchAprvalAt()) && !"Y".equals(searchVO.getMngAt())){
    		searchVO.setSearchAprvalAt("Y");
    	}

		return curriculumDAO.selectCurriculumListTotCnt(searchVO);
	}

    // 과정후기 목록 조회
    public List<?> selectCourseReviewList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		if("Y".equals(searchVO.getSearchMyCulAt())){
    			searchVO.setSearchFrstRegisterId(user.getId());
    		}
    		//searchVO.setUserSeCode(user.getUserSeCode());
    	}
    	/*
    	//본교생이 아닐 경우 학부전공연계과정은 제외
    	if(searchVO.getUserSeCode() == null || Integer.parseInt(searchVO.getUserSeCode()) < 6){
    		searchVO.setSearchTargetType("N");
    	}
    	 */
    	//승인된 교육과정만 불러오도록 처리
    	if(EgovStringUtil.isEmpty(searchVO.getSearchAprvalAt()) && !"Y".equals(searchVO.getMngAt())){
    		searchVO.setSearchAprvalAt("Y");
    	}
    	
    	return curriculumDAO.selectCourseReviewList(searchVO);
    }
    
    // 과정후기 공지 목록 조회
    public List<?> selectCourseReviewNoticeList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		if("Y".equals(searchVO.getSearchMyCulAt())){
    			searchVO.setSearchFrstRegisterId(user.getId());
    		}
    		//searchVO.setUserSeCode(user.getUserSeCode());
    	}
    	/*
    	//본교생이 아닐 경우 학부전공연계과정은 제외
    	if(searchVO.getUserSeCode() == null || Integer.parseInt(searchVO.getUserSeCode()) < 6){
    		searchVO.setSearchTargetType("N");
    	}
    	 */
    	//승인된 교육과정만 불러오도록 처리
    	if(EgovStringUtil.isEmpty(searchVO.getSearchAprvalAt()) && !"Y".equals(searchVO.getMngAt())){
    		searchVO.setSearchAprvalAt("Y");
    	}
    	
    	return curriculumDAO.selectCourseReviewNoticeList(searchVO);
    }
    
    // 과정후기 목록 갯수
    public int selectCourseReviewCnt(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) {
    	/*LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setUserSeCode(user.getUserSeCode());
    	}

    	//본교생이 아닐 경우 학부전공연계과정은 제외
    	if(searchVO.getUserSeCode() == null || Integer.parseInt(searchVO.getUserSeCode()) < 6){
    		searchVO.setSearchTargetType("N");
    	}*/
    	
    	//승인된 교육과정만 불러오도록 처리
    	if(EgovStringUtil.isEmpty(searchVO.getSearchAprvalAt()) && !"Y".equals(searchVO.getMngAt())){
    		searchVO.setSearchAprvalAt("Y");
    	}
    	
    	return curriculumDAO.selectCourseReviewCnt(searchVO);
    }
    
    //사업비
    public List selectCurriculumExpense(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectCurriculumExpense(vo);
    }

    //과정 일부만 수정
    public void updateCurriculumPart(CurriculumVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//첨부파일
    	if(!EgovStringUtil.isEmpty(vo.getFileGroupId())){
    		FileVO fvo = new FileVO();
        	fvo.setAtchFileId(vo.getAtchFileId());
    		fvo.setFileGroupId(vo.getFileGroupId());
    		String atchFileId = fileMngService.insertFileInfsByTemp(fvo, request, response).getAtchFileId();
    		vo.setAtchFileId(atchFileId);
    	}

    	curriculumDAO.updateCurriculumPart(vo);
    }


    //프로세스 업데이트
    public void updatePsCodeCurriculum(CurriculumVO vo) throws Exception {
    	//과정들이 생기는 로직 추가 작업 해야됨
    	if("1".equals(vo.getProcessSttusCode())){
    		//카테고리 마스터 등록
    		CtgryMaster ctgryMaster = new CtgryMaster();
    		ctgryMaster.setSiteId(vo.getCrclId());
    		ctgryMaster.setCtgrymasterNm(vo.getCrclId());

    		CtgryMaster master = egovBBSCtgryMasterService.selectCtgrymasterId(ctgryMaster);
    		if(master == null){
    			String ctgryMasterId = egovBBSCtgryMasterService.insertComtnbbsctgrymaster(ctgryMaster);

        		//게시판 마스터 등록
        		BoardMasterVO boardMasterVO = new BoardMasterVO();
        		boardMasterVO.setCtgrymasterId(ctgryMasterId);
        		boardMasterVO.setSiteId(vo.getCrclId());
        		boardMasterVO.setSysTyCode("ALL");
        		boardMasterVO.setBbsNm("전체 게시판");
        		boardMasterVO.setCommentUseAt("N");
        		boardMasterVO.setReplyPosblAt("N");
        		boardMasterVO.setFileAtchPosblAt("Y");
        		boardMasterVO.setPosblAtchFileNumber("10");
        		boardMasterVO.setPosblAtchFileSize("1024");
        		boardMasterVO.setInqireAuthor("02");
        		boardMasterVO.setRegistAuthor("10");
        		boardMasterVO.setAnswerAuthor("02");
        		boardMasterVO.setOthbcUseAt("N");
        		boardMasterVO.setSourcId(propertiesService.getString("Crcl.sourcId"));
        		boardMasterVO.setTmplatId(propertiesService.getString("Crcl.tmplatId"));
        		boardMasterVO.setSvcAt("Y");
        		boardMasterVO.setFrstRegisterId(vo.getLastUpdusrId());
        	    bbsAttrbService.insertBBSMastetInf(boardMasterVO);
    		}
    	}else if("0".equals(vo.getProcessSttusCode())){
    		//카테고리 마스터 삭제
    		CtgryMaster ctgryMaster = new CtgryMaster();
    		ctgryMaster.setSiteId(vo.getCrclId());
    		ctgryMaster.setCtgrymasterNm(vo.getCrclId());

    		CtgryMaster master = egovBBSCtgryMasterService.selectCtgrymasterId(ctgryMaster);
    		egovBBSCtgryMasterService.deleteComtnbbsctgrymaster(master);

    		BoardMasterVO boardMasterVO = new BoardMasterVO();
    		boardMasterVO.setSiteId(vo.getCrclId());
    		boardMasterVO.setSysTyCode("ALL");
    		boardMasterVO.setLastUpdusrId(vo.getLastUpdusrId());
    		bbsAttrbService.deleteBBSMasterForSiteId(boardMasterVO);

    	}

        curriculumDAO.updatePsCodeCurriculum(vo);
    }

    //학습내용
    public List selectCurriculumLesson(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectCurriculumLesson(vo);
    }

    //과정 사용자(관리자) 목록
    public List selectCurriculumMng(CurriculumVO vo) throws Exception {
    	
    	System.out.println(vo.getManageCode());
        return curriculumDAO.selectCurriculumMng(vo);
    }

    //교원배정 목록
    public List selectCurriculumFaculty(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectCurriculumFaculty(vo);
    }

    //교원배정(중복제거) 목록
    public List selectCurriculumFacultyDp(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectCurriculumFacultyDp(vo);
    }

    //총괄평가 목록
    public List selectTotalEvaluation(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectTotalEvaluation(vo);
    }

    //수업참여도 목록
    public List selectAttendbbs(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectAttendbbs(vo);
    }

    //교재 및 부교재 목록
    public List selectBookbbs(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectBookbbs(vo);
    }

	//수강신청 등록
    public void insertCurriculumMember(CurriculumVO vo) throws Exception {
    	curriculumDAO.deleteCurriculumMember(vo);

    	curriculumDAO.insertCurriculumMember(vo);
    }

    //수강신청 첨부 서류 재처리
    public void updateCurriculumMember(CurriculumVO vo) throws Exception {
    	curriculumDAO.updateCurriculumMember(vo);
    }

	//중복 수강신청 조회
    public int selectCurriculumDuplicationMemberCnt(CurriculumVO searchVO) {
		return curriculumDAO.selectCurriculumDuplicationMemberCnt(searchVO);
    }

    //수강신청 조회
    public CurriculumVO selectCurriculumMemberDetail(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectCurriculumMemberDetail(vo);
    }

    // 오늘의 수업
    public List<?> selectTodayCrclList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectTodayCrclList(searchVO);
    }

    /**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return homework 총 갯수
	 * @exception
	 */
    public int selectHomeworkTotCnt(CurriculumVO searchVO) {
		return curriculumDAO.selectHomeworkTotCnt(searchVO);
	}

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return homework 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectHomeworkList(searchVO);
    }

    /**
	 * 과제 제출 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkSubmitWaitingList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectHomeworkSubmitWaitingList(searchVO);
    }

    /**
	 * 과제 평가 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkTestWaitingList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectHomeworkTestWaitingList(searchVO);
    }

    /**
	 * 과제를 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 등록
	 * @exception Exception
	 */
    public void insertHomeworkArticle(CurriculumVO searchVO) throws Exception {
		if("".equals(searchVO.getHwId())) {
			searchVO.setHwId(idgenService.getNextStringId());
		}

		searchVO.setAtchFileId(this.insertFileInfsByTemp(searchVO));

		curriculumDAO.insertHomeworkArticle(searchVO);
    }

    /**
     * 임시첨부파일을 정식으로 등록 한다.
     *
     */
    public String insertFileInfsByTemp(CurriculumVO searchVO) throws Exception {
  	    FileVO fvo = new FileVO();
  	    fvo.setAtchFileId(searchVO.getAtchFileId());
  	    fvo.setFileGroupId(searchVO.getFileGroupId());

  	    return fileMngService.insertFileInfsByTemp(fvo).getAtchFileId();
    }

    /**
	 * 과제 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public CurriculumVO selectHomeworkArticle(CurriculumVO searchVO) throws Exception {
        return (CurriculumVO)curriculumDAO.selectHomeworkArticle(searchVO);
    }

    /**
	 * 과제를 삭제한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public void deleteHomeworkArticle(CurriculumVO searchVO) throws Exception {
        curriculumDAO.deleteHomeworkArticle(searchVO);
    }

    /**
	 * 과제를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateHomeworkArticle(CurriculumVO searchVO) throws Exception {
    	String atchFileId = this.insertFileInfsByTemp(searchVO);
    	searchVO.setAtchFileId(atchFileId);

		curriculumDAO.updateHomeworkArticle(searchVO);
    }

    /**
	 * 학생 공개를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkSubjectList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectHomeworkSubjectList(searchVO);
    }

    /**
	 * 학생 공개를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public void updateStuOpenAt(CurriculumVO searchVO) throws Exception {
    	curriculumDAO.updateStuOpenAt(searchVO);
    }

    /**
	 * 과제 후기 선정을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public void updateCommentPickAt(CurriculumVO searchVO) throws Exception {
    	curriculumDAO.updateCommentPickAt(searchVO);
    }

    /**
	 * 과제 피드백을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public void updateFdb(CurriculumVO searchVO) throws Exception {
    	curriculumDAO.updateFdb(searchVO);
    }

    /**
	 * 과제 제출 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    public CurriculumVO selectHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception {
        return (CurriculumVO)curriculumDAO.selectHomeworkSubmitArticle(searchVO);
    }
    
    // 과정후기 상세
    public CurriculumVO selectCourseReviewAtView(CurriculumVO searchVO) throws Exception {
    	return (CurriculumVO)curriculumDAO.selectCourseReviewAtView(searchVO);
    }

    /**
	 * 과제 제출을 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 등록
	 * @exception Exception
	 */
    public void insertHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception {
		if("".equals(searchVO.getHwsId())) {
			searchVO.setHwsId(idgenServiceHomeworkSubmit.getNextStringId());
		}

		searchVO.setAtchFileId(this.insertFileInfsByTemp(searchVO));

		curriculumDAO.insertHomeworkSubmitArticle(searchVO);
    }

    /**
	 * 과제 제출을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 수정
	 * @exception Exception
	 */
    public void updateHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception {
    	String atchFileId = this.insertFileInfsByTemp(searchVO);
    	searchVO.setAtchFileId(atchFileId);

		curriculumDAO.updateHomeworkSubmitArticle(searchVO);
    }

    // 과정후기를 등록한다
    public void insertCourseReview(CurriculumVO searchVO) throws Exception {
    	if("".equals(searchVO.getHwsId())) {
    		searchVO.setHwsId(idgenServiceHomeworkSubmit.getNextStringId());
    	}
    	
    	searchVO.setAtchFileId(this.insertFileInfsByTemp(searchVO));
    	
    	curriculumDAO.insertCourseReview(searchVO);
    }
    
    // 과정후기를 수정한다.
    public void updateCourseReview(CurriculumVO searchVO) throws Exception {
    	String atchFileId = this.insertFileInfsByTemp(searchVO);
    	searchVO.setAtchFileId(atchFileId);
    	
    	curriculumDAO.updateCourseReview(searchVO);
    }
    
    // 과정후기를 삭제한다.(use_at > N 처리)
    public void deleteCourseReview(CurriculumVO searchVO) throws Exception {
    	String atchFileId = this.insertFileInfsByTemp(searchVO);
    	searchVO.setAtchFileId(atchFileId);
    	
    	curriculumDAO.deleteCourseReview(searchVO);
    }
    //과제평가
    public List homeworkScoreList(CurriculumVO searchVO) throws Exception {
    	return curriculumDAO.homeworkScoreList(searchVO);
    }

    //과제평가 - 학생 점수 요약
    public List homeworkScoreSum(CurriculumVO searchVO) throws Exception {
    	return curriculumDAO.homeworkScoreSum(searchVO);
    }

    //과제평가 - 성적반영
    public void updateScoreApply(CurriculumVO searchVO) throws Exception {
    	//초기 삭제
    	curriculumDAO.deleteScoreApply(searchVO);
    	for(int i = 0; i < searchVO.getHwIdList().size(); i++){
    		searchVO.setHwId(searchVO.getHwIdList().get(i));
    		searchVO.setScoreApplyAt("Y");
    		curriculumDAO.updateScoreApply(searchVO);
    	}
    }

    /**
	 * 반 개수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 총 갯수
	 * @exception
	 */
    public int selectCurriculumMemberGroupCnt(CurriculumVO searchVO) {
		return curriculumDAO.selectCurriculumMemberGroupCnt(searchVO);
	}

    // My > 교육과정이력 조회
    public List selectMyCurriculumHistoryList(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectMyCurriculumHistoryList(vo);
    }

    // My > 교육과정이력 조회 count
    public List selectMyCurriculumHistoryListCount(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectMyCurriculumHistoryListCount(vo);
    }

    // My > 교육과정이력 - 총 과정수, 과정중 count
    public int selectMyCurriculumHistoryCnt(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectMyCurriculumHistoryCnt(vo);
    }

    //학생 과제 공개 개수
    public int selectHomeworkCommentPickAtCnt(CurriculumVO searchVO) {
		return curriculumDAO.selectHomeworkCommentPickAtCnt(searchVO);
    }

    /**
	 * 학생 과제 공개 리스트를 조회.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    public List<?> selectHomeworkCommentPickAtList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectHomeworkCommentPickAtList(searchVO);
    }

    public List<EgovMap> selectReportList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectReportList(searchVO);
    }

	public int selectReportListCnt(CurriculumVO searchVO) throws Exception {
		return curriculumDAO.selectReportListCnt(searchVO);
	}

	 public List<EgovMap> selectAdminReportList(CurriculumVO searchVO) throws Exception {
        return curriculumDAO.selectAdminReportList(searchVO);
    }

	public int selectAdminReportListCnt(CurriculumVO searchVO) throws Exception {
		return curriculumDAO.selectAdminReportListCnt(searchVO);
	}

    //교육과정 통계(운영보고서)
    public EgovMap curriculumSts(CurriculumVO vo) throws Exception{
    	return curriculumDAO.curriculumSts(vo);
    }
    
    //과정 목록 - 학생기준
    public List<EgovMap> selectMyCurriculumList(CurriculumVO vo) throws Exception {
        return curriculumDAO.selectMyCurriculumList(vo);
    }
    
    //과정 목록 수 - 학생기준
    public int selectMyCurriculumListCnt(CurriculumVO vo) throws Exception {
    	return curriculumDAO.selectMyCurriculumListCnt(vo);
    }

	public void downMemberZipFile(HashMap<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		
	}
    
    /*    @Override
   public void downMemberZipFile(HashMap<String, Object> map )throws Exception {
    	
	}*/
	
	//과정 진행 단계 체크
	public EgovMap curriculumStatusCheck(CurriculumVO vo) throws Exception {
		return curriculumDAO.curriculumStatusCheck(vo);
	}
	
	//설문 참여 체크
	public EgovMap curriculumSurveyCheck(CurriculumVO vo) throws Exception {
		return curriculumDAO.curriculumSurveyCheck(vo);
	}
    	
    
}
