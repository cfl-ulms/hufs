package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : BasefileVO.java
 * @Description : Basefile VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class BasefileVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** ATCH_FILE_ID */
    private java.lang.String atchFileId;
    
    /** CTGRY_ID */
    private java.lang.String ctgryId;
    
    /** UPPER_CTGRY_ID */
    private java.lang.String upperCtgryId;
    
    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;
    
    /** FRST_REGISTER_PNTTM */
    private java.lang.String frstRegisterPnttm;
    
    /** 카테고리 리스트 */
    private List<String> ctgryIdList;
    
    public java.lang.String getAtchFileId() {
        return this.atchFileId;
    }
    
    public void setAtchFileId(java.lang.String atchFileId) {
        this.atchFileId = atchFileId;
    }
    
    public java.lang.String getCtgryId() {
        return this.ctgryId;
    }
    
    public void setCtgryId(java.lang.String ctgryId) {
        this.ctgryId = ctgryId;
    }
    
    public java.lang.String getUpperCtgryId() {
        return this.upperCtgryId;
    }
    
    public void setUpperCtgryId(java.lang.String upperCtgryId) {
        this.upperCtgryId = upperCtgryId;
    }
    
    public java.lang.String getFrstRegisterId() {
        return this.frstRegisterId;
    }
    
    public void setFrstRegisterId(java.lang.String frstRegisterId) {
        this.frstRegisterId = frstRegisterId;
    }
    
    public java.lang.String getFrstRegisterPnttm() {
        return this.frstRegisterPnttm;
    }
    
    public void setFrstRegisterPnttm(java.lang.String frstRegisterPnttm) {
        this.frstRegisterPnttm = frstRegisterPnttm;
    }

	public List<String> getCtgryIdList() {
		return ctgryIdList;
	}

	public void setCtgryIdList(List<String> ctgryIdList) {
		this.ctgryIdList = ctgryIdList;
	}

    
}
