package egovframework.com.lms.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.cmm.service.FileVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

public interface CertificateService {
	
    //수료증등록
    void insertCertificate(CertificateVO vo) throws Exception;
    
	//재발급
    void reCertificate(CertificateVO vo) throws Exception;
	
    //주료증 목록
    List selectCertificateList(CertificateVO searchVO) throws Exception;
    
    //주료증 목록 수
    int selectCertificateListCnt(CertificateVO searchVO) throws Exception;
    
    //수료증 조회
    EgovMap selectCertificate(CertificateVO vo) throws Exception;
    
	//수료증 체크
	EgovMap certificateCheck(CertificateVO vo) throws Exception;
	
	//수료증 발급 이력 등록
	void insertCertificateHst(CertificateVO vo) throws Exception;
	
	//수료증 체크 후 과정만족도 필요한 경우 
	EgovMap selectMySurveyDetail(SurveyVO vo) throws Exception;
}
