package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : quizVO.java
 * @Description : quizVO VO class
 * @Modification Information
 *
 * @author 이재현
 * @since 2020-0-25
 * @version 1.0
 * @ljh
 *  
 *  Copyright (C)  All right reserved.
 */
public class QuizVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    private String quizId;
    private String crclId;
    private String crclNm;
    private String plId;
    private String plIdTarget;
    private String title;
    private String quizNm;
    private String atchFileId;
    private int    quizSort;
    private String userId;
    private String userNm;     
    private String examId;
    private String examType;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private int    subAnswer1;
    private int    subAnswer1p;
    private int    subAnswer2;
    private int    subAnswer2p;
    private int    subAnswer3;
    private int    subAnswer3p;
    private int    subAnswer4;
    private int    subAnswer4p;
    private int    subAnswer5;
    private int    subAnswer5p;
    
    private String answerNum;
    private String frstRegisterPnttm;
    private String frstRegisterId;
    private String lastUpdusrPnttm;
    private String lastUpdusrId;
    private String menuId;
    private String siteId;
    private String boardId;
    private String bbsId;
    private String useAt;
    private String delDiv;
    private String sortDiv;
    private String startDate;
    private String endDate;
    private String studySubject;
    private String plStartDt;
	private String regDate;
	private String searchDiv;
    private int    quizCnt;
    private String searchYY;
    private String searchQuizNm;
    private String quizType;
    private String dayOfWeek;
    private String periodTxt;
    private String startDt;
    private String pageDiv;
    private String crclLangNm;
    private String crclLang;
    private String examineeId;
    private String examineeAns;
    private String answerAt;
    private String submitAt;
    private String userSe;
    private int    myScore;
    private int    totCnt;
    private int    avgScoreTmp;
    private int    avgTotCntTmp;
    private int    examineeCnt;
    private int    avgScoreCnt;
    private int    avgTotCnt;
    private int    totExamineeCnt;
    private String brthdy;
    private String sexdstn;
    private String stGrade;
    private String stClass;
    private String stNumber;
    private String groupCode;
    private String groupNm;
    private String major;
    private int    myScoreNum;
    private int    ranking;
    private int    answerCnt;
    private int    notSubCnt;
    private String searchVal;
    
    private String step;
    private String tabType;
    private String subtabstep;
    private String tabStep;
    private int    quizAnswerYCnt;
    private int    quizAnswerNCnt;
    private String facNm;
    private String searchStartDt;
    private String searchEndDt;
    private String quizEnd;
    
    
    //수강신청 > 수강대상자 확정에 3번째 텝
    private java.lang.String thirdtabstep;
    
    //검색 학생 명
    private String searchStudentUserNm;
    
    //검색 학생 명
    private String searchUserId;
    
    //변환점수
    private java.lang.Double chScr;
    
	public String getQuizEnd() {
		return quizEnd;
	}
	public void setQuizEnd(String quizEnd) {
		this.quizEnd = quizEnd;
	}
	public String getSearchStartDt() {
		return searchStartDt;
	}
	public void setSearchStartDt(String searchStartDt) {
		this.searchStartDt = searchStartDt;
	}
	public String getSearchEndDt() {
		return searchEndDt;
	}
	public void setSearchEndDt(String searchEndDt) {
		this.searchEndDt = searchEndDt;
	}
	public String getFacNm() {
		return facNm;
	}
	public void setFacNm(String facNm) {
		this.facNm = facNm;
	}
	public String getPlIdTarget() {
		return plIdTarget;
	}
	public void setPlIdTarget(String plIdTarget) {
		this.plIdTarget = plIdTarget;
	}
	public int getQuizAnswerYCnt() {
		return quizAnswerYCnt;
	}
	public void setQuizAnswerYCnt(int quizAnswerYCnt) {
		this.quizAnswerYCnt = quizAnswerYCnt;
	}
	public int getQuizAnswerNCnt() {
		return quizAnswerNCnt;
	}
	public void setQuizAnswerNCnt(int quizAnswerNCnt) {
		this.quizAnswerNCnt = quizAnswerNCnt;
	}
	public int getSubAnswer1() {
		return subAnswer1;
	}
	public void setSubAnswer1(int subAnswer1) {
		this.subAnswer1 = subAnswer1;
	}
	public int getSubAnswer1p() {
		return subAnswer1p;
	}
	public void setSubAnswer1p(int subAnswer1p) {
		this.subAnswer1p = subAnswer1p;
	}
	public int getSubAnswer2() {
		return subAnswer2;
	}
	public void setSubAnswer2(int subAnswer2) {
		this.subAnswer2 = subAnswer2;
	}
	public int getSubAnswer2p() {
		return subAnswer2p;
	}
	public void setSubAnswer2p(int subAnswer2p) {
		this.subAnswer2p = subAnswer2p;
	}
	public int getSubAnswer3() {
		return subAnswer3;
	}
	public void setSubAnswer3(int subAnswer3) {
		this.subAnswer3 = subAnswer3;
	}
	public int getSubAnswer3p() {
		return subAnswer3p;
	}
	public void setSubAnswer3p(int subAnswer3p) {
		this.subAnswer3p = subAnswer3p;
	}
	public int getSubAnswer4() {
		return subAnswer4;
	}
	public void setSubAnswer4(int subAnswer4) {
		this.subAnswer4 = subAnswer4;
	}
	public int getSubAnswer4p() {
		return subAnswer4p;
	}
	public void setSubAnswer4p(int subAnswer4p) {
		this.subAnswer4p = subAnswer4p;
	}
	public int getSubAnswer5() {
		return subAnswer5;
	}
	public void setSubAnswer5(int subAnswer5) {
		this.subAnswer5 = subAnswer5;
	}
	public int getSubAnswer5p() {
		return subAnswer5p;
	}
	public void setSubAnswer5p(int subAnswer5p) {
		this.subAnswer5p = subAnswer5p;
	}
	public String getSearchVal() {
		return searchVal;
	}
	public void setSearchVal(String searchVal) {
		this.searchVal = searchVal;
	}
	public int getNotSubCnt() {
		return notSubCnt;
	}
	public void setNotSubCnt(int notSubCnt) {
		this.notSubCnt = notSubCnt;
	}
	public int getAnswerCnt() {
		return answerCnt;
	}
	public void setAnswerCnt(int answerCnt) {
		this.answerCnt = answerCnt;
	}
	public int getTotExamineeCnt() {
		return totExamineeCnt;
	}
	public void setTotExamineeCnt(int totExamineeCnt) {
		this.totExamineeCnt = totExamineeCnt;
	}
	public String getBrthdy() {
		return brthdy;
	}
	public void setBrthdy(String brthdy) {
		this.brthdy = brthdy;
	}
	public String getSexdstn() {
		return sexdstn;
	}
	public void setSexdstn(String sexdstn) {
		this.sexdstn = sexdstn;
	}
	public String getStGrade() {
		return stGrade;
	}
	public void setStGrade(String stGrade) {
		this.stGrade = stGrade;
	}
	public String getStClass() {
		return stClass;
	}
	public void setStClass(String stClass) {
		this.stClass = stClass;
	}
	public String getStNumber() {
		return stNumber;
	}
	public void setStNumber(String stNumber) {
		this.stNumber = stNumber;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupNm() {
		return groupNm;
	}
	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public int getMyScoreNum() {
		return myScoreNum;
	}
	public void setMyScoreNum(int myScoreNum) {
		this.myScoreNum = myScoreNum;
	}
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public String getUserSe() {
		return userSe;
	}
	public void setUserSe(String userSe) {
		this.userSe = userSe;
	}
	public String getSubmitAt() {
		return submitAt;
	}
	public void setSubmitAt(String submitAt) {
		this.submitAt = submitAt;
	}
	public String getExamineeId() {
		return examineeId;
	}
	public void setExamineeId(String examineeId) {
		this.examineeId = examineeId;
	}
	public String getExamineeAns() {
		return examineeAns;
	}
	public void setExamineeAns(String examineeAns) {
		this.examineeAns = examineeAns;
	}
	public String getAnswerAt() {
		return answerAt;
	}
	public void setAnswerAt(String answerAt) {
		this.answerAt = answerAt;
	}
	public String getCrclLangNm() {
		return crclLangNm;
	}
	public void setCrclLangNm(String crclLangNm) {
		this.crclLangNm = crclLangNm;
	}
	public String getCrclLang() {
		return crclLang;
	}
	public void setCrclLang(String crclLang) {
		this.crclLang = crclLang;
	}
	public String getPageDiv() {
		return pageDiv;
	}
	public void setPageDiv(String pageDiv) {
		this.pageDiv = pageDiv;
	}
	public String getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	public String getPeriodTxt() {
		return periodTxt;
	}
	public void setPeriodTxt(String periodTxt) {
		this.periodTxt = periodTxt;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	
	public String getQuizType() {
		return quizType;
	}
	public void setQuizType(String quizType) {
		this.quizType = quizType;
	}
	public String getSearchDiv() {
		return searchDiv;
	}
	public void setSearchDiv(String searchDiv) {
		this.searchDiv = searchDiv;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStudySubject() {
		return studySubject;
	}
	public void setStudySubject(String studySubject) {
		this.studySubject = studySubject;
	}
	public String getPlStartDt() {
		return plStartDt;
	}
	public void setPlStartDt(String plStartDt) {
		this.plStartDt = plStartDt;
	}
	public int getQuizCnt() {
		return quizCnt;
	}
	public void setQuizCnt(int quizCnt) {
		this.quizCnt = quizCnt;
	}
	public String getSearchYY() {
		return searchYY;
	}
	public void setSearchYY(String searchYY) {
		this.searchYY = searchYY;
	}
	public String getSearchQuizNm() {
		return searchQuizNm;
	}
	public void setSearchQuizNm(String searchQuizNm) {
		this.searchQuizNm = searchQuizNm;
	}
	public String getSortDiv() {
		return sortDiv;
	}
	public void setSortDiv(String sortDiv) {
		this.sortDiv = sortDiv;
	}
	public String getDelDiv() {
		return delDiv;
	}
	public void setDelDiv(String delDiv) {
		this.delDiv = delDiv;
	}
	public String getUseAt() {
		return useAt;
	}
	public void setUseAt(String useAt) {
		this.useAt = useAt;
	}
	public String getBbsId() {
		return bbsId;
	}
	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPlId() {
		return plId;
	}
	public void setPlId(String plId) {
		this.plId = plId;
	}
	public String getQuizId() {
		return quizId;
	}
	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}
	public String getCrclId() {
		return crclId;
	}
	public void setCrclId(String crclId) {
		this.crclId = crclId;
	}
	public String getCrclNm() {
		return crclNm;
	}
	public void setCrclNm(String crclNm) {
		this.crclNm = crclNm;
	}
	public String getQuizNm() {
		return quizNm;
	}
	public void setQuizNm(String quizNm) {
		this.quizNm = quizNm;
	}
	public String getAtchFileId() {
		return atchFileId;
	}
	public void setAtchFileId(String atchFileId) {
		this.atchFileId = atchFileId;
	}
	public int getQuizSort() {
		return quizSort;
	}
	public void setQuizSort(int quizSort) {
		this.quizSort = quizSort;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getExamId() {
		return examId;
	}
	public void setExamId(String examId) {
		this.examId = examId;
	}
	public String getExamType() {
		return examType;
	}
	public void setExamType(String examType) {
		this.examType = examType;
	}
	public String getAnswer1() {
		return answer1;
	}
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}
	public String getAnswer2() {
		return answer2;
	}
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}
	public String getAnswer3() {
		return answer3;
	}
	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}
	public String getAnswer4() {
		return answer4;
	}
	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}
	public String getAnswer5() {
		return answer5;
	}
	public void setAnswer5(String answer5) {
		this.answer5 = answer5;
	}
	public String getAnswerNum() {
		return answerNum;
	}
	public void setAnswerNum(String answerNum) {
		this.answerNum = answerNum;
	}
	public String getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}
	public void setFrstRegisterPnttm(String frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}
	public String getFrstRegisterId() {
		return frstRegisterId;
	}
	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}
	public String getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}
	public void setLastUpdusrPnttm(String lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}
	public String getLastUpdusrId() {
		return lastUpdusrId;
	}
	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getMyScore() {
		return myScore;
	}
	public void setMyScore(int myScore) {
		this.myScore = myScore;
	}
	public int getTotCnt() {
		return totCnt;
	}
	public void setTotCnt(int totCnt) {
		this.totCnt = totCnt;
	}
	public int getAvgScoreTmp() {
		return avgScoreTmp;
	}
	public void setAvgScoreTmp(int avgScoreTmp) {
		this.avgScoreTmp = avgScoreTmp;
	}
	public int getAvgTotCntTmp() {
		return avgTotCntTmp;
	}
	public void setAvgTotCntTmp(int avgTotCntTmp) {
		this.avgTotCntTmp = avgTotCntTmp;
	}
	public int getExamineeCnt() {
		return examineeCnt;
	}
	public void setExamineeCnt(int examineeCnt) {
		this.examineeCnt = examineeCnt;
	}
	public int getAvgScoreCnt() {
		return avgScoreCnt;
	}
	public void setAvgScoreCnt(int avgScoreCnt) {
		this.avgScoreCnt = avgScoreCnt;
	}
	public int getAvgTotCnt() {
		return avgTotCnt;
	}
	public void setAvgTotCnt(int avgTotCnt) {
		this.avgTotCnt = avgTotCnt;
	}
	public String getStep() {
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}
	public String getTabType() {
		return tabType;
	}
	public void setTabType(String tabType) {
		this.tabType = tabType;
	}
	public String getSubtabstep() {
		return subtabstep;
	}
	public void setSubtabstep(String subtabstep) {
		this.subtabstep = subtabstep;
	}
	public String getTabStep() {
		return tabStep;
	}
	public void setTabStep(String tabStep) {
		this.tabStep = tabStep;
	}
	public java.lang.String getThirdtabstep() {
		return thirdtabstep;
	}
	public void setThirdtabstep(java.lang.String thirdtabstep) {
		this.thirdtabstep = thirdtabstep;
	}
	public String getSearchStudentUserNm() {
		return searchStudentUserNm;
	}
	public void setSearchStudentUserNm(String searchStudentUserNm) {
		this.searchStudentUserNm = searchStudentUserNm;
	}
	public String getSearchUserId() {
		return searchUserId;
	}
	public void setSearchUserId(String searchUserId) {
		this.searchUserId = searchUserId;
	}
	public java.lang.Double getChScr() {
		return chScr;
	}
	public void setChScr(java.lang.Double chScr) {
		this.chScr = chScr;
	}
	
	
}
