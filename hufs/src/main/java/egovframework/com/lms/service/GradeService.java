package egovframework.com.lms.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.cmm.service.FileVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

public interface GradeService {
	
    //성적등록
    void uploadExcel(CurriculumVO vo) throws Exception;
    
    //엑셀파일을 업로드 하여 파싱한다.
	Map<String, Object> parseExcel(CurriculumVO searchVO, FileVO fileVO);
	
	//성적등록
    void gradeTotalUpdate(CurriculumVO vo) throws Exception;
	
    //성적목록
    List selectGradeList(CurriculumVO searchVO) throws Exception;
    
    //수료정보취합
    public EgovMap selectGradeSummary(CurriculumVO vo) throws Exception;
}
