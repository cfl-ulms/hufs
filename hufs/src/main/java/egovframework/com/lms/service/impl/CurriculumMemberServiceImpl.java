package egovframework.com.lms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumMemberServiceImpl.java
 * @Description : Curriculum Business Implement class
 * @Modification Information
 *
 * @author 김용완
 * @since 2019.12.16
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Service("curriculumMemberService")
public class CurriculumMemberServiceImpl extends EgovAbstractServiceImpl implements CurriculumMemberService {

	@Resource(name="curriculumMemberDAO")
    private CurriculumMemberDAO curriculumMemberDAO;

	/**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 총 갯수
	 * @exception
	 */
    public int selectCurriculumMemberTotCnt(CurriculumVO searchVO) {
		return curriculumMemberDAO.selectCurriculumMemberTotCnt(searchVO);
	}

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumMemberList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectCurriculumMemberList(searchVO);
    }

    /**
	 * curriculumMember을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumAccept(CurriculumVO vo) throws Exception {
    	curriculumMemberDAO.updateCurriculumAccept(vo);
    }

    /**
	 * curriculumMember 통계 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 통계 목록
	 * @exception Exception
	 */
    public List<?> curriculumMemberStatsList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.curriculumMemberStatsList(searchVO);
    }

    /**
	 * 수강 대상자 통계 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumMember 통계 목록
	 * @exception Exception
	 */
    public List<?> selectStudentList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectStudentList(searchVO);
    }

    /**
	 * 수강 그룹 개수 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 개수
	 * @exception Exception
	 */
    public List<?> selectMemberGroupCnt(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectMemberGroupCnt(searchVO);
    }

    /**
	 * 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    public List<?> selectGroupList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectGroupList(searchVO);
    }

    /**
	 * 조배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumGroup(CurriculumVO vo) throws Exception {
    	CurriculumVO curriculumVO = new CurriculumVO();

    	//반,조 초기화 처리
    	curriculumVO.setCrclId(vo.getCrclId());
		curriculumVO.setClassCnt(vo.getClassCnt());
    	curriculumMemberDAO.updateCurriculumGroupReset(curriculumVO);

    	//조배정 처리
    	if(vo.getGroupLeaderAtList() != null) {
    		for(int i=0;i<vo.getGroupCntList().size();i++) {
        		curriculumVO.setCrclId(vo.getCrclId());
        		curriculumVO.setUserId(vo.getUserIdList().get(i));
        		curriculumVO.setClassCnt(vo.getClassCnt());
        		curriculumVO.setGroupCnt(vo.getGroupCntList().get(i));
        		curriculumVO.setGroupLeaderAt(vo.getGroupLeaderAtList().get(i));

        		curriculumMemberDAO.updateCurriculumGroup(curriculumVO);
        	}
    	}
    }

    /**
	 * 선택한 수강 그룹 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 수강 그룹 목록
	 * @exception Exception
	 */
    public List<?> selectPickStudentList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectPickStudentList(searchVO);
    }

    /**
	 * 조의 개수 리스트를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculummember 총 갯수
	 * @exception
	 */
    public List<?> selectCurriculumGroupCntList(CurriculumVO searchVO) {
		return curriculumMemberDAO.selectCurriculumGroupCntList(searchVO);
	}

    /**
	 * curriculumgroup을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 목록
	 * @exception
	 */
    public List<?> selectCurriculumClassList(CurriculumVO searchVO) throws Exception {
        return curriculumMemberDAO.selectCurriculumClassList(searchVO);
    }

    /**
	 * 반의 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumgroup 총 갯수
	 * @exception
	 */
    public int selectCurriculumClassTotCnt(CurriculumVO searchVO) {
		return curriculumMemberDAO.selectCurriculumClassTotCnt(searchVO);
	}

    /**
	 * 반배정 처리.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumClass(CurriculumVO vo) throws Exception {
    	CurriculumVO ClassVO = new CurriculumVO();
    	CurriculumVO curriculumVO = new CurriculumVO();

    	//학생 정보에 반 정보 초기화
    	curriculumVO.setCrclId(vo.getCrclId());
    	curriculumMemberDAO.updateCurriculumClassReset(curriculumVO);

    	//반 정보 삭제
    	ClassVO.setCrclId(vo.getCrclId());
    	curriculumMemberDAO.deleteCurriculumManage(ClassVO);

    	//반 정보 등록
    	for(int i=0;i<vo.getManageClassCntList().size();i++) {
    		ClassVO.setClassCnt(vo.getManageClassCntList().get(i));
    		ClassVO.setUserId(vo.getManageIdList().get(i));
    		curriculumMemberDAO.insertCurriculumManage(ClassVO);
    	}

    	//학생 정보에 반 정보 수정
    	if(vo.getClassCntList() != null) {
    		for(int i=0;i<vo.getClassCntList().size();i++) {
        		curriculumVO.setClassCnt(vo.getClassCntList().get(i));
        		curriculumVO.setUserId(vo.getUserIdList().get(i));
        		curriculumMemberDAO.updateCurriculumClass(curriculumVO);
        	}
    	}
    }

    /**
	 * 반 정보 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
	public List<?> selectCurriculumGroupList(CurriculumVO searchVO) throws Exception {
		return this.curriculumMemberDAO.selectCurriculumGroupList(searchVO);
	}
	
	/**
	 * 학생 상태별 개수 조회.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
	public List<?> selectMemberSttusCntList(CurriculumVO searchVO) throws Exception {
		return this.curriculumMemberDAO.selectMemberSttusCntList(searchVO);
	}
	
	/**
	 * 학생 반, 조 개수 리스트
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
	public List<?> selectMemberCntList(CurriculumVO searchVO) throws Exception {
		return this.curriculumMemberDAO.selectMemberCntList(searchVO);
	}

	// 수강신청 인원 일괄 승인
	public void updateSttusCurriculumMember(EgovMap egovMap) throws Exception {
		
		String[] paramUserIdArr = {};
		String paramUserId = "";
		
		paramUserId = egovMap.get("paramUserId").toString();
		paramUserIdArr = paramUserId.split(",");
		
		CurriculumVO vo = null;
		for(int i = 0 ; i < paramUserIdArr.length ; i++) {
			vo = new CurriculumVO();
			vo.setCrclId(egovMap.get("crclId").toString());
			vo.setLastUpdusrId(egovMap.get("lastUpdusrId").toString());
			vo.setUserId(paramUserIdArr[i]);
			curriculumMemberDAO.updateSttusCurriculumMember(vo);
		}
	}
	
	// 수강신청 인원 삭제
	public void delCurriculumMember(EgovMap egovMap) throws Exception {
		
		String[] paramUserIdArr = {};
		String paramUserId = "";
		
		paramUserId = egovMap.get("paramUserId").toString();
		paramUserIdArr = paramUserId.split(",");
		
		CurriculumVO vo = null;
		for(int i = 0 ; i < paramUserIdArr.length ; i++) {
			vo = new CurriculumVO();
			vo.setCrclId(egovMap.get("crclId").toString());
			vo.setUserId(paramUserIdArr[i]);
			curriculumMemberDAO.delCurriculumMember(vo);
		}
	}
	
	
}
