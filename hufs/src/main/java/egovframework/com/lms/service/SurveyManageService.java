package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.service.SearchVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumbaseService.java
 * @Description : Curriculumbase Business class
 * @Modification Information
 *
 * @author 이정현
 * @since 2019.11.17
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public interface SurveyManageService {

	EgovMap selectSurvey(SurveyVO surveyVo) throws Exception;

	void insertSurvey(SurveyVO surveyVo) throws Exception;

	SurveyVO selectSurveyInfo(SurveyVO surveyVO) throws Exception;

	void deleteSurvey(SurveyVO surveyVo) throws Exception;

	EgovMap selectCurriculumSurvey(SurveyVO surveyVo) throws Exception;

	public List<EgovMap> selectCurriculumSurveyAnswer(SurveyVO surveyVo) throws Exception;

	EgovMap selectCurriculumAddInfo(SurveyVO surveyVo) throws Exception;
	
	//과정만족도 - 교원
	EgovMap selectCurriculumAddInfoType3(SurveyVO surveyVo) throws Exception;
	
	public List<EgovMap> selectSurveyMember(SurveyVO surveyVo) throws Exception;
	
	//설문 제출 현황 목록
    EgovMap selectSurveyMemberSummary(SurveyVO surveyVO) throws Exception;
	
	public List<EgovMap> selectCurriculumSurveyList(SurveyVO surveyVo) throws Exception;

	public int selectAnswerSurveyCnt(SurveyVO surveyVo) throws Exception;

	public void insertSurveyAnswer(EgovMap map) throws Exception;

	public void insertSurveySubmit(EgovMap map) throws Exception;

	public EgovMap selectMySurvey(SearchVO searchVo) throws Exception;

	public String selectSurveyId(SurveyVO surveyVO) throws Exception;
	
	//교원 만족도 조사
	public EgovMap selectMyProfessorSurvey(SearchVO searchVo) throws Exception;
	
	//설문 제출자 - 교원
    public List<EgovMap> selectSurveyProfessor(SurveyVO surveyVo) throws Exception;
    
    //설문 제출 현황 목록 - 교원
    public EgovMap selectSurveyProfessorSummary(SurveyVO surveyVO) throws Exception;

    // 과정만족도 답변 점수 목록
    public EgovMap curriculumSurveyAnswerScore(SurveyVO surveyVO) throws Exception;
    
    // 교원대상 과정만족도 답변 점수 목록
    public EgovMap curriculumSurveyAnswerScore3(SurveyVO surveyVO) throws Exception;
}
