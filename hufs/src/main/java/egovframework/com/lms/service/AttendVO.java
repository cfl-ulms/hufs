package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

public class AttendVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** 과정ID */
    private java.lang.String crclId;
    
    /** 수업ID */
    private java.lang.String plId;
    
    /** 메뉴ID */
    private java.lang.String menuId;
    
    /** 사용자ID */
    private java.lang.String userId;
    
    /** 사용자명 */
    private java.lang.String userNm;
    
    /** 출석상태 : Y - 출석, L - 지각, N - 결석 */
    private java.lang.String attentionType;
    
    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;
    
    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;
    
    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;
    
    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;
    
    /** 소속 */
    private java.lang.String mngDeptNm;
    
    /** 생년월일 */
    private java.lang.String brthdy;
    
    /** 학번 */
    private java.lang.String stNumber;
    
    /** 학년 */
    private java.lang.String stGrade;
    
    /** 반 */
    private java.lang.String classCnt;
    
    /** 조 */
    private java.lang.String groupCnt;
    
    //사용자Id
    private List<String> userIdList;
    
    //출석상태 목록
    private List<String> attentionTypeList;
    
    //검색 수업ID
    private java.lang.String searchPlId;
    
    //모달여부
    private java.lang.String modalAt;
    
	public java.lang.String getPlId() {
		return plId;
	}

	public void setPlId(java.lang.String plId) {
		this.plId = plId;
	}

	public java.lang.String getUserId() {
		return userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	public java.lang.String getAttentionType() {
		return attentionType;
	}

	public void setAttentionType(java.lang.String attentionType) {
		this.attentionType = attentionType;
	}

	public java.lang.String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(java.lang.String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public java.lang.String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(java.lang.String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public java.lang.String getUserNm() {
		return userNm;
	}

	public void setUserNm(java.lang.String userNm) {
		this.userNm = userNm;
	}

	public java.lang.String getMngDeptNm() {
		return mngDeptNm;
	}

	public void setMngDeptNm(java.lang.String mngDeptNm) {
		this.mngDeptNm = mngDeptNm;
	}

	public java.lang.String getBrthdy() {
		return brthdy;
	}

	public void setBrthdy(java.lang.String brthdy) {
		this.brthdy = brthdy;
	}

	public java.lang.String getStNumber() {
		return stNumber;
	}

	public void setStNumber(java.lang.String stNumber) {
		this.stNumber = stNumber;
	}

	public java.lang.String getStGrade() {
		return stGrade;
	}

	public void setStGrade(java.lang.String stGrade) {
		this.stGrade = stGrade;
	}

	public java.lang.String getClassCnt() {
		return classCnt;
	}

	public void setClassCnt(java.lang.String classCnt) {
		this.classCnt = classCnt;
	}

	public java.lang.String getCrclId() {
		return crclId;
	}

	public void setCrclId(java.lang.String crclId) {
		this.crclId = crclId;
	}

	public List<String> getAttentionTypeList() {
		return attentionTypeList;
	}

	public void setAttentionTypeList(List<String> attentionTypeList) {
		this.attentionTypeList = attentionTypeList;
	}

	public java.lang.String getMenuId() {
		return menuId;
	}

	public void setMenuId(java.lang.String menuId) {
		this.menuId = menuId;
	}

	public List<String> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}

	public java.lang.String getSearchPlId() {
		return searchPlId;
	}

	public void setSearchPlId(java.lang.String searchPlId) {
		this.searchPlId = searchPlId;
	}

	public java.lang.String getModalAt() {
		return modalAt;
	}

	public void setModalAt(java.lang.String modalAt) {
		this.modalAt = modalAt;
	}

	public java.lang.String getGroupCnt() {
		return groupCnt;
	}

	public void setGroupCnt(java.lang.String groupCnt) {
		this.groupCnt = groupCnt;
	}
    
    
}
