package egovframework.com.lms.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.CertificateService;
import egovframework.com.lms.service.CertificateVO;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;



@Service("certificateService")
public class CertificateServiceImpl extends EgovAbstractServiceImpl implements CertificateService {
        
    @Resource(name="gradeDAO")
    private GradeDAO gradeDao;
	
    @Resource(name="curriculumDAO")
    private CurriculumDAO curriculumDAO;
    
    @Resource(name="certificateDAO")
    private CertificateDAO certificateDAO;
    
    @Resource(name="surveyDAO")
    private SurveyDAO surveyDAO;
    
    public static String[] alphabet = new String[] {"A", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    
    //수료증등록
	public void insertCertificate(CertificateVO vo) throws Exception {
		vo.setFirstIndex(0);
		vo.setRecordCountPerPage(1);
		
		EgovMap egovMap = new EgovMap();
		CertificateVO certificateVO = new CertificateVO();
		certificateVO.setCrclId(vo.getCrclId());
		egovMap = certificateDAO.selectCertificateForctgryCd(certificateVO);
		
		if(egovMap != null) {
			if(egovMap.get("ctgryId").equals("CTG_0000000000000236")) {
				vo.setCtgryType("B");
			} else if (egovMap.get("ctgryId").equals("CTG_0000000000000237")) {
				vo.setCtgryType("C");
			} else {
				vo.setCtgryType("D");
			}
		} else {
			vo.setCtgryType("D");
		}
		
		List<EgovMap> certList = certificateDAO.selectCertificateList_2(vo);
		String year = EgovDateUtil.getToday().substring(0,4);
		String lastCertificateAlpha = vo.getCtgryType();
		String alpha = "A";
		int breakCnt = 0;
		int alphaNum = 0;
		int lastCertificateNum = 0;
		if(certList != null && certList.size() > 0 ) {
			lastCertificateNum = Integer.parseInt(certList.get(0).get("certificateNum").toString());
		}
		
		CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(vo.getCrclId());
		List<EgovMap> gradeList = gradeDao.selectGradeList(curriculumVO);
		for(int i = 0; i < gradeList.size(); i++) {
			
			if(gradeList.get(i).get("certificateId") == null || gradeList.get(i).get("certificateId").equals("")) {
			
				if("Y".equals(gradeList.get(i).get("finishAt").toString())) {
					String userId = gradeList.get(i).get("userId").toString();
					vo.setUserId(userId);
					
					//수료증 발행번호
					if(lastCertificateAlpha.equals("B") || lastCertificateAlpha.equals("C")) {
						alpha = lastCertificateAlpha;
					} else {
						for(int a=0; a < alphabet.length; a++) {
							if(breakCnt == 1) {
								break;
							}
							
							if(lastCertificateAlpha.equals(alphabet[a])) {
								alphaNum = a;
								alpha = alphabet[a];
								breakCnt++;
							}
						}
					}
					
					lastCertificateNum++;
					if(lastCertificateNum == 10000) {
						lastCertificateNum = 1;
						alpha = alphabet[alphaNum + 1];
					}
					String strNum = "";
					if(lastCertificateNum < 10) {
						strNum = "000" + Integer.toString(lastCertificateNum);
					}else if(lastCertificateNum < 100) {
						strNum = "00" + Integer.toString(lastCertificateNum);
					}else if(lastCertificateNum < 1000) { 
						strNum = "0" + Integer.toString(lastCertificateNum);
					} else {
						strNum = Integer.toString(lastCertificateNum);
					}
					
					String certificateId = "CFL-" + year + "-" + alpha + strNum;
					vo.setCertificateId(certificateId);
					
					certificateDAO.insertCertificate(vo);
				}
			}
		
		}
	}

    //재발급
	public void reCertificate(CertificateVO vo) throws Exception {
		vo.setReissueAt("Y");
		//certificateDAO.deleteCertificate(vo);
		insertCertificate(vo);
	}
	
	//주료증 목록
	public List selectCertificateList(CertificateVO searchVO) throws Exception {
		return certificateDAO.selectCertificateList(searchVO);
	}

	//주료증 목록 수
	public int selectCertificateListCnt(CertificateVO searchVO) throws Exception {
		return certificateDAO.selectCertificateListCnt(searchVO);
	}

	//수료증 조회
	public EgovMap selectCertificate(CertificateVO vo) throws Exception {
		certificateDAO.selectCertificate(vo);
		return null;
	}
	
	//수료증 체크
	public EgovMap certificateCheck(CertificateVO vo) throws Exception {
		return certificateDAO.certificateCheck(vo);
	}
    
	//수료증 발급 이력 등록
	public void insertCertificateHst(CertificateVO vo) throws Exception{
		certificateDAO.insertCertificateHst(vo);
	}
	
	//수료증 체크 후 과정만족도 필요한 경우 
	public EgovMap selectMySurveyDetail(SurveyVO vo) throws Exception{
		return surveyDAO.selectMySurveyDetail(vo);
	}
    
}
