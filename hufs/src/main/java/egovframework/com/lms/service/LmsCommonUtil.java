package egovframework.com.lms.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LmsCommonUtil {
	//년도
	public List<Integer> getYear() {
		List<Integer> arrYear = new ArrayList<Integer>();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		if(Calendar.getInstance().get(Calendar.MONTH) > 10){
			year++;
		}
		
		for(int i = 2019; i <= year; i++) {
			arrYear.add(i);
		}
		
		return arrYear;
	}
	
	//현재년도
	public int getTodayYear() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		return year;
	}
}
