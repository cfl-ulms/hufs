package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumVO.java
 * @Description : Curriculum VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public class SurveyQuestionVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;

    private String qesitmId;
    private String schdulId;
    private String qesitmSn;						//문항순번
    private String schdulClCode;
    private String essenAt; 						//필수여부체크
    private String qesitmSj;						//항목주제
    private String qesitmCn;						//설명
    private String qesitmTyCode;
    private String qesitmTy;
    private String crclId;
    private String plId;

	private List<SurveyQuestionExVO> examples;
    private List<SurveyQuestionExVO> widthExamples;
    private List<SurveyQuestionExVO> heightExamples;

	public String getQesitmId() {
		return qesitmId;
	}
	public void setQesitmId(String qesitmId) {
		this.qesitmId = qesitmId;
	}
	public String getSchdulId() {
		return schdulId;
	}
	public void setSchdulId(String schdulId) {
		this.schdulId = schdulId;
	}
	public String getSchdulClCode() {
		return schdulClCode;
	}
	public void setSchdulClCode(String schdulClCode) {
		this.schdulClCode = schdulClCode;
	}
	public String getEssenAt() {
		return essenAt;
	}
	public void setEssenAt(String essenAt) {
		this.essenAt = essenAt;
	}
	public String getQesitmSj() {
		return qesitmSj;
	}
	public void setQesitmSj(String qesitmSj) {
		this.qesitmSj = qesitmSj;
	}
	public String getQesitmCn() {
		return qesitmCn;
	}
	public void setQesitmCn(String qesitmCn) {
		this.qesitmCn = qesitmCn;
	}
	public String getQesitmTyCode() {
		return qesitmTyCode;
	}
	public void setQesitmTyCode(String qesitmTyCode) {
		this.qesitmTyCode = qesitmTyCode;
	}
	public String getQesitmTy() {
		return qesitmTy;
	}
	public void setQesitmTy(String qesitmTy) {
		this.qesitmTy = qesitmTy;
	}
	public List<SurveyQuestionExVO> getExamples() {
		return examples;
	}
	public void setExamples(List<SurveyQuestionExVO> examples) {
		this.examples = examples;
	}
	public String getQesitmSn() {
		return qesitmSn;
	}
	public void setQesitmSn(String qesitmSn) {
		this.qesitmSn = qesitmSn;
	}
	public List<SurveyQuestionExVO> getWidthExamples() {
		return widthExamples;
	}
	public void setWidthExamples(List<SurveyQuestionExVO> widthExamples) {
		this.widthExamples = widthExamples;
	}
	public List<SurveyQuestionExVO> getHeightExamples() {
		return heightExamples;
	}
	public void setHeightExamples(List<SurveyQuestionExVO> heightExamples) {
		this.heightExamples = heightExamples;
	}
	 public String getCrclId() {
		return crclId;
	}
	public void setCrclId(String crclId) {
		this.crclId = crclId;
	}
	public String getPlId() {
		return plId;
	}
	public void setPlId(String plId) {
		this.plId = plId;
	}

}
