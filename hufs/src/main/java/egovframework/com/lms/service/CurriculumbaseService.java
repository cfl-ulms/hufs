package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumbaseService.java
 * @Description : Curriculumbase Business class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface CurriculumbaseService {
	
	/**
	 * curriculumbase을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumbaseVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertCurriculumbase(CurriculumbaseVO vo) throws Exception;
    
    /**
	 * curriculumbase을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumbaseVO
	 * @return void형
	 * @exception Exception
	 */
    void updateCurriculumbase(CurriculumbaseVO vo) throws Exception;
    
    /**
	 * curriculumbase을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumbaseVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteCurriculumbase(CurriculumbaseVO vo) throws Exception;
    
    /**
	 * curriculumbase을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return 조회한 curriculumbase
	 * @exception Exception
	 */
    CurriculumbaseVO selectCurriculumbase(CurriculumbaseVO vo) throws Exception;
    
    //과정학습참고자료 조회
    List selectCurriculumbaseFile(CurriculumbaseVO vo) throws Exception;
    
    /**
	 * curriculumbase 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumbase 목록
	 * @exception Exception
	 */
    List selectCurriculumbaseList(CurriculumbaseVO searchVO) throws Exception;
    
    /**
	 * curriculumbase 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculumbase 총 갯수
	 * @exception
	 */
    int selectCurriculumbaseListTotCnt(CurriculumbaseVO searchVO);
    
}
