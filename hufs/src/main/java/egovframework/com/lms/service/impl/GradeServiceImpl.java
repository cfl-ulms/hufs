package egovframework.com.lms.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.GradeService;
import egovframework.com.utl.fcc.service.EgovStringUtil;



@Service("gradeService")
public class GradeServiceImpl extends EgovAbstractServiceImpl implements GradeService {
        
    @Resource(name="gradeDAO")
    private GradeDAO gradeDao;
	
    @Resource(name="curriculumDAO")
    private CurriculumDAO curriculumDAO;
    
    @Resource(name = "curriculumService")
	 private CurriculumService curriculumService;
    
    //성적등록 - 엑셀
    public void uploadExcel(CurriculumVO vo) throws Exception {
    	gradeDao.insertGrade(vo);
    	gradeDao.insertGradeEtc(vo);
    	/*
    	if(vo.getUserIdList().size() != 0 && vo.getAttentionTypeList().size() != 0){
    		//초기화
    		attendDAO.deleteAttention(vo);
    		
    		EgovMap paramMap = new EgovMap();
    		List<EgovMap> atdList = new ArrayList<EgovMap>();
    		for(int i = 0; i < vo.getUserIdList().size(); i++){
    			EgovMap atdMap = new EgovMap();

    			atdMap.put("plId", vo.getPlId());
    			atdMap.put("userId", vo.getUserIdList().get(i));
    			atdMap.put("attentionType", vo.getAttentionTypeList().get(i));
    			atdMap.put("frstRegisterId", vo.getFrstRegisterId());
    			
    			atdList.add(atdMap);
        	}
        	paramMap.put("atdList", atdList);
        	
        	attendDAO.insertAllAttention(paramMap);
    	}
    	*/
    }
    
    //성적등록
    public void gradeTotalUpdate(CurriculumVO vo) throws Exception {
    	//학생
    	if(vo.getUserIdList() != null){
    		
    		//중간, 기말, 수시 만점 
    		curriculumDAO.updateCurriculumPart(vo);
    		
    		for(int i = 0; i < vo.getUserIdList().size(); i++){
    			CurriculumVO curriculumVO = new CurriculumVO();
    			
    			curriculumVO.setCrclId(vo.getCrclId());
    			curriculumVO.setUserId(vo.getUserIdList().get(i));
        		
    			//해당 학생 초기화
        		//gradeDao.deleteGradeEtc(vo);
        		gradeDao.deleteGrade(curriculumVO);
    			
    			//중간고사
    			if(vo.getMidtermTotalScoreList() != null){
    				//curriculumVO.setMidtermScore(vo.getMidtermScoreList().get(i));
    				double midtermScore = vo.getMidtermTotalScoreList().get(i) / (double) (vo.getMidtermTopScore() / 100);
    				curriculumVO.setMidtermScore(midtermScore);
    				curriculumVO.setMidtermTotalScore(vo.getMidtermTotalScoreList().get(i));
    			}
    			
    			//기말고사
    			if(vo.getFinalTotalScoreList() != null){
    				//curriculumVO.setFinalScore(vo.getFinalScoreList().get(i));
    				double finalScore = vo.getFinalTotalScoreList().get(i) / (double) (vo.getFinalTopScore() / 100);
    				curriculumVO.setFinalScore(finalScore);
    				curriculumVO.setFinalTotalScore(vo.getFinalTotalScoreList().get(i));
    			}
    			
    			//수시시험
    			if(vo.getEvaluationTotalScoreList() != null){
    				//curriculumVO.setEvaluationScore(vo.getEvaluationScoreList().get(i));
    				double evaluationScore = vo.getEvaluationTotalScoreList().get(i) / (double) (vo.getEvaluationTopScore() / 100);
    				curriculumVO.setEvaluationScore(evaluationScore);
    				curriculumVO.setEvaluationTotalScore(vo.getEvaluationTotalScoreList().get(i));
    			}
    			
    			//출석변환점수
    			if(vo.getAttendScoreList() != null){
    				curriculumVO.setAttendScore(vo.getAttendScoreList().get(i));
    			}
    			
    			//확정등급
    			if(vo.getConfirmGradeList() != null){
    				curriculumVO.setConfirmGrade(vo.getConfirmGradeList().get(i));
    			}
    			
    			//기타점수1
    			if(vo.getEtcScoreList1() != null){
    				curriculumVO.setEtcScore1(vo.getEtcScoreList1().get(i));
    			}
    			//기타점수2
    			if(vo.getEtcScoreList2() != null){
    				curriculumVO.setEtcScore2(vo.getEtcScoreList2().get(i));
    			}
    			//기타점수3
    			if(vo.getEtcScoreList3() != null){
    				curriculumVO.setEtcScore3(vo.getEtcScoreList3().get(i));
    			}
    			//기타점수4
    			if(vo.getEtcScoreList4() != null){
    				curriculumVO.setEtcScore4(vo.getEtcScoreList4().get(i));
    			}
    			//기타점수5
    			if(vo.getEtcScoreList5() != null){
    				curriculumVO.setEtcScore5(vo.getEtcScoreList5().get(i));
    			}
    			//기타점수6
    			if(vo.getEtcScoreList6() != null){
    				curriculumVO.setEtcScore6(vo.getEtcScoreList6().get(i));
    			}
    			//기타점수7
    			if(vo.getEtcScoreList7() != null){
    				curriculumVO.setEtcScore7(vo.getEtcScoreList7().get(i));
    			}
    			//기타점수8
    			if(vo.getEtcScoreList8() != null){
    				curriculumVO.setEtcScore8(vo.getEtcScoreList8().get(i));
    			}
    			//기타점수9
    			if(vo.getEtcScoreList9() != null){
    				curriculumVO.setEtcScore9(vo.getEtcScoreList9().get(i));
    			}
    			//기타점수10
    			if(vo.getEtcScoreList10() != null){
    				curriculumVO.setEtcScore10(vo.getEtcScoreList10().get(i));
    			}
    			
    			//과정성적 등록
        		gradeDao.insertGrade(curriculumVO);
        	}
    	}
    }
    
    /** 
	 * 엑셀파일을 업로드 하여 파싱한다.한다.
	 * @param  vo ComtnlrncntntsVO  
	 * @param  FileVO fileVO  
	 * @exception Exception
	 */
	public Map<String, Object> parseExcel(CurriculumVO searchVO, FileVO fileVO)  {
		
		String fileExt = "";
	    int index = fileVO.getOrignlFileNm().lastIndexOf(".");
	    if(index != -1) {
	    	fileExt = fileVO.getOrignlFileNm().substring(index + 1);
	    } 
	    
	    CurriculumVO curriculumVO = null;
	    List<CurriculumVO> dataList = new ArrayList<CurriculumVO>();
		
		String baseMessage = null;
		Map<String, Object> map = new HashMap<String, Object>();		
	    try {
	    	Workbook wb = null;
	    	if("XLS".equals(fileExt.toUpperCase())) {
	    		wb = WorkbookFactory.create(new File(fileVO.getFileStreCours() + File.separator + fileVO.getStreFileNm()));
	    	} else if("XLSX".equals(fileExt.toUpperCase())) {
	    		wb = (XSSFWorkbook)WorkbookFactory.create(new File(fileVO.getFileStreCours() + File.separator + fileVO.getStreFileNm()));
	    	}
	    	FormulaEvaluator eval = wb.getCreationHelper().createFormulaEvaluator();
	    	
	    	//int sheetNum = wb.getNumberOfSheets(); //시트갯수 가져오기
	    	if(wb != null) {
		    	Sheet sheet = wb.getSheetAt(0);
		    	int rows = sheet.getPhysicalNumberOfRows(); //행 갯수 가져오기
	
	            for(int r = 3; r < rows; r++){ //row 루프            	
	            	Row row = sheet.getRow(r); //row 가져오기
	                if(row != null) {
	                	curriculumVO = new CurriculumVO();
	                	int cells = row.getPhysicalNumberOfCells();
		                for(int c = 1; c < 10; c++) {	//cell 가져오기
		                	Cell cell = row.getCell(c);
		                	if(cell != null) {
			                	String value = "";
			                	switch(cell.getCellType()) {
				                	case Cell.CELL_TYPE_FORMULA:
				                		if(!EgovStringUtil.isEmpty(cell.toString())) {
				                			switch(eval.evaluateFormulaCell(cell)) {
				                				case Cell.CELL_TYPE_NUMERIC:
				                					if (HSSFDateUtil.isCellDateFormatted(cell)){ 
							                	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
							                	        value = formatter.format(cell.getDateCellValue()); 
							                	    } else { 
							                	        value="" + (long)cell.getNumericCellValue(); 
							                	    }
				                					break;
				                				case Cell.CELL_TYPE_STRING:
							                		value = "" + cell.getRichStringCellValue();
							                		break;
							                	case Cell.CELL_TYPE_BLANK:
							                		value = "";
							                		break;
							                	case Cell.CELL_TYPE_ERROR:
							                		value = "" + cell.getErrorCellValue();
							                		break;
							                	case Cell.CELL_TYPE_BOOLEAN:
							                		value = "" + cell.getBooleanCellValue();
							                		break;
							                	default:
							                		break;
				                			}
				                		}
				                		break;
				                	case Cell.CELL_TYPE_NUMERIC:
				                		
				                		if (HSSFDateUtil.isCellDateFormatted(cell)){ 
				                	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
				                	        value = formatter.format(cell.getDateCellValue()); 
				                	    } else { 
				                	        value="" + (long)cell.getNumericCellValue(); 
				                	    }
				                	    
				                		break;
				                	case Cell.CELL_TYPE_STRING:
				                		value = "" + cell.getRichStringCellValue();
				                		break;
				                	case Cell.CELL_TYPE_BLANK:
				                		value = "";
				                		break;
				                	case Cell.CELL_TYPE_ERROR:
				                		value = "" + cell.getErrorCellValue();
				                		break;
				                	case Cell.CELL_TYPE_BOOLEAN:
				                		value = "" + cell.getBooleanCellValue();
				                		break;
				                	default:
				                		break;
			                	}
			                	
			                	if(!EgovStringUtil.isEmpty(value)) {
			                		value = value.trim();
			                	}			                	
			                	
			                	switch(c) {
			                		/*
					                case 1 : curriculumVO.setUserId(value); 		break;
					                case 2 : curriculumVO.setTag(value); 	break;
					                case 3 : curriculumVO.setLrnCntntsSeCode(value); 	break;
					                case 4 : curriculumVO.setStreDtaNm(value); 	break;
					                case 5 : curriculumVO.setPlayTime(value); 	break;
					                case 6 : curriculumVO.setThumbFileNm(value); 	break;
					                case 7 : curriculumVO.setOfferNo(BigDecimal.valueOf(Long.parseLong(value))); 	break;
					                case 8 : curriculumVO.setCpyrhtTyCode(value); 	break;
					                case 9 : curriculumVO.setCpyrhtDtlclfc(value); 	break;
					                
					                default : break;
					                */
			                	}
		                	}
		                }
		                
		                //총괄평가
		        		List evaluationList = curriculumService.selectTotalEvaluation(searchVO);
		                
		                curriculumVO.setCrclId(searchVO.getCrclId());
		                dataList.add(curriculumVO);
	                }
	            }
	    	}
	    } catch(FileNotFoundException e) {
	    	baseMessage = e.getMessage();//"엑셀 읽기 실패";
	    	e.printStackTrace(); 
	    } catch(IOException e) {
	    	baseMessage = e.getMessage();//"엑셀 읽기 실패";
	    	e.printStackTrace(); 
	    } catch(InvalidFormatException e) {
	    	baseMessage = e.getMessage();//"엑셀 읽기 실패";
	    	e.printStackTrace(); 
	    } catch(Exception e){
        	baseMessage = e.getMessage();//"엑셀 읽기 실패";
        	//e.printStackTrace(); 
        } finally {
        }
    	
	    map.put("message", baseMessage);
    	map.put("dataList", dataList);
    	
		return map;
	}
	
	//성적목록
	 public List selectGradeList(CurriculumVO vo) throws Exception {
        return gradeDao.selectGradeList(vo);
    }
	 
	//수료정보취합
    public EgovMap selectGradeSummary(CurriculumVO vo) throws Exception {
    	return gradeDao.selectGradeSummary(vo);
    }
    
}
