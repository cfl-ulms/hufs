package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

public class CertificateVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** 과정ID */
    private java.lang.String crclId;
    
    /** 사용자ID */
    private java.lang.String userId;
    
    /** 사용자명 */
    private java.lang.String userNm;
    
    // 수료증 발급자 ID
    private java.lang.String issuerId;
    
    //수료증ID
    private java.lang.String certificateId;
    
    //발행여부
    private java.lang.String publicAt;
    
    //재발급 요청여부
    private java.lang.String reissueAt;
    
    //사용여부
    private java.lang.String useAt;
    
    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;
    
    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;
    
    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;
    
    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;
    
    /** 소속 */
    private java.lang.String mngDeptNm;
    
    /** 생년월일 */
    private java.lang.String brthdy;
    
    /** 학번 */
    private java.lang.String stNumber;
    
    /** 학년 */
    private java.lang.String stGrade;
    
    /** 반 */
    private java.lang.String classCnt;
    
    /** 조 */
    private java.lang.String groupCnt;
    
    //사용자Id
    private List<String> userIdList;
    
    // 사용자 교육과정의 기본과정 대분류 코드
    private String ctgryCd;
    
    // 수료증ID 에서 '-' 기준 세번째 부분
    private String certificateId3;
    
    // 사용자 교욱과정의 기본과정 타입
    private String ctgryType;
    
    // 수료번호(String으로 설정)
    private String certificateNum;
    
    
    //검색용
    private java.lang.String searchCertificateId;
    private java.lang.String searchCrclYear;
    private java.lang.String searchCrclTerm;
    private java.lang.String searchStartDate;
    private java.lang.String searchEndDate;
    private java.lang.String searchCrclNm;
    private java.lang.String searchGroupCode;
    private java.lang.String searchUserNm;
    private java.lang.String searchMoblphonNo;
    private java.lang.String searchEmailAdres;
    private java.lang.String searchUserId;
    private java.lang.String searchBrthdy;
    private java.lang.String searchCrclId;
    
	public java.lang.String getCrclId() {
		return crclId;
	}

	public void setCrclId(java.lang.String crclId) {
		this.crclId = crclId;
	}

	public java.lang.String getUserId() {
		return userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	public java.lang.String getUserNm() {
		return userNm;
	}

	public void setUserNm(java.lang.String userNm) {
		this.userNm = userNm;
	}

	public java.lang.String getCertificateId() {
		return certificateId;
	}

	public void setCertificateId(java.lang.String certificateId) {
		this.certificateId = certificateId;
	}

	public java.lang.String getPublicAt() {
		return publicAt;
	}

	public void setPublicAt(java.lang.String publicAt) {
		this.publicAt = publicAt;
	}

	public java.lang.String getReissueAt() {
		return reissueAt;
	}

	public void setReissueAt(java.lang.String reissueAt) {
		this.reissueAt = reissueAt;
	}

	public java.lang.String getUseAt() {
		return useAt;
	}

	public void setUseAt(java.lang.String useAt) {
		this.useAt = useAt;
	}

	public java.lang.String getFrstRegisterId() {
		return frstRegisterId;
	}

	public void setFrstRegisterId(java.lang.String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}

	public java.util.Date getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}

	public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}

	public java.lang.String getLastUpdusrId() {
		return lastUpdusrId;
	}

	public void setLastUpdusrId(java.lang.String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}

	public java.util.Date getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}

	public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}

	public java.lang.String getMngDeptNm() {
		return mngDeptNm;
	}

	public void setMngDeptNm(java.lang.String mngDeptNm) {
		this.mngDeptNm = mngDeptNm;
	}

	public java.lang.String getBrthdy() {
		return brthdy;
	}

	public void setBrthdy(java.lang.String brthdy) {
		this.brthdy = brthdy;
	}

	public java.lang.String getStNumber() {
		return stNumber;
	}

	public void setStNumber(java.lang.String stNumber) {
		this.stNumber = stNumber;
	}

	public java.lang.String getStGrade() {
		return stGrade;
	}

	public void setStGrade(java.lang.String stGrade) {
		this.stGrade = stGrade;
	}

	public java.lang.String getClassCnt() {
		return classCnt;
	}

	public void setClassCnt(java.lang.String classCnt) {
		this.classCnt = classCnt;
	}

	public java.lang.String getGroupCnt() {
		return groupCnt;
	}

	public void setGroupCnt(java.lang.String groupCnt) {
		this.groupCnt = groupCnt;
	}

	public List<String> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public java.lang.String getSearchCrclYear() {
		return searchCrclYear;
	}

	public void setSearchCrclYear(java.lang.String searchCrclYear) {
		this.searchCrclYear = searchCrclYear;
	}

	public java.lang.String getSearchCrclTerm() {
		return searchCrclTerm;
	}

	public void setSearchCrclTerm(java.lang.String searchCrclTerm) {
		this.searchCrclTerm = searchCrclTerm;
	}

	public java.lang.String getSearchStartDate() {
		return searchStartDate;
	}

	public void setSearchStartDate(java.lang.String searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	public java.lang.String getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(java.lang.String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	public java.lang.String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(java.lang.String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public java.lang.String getSearchGroupCode() {
		return searchGroupCode;
	}

	public void setSearchGroupCode(java.lang.String searchGroupCode) {
		this.searchGroupCode = searchGroupCode;
	}

	public java.lang.String getSearchUserNm() {
		return searchUserNm;
	}

	public void setSearchUserNm(java.lang.String searchUserNm) {
		this.searchUserNm = searchUserNm;
	}

	public java.lang.String getSearchMoblphonNo() {
		return searchMoblphonNo;
	}

	public void setSearchMoblphonNo(java.lang.String searchMoblphonNo) {
		this.searchMoblphonNo = searchMoblphonNo;
	}

	public java.lang.String getSearchEmailAdres() {
		return searchEmailAdres;
	}

	public void setSearchEmailAdres(java.lang.String searchEmailAdres) {
		this.searchEmailAdres = searchEmailAdres;
	}

	public java.lang.String getSearchUserId() {
		return searchUserId;
	}

	public void setSearchUserId(java.lang.String searchUserId) {
		this.searchUserId = searchUserId;
	}

	public java.lang.String getSearchBrthdy() {
		return searchBrthdy;
	}

	public void setSearchBrthdy(java.lang.String searchBrthdy) {
		this.searchBrthdy = searchBrthdy;
	}

	public java.lang.String getSearchCrclId() {
		return searchCrclId;
	}

	public void setSearchCrclId(java.lang.String searchCrclId) {
		this.searchCrclId = searchCrclId;
	}

	public java.lang.String getSearchCertificateId() {
		return searchCertificateId;
	}

	public void setSearchCertificateId(java.lang.String searchCertificateId) {
		this.searchCertificateId = searchCertificateId;
	}

	public String getCtgryCd() {
		return ctgryCd;
	}

	public void setCtgryCd(String ctgryCd) {
		this.ctgryCd = ctgryCd;
	}

	public String getCertificateId3() {
		return certificateId3;
	}

	public void setCertificateId3(String certificateId3) {
		this.certificateId3 = certificateId3;
	}

	public String getCtgryType() {
		return ctgryType;
	}

	public void setCtgryType(String ctgryType) {
		this.ctgryType = ctgryType;
	}

	public String getCertificateNum() {
		return certificateNum;
	}

	public void setCertificateNum(String certificateNum) {
		this.certificateNum = certificateNum;
	}

	public java.lang.String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(java.lang.String issuerId) {
		this.issuerId = issuerId;
	}
	
}
