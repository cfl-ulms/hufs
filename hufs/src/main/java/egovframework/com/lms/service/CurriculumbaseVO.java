package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumbaseVO.java
 * @Description : Curriculumbase VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public class CurriculumbaseVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;
    
    /** CRCLB_ID */
    private java.lang.String crclbId;
    
    /** CRCLB_NM */
    private java.lang.String crclbNm;
    
    /** SYS_CODE */
    private java.lang.String sysCode;
    
    /** 과정체계코드경로 */
    private java.lang.String sysCodePath;
    
    /** 과정체계코드명경로 */
    private java.lang.String sysCodeNmPath;
    
    /** SYS_CODE */
    private List<String> sysCodeList;
    
    /** DIVISION */
    private java.lang.String division;
    
    //이수구분명
    private java.lang.String divisionNm;
    
    /** GRADE_AT */
    private java.lang.String gradeAt;
    
    /** GRADE_NUM */
    private java.lang.String gradeNum;
    
    /** CONTROL */
    private java.lang.String control;
    
    //관리구분명
    private java.lang.String controlNm;
    
    /** PROJECT_AT */
    private java.lang.String projectAt;
    
    /** TARGET_TYPE */
    private java.lang.String targetType;
    
    private java.lang.String evaluationAt;
    
    /** TARGET_DETAIL */
    private java.lang.String targetDetail;
    
    //대상상세명
    private java.lang.String targetDetailNm;
    
    /** TOTAL_TIME_AT */
    private java.lang.String totalTimeAt;
    
    /** GRADE_TYPE */
    private java.lang.String gradeType;
    private java.lang.String gradeTypeNm;
    
    /** REPORT_TYPE */
    private java.lang.String reportType;
    
    /** SURVEY_SATISFY_TYPE */
    private java.lang.String surveySatisfyType;
    
    /** 교원대상과정만족도설문유형ID */
    private java.lang.String professorSatisfyType;
    
    /** SURVEY_IND_AT */
    private java.lang.String surveyIndAt;
    
    /** SURVEY_IND */
    private java.lang.String surveyInd;
    
    /** STDNT_APLY_AT */
    private java.lang.String stdntAplyAt;
    
    /** APLY_FILE */
    private java.lang.String aplyFile;
    private java.lang.String aplyFileNm;
    
    /** PLAN_FILE */
    private java.lang.String planFile;
    private java.lang.String planFileNm;
    
    /** ETC_FILE */
    private java.lang.String etcFile;
    private java.lang.String etcFileNm;
    
    /** USE_AT */
    private java.lang.String useAt;
    
    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;
    
    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;
    
    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;
    
    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;
    
    private List<String> refeFileList;
    
    //참고자료 파일ID
    private java.lang.String atchFileId;
    
    //기본과정명 검색
    private java.lang.String searchcrclbNm;
    
    //기본과정코드검색
    private java.lang.String searchcrclbId;
    
    //대분류검색
    private java.lang.String searchSysCode1;
    
    //중분류검색
    private java.lang.String searchSysCode2;
    
    //소분류검색
    private java.lang.String searchSysCode3;
    
    //이수구분검색
    private java.lang.String searchDivision;
    
    //관리구분검색
    private java.lang.String searchControl;
    
    //대상검색
    private java.lang.String searchTargetType;
    
    //대상상세검색
    private java.lang.String searchTargetDetail;
    
    private java.lang.String searchProjectAt;
    
    private java.lang.String searchTotalTimeAt;
    
    public java.lang.String getCrclbId() {
        return this.crclbId;
    }
    
    public void setCrclbId(java.lang.String crclbId) {
        this.crclbId = crclbId;
    }
    
    public java.lang.String getCrclbNm() {
        return this.crclbNm;
    }
    
    public void setCrclbNm(java.lang.String crclbNm) {
        this.crclbNm = crclbNm;
    }
    
    public java.lang.String getSysCode() {
        return this.sysCode;
    }
    
    public void setSysCode(java.lang.String sysCode) {
        this.sysCode = sysCode;
    }
    
    public java.lang.String getDivision() {
        return this.division;
    }
    
    public void setDivision(java.lang.String division) {
        this.division = division;
    }
    
    public java.lang.String getGradeAt() {
        return this.gradeAt;
    }
    
    public void setGradeAt(java.lang.String gradeAt) {
        this.gradeAt = gradeAt;
    }
    
    public java.lang.String getGradeNum() {
        return this.gradeNum;
    }
    
    public void setGradeNum(java.lang.String gradeNum) {
        this.gradeNum = gradeNum;
    }
    
    public java.lang.String getControl() {
        return this.control;
    }
    
    public void setControl(java.lang.String control) {
        this.control = control;
    }
    
    public java.lang.String getProjectAt() {
        return this.projectAt;
    }
    
    public void setProjectAt(java.lang.String projectAt) {
        this.projectAt = projectAt;
    }
    
    public java.lang.String getTargetType() {
        return this.targetType;
    }
    
    public void setTargetType(java.lang.String targetType) {
        this.targetType = targetType;
    }
    
    public java.lang.String getTargetDetail() {
        return this.targetDetail;
    }
    
    public void setTargetDetail(java.lang.String targetDetail) {
        this.targetDetail = targetDetail;
    }
    
    public java.lang.String getTotalTimeAt() {
        return this.totalTimeAt;
    }
    
    public void setTotalTimeAt(java.lang.String totalTimeAt) {
        this.totalTimeAt = totalTimeAt;
    }
    
    public java.lang.String getGradeType() {
        return this.gradeType;
    }
    
    public void setGradeType(java.lang.String gradeType) {
        this.gradeType = gradeType;
    }
    
    public java.lang.String getReportType() {
        return this.reportType;
    }
    
    public void setReportType(java.lang.String reportType) {
        this.reportType = reportType;
    }
    
    public java.lang.String getSurveySatisfyType() {
        return this.surveySatisfyType;
    }
    
    public void setSurveySatisfyType(java.lang.String surveySatisfyType) {
        this.surveySatisfyType = surveySatisfyType;
    }
    
    public java.lang.String getSurveyIndAt() {
        return this.surveyIndAt;
    }
    
    public void setSurveyIndAt(java.lang.String surveyIndAt) {
        this.surveyIndAt = surveyIndAt;
    }
    
    public java.lang.String getSurveyInd() {
        return this.surveyInd;
    }
    
    public void setSurveyInd(java.lang.String surveyInd) {
        this.surveyInd = surveyInd;
    }
    
    public java.lang.String getStdntAplyAt() {
        return this.stdntAplyAt;
    }
    
    public void setStdntAplyAt(java.lang.String stdntAplyAt) {
        this.stdntAplyAt = stdntAplyAt;
    }
    
    public java.lang.String getAplyFile() {
        return this.aplyFile;
    }
    
    public void setAplyFile(java.lang.String aplyFile) {
        this.aplyFile = aplyFile;
    }
    
    public java.lang.String getPlanFile() {
        return this.planFile;
    }
    
    public void setPlanFile(java.lang.String planFile) {
        this.planFile = planFile;
    }
    
    public java.lang.String getEtcFile() {
        return this.etcFile;
    }
    
    public void setEtcFile(java.lang.String etcFile) {
        this.etcFile = etcFile;
    }
    
    public java.lang.String getUseAt() {
        return this.useAt;
    }
    
    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }
    
    public java.lang.String getFrstRegisterId() {
        return this.frstRegisterId;
    }
    
    public void setFrstRegisterId(java.lang.String frstRegisterId) {
        this.frstRegisterId = frstRegisterId;
    }
    
    public java.util.Date getFrstRegisterPnttm() {
        return this.frstRegisterPnttm;
    }
    
    public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
        this.frstRegisterPnttm = frstRegisterPnttm;
    }
    
    public java.lang.String getLastUpdusrId() {
        return this.lastUpdusrId;
    }
    
    public void setLastUpdusrId(java.lang.String lastUpdusrId) {
        this.lastUpdusrId = lastUpdusrId;
    }
    
    public java.util.Date getLastUpdusrPnttm() {
        return this.lastUpdusrPnttm;
    }
    
    public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
        this.lastUpdusrPnttm = lastUpdusrPnttm;
    }

	public List<String> getSysCodeList() {
		return sysCodeList;
	}

	public void setSysCodeList(List<String> sysCodeList) {
		this.sysCodeList = sysCodeList;
	}

	public List<String> getRefeFileList() {
		return refeFileList;
	}

	public void setRefeFileList(List<String> refeFileList) {
		this.refeFileList = refeFileList;
	}

	public java.lang.String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(java.lang.String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public java.lang.String getSysCodePath() {
		return sysCodePath;
	}

	public void setSysCodePath(java.lang.String sysCodePath) {
		this.sysCodePath = sysCodePath;
	}

	public java.lang.String getDivisionNm() {
		return divisionNm;
	}

	public void setDivisionNm(java.lang.String divisionNm) {
		this.divisionNm = divisionNm;
	}

	public java.lang.String getControlNm() {
		return controlNm;
	}

	public void setControlNm(java.lang.String controlNm) {
		this.controlNm = controlNm;
	}

	public java.lang.String getTargetDetailNm() {
		return targetDetailNm;
	}

	public void setTargetDetailNm(java.lang.String targetDetailNm) {
		this.targetDetailNm = targetDetailNm;
	}

	public java.lang.String getSearchcrclbNm() {
		return searchcrclbNm;
	}

	public void setSearchcrclbNm(java.lang.String searchcrclbNm) {
		this.searchcrclbNm = searchcrclbNm;
	}

	public java.lang.String getSearchcrclbId() {
		return searchcrclbId;
	}

	public void setSearchcrclbId(java.lang.String searchcrclbId) {
		this.searchcrclbId = searchcrclbId;
	}

	public java.lang.String getSearchSysCode1() {
		return searchSysCode1;
	}

	public void setSearchSysCode1(java.lang.String searchSysCode1) {
		this.searchSysCode1 = searchSysCode1;
	}

	public java.lang.String getSearchSysCode2() {
		return searchSysCode2;
	}

	public void setSearchSysCode2(java.lang.String searchSysCode2) {
		this.searchSysCode2 = searchSysCode2;
	}

	public java.lang.String getSearchSysCode3() {
		return searchSysCode3;
	}

	public void setSearchSysCode3(java.lang.String searchSysCode3) {
		this.searchSysCode3 = searchSysCode3;
	}

	public java.lang.String getSearchDivision() {
		return searchDivision;
	}

	public void setSearchDivision(java.lang.String searchDivision) {
		this.searchDivision = searchDivision;
	}

	public java.lang.String getSearchControl() {
		return searchControl;
	}

	public void setSearchControl(java.lang.String searchControl) {
		this.searchControl = searchControl;
	}

	public java.lang.String getSearchTargetType() {
		return searchTargetType;
	}

	public void setSearchTargetType(java.lang.String searchTargetType) {
		this.searchTargetType = searchTargetType;
	}

	public java.lang.String getSearchTargetDetail() {
		return searchTargetDetail;
	}

	public void setSearchTargetDetail(java.lang.String searchTargetDetail) {
		this.searchTargetDetail = searchTargetDetail;
	}

	
	public java.lang.String getSearchProjectAt() {
		return searchProjectAt;
	}

	public void setSearchProjectAt(java.lang.String searchProjectAt) {
		this.searchProjectAt = searchProjectAt;
	}

	public java.lang.String getSearchTotalTimeAt() {
		return searchTotalTimeAt;
	}

	public void setSearchTotalTimeAt(java.lang.String searchTotalTimeAt) {
		this.searchTotalTimeAt = searchTotalTimeAt;
	}

	public java.lang.String getSysCodeNmPath() {
		return sysCodeNmPath;
	}

	public void setSysCodeNmPath(java.lang.String sysCodeNmPath) {
		this.sysCodeNmPath = sysCodeNmPath;
	}

	public java.lang.String getAplyFileNm() {
		return aplyFileNm;
	}

	public void setAplyFileNm(java.lang.String aplyFileNm) {
		this.aplyFileNm = aplyFileNm;
	}

	public java.lang.String getPlanFileNm() {
		return planFileNm;
	}

	public void setPlanFileNm(java.lang.String planFileNm) {
		this.planFileNm = planFileNm;
	}

	public java.lang.String getEtcFileNm() {
		return etcFileNm;
	}

	public void setEtcFileNm(java.lang.String etcFileNm) {
		this.etcFileNm = etcFileNm;
	}

	public java.lang.String getEvaluationAt() {
		return evaluationAt;
	}

	public void setEvaluationAt(java.lang.String evaluationAt) {
		this.evaluationAt = evaluationAt;
	}

	public java.lang.String getGradeTypeNm() {
		return gradeTypeNm;
	}

	public void setGradeTypeNm(java.lang.String gradeTypeNm) {
		this.gradeTypeNm = gradeTypeNm;
	}

	public java.lang.String getProfessorSatisfyType() {
		return professorSatisfyType;
	}

	public void setProfessorSatisfyType(java.lang.String professorSatisfyType) {
		this.professorSatisfyType = professorSatisfyType;
	}
    
}
