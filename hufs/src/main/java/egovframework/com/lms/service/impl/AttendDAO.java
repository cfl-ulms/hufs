package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.lms.service.CurriculumVO;

//출석
@Repository("attendDAO")
public class AttendDAO extends EgovAbstractDAO {

    //출석 수정
    public void updateAttention(AttendVO vo) throws Exception {
        update("attendDAO.updateAttention", vo);
    }
    
    //출석등록
    public void insertAttention(AttendVO vo) throws Exception {
        insert("attendDAO.insertAttention", vo);
    }
    
    //출석일괄등록
    public void insertAllAttention(EgovMap vo) throws Exception {
        insert("attendDAO.insertAllAttention", vo);
    }
    
    //출석삭제
    public void deleteAttention(AttendVO vo) throws Exception {
        delete("attendDAO.deleteAttention", vo);
    }
    
    //출석 목록
    public List selectAttendList(AttendVO vo) throws Exception {
        return list("attendDAO.selectAttendList", vo);
    }
    
    //과정별 학생 출석 목록(성적)
    public List studentAttendList(AttendVO vo) throws Exception {
        return list("attendDAO.studentAttendList", vo);
    }
    
}
