package egovframework.com.lms.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.lms.service.CurriculumVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumService.java
 * @Description : Curriculum Business class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public interface CurriculumService {

	/**
	 * curriculum을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertCurriculum(CurriculumVO vo) throws Exception;

    /**
	 * curriculum을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    void updateCurriculum(CurriculumVO vo) throws Exception;

    //과정 일부만 수정
    void updateCurriculumPart(CurriculumVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception;

    /**
	 * curriculum을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumVO
	 * @return void형
	 * @exception Exception
	 */
    void deleteCurriculum(CurriculumVO vo) throws Exception;

    /**
	 * curriculum을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumVO
	 * @return 조회한 curriculum
	 * @exception Exception
	 */
    CurriculumVO selectCurriculum(CurriculumVO vo) throws Exception;

    /**
	 * curriculum 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 목록
	 * @exception Exception
	 */
    List<?> selectCurriculumList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception;

    /**
	 * curriculum 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return curriculum 총 갯수
	 * @exception
	 */
    int selectCurriculumListTotCnt(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response);
    
    //사업비
    List selectCurriculumExpense(CurriculumVO vo) throws Exception;

    //프로세스 업데이트
    void updatePsCodeCurriculum(CurriculumVO vo) throws Exception;

    //학습내용
    List selectCurriculumLesson(CurriculumVO vo) throws Exception;

    //과정 사용자(관리자) 목록
    List selectCurriculumMng(CurriculumVO vo) throws Exception;

    //교원배정 목록
    List selectCurriculumFaculty(CurriculumVO vo) throws Exception;

    //교원배정(중복제거) 목록
    List selectCurriculumFacultyDp(CurriculumVO vo) throws Exception;

    //총괄평가 목록
    List selectTotalEvaluation(CurriculumVO vo) throws Exception;

    //수업참여도 목록
    List selectAttendbbs(CurriculumVO vo) throws Exception;

    //교재 및 부교재 목록
    List selectBookbbs(CurriculumVO vo) throws Exception;

	//수강신청 등록
    void insertCurriculumMember(CurriculumVO vo) throws Exception;

    //수강신청 첨부 서류 재처리
    void updateCurriculumMember(CurriculumVO vo) throws Exception;

	//중복 수강신청 조회
    int selectCurriculumDuplicationMemberCnt(CurriculumVO searchVO);

    //수강신청 조회
    CurriculumVO selectCurriculumMemberDetail(CurriculumVO vo) throws Exception;

    // 오늘의 수업
    List selectTodayCrclList(CurriculumVO vo) throws Exception;

    /**
	 * curriculumMember 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return homework 총 갯수
	 * @exception
	 */
    int selectHomeworkTotCnt(CurriculumVO searchVO);

	/**
	 * curriculumMember 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return homework 목록
	 * @exception Exception
	 */
    List selectHomeworkList(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 제출 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    List selectHomeworkSubmitWaitingList(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 평가 대기 학생 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    List selectHomeworkTestWaitingList(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제를 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 등록
	 * @exception Exception
	 */
    public void insertHomeworkArticle(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    CurriculumVO selectHomeworkArticle(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제를 삭제한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    void deleteHomeworkArticle(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    public void updateHomeworkArticle(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 제출 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    List selectHomeworkSubjectList(CurriculumVO searchVO) throws Exception;

    /**
	 * 학생 공개를 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    public void updateStuOpenAt(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 후기 선정을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    public void updateCommentPickAt(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 피드백을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    public void updateFdb(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 제출 상세 내용을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homework 목록
	 * @exception Exception
	 */
    CurriculumVO selectHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception;
    
    // 과제후기 상세
    CurriculumVO selectCourseReviewAtView(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 제출을 등록한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 등록
	 * @exception Exception
	 */
    public void insertHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception;

    /**
	 * 과제 제출을 수정한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    
    // 
    public void updateHomeworkSubmitArticle(CurriculumVO searchVO) throws Exception;
    
    // 과정후기를 등록한다.
    public void insertCourseReview(CurriculumVO searchVO) throws Exception;

    // 과정후기를 수정한다.
    public void updateCourseReview(CurriculumVO searchVO) throws Exception;
    
    // 과정후기를 삭제한다.(use_at -> N 처리)
    public void deleteCourseReview(CurriculumVO searchVO) throws Exception;
    
    
    //과제평가
    public List homeworkScoreList(CurriculumVO searchVO) throws Exception;

    //과제평가 - 학생 점수 요약
    public List homeworkScoreSum(CurriculumVO searchVO) throws Exception;

    //과제평가 - 성적반영
    public void updateScoreApply(CurriculumVO searchVO) throws Exception;

    /**
	 * 반 개수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 수정
	 * @exception Exception
	 */
    int selectCurriculumMemberGroupCnt(CurriculumVO searchVO);

    // My > 교육과정이력 조회
    List selectMyCurriculumHistoryList(CurriculumVO vo) throws Exception;

    // My > 교육과정이력 조회 count
    List selectMyCurriculumHistoryListCount(CurriculumVO vo) throws Exception;

 	// My > 교육과정이력 - 총 과정수, 과정중 count
    int selectMyCurriculumHistoryCnt(CurriculumVO vo) throws Exception;

    //학생 과제 공개 개수
    int selectHomeworkCommentPickAtCnt(CurriculumVO searchVO);

    /**
	 * 학생 과제 공개 리스트를 조회.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return homeworksubmit 목록
	 * @exception Exception
	 */
    List selectHomeworkCommentPickAtList(CurriculumVO searchVO) throws Exception;

    // 과정후기 목록
    List<?> selectCourseReviewList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception;
    
    // 과정후기 공지 목록
    public List<?> selectCourseReviewNoticeList(CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception;
    
    //과정후기 목록 개수
    int selectCourseReviewCnt(CurriculumVO searchVO,HttpServletRequest request, HttpServletResponse response);
    
    public List<EgovMap> selectReportList(CurriculumVO searchVO) throws Exception;

    public int selectReportListCnt(CurriculumVO searchVO) throws Exception;

    public List<EgovMap> selectAdminReportList(CurriculumVO searchVO) throws Exception;

    public int selectAdminReportListCnt(CurriculumVO searchVO) throws Exception;

    //교육과정 통계(운영보고서)
    EgovMap curriculumSts(CurriculumVO vo) throws Exception;
    
    //과정 목록 - 학생기준
    List<EgovMap> selectMyCurriculumList(CurriculumVO vo) throws Exception;
    
    //과정 목록 수 - 학생기준
    int selectMyCurriculumListCnt(CurriculumVO vo) throws Exception;
    
    void downMemberZipFile(HashMap<String, Object> map ) throws Exception; 
    
    EgovMap curriculumStatusCheck(CurriculumVO vo) throws Exception;
    
    EgovMap curriculumSurveyCheck(CurriculumVO vo) throws Exception;
}
