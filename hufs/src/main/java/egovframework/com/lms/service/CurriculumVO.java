package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumVO.java
 * @Description : Curriculum VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public class CurriculumVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;

    /** CRCL_ID */
    private java.lang.String crclId;

    /** CRCLB_ID */
    private java.lang.String crclbId;

    /** GRADE_NUM */
    private java.lang.String gradeNum;

    /** CRCL_NM */
    private java.lang.String crclNm;

    /** CRCL_NM */
    private java.lang.String crclbNm;

    /** SYS_CODE */
    private java.lang.String sysCode;

    private List<String> SysCodeList;

    /** 과정체계코드경로 */
    private java.lang.String sysCodePath;

    /** 과정체계코드명경로 */
    private java.lang.String sysCodeNmPath;

    /** CRCL_YEAR */
    private java.lang.String crclYear;

    /** CRCL_TERM */
    private java.lang.String crclTerm;

    //학기명
    private java.lang.String crclTermNm;

    /** CRCL_LANG */
    private java.lang.String crclLang;

    /** 언어명 */
    private java.lang.String crclLangNm;

    /** START_DATE */
    private java.lang.String startDate;

    /** END_DATE */
    private java.lang.String endDate;

    /** TOTAL_TIME */
    private java.lang.String totalTime;

    /** WEEK_NUM */
    private java.lang.String weekNum;

    /** DAY_TIME */
    private java.lang.String dayTime;

    /** CAMPUS_ID */
    private java.lang.String campusId;
    private java.lang.String searchCampusId;

    //캠퍼스명
    private java.lang.String campusNm;

    /** CAMPUS_PLACE */
    private java.lang.String campusPlace;
    
    //캠퍼스 시간표 사용 여부
    private java.lang.String campustimeUseAt;

    /** HOST_CODE */
    private java.lang.String hostCode;

    //주관기관명
    private java.lang.String hostCodeNm;

    /** CRCL_GOAL */
    private java.lang.String crclGoal;

    /** CRCL_EFFECT */
    private java.lang.String crclEffect;

    /** TARGET_TYPE */
    private java.lang.String targetType;

    //대상상세
    private java.lang.String targetDetail;

    //대상상세명
    private java.lang.String targetDetailNm;

    /** PLAN_START_DATE */
    private java.lang.String planStartDate;

    /** PLAN_END_DATE */
    private java.lang.String planEndDate;

    /** APPLY_START_DATE */
    private java.lang.String applyStartDate;

    /** APPLY_END_DATE */
    private java.lang.String applyEndDate;

    /** APPLY_MAX_CNT */
    private java.lang.String applyMaxCnt;

    /** ASSIGN_AT */
    private java.lang.String assignAt;

    /** TUITION_AT */
    private java.lang.String tuitionAt;

    /** TUITION_FEES */
    private java.lang.String tuitionFees;

    /** REGISTRATION_FEES */
    private java.lang.String registrationFees;

    /** TUITION_START_DATE */
    private java.lang.String tuitionStartDate;

    /** TUITION_END_DATE */
    private java.lang.String tuitionEndDate;

    /** CRCL_OUTCOME */
    private java.lang.String crclOutcome;

    //과정성과명
    private java.lang.String crclOutcomeNm;

    /** PROCESS_STTUS_CODE */
    private java.lang.String processSttusCode;

    /** USE_AT */
    private java.lang.String useAt;

    /** PHOTO_STRE_FILE_NM */
    private String photoStreFileNm;

    /** FRST_REGISTER_ID */
    private java.lang.String frstRegisterId;

    /** FRST_REGISTER_PNTTM */
    private java.util.Date frstRegisterPnttm;

    /** LAST_UPDUSR_ID */
    private java.lang.String lastUpdusrId;

    /** LAST_UPDUSR_PNTTM */
    private java.util.Date lastUpdusrPnttm;

    //학점인정여부
    private java.lang.String gradeAt;

    //이수구분
    private java.lang.String division;

    //이수구분명
    private java.lang.String divisionNm;

    //관리구분
    private java.lang.String control;

    //관리구분명
    private java.lang.String controlNm;

    //성적처리기준
    private java.lang.String gradeType;

    //성적처리기준명
    private java.lang.String gradeTypeNm;

    /** PROJECT_AT */
    private java.lang.String projectAt;

    /** TOTAL_TIME_AT */
    private java.lang.String totalTimeAt;

    //책임교원Id
    private List<String> userIdList;

    //부책임교원Id
    private List<String> userIdList2;

    //지원비 비목ID
    private List<String> expIdList;

    //지원비 비목 코드
    private List<String> typeCodeList;

    //계정 목
    private List<String> accountCodeList;

    //금액
    private List<String> amountList;

    //산출근거
    private List<String> reasonList;

    //비고
    private List<String> etcList;

    //책임교원Id
    private java.lang.String userId;

    //과정권한코드
    private java.lang.String manageCode;

    //지원비 비목ID
    private java.lang.String expId;

    //지원비 비목 코드
    private java.lang.String typeCode;

    //계정 목
    private java.lang.String accountCode;

    //금액
    private java.lang.String amount;

    //산출근거
    private java.lang.String reason;

    //비고
    private java.lang.String etc;

    //대분류검색
    private java.lang.String searchSysCode1;

    //중분류검색
    private java.lang.String searchSysCode2;

    //소분류검색
    private java.lang.String searchSysCode3;

    //이수구분검색
    private java.lang.String searchDivision;

    //관리구분검색
    private java.lang.String searchControl;

    //기본과정명 검색
    private java.lang.String searchcrclbNm;

    //과정명 검색
    private java.lang.String searchCrclNm;

    //기본과정코드검색
    private java.lang.String searchcrclbId;

    //책임교원검색(ID)
    private java.lang.String searchUserId;

    //책임교원검색(이름)
    private java.lang.String searchUserNm;

    //학생 검색(이름)
    private java.lang.String searchStudentUserNm;

    //주관기관검색
    private java.lang.String searchHostCode;

    //학생 소속 거맥
    private java.lang.String searchStudentHostCode;

    //언어검색
    private java.lang.String searchCrclLang;

    //승인상태검색
    private java.lang.String searchSttus;

    //생년월일검색
    private java.lang.String searchBrthdy;

    //코멘트
    private java.lang.String comnt;

    //코멘트등록일자
    private java.util.Date comntPnttm;

    //교원명
    private java.lang.String userNm;

    /*승인여부
     *  Y : 승인,
     *  N : 반려,
     *  R : 대기,
     *  D : 승인취소
     *  T : 대기 이전
     */
    private java.lang.String aprvalAt;
    //승인일자
    private java.util.Date aprvalPnttm;

    //승인취소사유
    private java.lang.String aprvalDn;

    /** 승인결과 검색 */
    private java.lang.String searchAprvalAt;

    /** 프로젝트과정 검색 */
    private java.lang.String searchProjectAt;

    /** 시수과정 검색 */
    private java.lang.String searchTotalTimeAt;

    /** 관리여부 */
    private java.lang.String mngAt;

    //연도검색
    private java.lang.String searchCrclYear;

    //학기검색
    private java.lang.String searchCrclTerm;

    //시작일 검색
    private java.lang.String searchStartDate;

    //종료일 검색
    private java.lang.String searchEndDate;

    //수강신청 시작일 검색
    private java.lang.String searchApplyStartDate;

    //수강신청 시작일 종료
    private java.lang.String searchApplyEndDate;

    //대상검색
    private java.lang.String searchTargetType;

    //검색 input 값
    private java.lang.String searchInputValue;

    //대상상세검색
    private java.lang.String searchTargetDetail;

    //과정상태
    private java.lang.String searchProcessSttusCode;

    //과정상태 date로 분기처리
    private java.lang.String processSttusCodeDate;
    private java.lang.String searchProcessSttusCodeDate;
    private Integer[] arrProcessSttusCodeDate;

    //총사업비
    private java.lang.Integer totAmount;

    //기타사항
    private java.lang.String etcCn;

    //절대평가여부
    private java.lang.String evaluationAt;

    //운영보고서유형
    private java.lang.String reportType;

    /** SURVEY_SATISFY_TYPE */
    private java.lang.String surveySatisfyType;
    
    /** 교원대상과정만족도설문유형ID */
    private java.lang.String professorSatisfyType;

    /** SURVEY_IND_AT */
    private java.lang.String surveyIndAt;

    /** SURVEY_IND */
    private java.lang.String surveyInd;

    /** STDNT_APLY_AT */
    private java.lang.String stdntAplyAt;

    //지원비명
    private java.lang.String typeCodeNm;

    //계정목명
    private java.lang.String accountCodeNm;

    //성적발표 시작일
    private java.lang.String resultStartDate;

    //성적발표 시작시간
    private java.lang.String resultStartTime;

    //성적발표 종료일
    private java.lang.String resultEndDate;

    //성적발표 종료시간
    private java.lang.String resultEndTime;

    //단원명
    private java.lang.String lessonNm;
    
    //단원명 순서
    private java.lang.Integer lessonOrder;
    
    //단원명 순서 rowspan 값
    private java.lang.String lessonOrderList;

    //단원명목록
    private List<String> lessonNmList;

    //학습내용
    private java.lang.String chasiNm;
    
    //학습내용  순서
    private java.lang.Integer chasiOrder;

    // 과정내용 전체 문자열 화
    private  String lessonContentList;
    
    //학습내용목록
    private List<String> chasiList;

    //시수목록
    private List<String> sisuList;

    //교원목록
    private List<String> facIdList;

    //교원명목록
    private List<String> facNmList;

    //교원명 시수 랜덤 값 목록(구분자)
    private List<String> positionList;

    //교원Id목록
    private java.lang.String facId;

    //교원명목록
    private java.lang.String facNm;

    //시수
    private java.lang.String sisu;

    //구분 값
    private java.lang.String position;

    //A+
    private java.lang.String aplus;

    //A
    private java.lang.String a;

    //B+
    private java.lang.String bplus;

    //B
    private java.lang.String b;

    //C+
    private java.lang.String cplus;

    //C
    private java.lang.String c;

    //D+
    private java.lang.String dplus;

    //D
    private java.lang.String d;

    //F
    private java.lang.String F;

    //PASS
    private java.lang.String pass;

    //FAIL
    private java.lang.String fail;

    //총괄평가ID
    private List<String> evtIdList;
    private java.lang.String evtId;

    //총괄평가명
    private List<String> evtNmList;
    private java.lang.String evtNm;

    //총괄평가 반영률
    private List<String> evtValList;
    private java.lang.String evtVal;

    //총괄평가 기준
    private List<String> evtStandList;
    private java.lang.String evtStand;

    //총괄평가기타타입
    private java.lang.String evtType;

    //게시판 횟수 목록
    private List<String> colectList;

    //게시판 횟수
    private java.lang.String colect;

    //게시판ID
    private List<String> bbsIdList;
    private java.lang.String bbsId;

    //게시판명
    private java.lang.String bbsNm;

    //수료기준 총괄점수
    private java.lang.String fnTot;
    private java.lang.String fnTotAt;

    //수료기준 출결
    private java.lang.String fnAttend;
    private java.lang.String fnAttendAt;

    //수료기준성적종합
    private java.lang.String fnGradeTot;
    private java.lang.String fnGradeTotAt;

    //수료기준성적과락
    private java.lang.String fnGradeFail;
    private java.lang.String fnGradeFailAt;

    //수료기준과제종합
    private java.lang.String fnHomework;
    private java.lang.String fnHomeworkAt;

    //교재기타설명
    private java.lang.String bookEtcText;

    //자체운영계획 및 규정
    private java.lang.String managePlan;

    //교재 게시물번호
    private List<String> nttNoList;
    private java.lang.String nttNo;

    //비교
    private java.lang.String cpr;

    private java.lang.String searchMyCrclAt;

    //승인상태
    private java.lang.String sttus;
    private java.lang.Integer confmSttusCode;
    private java.lang.String confmPnttm;

    //반
    private java.lang.Integer classCnt;
    private List<Integer> classCntList;
    private List<Integer> manageClassCntList;

    //조
    private java.lang.Integer groupCnt;
    private List<Integer> groupCntList;

    //담당교수
    private java.lang.String manageId;
    private List<String> manageIdList;
    private java.lang.String manageNm;
    private java.lang.String searchManageNm;

    //조장여부
    private java.lang.String groupLeaderAt;
    private List<String> groupLeaderAtList;

    //PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 FLAG
    private java.lang.String processSttusCodeZeroAt;

    //과정계획 대기 조회 여부
    private java.lang.String searchPlanStartDateBeforeAt;

    //수강신청 현황 코드
    private java.lang.Integer searchApplySttusCode;

    //과정계획서 상태 코드
    private java.lang.Integer searchPlanSttusCode;

    //수강신청승인 대기 여부
    private java.lang.String searchRequestAt;

    //수강신청관리 신청서
    private java.lang.String aplyFile;
    private java.lang.String aplyOriFile;

    //수강신청관리 계획서
    private java.lang.String planFile;
    private java.lang.String planOriFile;

	//수강신청관리 기타파일
    private java.lang.String etfFile;
    private java.lang.String etfOriFile;

    //승인 상태 학생 수
    private java.lang.Integer classStudentCnt;

    //학생 사이트 여부(processSttusCodeDate 값이 0,1,5 값을 제외처리)
    private java.lang.String studentPageAt;

	//종료된 강의 조회(processSttusCodeDate 값이 8 이상 값을 조회)
    private java.lang.String closeCurriculumAt;

    //나의 교육과정 여부(학생 : student, 교원 : teacher)
    private java.lang.String myCurriculumPageFlag;

    //나의 관심과정 여부
    private java.lang.String wishCurriculumPageAt;

    //사용자구분코드
	private String userSeCode;

	// 최근 접수 리스트 여부
	private String recentListAt;

	//메뉴ID
	private String menuId;

	//등록 구분 값
	private String insertFlag;

	//메인여부
	private String isMainFlag;

	//교수 확인등록 여부
	private String registAt;
	private String searchRegistAt;
	private String registPnttm;

	//강의 시작 시간
    private java.lang.String startTime;

    //강의 요일
    private java.lang.String lectureDay;

    //담당자 여부
    private java.lang.String managerAt;

    //과제ID
    private java.lang.String hwId;

    //과제ID List
    private List<String> hwIdList;

    //제출 과제ID
    private java.lang.String hwsId;

    //제출 과제ID List
    private List<String> arrHwsId;

    //과제 대기 구분(1:제출 대기, 2:평가 대기)
    private java.lang.String hwWaitingType;

    //과제제출그룹타입
    private java.lang.String hwType;

    //과제제출그룹타입명
    private java.lang.String hwTypeNm;

    //등록자ID검색
    private java.lang.String searchFrstRegisterId;

    //내가 신청한 과정 검색여부
    private java.lang.String searchMyCulAt;

    //탭메뉴 step
    private java.lang.String step;

    //수강신청 > 수강대상자 확정에 3번째 텝
    private java.lang.String thirdtabstep;

    /**
	 * 등록-액션
	 */
    private String registAction = "";

    /** NTT_SJ */
    private java.lang.String nttSj;

    //과제명담당언어
    private java.lang.String nttSjLang;

    /** NTT_CN */
    private java.lang.String nttCn;

    /** NTCR_NM */
    private java.lang.String ntcrNm;

    /**
	 * 임시첨부파일 그룹아이디
	 */
    private String fileGroupId = "";

    //일정ID
    private java.lang.String plId;

    //과제구분
    private java.lang.String hwCode;

    //과제구분명
    private java.lang.String hwCodeNm;

    //첨부파일 아이디
    private java.lang.String atchFileId;

    //학생공개
    private java.lang.String stuOpenAt;

    //후기선정여부
    private java.lang.String commentPickAt;

    //학생공개날짜
    private java.lang.String stuOpenDate;

    //과제오픈날짜
    private java.lang.String openDate;

    //과제오픈시간
    private java.lang.String openTime;

    //과제종료날짜
    private java.lang.String closeDate;

    //과제종료시간
    private java.lang.String closeTime;

    //과정후기여부
    private java.lang.String curriculumCommentAt;

    //수업주제
    private java.lang.String studySubject;

    // 수업시간표 검색 - 언어선택
    private java.lang.String searchSchAt;

    //리턴  상세페이지 여부
    private java.lang.String retViewAt;

    //제출 과제 피드백
    private java.lang.String fdb;

    //과제 점수
    private java.lang.String scr;

    private java.lang.String selectLangAt;

    //검색 과제 마감 시작일
    private java.lang.String searchStartCloseDate;

    //검색 과제 마감 검색 종료일
    private java.lang.String searchEndCloseDate;

    //검색 마감된 과제 조회
    private java.lang.String searchCloseHomework;

    //검색 과제유형
    private java.lang.String searchHwType;

    //검색 과제코드
    private java.lang.String searchHwCode;

    //검색 과제명
    private java.lang.String searchHomeworkNm;

    //검색 나의 과제
    private java.lang.String searchMyHomework;

    //검색 과제 시작일
    private java.lang.String searchOpenDate;

    //검색 과제 종료일
    private java.lang.String searchCloseDate;

    //검색 미제출 과제
    private java.lang.String searchNotSubmitAt;

    //검색 제출 과제
    private java.lang.String searchSubmitAt;

    //과제 페이지 페이징 flag
    private java.lang.String pagingFlag = "Y";

    //과제평가 여부
    private java.lang.String homeworkScoreAt;

    //order by 타입
    private java.lang.String orderByType;

    //성적반영여부
    private java.lang.String scoreApplyAt;

    //반 검색
    private java.lang.Integer searchClassCnt;

    private String attentionType;

    private String searchHomeworkSubmitNttSj;

    //관리자 페이지 flag 값
    private String adminPageFlag;

    //중간고사변환점수
    private double midtermScore;

    //중간고사변환점수 List
    private List<Double> midtermScoreList;

    //중간고사총점수
    private int midtermTotalScore;

    //중간고사총점수 List
    private List<Integer> midtermTotalScoreList;

    //기말고사변환점수
    private double finalScore;

    //기말고사변환점수 List
    private List<Double> finalScoreList;

    //기말고사총점수
    private int finalTotalScore;

    //기말고사총점수 List
    private List<Integer> finalTotalScoreList;

    //수시시험변환점수
    private double evaluationScore;

    //수시시험변환점수 List
    private List<Double> evaluationScoreList;

    //수시시험총점수
    private int evaluationTotalScore;

    //수시시험총점수 List
    private List<Integer> evaluationTotalScoreList;

    //출석변환점수
    private double attendScore;

    //출석변환점수 List
    private List<Double> attendScoreList;

    //확정등급
    private java.lang.String confirmGrade;

    //확정등급 List
    private List<String> confirmGradeList;

    //기타순서
    private int etcSn;

    //기타순서 List
    private List<Integer> etcSnList;

    //기타변환점수
    private double etcScore;

    //기타변환점수1
    private double etcScore1;

    //기타변환점수2
    private double etcScore2;

    //기타변환점수3
    private double etcScore3;

    //기타변환점수4
    private double etcScore4;

    //기타변환점수5
    private double etcScore5;

    //기타변환점수6
    private double etcScore6;

    //기타변환점수7
    private double etcScore7;

    //기타변환점수8
    private double etcScore8;

    //기타변환점수9
    private double etcScore9;

    //기타변환점수10
    private double etcScore10;

    //기타변환점수 List
    private List<Double> etcScoreList;

    //기타변환점수 List
    private List<Double> etcScoreList1;

    //기타변환점수 List
    private List<Double> etcScoreList2;

    //기타변환점수 List
    private List<Double> etcScoreList3;

    //기타변환점수 List
    private List<Double> etcScoreList4;

    //기타변환점수 List
    private List<Double> etcScoreList5;

    //기타변환점수 List
    private List<Double> etcScoreList6;

    //기타변환점수 List
    private List<Double> etcScoreList7;

    //기타변환점수 List
    private List<Double> etcScoreList8;

    //기타변환점수 List
    private List<Double> etcScoreList9;

    //기타변환점수 List
    private List<Double> etcScoreList10;

    //과정운영평가
    private java.lang.String crclEvtCn;

    private java.lang.String allSubmit;

    private java.lang.String searchCtgryId;
    
    //중간고사 만점점수
    private int midtermTopScore;

    //기말고사 만점점수
    private int finalTopScore;
    
    //수시시험 만점점수
    private int evaluationTopScore;
    
    //엑셀다운로드여부
    private java.lang.String excelAt;

    //파일일괄다운로드여부
    private java.lang.String zipFileAt;
    
    //과정만족도설문제출시작일
    private java.lang.String surveyStartDate;
    
    //과정만족도설문제출시작시간
    private java.lang.String surveyStartTime;
    
    //수료상태 값 검색
    private java.lang.String searchFinishAt;
    
    // 과정후기 SEQ
    private int courseReviewSeq;
    
    // 과정후기 제목
    private String courseReviewSj;
    
    // 과정후기 내용
    private String courseReviewCn;
    
    // 검색용 과정후기 제목
    private String searchCourseReviewSj;
    
    private String groupStreFileNm;
    
    // 공지여부
    private String noticeYn;
    
    private String ctgryId;
    
	public java.lang.String getCrclId() {
        return this.crclId;
    }

    public void setCrclId(java.lang.String crclId) {
        this.crclId = crclId;
    }
    

    public java.lang.String getZipFileAt() {
		return zipFileAt;
	}

	public void setZipFileAt(java.lang.String zipFileAt) {
		this.zipFileAt = zipFileAt;
	}

	public java.lang.String getCrclbId() {
        return this.crclbId;
    }

    public void setCrclbId(java.lang.String crclbId) {
        this.crclbId = crclbId;
    }

    public java.lang.String getCrclNm() {
        return this.crclNm;
    }

    public void setCrclNm(java.lang.String crclNm) {
        this.crclNm = crclNm;
    }

    public java.lang.String getSysCode() {
        return this.sysCode;
    }

    public void setSysCode(java.lang.String sysCode) {
        this.sysCode = sysCode;
    }

    public java.lang.String getCrclTerm() {
		return crclTerm;
	}

	public void setCrclTerm(java.lang.String crclTerm) {
		this.crclTerm = crclTerm;
	}

	public java.lang.String getCrclLang() {
        return this.crclLang;
    }

    public void setCrclLang(java.lang.String crclLang) {
        this.crclLang = crclLang;
    }

    public java.lang.String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(java.lang.String startDate) {
        this.startDate = startDate;
    }

    public java.lang.String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }

    public java.lang.String getCampusId() {
        return this.campusId;
    }

    public void setCampusId(java.lang.String campusId) {
        this.campusId = campusId;
    }

    public java.lang.String getSearchCampusId() {
        return this.searchCampusId;
    }

    public void setSearchCampusId(java.lang.String searchCampusId) {
        this.searchCampusId = searchCampusId;
    }

    public java.lang.String getCampusPlace() {
        return this.campusPlace;
    }

    public void setCampusPlace(java.lang.String campusPlace) {
        this.campusPlace = campusPlace;
    }

    public java.lang.String getHostCode() {
        return this.hostCode;
    }

    public void setHostCode(java.lang.String hostCode) {
        this.hostCode = hostCode;
    }

    public java.lang.String getCrclGoal() {
        return this.crclGoal;
    }

    public void setCrclGoal(java.lang.String crclGoal) {
        this.crclGoal = crclGoal;
    }

    public java.lang.String getCrclEffect() {
        return this.crclEffect;
    }

    public void setCrclEffect(java.lang.String crclEffect) {
        this.crclEffect = crclEffect;
    }

    public java.lang.String getTargetType() {
        return this.targetType;
    }

    public void setTargetType(java.lang.String targetType) {
        this.targetType = targetType;
    }

    public java.lang.String getPlanStartDate() {
        return this.planStartDate;
    }

    public void setPlanStartDate(java.lang.String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public java.lang.String getPlanEndDate() {
        return this.planEndDate;
    }

    public void setPlanEndDate(java.lang.String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public java.lang.String getApplyStartDate() {
        return this.applyStartDate;
    }

    public void setApplyStartDate(java.lang.String applyStartDate) {
        this.applyStartDate = applyStartDate;
    }

    public java.lang.String getApplyEndDate() {
        return this.applyEndDate;
    }

    public void setApplyEndDate(java.lang.String applyEndDate) {
        this.applyEndDate = applyEndDate;
    }

    public java.lang.String getCrclYear() {
		return crclYear;
	}

	public void setCrclYear(java.lang.String crclYear) {
		this.crclYear = crclYear;
	}

	public java.lang.String getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(java.lang.String totalTime) {
		this.totalTime = totalTime;
	}

	public java.lang.String getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(java.lang.String weekNum) {
		this.weekNum = weekNum;
	}

	public java.lang.String getDayTime() {
		return dayTime;
	}

	public void setDayTime(java.lang.String dayTime) {
		this.dayTime = dayTime;
	}

	public java.lang.String getApplyMaxCnt() {
		return applyMaxCnt;
	}

	public void setApplyMaxCnt(java.lang.String applyMaxCnt) {
		this.applyMaxCnt = applyMaxCnt;
	}

	public java.lang.String getAssignAt() {
        return this.assignAt;
    }

    public void setAssignAt(java.lang.String assignAt) {
        this.assignAt = assignAt;
    }

    public java.lang.String getTuitionAt() {
        return this.tuitionAt;
    }

    public void setTuitionAt(java.lang.String tuitionAt) {
        this.tuitionAt = tuitionAt;
    }

    public java.lang.String getTuitionFees() {
		return tuitionFees;
	}

	public void setTuitionFees(java.lang.String tuitionFees) {
		this.tuitionFees = tuitionFees;
	}

	public java.lang.String getRegistrationFees() {
		return registrationFees;
	}

	public void setRegistrationFees(java.lang.String registrationFees) {
		this.registrationFees = registrationFees;
	}

	public java.lang.String getTuitionStartDate() {
        return this.tuitionStartDate;
    }

    public void setTuitionStartDate(java.lang.String tuitionStartDate) {
        this.tuitionStartDate = tuitionStartDate;
    }

    public java.lang.String getTuitionEndDate() {
        return this.tuitionEndDate;
    }

    public void setTuitionEndDate(java.lang.String tuitionEndDate) {
        this.tuitionEndDate = tuitionEndDate;
    }

    public java.lang.String getCrclOutcome() {
        return this.crclOutcome;
    }

    public void setCrclOutcome(java.lang.String crclOutcome) {
        this.crclOutcome = crclOutcome;
    }

    public java.lang.String getProcessSttusCode() {
        return this.processSttusCode;
    }

    public void setProcessSttusCode(java.lang.String processSttusCode) {
        this.processSttusCode = processSttusCode;
    }

    public java.lang.String getUseAt() {
        return this.useAt;
    }

    public void setUseAt(java.lang.String useAt) {
        this.useAt = useAt;
    }

    public String getPhotoStreFileNm() {
        return this.photoStreFileNm;
    }

    public void setPhotoStreFileNm(String photoStreFileNm) {
        this.photoStreFileNm = photoStreFileNm;
    }

    public java.lang.String getFrstRegisterId() {
        return this.frstRegisterId;
    }

    public void setFrstRegisterId(java.lang.String frstRegisterId) {
        this.frstRegisterId = frstRegisterId;
    }

    public java.util.Date getFrstRegisterPnttm() {
        return this.frstRegisterPnttm;
    }

    public void setFrstRegisterPnttm(java.util.Date frstRegisterPnttm) {
        this.frstRegisterPnttm = frstRegisterPnttm;
    }

    public java.lang.String getLastUpdusrId() {
        return this.lastUpdusrId;
    }

    public void setLastUpdusrId(java.lang.String lastUpdusrId) {
        this.lastUpdusrId = lastUpdusrId;
    }

    public java.util.Date getLastUpdusrPnttm() {
        return this.lastUpdusrPnttm;
    }

    public void setLastUpdusrPnttm(java.util.Date lastUpdusrPnttm) {
        this.lastUpdusrPnttm = lastUpdusrPnttm;
    }

	public List<String> getSysCodeList() {
		return SysCodeList;
	}

	public void setSysCodeList(List<String> sysCodeList) {
		SysCodeList = sysCodeList;
	}

	public List<String> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}

	public List<String> getExpIdList() {
		return expIdList;
	}

	public void setExpIdList(List<String> expIdList) {
		this.expIdList = expIdList;
	}

	public List<String> getAccountCodeList() {
		return accountCodeList;
	}

	public void setAccountCodeList(List<String> accountCodeList) {
		this.accountCodeList = accountCodeList;
	}

	public List<String> getAmountList() {
		return amountList;
	}

	public void setAmountList(List<String> amountList) {
		this.amountList = amountList;
	}

	public List<String> getReasonList() {
		return reasonList;
	}

	public void setReasonList(List<String> reasonList) {
		this.reasonList = reasonList;
	}

	public List<String> getEtcList() {
		return etcList;
	}

	public void setEtcList(List<String> etcList) {
		this.etcList = etcList;
	}

	public java.lang.String getUserId() {
		return userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	public java.lang.String getManageCode() {
		return manageCode;
	}

	public void setManageCode(java.lang.String manageCode) {
		this.manageCode = manageCode;
	}

	public java.lang.String getExpId() {
		return expId;
	}

	public void setExpId(java.lang.String expId) {
		this.expId = expId;
	}

	public java.lang.String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(java.lang.String accountCode) {
		this.accountCode = accountCode;
	}

	public java.lang.String getAmount() {
		return amount;
	}

	public void setAmount(java.lang.String amount) {
		this.amount = amount;
	}

	public java.lang.String getReason() {
		return reason;
	}

	public void setReason(java.lang.String reason) {
		this.reason = reason;
	}

	public java.lang.String getEtc() {
		return etc;
	}

	public void setEtc(java.lang.String etc) {
		this.etc = etc;
	}

	public List<String> getTypeCodeList() {
		return typeCodeList;
	}

	public void setTypeCodeList(List<String> typeCodeList) {
		this.typeCodeList = typeCodeList;
	}

	public java.lang.String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(java.lang.String typeCode) {
		this.typeCode = typeCode;
	}

	public java.lang.String getSearchSysCode1() {
		return searchSysCode1;
	}

	public void setSearchSysCode1(java.lang.String searchSysCode1) {
		this.searchSysCode1 = searchSysCode1;
	}

	public java.lang.String getSearchSysCode2() {
		return searchSysCode2;
	}

	public void setSearchSysCode2(java.lang.String searchSysCode2) {
		this.searchSysCode2 = searchSysCode2;
	}

	public java.lang.String getSearchSysCode3() {
		return searchSysCode3;
	}

	public void setSearchSysCode3(java.lang.String searchSysCode3) {
		this.searchSysCode3 = searchSysCode3;
	}

	public java.lang.String getSearchDivision() {
		return searchDivision;
	}

	public void setSearchDivision(java.lang.String searchDivision) {
		this.searchDivision = searchDivision;
	}

	public java.lang.String getSearchControl() {
		return searchControl;
	}

	public void setSearchControl(java.lang.String searchControl) {
		this.searchControl = searchControl;
	}

	public java.lang.String getSearchcrclbNm() {
		return searchcrclbNm;
	}

	public void setSearchcrclbNm(java.lang.String searchcrclbNm) {
		this.searchcrclbNm = searchcrclbNm;
	}

	public java.lang.String getSearchcrclbId() {
		return searchcrclbId;
	}

	public void setSearchcrclbId(java.lang.String searchcrclbId) {
		this.searchcrclbId = searchcrclbId;
	}

	public java.lang.String getSearchCrclNm() {
		return searchCrclNm;
	}

	public void setSearchCrclNm(java.lang.String searchCrclNm) {
		this.searchCrclNm = searchCrclNm;
	}

	public java.lang.String getSearchUserId() {
		return searchUserId;
	}

	public void setSearchUserId(java.lang.String searchUserId) {
		this.searchUserId = searchUserId;
	}

	public java.lang.String getSearchHostCode() {
		return searchHostCode;
	}

	public void setSearchHostCode(java.lang.String searchHostCode) {
		this.searchHostCode = searchHostCode;
	}

	public java.lang.String getSearchStudentHostCode() {
		return searchStudentHostCode;
	}

	public void setSearchStudentHostCode(java.lang.String searchStudentHostCode) {
		this.searchStudentHostCode = searchStudentHostCode;
	}

	public java.lang.String getSearchCrclLang() {
		return searchCrclLang;
	}

	public void setSearchCrclLang(java.lang.String searchCrclLang) {
		this.searchCrclLang = searchCrclLang;
	}

	public java.lang.String getSearchSttus() {
		return searchSttus;
	}

	public void setSearchSttus(java.lang.String searchSttus) {
		this.searchSttus = searchSttus;
	}

	public java.lang.String getSearchBrthdy() {
		return searchBrthdy;
	}

	public void setSearchBrthdy(java.lang.String searchBrthdy) {
		this.searchBrthdy = searchBrthdy;
	}

	public java.lang.String getComnt() {
		return comnt;
	}

	public void setComnt(java.lang.String comnt) {
		this.comnt = comnt;
	}

	public java.lang.String getAprvalAt() {
		return aprvalAt;
	}

	public void setAprvalAt(java.lang.String aprvalAt) {
		this.aprvalAt = aprvalAt;
	}

	public java.lang.String getSysCodePath() {
		return sysCodePath;
	}

	public void setSysCodePath(java.lang.String sysCodePath) {
		this.sysCodePath = sysCodePath;
	}

	public java.lang.String getSysCodeNmPath() {
		return sysCodeNmPath;
	}

	public void setSysCodeNmPath(java.lang.String sysCodeNmPath) {
		this.sysCodeNmPath = sysCodeNmPath;
	}

	public java.lang.String getCrclLangNm() {
		return crclLangNm;
	}

	public void setCrclLangNm(java.lang.String crclLangNm) {
		this.crclLangNm = crclLangNm;
	}

	public java.lang.String getHostCodeNm() {
		return hostCodeNm;
	}

	public void setHostCodeNm(java.lang.String hostCodeNm) {
		this.hostCodeNm = hostCodeNm;
	}

	public java.lang.String getDivisionNm() {
		return divisionNm;
	}

	public void setDivisionNm(java.lang.String divisionNm) {
		this.divisionNm = divisionNm;
	}

	public java.lang.String getControlNm() {
		return controlNm;
	}

	public void setControlNm(java.lang.String controlNm) {
		this.controlNm = controlNm;
	}

	public java.lang.String getProjectAt() {
		return projectAt;
	}

	public void setProjectAt(java.lang.String projectAt) {
		this.projectAt = projectAt;
	}

	public java.lang.String getTotalTimeAt() {
		return totalTimeAt;
	}

	public void setTotalTimeAt(java.lang.String totalTimeAt) {
		this.totalTimeAt = totalTimeAt;
	}

	public java.lang.String getSearchProjectAt() {
		return searchProjectAt;
	}

	public void setSearchProjectAt(java.lang.String searchProjectAt) {
		this.searchProjectAt = searchProjectAt;
	}

	public java.lang.String getSearchTotalTimeAt() {
		return searchTotalTimeAt;
	}

	public void setSearchTotalTimeAt(java.lang.String searchTotalTimeAt) {
		this.searchTotalTimeAt = searchTotalTimeAt;
	}

	public java.lang.String getSearchAprvalAt() {
		return searchAprvalAt;
	}

	public void setSearchAprvalAt(java.lang.String searchAprvalAt) {
		this.searchAprvalAt = searchAprvalAt;
	}

	public java.util.Date getAprvalPnttm() {
		return aprvalPnttm;
	}

	public void setAprvalPnttm(java.util.Date aprvalPnttm) {
		this.aprvalPnttm = aprvalPnttm;
	}

	public java.lang.String getSearchUserNm() {
		return searchUserNm;
	}

	public void setSearchUserNm(java.lang.String searchUserNm) {
		this.searchUserNm = searchUserNm;
	}

	public java.lang.String getSearchStudentUserNm() {
		return searchStudentUserNm;
	}

	public void setSearchStudentUserNm(java.lang.String searchStudentUserNm) {
		this.searchStudentUserNm = searchStudentUserNm;
	}

	public java.lang.String getUserNm() {
		return userNm;
	}

	public void setUserNm(java.lang.String userNm) {
		this.userNm = userNm;
	}

	public java.lang.String getMngAt() {
		return mngAt;
	}

	public void setMngAt(java.lang.String mngAt) {
		this.mngAt = mngAt;
	}

	public java.lang.String getSearchCrclYear() {
		return searchCrclYear;
	}

	public void setSearchCrclYear(java.lang.String searchCrclYear) {
		this.searchCrclYear = searchCrclYear;
	}

	public java.lang.String getSearchCrclTerm() {
		return searchCrclTerm;
	}

	public void setSearchCrclTerm(java.lang.String searchCrclTerm) {
		this.searchCrclTerm = searchCrclTerm;
	}

	public java.lang.String getSearchStartDate() {
		return searchStartDate;
	}

	public void setSearchStartDate(java.lang.String searchStartDate) {
		this.searchStartDate = searchStartDate;
	}

	public java.lang.String getSearchEndDate() {
		return searchEndDate;
	}

	public void setSearchEndDate(java.lang.String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}

	public java.lang.String getSearchApplyStartDate() {
		return searchApplyStartDate;
	}

	public void setSearchApplyStartDate(java.lang.String searchApplyStartDate) {
		this.searchApplyStartDate= searchApplyStartDate;
	}

	public java.lang.String getSearchApplyEndDate() {
		return searchApplyEndDate;
	}

	public void setSearchApplyEndDate(java.lang.String searchApplyEndDate) {
		this.searchApplyEndDate= searchApplyEndDate;
	}

	public java.lang.String getSearchTargetType() {
		return searchTargetType;
	}

	public void setSearchTargetType(java.lang.String searchTargetType) {
		this.searchTargetType = searchTargetType;
	}

	public java.lang.String getSearchInputValue() {
		return searchInputValue;
	}

	public void setSearchInputValue(java.lang.String searchInputValue) {
		this.searchInputValue = searchInputValue;
	}

	public java.lang.String getSearchTargetDetail() {
		return searchTargetDetail;
	}

	public void setSearchTargetDetail(java.lang.String searchTargetDetail) {
		this.searchTargetDetail = searchTargetDetail;
	}

	public java.lang.String getSearchProcessSttusCode() {
		return searchProcessSttusCode;
	}

	public void setSearchProcessSttusCode(java.lang.String searchProcessSttusCode) {
		this.searchProcessSttusCode = searchProcessSttusCode;
	}

	public java.lang.String getProcessSttusCodeDate() {
		return processSttusCodeDate;
	}

	public void setProcessSttusCodeDate(java.lang.String processSttusCodeDate) {
		this.processSttusCodeDate = processSttusCodeDate;
	}

	public java.lang.String getSearchProcessSttusCodeDate() {
		return searchProcessSttusCodeDate;
	}

	public void setSearchProcessSttusCodeDate(java.lang.String searchProcessSttusCodeDate) {
		this.searchProcessSttusCodeDate = searchProcessSttusCodeDate;
	}

	public Integer[] getArrProcessSttusCodeDate() {
		return arrProcessSttusCodeDate;
	}

	public void setArrProcessSttusCodeDate(Integer[] arrProcessSttusCodeDate) {
		this.arrProcessSttusCodeDate = arrProcessSttusCodeDate;
	}

	public java.lang.Integer getTotAmount() {
		return totAmount;
	}

	public void setTotAmount(java.lang.Integer totAmount) {
		this.totAmount = totAmount;
	}

	public java.lang.String getCrclTermNm() {
		return crclTermNm;
	}

	public void setCrclTermNm(java.lang.String crclTermNm) {
		this.crclTermNm = crclTermNm;
	}

	public java.lang.String getEtcCn() {
		return etcCn;
	}

	public void setEtcCn(java.lang.String etcCn) {
		this.etcCn = etcCn;
	}

	public java.lang.String getCrclbNm() {
		return crclbNm;
	}

	public void setCrclbNm(java.lang.String crclbNm) {
		this.crclbNm = crclbNm;
	}

	public java.lang.String getDivision() {
		return division;
	}

	public void setDivision(java.lang.String division) {
		this.division = division;
	}

	public java.lang.String getControl() {
		return control;
	}

	public void setControl(java.lang.String control) {
		this.control = control;
	}

	public java.lang.String getGradeAt() {
		return gradeAt;
	}

	public void setGradeAt(java.lang.String gradeAt) {
		this.gradeAt = gradeAt;
	}

	public java.lang.String getTargetDetail() {
		return targetDetail;
	}

	public void setTargetDetail(java.lang.String targetDetail) {
		this.targetDetail = targetDetail;
	}

	public java.lang.String getTargetDetailNm() {
		return targetDetailNm;
	}

	public void setTargetDetailNm(java.lang.String targetDetailNm) {
		this.targetDetailNm = targetDetailNm;
	}

	public java.lang.String getGradeType() {
		return gradeType;
	}

	public void setGradeType(java.lang.String gradeType) {
		this.gradeType = gradeType;
	}

	public java.lang.String getGradeTypeNm() {
		return gradeTypeNm;
	}

	public void setGradeTypeNm(java.lang.String gradeTypeNm) {
		this.gradeTypeNm = gradeTypeNm;
	}

	public java.lang.String getEvaluationAt() {
		return evaluationAt;
	}

	public void setEvaluationAt(java.lang.String evaluationAt) {
		this.evaluationAt = evaluationAt;
	}

	public java.lang.String getReportType() {
		return reportType;
	}

	public void setReportType(java.lang.String reportType) {
		this.reportType = reportType;
	}

	public java.lang.String getSurveySatisfyType() {
		return surveySatisfyType;
	}

	public void setSurveySatisfyType(java.lang.String surveySatisfyType) {
		this.surveySatisfyType = surveySatisfyType;
	}

	public java.lang.String getSurveyIndAt() {
		return surveyIndAt;
	}

	public void setSurveyIndAt(java.lang.String surveyIndAt) {
		this.surveyIndAt = surveyIndAt;
	}

	public java.lang.String getSurveyInd() {
		return surveyInd;
	}

	public void setSurveyInd(java.lang.String surveyInd) {
		this.surveyInd = surveyInd;
	}

	public java.lang.String getStdntAplyAt() {
		return stdntAplyAt;
	}

	public void setStdntAplyAt(java.lang.String stdntAplyAt) {
		this.stdntAplyAt = stdntAplyAt;
	}

	public java.lang.String getCampusNm() {
		return campusNm;
	}

	public void setCampusNm(java.lang.String campusNm) {
		this.campusNm = campusNm;
	}

	public java.lang.String getTypeCodeNm() {
		return typeCodeNm;
	}

	public void setTypeCodeNm(java.lang.String typeCodeNm) {
		this.typeCodeNm = typeCodeNm;
	}

	public java.lang.String getAccountCodeNm() {
		return accountCodeNm;
	}

	public void setAccountCodeNm(java.lang.String accountCodeNm) {
		this.accountCodeNm = accountCodeNm;
	}

	public java.lang.String getCrclOutcomeNm() {
		return crclOutcomeNm;
	}

	public void setCrclOutcomeNm(java.lang.String crclOutcomeNm) {
		this.crclOutcomeNm = crclOutcomeNm;
	}

	public java.util.Date getComntPnttm() {
		return comntPnttm;
	}

	public void setComntPnttm(java.util.Date comntPnttm) {
		this.comntPnttm = comntPnttm;
	}

	public java.lang.String getAprvalDn() {
		return aprvalDn;
	}

	public void setAprvalDn(java.lang.String aprvalDn) {
		this.aprvalDn = aprvalDn;
	}

	public java.lang.String getResultStartDate() {
		return resultStartDate;
	}

	public void setResultStartDate(java.lang.String resultStartDate) {
		this.resultStartDate = resultStartDate;
	}

	public java.lang.String getResultStartTime() {
		return resultStartTime;
	}

	public void setResultStartTime(java.lang.String resultStartTime) {
		this.resultStartTime = resultStartTime;
	}

	public java.lang.String getResultEndDate() {
		return resultEndDate;
	}

	public void setResultEndDate(java.lang.String resultEndDate) {
		this.resultEndDate = resultEndDate;
	}

	public java.lang.String getResultEndTime() {
		return resultEndTime;
	}

	public void setResultEndTime(java.lang.String resultEndTime) {
		this.resultEndTime = resultEndTime;
	}

	public List<String> getUserIdList2() {
		return userIdList2;
	}

	public void setUserIdList2(List<String> userIdList2) {
		this.userIdList2 = userIdList2;
	}

	public List<String> getLessonNmList() {
		return lessonNmList;
	}

	public void setLessonNmList(List<String> lessonNmList) {
		this.lessonNmList = lessonNmList;
	}

	public List<String> getChasiList() {
		return chasiList;
	}

	public void setChasiList(List<String> chasiList) {
		this.chasiList = chasiList;
	}

	public java.lang.String getLessonNm() {
		return lessonNm;
	}

	public void setLessonNm(java.lang.String lessonNm) {
		this.lessonNm = lessonNm;
	}

	public java.lang.String getChasiNm() {
		return chasiNm;
	}

	public void setChasiNm(java.lang.String chasiNm) {
		this.chasiNm = chasiNm;
	}

	public List<String> getSisuList() {
		return sisuList;
	}

	public void setSisuList(List<String> sisuList) {
		this.sisuList = sisuList;
	}

	public List<String> getFacIdList() {
		return facIdList;
	}

	public void setFacIdList(List<String> facIdList) {
		this.facIdList = facIdList;
	}

	public List<String> getFacNmList() {
		return facNmList;
	}

	public void setFacNmList(List<String> facNmList) {
		this.facNmList = facNmList;
	}

	public java.lang.String getFacId() {
		return facId;
	}

	public void setFacId(java.lang.String facId) {
		this.facId = facId;
	}

	public java.lang.String getFacNm() {
		return facNm;
	}

	public void setFacNm(java.lang.String facNm) {
		this.facNm = facNm;
	}

	public java.lang.String getSisu() {
		return sisu;
	}

	public void setSisu(java.lang.String sisu) {
		this.sisu = sisu;
	}

	public List<String> getPositionList() {
		return positionList;
	}

	public void setPositionList(List<String> positionList) {
		this.positionList = positionList;
	}

	public java.lang.String getPosition() {
		return position;
	}

	public void setPosition(java.lang.String position) {
		this.position = position;
	}

	public java.lang.String getAplus() {
		return aplus;
	}

	public void setAplus(java.lang.String aplus) {
		this.aplus = aplus;
	}

	public java.lang.String getA() {
		return a;
	}

	public void setA(java.lang.String a) {
		this.a = a;
	}

	public java.lang.String getBplus() {
		return bplus;
	}

	public void setBplus(java.lang.String bplus) {
		this.bplus = bplus;
	}

	public java.lang.String getB() {
		return b;
	}

	public void setB(java.lang.String b) {
		this.b = b;
	}

	public java.lang.String getCplus() {
		return cplus;
	}

	public void setCplus(java.lang.String cplus) {
		this.cplus = cplus;
	}

	public java.lang.String getC() {
		return c;
	}

	public void setC(java.lang.String c) {
		this.c = c;
	}

	public java.lang.String getDplus() {
		return dplus;
	}

	public void setDplus(java.lang.String dplus) {
		this.dplus = dplus;
	}

	public java.lang.String getD() {
		return d;
	}

	public void setD(java.lang.String d) {
		this.d = d;
	}

	public java.lang.String getF() {
		return F;
	}

	public void setF(java.lang.String f) {
		F = f;
	}

	public java.lang.String getPass() {
		return pass;
	}

	public void setPass(java.lang.String pass) {
		this.pass = pass;
	}

	public java.lang.String getFail() {
		return fail;
	}

	public void setFail(java.lang.String fail) {
		this.fail = fail;
	}

	public List<String> getEvtIdList() {
		return evtIdList;
	}

	public void setEvtIdList(List<String> evtIdList) {
		this.evtIdList = evtIdList;
	}

	public List<String> getEvtNmList() {
		return evtNmList;
	}

	public void setEvtNmList(List<String> evtNmList) {
		this.evtNmList = evtNmList;
	}

	public List<String> getEvtValList() {
		return evtValList;
	}

	public void setEvtValList(List<String> evtValList) {
		this.evtValList = evtValList;
	}

	public List<String> getEvtStandList() {
		return evtStandList;
	}

	public void setEvtStandList(List<String> evtStandList) {
		this.evtStandList = evtStandList;
	}

	public String getEvtType() {
		return evtType;
	}

	public void setEvtType(String evtType) {
		this.evtType = evtType;
	}

	public java.lang.String getBbsId() {
		return bbsId;
	}

	public void setBbsId(java.lang.String bbsId) {
		this.bbsId = bbsId;
	}

	public java.lang.String getBbsNm() {
		return bbsNm;
	}

	public void setBbsNm(java.lang.String bbsNm) {
		this.bbsNm = bbsNm;
	}

	public java.lang.String getFnTot() {
		return fnTot;
	}

	public void setFnTot(java.lang.String fnTot) {
		this.fnTot = fnTot;
	}

	public java.lang.String getFnAttend() {
		return fnAttend;
	}

	public void setFnAttend(java.lang.String fnAttend) {
		this.fnAttend = fnAttend;
	}

	public java.lang.String getFnGradeTot() {
		return fnGradeTot;
	}

	public void setFnGradeTot(java.lang.String fnGradeTot) {
		this.fnGradeTot = fnGradeTot;
	}

	public java.lang.String getFnGradeFail() {
		return fnGradeFail;
	}

	public void setFnGradeFail(java.lang.String fnGradeFail) {
		this.fnGradeFail = fnGradeFail;
	}

	public java.lang.String getFnHomework() {
		return fnHomework;
	}

	public void setFnHomework(java.lang.String fnHomework) {
		this.fnHomework = fnHomework;
	}

	public java.lang.String getFnTotAt() {
		return fnTotAt;
	}

	public void setFnTotAt(java.lang.String fnTotAt) {
		this.fnTotAt = fnTotAt;
	}

	public java.lang.String getFnAttendAt() {
		return fnAttendAt;
	}

	public void setFnAttendAt(java.lang.String fnAttendAt) {
		this.fnAttendAt = fnAttendAt;
	}

	public java.lang.String getFnGradeTotAt() {
		return fnGradeTotAt;
	}

	public void setFnGradeTotAt(java.lang.String fnGradeTotAt) {
		this.fnGradeTotAt = fnGradeTotAt;
	}

	public java.lang.String getFnGradeFailAt() {
		return fnGradeFailAt;
	}

	public void setFnGradeFailAt(java.lang.String fnGradeFailAt) {
		this.fnGradeFailAt = fnGradeFailAt;
	}

	public java.lang.String getFnHomeworkAt() {
		return fnHomeworkAt;
	}

	public void setFnHomeworkAt(java.lang.String fnHomeworkAt) {
		this.fnHomeworkAt = fnHomeworkAt;
	}

	public List<String> getColectList() {
		return colectList;
	}

	public void setColectList(List<String> colectList) {
		this.colectList = colectList;
	}

	public java.lang.String getColect() {
		return colect;
	}

	public void setColect(java.lang.String colect) {
		this.colect = colect;
	}

	public java.lang.String getEvtId() {
		return evtId;
	}

	public void setEvtId(java.lang.String evtId) {
		this.evtId = evtId;
	}

	public java.lang.String getEvtNm() {
		return evtNm;
	}

	public void setEvtNm(java.lang.String evtNm) {
		this.evtNm = evtNm;
	}

	public java.lang.String getEvtVal() {
		return evtVal;
	}

	public void setEvtVal(java.lang.String evtVal) {
		this.evtVal = evtVal;
	}

	public java.lang.String getEvtStand() {
		return evtStand;
	}

	public void setEvtStand(java.lang.String evtStand) {
		this.evtStand = evtStand;
	}

	public List<String> getBbsIdList() {
		return bbsIdList;
	}

	public void setBbsIdList(List<String> bbsIdList) {
		this.bbsIdList = bbsIdList;
	}

	public java.lang.String getBookEtcText() {
		return bookEtcText;
	}

	public void setBookEtcText(java.lang.String bookEtcText) {
		this.bookEtcText = bookEtcText;
	}

	public java.lang.String getManagePlan() {
		return managePlan;
	}

	public void setManagePlan(java.lang.String managePlan) {
		this.managePlan = managePlan;
	}

	public List<String> getNttNoList() {
		return nttNoList;
	}

	public void setNttNoList(List<String> nttNoList) {
		this.nttNoList = nttNoList;
	}

	public java.lang.String getNttNo() {
		return nttNo;
	}

	public void setNttNo(java.lang.String nttNo) {
		this.nttNo = nttNo;
	}

	public java.lang.String getCpr() {
		return cpr;
	}

	public void setCpr(java.lang.String cpr) {
		this.cpr = cpr;
	}

	public java.lang.String getSearchMyCrclAt() {
		return searchMyCrclAt;
	}

	public void setSearchMyCrclAt(java.lang.String searchMyCrclAt) {
		this.searchMyCrclAt = searchMyCrclAt;
	}

	public java.lang.String getSttus() {
		return sttus;
	}

	public void setSttus(java.lang.String sttus) {
		this.sttus = sttus;
	}

	public java.lang.Integer getConfmSttusCode() {
		return confmSttusCode;
	}

	public void setConfmSttusCode(java.lang.Integer confmSttusCode) {
		this.confmSttusCode = confmSttusCode;
	}

	public java.lang.String getConfmPnttm() {
		return confmPnttm;
	}

	public void setConfmPnttm(java.lang.String confmPnttm) {
		this.confmPnttm = confmPnttm;
	}

	public java.lang.Integer getClassCnt() {
		return classCnt;
	}

	public void setClassCnt(java.lang.Integer classCnt) {
		this.classCnt = classCnt;
	}

	public List<Integer> getClassCntList() {
		return classCntList;
	}

	public void setClassCntList(List<Integer> classCntList) {
		this.classCntList = classCntList;
	}

	public List<Integer> getManageClassCntList() {
		return manageClassCntList;
	}

	public void setManageClassCntList(List<Integer> manageClassCntList) {
		this.manageClassCntList = manageClassCntList;
	}

	public java.lang.Integer getGroupCnt() {
		return groupCnt;
	}

	public void setGroupCnt(java.lang.Integer groupCnt) {
		this.groupCnt = groupCnt;
	}

	public List<Integer> getGroupCntList() {
		return groupCntList;
	}

	public void setGroupCntList(List<Integer> groupCntList) {
		this.groupCntList = groupCntList;
	}

	public java.lang.String getManageId() {
		return manageId;
	}

	public void setManageId(java.lang.String manageId) {
		this.manageId = manageId;
	}

	public java.lang.String getManageNm() {
		return manageNm;
	}

	public void setManageNm(java.lang.String manageNm) {
		this.manageNm = manageNm;
	}

	public java.lang.String getSearchManageNm() {
		return searchManageNm;
	}

	public void setSearchManageNm(java.lang.String searchManageNm) {
		this.searchManageNm = searchManageNm;
	}

	public List<String> getManageIdList() {
		return manageIdList;
	}

	public void setManageIdList(List<String> manageIdList) {
		this.manageIdList = manageIdList;
	}

	public java.lang.String getGroupLeaderAt() {
		return groupLeaderAt;
	}

	public void setGroupLeaderAt(java.lang.String groupLeaderAt) {
		this.groupLeaderAt = groupLeaderAt;
	}

	public List<String> getGroupLeaderAtList() {
		return groupLeaderAtList;
	}

	public void setGroupLeaderAtList(List<String> groupLeaderAtList) {
		this.groupLeaderAtList = groupLeaderAtList;
	}

	public java.lang.String getProcessSttusCodeZeroAt() {
		return processSttusCodeZeroAt;
	}

	public void setProcessSttusCodeZeroAt(java.lang.String processSttusCodeZeroAt) {
		this.processSttusCodeZeroAt = processSttusCodeZeroAt;
	}

	public java.lang.String getSearchPlanStartDateBeforeAt() {
		return searchPlanStartDateBeforeAt;
	}

	public void setSearchPlanStartDateBeforeAt(java.lang.String searchPlanStartDateBeforeAt) {
		this.searchPlanStartDateBeforeAt = searchPlanStartDateBeforeAt;
	}

	public java.lang.Integer getSearchApplySttusCode() {
		return searchApplySttusCode;
	}

	public void setSearchApplySttusCode(java.lang.Integer searchApplySttusCode) {
		this.searchApplySttusCode = searchApplySttusCode;
	}

	public java.lang.Integer getSearchPlanSttusCode() {
		return searchPlanSttusCode;
	}

	public void setSearchPlanSttusCode(java.lang.Integer searchPlanSttusCode) {
		this.searchPlanSttusCode = searchPlanSttusCode;
	}

	public java.lang.String getSearchRequestAt() {
		return searchRequestAt;
	}

	public void setSearchRequestAt(java.lang.String searchRequestAt) {
		this.searchRequestAt = searchRequestAt;
	}

    public java.lang.String getAplyFile() {
		return aplyFile;
	}

	public void setAplyFile(java.lang.String aplyFile) {
		this.aplyFile = aplyFile;
	}

	public java.lang.String getAplyOriFile() {
		return aplyOriFile;
	}

	public void setAplyOriFile(java.lang.String aplyOriFile) {
		this.aplyOriFile = aplyOriFile;
	}

    public java.lang.String getPlanFile() {
		return planFile;
	}

	public void setPlanFile(java.lang.String planFile) {
		this.planFile = planFile;
	}

	public java.lang.String getPlanOriFile() {
		return planOriFile;
	}

	public void setPlanOriFile(java.lang.String planOriFile) {
		this.planOriFile = planOriFile;
	}

    public java.lang.String getEtfFile() {
		return etfFile;
	}

	public void setEtfFile(java.lang.String etfFile) {
		this.etfFile = etfFile;
	}

	public java.lang.String getEtfOriFile() {
		return etfOriFile;
	}

	public void setEtfOriFile(java.lang.String etfOriFile) {
		this.etfOriFile = etfOriFile;
	}

	public java.lang.Integer getClassStudentCnt() {
		return classStudentCnt;
	}

	public void setClassStudentCnt(java.lang.Integer classStudentCnt) {
		this.classStudentCnt = classStudentCnt;
	}

	public java.lang.String getStudentPageAt() {
		return studentPageAt;
	}

	public void setStudentPageAt(java.lang.String studentPageAt) {
		this.studentPageAt = studentPageAt;
	}

	public java.lang.String getCloseCurriculumAt() {
		return closeCurriculumAt;
	}

	public void setCloseCurriculumAt(java.lang.String closeCurriculumAt) {
		this.closeCurriculumAt = closeCurriculumAt;
	}

	public java.lang.String getMyCurriculumPageFlag() {
		return myCurriculumPageFlag;
	}

	public void setMyCurriculumPageFlag(java.lang.String myCurriculumPageFlag) {
		this.myCurriculumPageFlag = myCurriculumPageFlag;
	}

	public java.lang.String getWishCurriculumPageAt() {
		return wishCurriculumPageAt;
	}

	public void setWishCurriculumPageAt(java.lang.String wishCurriculumPageAt) {
		this.wishCurriculumPageAt = wishCurriculumPageAt;
	}

	public java.lang.String getUserSeCode() {
		return userSeCode;
	}

	public void setUserSeCode(java.lang.String userSeCode) {
		this.userSeCode = userSeCode;
	}

	public String getRecentListAt() {
		return recentListAt;
	}

	public void setRecentListAt(String recentListAt) {
		this.recentListAt = recentListAt;
	}

	public java.lang.String getGradeNum() {
		return gradeNum;
	}

	public void setGradeNum(java.lang.String gradeNum) {
		this.gradeNum = gradeNum;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getInsertFlag() {
		return insertFlag;
	}

	public void setInsertFlag(String insertFlag) {
		this.insertFlag = insertFlag;
	}

	public String getIsMainFlag() {
		return isMainFlag;
	}

	public void setIsMainFlag(String isMainFlag) {
		this.isMainFlag = isMainFlag;
	}

	public String getRegistAt() {
		return registAt;
	}

	public void setRegistAt(String registAt) {
		this.registAt = registAt;
	}

	public String getSearchRegistAt() {
		return searchRegistAt;
	}

	public void setSearchRegistAt(String searchRegistAt) {
		this.searchRegistAt = searchRegistAt;
	}

	public java.lang.String getStartTime() {
		return startTime;
	}

	public void setStartTime(java.lang.String startTime) {
		this.startTime = startTime;
	}

	public java.lang.String getLectureDay() {
		return lectureDay;
	}

	public void setLectureDay(java.lang.String lectureDay) {
		this.lectureDay = lectureDay;
	}

	public java.lang.String getManagerAt() {
		return managerAt;
	}

	public void setManagerAt(java.lang.String managerAt) {
		this.managerAt = managerAt;
	}

	public String getRegistPnttm() {
		return registPnttm;
	}

	public void setRegistPnttm(String registPnttm) {
		this.registPnttm = registPnttm;
	}

	public String getHwId() {
		return hwId;
	}

	public void setHwId(String hwId) {
		this.hwId = hwId;
	}

	public String getHwsId() {
		return hwsId;
	}

	public void setHwsId(String hwsId) {
		this.hwsId = hwsId;
	}

	public List<String> getArrHwsId() {
		return arrHwsId;
	}

	public void setArrHwsId(List<String> arrHwsId) {
		this.arrHwsId = arrHwsId;
	}

	public String getHwType() {
		return hwType;
	}

	public void setHwType(String hwType) {
		this.hwType = hwType;
	}

	public String getHwTypeNm() {
		return hwTypeNm;
	}

	public void setHwTypeNm(String hwTypeNm) {
		this.hwTypeNm = hwTypeNm;
	}

	public String getHwWaitingType() {
		return hwWaitingType;
	}

	public void setHwWaitingType(String hwWaitingType) {
		this.hwWaitingType = hwWaitingType;
	}

	public java.lang.String getSearchFrstRegisterId() {
		return searchFrstRegisterId;
	}

	public void setSearchFrstRegisterId(java.lang.String searchFrstRegisterId) {
		this.searchFrstRegisterId = searchFrstRegisterId;
	}

	public java.lang.String getSearchMyCulAt() {
		return searchMyCulAt;
	}

	public void setSearchMyCulAt(java.lang.String searchMyCulAt) {
		this.searchMyCulAt = searchMyCulAt;
	}

	public String getRegistAction() {
		return registAction;
	}

	public void setRegistAction(String registAction) {
		this.registAction = registAction;
	}

	public java.lang.String getNttSj() {
        return this.nttSj;
    }

    public void setNttSj(java.lang.String nttSj) {
        this.nttSj = nttSj;
    }

    public java.lang.String getNttSjLang() {
        return this.nttSjLang;
    }

    public void setNttSjLang(java.lang.String nttSjLang) {
        this.nttSjLang = nttSjLang;
    }

    public java.lang.String getNttCn() {
        return this.nttCn;
    }

    public void setNttCn(java.lang.String nttCn) {
        this.nttCn = nttCn;
    }

    public java.lang.String getNtcrNm() {
        return this.ntcrNm;
    }

    public void setNtcrNm(java.lang.String ntcrNm) {
        this.ntcrNm = ntcrNm;
    }

    public String getFileGroupId() {
		return fileGroupId;
	}

	public void setFileGroupId(String fileGroupId) {
		this.fileGroupId = fileGroupId;
	}

    public java.lang.String getPlId() {
		return plId;
	}

	public void setPlId(java.lang.String plId) {
		this.plId = plId;
	}

	public java.lang.String getHwCode() {
		return hwCode;
	}

	public void setHwCode(java.lang.String hwCode) {
		this.hwCode = hwCode;
	}

	public java.lang.String getHwCodeNm() {
		return hwCodeNm;
	}

	public void setHwCodeNm(java.lang.String hwCodeNm) {
		this.hwCodeNm = hwCodeNm;
	}

	public java.lang.String getAtchFileId() {
		return atchFileId;
	}

	public void setAtchFileId(java.lang.String atchFileId) {
		this.atchFileId = atchFileId;
	}

	public java.lang.String getStuOpenAt() {
		return stuOpenAt;
	}

	public void setStuOpenAt(java.lang.String stuOpenAt) {
		this.stuOpenAt = stuOpenAt;
	}

	public java.lang.String getCommentPickAt() {
		return commentPickAt;
	}

	public void setCommentPickAt(java.lang.String commentPickAt) {
		this.commentPickAt = commentPickAt;
	}

	public java.lang.String getStuOpenDate() {
		return stuOpenDate;
	}

	public void setStuOpenDate(java.lang.String stuOpenDate) {
		this.stuOpenDate = stuOpenDate;
	}

	public java.lang.String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(java.lang.String openDate) {
		this.openDate = openDate;
	}

	public java.lang.String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(java.lang.String openTime) {
		this.openTime = openTime;
	}

	public java.lang.String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(java.lang.String closeDate) {
		this.closeDate = closeDate;
	}

	public java.lang.String getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(java.lang.String closeTime) {
		this.closeTime = closeTime;
	}

	public java.lang.String getCurriculumCommentAt() {
		return curriculumCommentAt;
	}

	public void setCurriculumCommentAt(java.lang.String curriculumCommentAt) {
		this.curriculumCommentAt = curriculumCommentAt;
	}

	public java.lang.String getStudySubject() {
		return studySubject;
	}

	public void setStudySubject(java.lang.String studySubject) {
		this.studySubject = studySubject;
	}

	public java.lang.String getStep() {
        return this.step;
    }

    public void setStep(java.lang.String step) {
        this.step = step;
    }

    public java.lang.String getThirdtabstep() {
        return this.thirdtabstep;
    }

    public void setThirdtabstep(java.lang.String thirdtabstep) {
        this.thirdtabstep = thirdtabstep;
    }

	public java.lang.String getSearchSchAt() {
		return searchSchAt;
	}

	public void setSearchSchAt(java.lang.String searchSchAt) {
		this.searchSchAt = searchSchAt;
	}

	public java.lang.String getRetViewAt() {
		return retViewAt;
	}

	public void setRetViewAt(java.lang.String retViewAt) {
		this.retViewAt = retViewAt;
	}

	public java.lang.String getFdb() {
		return fdb;
	}

	public void setFdb(java.lang.String fdb) {
		this.fdb = fdb;
	}

	public java.lang.String getScr() {
		return scr;
	}

	public void setScr(java.lang.String scr) {
		this.scr = scr;
	}

	public java.lang.String getSelectLangAt() {
		return selectLangAt;
	}

	public void setSelectLangAt(java.lang.String selectLangAt) {
		this.selectLangAt = selectLangAt;
	}

	public java.lang.String getSearchStartCloseDate() {
		return searchStartCloseDate;
	}

	public void setSearchStartCloseDate(java.lang.String searchStartCloseDate) {
		this.searchStartCloseDate = searchStartCloseDate;
	}

	public java.lang.String getSearchEndCloseDate() {
		return searchEndCloseDate;
	}

	public void setSearchEndCloseDate(java.lang.String searchEndCloseDate) {
		this.searchEndCloseDate = searchEndCloseDate;
	}

	public java.lang.String getSearchCloseHomework() {
		return searchCloseHomework;
	}

	public void setSearchCloseHomework(java.lang.String searchCloseHomework) {
		this.searchCloseHomework = searchCloseHomework;
	}

	public java.lang.String getSearchHwType() {
		return searchHwType;
	}

	public void setSearchHwType(java.lang.String searchHwType) {
		this.searchHwType = searchHwType;
	}

	public java.lang.String getSearchHwCode() {
		return searchHwCode;
	}

	public void setSearchHwCode(java.lang.String searchHwCode) {
		this.searchHwCode = searchHwCode;
	}

	public java.lang.String getSearchHomeworkNm() {
		return searchHomeworkNm;
	}

	public void setSearchHomeworkNm(java.lang.String searchHomeworkNm) {
		this.searchHomeworkNm = searchHomeworkNm;
	}

	public java.lang.String getSearchMyHomework() {
		return searchMyHomework;
	}

	public void setSearchMyHomework(java.lang.String searchMyHomework) {
		this.searchMyHomework = searchMyHomework;
	}

	public java.lang.String getSearchOpenDate() {
		return searchOpenDate;
	}

	public void setSearchOpenDate(java.lang.String searchOpenDate) {
		this.searchOpenDate = searchOpenDate;
	}

	public java.lang.String getSearchCloseDate() {
		return searchCloseDate;
	}

	public void setSearchCloseDate(java.lang.String searchCloseDate) {
		this.searchCloseDate = searchCloseDate;
	}

	public java.lang.String getSearchNotSubmitAt() {
		return searchNotSubmitAt;
	}

	public void setSearchNotSubmitAt(java.lang.String searchNotSubmitAt) {
		this.searchNotSubmitAt = searchNotSubmitAt;
	}

	public java.lang.String getSearchSubmitAt() {
		return searchSubmitAt;
	}

	public void setSearchSubmitAt(java.lang.String searchSubmitAt) {
		this.searchSubmitAt = searchSubmitAt;
	}

	public java.lang.String getPagingFlag() {
		return pagingFlag;
	}

	public void setPagingFlag(java.lang.String pagingFlag) {
		this.pagingFlag = pagingFlag;
	}

	public java.lang.String getHomeworkScoreAt() {
		return homeworkScoreAt;
	}

	public void setHomeworkScoreAt(java.lang.String homeworkScoreAt) {
		this.homeworkScoreAt = homeworkScoreAt;
	}

	public java.lang.String getOrderByType() {
		return orderByType;
	}

	public void setOrderByType(java.lang.String orderByType) {
		this.orderByType = orderByType;
	}

	public java.lang.String getScoreApplyAt() {
		return scoreApplyAt;
	}

	public void setScoreApplyAt(java.lang.String scoreApplyAt) {
		this.scoreApplyAt = scoreApplyAt;
	}

	public java.lang.Integer getSearchClassCnt() {
		return searchClassCnt;
	}

	public void setSearchClassCnt(java.lang.Integer searchClassCnt) {
		this.searchClassCnt = searchClassCnt;
	}

	public List<String> getHwIdList() {
		return hwIdList;
	}

	public void setHwIdList(List<String> hwIdList) {
		this.hwIdList = hwIdList;
	}

	public String getAttentionType() {
		return attentionType;
	}

	public void setAttentionType(String attentionType) {
		this.attentionType = attentionType;
	}

	public String getAdminPageFlag() {
		return adminPageFlag;
	}

	public void setAdminPageFlag(String adminPageFlag) {
		this.adminPageFlag = adminPageFlag;
	}

    public String getSearchHomeworkSubmitNttSj() {
		return searchHomeworkSubmitNttSj;
	}

	public void setSearchHomeworkSubmitNttSj(String searchHomeworkSubmitNttSj) {
		this.searchHomeworkSubmitNttSj = searchHomeworkSubmitNttSj;
	}

	public double getMidtermScore() {
		return midtermScore;
	}

	public void setMidtermScore(double midtermScore) {
		this.midtermScore = midtermScore;
	}

	public List<Double> getMidtermScoreList() {
		return midtermScoreList;
	}

	public void setMidtermScoreList(List<Double> midtermScoreList) {
		this.midtermScoreList = midtermScoreList;
	}

	public int getMidtermTotalScore() {
		return midtermTotalScore;
	}

	public void setMidtermTotalScore(int midtermTotalScore) {
		this.midtermTotalScore = midtermTotalScore;
	}

	public List<Integer> getMidtermTotalScoreList() {
		return midtermTotalScoreList;
	}

	public void setMidtermTotalScoreList(List<Integer> midtermTotalScoreList) {
		this.midtermTotalScoreList = midtermTotalScoreList;
	}

	public double getFinalScore() {
		return finalScore;
	}

	public void setFinalScore(double finalScore) {
		this.finalScore = finalScore;
	}

	public List<Double> getFinalScoreList() {
		return finalScoreList;
	}

	public void setFinalScoreList(List<Double> finalScoreList) {
		this.finalScoreList = finalScoreList;
	}

	public int getFinalTotalScore() {
		return finalTotalScore;
	}

	public void setFinalTotalScore(int finalTotalScore) {
		this.finalTotalScore = finalTotalScore;
	}

	public List<Integer> getFinalTotalScoreList() {
		return finalTotalScoreList;
	}

	public void setFinalTotalScoreList(List<Integer> finalTotalScoreList) {
		this.finalTotalScoreList = finalTotalScoreList;
	}

	public double getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(double evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

	public List<Double> getEvaluationScoreList() {
		return evaluationScoreList;
	}

	public void setEvaluationScoreList(List<Double> evaluationScoreList) {
		this.evaluationScoreList = evaluationScoreList;
	}

	public int getEvaluationTotalScore() {
		return evaluationTotalScore;
	}

	public void setEvaluationTotalScore(int evaluationTotalScore) {
		this.evaluationTotalScore = evaluationTotalScore;
	}

	public List<Integer> getEvaluationTotalScoreList() {
		return evaluationTotalScoreList;
	}

	public void setEvaluationTotalScoreList(List<Integer> evaluationTotalScoreList) {
		this.evaluationTotalScoreList = evaluationTotalScoreList;
	}

	public double getAttendScore() {
		return attendScore;
	}

	public void setAttendScore(double attendScore) {
		this.attendScore = attendScore;
	}

	public List<Double> getAttendScoreList() {
		return attendScoreList;
	}

	public void setAttendScoreList(List<Double> attendScoreList) {
		this.attendScoreList = attendScoreList;
	}

	public java.lang.String getConfirmGrade() {
		return confirmGrade;
	}

	public void setConfirmGrade(java.lang.String confirmGrade) {
		this.confirmGrade = confirmGrade;
	}

	public List<String> getConfirmGradeList() {
		return confirmGradeList;
	}

	public void setConfirmGradeList(List<String> confirmGradeList) {
		this.confirmGradeList = confirmGradeList;
	}

	public int getEtcSn() {
		return etcSn;
	}

	public void setEtcSn(int etcSn) {
		this.etcSn = etcSn;
	}

	public List<Integer> getEtcSnList() {
		return etcSnList;
	}

	public void setEtcSnList(List<Integer> etcSnList) {
		this.etcSnList = etcSnList;
	}

	public double getEtcScore() {
		return etcScore;
	}

	public void setEtcScore(double etcScore) {
		this.etcScore = etcScore;
	}

	public List<Double> getEtcScoreList() {
		return etcScoreList;
	}

	public void setEtcScoreList(List<Double> etcScoreList) {
		this.etcScoreList = etcScoreList;
	}

	public List<Double> getEtcScoreList1() {
		return etcScoreList1;
	}

	public void setEtcScoreList1(List<Double> etcScoreList1) {
		this.etcScoreList1 = etcScoreList1;
	}

	public List<Double> getEtcScoreList2() {
		return etcScoreList2;
	}

	public void setEtcScoreList2(List<Double> etcScoreList2) {
		this.etcScoreList2 = etcScoreList2;
	}

	public List<Double> getEtcScoreList3() {
		return etcScoreList3;
	}

	public void setEtcScoreList3(List<Double> etcScoreList3) {
		this.etcScoreList3 = etcScoreList3;
	}

	public List<Double> getEtcScoreList4() {
		return etcScoreList4;
	}

	public void setEtcScoreList4(List<Double> etcScoreList4) {
		this.etcScoreList4 = etcScoreList4;
	}

	public List<Double> getEtcScoreList5() {
		return etcScoreList5;
	}

	public void setEtcScoreList5(List<Double> etcScoreList5) {
		this.etcScoreList5 = etcScoreList5;
	}

	public List<Double> getEtcScoreList6() {
		return etcScoreList6;
	}

	public void setEtcScoreList6(List<Double> etcScoreList6) {
		this.etcScoreList6 = etcScoreList6;
	}

	public List<Double> getEtcScoreList7() {
		return etcScoreList7;
	}

	public void setEtcScoreList7(List<Double> etcScoreList7) {
		this.etcScoreList7 = etcScoreList7;
	}

	public List<Double> getEtcScoreList8() {
		return etcScoreList8;
	}

	public void setEtcScoreList8(List<Double> etcScoreList8) {
		this.etcScoreList8 = etcScoreList8;
	}

	public List<Double> getEtcScoreList9() {
		return etcScoreList9;
	}

	public void setEtcScoreList9(List<Double> etcScoreList9) {
		this.etcScoreList9 = etcScoreList9;
	}

	public List<Double> getEtcScoreList10() {
		return etcScoreList10;
	}

	public void setEtcScoreList10(List<Double> etcScoreList10) {
		this.etcScoreList10 = etcScoreList10;
	}

	public double getEtcScore1() {
		return etcScore1;
	}

	public void setEtcScore1(double etcScore1) {
		this.etcScore1 = etcScore1;
	}

	public double getEtcScore2() {
		return etcScore2;
	}

	public void setEtcScore2(double etcScore2) {
		this.etcScore2 = etcScore2;
	}

	public double getEtcScore3() {
		return etcScore3;
	}

	public void setEtcScore3(double etcScore3) {
		this.etcScore3 = etcScore3;
	}

	public double getEtcScore4() {
		return etcScore4;
	}

	public void setEtcScore4(double etcScore4) {
		this.etcScore4 = etcScore4;
	}

	public double getEtcScore5() {
		return etcScore5;
	}

	public void setEtcScore5(double etcScore5) {
		this.etcScore5 = etcScore5;
	}

	public double getEtcScore6() {
		return etcScore6;
	}

	public void setEtcScore6(double etcScore6) {
		this.etcScore6 = etcScore6;
	}

	public double getEtcScore7() {
		return etcScore7;
	}

	public void setEtcScore7(double etcScore7) {
		this.etcScore7 = etcScore7;
	}

	public double getEtcScore8() {
		return etcScore8;
	}

	public void setEtcScore8(double etcScore8) {
		this.etcScore8 = etcScore8;
	}

	public double getEtcScore9() {
		return etcScore9;
	}

	public void setEtcScore9(double etcScore9) {
		this.etcScore9 = etcScore9;
	}

	public double getEtcScore10() {
		return etcScore10;
	}

	public void setEtcScore10(double etcScore10) {
		this.etcScore10 = etcScore10;
	}

	public java.lang.String getCrclEvtCn() {
		return crclEvtCn;
	}

	public void setCrclEvtCn(java.lang.String crclEvtCn) {
		this.crclEvtCn = crclEvtCn;
	}

	public java.lang.String getAllSubmit() {
		return allSubmit;
	}

	public void setAllSubmit(java.lang.String allSubmit) {
		this.allSubmit = allSubmit;
	}

	public java.lang.String getSearchCtgryId() {
		return searchCtgryId;
	}

	public void setSearchCtgryId(java.lang.String searchCtgryId) {
		this.searchCtgryId = searchCtgryId;
	}

	public int getMidtermTopScore() {
		return midtermTopScore;
	}

	public void setMidtermTopScore(int midtermTopScore) {
		this.midtermTopScore = midtermTopScore;
	}

	public int getFinalTopScore() {
		return finalTopScore;
	}

	public void setFinalTopScore(int finalTopScore) {
		this.finalTopScore = finalTopScore;
	}

	public int getEvaluationTopScore() {
		return evaluationTopScore;
	}

	public void setEvaluationTopScore(int evaluationTopScore) {
		this.evaluationTopScore = evaluationTopScore;
	}

	public java.lang.String getExcelAt() {
		return excelAt;
	}

	public void setExcelAt(java.lang.String excelAt) {
		this.excelAt = excelAt;
	}

	public java.lang.String getSurveyStartDate() {
		return surveyStartDate;
	}

	public void setSurveyStartDate(java.lang.String surveyStartDate) {
		this.surveyStartDate = surveyStartDate;
	}

	public java.lang.String getSurveyStartTime() {
		return surveyStartTime;
	}

	public void setSurveyStartTime(java.lang.String surveyStartTime) {
		this.surveyStartTime = surveyStartTime;
	}

	public java.lang.String getSearchFinishAt() {
		return searchFinishAt;
	}

	public void setSearchFinishAt(java.lang.String searchFinishAt) {
		this.searchFinishAt = searchFinishAt;
	}

	public java.lang.String getProfessorSatisfyType() {
		return professorSatisfyType;
	}

	public void setProfessorSatisfyType(java.lang.String professorSatisfyType) {
		this.professorSatisfyType = professorSatisfyType;
	}

	public java.lang.String getCampustimeUseAt() {
		return campustimeUseAt;
	}

	public void setCampustimeUseAt(java.lang.String campustimeUseAt) {
		this.campustimeUseAt = campustimeUseAt;
	}

	public java.lang.Integer getLessonOrder() {
		return lessonOrder;
	}

	public void setLessonOrder(java.lang.Integer lessonOrder) {
		this.lessonOrder = lessonOrder;
	}

	public java.lang.Integer getChasiOrder() {
		return chasiOrder;
	}

	public void setChasiOrder(java.lang.Integer chasiOrder) {
		this.chasiOrder = chasiOrder;
	}

	public java.lang.String getLessonOrderList() {
		return lessonOrderList;
	}

	public void setLessonOrderList(java.lang.String lessonOrderList) {
		this.lessonOrderList = lessonOrderList;
	}

	public String getLessonContentList() {
		return lessonContentList;
	}

	public void setLessonContentList(String lessonContentList) {
		this.lessonContentList = lessonContentList;
	}

	public int getCourseReviewSeq() {
		return courseReviewSeq;
	}

	public void setCourseReviewSeq(int courseReviewSeq) {
		this.courseReviewSeq = courseReviewSeq;
	}

	public String getCourseReviewSj() {
		return courseReviewSj;
	}

	public void setCourseReviewSj(String courseReviewSj) {
		this.courseReviewSj = courseReviewSj;
	}

	public String getCourseReviewCn() {
		return courseReviewCn;
	}

	public void setCourseReviewCn(String courseReviewCn) {
		this.courseReviewCn = courseReviewCn;
	}

	public String getSearchCourseReviewSj() {
		return searchCourseReviewSj;
	}

	public void setSearchCourseReviewSj(String searchCourseReviewSj) {
		this.searchCourseReviewSj = searchCourseReviewSj;
	}

	public String getGroupStreFileNm() {
		return groupStreFileNm;
	}

	public void setGroupStreFileNm(String groupStreFileNm) {
		this.groupStreFileNm = groupStreFileNm;
	}

	public String getNoticeYn() {
		return noticeYn;
	}

	public void setNoticeYn(String noticeYn) {
		this.noticeYn = noticeYn;
	}

	public String getCtgryId() {
		return ctgryId;
	}

	public void setCtgryId(String ctgryId) {
		this.ctgryId = ctgryId;
	}
}
