package egovframework.com.lms.service;

import java.util.List;
import egovframework.com.lms.service.BasefileVO;

/**
 * @Class Name : BasefileService.java
 * @Description : Basefile Business class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */
public interface BasefileService {
	
	/**
	 * basefile을 등록한다.
	 * @param vo - 등록할 정보가 담긴 BasefileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    String insertBasefile(BasefileVO vo) throws Exception;
    
    /**
	 * basefile을 수정한다.
	 * @param vo - 수정할 정보가 담긴 BasefileVO
	 * @return void형
	 * @exception Exception
	 */
    void updateBasefile(BasefileVO vo) throws Exception;
    
    /**
	 * basefile을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 BasefileVO
	 * @return void형 
	 * @exception Exception
	 */
    void deleteBasefile(BasefileVO vo) throws Exception;
    
    /**
	 * basefile을 조회한다.
	 * @param vo - 조회할 정보가 담긴 BasefileVO
	 * @return 조회한 basefile
	 * @exception Exception
	 */
    BasefileVO selectBasefile(BasefileVO vo) throws Exception;
    
    /**
	 * basefile 목록을 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return basefile 목록
	 * @exception Exception
	 */
    List selectBasefileList(BasefileVO searchVO) throws Exception;
    
    /**
	 * basefile 총 갯수를 조회한다.
	 * @param searchVO - 조회할 정보가 담긴 VO
	 * @return basefile 총 갯수
	 * @exception
	 */
    int selectBasefileListTotCnt(BasefileVO searchVO);
    
}
