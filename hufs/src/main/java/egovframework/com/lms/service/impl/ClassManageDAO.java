package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.SearchVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumDAO.java
 * @Description : Curriculum DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Repository("classManageDAO")
public class ClassManageDAO extends EgovAbstractDAO {

    public List selectClassCurriculumList(FileVO fileVo) throws Exception {
        return list("classManageDAO.selectClassCurriculumList", fileVo);
    }

    public List selectClassFileList(FileVO fileVo) throws Exception {
        return list("classManageDAO.selectClassFileList", fileVo);
    }

    public int selectClassFileListCnt(FileVO fileVo) throws Exception {
        return (Integer)select("classManageDAO.selectClassFileListCnt", fileVo);
    }

    public List selectSearchStudyFileList(FileVO fileVo) throws Exception {
        return list("classManageDAO.selectSearchStudyFileList", fileVo);
    }

    public List selectCurriculumStudyList(SearchVO searchVo) throws Exception {
        return list("classManageDAO.selectCurriculumStudyList", searchVo);
    }

    public int selectCurriculumStudyListCnt(SearchVO searchVo) throws Exception {
        return (Integer)select("classManageDAO.selectCurriculumStudyListCnt", searchVo);
    }

    public List selectCurriculumList(SearchVO searchVo) throws Exception {
        return list("classManageDAO.selectCurriculumList", searchVo);
    }

    public List selectCurriculumBaseList(SearchVO searchVo) throws Exception {
        return list("classManageDAO.selectCurriculumBaseList", searchVo);
    }

    public List selectCurriculumBoardList(SearchVO searchVo) throws Exception {
        return list("classManageDAO.selectCurriculumBoardList", searchVo);
    }

    public int selectCurriculumBoardListCnt(SearchVO searchVo) throws Exception {
        return (Integer)select("classManageDAO.selectCurriculumBoardListCnt", searchVo);
    }

    public List selectSurveyList(SearchVO searchVo) throws Exception {
        return list("classManageDAO.selectSurveyList", searchVo);
    }

    public int selectSurveyListCnt(SearchVO searchVo) throws Exception {
        return (Integer)select("classManageDAO.selectSurveyListCnt", searchVo);
    }

    public EgovMap selectCurriculumUserInfo(EgovMap map) throws Exception {
        return (EgovMap)select("classManageDAO.selectCurriculumUserInfo", map);
    }

    public List selectTeacherClassFileList(FileVO fileVo) throws Exception {
        return list("classManageDAO.selectTeacherClassFileList", fileVo);
    }

    public int selectTeacherClassFileListCnt(FileVO fileVo) throws Exception {
        return (Integer)select("classManageDAO.selectTeacherClassFileListCnt", fileVo);
    }

    public List selectStudentClassFileList(FileVO fileVo) throws Exception {
        return list("classManageDAO.selectStudentClassFileList", fileVo);
    }

    public int selectStudentClassFileListCnt(FileVO fileVo) throws Exception {
        return (Integer)select("classManageDAO.selectStudentClassFileListCnt", fileVo);
    }
}
