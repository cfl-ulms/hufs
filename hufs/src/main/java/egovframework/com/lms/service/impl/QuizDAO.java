package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.lms.service.QuizVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Repository("quizDAO")
public class QuizDAO extends EgovAbstractDAO {

    public String insertQuiz(QuizVO vo) throws Exception {
        return (String)insert("quizDAO.insertQuiz", vo);
    }
    
    public String insertQuizAnswer(QuizVO vo) throws Exception {
        return (String)insert("quizDAO.insertQuizAnswer", vo);
    }
    
	public int selectQuizListTotCnt(QuizVO searchVO) {
        return (Integer)select("quizDAO.selectQuizListTotCnt_S", searchVO);
    }
	
	public int selectQuizExam_Cnt(QuizVO searchVO) {
        return (Integer)select("quizDAO.selectQuizExam_Cnt", searchVO);
    }

    public List<QuizVO> selectQuizList(QuizVO searchVO) throws Exception {
        return (List<QuizVO>) list("quizDAO.selectQuizList", searchVO);
    }
    
    public List<QuizVO> selectBeforeQuizList(QuizVO searchVO) throws Exception {
        return (List<QuizVO>) list("quizDAO.selectBeforeQuizList", searchVO);
    }
    
    public List<QuizVO> selectPreQuizList(QuizVO searchVO) throws Exception {
        return (List<QuizVO>) list("quizDAO.selectPreQuizList", searchVO);
    }

    public QuizVO selectQuiz_S(QuizVO vo) throws Exception {
        return (QuizVO) select("quizDAO.selectQuiz_S", vo);
    }
    
    public void updateQuiz(QuizVO vo) throws Exception {
        update("quizDAO.updateQuiz", vo);
    }
    
    public void updateQuiz_D(QuizVO vo) throws Exception {
        update("quizDAO.updateQuiz_D", vo);
    }
    
    public void updateQuizAnswer(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizAnswer", vo);
    }

    public void updateQuizAnswer_D(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizAnswer_D", vo);
    }
    
    public void updateQuiz_Sort(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuiz_Sort", vo);
    }
    
    public void updateQuizAnswer_A(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizAnswer_A", vo);
    }
    
    public void updateQuizEnd(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizEnd", vo);
    }
    
    public void deleteOffQuizAnswer(QuizVO vo) throws Exception {
    	update("quizDAO.deleteOffQuizAnswer", vo);
    }
    
    public void deleteOffQuiz(QuizVO vo) throws Exception {
    	update("quizDAO.deleteOffQuiz", vo);
    }
    
    public void updateQuizExamAnswer(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizExamAnswer", vo);
    }
    
    
    public QuizVO selectQuizExam_S(QuizVO vo) throws Exception {
        return (QuizVO) select("quizDAO.selectQuizExam_S", vo);
    }
    
    
    public QuizVO selectPreQuizList_S(QuizVO vo) throws Exception {
        return (QuizVO) select("quizDAO.selectPreQuizList", vo);
    }
    
    public QuizVO selectQuizExam_Report(QuizVO vo) throws Exception {
        return (QuizVO) select("quizDAO.selectQuizExam_Report", vo);
    }
    
    public List<QuizVO> selectQuizResult_Report(QuizVO vo) throws Exception {
        return (List<QuizVO>) list("quizDAO.selectQuizResult_Report", vo);
    }
    
    public List<QuizVO> selectQuizExamAnswer_Cnt(QuizVO vo) throws Exception {
        return (List<QuizVO>) list("quizDAO.selectQuizExamAnswer_Cnt", vo);
    }
    
    public String insertQuizExam(QuizVO vo) throws Exception {
        return (String)insert("quizDAO.insertQuizExam", vo);
    }
    
    public void deleteQuizExam(QuizVO vo) throws Exception {
    	update("quizDAO.deleteQuizExam", vo);
    }
    
    public void updateQuizExam(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizExam", vo);
    }
    
    public void updateQuizExam_Submit(QuizVO vo) throws Exception {
    	update("quizDAO.updateQuizExam_Submit", vo);
    }
    
    //퀴즈 성적 요약
    public EgovMap quizExamSummary(QuizVO vo) throws Exception {
        return (EgovMap) select("quizDAO.quizExamSummary", vo);
    }
    
}
