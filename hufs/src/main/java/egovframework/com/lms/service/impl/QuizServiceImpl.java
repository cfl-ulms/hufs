package egovframework.com.lms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.QuizService;
import egovframework.com.lms.service.QuizVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;


@Service("quizService")
public class QuizServiceImpl extends EgovAbstractServiceImpl implements QuizService {

	@Resource(name="quizDAO")
    private QuizDAO quizDAO;

    @Resource(name="quizIdGnrService")    
    private EgovIdGnrService quizIdGnrService;
    
    @Resource(name="examIdGnrService")    
    private EgovIdGnrService examIdGnrService;
    
	public String insertQuiz(QuizVO vo) throws Exception {
    	/** ID Generation Service */
    	String id = quizIdGnrService.getNextStringId();
    	vo.setQuizId(id);
    	quizDAO.insertQuiz(vo); 
    	return vo.getQuizId();
	}
	
	public String insertQuizAnswer(QuizVO vo) throws Exception {
    	/** ID Generation Service */
    	String id = examIdGnrService.getNextStringId();
    	vo.setExamId(id);
    	return quizDAO.insertQuizAnswer(vo);
	}
	
	
    public int selectQuizListTotCnt(QuizVO searchVO) {
		return quizDAO.selectQuizListTotCnt(searchVO);
	}

    public int selectQuizExam_Cnt(QuizVO searchVO) {
		return quizDAO.selectQuizExam_Cnt(searchVO);
	}
    
    public List<QuizVO> selectQuizList(QuizVO searchVO) throws Exception {
        return quizDAO.selectQuizList(searchVO);
    }
    
    public List<QuizVO> selectBeforeQuizList(QuizVO searchVO) throws Exception {
        return quizDAO.selectBeforeQuizList(searchVO);
    }
    
    public List<QuizVO> selectPreQuizList(QuizVO searchVO) throws Exception {
        return quizDAO.selectPreQuizList(searchVO);
    }

    public QuizVO selectQuiz_S(QuizVO vo) throws Exception {
        return quizDAO.selectQuiz_S(vo);
    }
    
    public void updateQuiz(QuizVO vo) throws Exception {
    	quizDAO.updateQuiz(vo);
    }
    
    public void updateQuiz_D(QuizVO vo) throws Exception {
    	quizDAO.updateQuiz_D(vo);
    }
    
    public void updateQuizAnswer(QuizVO vo) throws Exception {
    	quizDAO.updateQuizAnswer(vo);
    }
    
    public void updateQuizAnswer_D(QuizVO vo) throws Exception {
    	quizDAO.updateQuizAnswer_D(vo);
    }
    
    public void updateQuiz_Sort(QuizVO vo) throws Exception {
    	quizDAO.updateQuiz_Sort(vo);
    }
    
    public void updateQuizAnswer_A(QuizVO vo) throws Exception {
    	quizDAO.updateQuizAnswer_A(vo);
    }
    
    public void updateQuizExamAnswer(QuizVO vo) throws Exception {
    	quizDAO.updateQuizExamAnswer(vo);
    }
    
    public void updateQuizEnd(QuizVO vo) throws Exception {
    	quizDAO.updateQuizEnd(vo);
    }
    
    public void deleteOffQuizAnswer(QuizVO vo) throws Exception {
    	quizDAO.deleteOffQuizAnswer(vo);
    }
    
    public void deleteOffQuiz(QuizVO vo) throws Exception {
    	quizDAO.deleteOffQuiz(vo);
    }
    
    public QuizVO selectQuizExam_S(QuizVO vo) throws Exception {
        return quizDAO.selectQuizExam_S(vo);
    }
    
    public QuizVO selectPreQuizList_S(QuizVO vo) throws Exception {
        return quizDAO.selectPreQuizList_S(vo);
    }
    
    public QuizVO selectQuizExam_Report(QuizVO vo) throws Exception {
        return quizDAO.selectQuizExam_Report(vo);
    }
    
    public List<QuizVO> selectQuizResult_Report(QuizVO vo) throws Exception {
    	return quizDAO.selectQuizResult_Report(vo);
    }
    
    public List<QuizVO> selectQuizExamAnswer_Cnt(QuizVO vo) throws Exception {
    	return quizDAO.selectQuizExamAnswer_Cnt(vo);
    }
    

    public String insertQuizExam(QuizVO vo) throws Exception {
    	return quizDAO.insertQuizExam(vo);
	}
    
    public void deleteQuizExam(QuizVO vo) throws Exception {
    	quizDAO.deleteQuizExam(vo);
    }
    
    public void updateQuizExam(QuizVO vo) throws Exception {
    	quizDAO.updateQuizExam(vo);
    }
    
    public void updateQuizExam_Submit(QuizVO vo) throws Exception {
    	quizDAO.updateQuizExam_Submit(vo);
    }
    
    //퀴즈 성적 요약
    public EgovMap quizExamSummary(QuizVO vo) throws Exception {
        return quizDAO.quizExamSummary(vo);
    }
}
