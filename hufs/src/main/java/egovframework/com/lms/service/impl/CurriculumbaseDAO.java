package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.CurriculumbaseVO;

/**
 * @Class Name : CurriculumbaseDAO.java
 * @Description : Curriculumbase DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("curriculumbaseDAO")
public class CurriculumbaseDAO extends EgovAbstractDAO {

	/**
	 * curriculumbase을 등록한다.
	 * @param vo - 등록할 정보가 담긴 CurriculumbaseVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertCurriculumbase(CurriculumbaseVO vo) throws Exception {
        return (String)insert("curriculumbaseDAO.insertCurriculumbase_S", vo);
    }

    /**
	 * curriculumbase을 수정한다.
	 * @param vo - 수정할 정보가 담긴 CurriculumbaseVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateCurriculumbase(CurriculumbaseVO vo) throws Exception {
        update("curriculumbaseDAO.updateCurriculumbase_S", vo);
    }

    /**
	 * curriculumbase을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 CurriculumbaseVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteCurriculumbase(CurriculumbaseVO vo) throws Exception {
    	update("curriculumbaseDAO.deleteCurriculumbase_S", vo);
    }

    /**
	 * curriculumbase을 조회한다.
	 * @param vo - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return 조회한 curriculumbase
	 * @exception Exception
	 */
    public CurriculumbaseVO selectCurriculumbase(CurriculumbaseVO vo) throws Exception {
        return (CurriculumbaseVO) select("curriculumbaseDAO.selectCurriculumbase_S", vo);
    }

    /**
	 * curriculumbase 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculumbase 목록
	 * @exception Exception
	 */
    public List<?> selectCurriculumbaseList(CurriculumbaseVO searchVO) throws Exception {
        return list("curriculumbaseDAO.selectCurriculumbaseList_D", searchVO);
    }
    
    //과정학습참고자료 조회
    public List selectCurriculumbaseFile(CurriculumbaseVO searchVO) throws Exception {
        return list("curriculumbaseDAO.selectCurriculumbaseFile", searchVO);
    }
    
    /**
	 * curriculumbase 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return curriculumbase 총 갯수
	 * @exception
	 */
    public int selectCurriculumbaseListTotCnt(CurriculumbaseVO searchVO) {
        return (Integer)select("curriculumbaseDAO.selectCurriculumbaseListTotCnt_S", searchVO);
    }
    
    //참고자료 등록
    public String insertCurriculumbasefile(CurriculumbaseVO vo) throws Exception {
        return (String)insert("curriculumbaseDAO.insertCurriculumbasefile_S", vo);
    }
    
    public void deleteCurriculumbasefile(CurriculumbaseVO vo) throws Exception {
    	delete("curriculumbaseDAO.deleteCurriculumbasefile_S", vo);
    }
}
