package egovframework.com.lms.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.SearchVO;
import egovframework.rte.psl.dataaccess.util.EgovMap;


/**
 * @Class Name : CurriculumbaseService.java
 * @Description : Curriculumbase Business class
 * @Modification Information
 *
 * @author 이정현
 * @since 2019.11.17
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
public interface ClassManageService {

	 public List selectClassCurriculumList(FileVO fileVo) throws Exception;

	 EgovMap selectFileListMap(FileVO fileVo,HttpServletRequest request) throws Exception;

	 public List selectSearchStudyFileList(FileVO fileVo,HttpServletRequest request) throws Exception;

	 public FileVO selectFileInfo(FileVO fvo) throws Exception;

	 EgovMap selectCurriculumStudyMap(SearchVO searchVo) throws Exception;

	 public List selectCurriculumList(SearchVO searchVo) throws Exception;

	 public List selectCurriculumBaseList(SearchVO searchVo) throws Exception;

	 EgovMap selectCurriculumBoardMap(SearchVO searchVo) throws Exception;

	 EgovMap selectSurveyMap(SearchVO searchVo) throws Exception;

	 public EgovMap selectCurriculumUserInfo(EgovMap map) throws Exception;

	 public EgovMap selectTeacherFileListMap(FileVO fileVo, HttpServletRequest request) throws Exception;

	 public EgovMap selectStudentFileListMap(FileVO fileVo, HttpServletRequest request) throws Exception;
}
