package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.lms.service.BasefileVO;

public interface AttendService {
	
    //출석 수정
    void updateAttention(AttendVO vo) throws Exception;
    
    //출석목록
    public List selectAttendList(AttendVO vo) throws Exception;
    
    //과정별 학생 출석 목록(성적)
    public List studentAttendList(AttendVO vo) throws Exception;
}
