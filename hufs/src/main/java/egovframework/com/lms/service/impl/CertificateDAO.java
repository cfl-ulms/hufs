package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.BasefileVO;
import egovframework.com.lms.service.CertificateVO;
import egovframework.com.lms.service.CurriculumVO;

//수료증
@Repository("certificateDAO")
public class CertificateDAO extends EgovAbstractDAO {

    //수료증등록
  	public void insertCertificate(CertificateVO vo) throws Exception {
  		insert("certificateDAO.insertCertificate", vo);
  	}

    //발급 수료증 폐기
  	public void deleteCertificate(CertificateVO vo) throws Exception {
  		update("certificateDAO.deleteCertificate", vo);
  	}
  	
  	//주료증 목록
  	public List selectCertificateList(CertificateVO vo) throws Exception {
  		return list("certificateDAO.selectCertificateList", vo);
  	}

  	//주료증 목록 수
  	public int selectCertificateListCnt(CertificateVO vo) throws Exception {
  		return (Integer) select("certificateDAO.selectCertificateListCnt", vo);
  	}

  	//수료증 조회
  	public EgovMap selectCertificate(CertificateVO vo) throws Exception {
  		return (EgovMap) select("certificateDAO.selectCertificate", vo);
  	}
  	
  	//수료증 조회를 위한 해당 교육과정의 기본과정 대분류 조회
  	public EgovMap selectCertificateForctgryCd(CertificateVO vo) throws Exception {
  		return (EgovMap) select("certificateDAO.selectCertificateForctgryCd", vo);
  	}
    
  	//수료증 정보
  	public List selectCertificateList_2(CertificateVO vo) throws Exception {
  		return list("certificateDAO.selectCertificateList_2", vo);
  	}
  	
  	//수료증 정보 체크
  	public EgovMap certificateCheck(CertificateVO vo) throws Exception {
  		return (EgovMap) select("certificateDAO.certificateCheck", vo);
  	}
  	
  	//수료증 발급 이력 등록
  	public void insertCertificateHst(CertificateVO vo) throws Exception {
  		insert("certificateDAO.insertCertificateHst", vo);
  	}
}
