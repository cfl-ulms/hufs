package egovframework.com.lms.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.com.cmm.service.SearchVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyQuestionExVO;
import egovframework.com.lms.service.SurveyQuestionVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

/**
 * @Class Name : CurriculumbaseService.java
 * @Description : Curriculumbase Business class
 * @Modification Information
 *
 * @author 이정현
 * @since 2019.11.17
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
@Service("surveyManageService")
public class SurveyManageServiceImpl extends EgovAbstractServiceImpl implements SurveyManageService {

	/** ID Generation */
    @Resource(name="surveyIdGnrService")
    private EgovIdGnrService surveyIdGnrService;

    @Resource(name="surveyQuestionIdGnrService")
    private EgovIdGnrService surveyQuestionIdGnrService;

    @Resource(name="surveyExampelIdGnrService")
    private EgovIdGnrService surveyExampelIdGnrService;

    @Resource(name="surveyDAO")
    private SurveyDAO surveyDAO;

	public EgovMap selectSurvey(SurveyVO surveyVo) throws Exception {
		EgovMap resultMap = new EgovMap();
		List<?> surveyList = this.surveyDAO.selectSurveyList(surveyVo);
		int surveyListCnt = this.surveyDAO.selectSurveyListCnt(surveyVo);

		resultMap.put("resultList", surveyList);
		resultMap.put("resultCnt", surveyListCnt);
		return resultMap;

	}

	public void insertSurvey(SurveyVO surveyVo) throws Exception {
		/** ID Generation Service */
    	String id = surveyIdGnrService.getNextStringId();
    	surveyVo.setSchdulId(id);
    	surveyVo.setSiteId("SITE_000000000000001");
    	surveyVo.setSchdulBgnde("0");
    	surveyVo.setSchdulEndde("0");
    	this.surveyDAO.insertSurvey(surveyVo);
    	for(SurveyQuestionVO questVo : surveyVo.getQuestionsArray()){
    		String questId = surveyQuestionIdGnrService.getNextStringId();
    		questVo.setQesitmId(questId);
    		questVo.setSchdulId(surveyVo.getSchdulId());
    		this.surveyDAO.insertSurveyQuestion(questVo);
    		if(!"answer".equals(questVo.getQesitmTyCode())){
    			for(SurveyQuestionExVO exVo : questVo.getExamples()){
    				String exId = surveyExampelIdGnrService.getNextStringId();
    				exVo.setExId(exId);
    				exVo.setQesitmId(questId);
    				this.surveyDAO.insertSurveyExample(exVo);
    			}
    		}
    	}

	}

	// 과정설문관리 삭제(상태변경)
	public void deleteSurvey(SurveyVO surveyVo) throws Exception {
		
		this.surveyDAO.deleteSurveyQuestion(surveyVo);
	}

	public SurveyVO selectSurveyInfo(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectSurveyInfo(surveyVo);
	}

	public EgovMap selectCurriculumSurvey(SurveyVO surveyVo) throws Exception {
		EgovMap resultMap = new EgovMap();
		List surveyList = this.selectCurriculumSurveyList(surveyVo);
		int surveyListCnt = this.surveyDAO.selectCurriculumSurveyListCnt(surveyVo);

		resultMap.put("resultList", surveyList);
		resultMap.put("resultCnt", surveyListCnt);
		return resultMap;
	}

	public List<EgovMap> selectCurriculumSurveyAnswer(SurveyVO surveyVo) throws Exception {
		List<EgovMap> questionList = this.surveyDAO.selectSurveyQuestionList(surveyVo);
		for(EgovMap tempMap : questionList){
				SurveyQuestionVO tempVo = new SurveyQuestionVO();
				tempVo.setCrclId(surveyVo.getCrclId());
				tempVo.setPlId(surveyVo.getPlId());
				tempVo.setQesitmId((String)tempMap.get("qesitmId"));
			if("multiple".equals(tempMap.get("qesitmTyCode").toString())){
				List<EgovMap> answerList = this.surveyDAO.selectCurriculumSurveyAnswer(tempVo);
				tempMap.put("answerList", answerList);
			}else if("answer".equals(tempMap.get("qesitmTyCode").toString())){
				List<EgovMap> EssayAnswerList = this.surveyDAO.selectCurriculumSurveyEssayAnswer(tempVo);
				tempMap.put("essayList", EssayAnswerList);
			}
		}
		return questionList;
	}

	public EgovMap selectCurriculumAddInfo(SurveyVO surveyVo) throws Exception {
		if(surveyVo.getPlId() == null){
			if(surveyVo.getSchdulClCode() == null) {
				return this.surveyDAO.selectCurriculumAddInfo(surveyVo);
			} else {
				if(surveyVo.getSchdulClCode().equals("TYPE_3")) { 
					return this.surveyDAO.selectCurriculumAddInfoType3(surveyVo);
				}else { 
					return this.surveyDAO.selectCurriculumAddInfo(surveyVo);
					}
			}
		}else{
			return this.surveyDAO.selectCurriculumAddSchedule(surveyVo);
		}
	}
	
	//과정만족도 - 교원
	public EgovMap selectCurriculumAddInfoType3(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectCurriculumAddInfoType3(surveyVo);
	}
	
	public List<EgovMap> selectSurveyMember(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectSurveyMember(surveyVo);
	}
	
	//설문 제출 현황 목록
    public EgovMap selectSurveyMemberSummary(SurveyVO surveyVO) throws Exception {
    	return surveyDAO.selectSurveyMemberSummary(surveyVO);
    }
	
    //설문 제출자 - 교원
    public List<EgovMap> selectSurveyProfessor(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectSurveyProfessor(surveyVo);
	}
    
    //설문 제출 현황 목록 - 교원
    public EgovMap selectSurveyProfessorSummary(SurveyVO surveyVO) throws Exception {
    	return surveyDAO.selectSurveyProfessorSummary(surveyVO);
    }
    
	public List<EgovMap> selectCurriculumSurveyList(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectCurriculumSurveyList(surveyVo);
	}

	public int selectAnswerSurveyCnt(SurveyVO surveyVo) throws Exception {
		return this.surveyDAO.selectAnswerSurveyCnt(surveyVo);
	}

	public void insertSurveyAnswer(EgovMap map) throws Exception {
		this.surveyDAO.insertSurveyAnswer(map);
	}

	public void insertSurveySubmit(EgovMap map) throws Exception {
		this.surveyDAO.insertSurveySubmit(map);
		this.surveyDAO.insertSurveyAnswer(map);
	}

	public EgovMap selectMySurvey(SearchVO searchVo) throws Exception {
		EgovMap resultMap = new EgovMap();
		List surveyList = this.surveyDAO.selectMySurveyList(searchVo);
		int surveyListCnt = this.surveyDAO.selectMySurveyListCnt(searchVo);

		resultMap.put("resultList", surveyList);
		resultMap.put("resultCnt", surveyListCnt);
		return resultMap;
	}

	public String selectSurveyId(SurveyVO surveyVO) throws Exception {
		return this.surveyDAO.selectSurveyId(surveyVO);
	}

	//과정만족도 - 교원용
	public EgovMap selectMyProfessorSurvey(SearchVO searchVo) throws Exception {
		EgovMap resultMap = new EgovMap();
		List surveyList = this.surveyDAO.selectMyProfessorSurveyList(searchVo);
		int surveyListCnt = this.surveyDAO.selectMyProfessorSurveyCnt(searchVo);

		resultMap.put("resultList", surveyList);
		resultMap.put("resultCnt", surveyListCnt);
		return resultMap;
	}

	// 과정만족도
	public EgovMap curriculumSurveyAnswerScore(SurveyVO surveyVO) throws Exception {
		EgovMap resultMap = new EgovMap();
		
		List<EgovMap> tempList = new ArrayList<EgovMap>();
		List<EgovMap> innerVeryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> innerGoodList = new ArrayList<EgovMap>();
		List<EgovMap> innerSoSoList = new ArrayList<EgovMap>();
		List<EgovMap> innerNotBadList = new ArrayList<EgovMap>();
		List<EgovMap> innerBadList = new ArrayList<EgovMap>();
		
		SurveyQuestionVO surveyQuestionVo = new SurveyQuestionVO();
		surveyQuestionVo.setCrclId(surveyVO.getCrclId());
		surveyQuestionVo.setSchdulId(surveyVO.getSchdulId());
		
		tempList = surveyDAO.curriculumSurveyAnswerScoreList(surveyQuestionVo);
		EgovMap innerMap;
		for(int i= 0 ; i < tempList.size() ; i++) {
			innerMap = new EgovMap();
			innerMap.put("qesitmId", tempList.get(i).get("qesitmId"));
			innerMap.put("cntA", tempList.get(i).get("cntA"));
			
			if("1".equals(tempList.get(i).get("qesitmTy"))) {
				if("매우 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerVeryGoodList.add(innerMap);
				}
				if("그렇다".equals(tempList.get(i).get("exCn"))) {
					innerGoodList.add(innerMap);
				}
				if("그저 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerSoSoList.add(innerMap);
				}
				if("그렇지 않다".equals(tempList.get(i).get("exCn"))) {
					innerNotBadList.add(innerMap);
				}
				if("매우 그렇지 않다".equals(tempList.get(i).get("exCn"))) {
					innerBadList.add(innerMap);
				}
			}
			
			if("2".equals(tempList.get(i).get("qesitmTy"))) {
				if("매우 만족".equals(tempList.get(i).get("exCn"))) {
					innerVeryGoodList.add(innerMap);
				}
				if("만족".equals(tempList.get(i).get("exCn"))) {
					innerGoodList.add(innerMap);
				}
				if("그저 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerSoSoList.add(innerMap);
				}
				if("불만족".equals(tempList.get(i).get("exCn"))) {
					innerNotBadList.add(innerMap);
				}
				if("매우 불만족".equals(tempList.get(i).get("exCn"))) {
					innerBadList.add(innerMap);
				}
			}
			
		}
		
		List<EgovMap> veryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> goodList = new ArrayList<EgovMap>();
		List<EgovMap> soSoList = new ArrayList<EgovMap>();
		List<EgovMap> notBadList = new ArrayList<EgovMap>();
		List<EgovMap> badList = new ArrayList<EgovMap>();
		
		List<EgovMap> tempVeryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> tempGoodList = new ArrayList<EgovMap>();
		List<EgovMap> tempSoSoList = new ArrayList<EgovMap>();
		List<EgovMap> tempNotBadList = new ArrayList<EgovMap>();
		List<EgovMap> tempBadList = new ArrayList<EgovMap>();
		
		List<String> finalVeryGoodList = new ArrayList<String>();
		List<String> finalGoodList = new ArrayList<String>();
		List<String> finalSoSoList = new ArrayList<String>();
		List<String> finalNotBadList = new ArrayList<String>();
		List<String> finalBadList = new ArrayList<String>();
		
		surveyVO.setQesitmTyCode("multiple");
		List<EgovMap> questionList = this.surveyDAO.selectSurveyQuestionList(surveyVO);
		
		EgovMap innerMap2;
		for(int a = 0 ; a < questionList.size() ; a++) {
			innerMap2 = new EgovMap();
			
			if(!"answer".equals(questionList.get(a).get("qesitmTyCode"))) {
				innerMap2.put("qesitmId", questionList.get(a).get("qesitmId"));
				innerMap2.put("cntA", "0");
				innerMap2.put("cntScore", "0");
				
				veryGoodList.add(innerMap2);
				goodList.add(innerMap2);
				soSoList.add(innerMap2);
				notBadList.add(innerMap2);
				badList.add(innerMap2);
			}
		}
		
		String paramQesitmId = "";
		EgovMap tempMap = new EgovMap();
		int cntSum = 0;
		Boolean innerFlag = false;
		
		if(!innerVeryGoodList.isEmpty()) {
			for(int b = 0 ; b < veryGoodList.size() ; b++) {
				paramQesitmId = veryGoodList.get(b).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerB = 0 ; innerB < innerVeryGoodList.size(); innerB++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerVeryGoodList.get(innerB).get("qesitmId").toString())) {

						cntSum = Integer.parseInt(innerVeryGoodList.get(innerB).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*5);
						tempVeryGoodList.add(tempMap);
						break;
					} 
					
					if((innerB+1) == innerVeryGoodList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempVeryGoodList.add(tempMap);
				}
			}
		} else {
			tempVeryGoodList = veryGoodList;
		}
		
		if(!innerGoodList.isEmpty()) {
			for(int c = 0 ; c < goodList.size() ; c++) {
				paramQesitmId = goodList.get(c).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerC = 0 ; innerC < innerGoodList.size(); innerC++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerGoodList.get(innerC).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerGoodList.get(innerC).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*4);
						tempGoodList.add(tempMap);
						break;
					}
					
					if((innerC+1) == innerGoodList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempGoodList.add(tempMap);
				}
			}
		} else {
			tempGoodList = goodList;
		}
		
		if(!innerSoSoList.isEmpty()) {
			for(int d = 0 ; d < soSoList.size() ; d++) {
				paramQesitmId = soSoList.get(d).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerD = 0 ; innerD < innerSoSoList.size(); innerD++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerSoSoList.get(innerD).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerSoSoList.get(innerD).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*3);
						tempSoSoList.add(tempMap);
						break;
					}
					
					if((innerD+1) == innerSoSoList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempSoSoList.add(tempMap);
				}
				
			}
		} else {
			tempSoSoList = soSoList;
		}
		
		if(!innerNotBadList.isEmpty()) {
			for(int e = 0 ; e < notBadList.size() ; e++) {
				paramQesitmId = notBadList.get(e).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerE = 0 ; innerE < innerNotBadList.size(); innerE++) {
					
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerNotBadList.get(innerE).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerNotBadList.get(innerE).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*2);
						tempNotBadList.add(tempMap);
						break;
					}
					
					if((innerE+1) == innerNotBadList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempNotBadList.add(tempMap);
				}
			}
		} else {
			tempNotBadList = notBadList;
		}
		
		if(!innerBadList.isEmpty()) {
			for(int g = 0 ; g < badList.size() ; g++) {
				paramQesitmId = badList.get(g).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerG = 0 ; innerG < innerBadList.size(); innerG++) {
					
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerBadList.get(innerG).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerBadList.get(innerG).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum);
						tempBadList.add(tempMap);
						break;
					}
					
					if((innerG+1) == innerBadList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempBadList.add(tempMap);
				}
			}
		} else {
			tempBadList = badList;
		}
		
		int surveyAvg = 0;
		int totalCnt = 0;
		int totalSum = 0;
		List<String> totalList = new ArrayList<String>(); 
		for(int h = 0 ; h < tempVeryGoodList.size(); h++) {
			
			totalSum = 0;
			surveyAvg = 0;
			totalCnt = 0;
			
			totalCnt = Integer.parseInt(tempVeryGoodList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempGoodList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempSoSoList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempNotBadList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempBadList.get(h).get("cntA").toString());
			
			if( "0".equals(tempVeryGoodList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum = Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString());
			}
			finalVeryGoodList.add(surveyAvg+"");
			
			if(  "0".equals(tempGoodList.get(h).get("cntScore").toString())) { 
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempGoodList.get(h).get("cntScore").toString());
/*				surveyAvg = Float.valueOf(Integer.parseInt(tempGoodList.get(h).get("cntScore").toString())/(float)totalCnt); */
				totalSum += Integer.parseInt(tempGoodList.get(h).get("cntScore").toString());
			}
			finalGoodList.add(surveyAvg+"");

			if(  "0".equals(tempSoSoList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else { 
				surveyAvg = Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString());
			}
			finalSoSoList.add(surveyAvg+"");
			
			if(  "0".equals(tempNotBadList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString());
			}
			finalNotBadList.add(surveyAvg+"");
			
			if(  "0".equals(tempBadList.get(h).get("cntScore").toString())) { 
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempBadList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempBadList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempBadList.get(h).get("cntScore").toString());
			}
			finalBadList.add(surveyAvg+"");
			
			if(totalSum == 0) {
				totalList.add("0");
			} else {
				totalList.add(String.format("%.2f",totalSum/(float)totalCnt));
			}
		}
		
		Float paramSurveyScore = 0.0f;
		String totalSurveyScore = "0";
		if(totalList != null && totalList.size() > 0 ) {
			
			for(int i= 0 ; i < totalList.size(); i++){
				paramSurveyScore += Float.valueOf(totalList.get(i));
			}
		}
		
		if(paramSurveyScore == 0) {
			totalSurveyScore = "0";
		} else {
			paramSurveyScore = paramSurveyScore/totalList.size();
			totalSurveyScore =  String.format("%.2f", paramSurveyScore);
		}
		
		resultMap.put("veryGoodList", finalVeryGoodList);
		resultMap.put("goodList", finalGoodList);
		resultMap.put("soSoList", finalSoSoList);
		resultMap.put("notBadList", finalNotBadList);
		resultMap.put("badList", finalBadList);
		resultMap.put("surveyAvgList", totalList);
		resultMap.put("totalSurveyScore", totalSurveyScore);
		
		return resultMap;
	}
	
	
	
	// 교원대상 과정만족도
	public EgovMap curriculumSurveyAnswerScore3(SurveyVO surveyVO) throws Exception {
		EgovMap resultMap = new EgovMap();
		
		List<EgovMap> tempList = new ArrayList<EgovMap>();
		List<EgovMap> innerVeryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> innerGoodList = new ArrayList<EgovMap>();
		List<EgovMap> innerSoSoList = new ArrayList<EgovMap>();
		List<EgovMap> innerNotBadList = new ArrayList<EgovMap>();
		List<EgovMap> innerBadList = new ArrayList<EgovMap>();
		
		SurveyQuestionVO surveyQuestionVo = new SurveyQuestionVO();
		surveyQuestionVo.setCrclId(surveyVO.getCrclId());
		surveyQuestionVo.setSchdulId(surveyVO.getSchdulId());
		
		tempList = surveyDAO.curriculumSurveyAnswerScoreList(surveyQuestionVo);
		
		EgovMap innerMap;
		for(int i= 0 ; i < tempList.size() ; i++) {
			innerMap = new EgovMap();
			innerMap.put("qesitmId", tempList.get(i).get("qesitmId"));
			innerMap.put("cntA", tempList.get(i).get("cntA"));
			innerMap.put("exCn", tempList.get(i).get("exCn"));
			
			if("1".equals(tempList.get(i).get("qesitmTy"))) {
				if("매우 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerVeryGoodList.add(innerMap);
				}
				if("그렇다".equals(tempList.get(i).get("exCn"))) {
					innerGoodList.add(innerMap);
				}
				if("그저 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerSoSoList.add(innerMap);
				}
				if("그렇지 않다".equals(tempList.get(i).get("exCn"))) {
					innerNotBadList.add(innerMap);
				}
				if("매우 그렇지 않다".equals(tempList.get(i).get("exCn"))) {
					innerBadList.add(innerMap);
				}
			}
			
			if("2".equals(tempList.get(i).get("qesitmTy"))) {
				if("매우 만족".equals(tempList.get(i).get("exCn"))) {
					innerVeryGoodList.add(innerMap);
				}
				if("만족".equals(tempList.get(i).get("exCn"))) {
					innerGoodList.add(innerMap);
				}
				if("그저 그렇다".equals(tempList.get(i).get("exCn"))) {
					innerSoSoList.add(innerMap);
				}
				if("불만족".equals(tempList.get(i).get("exCn"))) {
					innerNotBadList.add(innerMap);
				}
				if("매우 불만족".equals(tempList.get(i).get("exCn"))) {
					innerBadList.add(innerMap);
				}
			}
		}
		
		List<EgovMap> veryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> goodList = new ArrayList<EgovMap>();
		List<EgovMap> soSoList = new ArrayList<EgovMap>();
		List<EgovMap> notBadList = new ArrayList<EgovMap>();
		List<EgovMap> badList = new ArrayList<EgovMap>();
		
		List<EgovMap> tempVeryGoodList = new ArrayList<EgovMap>();
		List<EgovMap> tempGoodList = new ArrayList<EgovMap>();
		List<EgovMap> tempSoSoList = new ArrayList<EgovMap>();
		List<EgovMap> tempNotBadList = new ArrayList<EgovMap>();
		List<EgovMap> tempBadList = new ArrayList<EgovMap>();
		
		List<String> finalVeryGoodList = new ArrayList<String>();
		List<String> finalGoodList = new ArrayList<String>();
		List<String> finalSoSoList = new ArrayList<String>();
		List<String> finalNotBadList = new ArrayList<String>();
		List<String> finalBadList = new ArrayList<String>();
		
		surveyVO.setQesitmTyCode("multiple");
		List<EgovMap> questionList = this.surveyDAO.selectSurveyQuestionList(surveyVO);
		
		EgovMap innerMap2;
		for(int a = 0 ; a < questionList.size() ; a++) {
			innerMap2 = new EgovMap();
			
			if(!"answer".equals(questionList.get(a).get("qesitmTyCode"))) {
				innerMap2.put("qesitmId", questionList.get(a).get("qesitmId"));
				innerMap2.put("cntA", "0");
				innerMap2.put("cntScore", "0");
				
				veryGoodList.add(innerMap2);
				goodList.add(innerMap2);
				soSoList.add(innerMap2);
				notBadList.add(innerMap2);
				badList.add(innerMap2);
			}
		}
		
		String paramQesitmId = "";
		EgovMap tempMap = new EgovMap();
		int cntSum = 0;
		Boolean innerFlag = false;
		
		if(!innerVeryGoodList.isEmpty()) {
			for(int b = 0 ; b < veryGoodList.size() ; b++) {
				paramQesitmId = veryGoodList.get(b).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerB = 0 ; innerB < innerVeryGoodList.size(); innerB++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerVeryGoodList.get(innerB).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerVeryGoodList.get(innerB).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*5);
						tempVeryGoodList.add(tempMap);
						break;
					} 
					
					if((innerB+1) == innerVeryGoodList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempVeryGoodList.add(tempMap);
				}
			}
		} else {
			tempVeryGoodList = veryGoodList;
		}
		
		if(!innerGoodList.isEmpty()) {
			for(int c = 0 ; c < goodList.size() ; c++) {
				paramQesitmId = goodList.get(c).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerC = 0 ; innerC < innerGoodList.size(); innerC++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerGoodList.get(innerC).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerGoodList.get(innerC).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*4);
						tempGoodList.add(tempMap);
						break;
					}
					
					if((innerC+1) == innerGoodList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempGoodList.add(tempMap);
				}
			}
		} else {
			tempGoodList = goodList;
		}
		
		if(!innerSoSoList.isEmpty()) {
			for(int d = 0 ; d < soSoList.size() ; d++) {
				paramQesitmId = soSoList.get(d).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerD = 0 ; innerD < innerSoSoList.size(); innerD++) {
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerSoSoList.get(innerD).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerSoSoList.get(innerD).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*3);
						tempSoSoList.add(tempMap);
						break;
					}
					
					if((innerD+1) == innerSoSoList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempSoSoList.add(tempMap);
				}
				
			}
		} else {
			tempSoSoList = soSoList;
		}
		
		if(!innerNotBadList.isEmpty()) {
			for(int e = 0 ; e < notBadList.size() ; e++) {
				paramQesitmId = notBadList.get(e).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerE = 0 ; innerE < innerNotBadList.size(); innerE++) {
					
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerNotBadList.get(innerE).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerNotBadList.get(innerE).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum*2);
						tempNotBadList.add(tempMap);
						break;
					}
					
					if((innerE+1) == innerNotBadList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempNotBadList.add(tempMap);
				}
			}
		} else {
			tempNotBadList = notBadList;
		}
		
		if(!innerBadList.isEmpty()) {
			for(int g = 0 ; g < badList.size() ; g++) {
				paramQesitmId = badList.get(g).get("qesitmId").toString();
				innerFlag = false;
				
				for(int innerG = 0 ; innerG < innerBadList.size(); innerG++) {
					
					tempMap = new EgovMap();
					if(paramQesitmId.equals(innerBadList.get(innerG).get("qesitmId").toString())) {
						
						cntSum = Integer.parseInt(innerBadList.get(innerG).get("cntA").toString());
						
						tempMap.put("qesitmId", paramQesitmId);
						tempMap.put("cntA", cntSum);
						tempMap.put("cntScore", cntSum);
						tempBadList.add(tempMap);
						break;
					}
					
					if((innerG+1) == innerBadList.size()) {
						innerFlag = true;
					}
				}
				
				if(innerFlag == true) {
					tempMap = new EgovMap();
					tempMap.put("qesitmId", paramQesitmId);
					tempMap.put("cntA", "0");
					tempMap.put("cntScore", "0");
					tempBadList.add(tempMap);
				}
			}
		} else {
			tempBadList = badList;
		}
		
		int surveyAvg = 0;
		int totalCnt = 0;
		int totalSum = 0;
		List<String> totalList = new ArrayList<String>(); 
		for(int h = 0 ; h < tempVeryGoodList.size(); h++) {
			
			totalSum = 0;
			surveyAvg = 0;
			totalCnt = 0;
			
			totalCnt = Integer.parseInt(tempVeryGoodList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempGoodList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempSoSoList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempNotBadList.get(h).get("cntA").toString());
			totalCnt += Integer.parseInt(tempBadList.get(h).get("cntA").toString());
			
			if( "0".equals(tempVeryGoodList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum = Integer.parseInt(tempVeryGoodList.get(h).get("cntScore").toString());
			}
			finalVeryGoodList.add(surveyAvg+"");
			
			if(  "0".equals(tempGoodList.get(h).get("cntScore").toString())) { 
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempGoodList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempGoodList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempGoodList.get(h).get("cntScore").toString());
			}
			finalGoodList.add(surveyAvg+"");
			
			if(  "0".equals(tempSoSoList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else { 
				surveyAvg = Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempSoSoList.get(h).get("cntScore").toString());
			}
			finalSoSoList.add(surveyAvg+"");
			
			if(  "0".equals(tempNotBadList.get(h).get("cntScore").toString())) {
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempNotBadList.get(h).get("cntScore").toString());
			}
			finalNotBadList.add(surveyAvg+"");
			
			if(  "0".equals(tempBadList.get(h).get("cntScore").toString())) { 
				surveyAvg = 0;
			} else {
				surveyAvg = Integer.parseInt(tempBadList.get(h).get("cntScore").toString());
				/*surveyAvg = Float.valueOf(Integer.parseInt(tempBadList.get(h).get("cntScore").toString())/(float)totalCnt);*/
				totalSum += Integer.parseInt(tempBadList.get(h).get("cntScore").toString());
			}
			finalBadList.add(surveyAvg+"");
			
			if(totalSum == 0) {
				totalList.add("0");
			} else {
				totalList.add(String.format("%.2f",totalSum/(float)totalCnt));
			}
		}
		
		Float paramSurveyScore = 0.0f;
		String totalSurveyScore = "0";
		if(totalList != null && totalList.size() > 0 ) {
			
			for(int i= 0 ; i < totalList.size(); i++){
				paramSurveyScore += Float.valueOf(totalList.get(i));
			}
		}
		
		if(paramSurveyScore == 0) {
			totalSurveyScore = "0";
		} else {
			paramSurveyScore = paramSurveyScore/totalList.size() ;
			totalSurveyScore =  String.format("%.2f", paramSurveyScore);
		}
		
		resultMap.put("veryGoodList3", finalVeryGoodList);
		resultMap.put("goodList3", finalGoodList);
		resultMap.put("soSoList3", finalSoSoList);
		resultMap.put("notBadList3", finalNotBadList);
		resultMap.put("badList3", finalBadList);
		resultMap.put("surveyAvgList3", totalList);
		resultMap.put("totalSurveyScore3", totalSurveyScore);
		
		return resultMap;
	}
}
