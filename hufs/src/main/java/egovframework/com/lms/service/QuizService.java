package egovframework.com.lms.service;

import java.util.List;

import egovframework.rte.psl.dataaccess.util.EgovMap;

public interface QuizService {
	String insertQuiz(QuizVO vo) throws Exception;
	String insertQuizAnswer(QuizVO vo) throws Exception;
	
    int selectQuizListTotCnt(QuizVO searchVO);
    int selectQuizExam_Cnt(QuizVO searchVO);
    
    QuizVO selectQuiz_S(QuizVO vo) throws Exception;
    
    List<QuizVO> selectQuizList(QuizVO searchVO) throws Exception;
    List<QuizVO> selectBeforeQuizList(QuizVO searchVO) throws Exception;
    List<QuizVO> selectPreQuizList(QuizVO searchVO) throws Exception;
    
    List<QuizVO> selectQuizResult_Report(QuizVO searchVO) throws Exception;
    List<QuizVO> selectQuizExamAnswer_Cnt(QuizVO searchVO) throws Exception;
    
    void updateQuiz(QuizVO vo) throws Exception;
    void updateQuiz_D(QuizVO vo) throws Exception;
    void updateQuizAnswer(QuizVO vo) throws Exception;
    void updateQuizAnswer_D(QuizVO vo) throws Exception;
    void updateQuiz_Sort(QuizVO vo) throws Exception;
    void updateQuizAnswer_A(QuizVO vo) throws Exception;
    void updateQuizEnd(QuizVO vo) throws Exception;
    void updateQuizExamAnswer(QuizVO vo) throws Exception;
    
    void deleteOffQuizAnswer(QuizVO vo) throws Exception;
    void deleteOffQuiz(QuizVO vo) throws Exception;
    
    String insertQuizExam(QuizVO vo) throws Exception;
    QuizVO selectQuizExam_S(QuizVO vo) throws Exception;
    QuizVO selectPreQuizList_S(QuizVO searchVO) throws Exception;
    QuizVO selectQuizExam_Report(QuizVO searchVO) throws Exception;
    
    void updateQuizExam(QuizVO vo) throws Exception;
    void updateQuizExam_Submit(QuizVO vo) throws Exception;
    void deleteQuizExam(QuizVO vo) throws Exception;
    
    //퀴즈 성적 요약
    EgovMap quizExamSummary(QuizVO vo) throws Exception;
}
