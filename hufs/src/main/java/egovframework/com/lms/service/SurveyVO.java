package egovframework.com.lms.service;

import java.util.List;

import egovframework.com.cmm.ComDefaultVO;

/**
 * @Class Name : CurriculumVO.java
 * @Description : Curriculum VO class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */
/**
 * @author sm-788
 *
 */
/**
 * @author bkbla
 *
 */
public class SurveyVO extends ComDefaultVO{
    private static final long serialVersionUID = 1L;

    private String schdulId;
    private String siteId;
    private String schdulClCode;
    private String schdulBgnde;
    private String schdulEndde;
    private String presnatnDe;
    private String schdulNm;
    private String schdulStreFileNm;
    private String schdulCn;
    private String schdulChargerId;
    private String atchFileId;
    private String upendStreFileNm;
    private String middleStreFileNm;
    private String lptStreFileNm;
    private String reptitAt;
    private String inqireCo;
    private String frstRegisterPnttm;
    private String frstRegisterId;
    private String lastUpdusrPnttm;
    private String lastUpdusrId;
    private String useAt;
    private String przwnerNmpr;
    private String popupAt;
    private String questionCnt;
    private String crclId;
    private String plId;
    private String userId;
    
    private String qesitmTyCode;
    private int currUseCnt;

    //설문 제출여부 검색
    private String searchSubmitAt;
    
    private List<SurveyQuestionVO> questionsArray;

	public String getSchdulId() {
		return schdulId;
	}
	public void setSchdulId(String schdulId) {
		this.schdulId = schdulId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getSchdulClCode() {
		return schdulClCode;
	}
	public void setSchdulClCode(String schdulClCode) {
		this.schdulClCode = schdulClCode;
	}
	public String getSchdulBgnde() {
		return schdulBgnde;
	}
	public void setSchdulBgnde(String schdulBgnde) {
		this.schdulBgnde = schdulBgnde;
	}
	public String getSchdulEndde() {
		return schdulEndde;
	}
	public void setSchdulEndde(String schdulEndde) {
		this.schdulEndde = schdulEndde;
	}
	public String getPresnatnDe() {
		return presnatnDe;
	}
	public void setPresnatnDe(String presnatnDe) {
		this.presnatnDe = presnatnDe;
	}
	public String getSchdulNm() {
		return schdulNm;
	}
	public void setSchdulNm(String schdulNm) {
		this.schdulNm = schdulNm;
	}
	public String getSchdulStreFileNm() {
		return schdulStreFileNm;
	}
	public void setSchdulStreFileNm(String schdulStreFileNm) {
		this.schdulStreFileNm = schdulStreFileNm;
	}
	public String getSchdulCn() {
		return schdulCn;
	}
	public void setSchdulCn(String schdulCn) {
		this.schdulCn = schdulCn;
	}
	public String getSchdulChargerId() {
		return schdulChargerId;
	}
	public void setSchdulChargerId(String schdulChargerId) {
		this.schdulChargerId = schdulChargerId;
	}
	public String getAtchFileId() {
		return atchFileId;
	}
	public void setAtchFileId(String atchFileId) {
		this.atchFileId = atchFileId;
	}
	public String getUpendStreFileNm() {
		return upendStreFileNm;
	}
	public void setUpendStreFileNm(String upendStreFileNm) {
		this.upendStreFileNm = upendStreFileNm;
	}
	public String getMiddleStreFileNm() {
		return middleStreFileNm;
	}
	public void setMiddleStreFileNm(String middleStreFileNm) {
		this.middleStreFileNm = middleStreFileNm;
	}
	public String getLptStreFileNm() {
		return lptStreFileNm;
	}
	public void setLptStreFileNm(String lptStreFileNm) {
		this.lptStreFileNm = lptStreFileNm;
	}
	public String getReptitAt() {
		return reptitAt;
	}
	public void setReptitAt(String reptitAt) {
		this.reptitAt = reptitAt;
	}
	public String getInqireCo() {
		return inqireCo;
	}
	public void setInqireCo(String inqireCo) {
		this.inqireCo = inqireCo;
	}
	public String getFrstRegisterPnttm() {
		return frstRegisterPnttm;
	}
	public void setFrstRegisterPnttm(String frstRegisterPnttm) {
		this.frstRegisterPnttm = frstRegisterPnttm;
	}
	public String getFrstRegisterId() {
		return frstRegisterId;
	}
	public void setFrstRegisterId(String frstRegisterId) {
		this.frstRegisterId = frstRegisterId;
	}
	public String getLastUpdusrPnttm() {
		return lastUpdusrPnttm;
	}
	public void setLastUpdusrPnttm(String lastUpdusrPnttm) {
		this.lastUpdusrPnttm = lastUpdusrPnttm;
	}
	public String getLastUpdusrId() {
		return lastUpdusrId;
	}
	public void setLastUpdusrId(String lastUpdusrId) {
		this.lastUpdusrId = lastUpdusrId;
	}
	public String getUseAt() {
		return useAt;
	}
	public void setUseAt(String useAt) {
		this.useAt = useAt;
	}
	public String getPrzwnerNmpr() {
		return przwnerNmpr;
	}
	public void setPrzwnerNmpr(String przwnerNmpr) {
		this.przwnerNmpr = przwnerNmpr;
	}
	public String getPopupAt() {
		return popupAt;
	}
	public void setPopupAt(String popupAt) {
		this.popupAt = popupAt;
	}
	public String getQuestionCnt() {
		return questionCnt;
	}
	public void setQuestionCnt(String questionCnt) {
		this.questionCnt = questionCnt;
	}
	public List<SurveyQuestionVO> getQuestionsArray() {
		return questionsArray;
	}
	public void setQuestionsArray(List<SurveyQuestionVO> questionsArray) {
		this.questionsArray = questionsArray;
	}
	public String getCrclId() {
		return crclId;
	}
	public void setCrclId(String crclId) {
		this.crclId = crclId;
	}
	public String getPlId() {
		return plId;
	}
	public void setPlId(String plId) {
		this.plId = plId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSearchSubmitAt() {
		return searchSubmitAt;
	}
	public void setSearchSubmitAt(String searchSubmitAt) {
		this.searchSubmitAt = searchSubmitAt;
	}
	public String getQesitmTyCode() {
		return qesitmTyCode;
	}
	public void setQesitmTyCode(String qesitmTyCode) {
		this.qesitmTyCode = qesitmTyCode;
	}
	public int getCurrUseCnt() {
		return currUseCnt;
	}
	public void setCurrUseCnt(int currUseCnt) {
		this.currUseCnt = currUseCnt;
	}
	
}
