package egovframework.com.lms.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;
import egovframework.com.lms.service.BasefileVO;

/**
 * @Class Name : BasefileDAO.java
 * @Description : Basefile DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019-10-11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Repository("basefileDAO")
public class BasefileDAO extends EgovAbstractDAO {

	/**
	 * basefile을 등록한다.
	 * @param vo - 등록할 정보가 담긴 BasefileVO
	 * @return 등록 결과
	 * @exception Exception
	 */
    public String insertBasefile(BasefileVO vo) throws Exception {
        return (String)insert("basefileDAO.insertBasefile_S", vo);
    }

    /**
	 * basefile을 수정한다.
	 * @param vo - 수정할 정보가 담긴 BasefileVO
	 * @return void형
	 * @exception Exception
	 */
    public void updateBasefile(BasefileVO vo) throws Exception {
        update("basefileDAO.updateBasefile_S", vo);
    }

    /**
	 * basefile을 삭제한다.
	 * @param vo - 삭제할 정보가 담긴 BasefileVO
	 * @return void형 
	 * @exception Exception
	 */
    public void deleteBasefile(BasefileVO vo) throws Exception {
        delete("basefileDAO.deleteBasefile_S", vo);
    }

    /**
	 * basefile을 조회한다.
	 * @param vo - 조회할 정보가 담긴 BasefileVO
	 * @return 조회한 basefile
	 * @exception Exception
	 */
    public BasefileVO selectBasefile(BasefileVO vo) throws Exception {
        return (BasefileVO) select("basefileDAO.selectBasefile_S", vo);
    }

    /**
	 * basefile 목록을 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return basefile 목록
	 * @exception Exception
	 */
    public List<?> selectBasefileList(BasefileVO searchVO) throws Exception {
        return list("basefileDAO.selectBasefileList_D", searchVO);
    }

    /**
	 * basefile 총 갯수를 조회한다.
	 * @param searchMap - 조회할 정보가 담긴 Map
	 * @return basefile 총 갯수
	 * @exception
	 */
    public int selectBasefileListTotCnt(BasefileVO searchVO) {
        return (Integer)select("basefileDAO.selectBasefileListTotCnt_S", searchVO);
    }

}
