package egovframework.com.lms.service.impl;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * @Class Name : CurriculumDAO.java
 * @Description : Curriculum DAO Class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Repository("classDAO")
public class ClassDAO extends EgovAbstractDAO {


}
