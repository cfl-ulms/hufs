package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import egovframework.com.lms.service.AttendService;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
public class AttendController {

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
    @Resource(name = "attendService")
    private AttendService attendService;
    
    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "propertiesService")
	protected EgovPropertyService         propertyService;
    
    @Resource(name = "SiteManageService")
	EgovSiteManageService 				  siteManageService;
    
    //출석 등록
    @RequestMapping(value="/lms/atd/attendUpt.do")
    public String attendUpt(@ModelAttribute("searchVO") AttendVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	searchVO.setFrstRegisterId(user.getId());
    	
    	if(searchVO.getUserIdList().size() != 0 && searchVO.getAttentionTypeList().size() != 0 && searchVO.getUserIdList().size() == searchVO.getAttentionTypeList().size()){
    		attendService.updateAttention(searchVO);
    	}
    	
        return "forward:/lms/atd/selectAttendList.do";
    }
    
    //출석 목록
    @RequestMapping(value="/lms/atd/selectAttendList.do")
    public String selectAttendList(@ModelAttribute("searchVO") AttendVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String ret = "/lms/atd/AttendList";
    	
    	//시간표 및 수업계획 조회여부
    	if(!"Y".equals(searchVO.getModalAt())){
    		//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
        	model.addAttribute("curriculumVO", curriculumVO);
        	
        	//등록 된 시간표 담당교원
        	ScheduleMngVO scheduleMngVO = new ScheduleMngVO();
        	scheduleMngVO.setCrclId(searchVO.getCrclId());
        	scheduleMngVO.setPlId(searchVO.getPlId());
        	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
        	model.addAttribute("facPlList", facPlList);
        	
        	//수업 담당자 인지 확인
          	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
          	String studyMngAt = "N";
          	if(user != null){
          		for(int i = 0; i < facPlList.size(); i ++){
              		EgovMap map = (EgovMap) facPlList.get(i);
              		String userId = map.get("facId").toString();
              		if(user.getId().equals(userId)){
              			studyMngAt = "Y";
              		}
              	}
          	}
          	model.addAttribute("studyMngAt", studyMngAt);
          	
          	//과정 담당자 인지 확인
          	String managerAt = "N";
          	if(user != null){
          		curriculumVO.setManageCode(null);
          		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
              	for(int i = 0; i < subUserList.size(); i ++){
              		EgovMap map = (EgovMap) subUserList.get(i);
              		String userId = map.get("userId").toString();
              		if(user.getId().equals(userId)){
              			managerAt = "Y";
              		}
              	}
          	}
          	model.addAttribute("managerAt", managerAt);
    	}else{
    		ret = "/lms/atd/AttendListAjax";
    	}
    	
      	//출석목록
      	List resultList = attendService.selectAttendList(searchVO);
      	model.addAttribute("resultList", resultList);
      	
        return ret;
    }
}
