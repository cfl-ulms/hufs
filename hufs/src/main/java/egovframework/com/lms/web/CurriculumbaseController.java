package egovframework.com.lms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;

/**
 * @Class Name : CurriculumbaseController.java
 * @Description : Curriculumbase Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.11
 * @version 1.0
 * @see
 *  
 *  Copyright (C)  All right reserved.
 */

@Controller
// @SessionAttributes(types=CurriculumbaseVO.class)
public class CurriculumbaseController {

    @Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;
    
    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;
    
    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    /**
	 * curriculumbase 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return "/mng/lms/crclb/CurriculumbaseList"
	 * @exception Exception
	 */
    @RequestMapping(value="/lms/crclb/CurriculumbaseList.do")
    public String selectCurriculumbaseList(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, ModelMap model)throws Exception {
    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
        List<?> curriculumbaseList = curriculumbaseService.selectCurriculumbaseList(searchVO);
        model.addAttribute("resultList", curriculumbaseList);
        
        int totCnt = curriculumbaseService.selectCurriculumbaseListTotCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        //과정체계
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000004");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//이수구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000003");
  		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//관리구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000005");
  		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//대상
  		ctgry.setCtgrymasterId("CTGMST_0000000000015");
  		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        
        return "/lms/crclb/CurriculumbaseList";
    }
    
    /**
	 * curriculumbase 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 CurriculumbaseVO
	 * @return "/mng/lms/crclb/CurriculumbaseList"
	 * @exception Exception
	 */
    @RequestMapping(value="/lms/crclb/CurriculumbaseView.do")
    public String CurriculumbaseView(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, ModelMap model)throws Exception {
    	//과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
 		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList3", resultMap3.get("resultList"));
 		
    	CurriculumbaseVO curriculumbaseVO = curriculumbaseService.selectCurriculumbase(searchVO);
        model.addAttribute("resultVO", curriculumbaseVO);
        
        return "/lms/crclb/CurriculumbaseView";
    }
    
    //기본과정 Ajax
    @RequestMapping("/lms/crclb/selectCurriculumbaseAjax.do")
    public String selectCurriculumbase(@ModelAttribute("searchVO") CurriculumbaseVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	//과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList3", resultMap3.get("resultList"));
		
    	model.addAttribute("resultVO", curriculumbaseService.selectCurriculumbase(searchVO));
    	//과정학습참고자료
		model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(searchVO));
    			
        return "/lms/crclb/CurriculumbaseDetailAjax";
    }
    
}
