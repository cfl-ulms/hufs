package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.mysql.fabric.xmlrpc.base.Array;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.SearchVO;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;



@Controller
public class LmsTemplateController {

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "curriculumService")
	private CurriculumService curriculumService;

    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;


    //과정관리 - 탭메뉴
    @RequestMapping(value="/lms/tabmenu.do")
    public String lmsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String ip = request.getRemoteAddr();
    	model.addAttribute("ip", ip);

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	  	String pageFlag = "";
	  	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	  		pageFlag = "student";
	  		String tempCrclId = searchVO.getCrclId().toString().split(",")[0];
	  		SurveyVO surveyVo = new SurveyVO();
	  		surveyVo.setCrclId(tempCrclId);
	  		surveyVo.setPlId(searchVO.getPlId());
	  		List<EgovMap> resultSurveyList = this.surveyManageService.selectCurriculumSurveyList(surveyVo);
	  		model.addAttribute("resultSurveyList", resultSurveyList);

  		} else { //교원
  			pageFlag = "teacher";
  		}

	  	if(!EgovStringUtil.isEmpty(searchVO.getPlId())){
	  		ScheduleMngVO ScheduleVO = new ScheduleMngVO();
	  		ScheduleVO.setPlId(searchVO.getPlId());
	  		ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(ScheduleVO);
		   	model.addAttribute("scheduleMngVO", scheduleMngVO);

		   	//과정에 등록된 평가
		   	String evtId = "";
		   	if("CTG_0000000000000054".equals(scheduleMngVO.getCourseId())){
		   		//하나만 저장 됨
		   		EgovMap map = (EgovMap) scheduleMngService.selectStudyEvtList(scheduleMngVO).get(0);
		   		evtId = map.get("evtId").toString();
		   	}
		   	model.addAttribute("evtId", evtId);

	  	}

	  	model.addAttribute("pageFlag", pageFlag);
	  	model.addAttribute("USER_INFO", user);
        return "/lms/template/tabmenu";
    }

	//수강신청 - 탭메뉴
    @RequestMapping(value="/lms/curriculumtabmenu.do")
    public String curriculumtabmenu(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        return "/lms/template/curriculumtabmenu";
    }

    //과정관리 - 헤더
    @RequestMapping(value="/lms/crclHeader.do")
    public String crclHeader(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		curriculumVO = curriculumService.selectCurriculum(curriculumVO);

    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	Date startDate = format.parse(curriculumVO.getStartDate());
    	Date endDate = format.parse(curriculumVO.getEndDate());

    	LocalDateTime current = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    	String formatted = current.format(formatter);
    	Date todayDate = format.parse(formatted);

    	long today = todayDate.getTime() / (24*60*60*1000);
    	long startDay = startDate.getTime() / (24*60*60*1000);

    	if(startDay > today){
    		model.addAttribute("processPerc", 0);
    	}else{
    		long calDate = endDate.getTime() - startDate.getTime();
        	long calDateDays = calDate / (24*60*60*1000);
        	calDateDays = Math.abs(calDateDays);

        	long calDate2 = todayDate.getTime() - startDate.getTime();
        	long calDateDays2 = calDate2 / (24*60*60*1000);
        	calDateDays2 = Math.abs(calDateDays2);

        	double tmpCal = calDateDays2 / (double)calDateDays;

        	int processPerc = (int) (tmpCal*100);

        	if (100 < processPerc) {
        		processPerc = 100;
        	}

        	model.addAttribute("processPerc", processPerc);
    	}

		model.addAttribute("curriculumVO", curriculumVO);

		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/template/crclHeader";
    }

    //수업관리 - 헤더
    @RequestMapping(value="/lms/claHeader.do")
    public String claHeader(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	model.addAttribute("USER_INFO", user);

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		curriculumVO = curriculumService.selectCurriculum(curriculumVO);
		model.addAttribute("curriculumVO", curriculumVO);

		//수업정보
		ScheduleMngVO tempScheduleVo = new ScheduleMngVO();
		tempScheduleVo.setPlId(searchVO.getPlId());
	   	model.addAttribute("scheduleMngVO", scheduleMngService.selectClassScheduleView(tempScheduleVo));

	   	//출석정보
	   	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	   		tempScheduleVo.setStudentId(user.getId());
	   		model.addAttribute("scheduleAttention", scheduleMngService.selectScheduleAttention(tempScheduleVo));
	   	}

    	//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/template/claHeader";
    }

}
