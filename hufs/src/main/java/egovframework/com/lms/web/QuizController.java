package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.QuizService;
import egovframework.com.lms.service.QuizVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

/**
 * 
 * @author ywkim 
 * QuizController.java 
 * quiz 교원페이지 CRUD 
 */

@Controller
public class QuizController {
	
	@Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

	@Resource(name = "quizService")
	private QuizService quizService;
	
	@Resource(name = "EgovFileMngUtil")
 	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;
     
    @Resource(name = "EgovBBSAttributeManageService")
    private EgovBBSAttributeManageService bbsAttrbService;
    
    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
	
    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
    
    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;
    
    //퀴즈 화면
	@RequestMapping(value="/lms/quiz/QuizList.do")
    public String selectQuizList(@ModelAttribute("searchVO") QuizVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setSortDiv("ASC");
		
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
        model.addAttribute("resultList", quizList);
        
        int totCnt = quizService.selectQuizListTotCnt(searchVO);
        model.addAttribute("totCnt", totCnt);
        
        QuizVO quizVO = new QuizVO();
        quizVO.setUserId(user.getId());
        quizVO.setUserSe(user.getUserSe());
        quizVO.setSearchYY(searchVO.getSearchYY());
        quizVO.setSearchQuizNm(searchVO.getSearchQuizNm());
        
        List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(quizVO);
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy");
        Date curyyyy = new Date();
        String yyyy = format1.format(curyyyy);
        model.addAttribute("yyyy", yyyy);
        model.addAttribute("search2", searchVO);
        if(totCnt > 0) {
        	return "forward:/lms/quiz/QuizEvalList.do";
        }else {
        	return "/lms/manage/quizList";
        }
    }
	
	
	//퀴즈 등록 페이지 이동
	@RequestMapping("/lms/quiz/QuizRegView.do")
    public String QuizRegView(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, Model model
    		, HttpServletRequest request
    		, HttpServletResponse response
    	)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		
		//퀴즈 등록페이지 하단에 퀴즈 리스트를 선택 할 경우 해당 자료 조회
		if(!EgovStringUtil.isEmpty(searchVO.getQuizId())) {
			QuizVO quizVO = quizService.selectQuiz_S(searchVO);
			model.addAttribute("quizVO", quizVO);
		}
	
		//해당 과정 QUIZ 정보 가져오기
		searchVO.setSortDiv("DESC");
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
		model.addAttribute("resultList", quizList);
		
		int totCnt = quizList.size();
		model.addAttribute("totCnt", totCnt);
		
		return "/lms/manage/quizReg";
	}
	
	//퀴즈 등록 및 수정
	@RequestMapping("/lms/quiz/QuizReg.do")
    public String QuizReg(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response, final MultipartHttpServletRequest multiRequest)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		if("N".equals(searchVO.getQuizEnd()) || EgovStringUtil.isEmpty(searchVO.getQuizEnd()) || "".equals(searchVO.getQuizEnd())) {
			//답지 순서 지정
			String[] arrayAnswer = request.getParameterValues("answer");
			if(arrayAnswer.length>0) {
				if(!EgovStringUtil.isEmpty(arrayAnswer[0])) {
					searchVO.setAnswer1(arrayAnswer[0]);
				}
				if(!EgovStringUtil.isEmpty(arrayAnswer[1])) {
					searchVO.setAnswer2(arrayAnswer[1]);
				}
				if(!EgovStringUtil.isEmpty(arrayAnswer[2])) {
					searchVO.setAnswer3(arrayAnswer[2]);
				}
				if(!EgovStringUtil.isEmpty(arrayAnswer[3])) {
					searchVO.setAnswer4(arrayAnswer[3]);
				}
				if(!EgovStringUtil.isEmpty(arrayAnswer[4])) {
					searchVO.setAnswer5(arrayAnswer[4]);
				}
			}
			
			//답안 순서 지정
			if(searchVO.getAnswerNum().length()>1) {
				String[] sort = searchVO.getAnswerNum().split(",");
				Arrays.sort(sort);
				String answerNum ="";
				for(int i=0; i<sort.length; i++) {
					if(i==0) {
						searchVO.setAnswerNum(sort[i]);
					}else {
						searchVO.setAnswerNum(searchVO.getAnswerNum()+","+sort[i]);
					}
				}
			}
			
			//Quiz, Answer 저장 및 수정
			if(EgovStringUtil.isEmpty(searchVO.getQuizId())){
				searchVO.setFrstRegisterId(user.getId());
				quizService.insertQuiz(searchVO);
				quizService.insertQuizAnswer(searchVO);
			}else {
				searchVO.setLastUpdusrId(user.getId());
				quizService.updateQuiz(searchVO);
				quizService.updateQuizAnswer(searchVO);
				quizService.updateQuizExamAnswer(searchVO);
			}
		}else {
			quizService.updateQuizAnswer(searchVO);
			quizService.updateQuizExamAnswer(searchVO);
		}
		
		
		return "forward:/lms/quiz/QuizRegView.do";
	}
	
	
	//퀴즈 등록 및 수정
	@RequestMapping("/lms/quiz/QuizCopy.do")
    public String QuizCopy(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		String plId = searchVO.getPlId();
		String plIdTarget = searchVO.getPlIdTarget();
		searchVO.setPlId(plIdTarget);
		
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
		QuizVO quizVO = new QuizVO();
		for(int i=0; i<quizList.size(); i++) {
			quizVO.setPlId(plId);
			quizVO.setQuizNm(quizList.get(i).getQuizNm());
			quizVO.setAtchFileId(quizList.get(i).getAtchFileId());
			quizVO.setQuizType(quizList.get(i).getQuizType());
			quizVO.setUserId(user.getId());
			String quizId = quizService.insertQuiz(quizVO);
			
			quizVO.setQuizId(quizId);
			quizVO.setAtchFileId(quizList.get(i).getAtchFileId());
			quizVO.setExamType(quizList.get(i).getExamType());
			quizVO.setAnswer1(quizList.get(i).getAnswer1());
			quizVO.setAnswer2(quizList.get(i).getAnswer2());
			quizVO.setAnswer3(quizList.get(i).getAnswer3());
			quizVO.setAnswer4(quizList.get(i).getAnswer4());
			quizVO.setAnswer5(quizList.get(i).getAnswer5());
			quizVO.setAnswerNum(quizList.get(i).getAnswerNum());
			quizService.insertQuizAnswer(quizVO);
		}
		if("ON".equals(searchVO.getQuizType())) {
			return "forward:/lms/quiz/QuizRegView.do";
		}else {
			return "forward:/lms/quiz/QuizAnswerRegView.do"; 
		}
		
	}
	
	
	@RequestMapping("/lms/quiz/QuizAnswerUpdate.do")
	public String QuizAnswerUpdate(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, Model model
    		, HttpServletRequest request
    		, HttpServletResponse response
    		, final MultipartHttpServletRequest multiRequest
   	)throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		
		quizService.updateQuizExamAnswer(searchVO);
		model.addAttribute("updAnswerYn", "Y");
		
		return "/lms/manage/quizReg";
	}
	
	//퀴즈 삭제 처리 
	@RequestMapping("/lms/quiz/QuizDel.do")
    public String QuizDel(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, Model model
    		, HttpServletRequest request
    		, HttpServletResponse response
    		, final MultipartHttpServletRequest multiRequest
    	)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		if("ALL".equals(searchVO.getDelDiv())) {
			QuizVO quizVO = new QuizVO();
			quizVO.setUserId(user.getId());
			quizVO.setLastUpdusrId(user.getId());
			quizVO.setUseAt("N");
			quizVO.setPlId(searchVO.getPlId());
			if(EgovStringUtil.isEmpty(searchVO.getQuizType())) {
				quizVO.setQuizType("ON");
			}else {
				quizVO.setQuizType(searchVO.getQuizType());
			}
			quizService.updateQuiz_D(quizVO);
			quizService.updateQuizAnswer_D(quizVO);
		}else {
			searchVO.setLastUpdusrId(user.getId());
			searchVO.setUseAt("N");
			quizService.updateQuiz_D(searchVO);
			quizService.updateQuizAnswer_D(searchVO);
			quizService.updateQuiz_Sort(searchVO);
		}
		if("eval".equals(searchVO.getPageDiv())) {
			//return "redirect:/lms/quiz/QuizEvalList.do?plId="+searchVO.getPlId()+"&crclId="+searchVO.getCrclId()+"&menuId="+searchVO.getMenuId();
			return "forward:/lms/quiz/QuizList.do";
		}else {
			return "redirect:/lms/quiz/QuizRegView.do?plId="+searchVO.getPlId()+"&crclId="+searchVO.getCrclId()+"&menuId="+searchVO.getMenuId()+"&quizType=ON";
		}
	}
	
	
	
	//퀴즈 일괄 Sort 수정
	@RequestMapping("/lms/quiz/QuizSortBatch.do")
    public String QuizSortBatch(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, Model model
    		, HttpServletRequest request
    		, HttpServletResponse response
    	)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		QuizVO quizVO = new QuizVO();
		quizVO.setUserId(user.getId());
		quizVO.setLastUpdusrId(user.getId());
		quizVO.setQuizType("ON");
		String[] quizListSort = request.getParameterValues("quizListSort");
		for (int i = 0; i < quizListSort.length; i++) {
			quizVO.setQuizId(quizListSort[i]);
			quizVO.setQuizSort(quizListSort.length-i);
			quizService.updateQuiz(quizVO);
		}
		
		//return "redirect:/lms/quiz/QuizRegView.do?plId="+searchVO.getPlId()+"&crclId="+searchVO.getCrclId()+"&menuId="+searchVO.getMenuId()+"&quizType=ON";
		return "forward:/lms/quiz/QuizRegView.do";
	}
	
	
	//답안 등록페이지 이동
	@RequestMapping("/lms/quiz/QuizAnswerRegView.do")
    public String QuizAnswerRegView(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, Model model
    		, HttpServletRequest request
    		, HttpServletResponse response
    	)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setSortDiv("ASC");
		
		//해당 과정 QUIZ 정보 가져오기
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
		model.addAttribute("resultList", quizList);
		
		QuizVO quizVO = new QuizVO();
		quizVO.setPlId(searchVO.getPlId());
		int totCnt = quizService.selectQuizListTotCnt(quizVO);
		model.addAttribute("totCnt", totCnt);
				
		return "/lms/manage/quizAnswerReg";
	}
	
	
	//답안 등록 및 수정
	@RequestMapping("/lms/quiz/QuizAnswerReg.do")
    public String QuizAnswerReg(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setExamType("1");
		
		if("Y".equals(searchVO.getQuizEnd()) || "P".equals(searchVO.getQuizEnd())) {
			String[] arrquizIdArray = request.getParameterValues("quizIdArray");
			String[] arrayAnswerNum = request.getParameterValues("answerNum");
			for (int i = 0; i < arrayAnswerNum.length; i++) {
				searchVO.setQuizId(arrquizIdArray[i]);
				searchVO.setAnswerNum(arrayAnswerNum[i]);
				quizService.updateQuizAnswer(searchVO);
				quizService.updateQuizExamAnswer(searchVO);
			}
		}else {
			quizService.deleteOffQuizAnswer(searchVO);
			quizService.deleteOffQuiz(searchVO);
			String[] arrayAnswerNum = request.getParameterValues("answerNum");
			for (int i = 0; i < arrayAnswerNum.length; i++) {
				searchVO.setAnswerNum(arrayAnswerNum[i]);
				searchVO.setQuizSort(i+1);
				
				String quizId = quizService.insertQuiz(searchVO);
				searchVO.setQuizId(quizId);
				quizService.insertQuizAnswer(searchVO);
			} 
		}
			
		//해당 과정 QUIZ 정보 가져오기
		searchVO.setSortDiv("ASC");
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
		model.addAttribute("resultList", quizList);
		
		return "/lms/manage/quizAnswerReg";
	}
	
	
	/*
	@RequestMapping(value = "/lms/quiz/BeforeQuizList.do")
	public void BeforeQuizList(
			@ModelAttribute("searchVO") QuizVO searchVO
			, Model model
			, HttpServletRequest request
			, HttpServletResponse response
		) throws Exception {
		String successYn = "Y";
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(searchVO);

		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("beforeQuizList", beforeQuizList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	*/
	
	
	//미리보기 Ajax Page
	@RequestMapping(value = "/lms/quiz/PreQuizListAjax.json")
	public void PreQuizListAjax(
			@ModelAttribute("searchVO") QuizVO searchVO
			, Model model
			, HttpServletRequest request
			, HttpServletResponse response
		) throws Exception {
		String successYn = "Y";
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		List<QuizVO> preQuizList = quizService.selectPreQuizList(searchVO);

		//퀴즈 풀이_학생용
		if("stu_exam".equals(searchVO.getPageDiv())) {
			String[] arrayAnswerNum = request.getParameterValues("answerNum");
			if(arrayAnswerNum.length>0) {
				QuizVO quizVO = quizService.selectQuizExam_S(searchVO);
				for (int i = 0; i < arrayAnswerNum.length; i++) {
					if(i==0) {
						searchVO.setExamineeAns(arrayAnswerNum[i]);
					}else {
						searchVO.setExamineeAns(searchVO.getExamineeAns()+","+arrayAnswerNum[i]);
					}
				}
				if(EgovStringUtil.isEmpty(quizVO.getExamineeAns())) {
					quizService.insertQuizExam(searchVO);
				}else {
					quizService.updateQuizExam(searchVO);
				}
			}
		}
		
		JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("preQuizList", preQuizList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	
	//퀴즈 리스트 화면
	@RequestMapping(value="/lms/quiz/QuizEvalList.do")
    public String selectQuizEvalList(@ModelAttribute("searchVO") QuizVO searchVO, ScheduleMngVO scheduleMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		
		List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);
    	
    	searchVO.setUserSe(user.getUserSe());
        List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(searchVO);
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy");
        Date curyyyy = new Date();
        String yyyy = format1.format(curyyyy);
        model.addAttribute("yyyy", yyyy);
        
        
        model.addAttribute("search2", searchVO);
        return "/lms/manage/quizEvalList";
    }
	
	
	//퀴즈 평가결과조회
	@RequestMapping(value="/lms/quiz/QuiResultList.do")
    public String selectQuiResultList(@ModelAttribute("searchVO") QuizVO searchVO, ScheduleMngVO scheduleMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String retVal = "/lms/manage/quizResult";
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		
		List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);
    	
        List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(searchVO);
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        
        if(!"08".equals(user.getUserSe())){
        	//전체 성적 지표
        	EgovMap summary = quizService.quizExamSummary(searchVO);
        	model.addAttribute("summary", summary);
        	
        	searchVO.setSearchUserId(user.getId());
        	retVal = "/lms/manage/quizResultStu";
        }
        List<QuizVO> quizListResult = quizService.selectQuizResult_Report(searchVO);
		model.addAttribute("quizResult", quizListResult);
		
        model.addAttribute("search2", searchVO);
        return retVal;
    }
	
	
	//퀴즈 문제 풀이 시작
	@RequestMapping(value="/lms/quiz/QuizSolveInfo.do")
    public String selectQuizSolveInfo(@ModelAttribute("searchVO") QuizVO searchVO, ScheduleMngVO scheduleMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		searchVO.setSortDiv("ASC");
        
		List<QuizVO> quizList = quizService.selectQuizList(searchVO);
        model.addAttribute("resultList", quizList);
        
        int totCnt = quizService.selectQuizListTotCnt(searchVO);
        model.addAttribute("totCnt", totCnt);		
        
        //List<QuizVO> beforeQuizList = quizService.selectBeforeQuizList(searchVO);
        //model.addAttribute("beforeQuizList", beforeQuizList);
        
        if("08".equals(user.getUserSe())) {
        	//응시자, 제출자
        	QuizVO quizVO = quizService.selectQuizExam_Report(searchVO);
        	model.addAttribute("quizVO", quizVO );
        }
		
        //해당 퀴즈 제출여부 체크 Start
        if(quizList.size()>0) {
        	for(int i=0; i<1; i++) {
        		searchVO.setQuizId(quizList.get(i).getQuizId());
        	}
        }
        
        int examCnt = quizService.selectQuizExam_Cnt(searchVO);
        
        //해당 퀴즈 제출여부 체크 End
        if(examCnt > 0 ) {
        	QuizVO quizVO = quizService.selectQuizExam_Report(searchVO);
			model.addAttribute("quizVO", quizVO );
			
        	model.addAttribute("examExi", "Y");
        	model.addAttribute("status", "end");
        	//return "/lms/manage/quizEvalList";
        }else {
        	model.addAttribute("status", "start");
        }
        
        return "/lms/manage/quizSolveInfo";
    }
	
	
	@RequestMapping(value = "/lms/quiz/PreQuizList.do")
	public String PreQuizList(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String status = "ing";
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		searchVO.setSortDiv("ASC");
		
        List<QuizVO> quizList = quizService.selectQuizList(searchVO);
        model.addAttribute("resultList", quizList);
        
        int totCnt = quizService.selectQuizListTotCnt(searchVO);
        model.addAttribute("totCnt", totCnt);		
        
		QuizVO quiz= quizService.selectPreQuizList_S(searchVO);
		
		//퀴즈 풀이_학생용
		if("stu_exam".equals(searchVO.getPageDiv()) && !"08".equals(user.getUserSe())) {
			String[] arrayAnswerNum = request.getParameterValues("answerNum");
			if(arrayAnswerNum.length>0) {
				int examCnt = quizService.selectQuizExam_Cnt(searchVO);
				for (int i = 0; i < arrayAnswerNum.length; i++) {
					if(i==0) {
						searchVO.setExamineeAns(arrayAnswerNum[i]);
					}else {
						searchVO.setExamineeAns(searchVO.getExamineeAns()+","+arrayAnswerNum[i]);
					}
				}
				
				if(examCnt == 0) {
					quizService.insertQuizExam(searchVO);
				}else {
					quizService.updateQuizExam(searchVO);
				}
			}
		}
		
		
		if("08".equals(user.getUserSe())) {
			searchVO.setQuizId(quiz.getQuizId());
			
			//답안 선택별 List 가져오기
			List<QuizVO> quizAnsList = quizService.selectQuizExamAnswer_Cnt(searchVO);
			QuizVO quizVO = quizService.selectQuizExam_Report(searchVO);
			if(quizVO != null){
				//답안 선택 비율 구하기
				double temp1 = 0;
				double temp2 = (double)quizVO.getExamineeCnt();
				quiz.setSubAnswer1(0);
				quiz.setSubAnswer1p(0);
				quiz.setSubAnswer2(0);
				quiz.setSubAnswer2p(0);
				quiz.setSubAnswer3(0);
				quiz.setSubAnswer3p(0);
				quiz.setSubAnswer4(0);
				quiz.setSubAnswer4p(0);
				quiz.setSubAnswer5(0);
				quiz.setSubAnswer5p(0);
				for(int i=0; i<quizAnsList.size(); i++) {
					String examineeAnsSpl[] = quizAnsList.get(i).getExamineeAns().split(",");
					for(int a=0; a<examineeAnsSpl.length; a++){
						if("1".equals(examineeAnsSpl[a])) {
							quiz.setSubAnswer1(quizAnsList.get(i).getAnswerCnt());
							temp1 = (double)quizAnsList.get(i).getAnswerCnt();
							quiz.setSubAnswer1p((int) Math.round((temp1/temp2)*100));
						}else if("2".equals(examineeAnsSpl[a])) {
							quiz.setSubAnswer2(quizAnsList.get(i).getAnswerCnt());
							temp1 = (double)quizAnsList.get(i).getAnswerCnt();
							quiz.setSubAnswer2p((int) Math.round((temp1/temp2)*100));
						}else if("3".equals(examineeAnsSpl[a])) {
							quiz.setSubAnswer3(quizAnsList.get(i).getAnswerCnt());
							temp1 = (double)quizAnsList.get(i).getAnswerCnt();
							quiz.setSubAnswer3p((int) Math.round((temp1/temp2)*100));
						}else if("4".equals(examineeAnsSpl[a])) {
							quiz.setSubAnswer4(quizAnsList.get(i).getAnswerCnt());
							temp1 = (double)quizAnsList.get(i).getAnswerCnt();
							quiz.setSubAnswer4p((int) Math.round((temp1/temp2)*100));
						}else if("5".equals(examineeAnsSpl[a])) {
							quiz.setSubAnswer5(quizAnsList.get(i).getAnswerCnt());
							temp1 = (double)quizAnsList.get(i).getAnswerCnt();
							quiz.setSubAnswer5p((int) Math.round((temp1/temp2)*100));
						}
					}
					/*
					if("1".equals(quizAnsList.get(i).getExamineeAns())) {
						quiz.setSubAnswer1(quizAnsList.get(i).getAnswerCnt());
						temp1 = (double)quizAnsList.get(i).getAnswerCnt();
						quiz.setSubAnswer1p((int) Math.round((temp1/temp2)*100));
					}else if("2".equals(quizAnsList.get(i).getExamineeAns())) {
						quiz.setSubAnswer2(quizAnsList.get(i).getAnswerCnt());
						temp1 = (double)quizAnsList.get(i).getAnswerCnt();
						quiz.setSubAnswer2p((int) Math.round((temp1/temp2)*100));
					}else if("3".equals(quizAnsList.get(i).getExamineeAns())) {
						quiz.setSubAnswer3(quizAnsList.get(i).getAnswerCnt());
						temp1 = (double)quizAnsList.get(i).getAnswerCnt();
						quiz.setSubAnswer3p((int) Math.round((temp1/temp2)*100));
					}else if("4".equals(quizAnsList.get(i).getExamineeAns())) {
						quiz.setSubAnswer4(quizAnsList.get(i).getAnswerCnt());
						temp1 = (double)quizAnsList.get(i).getAnswerCnt();
						quiz.setSubAnswer4p((int) Math.round((temp1/temp2)*100));
					}else if("5".equals(quizAnsList.get(i).getExamineeAns())) {
						quiz.setSubAnswer5(quizAnsList.get(i).getAnswerCnt());
						temp1 = (double)quizAnsList.get(i).getAnswerCnt();
						quiz.setSubAnswer5p((int) Math.round((temp1/temp2)*100));
					}
					*/
				}
			}
			
			List<QuizVO> quizListResult = quizService.selectQuizResult_Report(searchVO);
			model.addAttribute("quizResult", quizListResult);
			model.addAttribute("quizVO", quizVO );
		}
		model.addAttribute("quiz", quiz);
		
		//최종 제출하기
		if("Y".equals(searchVO.getSubmitAt())) {
			if(!"08".equals(user.getUserSe())){
				quizService.updateQuizExam_Submit(searchVO);
				QuizVO quizVO = quizService.selectQuizExam_Report(searchVO);
				model.addAttribute("quizVO", quizVO );
			}
			status = "end";
		}
		
		model.addAttribute("status", status);
		
		return "/lms/manage/quizSolveInfo";
	}
	
	
	@RequestMapping(value = "/lms/quiz/quizEnd.do")
	public String quizEnd(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		
		if("08".equals(user.getUserSe())) {
			quizService.updateQuizEnd(searchVO);
		}
		
		model.addAttribute("closeYn", "Y");
		
		return "/lms/manage/quizSolveInfo";
	}
	
	//퀴즈 시작
	@RequestMapping(value = "/lms/quiz/quizStart.do")
	public void quizStart(@ModelAttribute("searchVO") QuizVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String successYn = "N";
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		
		if("08".equals(user.getUserSe())) {
			searchVO.setQuizEnd("P");
			quizService.updateQuizEnd(searchVO);
			successYn = "Y";
		}
		
		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
	}
	
	@RequestMapping(value="/lms/quiz/QuizSolveFooter.do")
    public String selectQuizSolveFooter(
    		@ModelAttribute("searchVO") QuizVO searchVO
    		, ScheduleMngVO scheduleMngVO
    		, ModelMap model
    		, HttpServletRequest request
    		, HttpServletResponse response
    ) {
		return "/lms/manage/quizSolveFooter";
	}
	
	
	@RequestMapping(value="/lms/quiz/QuizExamList.do")
    public String selectQuizExamList(@ModelAttribute("searchVO") QuizVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setUserId(user.getId());
		searchVO.setUserSe(user.getUserSe());
		
		model.addAttribute("USER_INFO", user);
		
		List<QuizVO> beforeQuizList = null;
		try {
			beforeQuizList = quizService.selectBeforeQuizList(searchVO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("beforeQuizList", beforeQuizList);
        
        Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
        try {
			model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        CurriculumVO curriculumVO = new CurriculumVO();
        curriculumVO.setSearchSchAt("Y");
        List<?> curriculumList = null;
		try {
			curriculumList = curriculumService.selectCurriculumList(curriculumVO, request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("resultList", curriculumList);
        
        /** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		paginationInfo.setTotalRecordCount(beforeQuizList.size());
        model.addAttribute("paginationInfo", paginationInfo);
        
		return "/lms/manage/quizExamList";
	}
	
}