package egovframework.com.lms.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.msi.service.ContentsServiceVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.mpm.service.EgovMpmService;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.ion.bnr.service.BannerVO;
import egovframework.com.uss.ion.bnr.service.EgovBannerService;
import egovframework.com.uss.ion.pwm.service.EgovPopupManageService;
import egovframework.com.uss.ion.pwm.service.PopupManageVO;
import egovframework.com.uss.ion.sit.service.EgovLinkSiteManageService;
import egovframework.com.uss.ion.sit.service.LinkSiteManageVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import egovframework.com.evt.service.ComtnschdulinfoService;
import egovframework.com.evt.service.ComtnschdulinfoVO;

@Controller
public class AcademicController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertyService;
	
	@Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
	
	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;
	
	@RequestMapping(value = "/lms/common/flag.do")
	public String flag(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return "/lms/common/flag";
	}
	
	@RequestMapping(value = "/lms/common/app.do")
	public String app(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		return "/lms/common/app";
	}
	
	@RequestMapping(value="/lms/academic/academicSystem.do")
    public String academicSystem(@ModelAttribute("searchVO") Ctgry searchVO, ModelMap model) throws Exception {
		
		//과정체계관리 마스터코드
		searchVO.setCtgrymasterId("CTGMST_0000000000004");
        model.addAttribute("resultList", egovBBSCtgryService.selectComtnbbsctgryList(searchVO));
        
        return "/lms/academic/academicSystem";
    }
	
}
