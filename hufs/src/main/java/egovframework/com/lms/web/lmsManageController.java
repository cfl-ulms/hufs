package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.GradeService;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;


@Controller
public class lmsManageController {

	 @Resource(name = "EgovBBSAttributeManageService")
	 private EgovBBSAttributeManageService bbsAttrbService;

	 @Resource(name = "EgovBBSCtgryMasterService")
	 private EgovBBSCtgryMasterService egovBBSCtgryMasterService;

	 @Resource(name = "EgovBBSCtgryService")
	 private EgovBBSCtgryService egovBBSCtgryService;

	 @Resource(name = "curriculumService")
	 private CurriculumService curriculumService;

	 @Resource(name="EgovCmmUseService")
	 private EgovCmmUseService cmmUseService;

	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;

    @Resource(name = "gradeService")
	private GradeService gradeService;

    /**
     * XSS 방지 처리.
     *
     * @param data
     * @return
     */
    protected String unscript(String data) {
      if(data == null || data.trim().equals("")) {
        return "";
      }

      String ret = data;

      ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
      ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

      ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
      ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

      ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
      ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

       ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
       ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

      ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

      return ret;
    }

    //과정관리
    @RequestMapping(value="/lms/manage/lmsControl.do")
    public String lmsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

    	//게시판 카테고리 목록
		CtgryMaster ctgryMaster = new CtgryMaster();
		ctgryMaster.setSiteId(searchVO.getCrclId());
		List<CtgryMaster> ctgryMasterList = egovBBSCtgryMasterService.selectCtgrymasterList(ctgryMaster);

		List<Ctgry> ctgryList = new ArrayList<Ctgry>();
		for(int i = 0; i < ctgryMasterList.size(); i++){
			Ctgry ctgry = new Ctgry();
			ctgry.setCtgrymasterId(ctgryMasterList.get(i).getCtgrymasterId());
			List<Ctgry> ctgryResultList = egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
			//카테고리 리스트를 하나로 합침
			for(int a = 0; a < ctgryResultList.size(); a++){
				Ctgry ctgryTemp = ctgryResultList.get(a);
				ctgryList.add(ctgryTemp);
			}
		}
		model.addAttribute("ctgryList", ctgryList);

		//과정정보
		CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(curriculumVO));

		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map1 = (EgovMap) subUserList.get(i);
          		String userId = map1.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/manage/lmsControl";
    }

    //과정게시판관리
    @RequestMapping(value="/lms/manage/lmsBbsControl.do")
    public String lmsBbsControl(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

		//과정정보
		CurriculumVO curriculumVO = new CurriculumVO();
		curriculumVO.setCrclId(searchVO.getCrclId());
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(curriculumVO));

		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

        return "/lms/manage/lmsBbsControl";
    }

  //과정게시판삭제
    @RequestMapping(value="/lms/manage/lmsBbsDelete.do")
    public void lmsBbsDelete(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

    	//게시판 마스터 삭제
   		BoardMasterVO boardMasterVO = new BoardMasterVO();
   		boardMasterVO.setBbsId(searchVO.getBbsId());
   		boardMasterVO.setSiteId(searchVO.getCrclId());
   		boardMasterVO.setCtgrymasterId(searchVO.getCtgrymasterId());
   		boardMasterVO.setLastUpdusrId(user.getId());

   		bbsAttrbService.deleteBBSMasterInf(boardMasterVO);

   		//카테고리 삭제
		CtgryMaster ctgryMaster = new CtgryMaster();
		ctgryMaster.setCtgrymasterId(searchVO.getCtgrymasterId());
		ctgryMaster.setSiteId(searchVO.getCrclId());
		egovBBSCtgryMasterService.deleteComtnbbsctgrymaster(ctgryMaster);

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    @RequestMapping("/cop/bbs/ctg/insertBBSCtgryMaster.json")
    public void addComtnbbsctgrymasterJson(CtgryMaster ctgryMaster, @ModelAttribute("searchVO") CtgryMaster searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String successYn = "Y";

        egovBBSCtgryMasterService.insertComtnbbsctgrymaster(ctgryMaster);
        String ctgrymasterId = egovBBSCtgryMasterService.insertComtnbbsctgrymaster(ctgryMaster);

        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("ctgrymasterId", ctgrymasterId);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    @RequestMapping(value="/lms/manage/selectBBSCtgryList.json")
    public void selectBBSCtgryList(@ModelAttribute("searchVO") Ctgry searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	 String successYn = "Y";

         List<Ctgry> tagList =  egovBBSCtgryService.selectComtnbbsctgryList(searchVO);

         JSONObject jo = new JSONObject();
   		response.setContentType("application/json;charset=utf-8");

 		jo.put("successYn", successYn);
 		jo.put("tagList", tagList);

 		PrintWriter printwriter = response.getWriter();
 		printwriter.println(jo.toString());
 		printwriter.flush();
 		printwriter.close();
    }

  //태그 삭제
    @RequestMapping(value="/lms/manage/tagDelete.do")
    public void tagDelete(@ModelAttribute("searchVO") Ctgry searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	egovBBSCtgryService.deleteComtnbbsctgry(searchVO); // 태그 삭제

    	List<Ctgry> tagList =  egovBBSCtgryService.selectComtnbbsctgryList(searchVO); // 목록 조회

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("tagList", tagList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

  //태그 추가
    @RequestMapping(value="/lms/manage/tagAdd.do")
    public void tagAdd(@ModelAttribute("searchVO") Ctgry searchVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";

    	searchVO.setSortOrdr(1);
    	searchVO.setCtgryLevel(1);
    	//searchVO.setCtgryCn("");
    	//searchVO.setCtgryVal("");
    	egovBBSCtgryService.insertComtnbbsctgry(searchVO); // 태그 추가

    	List<Ctgry> tagList =  egovBBSCtgryService.selectComtnbbsctgryList(searchVO); // 목록 조회

		JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);
		jo.put("tagList", tagList);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

  //과정게시판등록
    @RequestMapping(value="/lms/manage/lmsBbsInsert.do")
    public String lmsBbsInsert(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

    	//게시판 마스터 등록&수정
    	//기본 게시판은 수정처리 안함
    	for(int i = 1; i < searchVO.getBbsIdList().size(); i++){
    		BoardMasterVO boardMasterVO = new BoardMasterVO();
    		boardMasterVO.setCtgrymasterId(searchVO.getCtgrymasterIdList().get(i));
    		boardMasterVO.setSiteId(searchVO.getCrclId());
    		boardMasterVO.setSysTyCode(searchVO.getSysTyCodeList().get(i));
    		boardMasterVO.setBbsId(searchVO.getBbsIdList().get(i));
    		boardMasterVO.setBbsNm(searchVO.getBbsNmList().get(i));
    		boardMasterVO.setCommentUseAt("N");
    		boardMasterVO.setReplyPosblAt("N");
    		boardMasterVO.setFileAtchPosblAt("Y");
    		boardMasterVO.setPosblAtchFileNumber("10");
    		boardMasterVO.setPosblAtchFileSize("1024");
    		boardMasterVO.setInqireAuthor("02");
    		boardMasterVO.setRegistAuthor("10");
    		boardMasterVO.setAnswerAuthor("02");
    		boardMasterVO.setOthbcUseAt("N");
    		boardMasterVO.setSourcId(propertiesService.getString("Crcl.sourcId"));
    		boardMasterVO.setTmplatId(propertiesService.getString("Crcl.tmplatId"));
    		boardMasterVO.setSvcAt("Y");
    		if(EgovStringUtil.isEmpty(searchVO.getBbsIdList().get(i))){
    			boardMasterVO.setFrstRegisterId(user.getId());
    			bbsAttrbService.insertBBSMastetInf(boardMasterVO);
    		}else{
    			boardMasterVO.setLastUpdusrId(user.getId());
    			bbsAttrbService.updateBBSMasterInf(boardMasterVO);
    		}
    	}

        return "redirect:/lms/manage/lmsControl.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId();
    }

    //과정 시간표 설정
    @RequestMapping(value="/lms/manage/schedule.do")
    public String schedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));

    	//등록 된 시간표 담당교원
    	searchVO.setPlId("");
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

    	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/manage/schedule";
    }

    //과정 시간표 등록&수정
    @RequestMapping(value="/lms/manage/scheduleReg.do")
    public String scheduleReg(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정시간코드
    	searchVO.setPlType("crcl");

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
        	model.addAttribute("curriculumVO", curriculumVO);
        	
    		searchVO.setCrclLang(request.getParameter("crclLang"));
    		
    		//시간표 수기 입력
    		if("N".equals(curriculumVO.getCampustimeUseAt())) {
    			searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
    			searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());
    		}
    		
    		//등록
    		if(EgovStringUtil.isEmpty(searchVO.getPlId())){
    			searchVO.setFrstRegisterId(user.getId());
        		scheduleMngService.insertClassSchedule(searchVO);
    		}else{ //수정
    			searchVO.setLastUpdusrId(user.getId());
    			scheduleMngService.updateClassSchedule(searchVO, request, response);
    		}

        	//과정에 등록된 교원
        	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

        	//캠퍼스 시간표
        	searchVO.setCampusId(curriculumVO.getCampusId());
        	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

        	//등록 된 시간표
        	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
        	scheduleMngVO.setPeriodTxt(searchVO.getPeriodTxt());
        	model.addAttribute("scheduleMngVO", scheduleMngVO);

        	//등록 된 시간표 담당교원
        	searchVO.setFacId("");
        	List facPlList = scheduleMngService.selectFacultyList(searchVO);
        	model.addAttribute("facPlList", facPlList);

        	//과정 담당자 인지 확인
          	String managerAt = "N";
          	if(user != null){
          		curriculumVO.setManageCode("");
          		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
              	for(int i = 0; i < subUserList.size(); i ++){
              		EgovMap map = (EgovMap) subUserList.get(i);
              		String userId = map.get("userId").toString();
              		if(user.getId().equals(userId)){
              			managerAt = "Y";
              		}
              	}
          	}
          	model.addAttribute("managerAt", managerAt);

    	}else{

    	}

		return "/lms/manage/scheduleIstAjax";
    }

    //과정 시간표 삭제
    @RequestMapping(value="/lms/manage/dltSchedule.json")
    public void dltSchedule(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";

    	scheduleMngService.deleteClassSchedule(searchVO);

    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

  //과정 시간표 설정
    @RequestMapping(value="/lms/manage/scheduleCalendar.do")
    public String scheduleCalendar(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	searchVO.setPlType("crcl");

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	Ctgry ctgry = new Ctgry();
    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));
    	model.addAttribute("crclId", searchVO.getCrclId());


        return "/lms/manage/scheduleCalendar";
    }

    //과정 시간표 상세
    @RequestMapping(value="/lms/manage/scheduleCalendarView.do")
    public String scheduleCalendarView(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	searchVO.setPlType("crcl");

    	Ctgry ctgry = new Ctgry();

    	//언어
    	ctgry.setCtgrymasterId("CTGMST_0000000000002");
    	model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//캠퍼스 카테고리 리스트
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//과정에 등록된 교원
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//과정 시간표
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));
    	model.addAttribute("scheduleMngVO", scheduleMngService.selectCalendarScheduleView(searchVO));
    	model.addAttribute("crclLang", request.getParameter("crclLang"));

        return "/lms/manage/scheduleCalendarView";
    }

  //과정 시간표 등록&수정
    @RequestMapping(value="/lms/manage/scheduleCalendarReg.do")
    public String scheduleCalendarReg(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정시간코드
    	searchVO.setPlType("crcl");

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setCrclLang(request.getParameter("crclLang"));
    		searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
      		searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());

    		//등록
    		if(EgovStringUtil.isEmpty(searchVO.getPlId())){
    			searchVO.setFrstRegisterId(user.getId());
        		scheduleMngService.insertClassSchedule(searchVO);
    		}else{ //수정
    			searchVO.setLastUpdusrId(user.getId());
    			scheduleMngService.updateClassSchedule(searchVO, request, response);
    		}

        	//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
        	model.addAttribute("curriculumVO", curriculumVO);

        	//과정에 등록된 교원
        	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(curriculumVO));

        	//캠퍼스 시간표
        	searchVO.setCampusId(curriculumVO.getCampusId());
        	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

        	//등록 된 시간표
        	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
        	scheduleMngVO.setPeriodTxt(searchVO.getPeriodTxt());
        	model.addAttribute("scheduleMngVO", scheduleMngVO);

        	//등록 된 시간표 담당교원
        	List facPlList = scheduleMngService.selectFacultyList(searchVO);
        	model.addAttribute("facPlList", facPlList);
    	}else{

    	}

		return "redirect:/lms/manage/scheduleCalendar.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId()+"&step=4";
    }

  //과정 수업계획
    @RequestMapping(value="/lms/manage/studyPlan.do")
    public String studyPlan(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정 시간표(수업계획)
    	model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(searchVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(curriculumVO));

    	//오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/manage/studyPlan";
    }

    //과정 수업계획 상세
    @RequestMapping(value="/lms/manage/studyPlanView.do")
    public String studyPlanView(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//수업계획
    	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
    	model.addAttribute("scheduleMngVO", scheduleMngVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//사진경로
		model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

    	//수업 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String studyMngAt = "N";
      	if(user != null){
      		for(int i = 0; i < facPlList.size(); i ++){
          		EgovMap map = (EgovMap) facPlList.get(i);
          		String userId = map.get("facId").toString();
          		if(user.getId().equals(userId)){
          			studyMngAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("studyMngAt", studyMngAt);

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

		//과정에 등록된 평가
    	model.addAttribute("evtResultList", scheduleMngService.selectStudyEvtList(searchVO));

    	//등록한 수업자료 리스트
    	model.addAttribute("fileList", scheduleMngService.selectStudyFileList(searchVO));

        return "/lms/manage/studyPlanView";
    }

    //과정 수업계획 등록&수정 페이지
    @RequestMapping(value="/lms/manage/studyPlanReg.do")
    public String studyPlanReg(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	  	model.addAttribute("siteInfo", siteVO);

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(searchVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//수업계획
    	ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
    	model.addAttribute("scheduleMngVO", scheduleMngVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

    	//등록 된 시간표 담당교원
    	List facPlList = scheduleMngService.selectFacultyList(searchVO);
    	model.addAttribute("facPlList", facPlList);

    	//캠퍼스 시간표
    	searchVO.setCampusId(curriculumVO.getCampusId());
    	model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

    	//캠퍼스
    	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//레벨
		ctgry.setCtgrymasterId("CTGMST_0000000000014");
		model.addAttribute("levelList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//온라인강의(강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000006");
		model.addAttribute("onList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//오프라인강의(비강의유형)
		ctgry.setCtgrymasterId("CTGMST_0000000000007");
		model.addAttribute("offList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//평가
		ctgry.setCtgrymasterId("CTGMST_0000000000008");
		model.addAttribute("evtList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//과정에 등록된 교원
    	model.addAttribute("evtResultList", scheduleMngService.selectStudyEvtList(searchVO));

    	//등록한 수업자료 리스트
    	model.addAttribute("fileList", scheduleMngService.selectStudyFileList(searchVO));

		request.getSession().setAttribute("sessionVO", searchVO);

        return "/lms/manage/studyPlanReg";
    }

    //과정 수업 등록&수정
    @RequestMapping(value="/lms/manage/studyPlanUpt.do")
    public String studyPlanUpt(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/lms/crclb/CurriculumbaseList.do";
		}

    	//과정시간코드
    	searchVO.setPlType("crcl");

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		//과정정보
        	CurriculumVO curriculumVO = new CurriculumVO();
        	curriculumVO.setCrclId(searchVO.getCrclId());
        	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    		
    		//시간표 수기 입력
    		if("N".equals(curriculumVO.getCampustimeUseAt())) {
    			searchVO.setStartTime(searchVO.getStartTimeHH() + searchVO.getStartTimeMM());
    			searchVO.setEndTime(searchVO.getEndTimeHH() + searchVO.getEndTimeMM());
    		}
    		
    		//등록
    		if(EgovStringUtil.isEmpty(searchVO.getPlId())){
    			searchVO.setFrstRegisterId(user.getId());
        		scheduleMngService.insertClassSchedule(searchVO);
    		}else{ //수정
    			searchVO.setLastUpdusrId(user.getId());
    			scheduleMngService.updateClassSchedule(searchVO, request, response);
    		}
    	}else{

    	}

    	request.getSession().removeAttribute("sessionVO");

		return "forward:/lms/manage/studyPlanView.do";
    }

    //과정게시판 선택 팝업
    @RequestMapping(value="/lms/manage/lmsBbsPopList.do")
    public String lmsBbsPopList(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정게시판 목록
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(searchVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		int totCnt = Integer.parseInt((String)map.get("resultCnt"));

		model.addAttribute("bbsMasterList", map.get("resultList"));
		model.addAttribute("totCnt", totCnt);

        return "/lms/manage/lmsBbsPopList";
    }

    //총괄평가기준 선택 팝업
    @RequestMapping(value="/lms/manage/lmsEvtPopList.do")
    public String lmsEvtPopList(@ModelAttribute("searchVO") LmsMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정체계
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000012");
		model.addAttribute("evaluationBaseList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        return "/lms/manage/lmsEvtPopList";
    }

    //수업 > 과제(수업과제)
  	@RequestMapping(value = "/lms/manage/homeworkList.do")
  	public String selectHomeworkList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	//등록 된 시간표 담당교원
      	ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());
    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

    	//과제 조회를 위한 유저 반,조 조회(학생 권한만 사용)
    	if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	    	searchVO.setUserId(user.getId());
	      	CurriculumVO curriculumMemberVO = curriculumService.selectCurriculumMemberDetail(searchVO);

	      	searchVO.setClassCnt(curriculumMemberVO.getClassCnt());
	  		searchVO.setGroupCnt(curriculumMemberVO.getGroupCnt());
    	}

  		//과제 조회
    	searchVO.setPagingFlag("N");
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

  		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        //수업 담당자 인지 확인
        String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);

      	model.addAttribute("USER_INFO", user);

      	//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	request.getSession().removeAttribute("sessionVO");

  		return "/lms/manage/HomeworkList";
  	}

  	//학생 과제 제출 페이지
  	@RequestMapping("/lms/manage/selectHomeworkSubmitArticle.do")
    public String selectHomeworkArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
      	model.addAttribute("homeworkVO", homeworkVO);

      	//과제 제출 상세 조회
      	if(!EgovStringUtil.isEmpty(searchVO.getHwsId())) {
      		CurriculumVO homeworkSubmitVO = curriculumService.selectHomeworkSubmitArticle(searchVO);
          	model.addAttribute("homeworkSubmitVO", homeworkSubmitVO);
      	}

      	//조원 조회
      	searchVO.setUserId(user.getId());
      	CurriculumVO curriculumMemberVO = curriculumService.selectCurriculumMemberDetail(searchVO);
      	model.addAttribute("curriculumMemberVO", curriculumMemberVO);

      	searchVO.setClassCnt(curriculumMemberVO.getClassCnt());
  		searchVO.setGroupCnt(curriculumMemberVO.getGroupCnt());

  		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
  		model.addAttribute("pickStudentList", selectPickStudentList);

  		//같은 조원인지 확인
  		String groupAt = "N";
  		if(user != null){
  			for(int i = 0; i < selectPickStudentList.size(); i ++){
  				EgovMap map = (EgovMap) selectPickStudentList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			groupAt = "Y";
          		}
  			}
  		}
  		model.addAttribute("groupAt", groupAt);

  		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

      	//등록 된 시간표 담당교원
  		ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());

    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

        //수업 담당자 인지 확인
        String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);

        model.addAttribute("USER_INFO", user);

  		model.addAttribute("BbsFileStoreWebPathByWebFile", propertiesService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));

  		//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

	    return "/lms/manage/HomeworkSubmitArticle";
    }

  	//과제 제출 등록
  	@RequestMapping("/lms/manage/insertHomeworkSubmitArticle.do")
    public String insertHomeworkSubmitArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "forward:/lms/manage/homeworkList.do?tabType=T";

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		searchVO.setFrstRegisterId(user.getId());
		searchVO.setNtcrNm(user.getName());

		String atchFileId = "";

	    List<FileVO> result = null;

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	        result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, "",
	        		siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
	        atchFileId = fileMngService.insertFileInfs(result);
	    }

	    searchVO.setAtchFileId(atchFileId);
	    searchVO.setFrstRegisterId(user.getId());
	    searchVO.setNtcrNm(user.getName());
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지

	    curriculumService.insertHomeworkSubmitArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//과제 수정
  	@RequestMapping("/lms/manage/updateHomeworkSubmitArticle.do")
    public String updateHomeworkSubmitArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "forward:/lms/manage/homeworkList.do?tabType=T";

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		String atchFileId = searchVO.getAtchFileId();

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
		    if(EgovStringUtil.isEmpty(atchFileId)) {
			  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
			    atchFileId = fileMngService.insertFileInfs(result);
			    searchVO.setAtchFileId(atchFileId);
		    } else {
			    FileVO fvo = new FileVO();
			    fvo.setAtchFileId(atchFileId);
			    //int cnt = fileMngService.getMaxFileSN(fvo);
			    List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getBbsId(), "", "");
			    fileMngService.updateFileInfs(_result);
		    }
	    }

	    searchVO.setLastUpdusrId(user.getId());
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지

	    curriculumService.updateHomeworkSubmitArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//수업 > 과제
  	@RequestMapping(value = "/lms/manage/homeworkTotalList.do")
  	public String selectHomeworkTotalList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());

  		//학생 권한일 경우 데이터 조회를 위한 flag처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setStudentPageAt("Y");
  		} else {
  			searchVO.setMyCurriculumPageFlag("teacherHomeworkTotalList");
  		}

  		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      	//과제 조회
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

  		//과제 총 합계
      	int totCnt = curriculumService.selectHomeworkTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);

  		if("Y".equals(searchVO.getStudentPageAt())) {
  			return "/lms/manage/HomeworkTotalStudentList";
  		} else {
  			return "/lms/manage/HomeworkTotalList";
  		}
  	}

  	//운영보고서
  	@RequestMapping(value = "/lms/manage/manageReport.do")
  	public String manageReport(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());
  		model.addAttribute("USER_INFO", user);
  		
  		CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

  		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

      	//등록 된 시간표 담당교원
  		ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());

    	List facPlList = scheduleMngService.selectCrclFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

    	List facPlListCnt = scheduleMngService.selectCrclFacultyListCnt(scheduleMngVO);
    	model.addAttribute("facPlListCnt", facPlListCnt);

        //수업 담당자 인지 확인
        String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);

        //학습내용
    	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

    	//교육과정 통계
  		model.addAttribute("curriculumSts", curriculumService.curriculumSts(searchVO));

    	//성적 목록
  		List gradeList = gradeService.selectGradeList(searchVO);
  		model.addAttribute("gradeList", gradeList);

        //과정 설문
        SurveyVO surveyVo = new SurveyVO();
        surveyVo.setCrclId(searchVO.getCrclId());
        //surveyVo.setSchdulId(this.surveyManageService.selectSurveyId(surveyVo));
        //과정만족도
        surveyVo.setSchdulId(curriculumVO.getSurveySatisfyType());
        if(!"".equals(surveyVo.getSchdulId().toString())){
        	//과정 추가 정보
        	model.addAttribute("curriculumAddInfo", this.surveyManageService.selectCurriculumAddInfo(surveyVo));
        	model.addAttribute("surveyAnswer", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVo));
        	
        	// 과정만족도 답변점수목록
        	model.addAllAttributes(this.surveyManageService.curriculumSurveyAnswerScore(surveyVo));
        }
        
        //과정만족도 - 교원대상
        SurveyVO surveyVO2 = new SurveyVO();
        surveyVO2.setCrclId(searchVO.getCrclId());
        surveyVO2.setSchdulId(curriculumVO.getProfessorSatisfyType());
        if(!EgovStringUtil.isEmpty(surveyVO2.getSchdulId())){
        	model.addAttribute("curriculumAddInfoType3", this.surveyManageService.selectCurriculumAddInfoType3(surveyVO2));
        	model.addAttribute("surveyAnswerType3", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVO2));
        	
        	// 교원 과정만족도 답변점수목록
        	model.addAllAttributes(this.surveyManageService.curriculumSurveyAnswerScore3(surveyVO2));
        	
        }
        
        //오늘 일자(인쇄시 사용)
        model.addAttribute("today", EgovDateUtil.getToday().replaceAll("-", ""));
        
		return "/lms/manage/manageReport";
  	}

  	//운영보고서 수정
  	@RequestMapping(value = "/lms/manage/updateReport.do")
  	public String updateReport(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());
  		if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}

  		curriculumService.updateCurriculumPart(searchVO, request, response);

		return "forward:/lms/manage/manageReport.do";
  	}
}
