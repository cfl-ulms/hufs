package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.Board;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CertificateVO;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

/**
 * @Class Name : CurriculumController.java
 * @Description : Curriculum Controller class
 * @Modification Information
 *
 * @author 문성진
 * @since 2019.10.12
 * @version 1.0
 * @see
 *
 *  Copyright (C)  All right reserved.
 */

@Controller
public class CurriculumController {

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

    /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

    /** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;

    @Resource(name = "EgovBBSAttributeManageService")
   	private EgovBBSAttributeManageService bbsAttrbService;

    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;

    @Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    @Resource(name = "propertiesService")
	protected EgovPropertyService         propertyService;

    @Resource(name = "SiteManageService")
	EgovSiteManageService 				  siteManageService;

    /**
     * XSS 방지 처리.
     *
     * @param data
     * @return
     */
    protected String unscript(String data) {
      if(data == null || data.trim().equals("")) {
        return "";
      }

      String ret = data;

      ret = ret.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;script");
      ret = ret.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "&lt;/script");

      ret = ret.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;object");
      ret = ret.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "&lt;/object");

      ret = ret.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;applet");
      ret = ret.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "&lt;/applet");

       ret = ret.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");
       ret = ret.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "&lt;embed");

      ret = ret.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "&lt;form");
      ret = ret.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "&lt;form");

      return ret;
    }

    //과정등록관리
    @RequestMapping(value="/lms/crcl/CurriculumList.do")
    public String selectCurriculumList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String retUrl = "/lms/crcl/CurriculumList";
    	if("Y".equals(searchVO.getMngAt()) || "MNU_0000000000000059".equals(searchVO.getMenuId())){
    		retUrl = "/lms/crcl/CurriculumMngList";
    	}else{
    		searchVO.setSearchAprvalAt("Y");
    	}

    	Ctgry ctgry = new Ctgry();

    	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//대상
  		ctgry.setCtgrymasterId("CTGMST_0000000000015");
  		model.addAttribute("targetList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    	//과정체계
		ctgry.setCtgrymasterId("CTGMST_0000000000004");
		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//이수구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000003");
  		model.addAttribute("divisionList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//관리구분
  		ctgry.setCtgrymasterId("CTGMST_0000000000005");
  		model.addAttribute("controlList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//언어
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//주관기관
		ctgry.setCtgrymasterId("CTGMST_0000000000009");
		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

    	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		/*
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		searchVO.setSearchHostCode(user.getMngDeptCode());
		searchVO.setUserSeCode(user.getUserSeCode());
		*/
		if("teacher".equals(searchVO.getMyCurriculumPageFlag())){
			LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
			searchVO.setUserId(user.getId());
			searchVO.setSearchCrclLang(user.getMajor());
		}
        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

        return retUrl;
    }

    //과정개설신청
    @RequestMapping("/lms/crcl/addCurriculumView.do")
    public String addCurriculumView(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user == null){
    		return "redirect:/index.do";
    	}else{
        	//과정체계
    		Ctgry ctgry = new Ctgry();
    		ctgry.setCtgrymasterId("CTGMST_0000000000004");
    		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//학기
    		ctgry.setCtgrymasterId("CTGMST_0000000000016");
    		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//언어
    		ctgry.setCtgrymasterId("CTGMST_0000000000002");
    		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//캠퍼스
    		ctgry.setCtgrymasterId("CTGMST_0000000000017");
    		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//주관기관
    		ctgry.setCtgrymasterId("CTGMST_0000000000009");
    		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//사업비
    		ctgry.setCtgrymasterId("CTGMST_0000000000018");
    		model.addAttribute("feeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//년도
    		model.addAttribute("yearList", new LmsCommonUtil().getYear());

    		//현재년도
    		model.addAttribute("todayYear", new LmsCommonUtil().getTodayYear());

    		//과정성과 - 공통코드(LMS10)
    		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
    	   	voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS10");
        	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("listComCode", listComCode);

        	model.addAttribute("curriculumVO", searchVO);
        	model.addAttribute("USER_INFO", user);
            request.getSession().setAttribute("sessionVO", searchVO);

            return "/lms/crcl/CurriculumRegister";
    	}
    }

    @RequestMapping("/lms/crcl/addCurriculum.do")
    public String addCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return "forward:/lms/crcl/CurriculumList.do";
		}

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    		if(Integer.parseInt(user.getUserSeCode()) > 8){
    			searchVO.setAprvalAt("Y");
    			searchVO.setRegistAt("Y");
    		}else{
    			searchVO.setProcessSttusCode("0");
    			searchVO.setAprvalAt("T");
    			searchVO.setRegistAt("N");
    		}
    	}else{
    		return "forward:/index.do";
    	}

        String crclId = curriculumService.insertCurriculum(searchVO);

        request.getSession().removeAttribute("sessionVO");

        return "redirect:/lms/crcl/selectCurriculum.do?crclId=" + crclId + "&menuId=" + searchVO.getMenuId();
        //return "forward:/lms/crcl/CurriculumList.do";
    }

    @RequestMapping("/lms/crcl/selectCurriculum.do")
    public String selectCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();

    	//교수가 작성한 교육과정개설신청 확인 시 필요
    	if("Y".equals(searchVO.getMngAt())){
    		//과정성과 - 공통코드(LMS10)
    	   	voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS10");
        	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("listComCode", listComCode);
    	}

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
      	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));

      	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
      	
      	String adminAt = "N";
      	if(user.getId().equals("admin")) {
      		adminAt = "Y";
      	}
      	model.addAttribute("adminAt", adminAt);

      	//과정만족도
		SurveyVO surveyVo = new SurveyVO();
		surveyVo.setSchdulClCode("TYPE_1");
		surveyVo.setRecordCountPerPage(1000);
		surveyVo.setFirstIndex(0);
		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList", resultMap.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_2");
		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
		model.addAttribute("surveyList2", resultMap2.get("resultList"));
		surveyVo.setSchdulClCode("TYPE_3");
 		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
 		model.addAttribute("surveyList3", resultMap3.get("resultList"));

      	//과정확정 이후 수정 시
    	if(Integer.parseInt(curriculumVO.getProcessSttusCode()) > 0){
    		//과정진행상태 - 공통코드(LMS30)
        	voComCode.setCodeId("LMS30");
        	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("statusComCode", statusComCode);

        	//오늘일자
        	model.addAttribute("today", EgovDateUtil.getToday());

        	//학습내용
        	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

        	//부책임교원
        	CurriculumVO curriculum = new CurriculumVO();
        	curriculum.setCrclId(searchVO.getCrclId());
        	//부책임교원 코드 08
        	curriculum.setManageCode("08");
        	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

        	//성적기준
        	Ctgry ctgry = new Ctgry();
    		ctgry.setCtgrymasterId("CTGMST_0000000000011");
    		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//총괄평가
    		List evaluationList = curriculumService.selectTotalEvaluation(curriculum);
    		model.addAttribute("evaluationList", evaluationList);

    		//수업참여도 게시판
			List attendBbsList = curriculumService.selectAttendbbs(curriculum);
			model.addAttribute("attendBbsList", attendBbsList);

    		//교원배정 - 총 시간 입력 시 나옴
			if("Y".equals(curriculumVO.getTotalTimeAt())){
				searchVO.setCpr("Y");//시간표 교원 비교하기 위해서 정렬
				List tempFacList = curriculumService.selectCurriculumFaculty(searchVO);
	        	List<EgovMap> facList = new ArrayList<EgovMap>();
	        	List<String> facIdList = new ArrayList<String>();
	        	if(tempFacList != null && tempFacList.size() > 0){
	        		EgovMap tempMap = new EgovMap();
	        		String prevPosition = "";
	        		String facId = "";
	        		String facNm = "";
	        		String prevSisu = "";
	        		String sisu = "";
	        		for(int i = 0; i < tempFacList.size(); i++){
	        			EgovMap map = (EgovMap) tempFacList.get(i);
	        			if(prevPosition.equals(map.get("position").toString()) || i == 0){
	        				facId += " " + map.get("facId").toString();
	        				facNm += " " + map.get("facNm").toString();

	        			}else{
	        				tempMap.put("facId", facId);
	        				tempMap.put("facNm", facNm);
	        				tempMap.put("sisu", prevSisu);
	        				tempMap.put("sisu", prevSisu);
	        				facList.add(tempMap);
	        				tempMap = new EgovMap();

	        				facId = " " + map.get("facId").toString();
	        				facNm = " " + map.get("facNm").toString();

	        			}

	        			//마지막 부분 담기
	        			if(i == (tempFacList.size() - 1)){
	        				sisu = map.get("sisu").toString();
	        				tempMap.put("facId", facId);
	        				tempMap.put("facNm", facNm);
	        				tempMap.put("sisu", sisu);
	        				facList.add(tempMap);
	        			}

	        			prevPosition = map.get("position").toString();
	        			prevSisu = map.get("sisu").toString();
	        			facIdList.add(map.get("facId").toString());
	        		}
	        	}
	        	model.addAttribute("facList", facList);
	        	model.addAttribute("facIdList", facIdList);

	        	//등록 된 시간표 담당교원
	        	ScheduleMngVO scheduleMngVO = new ScheduleMngVO();
	        	scheduleMngVO.setCrclId(searchVO.getCrclId());
	        	scheduleMngVO.setCpr("Y");//교원 비교하기 위해서 정렬
	        	List tempFacPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
	        	List<EgovMap> facPlList = new ArrayList<EgovMap>();
	        	if(tempFacPlList != null && tempFacPlList.size() > 0){
	        		String prevPlId = "";
	        		String facId = "";
	        		String sisu = "";
	        		String prevSisu = "";
	        		EgovMap tempMap = new EgovMap();
	        		for(int i = 0; i < tempFacPlList.size(); i++){
	        			EgovMap map = (EgovMap) tempFacPlList.get(i);
	        			if(prevPlId.equals(map.get("plId").toString()) || i == 0){
	        				facId += " " + map.get("facId").toString();
	        			}else{
	        				tempMap.put("facId", facId);
	        				tempMap.put("sisu", prevSisu);
	        				facPlList.add(tempMap);
	        				tempMap = new EgovMap();

	        				facId = " " + map.get("facId").toString();
	        			}

	        			//마지막 부분 담기
	        			if(i == (tempFacPlList.size() - 1)){
	        				sisu = map.get("sisu").toString();
	        				tempMap.put("facId", facId);
	        				tempMap.put("sisu", sisu);
	        				facPlList.add(tempMap);
	        			}

	        			prevPlId = map.get("plId").toString();
	        			prevSisu = map.get("sisu").toString();
	        		}
	        	}
	        	model.addAttribute("facPlList", facPlList);
			}

        	//과제타입코드
        	voComCode.setCodeId("LMS80");
        	List<CmmnDetailCode> homeworkComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("homeworkComCode", homeworkComCode);

        	//수료기준
    		ctgry.setCtgrymasterId("CTGMST_0000000000010");
    		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        	//교재 및 부교재
        	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
    	}

      	request.getSession().setAttribute("sessionVO", searchVO);

        return "/lms/crcl/CurriculumView";
    }

    //과정관리 > 학생 > 전체명단
  	@RequestMapping(value = "/lms/crcl/curriculumStudent.do")
  	public String selectCurriculumStudent(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

  		//수강대상자 총 합계
  		searchVO.setSearchSttus("1");
  		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);

      	//수강 대상자 조회
  		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
  		model.addAttribute("selectStudentList", selectStudentList);

  		//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
      	
      	request.getSession().setAttribute("sessionVO", searchVO);
      	
      	String retUrl = "/lms/crcl/curriculumStudentView";
      	if("Y".equals(searchVO.getExcelAt())){
      		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
      	  	model.addAttribute("siteInfo", siteVO);
      	  	
      		retUrl = "/lms/crcl/curriculumStudentViewExcel";
      	}
      	
		return retUrl;
  	}

  	//과정관리 > 학생 > 조배정 뷰 페이지
  	@RequestMapping(value = "/lms/crcl/curriculumGroupView.do")
  	public String selectCurriculumGroupView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//로그인 회원정보
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("USER_INFO", user);

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	//수강 그룹 조회(반,조)
  		List<?> selectGroupList = curriculumMemberService.selectGroupList(searchVO);
  		model.addAttribute("selectGroupList", selectGroupList);

  		//선택된 수강 학생 조회
  		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
  		model.addAttribute("pickStudentList", selectPickStudentList);

  		//조의 개수 리스트를 조회
  		List<?>  groupCntList = curriculumMemberService.selectCurriculumGroupCntList(searchVO);
  		model.addAttribute("groupCntList", groupCntList);

  		//수강 대상자 조회
		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
		model.addAttribute("selectStudentList", selectStudentList);

        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crcl/curriculumGroupView";
  	}

  	//과정관리 > 학생 > 반배정 뷰 페이지
  	@RequestMapping(value = "/lms/crcl/curriculumClassView.do")
  	public String selectCurriculumClassView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//로그인 회원정보
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("USER_INFO", user);

  		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(searchVO));

    	//반 정보 조회
		List<?> selectCurriculumClassList = curriculumMemberService.selectCurriculumClassList(searchVO);
		model.addAttribute("curriculumClassList", selectCurriculumClassList);

    	//반 개수 조회
		int classCnt = curriculumMemberService.selectCurriculumClassTotCnt(searchVO);
		model.addAttribute("classCnt", classCnt);

		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);

      	request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crcl/curriculumClassView";
  	}

    @RequestMapping("/lms/crcl/updateCurriculumView.do")
    public String updateCurriculumView(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user == null){
    		return "redirect:/index.do";
    	}else{
    		//과정체계
    		Ctgry ctgry = new Ctgry();
    		ctgry.setCtgrymasterId("CTGMST_0000000000004");
    		model.addAttribute("sysCodeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//학기
    		ctgry.setCtgrymasterId("CTGMST_0000000000016");
    		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//언어
    		ctgry.setCtgrymasterId("CTGMST_0000000000002");
    		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//캠퍼스
    		ctgry.setCtgrymasterId("CTGMST_0000000000017");
    		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//주관기관
    		ctgry.setCtgrymasterId("CTGMST_0000000000009");
    		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//사업비
    		ctgry.setCtgrymasterId("CTGMST_0000000000018");
    		model.addAttribute("feeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

    		//과정성과 - 공통코드(LMS10)
    		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
    	   	voComCode = new ComDefaultCodeVO();
        	voComCode.setCodeId("LMS10");
        	List<CmmnDetailCode> listComCode = cmmUseService.selectCmmCodeDetail(voComCode);
        	model.addAttribute("listComCode", listComCode);

        	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
        	model.addAttribute("curriculumVO", curriculumVO);
        	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));

        	//년도
        	model.addAttribute("yearList", new LmsCommonUtil().getYear());

        	//과정만족도
    		SurveyVO surveyVo = new SurveyVO();
    		surveyVo.setSchdulClCode("TYPE_1");
    		surveyVo.setRecordCountPerPage(1000);
    		surveyVo.setFirstIndex(0);
    		EgovMap resultMap = surveyManageService.selectSurvey(surveyVo);
    		model.addAttribute("surveyList", resultMap.get("resultList"));
    		surveyVo.setSchdulClCode("TYPE_2");
    		EgovMap resultMap2 = surveyManageService.selectSurvey(surveyVo);
    		model.addAttribute("surveyList2", resultMap2.get("resultList"));
    		surveyVo.setSchdulClCode("TYPE_3");
     		EgovMap resultMap3 = surveyManageService.selectSurvey(surveyVo);
     		model.addAttribute("surveyList3", resultMap3.get("resultList"));

        	//과정확정 이후 수정 시
        	if(Integer.parseInt(curriculumVO.getProcessSttusCode()) > 0){
        		//과정진행상태 - 공통코드(LMS30)
            	voComCode.setCodeId("LMS30");
            	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
            	model.addAttribute("statusComCode", statusComCode);

            	//오늘일자
            	model.addAttribute("today", EgovDateUtil.getToday());

            	//학습내용
            	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));

            	//부책임교원
            	CurriculumVO curriculum = new CurriculumVO();
            	curriculum.setCrclId(searchVO.getCrclId());
            	//부책임교원 코드 08
            	curriculum.setManageCode("08");
            	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

            	//성적기준
        		ctgry.setCtgrymasterId("CTGMST_0000000000011");
        		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        		//총괄평가
        		List evaluationList = curriculumService.selectTotalEvaluation(curriculum);
        		model.addAttribute("evaluationList", evaluationList);
        		//총괄평가 기본 목록
        		if(evaluationList == null || evaluationList.size() == 0){
        			ctgry.setCtgrymasterId("CTGMST_0000000000012");
            		model.addAttribute("evaluationBaseList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        		}else{
        			ctgry.setCtgrymasterId("CTGMST_0000000000012");
            		model.addAttribute("evaluationBaseList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

        			model.addAttribute("evaluationList", evaluationList);

        			//수업참여도 게시판
        			List attendBbsList = curriculumService.selectAttendbbs(curriculum);
        			model.addAttribute("attendBbsList", attendBbsList);
        		}

        		//교원배정
            	model.addAttribute("facList", curriculumService.selectCurriculumFaculty(searchVO));

            	//과제타입코드
            	voComCode.setCodeId("LMS80");
            	List<CmmnDetailCode> homeworkComCode = cmmUseService.selectCmmCodeDetail(voComCode);
            	model.addAttribute("homeworkComCode", homeworkComCode);

            	//수료기준
        		ctgry.setCtgrymasterId("CTGMST_0000000000010");
        		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

            	//교재 및 부교재
            	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
        	}

        	model.addAttribute("USER_INFO", user);
            request.getSession().setAttribute("sessionVO", searchVO);

            return "/lms/crcl/CurriculumRegister";
    	}

    }

    @RequestMapping("/lms/crcl/updateCurriculum.do")
    public String updateCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String retStr = "redirect:/lms/crcl/CurriculumList.do?mngAt=Y&searchMyCulAt=Y&menuId=" + searchVO.getMenuId();
    	if("Y".equals(searchVO.getRetViewAt())){
    		retStr = "redirect:/lms/crcl/selectCurriculum.do?menuId=" + searchVO.getMenuId() + "&crclId=" + searchVO.getCrclId() + "&step=3";
    	}else if("MNU_0000000000000059".equals(searchVO.getMenuId())){
    		retStr = "redirect:/lms/crcl/CurriculumList.do?mngAt=Y&searchMyCulAt=Y&menuId=" + searchVO.getMenuId();
    	}else{
    		retStr = "redirect:/lms/crcl/CurriculumList.do?myCurriculumPageFlag=teacher&menuId=" + searchVO.getMenuId();
    	}

    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}

        curriculumService.updateCurriculum(searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

    @RequestMapping("/lms/crcl/updateCurriculumPart.do")
    public void updateCurriculumPart(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	//String retStr = "forward:/mng/lms/crcl/CurriculumList.do";

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}

        String successYn = "Y";

		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(Integer.MAX_VALUE);

		curriculumService.updateCurriculumPart(searchVO, request, response);

        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    @RequestMapping("/lms/crcl/updatePsCodeCurriculum.do")
    public String updatePsCodeCurriculum(@ModelAttribute("searchVO") CurriculumVO searchVO, Model model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String retStr = "forward:/lms/crcl/CurriculumList.do";
    	/*
    	if(request.getSession().getAttribute("sessionVO") == null) {
			return retStr;
		}
    	 */
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}

        curriculumService.updatePsCodeCurriculum(searchVO);

        //request.getSession().removeAttribute("sessionVO");
        if("D".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "승인취소 되었습니다.");
        }else if(Integer.parseInt(searchVO.getProcessSttusCode()) > 0){
        	model.addAttribute("message", "과정상태가 변경 되었습니다.");
        	retStr = "redirect:/lms/manage/lmsControl.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId()+"&step=1";
        }else if("0".equals(searchVO.getProcessSttusCode())){
        	model.addAttribute("message", "확정취소 되었습니다.");
        }else if("Y".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "승인 되었습니다.");
        	retStr = "redirect:/lms/crcl/selectCurriculum.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId()+"&step=3";
        }else if("N".equals(searchVO.getAprvalAt())){
        	model.addAttribute("message", "반려 되었습니다.");
        	retStr = "redirect:/lms/crcl/selectCurriculum.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId()+"&step=3";
        }else if(!EgovStringUtil.isEmpty(searchVO.getComnt())){
        	retStr = "redirect:/lms/crcl/selectCurriculum.do?menuId=MNU_0000000000000062&crclId="+searchVO.getCrclId()+"&step=3";
        }

        return retStr;
    }

    @RequestMapping("/lms/crcl/curriculumBoardList.do")
    public String selectCurriculum(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO,Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

    	//bbsid체크
    	if(EgovStringUtil.isEmpty(lmsMngVO.getBbsId())){
    		master = result.get(0);
    	}else{
    		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
    		boardMasterVO.setBbsId(lmsMngVO.getBbsId());

      	  	master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
    	}

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		//boardVO.setFrstRegisterId(user.getId());
    		model.addAttribute("sessionUniqId", user.getId());
    	}
    	//과정 반 조회

    	if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selectClassList", selectClassList);
    	}else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selectGroupList", selectGroupList);
    	}

        PaginationInfo paginationInfo = new PaginationInfo();
  	  	if(master != null) {

  		  // 페이징 정보 설정
  		  boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
  		  boardVO.setPageSize(propertiesService.getInt("pageSize"));
  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  boardVO.setSysTyCode(master.getSysTyCode());
  		  /*
  		  //공지게시물 가져오기
  		  BoardVO noticeVO = new BoardVO();
  		  noticeVO.setBbsId(lmsMngVO.getBbsId());
  		  noticeVO.setTmplatId(master.getTmplatId());
  		  noticeVO.setCommentUseAt(master.getCommentUseAt());
  		  noticeVO.setSearchNoticeAt("Y");
  		  noticeVO.setFirstIndex(0);
  		  noticeVO.setRecordCountPerPage(9999);

  		  model.addAttribute("noticeList", bbsMngService.selectBoardArticles(noticeVO));
  		  */
  		  paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
  		  paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
  		  paginationInfo.setPageSize(boardVO.getPageSize());

  		  boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
  		  boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
  		  boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

  		  boardVO.setBbsId(master.getBbsId());
  		  boardVO.setCommentUseAt(master.getCommentUseAt());
  		  boardVO.setTmplatId(master.getTmplatId());
  		  boardVO.setBbsAttrbCode(master.getBbsAttrbCode());

  		  List<BoardVO> resultList = bbsMngService.selectBoardArticles(boardVO);
  		  int totCnt = bbsMngService.selectBoardArticlesCnt(boardVO);

  		  paginationInfo.setTotalRecordCount(totCnt);

  		  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
  			  Ctgry ctgry = new Ctgry();
  			  ctgry.setCtgrymasterId(master.getCtgrymasterId());
  			  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
  		  }

  		  model.addAttribute("resultList", resultList);
  		  model.addAttribute("resultCnt", totCnt);
  		  model.addAttribute("brdMstrVO", master);
        }

  	  	model.addAttribute("paginationInfo", paginationInfo);

  	  	//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		curriculumVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map1 = (EgovMap) subUserList.get(i);
          		String userId = map1.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

      	request.getSession().setAttribute("sessionVO", boardVO);

        return "/lms/crcl/CurriculumBoardListStaff";
    }

    //과정게시판 등록
    @RequestMapping(value="/lms/crcl/curriculumBoardAdd.do")
    public String lmsBbsAdd(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

    	BoardMasterVO vo = new BoardMasterVO();
    	vo.setSiteId(lmsMngVO.getCrclId());
	  	vo.setSysTyCode(boardVO.getSysTyCode());
	  	vo.setBbsId(boardVO.getBbsId());
	  	vo.setTrgetId(boardVO.getTrgetId());

	  	BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

	  	if(master != null) {
	  	  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
	  		  Ctgry ctgry = new Ctgry();
	  		  ctgry.setCtgrymasterId(master.getCtgrymasterId());
	  		  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
	  		  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
	  	    }

	  	  model.addAttribute("brdMstrVO", master);

	  	if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selecteResultList", selectClassList);
    	}else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selecteResultList", selectGroupList);
    	}

	  	  Board board = new Board();
	  	  model.addAttribute("board", board);

	  	  request.getSession().setAttribute("sessionVO", boardVO);
	  	}

        return "/lms/crcl/CurriculumBoardAddStaff";
    }

    //과정게시판 등록 프로세스
    @RequestMapping("/lms/crcl/insertBoardStaffArticle.do")
    public String insertBoardStaffArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO,
  		  Board board, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

  	  if(request.getSession().getAttribute("sessionVO") == null) {
  		  return "forward:/lms/crcl/curriculumBoardList.do";
  	  }

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  String atchFileId = "";
  		  if(EgovStringUtil.isEmpty(boardVO.getSiteId())){
  			  boardVO.setSiteId(master.getSiteId());
  		  }

  	      List<FileVO> result = null;

  	      final Map<String, MultipartFile> files = multiRequest.getFileMap();
  	      if(!files.isEmpty()) {
  	        result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, "", boardVO.getSiteId(), boardVO.getBbsId(),"","");
  	        atchFileId = fileMngService.insertFileInfs(result);
  	      }


  	      boardVO.setAtchFileId(atchFileId);
  	      boardVO.setFrstRegisterId(user.getId());
  	      boardVO.setNtcrNm(user.getName());
  	      boardVO.setCreatIp(EgovClntInfo.getClntIP(request));
  	      boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));

  	      bbsMngService.insertBoardArticle(boardVO, master);

  	      request.getSession().removeAttribute("sessionVO");
  	  }

  	  String tempMenuId = request.getParameter("menuId").toString();
   	  return "redirect:/lms/crcl/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+boardVO.getBbsId();
    }

    @RequestMapping("/lms/crcl/curriculumBoardView.do")
    public String lmsBbsView(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

	  	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	  	if(user != null){
    		boardVO.setFrstRegisterId(user.getId());
    		model.addAttribute("sessionUniqId", user.getId());
    	}

	  	BoardMasterVO vo = new BoardMasterVO();
	  	vo.setSiteId(lmsMngVO.getCrclId());
	  	//vo.setSysTyCode(boardVO.getSysTyCode());
	  	vo.setBbsId(boardVO.getBbsId());
	  	vo.setTrgetId(boardVO.getTrgetId());

	  	BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

	  	if(master != null) {

  		  model.addAttribute("brdMstrVO", master);

  		  // 조회수 증가 여부 지정
  		  boardVO.setPlusCount(false);
  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

  	  }

      	request.getSession().setAttribute("sessionVO", boardVO);

        return "/lms/crcl/CurriculumBoardViewStaff";
    }

    //게시판 게시글 삭제
    @RequestMapping("/lms/crcl/deleteBoardStaffArticle.do")
    public String deleteBoardStaffArticle(@ModelAttribute("searchVO") BoardVO boardVO, BoardVO board, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  board.setLastUpdusrId(user.getId());
  		  bbsMngService.deleteBoardArticle(board, master);
  	  }

  	 String tempMenuId = request.getParameter("menuId").toString();
  	  return "redirect:/lms/crcl/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+boardVO.getBbsId();
    }

    @RequestMapping("/lms/crcl/curriculumBoardStatistics.do")
    public String curriculumBoardStatistics(@ModelAttribute("searchVO") BoardVO boardVO,  LmsMngVO lmsMngVO,Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	int totalBbsCnt = 0;

    	if(boardVO.getSearchType() == ""){
    		boardVO.setSearchType("student");
    	}
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(boardVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();

		boardMasterVO.setSiteId(boardVO.getCrclId());
		boardMasterVO.setBbsId(boardVO.getBbsId());
		master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
		model.addAttribute("masterVo", master);

		//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

		boardVO.setCntType(master.getSysTyCode().toLowerCase());

    	//게시판 구분별 수
    	List<EgovMap> pieList = this.bbsMngService.selectBoardStatisticsPie(boardVO);
    	model.addAttribute("pieList", pieList);

    	if(pieList != null){
    		List<EgovMap> barList = new ArrayList<EgovMap>();
    		for(EgovMap tempMap : pieList){
    			boardVO.setCtgryId(tempMap.get("ctgryId").toString());
    			if("0".equals(tempMap.get("ctgryLevel").toString())){
                    boardVO.setCtgryId(null);
                 }
    			EgovMap barMap = this.bbsMngService.selectBoardStatisticsBar(boardVO);
    			barList.add(barMap);
    			totalBbsCnt += Integer.parseInt(tempMap.get("cnt").toString());
    		}
    		model.addAttribute("barList", barList);
    		model.addAttribute("barMemberList", this.bbsMngService.selectBoardStatisticsBarMemberList(boardVO));
    		model.addAttribute("totalBbsCnt", totalBbsCnt);
    	}


    	PaginationInfo paginationInfo = new PaginationInfo();

  		// 페이징 정보 설정
    	if(boardVO.getSortType() == ""){
    		boardVO.setSortType("sum");
    	}

    	boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	boardVO.setPageSize(propertiesService.getInt("pageSize"));
    	paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
	  	paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
	  	paginationInfo.setPageSize(boardVO.getPageSize());

	  	boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
	  	boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
	  	boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

	  	EgovMap resultMap = this.bbsMngService.selectStatistics(boardVO);
	  	int totCnt = Integer.parseInt(resultMap.get("resultCnt").toString());
	  	paginationInfo.setTotalRecordCount(totCnt);

	  	EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
	  	if(tempCollect != null){
	  		model.addAttribute("attendCollect", tempCollect.get("colect"));
	  	}

	  	model.addAttribute("resultList", resultMap.get("resultList"));
	  	model.addAttribute("resultCnt", totCnt);
  	  	model.addAttribute("paginationInfo", paginationInfo);

      	request.getSession().setAttribute("sessionVO", boardVO);

        return "/lms/crcl/CurriculumBoardStatistcsStaff";
    }

    //게시판 게시물 수정
    @RequestMapping("/lms/crcl/forUpdateBoardStaffArticle.do")
    public String forUpdateBoardStaffArticle(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);


  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(boardVO.getCrclId());
  	  //vo.setSysTyCode(boardVO.getSysTyCode());
  	  vo.setBbsId(boardVO.getBbsId());
  	  vo.setTrgetId(boardVO.getTrgetId());
  	  boardVO.setAdminAt("Y");

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
  			  Ctgry ctgry = new Ctgry();
  			  ctgry.setCtgrymasterId(master.getCtgrymasterId());
  			  model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			  model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
  	      }

  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);

  		  model.addAttribute("brdMstrVO", master);
  		  model.addAttribute("board", dataVO);

  		  request.getSession().setAttribute("sessionVO", boardVO);

  		  if("CLASS".equals(master.getSysTyCode())){
	    	List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
			model.addAttribute("selecteResultList", selectClassList);
  		  }else if("GROUP".equals(master.getSysTyCode())){
	    	//과정 조 조회
			List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
			model.addAttribute("selecteResultList", selectGroupList);
  		  }
  	  }

  	   return "/lms/crcl/CurriculumBoardAddStaff";
    }

    //게시판 게시 글 수정프로세스
    @RequestMapping("/lms/crcl/updateBoardStaffArticle.do")
    public String updateBoardStaffArticle(final MultipartHttpServletRequest multiRequest,
  		  BoardVO board, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  	  LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  	  String atchFileId = board.getAtchFileId();

  	  BoardMasterVO vo = new BoardMasterVO();
  	  vo.setSiteId(board.getCrclId());
  	  //vo.setSysTyCode(board.getSysTyCode());
  	  vo.setBbsId(board.getBbsId());
  	  vo.setTrgetId(board.getTrgetId());

  	  BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

  	  if(master != null) {
  		  if(EgovStringUtil.isEmpty(board.getSiteId())){
  			  board.setSiteId(master.getSiteId());
  		  }
  		  final Map<String, MultipartFile> files = multiRequest.getFileMap();
  		  if(!files.isEmpty()) {
  			  if(EgovStringUtil.isEmpty(atchFileId)) {
  				  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, atchFileId, board.getSiteId(), board.getBbsId(),"","");
  				  atchFileId = fileMngService.insertFileInfs(result);
  				  board.setAtchFileId(atchFileId);
  			  } else {
  				  FileVO fvo = new FileVO();
  				  fvo.setAtchFileId(atchFileId);
  				  int cnt = fileMngService.getMaxFileSN(fvo);
  				  List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, cnt, atchFileId, board.getSiteId(), board.getBbsId(),"","");
  				  fileMngService.updateFileInfs(_result);
  			  }
  		  }

  		  board.setAdminAt("Y");
  		  board.setLastUpdusrId(user.getId());
  		  board.setEstnData(EgovHttpUtil.getEstnParseData(request));

  		  bbsMngService.updateBoardArticle(board, master, false);

  		  request.getSession().removeAttribute("sessionVO");
      }

  	 String tempMenuId = request.getParameter("menuId").toString();
   	  return "redirect:/lms/crcl/curriculumBoardList.do?crclId="+board.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+board.getBbsId();
    }

    @RequestMapping(value="/lms/crcl/selectLmsBbsList.json")
    public void selectLmsBbsList(@ModelAttribute("searchVO") BoardVO boardVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	JSONObject jo = new JSONObject();
    	response.setContentType("application/json;charset=utf-8");

    	BoardMasterVO master = new BoardMasterVO();
    	BoardMasterVO boardMasterVO = new BoardMasterVO();
		boardMasterVO.setSiteId(boardVO.getCrclId());
		boardMasterVO.setBbsId(boardVO.getBbsId());
		master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
		jo.put("masterVo", master);

		boardVO.setCntType(master.getSysTyCode().toLowerCase());

		List<EgovMap> resultList = this.bbsMngService.selectStaffMemBoardList(boardVO);

		EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
	  	if(tempCollect != null){
	  		jo.put("attendCollect", tempCollect.get("colect"));
	  	}
		jo.put("resultList", resultList);

		PrintWriter printwriter = response.getWriter();
    	printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }

    //과정관리 > 과제 > 과제평가
  	@RequestMapping(value = "/lms/crcl/homeworkTestList.do")
  	public String selectHomeworkTestList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

      	//수강 그룹 조회(반,조)
  		List selectGroupList = curriculumMemberService.selectGroupList(searchVO);
  		model.addAttribute("selectGroupList", selectGroupList);

      	//과제 목록
      	searchVO.setHomeworkScoreAt("Y");
      	searchVO.setPagingFlag("N");
      	List homeworkList = curriculumService.selectHomeworkList(searchVO);
      	model.addAttribute("homeworkList", homeworkList);

      	//수강 대상자 조회
      	if(searchVO.getSearchClassCnt() == null || searchVO.getSearchClassCnt() == 0){
      		searchVO.setSearchClassCnt(0);
      	}
  		List selectStudentList = curriculumService.homeworkScoreSum(searchVO);
  		model.addAttribute("selectStudentList", selectStudentList);

  		//학생 점수 조회
  		List homeworkScoreList = curriculumService.homeworkScoreList(searchVO);
  		model.addAttribute("scoreList", homeworkScoreList);

        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crcl/HomeworkTestList";
  	}

  	//과정관리 > 과제 > 과제평가 성적반영
  	@RequestMapping(value = "/lms/crcl/homeworkTestUpdate.do")
  	public String homeworkTestUpdate(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  		//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}

      	if("Y".equals(managerAt)){
      		curriculumService.updateScoreApply(searchVO);
      	}else{
      		model.addAttribute("message", "해당 과정에 권한이 없습니다.");
      	}

  		return "forward:/lms/crcl/homeworkTestList.do";
  	}

    //과정관리 > 과제 > 과제
  	@RequestMapping(value = "/lms/crcl/homeworkList.do")
  	public String selectHomeworkList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      	//과제 조회
  		List<?> selectHomeworkList = curriculumService.selectHomeworkList(searchVO);
  		model.addAttribute("selectHomeworkList", selectHomeworkList);

  		//과제 총 합계
      	int totCnt = curriculumService.selectHomeworkTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);

  		//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

  		return "/lms/crcl/HomeworkList";
  	}

  	//과제 제출, 평가 대기 인원 리스트
  	@RequestMapping(value = "/lms/crcl/homeworkWaitingMemberListAjax.do")
  	public String curriculumAllListAjax(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		if("1".equals(searchVO.getHwWaitingType())) {
  			List<?> selectHomeworkSubmitWaitingList = curriculumService.selectHomeworkSubmitWaitingList(searchVO);
  	  		model.addAttribute("selectHomeworkSubmitWaitingList", selectHomeworkSubmitWaitingList);
  		} else if("2".equals(searchVO.getHwWaitingType())) {
  			List<?> selectHomeworkTestWaitingList = curriculumService.selectHomeworkTestWaitingList(searchVO);
  	  		model.addAttribute("selectHomeworkTestWaitingList", selectHomeworkTestWaitingList);
  		}

  		model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

  		return "/lms/crcl/HomeworkWaitingMemberListAjax";
  	}

  	//과정관리 > 과제 > 과제  > 과제 등록
  	@RequestMapping(value = "/lms/crcl/homeworkRegister.do")
  	public String selectCurriculumRegister(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	if(!EgovStringUtil.isEmpty(searchVO.getHwId())) {
      		CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
          	model.addAttribute("homeworkVO", homeworkVO);
      	}

      	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
      	model.addAttribute("curriculum", curriculum);
      	model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));

      	//조별과제 분기를 위한 반 개수 조회
      	model.addAttribute("groupCnt", curriculumService.selectCurriculumMemberGroupCnt(searchVO));

      	//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

        return "/lms/crcl/HomeworkRegister";
  	}

  	//과제 등록
  	@RequestMapping("/lms/crcl/insertHomeworkArticle.do")
    public String insertHomeworkArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(EgovStringUtil.isEmpty(request.getParameter("plId"))) {
  			forwardUrl = "forward:/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/lms/manage/homeworkList.do?tabType=T";
  		}

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		searchVO.setFrstRegisterId(user.getId());
		searchVO.setNtcrNm(user.getName());

		String atchFileId = "";

	    List<FileVO> result = null;

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	        result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, "",
	        		siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
	        atchFileId = fileMngService.insertFileInfs(result);
	    }

	    searchVO.setAtchFileId(atchFileId);
	    searchVO.setFrstRegisterId(user.getId());
	    searchVO.setNtcrNm(user.getName());
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지
	    searchVO.setOpenTime(searchVO.getOpenTime().replace(":", ""));
	    searchVO.setCloseTime(searchVO.getCloseTime().replace(":", ""));

	    curriculumService.insertHomeworkArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//과제 상세
  	@RequestMapping("/lms/crcl/selectHomeworkArticle.do")
    public String selectHomeworkArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

  		//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

      //등록 된 시간표 담당교원
  		ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

      	scheduleMngVO.setCrclId(searchVO.getCrclId());
      	scheduleMngVO.setPlId(searchVO.getPlId());

    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
    	model.addAttribute("facPlList", facPlList);

        //수업 담당자 인지 확인
        String studyMngAt = "N";
        if(user != null){
           for(int i = 0; i < facPlList.size(); i ++){
               EgovMap map = (EgovMap) facPlList.get(i);
               String userId = map.get("facId").toString();
               if(user.getId().equals(userId)){
                  studyMngAt = "Y";
               }
            }
        }
        model.addAttribute("studyMngAt", studyMngAt);

      	CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
      	model.addAttribute("homeworkVO", homeworkVO);

  		if("homeworksubmit".equals(request.getParameter("viewFlag"))) {
  			//과제 제출 상세 조회
  			CurriculumVO homeworkSubmitVO = curriculumService.selectHomeworkSubmitArticle(searchVO);
  	      	model.addAttribute("homeworkSubmitVO", homeworkSubmitVO);

  	      	//조원 조회
  	  		searchVO.setClassCnt(homeworkSubmitVO.getClassCnt());
  	  	    searchVO.setGroupCnt(homeworkSubmitVO.getGroupCnt());
  	  		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
  	  		model.addAttribute("pickStudentList", selectPickStudentList);
  		} else {
  			//과제 제출 목록 조회
  	      	searchVO.setHwType(homeworkVO.getHwType());
  	  		List<?> selectHomeworkSubjectList = curriculumService.selectHomeworkSubjectList(searchVO);
  	  		model.addAttribute("selectHomeworkSubjectList", selectHomeworkSubjectList);
  		}

  		//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	//과제 제출 이미지 경로
      	model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

	    return "/lms/crcl/HomeworkArticle";
    }

  	//과제 삭제
  	@RequestMapping("/lms/crcl/deleteHomeworkArticle.do")
    public String deleteHomeworkArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(EgovStringUtil.isEmpty(request.getParameter("plId"))) {
  			forwardUrl = "forward:/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/lms/manage/homeworkList.do?tabType=T";
  		}

  		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

  		curriculumService.deleteHomeworkArticle(searchVO);

  		request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//과제 수정
  	@RequestMapping("/lms/crcl/updateHomeworkArticle.do")
    public String updateHomeworkArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "";

  		//forward url 과정과제, 수업과제 분기처리
  		if(EgovStringUtil.isEmpty(request.getParameter("plId"))) {
  			forwardUrl = "forward:/lms/crcl/homeworkList.do";
  		} else {
  			forwardUrl = "forward:/lms/manage/homeworkList.do?tabType=T";
  		}

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		String atchFileId = searchVO.getAtchFileId();

	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
		    if(EgovStringUtil.isEmpty(atchFileId)) {
			  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
			    atchFileId = fileMngService.insertFileInfs(result);
			    searchVO.setAtchFileId(atchFileId);
		    } else {
			    FileVO fvo = new FileVO();
			    fvo.setAtchFileId(atchFileId);
			    //int cnt = fileMngService.getMaxFileSN(fvo);
			    List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getBbsId(), "", "");
			    fileMngService.updateFileInfs(_result);
		    }
	    }

	    searchVO.setLastUpdusrId(user.getId());
	    searchVO.setNttCn(unscript(searchVO.getNttCn())); // XSS 방지
	    searchVO.setOpenTime(searchVO.getOpenTime().replace(":", ""));
	    searchVO.setCloseTime(searchVO.getCloseTime().replace(":", ""));

	    curriculumService.updateHomeworkArticle(searchVO);

	    request.getSession().removeAttribute("sessionVO");

	    return forwardUrl;
    }

  	//학생 공개 업데이트
  	@RequestMapping("/lms/crcl/updateStuOpenAt.do")
    public String updateStuOpenAt(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}

  		curriculumService.updateStuOpenAt(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  	//후기 선정 업데이트
  	@RequestMapping("/lms/crcl/updateCommentPickAt.do")
    public String updateCommentPickAt(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}

  		curriculumService.updateCommentPickAt(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  	//점수 및 피드백 처리
  	@RequestMapping("/lms/crcl/updateFdb.do")
    public String updateFdb(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl");

      	if(request.getSession().getAttribute("sessionVO") == null) {
      		return retStr;
  		}

  		curriculumService.updateFdb(searchVO);

  		model.addAttribute("searchVO", searchVO);

        request.getSession().removeAttribute("sessionVO");

        return retStr;
    }

  //과정 설문
    @RequestMapping(value="/lms/crcl/curriculumSurveyList.do")
    public String curriculumSurveyList(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(surveyVo.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

      	CurriculumVO searchVO = curriculumVO;
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

    	//설문리스트
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(surveyVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(surveyVo.getPageUnit());
		paginationInfo.setPageSize(surveyVo.getPageSize());

		surveyVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
		surveyVo.setLastIndex(paginationInfo.getLastRecordIndex());
		surveyVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		EgovMap resultMap = this.surveyManageService.selectCurriculumSurvey(surveyVo);
		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
		model.addAttribute("surveyCurriculumList", resultMap.get("resultList"));
		model.addAttribute("surveyCurriculumListCnt", resultMap.get("resultCnt"));
        model.addAttribute("paginationInfo", paginationInfo);


        return "/lms/crcl/curriculumSurveyList";
    }

    //과정 설문 (미)제출자 확인 - 기본 미제출자 조회
  	@RequestMapping(value="/lms/crcl/curriculumSurveyMember.json")
  	public void curriculumSurveyMember(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		
  		List<EgovMap> memberList = new ArrayList<EgovMap>();
  		EgovMap surveyMemberSummary = new EgovMap();
  		if("TYPE_3".equals(surveyVo.getSchdulClCode())){
  			memberList = this.surveyManageService.selectSurveyProfessor(surveyVo);
  	  		surveyMemberSummary = this.surveyManageService.selectSurveyProfessorSummary(surveyVo);
  		}else {
  			memberList = this.surveyManageService.selectSurveyMember(surveyVo);
  	  		surveyMemberSummary = this.surveyManageService.selectSurveyMemberSummary(surveyVo);
  		}
  		
	    JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
		jo.put("memberList", memberList);
		jo.put("surveyMemberSummary", surveyMemberSummary);
		jo.put("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
  	}

  //과정 설문 상세
    @RequestMapping(value="/lms/crcl/curriculumSurveyView.do")
    public String curriculumSurveyView(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(surveyVo.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	CurriculumVO searchVO = curriculumVO;
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
    	//과정 추가 정보
    	model.addAttribute("curriculumAddInfo", this.surveyManageService.selectCurriculumAddInfo(surveyVo));
    	//과정 설문
    	model.addAttribute("surveyAnswer", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVo));


        return "/lms/crcl/curriculumSurveyView";
    }

    //커뮤니티 과정후기 
  	@RequestMapping("/lms/crcl/selectHomeworkCommentPickAtList.do")
    public String selectHomeworkCommentPickAtList(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("user", user);
  		
  		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//과정후기 리스트
  		List<?> selectCourseReviewList = curriculumService.selectCourseReviewList(searchVO,request,response);
	  	model.addAttribute("selectCourseReviewList", selectCourseReviewList);

	  	//과정후기 총 합계
      	int totCnt = curriculumService.selectCourseReviewCnt(searchVO,request,response);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);
  		
  		// 공지사항
  		List<?> selectCourseReviewNoticeList = curriculumService.selectCourseReviewNoticeList(searchVO,request,response);
  		model.addAttribute("noticeList", selectCourseReviewNoticeList);

      	//과제 제출 이미지 경로
      	model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

	    return "/lms/crcl/HomeworkCommentPickAtList";
    }
  	
/*  	//커뮤니티 과정후기
  	@RequestMapping("/lms/crcl/selectHomeworkCommentPickAtList.do")
  	public String selectHomeworkCommentPickAtList(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		*//** EgovPropertyService.sample *//*
  		searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
  		searchVO.setPageSize(propertiesService.getInt("pageSize"));
  		
  		*//** pageing *//*
  		PaginationInfo paginationInfo = new PaginationInfo();
  		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
  		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
  		paginationInfo.setPageSize(searchVO.getPageSize());
  		
  		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
  		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
  		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
  		
  		//제출 과제 리스트
  		List<?> selectHomeworkCommentPickAtList = curriculumService.selectHomeworkCommentPickAtList(searchVO);
  		model.addAttribute("selectHomeworkCommentPickAtList", selectHomeworkCommentPickAtList);
  		
  		//과제 제출 총 합계
  		int totCnt = curriculumService.selectHomeworkCommentPickAtCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		paginationInfo.setTotalRecordCount(totCnt);
  		model.addAttribute("paginationInfo", paginationInfo);
  		
  		//과제 제출 이미지 경로
  		model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));
  		
  		request.getSession().setAttribute("sessionVO", searchVO);
  		
  		return "/lms/crcl/HomeworkCommentPickAtList";
  	}
*/  	
  	

  	//과정후기 등록화면이동
  	@RequestMapping("/lms/crcl/selectHomeworkCommentPickArticle.do")
  	public String selectHomeworkCommentPickArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		
  		//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
  		
  		//나의 교육과정 개수 조회
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("user", user);
      	
      	int myCurriculumCnt = 0;
      	CurriculumVO myCurriculumVO = new CurriculumVO();

      	//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			myCurriculumVO.setStudentPageAt("Y");
  			myCurriculumVO.setMyCurriculumPageFlag("student");
  			myCurriculumVO.setSttus("1");
  		} else { //교원
  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
  		}
  		
  		//나의 교육과정 개수 조회
  		myCurriculumVO.setSearchProcessSttusCodeDate("7");
      	myCurriculumVO.setUserId(user.getId());
        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
        model.addAttribute("myCurriculumCnt", myCurriculumCnt);
        
      	//캠퍼스
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        
  		searchVO.setSearchSchAt("Y");
  		
  		//리스트 조회
  		searchVO.setUserId(user.getId());
  		searchVO.setUserSeCode(user.getUserSeCode());

  		//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setStudentPageAt("Y");
  			searchVO.setSearchSttus("5");
  			searchVO.setMyCurriculumPageFlag("student");
  		} else if("08".equals(user.getUserSeCode())) {
  			//년도
  			model.addAttribute("yearList", new LmsCommonUtil().getYear());
  			
  			//학기
  	    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
  			model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			
  			searchVO.setMyCurriculumPageFlag("teacher");
  		}

	        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
	        model.addAttribute("resultList", curriculumList);
  		System.out.println(searchVO);
//        if(searchVO.getCrclId() != null && !searchVO.getCrclId().equals("total")) {
//        	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
//        	model.addAttribute("curriculumVO", curriculumVO);
//        }
  		
//  		CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
//  		model.addAttribute("homeworkVO", homeworkVO);
  		
	 	//과제 제출 상세 조회
  		CurriculumVO courseReviewVo = curriculumService.selectCourseReviewAtView(searchVO);
  		model.addAttribute("courseReviewVo", courseReviewVo);
  		
  		//과제 제출 이미지 경로
  		model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));
        
  		request.getSession().setAttribute("sessionVO", searchVO);
  		
  		return "/lms/crcl/HomeworkCommentPickArticle";
  	}
  	
  	
  	//과정후기 등록
  	@RequestMapping("/lms/crcl/insertCourseReview.do")
    public String insertCourseReview(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "forward:/lms/crcl/selectHomeworkCommentPickAtList.do?boardType=list";

		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		searchVO.setFrstRegisterId(user.getId());
		searchVO.setNtcrNm(user.getName());

		String atchFileId = "";

	    List<FileVO> result = null;

	    System.out.println(searchVO);
	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	        result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, "",
	        		siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
	        atchFileId = fileMngService.insertFileInfs(result);
	    }

	    searchVO.setAtchFileId(atchFileId);
	    searchVO.setFrstRegisterId(user.getId());
	    searchVO.setNtcrNm(user.getName());
	    searchVO.setCourseReviewCn(unscript(searchVO.getCourseReviewCn())); // XSS 방지

	    curriculumService.insertCourseReview(searchVO);

	    request.getSession().removeAttribute("sessionVO");
	    //request.getSession().setAttribute("sessionVO", searchVO);

	    return forwardUrl;
    }
  	
  	//과정후기 수정
  	@RequestMapping("/lms/crcl/updateCourseReview.do")
    public String updateCourseReview(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO,
  		  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		String forwardUrl = "forward:/lms/crcl/selectHomeworkCommentPickAtList.do?boardType=list";
  		
		if(request.getSession().getAttribute("sessionVO") == null) {
			return forwardUrl;
		}

		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

		String atchFileId = searchVO.getAtchFileId();
		
	    final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    
	    if(!files.isEmpty()) {
		    if(EgovStringUtil.isEmpty(atchFileId)) {
			  List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getHwId(), "", "");
			    atchFileId = fileMngService.insertFileInfs(result);
			    searchVO.setAtchFileId(atchFileId);
		    } else {
			    FileVO fvo = new FileVO();
			    fvo.setAtchFileId(atchFileId);
			    //int cnt = fileMngService.getMaxFileSN(fvo);
			    List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong("100") * 1024 * 1024, files, 0, atchFileId,
					  siteManageService.selectSiteServiceInfo(request).getSiteId(), searchVO.getBbsId(), "", "");
			    fileMngService.updateFileInfs(_result);
		    }
	    }

	    //courseReviewCn
	    searchVO.setLastUpdusrId(user.getId());
	    searchVO.setCourseReviewCn(unscript(searchVO.getCourseReviewCn())); // XSS 방지

	    curriculumService.updateCourseReview(searchVO);

	    request.getSession().removeAttribute("sessionVO");
	    //request.getSession().setAttribute("sessionVO", searchVO);
	    return forwardUrl;
    }
  	
  	//과정후기 상세
  	@RequestMapping("/lms/crcl/selecCourseReviewAtView.do")
  	public String selecCourseReviewView(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
  		
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("user", user);
  		
//  		if(!searchVO.getCrclLang().equals("total")){
//  			CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
//  			model.addAttribute("curriculumVO", curriculumVO);
//  		}
  		
//  		CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
//  		model.addAttribute("homeworkVO", homeworkVO);
  		
  		//과제 제출 상세 조회
  		CurriculumVO courseReviewVo = curriculumService.selectCourseReviewAtView(searchVO);
  		model.addAttribute("courseReviewVo", courseReviewVo);
  		
  		//과제 제출 이미지 경로
  		model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));
  		
  		request.getSession().setAttribute("sessionVO", searchVO);
  		
  		return "/lms/crcl/HomeworkCourseReviewAtView";
  	}
  	
    //과정후기 삭제
    @RequestMapping("/lms/crcl/deleteCourseReview.json")
    public void deleteCourseReview(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    		searchVO.setLastUpdusrId(user.getId());
    	}
    	
    	curriculumService.deleteCourseReview(searchVO);
    	
    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
  	
  	//과제 상세
  	@RequestMapping("/lms/crcl/selectHomeworkCommentPickAtArticle.do")
    public String selectHomeworkCommentPickAtArticle(@ModelAttribute("searchVO") CurriculumVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
      	model.addAttribute("homeworkVO", homeworkVO);

      	//과제 제출 상세 조회
		CurriculumVO homeworkSubmitVO = curriculumService.selectHomeworkSubmitArticle(searchVO);
      	model.addAttribute("homeworkSubmitVO", homeworkSubmitVO);

      	//과제 제출 이미지 경로
      	model.addAttribute("HomeworkSubmitFileStoreWebPath", propertiesService.getString("HomeworkSubmit.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

	    return "/lms/crcl/HomeworkCommentPickAtView";
    }
  	
    @RequestMapping(value="/lms/crcl/popCurriculumSurveyView.do")
    public String popCurriculumSurveyView(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(surveyVo.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	CurriculumVO searchVO = curriculumVO;
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
    	//과정 추가 정보
    	model.addAttribute("curriculumAddInfo", this.surveyManageService.selectCurriculumAddInfo(surveyVo));
    	//과정 설문
    	model.addAttribute("surveyAnswer", this.surveyManageService.selectCurriculumSurveyAnswer(surveyVo));
    	
    	// 과정만족도 답변점수목록
    	model.addAllAttributes(this.surveyManageService.curriculumSurveyAnswerScore(surveyVo));

    	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);


        return "/lms/crcl/popCurriculumSurveyView";
    }
    
    
    @RequestMapping("/lms/crcl/CurriculumBoardPrintStaff.do")
    public String CurriculumBoardPrintStaff(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	//과정정보
    	CurriculumVO curriculumVO = new CurriculumVO();
    	curriculumVO.setCrclId(lmsMngVO.getCrclId());
    	curriculumVO = curriculumService.selectCurriculum(curriculumVO);
    	model.addAttribute("curriculumVO", curriculumVO);

    	BoardMasterVO boardMasterVO = new BoardMasterVO();

    	//과정게시판 목록
		boardMasterVO.setFirstIndex(0);
		boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
		boardMasterVO.setSiteId(lmsMngVO.getCrclId());
		Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
		List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
		model.addAttribute("masterList", result);

	  	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	  	if(user != null){
    		boardVO.setFrstRegisterId(user.getId());
    		model.addAttribute("sessionUniqId", user.getId());
    	}

	  	BoardMasterVO vo = new BoardMasterVO();
	  	vo.setSiteId(lmsMngVO.getCrclId());
	  	//vo.setSysTyCode(boardVO.getSysTyCode());
	  	vo.setBbsId(boardVO.getBbsId());
	  	vo.setTrgetId(boardVO.getTrgetId());

	  	BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

	  	if(master != null) {

  		  model.addAttribute("brdMstrVO", master);

  		  // 조회수 증가 여부 지정
  		  boardVO.setPlusCount(false);
  		  boardVO.setCtgrymasterId(master.getCtgrymasterId());
  		  model.addAttribute("board", bbsMngService.selectPrintBoardArticle(boardVO));

  	  }

      	request.getSession().setAttribute("sessionVO", boardVO);

        return "/lms/crcl/CurriculumBoardPrintStaff";
    }
}
