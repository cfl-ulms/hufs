package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cmm.service.SearchVO;
import egovframework.com.cop.bbs.service.Board;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.ClassManageService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.GradeService;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.service.SurveyManageService;
import egovframework.com.lms.service.SurveyQuestionExVO;
import egovframework.com.lms.service.SurveyQuestionVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovHttpUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.com.utl.sim.service.EgovClntInfo;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class ClassController {

   @Resource(name = "propertiesService")
   protected EgovPropertyService propertyService;

   @Resource(name = "SiteManageService")
   EgovSiteManageService siteManageService;

   @Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

   @Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;

   @Resource(name = "classManageService")
    private ClassManageService classManageService;

    @Resource(name="EgovCmmUseService")
   private EgovCmmUseService cmmUseService;

    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "ScheduleMngService")
   private ScheduleMngService scheduleMngService;

    @Resource(name = "EgovFileMngService")
    private EgovFileMngService fileService;

    @Resource(name = "EgovBBSManageService")
      private EgovBBSManageService bbsMngService;

    @Resource(name = "EgovBBSAttributeManageService")
      private EgovBBSAttributeManageService bbsAttrbService;

    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "EgovFileMngUtil")
   private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
   private EgovFileMngService fileMngService;

    @Resource(name = "surveyManageService")
    private SurveyManageService surveyManageService;

    @Resource(name="surveyAnswerIdGnrService")
    private EgovIdGnrService surveyAnswerIdGnrService;

    @Resource(name = "gradeService")
	private GradeService gradeService;



    @RequestMapping(value="/lms/cla/classFileList.do")
    public String classFileList(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	String returnPage = "";

       LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
       if(user == null){
          return "redirect:/index.do";
       }else{
    	  model.addAttribute("USER_INFO", user);
    	   
    	  fileVo.setSessionId(user.getId());
    	  fileVo.setUserId(user.getId());

    	  if("gallery".equals(fileVo.getViewType())){
    		  fileVo.setPageUnit(9);
    	  }else{
    		  fileVo.setPageUnit(propertyService.getInt("pageUnit"));
    	  }
          fileVo.setPageSize(propertyService.getInt("pageSize"));

          PaginationInfo paginationInfo = new PaginationInfo();
          paginationInfo.setCurrentPageNo(fileVo.getPageIndex());
          paginationInfo.setRecordCountPerPage(fileVo.getPageUnit());
          paginationInfo.setPageSize(fileVo.getPageSize());

          fileVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
          fileVo.setLastIndex(paginationInfo.getLastRecordIndex());
          fileVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

    	   if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
    		   	if(fileVo.getViewType() == null){
    			   fileVo.setViewType("list");
    		   	}

    		   	if(fileVo.getDateType() == null){
    			   fileVo.setDateType("all");//today->all로 변경
    		   	}

    		   	//언어조회
    		   	Ctgry ctgry = new Ctgry();
               	ctgry.setCtgrymasterId("CTGMST_0000000000002");
               	model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
               	
               	fileVo.setStudentAt("Y");
               	EgovMap resultMap = this.classManageService.selectStudentFileListMap(fileVo,request);
   				model.addAttribute("fileList", resultMap.get("resultList"));
    	   		model.addAttribute("fileListCnt", resultMap.get("resultCnt"));
    	   		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
    	   		model.addAttribute("paginationInfo", paginationInfo);

    		   returnPage = "/lms/cla/StudentClassFileList";
            } else {
            	if("Y".equals(fileVo.getSearchFrstRegisterId())){
                    fileVo.setFrstRegisterId(user.getId());
                }

            	//과정조회
            	List<?> curriculumbaseList = this.classManageService.selectClassCurriculumList(fileVo);
            	model.addAttribute("curriculumbaseList", curriculumbaseList);

     	   		EgovMap resultMap = this.classManageService.selectTeacherFileListMap(fileVo,request);
     	   		model.addAttribute("fileList", resultMap.get("resultList"));
     	   		model.addAttribute("fileListCnt", resultMap.get("resultCnt"));
     	   		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
     	   		model.addAttribute("paginationInfo", paginationInfo);

     	   		returnPage = "/lms/cla/TeacherClassFileList";

           }

    	   model.addAttribute("fileStorePath", propertiesService.getString("Study.fileStoreWebPath"));

    	   return returnPage;
       }
    }

    @RequestMapping(value = "/lms/cla/classFileListAjax.do")
	public String classFileListAjax(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
    	 LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	 fileVo.setSessionId(user.getId());

    	 if("gallery".equals(fileVo.getViewType())){
   		  	fileVo.setPageUnit(9);
	   	 }else{
	   		  fileVo.setPageUnit(propertyService.getInt("pageUnit"));
	   	 }
    	 fileVo.setPageSize(propertyService.getInt("pageSize"));

    	 PaginationInfo paginationInfo = new PaginationInfo();
    	 paginationInfo.setCurrentPageNo(fileVo.getPageIndex());
    	 paginationInfo.setRecordCountPerPage(fileVo.getPageUnit());
    	 paginationInfo.setPageSize(fileVo.getPageSize());

    	 fileVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
    	 fileVo.setLastIndex(paginationInfo.getLastRecordIndex());
    	 fileVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

     	EgovMap resultMap = this.classManageService.selectStudentFileListMap(fileVo,request);
		model.addAttribute("fileList", resultMap.get("resultList"));
   		model.addAttribute("fileListCnt", resultMap.get("resultCnt"));
   		paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));

		return "/lms/cla/StudentClassFileAjaxList";
	}

   @RequestMapping("/lms/cla/classCurriculumList.json")
   public void classCurriculumList(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
      JSONObject jo = new JSONObject();
      response.setContentType("application/json;charset=utf-8");

      List<?> curriculumbaseList = classManageService.selectClassCurriculumList(fileVo);
      model.addAttribute("curriculumbaseList", curriculumbaseList);

      jo.put("curriculumbaseList", curriculumbaseList);

      PrintWriter printwriter = response.getWriter();
      printwriter.println(jo.toString());
      printwriter.flush();
      printwriter.close();
   }

   @RequestMapping("/lms/cla/classFileView.do")
   public String classFileView(@ModelAttribute("fileVo") FileVO fileVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
      model.addAttribute("fileInfo", classManageService.selectFileInfo(fileVo));
      return "/lms/cla/classFileView";
   }

   @RequestMapping(value="/lms/cla/curriculumStudyList.do")
   public String curriculumStudyList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
      //** EgovPropertyService.sample */
       searchVO.setPageUnit(propertyService.getInt("pageUnit"));
       searchVO.setPageSize(propertyService.getInt("pageSize"));

       PaginationInfo paginationInfo = new PaginationInfo();
      paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
      paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
      paginationInfo.setPageSize(searchVO.getPageSize());

      searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
      searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
      searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      //주관기관조회
      Ctgry ctgry = new Ctgry();
      ctgry.setCtgrymasterId("CTGMST_0000000000009");
      List<Ctgry> ctgryList = this.egovBBSCtgryService.selectComtnbbsctgryList(ctgry);
      model.addAttribute("ctgryList", ctgryList);

      //기본과정
      model.addAttribute("curriculumBaseList", this.classManageService.selectCurriculumBaseList(searchVO));

      //과정
      model.addAttribute("curriculumList", this.classManageService.selectCurriculumList(searchVO));


      EgovMap resultMap = this.classManageService.selectCurriculumStudyMap(searchVO);
      model.addAttribute("studyList", resultMap.get("resultList"));
      model.addAttribute("studyListCnt", resultMap.get("resultCnt"));
      paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
       model.addAttribute("paginationInfo", paginationInfo);
       return "/lms/cla/curriculumStudyList";
   }

   @RequestMapping("/lms/cla/curriculumList.json")
   public void curriculumList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
      JSONObject jo = new JSONObject();
      response.setContentType("application/json;charset=utf-8");
      //기본과정
      jo.put("curriculumBaseList", this.classManageService.selectCurriculumBaseList(searchVO));
      //과정
      jo.put("curriculumList", this.classManageService.selectCurriculumList(searchVO));
      PrintWriter printwriter = response.getWriter();
      printwriter.println(jo.toString());
      printwriter.flush();
      printwriter.close();
   }

   @RequestMapping(value="/lms/cla/curriculumBoardList.do")
   public String curriculumBoardList(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO,Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
      //과정정보
      CurriculumVO curriculumVO = new CurriculumVO();
      curriculumVO.setCrclId(lmsMngVO.getCrclId());
      curriculumVO = curriculumService.selectCurriculum(curriculumVO);
      model.addAttribute("curriculumVO", curriculumVO);

      BoardMasterVO master = new BoardMasterVO();
      BoardMasterVO boardMasterVO = new BoardMasterVO();

      //과정게시판 목록
      boardMasterVO.setFirstIndex(0);
      boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
      boardMasterVO.setSiteId(lmsMngVO.getCrclId());
      Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
      List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
      model.addAttribute("masterList", result);

      //bbsid체크
      if(EgovStringUtil.isEmpty(lmsMngVO.getBbsId())){
         master = result.get(0);
      }else{
         boardMasterVO.setSiteId(lmsMngVO.getCrclId());
         boardMasterVO.setBbsId(lmsMngVO.getBbsId());
         master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
      }

      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      if(user != null){
         //boardVO.setFrstRegisterId(user.getId());
         //boardVO.setAdminAt("Y");
         boardVO.setSessionId(user.getId());
         model.addAttribute("sessionUniqId", user.getId());
      }
      
      //과정 반 조회
      if("CLASS".equals(master.getSysTyCode())){
          List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
         model.addAttribute("selectClassList", selectClassList);
      }else if("GROUP".equals(master.getSysTyCode())){
          //과정 조 조회
         List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
         model.addAttribute("selectGroupList", selectGroupList);
      }

       PaginationInfo paginationInfo = new PaginationInfo();
         if(master != null) {

         // 페이징 정보 설정
         boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
         boardVO.setPageSize(propertiesService.getInt("pageSize"));
         boardVO.setCtgrymasterId(master.getCtgrymasterId());
         //boardVO.setSysTyCode(master.getSysTyCode());
         /*
         //공지게시물 가져오기
         BoardVO noticeVO = new BoardVO();
         noticeVO.setBbsId(lmsMngVO.getBbsId());
         noticeVO.setTmplatId(master.getTmplatId());
         noticeVO.setCommentUseAt(master.getCommentUseAt());
         noticeVO.setSearchNoticeAt("Y");
         noticeVO.setFirstIndex(0);
         noticeVO.setRecordCountPerPage(9999);

         model.addAttribute("noticeList", bbsMngService.selectBoardArticles(noticeVO));
         */
         paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
         paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
         paginationInfo.setPageSize(boardVO.getPageSize());

         boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
         boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
         boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

         boardVO.setBbsId(master.getBbsId());
         boardVO.setCommentUseAt(master.getCommentUseAt());
         boardVO.setTmplatId(master.getTmplatId());
         boardVO.setBbsAttrbCode(master.getBbsAttrbCode());

         if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
             boardVO.setUserFlag("student");
             EgovMap tempMap = new EgovMap();
             tempMap.put("crclId", curriculumVO.getCrclId());
             tempMap.put("userId", user.getId());
             EgovMap userMap = this.classManageService.selectCurriculumUserInfo(tempMap);
             model.addAttribute("userInfo", userMap);
             if("CLASS".equals(master.getSysTyCode())){
            	 boardVO.setSearchClass(userMap.get("classCnt").toString());
             }else if("GROUP".equals(master.getSysTyCode())){
            	 boardVO.setSearchClass(userMap.get("groupCnt").toString());
             }else if("INDIV".equals(master.getSysTyCode())){
            	 boardVO.setFrstRegisterId(user.getId());
             }
          } else { //교원
        	  boardVO.setUserFlag("teacher");
          }
         boardVO.setSysTyCode(master.getSysTyCode());

         List<BoardVO> resultList = bbsMngService.selectBoardArticles(boardVO);
         int totCnt = bbsMngService.selectBoardArticlesCnt(boardVO);

         paginationInfo.setTotalRecordCount(totCnt);

         if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
            Ctgry ctgry = new Ctgry();
            ctgry.setCtgrymasterId(master.getCtgrymasterId());
            model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
            model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
         }

         model.addAttribute("resultList", resultList);
         model.addAttribute("resultCnt", totCnt);
         model.addAttribute("brdMstrVO", master);
       }

         model.addAttribute("paginationInfo", paginationInfo);

         //과정 담당자 인지 확인
        /*String managerAt = "N";
        if(user != null){
           curriculumVO.setManageCode("");
           List subUserList = curriculumService.selectCurriculumMng(curriculumVO);
            for(int i = 0; i < subUserList.size(); i ++){
               EgovMap map1 = (EgovMap) subUserList.get(i);
               String userId = map1.get("userId").toString();
               if(user.getId().equals(userId)){
                  managerAt = "Y";
               }
            }
        }
        model.addAttribute("managerAt", managerAt);*/

        request.getSession().setAttribute("sessionVO", boardVO);

        if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
            return "/lms/cla/MyCurriculumBoardList";
         } else {
           //과정상태
           return "/lms/cla/MyTeacherCurriculumBoardList";
        }
   }


   @RequestMapping(value="/lms/cla/myClassSurveyList.do")
   public String curriculumSurveyList(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

     CurriculumVO curriculumVO = new CurriculumVO();
     curriculumVO.setCrclId(surveyVo.getCrclId());
     curriculumVO = curriculumService.selectCurriculum(curriculumVO);
     model.addAttribute("curriculumVO", curriculumVO);

   //설문리스트
	List<EgovMap> resultSurveyList = this.surveyManageService.selectCurriculumSurveyList(surveyVo);
	model.addAttribute("resultSurveyList", resultSurveyList);

	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
	String curDate = sdf.format(date);

 	 model.addAttribute("curDate", curDate);
     return "/lms/cla/myClassSurveyList";
  }

   @RequestMapping(value="/lms/cla/surveyView.do")
   public String surveyView(@ModelAttribute("surveyVO") SurveyVO surveyVo,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	   CurriculumVO curriculumVO = new CurriculumVO();
	   curriculumVO.setCrclId(surveyVo.getCrclId());
	   curriculumVO = curriculumService.selectCurriculum(curriculumVO);
	   model.addAttribute("curriculumVO", curriculumVO);

	   List<EgovMap> resultSurveyList = this.surveyManageService.selectCurriculumSurveyList(surveyVo);
	   model.addAttribute("resultSurveyList", resultSurveyList);

	   LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	   surveyVo.setUserId(user.getId());

	   int answerCnt = surveyManageService.selectAnswerSurveyCnt(surveyVo);
	   model.addAttribute("answerCnt", answerCnt);

	   if(answerCnt == 0){
		   SurveyVO surveyInfo = surveyManageService.selectSurveyInfo(surveyVo);
			for(SurveyQuestionVO queVo : surveyInfo.getQuestionsArray()){
				List<SurveyQuestionExVO> widthExVoList = new ArrayList<SurveyQuestionExVO>();
				List<SurveyQuestionExVO> heigthExVoList = new ArrayList<SurveyQuestionExVO>();

				if("table".equals(queVo.getQesitmTyCode())){
					for(SurveyQuestionExVO tempEx : queVo.getExamples()){
						if("W".equals(tempEx.getExTy())){
							widthExVoList.add(tempEx);
						}else{
							heigthExVoList.add(tempEx);
						}
					}
					queVo.setWidthExamples(widthExVoList);
					queVo.setHeightExamples(heigthExVoList);
				}
			}
		   model.addAttribute("surveyInfo", surveyInfo);
	   }
	   Date date = new Date();
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	   String curDate = sdf.format(date);
	   
	   String referer = request.getHeader("referer");
	   model.addAttribute("referer", referer);
	   
	   return "/lms/cla/surveyView";
  }

   @RequestMapping(value="/lms/cla/insertSurvey.json")
   public void insertSurvey(@ModelAttribute("surveyVO") SurveyVO surveyVo ,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
	   LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	   EgovMap paramMap = new EgovMap();
	   JSONArray array = JSONArray.fromObject(request.getParameter("answerList"));
	   List<EgovMap> answerList = new ArrayList<EgovMap>();
	   for(int i=0; i<array.size(); i++){
	        JSONObject obj = (JSONObject)array.get(i);
	        System.out.println(obj.get("cnsr"));
	        EgovMap tempMap = new EgovMap();
	        tempMap.put("cnsr", obj.get("cnsr"));
	        tempMap.put("qesitmId", obj.get("qesitmId"));
	        tempMap.put("aswperId",surveyAnswerIdGnrService.getNextStringId());
	        answerList.add(tempMap);
	   }

	   paramMap.put("crclId", surveyVo.getCrclId());
	   paramMap.put("schdulId", surveyVo.getSchdulId());
	   paramMap.put("userId", user.getId());
	   paramMap.put("plId", surveyVo.getPlId());
	   paramMap.put("answerList", answerList);
	   paramMap.put("clientIp", EgovClntInfo.getClntIP(request));


	    JSONObject jo = new JSONObject();
		response.setContentType("application/json;charset=utf-8");

		surveyManageService.insertSurveySubmit(paramMap);
		//impl로 이동
		//surveyManageService.insertSurveyAnswer(paramMap);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
  }

   @RequestMapping(value="/lms/cla/mySurveyList.do")
   public String mySurveyList(@ModelAttribute("searchVo") SearchVO searchVo, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
	   LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	   model.addAttribute("USER_INFO", user);
	   
	   PaginationInfo paginationInfo = new PaginationInfo();
	   paginationInfo.setCurrentPageNo(searchVo.getPageIndex());
	   paginationInfo.setRecordCountPerPage(searchVo.getPageUnit());
	   paginationInfo.setPageSize(searchVo.getPageSize());

	   searchVo.setFirstIndex(paginationInfo.getFirstRecordIndex());
	   searchVo.setLastIndex(paginationInfo.getLastRecordIndex());
	   searchVo.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
	   searchVo.setSessionId(user.getId());
	   
	   EgovMap resultMap = new EgovMap();
	   if("08".equals(user.getUserSeCode())) {
		   resultMap = this.surveyManageService.selectMyProfessorSurvey(searchVo);
	   }else {
		   resultMap = this.surveyManageService.selectMySurvey(searchVo);
	   }
	   
	   paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
	   model.addAttribute("surveyList", resultMap.get("resultList"));
	   model.addAttribute("surveyListCnt", resultMap.get("resultCnt"));
	   model.addAttribute("paginationInfo", paginationInfo);

	   Ctgry ctgry = new Ctgry();
	   ctgry.setCtgrymasterId("CTGMST_0000000000002");
	   model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

	   Date date = new Date();
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
	   String curDate = sdf.format(date);
	   model.addAttribute("curDate", curDate);

      return "/lms/cla/mySurveyList";
   }

   @RequestMapping(value="/lms/cla/curBoardList.do")
   public String curBoardList(@ModelAttribute("searchVO") SearchVO searchVO, HttpServletRequest request, HttpServletResponse response, ModelMap model)throws Exception{
      //** EgovPropertyService.sample */
        LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

       searchVO.setPageUnit(propertyService.getInt("pageUnit"));
       searchVO.setPageSize(propertyService.getInt("pageSize"));

       PaginationInfo paginationInfo = new PaginationInfo();
      paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
      paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
      paginationInfo.setPageSize(searchVO.getPageSize());

      searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
      searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
      searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

      //과정
      //model.addAttribute("curriculumList", this.classManageService.selectCurriculumList(searchVO));

      String pageFlag = "";
      searchVO.setSessionId(user.getId());
        if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
           pageFlag = "student";

        } else { //교원
           pageFlag = "teacher";
        }

        searchVO.setSearchPageFlag(pageFlag);
      EgovMap resultMap = this.classManageService.selectCurriculumBoardMap(searchVO);
      model.addAttribute("boardList", resultMap.get("resultList"));
      model.addAttribute("boardListCnt", resultMap.get("resultCnt"));
      paginationInfo.setTotalRecordCount(Integer.parseInt(resultMap.get("resultCnt").toString()));
      model.addAttribute("paginationInfo", paginationInfo);

       if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
             //언어코드
            Ctgry ctgry = new Ctgry();
            ctgry.setCtgrymasterId("CTGMST_0000000000002");
            model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
            return "/lms/cla/MyCurBoardList";
       } else {
          //과정상태
    	   	ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
            voComCode = new ComDefaultCodeVO();
            voComCode.setCodeId("LMS30");
            List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
            model.addAttribute("statusComCode", statusComCode);
            return "/lms/cla/MyTeacherCurBoardList";
       }

   }

   @RequestMapping(value="/lms/cla/studyPlanView.do")
   public String studyPlanView(@ModelAttribute("searchVO") ScheduleMngVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

      SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
       model.addAttribute("siteInfo", siteVO);

         //과정정보
         CurriculumVO curriculumVO = new CurriculumVO();
         curriculumVO.setCrclId(searchVO.getCrclId());
         curriculumVO = curriculumService.selectCurriculum(curriculumVO);
         model.addAttribute("curriculumVO", curriculumVO);

         //수업계획
         ScheduleMngVO scheduleMngVO = scheduleMngService.selectClassScheduleView(searchVO);
         model.addAttribute("scheduleMngVO", scheduleMngVO);

         //과정에 등록된 교원
         model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));

         //등록 된 시간표 담당교원
         List facPlList = scheduleMngService.selectFacultyList(searchVO);
         model.addAttribute("facPlList", facPlList);

         //캠퍼스 시간표
         searchVO.setCampusId(curriculumVO.getCampusId());
         model.addAttribute("camSchList", scheduleMngService.selectCampusSchedule(searchVO));

         //과정에 등록된 교원
         model.addAttribute("evtResultList", scheduleMngService.selectStudyEvtList(searchVO));

         //등록한 수업자료 리스트
         model.addAttribute("fileList", scheduleMngService.selectStudyFileList(searchVO));

         EgovMap fileVO = new EgovMap();
         fileVO.put("plId", searchVO.getPlId());
         fileVO.put("userId", user.getId());
         model.addAttribute("fileDownCnt", fileService.selectFileDownLogCnt(fileVO));

         return "/lms/cla/studyPlanView";
   }

   @RequestMapping("/lms/cla/curriculumBoardView.do")
   public String curriculumBoardView(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
         //과정정보
      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
         CurriculumVO curriculumVO = new CurriculumVO();
         curriculumVO.setCrclId(lmsMngVO.getCrclId());
         curriculumVO = curriculumService.selectCurriculum(curriculumVO);
         model.addAttribute("curriculumVO", curriculumVO);

         BoardMasterVO boardMasterVO = new BoardMasterVO();

         //과정게시판 목록
      boardMasterVO.setFirstIndex(0);
      boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
      boardMasterVO.setSiteId(lmsMngVO.getCrclId());
      Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
      List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
      model.addAttribute("masterList", result);


        if(user != null){
            boardVO.setFrstRegisterId(user.getId());
            boardVO.setAdminAt("Y");

            model.addAttribute("sessionUniqId", user.getId());
        }

        BoardMasterVO vo = new BoardMasterVO();
        vo.setSiteId(lmsMngVO.getCrclId());
        //vo.setSysTyCode(boardVO.getSysTyCode());
        vo.setBbsId(boardVO.getBbsId());
        vo.setTrgetId(boardVO.getTrgetId());

        BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

        if(master != null) {
         model.addAttribute("brdMstrVO", master);

         // 조회수 증가 여부 지정
         boardVO.setPlusCount(false);
         boardVO.setCtgrymasterId(master.getCtgrymasterId());
         model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

        }
        request.getSession().setAttribute("sessionVO", boardVO);


        if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
          return "/lms/cla/MyCurriculumBoardView";
       } else {
          //과정상태
          return "/lms/cla/MyTeacherCurriculumBoardView";
       }
   }

   //게시판 게시글 삭제
   @RequestMapping(value={"/lms/cla/deleteCurriculumBoard.do", "/lms/cla/deleteCurBoard"})
   public String deleteCurriculumBoard(@ModelAttribute("searchVO") BoardVO boardVO, BoardVO board, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
     String reqUrl = request.getRequestURI();
     LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

      BoardMasterVO vo = new BoardMasterVO();
      vo.setSiteId(boardVO.getCrclId());
      //vo.setSysTyCode(boardVO.getSysTyCode());
      vo.setBbsId(boardVO.getBbsId());
      vo.setTrgetId(boardVO.getTrgetId());

      BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

      if(master != null) {
         board.setAdminAt("Y");
         board.setLastUpdusrId(user.getId());
         bbsMngService.deleteBoardArticle(board, master);
      }

      String tempMenuId = request.getParameter("menuId").toString();
      if(reqUrl.contains("deleteCurBoard")){//수업 >> 오늘의 수업 >> 게시판
    	  return "redirect:/lms/cla/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+boardVO.getBbsId();
      }else{//수업 >> 과정게시판, 교육과정
    	  String tempPlid = request.getParameter("plId").toString();
    	  String retUrl = "/lms/cla/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&plId="+tempPlid+"&bbsId="+boardVO.getBbsId();
    	  if(!EgovStringUtil.isEmpty(request.getParameter("menu"))){
    		  retUrl += "&menu=" + request.getParameter("menu");
    	  }
    	  if(!EgovStringUtil.isEmpty(request.getParameter("subtabstep"))){
    		  retUrl += "&subtabstep=" + request.getParameter("subtabstep");
    	  }
    	  
    	  return "redirect:" + retUrl;
    	  
      }
   }

   //과정게시판 등록
   @RequestMapping(value="/lms/cla/curriculumBoardAdd.do")
   public String lmsBbsAdd(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	   LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
         //과정정보
	   CurriculumVO curriculumVO = new CurriculumVO();
	   curriculumVO.setCrclId(lmsMngVO.getCrclId());
	   curriculumVO = curriculumService.selectCurriculum(curriculumVO);
	   model.addAttribute("curriculumVO", curriculumVO);

       BoardMasterVO boardMasterVO = new BoardMasterVO();

         //과정게시판 목록
       boardMasterVO.setFirstIndex(0);
       boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
       boardMasterVO.setSiteId(lmsMngVO.getCrclId());
       Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
       List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
       model.addAttribute("masterList", result);

       BoardMasterVO vo = new BoardMasterVO();
       vo.setSiteId(lmsMngVO.getCrclId());
       vo.setSysTyCode(boardVO.getSysTyCode());
       vo.setBbsId(boardVO.getBbsId());
       vo.setTrgetId(boardVO.getTrgetId());

       BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

        if(master != null) {
          if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
             Ctgry ctgry = new Ctgry();
             ctgry.setCtgrymasterId(master.getCtgrymasterId());
             model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
             model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
            }

             model.addAttribute("brdMstrVO", master);

           if("CLASS".equals(master.getSysTyCode())){
             List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
            model.addAttribute("selecteResultList", selectClassList);
            }else if("GROUP".equals(master.getSysTyCode())){
                //과정 조 조회
               List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
               model.addAttribute("selecteResultList", selectGroupList);
            }

          Board board = new Board();
          model.addAttribute("board", board);

          request.getSession().setAttribute("sessionVO", boardVO);
        }

        EgovMap tempMap = new EgovMap();
        tempMap.put("crclId", curriculumVO.getCrclId());
        tempMap.put("userId", user.getId());
        model.addAttribute("userInfo", this.classManageService.selectCurriculumUserInfo(tempMap));


       if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
             return "/lms/cla/MyCurriculumBoardAdd";
      } else {
         return "/lms/cla/MyTeacherCurriculumBoardAdd";
      }
   }

   //과정게시판 등록 프로세스
   @RequestMapping("/lms/cla/insertBoardArticle.do")
   public String insertBoardStaffArticle(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") BoardVO boardVO,
         Board board, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

	   String reqUrl = request.getRequestURI();
      if(request.getSession().getAttribute("sessionVO") == null) {
         return "forward:/lms/cla/curriculumBoardList.do";
      }

      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

      BoardMasterVO vo = new BoardMasterVO();
      vo.setSiteId(boardVO.getCrclId());
      //vo.setSysTyCode(boardVO.getSysTyCode());
      vo.setBbsId(boardVO.getBbsId());
      vo.setTrgetId(boardVO.getTrgetId());

      BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

      if(master != null) {
         String atchFileId = "";
         if(EgovStringUtil.isEmpty(boardVO.getSiteId())){
            boardVO.setSiteId(master.getSiteId());
         }

          List<FileVO> result = null;

          final Map<String, MultipartFile> files = multiRequest.getFileMap();
          if(!files.isEmpty()) {
            result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, "", boardVO.getSiteId(), boardVO.getBbsId(),"","");
            atchFileId = fileMngService.insertFileInfs(result);
          }


          boardVO.setAtchFileId(atchFileId);
          boardVO.setFrstRegisterId(user.getId());
          boardVO.setNtcrNm(user.getName());
          boardVO.setCreatIp(EgovClntInfo.getClntIP(request));
          boardVO.setEstnData(EgovHttpUtil.getEstnParseData(request));

          bbsMngService.insertBoardArticle(boardVO, master);

          request.getSession().removeAttribute("sessionVO");
      }

      String tempMenuId = request.getParameter("menuId").toString();
      if("MNU_0000000000000086".equals(tempMenuId)){//수업 >> 오늘의 수업 >> 게시판
    	  String tempPlid = request.getParameter("plId").toString();
    	  return "redirect:/lms/cla/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&plId="+tempPlid+"&bbsId="+boardVO.getBbsId();
      }else{//수업 >> 과정게시판, 교육과정
    	  String retUrl = "/lms/cla/curriculumBoardList.do?crclId="+boardVO.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+boardVO.getBbsId();
    	  if(!EgovStringUtil.isEmpty(request.getParameter("menu"))){
    		  retUrl += "&menu=" + request.getParameter("menu");
    	  }
    	  if(!EgovStringUtil.isEmpty(request.getParameter("subtabstep"))){
    		  retUrl += "&subtabstep=" + request.getParameter("subtabstep");
    	  }
    	  
    	  return "redirect:" + retUrl;
      }

   }

   @RequestMapping("/lms/cla/forUpdateBoardArticle.do")
   public String forUpdateBoardStaffArticle(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
	   LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
	   //과정정보
      CurriculumVO curriculumVO = new CurriculumVO();
      curriculumVO.setCrclId(lmsMngVO.getCrclId());
      curriculumVO = curriculumService.selectCurriculum(curriculumVO);
      model.addAttribute("curriculumVO", curriculumVO);

      BoardMasterVO boardMasterVO = new BoardMasterVO();

      //과정게시판 목록
      boardMasterVO.setFirstIndex(0);
      boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
      boardMasterVO.setSiteId(lmsMngVO.getCrclId());
      Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
      List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
      model.addAttribute("masterList", result);


      BoardMasterVO vo = new BoardMasterVO();
      vo.setSiteId(boardVO.getCrclId());
      //vo.setSysTyCode(boardVO.getSysTyCode());
      vo.setBbsId(boardVO.getBbsId());
      vo.setTrgetId(boardVO.getTrgetId());
      boardVO.setAdminAt("Y");

      BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

      if(master != null) {
         if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
            Ctgry ctgry = new Ctgry();
            ctgry.setCtgrymasterId(master.getCtgrymasterId());
            model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
            model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
          }

         boardVO.setCtgrymasterId(master.getCtgrymasterId());
         BoardVO dataVO = bbsMngService.selectBoardArticle(boardVO);

         model.addAttribute("brdMstrVO", master);
         model.addAttribute("board", dataVO);

         request.getSession().setAttribute("sessionVO", boardVO);

         if("CLASS".equals(master.getSysTyCode())){
          List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
         model.addAttribute("selecteResultList", selectClassList);
         }else if("GROUP".equals(master.getSysTyCode())){
          //과정 조 조회
         List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
         model.addAttribute("selecteResultList", selectGroupList);
         }
      }

      if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
    	  EgovMap tempMap = new EgovMap();
          tempMap.put("crclId", curriculumVO.getCrclId());
          tempMap.put("userId", user.getId());
          model.addAttribute("userInfo", this.classManageService.selectCurriculumUserInfo(tempMap));
          return "/lms/cla/MyCurriculumBoardAdd";
	   } else {
	      return "/lms/cla/MyTeacherCurriculumBoardAdd";
	   }
   }

   //게시판 게시 글 수정프로세스
   @RequestMapping(value={"/lms/cla/updateBoardArticle.do", "/lms/cla/updateCurBoard.do"})
   public String updateBoardStaffArticle(final MultipartHttpServletRequest multiRequest,
         BoardVO board, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

     String reqUrl = request.getRequestURI();
      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      String atchFileId = board.getAtchFileId();

      BoardMasterVO vo = new BoardMasterVO();
      vo.setSiteId(board.getCrclId());
      //vo.setSysTyCode(board.getSysTyCode());
      vo.setBbsId(board.getBbsId());
      vo.setTrgetId(board.getTrgetId());

      BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

      if(master != null) {
         if(EgovStringUtil.isEmpty(board.getSiteId())){
            board.setSiteId(master.getSiteId());
         }
         final Map<String, MultipartFile> files = multiRequest.getFileMap();
         if(!files.isEmpty()) {
            if(EgovStringUtil.isEmpty(atchFileId)) {
               List<FileVO> result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, 0, atchFileId, board.getSiteId(), board.getBbsId(),"","");
               atchFileId = fileMngService.insertFileInfs(result);
               board.setAtchFileId(atchFileId);
            } else {
               FileVO fvo = new FileVO();
               fvo.setAtchFileId(atchFileId);
               int cnt = fileMngService.getMaxFileSN(fvo);
               List<FileVO> _result = fileUtil.parseBoardFileInf(Long.parseLong(master.getPosblAtchFileSize()) * 1024 * 1024, files, cnt, atchFileId, board.getSiteId(), board.getBbsId(),"","");
               fileMngService.updateFileInfs(_result);
            }
         }

         board.setAdminAt("Y");
         board.setLastUpdusrId(user.getId());
         board.setEstnData(EgovHttpUtil.getEstnParseData(request));

         bbsMngService.updateBoardArticle(board, master, false);

         request.getSession().removeAttribute("sessionVO");
     }

      String tempMenuId = request.getParameter("menuId").toString();
      if(reqUrl.contains("updateCurBoard")){//수업 >> 오늘의 수업 >> 게시판
    	  return "redirect:/lms/cla/curriculumBoardList.do?crclId="+board.getCrclId()+"&menuId="+tempMenuId+"&bbsId="+board.getBbsId();
      }else{//수업 >> 과정게시판, 교육과정
    	  String tempPlid = request.getParameter("plId").toString();
    	  String retUrl = "/lms/cla/curriculumBoardList.do?crclId="+board.getCrclId()+"&menuId="+tempMenuId+"&plId="+tempPlid+"&bbsId="+board.getBbsId();
    	  if(!EgovStringUtil.isEmpty(request.getParameter("menu"))){
    		  retUrl += "&menu=" + request.getParameter("menu");
    	  }
    	  if(!EgovStringUtil.isEmpty(request.getParameter("subtabstep"))){
    		  retUrl += "&subtabstep=" + request.getParameter("subtabstep");
    	  }
    	  
    	  return "redirect:" + retUrl;
      }
   }

   @RequestMapping("/lms/cla/curBoardView.do")
   public String curBoardView(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
         //과정정보
      LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
         CurriculumVO curriculumVO = new CurriculumVO();
         curriculumVO.setCrclId(lmsMngVO.getCrclId());
         curriculumVO = curriculumService.selectCurriculum(curriculumVO);
         model.addAttribute("curriculumVO", curriculumVO);

         BoardMasterVO boardMasterVO = new BoardMasterVO();

         //과정게시판 목록
      boardMasterVO.setFirstIndex(0);
      boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
      boardMasterVO.setSiteId(lmsMngVO.getCrclId());
      Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
      List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
      model.addAttribute("masterList", result);


        if(user != null){
            boardVO.setFrstRegisterId(user.getId());
            boardVO.setAdminAt("Y");

            model.addAttribute("sessionUniqId", user.getId());
        }

        BoardMasterVO vo = new BoardMasterVO();
        vo.setSiteId(lmsMngVO.getCrclId());
        //vo.setSysTyCode(boardVO.getSysTyCode());
        vo.setBbsId(boardVO.getBbsId());
        vo.setTrgetId(boardVO.getTrgetId());

        BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

        if(master != null) {
         model.addAttribute("brdMstrVO", master);

         // 조회수 증가 여부 지정
         boardVO.setPlusCount(false);
         boardVO.setCtgrymasterId(master.getCtgrymasterId());
         model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

        }
        request.getSession().setAttribute("sessionVO", boardVO);


        if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
          return "/lms/cla/MyCurBoardView";
       } else {
          //과정상태
          return "/lms/cla/MyTeacherCurBoardView";
       }
   }

 //과정게시판 등록
   @RequestMapping(value="/lms/cla/curBoardAdd.do")
   public String curBoardAdd(@ModelAttribute("searchVO") BoardVO boardVO, LmsMngVO lmsMngVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

      //과정정보
      CurriculumVO curriculumVO = new CurriculumVO();
      curriculumVO.setCrclId(lmsMngVO.getCrclId());
      curriculumVO = curriculumService.selectCurriculum(curriculumVO);
      model.addAttribute("curriculumVO", curriculumVO);

      BoardMasterVO boardMasterVO = new BoardMasterVO();

      //과정게시판 목록
   boardMasterVO.setFirstIndex(0);
   boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
   boardMasterVO.setSiteId(lmsMngVO.getCrclId());
   Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
   List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
   model.addAttribute("masterList", result);

   BoardMasterVO vo = new BoardMasterVO();
   vo.setSiteId(lmsMngVO.getCrclId());
     vo.setSysTyCode(boardVO.getSysTyCode());
     vo.setBbsId(boardVO.getBbsId());
     vo.setTrgetId(boardVO.getTrgetId());

     BoardMasterVO master = bbsAttrbService.selectBBSMasterInf(vo);

     if(master != null) {
       if(!EgovStringUtil.isEmpty(master.getCtgrymasterId())) {
          Ctgry ctgry = new Ctgry();
          ctgry.setCtgrymasterId(master.getCtgrymasterId());
          model.addAttribute("boardCateList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
          model.addAttribute("boardCateLevel", egovBBSCtgryService.selectComtnbbsctgryLevel(ctgry));
         }

       model.addAttribute("brdMstrVO", master);

     if("CLASS".equals(master.getSysTyCode())){
       List<?> selectClassList = curriculumMemberService.selectCurriculumClassList(curriculumVO);
      model.addAttribute("selecteResultList", selectClassList);
      }else if("GROUP".equals(master.getSysTyCode())){
          //과정 조 조회
         List<?> selectGroupList = curriculumMemberService.selectCurriculumGroupList(curriculumVO);
         model.addAttribute("selecteResultList", selectGroupList);
      }

       Board board = new Board();
       model.addAttribute("board", board);

       request.getSession().setAttribute("sessionVO", boardVO);
     }

       return "/lms/cla/MyCurriculumBoardAdd";
   }

   @RequestMapping("/lms/cla/curriculumBoardStatistics.do")
   public String curriculumBoardStatistics(@ModelAttribute("searchVO") BoardVO boardVO,  LmsMngVO lmsMngVO,Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
         int totalBbsCnt = 0;

         if(boardVO.getSearchType() == ""){
            boardVO.setSearchType("student");
         }
         CurriculumVO curriculumVO = new CurriculumVO();
         curriculumVO.setCrclId(boardVO.getCrclId());
         curriculumVO = curriculumService.selectCurriculum(curriculumVO);
         model.addAttribute("curriculumVO", curriculumVO);

         BoardMasterVO master = new BoardMasterVO();
         BoardMasterVO boardMasterVO = new BoardMasterVO();

      boardMasterVO.setSiteId(boardVO.getCrclId());
      boardMasterVO.setBbsId(boardVO.getBbsId());
      master = bbsAttrbService.selectBBSMasterInf(boardMasterVO);
      model.addAttribute("masterVo", master);

      //과정게시판 목록
      boardMasterVO.setFirstIndex(0);
      boardMasterVO.setRecordCountPerPage(Integer.MAX_VALUE);
      Map<String, Object> map = bbsAttrbService.selectBBSMasterInfs(boardMasterVO);
      List<BoardMasterVO> result = (List<BoardMasterVO>) map.get("resultList");
      model.addAttribute("masterList", result);

      boardVO.setCntType(master.getSysTyCode().toLowerCase());

         //게시판 구분별 수
         List<EgovMap> pieList = this.bbsMngService.selectBoardStatisticsPie(boardVO);
         model.addAttribute("pieList", pieList);

         if(pieList != null){
            List<EgovMap> barList = new ArrayList<EgovMap>();
            for(EgovMap tempMap : pieList){
               boardVO.setCtgryId(tempMap.get("ctgryId").toString());
               if("0".equals(tempMap.get("ctgryLevel").toString())){
                  boardVO.setCtgryId(null);
               }
               EgovMap barMap = this.bbsMngService.selectBoardStatisticsBar(boardVO);
               barList.add(barMap);
               totalBbsCnt += Integer.parseInt(tempMap.get("cnt").toString());
            }
            model.addAttribute("barList", barList);
            model.addAttribute("barMemberList", this.bbsMngService.selectBoardStatisticsBarMemberList(boardVO));
            model.addAttribute("totalBbsCnt", totalBbsCnt);
         }


         PaginationInfo paginationInfo = new PaginationInfo();

          // 페이징 정보 설정
         if(boardVO.getSortType() == ""){
            boardVO.setSortType("sum");
         }

         boardVO.setPageUnit(propertiesService.getInt("pageUnit"));
         boardVO.setPageSize(propertiesService.getInt("pageSize"));
         paginationInfo.setCurrentPageNo(boardVO.getPageIndex());
        paginationInfo.setRecordCountPerPage(boardVO.getPageUnit());
        paginationInfo.setPageSize(boardVO.getPageSize());

        boardVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
        boardVO.setLastIndex(paginationInfo.getLastRecordIndex());
        boardVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

        EgovMap resultMap = this.bbsMngService.selectStatistics(boardVO);
        int totCnt = Integer.parseInt(resultMap.get("resultCnt").toString());
        paginationInfo.setTotalRecordCount(totCnt);

        EgovMap tempCollect = this.bbsMngService.selectAttendCollectCnt(boardVO);
        if(tempCollect != null){
           model.addAttribute("attendCollect", tempCollect.get("colect"));
        }

        model.addAttribute("resultList", resultMap.get("resultList"));
        model.addAttribute("resultCnt", totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

       request.getSession().setAttribute("sessionVO", boardVO);

      return "/lms/cla/CurriculumBoardStatistcs";
   }

   //운영보고서 리스트
 	@RequestMapping(value = "/lms/cla/manageReportList.do")
 	public String manageReportList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
 		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
 		searchVO.setUserId(user.getId());


 		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
	   	voComCode.setCodeId("LMS30");
	   	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
	   	model.addAttribute("statusComCode", statusComCode);


	   	/** pageing */
	   	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
	   	searchVO.setPageSize(propertiesService.getInt("pageSize"));

	   	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", this.curriculumService.selectReportList(searchVO));
		int totCnt = this.curriculumService.selectReportListCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("resultListCnt",totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		model.addAttribute("curDate", sdf.format(date));

		return "/lms/cla/ClaManageReportList";
 	}

}