package egovframework.com.lms.web;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import egovframework.com.lms.service.CertificateService;
import egovframework.com.lms.service.CertificateVO;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.SurveyVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import net.sf.json.JSONObject;


@Controller
public class CertificateController {

	 @Resource(name = "curriculumService")
	 private CurriculumService curriculumService;
	 
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
    
    @Resource(name = "certificateService")
    private CertificateService certificateService;

    //수료증 등록
    @RequestMapping("/lms/insertCertificate.json")
    public void insertCertificate(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    	}
    	
    	certificateService.insertCertificate(searchVO);
    	
    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
    //수료증 조회
    @RequestMapping("/lms/selectCertificate.do")
    public String selectCertificate(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setIssuerId(user.getId());
    	}
    	
    	searchVO.setFirstIndex(0);
    	searchVO.setRecordCountPerPage(9999);
    	searchVO.setUseAt("Y");
    	List<EgovMap> resultList = certificateService.selectCertificateList(searchVO);
    	model.addAttribute("resultList", resultList);
    	
    	String today = EgovDateUtil.getToday();
    	model.addAttribute("today", today);
    	
    	// 수료증 발급 이력 저장
    	certificateService.insertCertificateHst(searchVO);
    	
    	return "/lms/certificate/certificate";
    }
    
    //학생 수료증 발급 시 유효성 체크
    @RequestMapping(value="/ajax/lms/st/certificateCheck.json", method=RequestMethod.POST)
    public void certificateCheck(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String result = "Y";
    	String exitFlag = "N";
    	int passCnt = 0;
    	
    	CurriculumVO paramVO = new CurriculumVO();
    	paramVO.setCrclId(searchVO.getCrclId());
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		paramVO.setUserId(user.getId());
    		searchVO.setUserId(user.getId());
    	} else {
    		result = "N";
    	}
    	
    	JSONObject jo = new JSONObject();
    	response.setContentType("application/json;charset=utf-8");
    	
    	
    	if(!result.equals("N")) {
    		
    		String innerResult1 = "N";
    		String innerResult2 = "N";
    		String innerResult3 = "N";
    		
	    	// 과정종료(성적발표) 이후 과정 체크
	    	EgovMap currEgovMap = new EgovMap();
	    	currEgovMap = curriculumService.curriculumStatusCheck(paramVO);
	    	
	    	if(currEgovMap != null && currEgovMap.get("processSttusCode") != null && !StringUtils.isEmpty(currEgovMap.get("processSttusCode")) && exitFlag.equals("N")) {
	    		innerResult1 = "Y";
	    	} else {
	    		exitFlag = "Y";
	    		jo.put("result", "case1Fail");
	    	}
	    	
	    	// 과정만족도 참여여부 체크
	    	EgovMap surveyEgovMap = new EgovMap();
	    	surveyEgovMap = curriculumService.curriculumSurveyCheck(paramVO);
	    	
	    	if(surveyEgovMap != null && surveyEgovMap.get("schdulId") != null && surveyEgovMap.get("userId") != null && !StringUtils.isEmpty(surveyEgovMap.get("schdulId")) && !StringUtils.isEmpty(surveyEgovMap.get("userId")) && exitFlag.equals("N")) {
	    		innerResult2 = "Y";
	    	} else {
	    		if(exitFlag.equals("N")) {
	    			exitFlag = "Y";
	    			jo.put("result", "case2Fail");
	    			
	    			SurveyVO surveyVO = new SurveyVO();
	    			EgovMap map = new EgovMap();
	    			surveyVO.setUserId(user.getId());
	    			surveyVO.setCrclId(paramVO.getCrclId());
	    			map = certificateService.selectMySurveyDetail(surveyVO);
	    			jo.put("surveyVO", map);
	    		}
	    	}
	    	
	    	// 관리자 수료증 발급 여부 확인
	    	EgovMap certificateEgovMap = new EgovMap();
	    	certificateEgovMap = certificateService.certificateCheck(searchVO);
	    	
	    	if(certificateEgovMap != null && certificateEgovMap.get("certificateId") != null && certificateEgovMap.get("userId") != null && !StringUtils.isEmpty(certificateEgovMap.get("certificateId")) && !StringUtils.isEmpty(certificateEgovMap.get("userId")) && exitFlag.equals("N")) {
	    		innerResult3 = "Y";
	    	} else {
	    		if(exitFlag.equals("N")) {
	    			exitFlag = "Y";
	    			jo.put("result", "case3Fail");
	    		}
	    	}
	    	
	    	if(innerResult1.equals("Y") && innerResult2.equals("Y") && innerResult3.equals("Y")) {
	    		jo.put("result", "Y");
	    		jo.put("certificateId", certificateEgovMap.get("certificateId"));
	    	}
	    	
    	} else {
    		jo.put("result", result);
    	}
    	
    	
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    	
    }
    //학생 수료증 조회
    @RequestMapping("/lms/st/selectCertificate.do")
    public String selectStCertificate(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setIssuerId(user.getId());
    	}
    	
    	searchVO.setFirstIndex(0);
    	searchVO.setRecordCountPerPage(9999);
    	searchVO.setUseAt("Y");
    	
    	
    	List<EgovMap> resultList = certificateService.selectCertificateList(searchVO);
    	model.addAttribute("resultList", resultList);
    	
    	String today = EgovDateUtil.getToday();
    	model.addAttribute("today", today);
    	
    	// 수료증 발급 이력 저장
    	certificateService.insertCertificateHst(searchVO);
    	
    	return "/lms/certificate/certificate";
    }
    
    //재발급
    @RequestMapping("/lms/reCertificate.json")
    public void reCertificate(@ModelAttribute("searchVO") CertificateVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	String successYn = "Y";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setFrstRegisterId(user.getId());
    	}
    	
    	certificateService.reCertificate(searchVO);
    	
    	JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");

		jo.put("successYn", successYn);

		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
}
