package egovframework.com.lms.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.BoardVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.cop.bbs.service.EgovBBSManageService;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.CurriculumbaseService;
import egovframework.com.lms.service.CurriculumbaseVO;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.lms.service.impl.ZipUtils;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.uss.ivp.service.EgovIndvdlestbsService;
import egovframework.com.uss.ivp.service.IndvdlestbsVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;

/**
 * 
 * @author ywkim 
 * CurseRegMngController.java 
 * CurseRegistManage 수강신청관리         
 */

@Controller
public class CurseRegController {
	@Resource(name = "EgovBBSCtgryService")
    private EgovBBSCtgryService egovBBSCtgryService;

	/** EgovCmmUseService */
    @Resource(name="EgovCmmUseService")
	private EgovCmmUseService cmmUseService;
    
    @Resource(name = "curriculumService")
    private CurriculumService curriculumService;

    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;

    @Resource(name = "curriculumbaseService")
    private CurriculumbaseService curriculumbaseService;

    @Resource(name = "IndvdlestbsService")
    private EgovIndvdlestbsService indvdlestbsService;

    @Resource(name = "EgovBBSManageService")
	private EgovBBSManageService bbsMngService;

    @Resource(name = "EgovFileMngService")
    private EgovFileMngService fileService;

    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
    
    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "propertiesService")
	protected EgovPropertyService         propertyService;

	//전체 교육과정
	@RequestMapping(value = "/lms/crm/selectCurriculumAllList.do")
	public String selectCurriculumAllList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//캠퍼스
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		searchVO.setFirstIndex(0);
		searchVO.setLastIndex(10);
		searchVO.setRecordCountPerPage(10);
		
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if(user != null){
			searchVO.setUserSeCode(user.getUserSeCode());
			
			//학생, 교원  페이지 분기 처리
			//if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
				searchVO.setStudentPageAt("Y");
			//}

			//전체 교육과정에서는 폐기과정이 안나와야 하므로 따로 분기처리(관심교육과정에도 쿼리를 같이 쓰기 때문에(setStudentPageAt == 'Y') 분기처리를 여기만 따로 하는것임) - 0727교원도 전체에서는 안 나오게 추가
			if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode()) || "08".equals(user.getUserSeCode())) {
				searchVO.setStudentPageAt("curriculumAllList");
			}

	        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
	        model.addAttribute("resultList", curriculumList);
	
	        //나의 교육과정 개수 조회
	        int myCurriculumCnt = 0;
	      	CurriculumVO myCurriculumVO = new CurriculumVO();
	
	      	//학생, 교원  페이지 분기 처리
	  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	  			myCurriculumVO.setStudentPageAt("Y");
	  			myCurriculumVO.setMyCurriculumPageFlag("student");
	  			myCurriculumVO.setSttus("1");
	  		} else { //교원
	  			myCurriculumVO.setStudentPageAt("Y"); //교원 교육과정 메뉴도 개설예정 이후 부터 나와야 됨
	  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
	  		}
	  		
	
	      	myCurriculumVO.setSearchProcessSttusCodeDate("7");
	      	myCurriculumVO.setUserId(user.getId());
	        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
	        model.addAttribute("myCurriculumCnt", myCurriculumCnt);

	        return "/lms/crm/CurriculumAllList";
		}else{
			//searchVO.setStudentPageAt("Y");
			searchVO.setStudentPageAt("curriculumAllList");
			
			List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
	        model.addAttribute("resultList", curriculumList);
	        
	        searchVO.setRecentListAt("Y");
	        List<?> recentCurriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
			model.addAttribute("recentCurriculumList", recentCurriculumList);
	        
	        return "/lms/crm/CurriculumAllListNoLogin";
		}
	}

	//전체, 나의, 관심 교육과정 페이징
	@RequestMapping(value = "/lms/crm/CurriculumListAjax.do")
	public String curriculumAllListAjax(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//언어코드
		Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
    	voComCode.setCodeId("LMS30");
    	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
    	model.addAttribute("statusComCode", statusComCode);

    	//캠퍼스
		ctgry.setCtgrymasterId("CTGMST_0000000000017");
		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//학생, 교원  페이지 분기 처리
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		String myCurriculumPageFlag = "";
		if(user != null){
			searchVO.setStudentPageAt("Y"); //교원 교육과정 메뉴도 개설예정 이후 부터 나와야 됨
			if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
				//나의 교육과정
				if("myStudent".equals(request.getParameter("ajaxFlag"))) {
					myCurriculumPageFlag = "student";
				}
				
				//관심 교육과정
				if("wish".equals(request.getParameter("ajaxFlag"))) {
					searchVO.setWishCurriculumPageAt("Y");
					searchVO.setUserId(user.getId());
				}
				
				//전체 교육과정에서는 폐기과정이 안나와야 하므로 따로 분기처리(관심교육과정에도 쿼리를 같이 쓰기 때문에(setStudentPageAt == 'Y') 분기처리를 여기만 따로 하는것임)
				if("all".equals(request.getParameter("ajaxFlag"))) {
					searchVO.setStudentPageAt("curriculumAllList");
				}
			} else if("08".equals(user.getUserSeCode()) && "all".equals(request.getParameter("ajaxFlag"))) {
				searchVO.setStudentPageAt("curriculumAllList");
			} else if("08".equals(user.getUserSeCode())) {
				searchVO.setUserId(user.getId());
				myCurriculumPageFlag = "teacher";
			}
		}else{
			//myCurriculumPageFlag = "student";
			//searchVO.setStudentPageAt("Y");
			searchVO.setStudentPageAt("curriculumAllList");
		}
		
		searchVO.setMyCurriculumPageFlag(myCurriculumPageFlag);

  		//리스트 조회
		searchVO.setFirstIndex(searchVO.getPageIndex() * 10);
		searchVO.setLastIndex(10);
		searchVO.setRecordCountPerPage(10);

		//searchVO.setUserId(user.getId());
		//searchVO.setUserSeCode(user.getUserSeCode());
        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

		return "/lms/crm/CurriculumAllListAjax";
	}
	
	//전체 교육과정 > 수강신청승인에서 취소상태 수정
  	@RequestMapping(value = "/lms/crm/updateCurriculumCanCle.do")
  	public String updateCurriculumCanCle(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		String retStr = "forward:/lms/crm/CurriculumAllView.do";

      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}
      	
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	if(user != null){
      		searchVO.setLastUpdusrId(user.getId());
      	}else{
      		return "forward:/index.do";
      	}
      	
      	curriculumMemberService.updateCurriculumAccept(searchVO);
      	
      	model.addAttribute("searchVO", searchVO);
          
        request.getSession().removeAttribute("sessionVO");
          
        return retStr;
  	}
	
	//전체 교육과정 상세 페이지
	@RequestMapping(value = "/lms/crm/CurriculumAllView.do")
	public String curriculumAllView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//사이트 정보
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	    model.addAttribute("siteInfo", siteVO);

		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
      	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
      	
      	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
	  	voComCode.setCodeId("LMS30");
	  	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
	  	model.addAttribute("statusComCode", statusComCode);

      	//과정학습참고자료 및 기본과정 조회
    	CurriculumbaseVO curriculumbaseVO = new CurriculumbaseVO();
    	curriculumbaseVO.setCrclbId(searchVO.getCrclbId());
    	model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(curriculumbaseVO));
    	model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(curriculumbaseVO));
  	    
  	    //학습내용
    	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));
    	
    	//성적기준
    	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//교재 및 부교재
    	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
    	
    	//좋아요 목록
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	model.addAttribute("USER_INFO", user);
    	
    	if(user != null) {
    		IndvdlestbsVO indvdlestbsVO = new IndvdlestbsVO();
    		indvdlestbsVO.setUserId(user.getId());
    		indvdlestbsVO.setSiteId(siteVO.getSiteId());
    		indvdlestbsVO.setTrgetTyCode("CURRICULUM_LIKE");
    		model.addAttribute("wishList", indvdlestbsService.selectIvpList(indvdlestbsVO));
    	}
    	
    	//D-Day 구하기
      	model.addAttribute("applyDateDDay", EgovDateUtil.getDaysDiff(EgovDateUtil.getToday(), curriculumVO.getApplyEndDate().replace("-", "")));
      	
      	//취소/환불규정 조회
      	BoardVO boardVO = new BoardVO();
      	boardVO.setBbsId("BBSMSTR_000000000025");
      	boardVO.setNttNo(new BigDecimal(0));
      	model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

      	request.getSession().setAttribute("sessionVO", searchVO);
      	
      	//나의 교육과정 개수 조회
        int myCurriculumCnt = 0;
      	CurriculumVO myCurriculumVO = new CurriculumVO();

      	//학생, 교원  페이지 분기 처리
  		if(user == null || "02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			myCurriculumVO.setStudentPageAt("Y");
  			myCurriculumVO.setMyCurriculumPageFlag("student");
  			myCurriculumVO.setSttus("1");
  		} else { //교원
  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
  		}

      	myCurriculumVO.setSearchProcessSttusCodeDate("7");
      	if(user != null){
      		myCurriculumVO.setUserId(user.getId());
      		
      		//수강신청 했는지 개수 확인
            searchVO.setUserId(user.getId());
            int curriculumDuplicationMemberCnt = curriculumService.selectCurriculumDuplicationMemberCnt(searchVO);
          	model.addAttribute("curriculumDuplicationMemberCnt", curriculumDuplicationMemberCnt);
          	
          	//수강신청 했으면 해당 정보 조회
          	if(0 < curriculumDuplicationMemberCnt) {
          		model.addAttribute("curriculumMemberVO", curriculumService.selectCurriculumMemberDetail(searchVO));
          	}
      	}
        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
        model.addAttribute("myCurriculumCnt", myCurriculumCnt);
        
        //과정 신청 상태값을 조회하기 위하여 처리
      	CurriculumVO myCurriculumSttusVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("myCurriculumSttusVO", myCurriculumSttusVO);

		return "/lms/crm/CurriculumAllView";
	}
	
	//수강신청 약관 내용
    @RequestMapping("/lms/crm/terms.do")
    public String terms(HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		return "/lms/crm/Terms";
    }
	
	//첨부파일에 대한 목록을 조회한다.
    @RequestMapping("/lms/crm/selectFileInfs.do")
    public String selectFileInfs(@ModelAttribute("searchVO") FileVO fileVO, @RequestParam("param_atchFileId") String param_atchFileId, ModelMap model) throws Exception {
		String atchFileId = param_atchFileId;
	
		fileVO.setAtchFileId(atchFileId);
		List<FileVO> result = fileService.selectFileInfs(fileVO);
	
		model.addAttribute("fileList", result);
		model.addAttribute("updateFlag", "N");
		model.addAttribute("fileListCnt", result.size());
		model.addAttribute("atchFileId", atchFileId);
		model.addAttribute("webPath", propertiesService.getString("web.path"));
	
		return "/lms/crm/RefundFileList";
    }
    
    //수강신청 등록
    @RequestMapping("/lms/crm/insertCurriculumMember.do")
    public String insertCurriculumMember(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, final MultipartHttpServletRequest multiRequest) throws Exception {
	    List<FileVO> result = null;
	    String arrFileExt[] = {"ppt", "pptx", "doc", "docx", "xls", "xlsx", "hwp", "pdf", ""}; //기타파일은 필수가 아니기 떄문에 공백값도 있음.
	    String aplyFileExt  = "";
	    String planFileExt  = "";
	    String etfFileExt   = "";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setUserId(user.getId());
    		searchVO.setFrstRegisterId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}

    	//파일 업로드
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty() && "Y".equals(searchVO.getStdntAplyAt())) {
	    	result = fileUtil.directParseFileInf(files, "CURRICULUMALL_", 0, "CurriculumAll.fileStorePath", "");

	    	if(result != null) {
		    	for(int index=0; index < result.size(); index++) {
		    		FileVO file = result.get(index);
		    		if(file.getFormNm().startsWith("aplyFile")) {
		    			searchVO.setAplyFile(file.getStreFileNm());
		    			searchVO.setAplyOriFile(file.getOrignlFileNm());
		    			aplyFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("planFile")) {
		    			searchVO.setPlanFile(file.getStreFileNm());
		    			searchVO.setPlanOriFile(file.getOrignlFileNm());
		    			planFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("etfFile")) {
		    			searchVO.setEtfFile(file.getStreFileNm());
		    			searchVO.setEtfOriFile(file.getOrignlFileNm());
		    			etfFileExt = file.getFileExtsn();
		    		}
		    	}
	    	}
		    
	    	//확장자 체크
	    	if(!Arrays.asList(arrFileExt).contains(aplyFileExt)
	        || !Arrays.asList(arrFileExt).contains(planFileExt)
	        || !Arrays.asList(arrFileExt).contains(etfFileExt)) {
	    		model.addAttribute("requestMessage", "첨부하신 파일 확장자는 불가능합니다.");
	    		return "forward:/lms/crm/CurriculumAllView.do";
	    	}
	    	
	    	curriculumService.insertCurriculumMember(searchVO);
	    } else {
	    	curriculumService.insertCurriculumMember(searchVO);
	    }
	    
	    model.addAttribute("requestMessage", "수강 신청이 완료되었습니다.");
	    request.getSession().removeAttribute("sessionVO");

		return "forward:/lms/crm/CurriculumAllView.do";
    }
    
    //수강신청 수정
    @RequestMapping("/lms/crm/updateCurriculumMember.do")
    public String updateCurriculumMember(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, final MultipartHttpServletRequest multiRequest) throws Exception {
	    List<FileVO> result = null;
	    String arrFileExt[] = {"ppt", "pptx", "doc", "docx", "xls", "xlsx", "hwp", "pdf", ""}; //기타파일은 필수가 아니기 떄문에 공백값도 있음.
	    String aplyFileExt  = "";
	    String planFileExt  = "";
	    String etfFileExt   = "";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setUserId(user.getId());
    	}else{
    		return "forward:/index.do";
    	}

    	//파일 업로드
    	final Map<String, MultipartFile> files = multiRequest.getFileMap();
	    if(!files.isEmpty()) {
	    	result = fileUtil.directParseFileInf(files, "CURRICULUMALL_", 0, "CurriculumAll.fileStorePath", "");

	    	if(result != null) {
		    	for(int index=0; index < result.size(); index++) {
		    		FileVO file = result.get(index);
		    		if(file.getFormNm().startsWith("aplyFile")) {
		    			searchVO.setAplyFile(file.getStreFileNm());
		    			searchVO.setAplyOriFile(file.getOrignlFileNm());
		    			aplyFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("planFile")) {
		    			searchVO.setPlanFile(file.getStreFileNm());
		    			searchVO.setPlanOriFile(file.getOrignlFileNm());
		    			planFileExt = file.getFileExtsn();
		    		} else if(file.getFormNm().startsWith("etfFile")) {
		    			searchVO.setEtfFile(file.getStreFileNm());
		    			searchVO.setEtfOriFile(file.getOrignlFileNm());
		    			etfFileExt = file.getFileExtsn();
		    		}
		    	}
	    	}
		    
	    	//확장자 체크
	    	if(!Arrays.asList(arrFileExt).contains(aplyFileExt)
	        || !Arrays.asList(arrFileExt).contains(planFileExt)
	        || !Arrays.asList(arrFileExt).contains(etfFileExt)) {
	    		model.addAttribute("requestMessage", "첨부하신 파일 확장자는 불가능합니다.");
	    		return "forward:/lms/crm/CurriculumAllView.do";
	    	}
	    	
	    	curriculumService.updateCurriculumMember(searchVO);
	    }
	    
	    model.addAttribute("requestMessage", "재제출 신청이 완료되었습니다.");
	    request.getSession().removeAttribute("sessionVO");

		return "forward:/lms/crm/CurriculumAllView.do";
    }
    
	//중복 수강신청 조회
    @RequestMapping(value="/lms/crm/selectCurriculumDuplicationMemberCnt.json")
    public void CurriculumDuplicationMemberCntJson(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response)throws Exception {
    	String successYn = "Y";
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setUserId(user.getId());
    	}
    	
		searchVO.setFirstIndex(0);
		searchVO.setRecordCountPerPage(Integer.MAX_VALUE);
		
        JSONObject jo = new JSONObject();
  		response.setContentType("application/json;charset=utf-8");
  	
		jo.put("successYn", successYn);
		jo.put("curriculumDuplicationMemberCnt", curriculumService.selectCurriculumDuplicationMemberCnt(searchVO));
		
		PrintWriter printwriter = response.getWriter();
		printwriter.println(jo.toString());
		printwriter.flush();
		printwriter.close();
    }
    
	//나의 교육과정 리스트
  	@RequestMapping(value = "/lms/crm/selectMyCurriculumList.do")
  	public String selectMyCurriculumList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
      	voComCode.setCodeId("LMS30");
      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
      	model.addAttribute("statusComCode", statusComCode);
      	
      	//나의 교육과정 개수 조회
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

      	int myCurriculumCnt = 0;
      	CurriculumVO myCurriculumVO = new CurriculumVO();

      	//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			myCurriculumVO.setStudentPageAt("Y");
  			myCurriculumVO.setMyCurriculumPageFlag("student");
  			myCurriculumVO.setSttus("1");
  		} else { //교원
  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
  		}
  		
  		//나의 교육과정 개수 조회
  		myCurriculumVO.setSearchProcessSttusCodeDate("7");
      	myCurriculumVO.setUserId(user.getId());
        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
        model.addAttribute("myCurriculumCnt", myCurriculumCnt);
        
        //페이지 탭 설정(첫 진입시 진행중 과정이 있다면 "진행중 과정" 탭을 활성화
        if("Y".equals(request.getParameter("firstEnter")) && ("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode()))) {
        	if(0 < myCurriculumCnt) {
        		model.addAttribute("tabStep", 2);
        		searchVO.setSearchProcessSttusCodeDate("7");
        	} else {
        		model.addAttribute("tabStep", 1);
        	}
        } else if("Y".equals(request.getParameter("firstEnter"))) {
        	model.addAttribute("tabStep", 1);
        }
        
      	//캠퍼스
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		searchVO.setFirstIndex(0);
  		searchVO.setLastIndex(10);
  		searchVO.setRecordCountPerPage(10);
  		
  		//리스트 탭메뉴 분기처리
      	if("Y".equals(request.getParameter("curriculumIngFlag"))) {
      		Integer[] arrProcessSttusCodeDate = {3,4,6};
      		searchVO.setArrProcessSttusCodeDate(arrProcessSttusCodeDate);
      	}

  		//리스트 조회
  		searchVO.setUserId(user.getId());
  		searchVO.setUserSeCode(user.getUserSeCode());

  		//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setStudentPageAt("Y");
  			searchVO.setSearchSttus("5");
  			searchVO.setMyCurriculumPageFlag("student");
  		} else if("08".equals(user.getUserSeCode())) {
  			//년도
  			model.addAttribute("yearList", new LmsCommonUtil().getYear());
  			
  			//학기
  	    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
  			model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  			
  			searchVO.setMyCurriculumPageFlag("teacher");
  		}

        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        //학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			return "/lms/crm/MyCurriculumList";
  		} else {
  			return "/lms/crm/MyTeacherCurriculumList";
  		}
  	}
  	
	//나의 교육과정 상세 페이지
  	@RequestMapping(value = "/lms/crm/myCurriculumView.do")
  	public String myCurriculumView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  		//사이트 정보
      	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
    	model.addAttribute("siteInfo", siteVO);

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	searchVO.setUserId(user.getId());
      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);
        model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
        	
        //과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
  	  	voComCode.setCodeId("LMS30");
  	  	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
  	  	model.addAttribute("statusComCode", statusComCode);

  	  	//나의 교육과정 개수 조회
      	int myCurriculumCnt = 0;
      	CurriculumVO myCurriculumVO = new CurriculumVO();
      	String teacherYn = "N";
      	
      	//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			myCurriculumVO.setStudentPageAt("Y");
  			myCurriculumVO.setMyCurriculumPageFlag("student");
  			myCurriculumVO.setSttus("1");
  		} else { //교원
  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
  			teacherYn = "Y";
  		}

      	myCurriculumVO.setSearchProcessSttusCodeDate("7");
      	myCurriculumVO.setUserId(user.getId());
        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
        model.addAttribute("myCurriculumCnt", myCurriculumCnt);
        
        
      	if(user != null){
      		//myCurriculumVO.setUserId(user.getId());
      		
      		//수강신청 했는지 개수 확인
            searchVO.setUserId(user.getId());
            int curriculumDuplicationMemberCnt = curriculumService.selectCurriculumDuplicationMemberCnt(searchVO);
          	model.addAttribute("curriculumDuplicationMemberCnt", curriculumDuplicationMemberCnt);
          	
          	//수강신청 했으면 해당 정보 조회
          	if(0 < curriculumDuplicationMemberCnt) {
          		model.addAttribute("curriculumMemberVO", curriculumService.selectCurriculumMemberDetail(searchVO));
          	}
      	}
        

  	  	//시간표 개수 조회(jsp 파일 분기처리용)
  		/*ScheduleMngVO scheduleMngVO = new ScheduleMngVO();
  		scheduleMngVO.setCrclId(searchVO.getCrclId());
  		int crclScheduleTotCnt = scheduleMngService.selectCrclScheduleTotCnt(scheduleMngVO);*/

        //등록학생 수
        CurriculumVO curriculumMemberVO = new CurriculumVO();
        curriculumMemberVO.setCrclId(searchVO.getCrclId());
        curriculumMemberVO.setSearchSttus("1");
        model.addAttribute("registerCnt", curriculumMemberService.selectCurriculumMemberTotCnt(curriculumMemberVO));
        
        //계정 정보
        model.addAttribute("USER_INFO", user);
  		
  		//if(("Y".equals(curriculumVO.getTotalTimeAt()) && "7".equals(curriculumVO.getProcessSttusCodeDate()))) { //교원은 바로 시간표만 나옴.
        if(("Y".equals(curriculumVO.getTotalTimeAt())) && ("Y".equals(teacherYn) || ("N".equals(teacherYn) && "7".equals(curriculumVO.getProcessSttusCodeDate())))){
  			ScheduleMngVO scheduleMngVO = new ScheduleMngVO();

  			scheduleMngVO.setCrclId(searchVO.getCrclId());

  			if("Y".equals(request.getParameter("todayFlag"))) {
  				scheduleMngVO.setStartDt(EgovDateUtil.getToday());
  			}
  			
  			//학생 출석여부
  	  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  	  			scheduleMngVO.setSearchUserId(user.getId());
  	  		}
  			//과정 시간표
  			model.addAttribute("resultList", scheduleMngService.selectCrclSchedule(scheduleMngVO));
  			
  			//등록 된 시간표 담당교원
  	    	List facPlList = scheduleMngService.selectFacultyList(scheduleMngVO);
  	    	model.addAttribute("facPlList", facPlList);

  	    	//오늘일자
  	    	model.addAttribute("today", EgovDateUtil.getToday());

  			return "/lms/crm/MyCurriculumScheduleView";
  		} else {
  			//과정학습참고자료 및 기본과정 조회
  	      	CurriculumbaseVO curriculumbaseVO = new CurriculumbaseVO();
  	      	curriculumbaseVO.setCrclbId(searchVO.getCrclbId());
  	      	model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(curriculumbaseVO));
  	      	model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(curriculumbaseVO));
  	    	    
  	    	//학습내용
  	      	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));
  	      	
  	      	//과정에 등록된 교원
  	      	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));
  	      	
  	      	//성적기준
  	      	Ctgry ctgry = new Ctgry();
  	  		ctgry.setCtgrymasterId("CTGMST_0000000000011");
  	  		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  	  		//교재 및 부교재
  	      	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
  	      	
  	      	//좋아요 목록
  	      	model.addAttribute("USER_INFO", user);
  	      	
  	  		IndvdlestbsVO indvdlestbsVO = new IndvdlestbsVO();
  	  		indvdlestbsVO.setUserId(user.getId());
  	  		indvdlestbsVO.setSiteId(siteVO.getSiteId());
  	  		indvdlestbsVO.setTrgetTyCode("CURRICULUM_LIKE");
  	  		model.addAttribute("wishList", indvdlestbsService.selectIvpList(indvdlestbsVO));
  	    	
  	    	//취소/환불규정 조회
  	    	BoardVO boardVO = new BoardVO();
  	    	boardVO.setBbsId("BBSMSTR_000000000025");
  	    	boardVO.setNttNo(new BigDecimal(0));
  	    	model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

  	    	request.getSession().setAttribute("sessionVO", searchVO);

  	  		return "/lms/crm/MyCurriculumView";
  		}
  	}
  	
	//관심 교육과정
  	@RequestMapping(value = "/lms/crm/selectWishCurriculumList.do")
  	public String selectWishCurriculumList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
      	voComCode.setCodeId("LMS30");
      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
      	model.addAttribute("statusComCode", statusComCode);

      	//캠퍼스
  		ctgry.setCtgrymasterId("CTGMST_0000000000017");
  		model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		searchVO.setFirstIndex(0);
  		searchVO.setLastIndex(10);
  		searchVO.setRecordCountPerPage(10);
  		
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		searchVO.setUserId(user.getId());
  		searchVO.setUserSeCode(user.getUserSeCode());
  		//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setStudentPageAt("Y");
  		}
  		searchVO.setWishCurriculumPageAt("Y");
        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        //나의 교육과정 개수 조회
        int myCurriculumCnt = 0;
      	CurriculumVO myCurriculumVO = new CurriculumVO();

      	//학생, 교원  페이지 분기 처리
  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			myCurriculumVO.setStudentPageAt("Y");
  			myCurriculumVO.setSttus("1");
  		} else { //교원
  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
  		}

      	myCurriculumVO.setSearchProcessSttusCodeDate("7");
      	myCurriculumVO.setUserId(user.getId());
        myCurriculumCnt = curriculumService.selectCurriculumListTotCnt(myCurriculumVO, request, response);
        model.addAttribute("myCurriculumCnt", myCurriculumCnt);		

  		return "/lms/crm/WishCurriculumList";
  	}
  	
  	//교원 수강신청
  	@RequestMapping(value = "/lms/crm/selectCurseregManage.do")
  	public String selectCurseregManageList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		Ctgry ctgry = new Ctgry();
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);

  		//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());

		//학기
    	ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
      	voComCode.setCodeId("LMS30");
      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
      	model.addAttribute("statusComCode", statusComCode);
      	
      	//취소/환불규정 조회
      	BoardVO boardVO = new BoardVO();
      	boardVO.setBbsId("BBSMSTR_000000000025");
      	boardVO.setNttNo(new BigDecimal(0));
      	model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));

      	/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));
    	
    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		searchVO.setUserSeCode(user.getUserSeCode());
		searchVO.setUserId(user.getId());

		//PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 안함
		searchVO.setProcessSttusCodeZeroAt("N");
		
		//언어코드 가져오기 - 주관기관에서 언어로 변경 
		if("08".equals(user.getUserSeCode())) {
			//searchVO.setSearchHostCode(user.getMngDeptCode());
			searchVO.setSearchCrclLang(user.getMajor());
		}
		
        List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);

        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
        paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        //오늘일자
    	model.addAttribute("today", EgovDateUtil.getToday());

  		return "/lms/crm/CurseregManage";
  	}

  	//수강신청 > 과정계획서
  	@RequestMapping(value = "/lms/crm/curriculumManage.do")
  	public String selectCurriculumManage(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);
    	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
    	
    	//과정 담당자 인지 확인
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
    	
    	request.getSession().setAttribute("sessionVO", searchVO);
        	
  		return "/lms/crm/curseregManageView";
  	}

  	//수강신청 > 수강신청 화면
  	@RequestMapping(value = "/lms/crm/curriculumRegister.do")
  	public String selectCurriculumRegister(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//사이트 정보
    	SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
  	    model.addAttribute("siteInfo", siteVO);

		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
      	model.addAttribute("expense", curriculumService.selectCurriculumExpense(searchVO));
      	
      	//과정진행상태 - 공통코드(LMS30)
		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
	   	voComCode = new ComDefaultCodeVO();
	  	voComCode.setCodeId("LMS30");
	  	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
	  	model.addAttribute("statusComCode", statusComCode);

      	//과정학습참고자료 및 기본과정 조회
    	CurriculumbaseVO curriculumbaseVO = new CurriculumbaseVO();
    	curriculumbaseVO.setCrclbId(searchVO.getCrclbId());
    	model.addAttribute("curriculumbaseVO", curriculumbaseService.selectCurriculumbase(curriculumbaseVO));
    	model.addAttribute("refFileList", curriculumbaseService.selectCurriculumbaseFile(curriculumbaseVO));
  	    
  	    //학습내용
    	model.addAttribute("lessonList", curriculumService.selectCurriculumLesson(searchVO));
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(curriculumVO));
    	
    	//성적기준
    	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

		//교재 및 부교재
    	model.addAttribute("bookList", curriculumService.selectBookbbs(searchVO));
    	
    	//D-Day 구하기
      	model.addAttribute("applyDateDDay", EgovDateUtil.getDaysDiff(EgovDateUtil.getToday(), curriculumVO.getApplyEndDate().replace("-", "")));
      	
      	//취소/환불규정 조회
      	BoardVO boardVO = new BoardVO();
      	boardVO.setBbsId("BBSMSTR_000000000025");
      	boardVO.setNttNo(new BigDecimal(0));
      	model.addAttribute("board", bbsMngService.selectBoardArticle(boardVO));
      	
      	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        return "/lms/crm/curriculumRegisterView";
  	}
  	
  	//수강신청 > 수강신청 승인
  	@RequestMapping(value = "/lms/crm/curriculumAccept.do")
  	public String selectCurriculumAccept(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//로그인 회원정보
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("USER_INFO", user);
  		
  		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());
    	/*
    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));
		*/
    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//수강신청 회원 리스트 조회
		List<?> curriculumMemberList = curriculumMemberService.selectCurriculumMemberList(searchVO);
		model.addAttribute("curriculumMemberList", curriculumMemberList);
		
		//수강신청 회원 총개수
		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
		model.addAttribute("totCnt", totCnt);
		
		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
		
      	request.getSession().setAttribute("sessionVO", searchVO);
      	
  		return "/lms/crm/curriculumAcceptView";
  	}
  	
	//수강신청관리 > 수강신청승인에서 승인상태 수정
  	@RequestMapping(value = "/lms/crm/updateCurriculumAccept.do")
  	public String updateCurriculumAccept(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		String retStr = "forward:/lms/crm/curriculumAccept.do?subtabstep=" + request.getParameter("subtabstep");

      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}
      	
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	if(user != null){
      		searchVO.setLastUpdusrId(user.getId());
      	}else{
      		return "forward:/index.do";
      	}
      	
      	curriculumMemberService.updateCurriculumAccept(searchVO);
      	
      	model.addAttribute("searchVO", searchVO);
          
        request.getSession().removeAttribute("sessionVO");
          
        return retStr;
  	}

  	//수강신청관리 > 과정계획서 수강신청 조기마감
  	@RequestMapping(value = "/lms/crm/updateProcessSttusCode.do")
  	public String updateProcessSttusCode(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
  		String retStr = "redirect:" + request.getParameter("forwardUrl"); // + "?subtabstep=" + request.getParameter("subtabstep")
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}
      	
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	if(user != null){
      		searchVO.setLastUpdusrId(user.getId());
      	}else{
      		return "forward:/index.do";
      	}
      	
      	curriculumService.updateCurriculum(searchVO);
        status.setComplete();
        
        model.addAttribute("searchVO", searchVO);
          
        request.getSession().removeAttribute("sessionVO");
          
        return retStr;
  	}

  	//수강신청관리 > 수강대상자 확정 > 전체명단
  	@RequestMapping(value = "/lms/crm/curriculumStudent.do")
  	public String selectCurriculumStudent(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);
      	
      	//수강대상자 통계 조회
      	List<?> curriculumMemberStatsList = curriculumMemberService.curriculumMemberStatsList(searchVO);
  		model.addAttribute("curriculumMemberStatsList", curriculumMemberStatsList);

  		//수강대상자 총 합계
  		searchVO.setSearchSttus("1");  		
  		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);
  		
      	//수강 대상자 조회
  		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
  		model.addAttribute("selectStudentList", selectStudentList);
  		
  		if("Y".equals(searchVO.getZipFileAt())){
      		SiteManageVO siteVO = siteManageService.selectSiteServiceInfo(request);
      		model.addAttribute("siteInfo", siteVO);
      		
      		return "forward:/lms/crm/downZipFile.do";
      	}
  		
  		//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
  		
        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crm/curriculumStudentView";
  	}
  	
  	//학생 신청자료 압축파일
  	@RequestMapping(value="/lms/crm/downZipFile.do")
	public void downZipFile(HttpServletRequest request,HttpServletResponse response){
  		/***커리큘럼정보*****/
  		CurriculumVO curriculumVO = (CurriculumVO) request.getAttribute("curriculumVO");
  		
		String filePath = propertyService.getString("CurriculumAll.fileStorePath") + File.separator;
		String tempPath = propertyService.getString("CurriculumAll.fileStorePath") + "/temp/" + curriculumVO.getCrclId();
		
		try {
			/***학생목록*****/
			List<EgovMap> selectStudentList = (List<EgovMap>)request.getAttribute("selectStudentList");

			String[] fileNmArr = {"aplyFile","planFile","etcFile"};
			String[] fileKoNmArr = {"aplyOriFile","planOriFile","etcOriFile"};
			 try {
			      for(EgovMap map : selectStudentList){
			         String mkdirNm = tempPath + File.separator + (String)map.get("userNm") + "_" + (String)map.get("brthdy");
			         
			         File folder = new File(mkdirNm);
			         folder.mkdirs();
			         for(int i = 0; i < fileNmArr.length; i++){
			            if(map.get(fileNmArr[i])!=null){
			               File file = new File(filePath,(String)map.get(fileNmArr[i]));
			               //File outfile = new File(mkdirNm,(String)map.get(fileNmArr[i]));
			               File outfile = new File(mkdirNm,(String)map.get(fileKoNmArr[i]));
			                  
		                   FileInputStream in = null;
		                   FileOutputStream out = null;
			                  
			               try {
		                     in = new FileInputStream(file);
		                     out = new FileOutputStream(outfile);
		                     byte[] b = new byte[1024];
		                     int cnt = 0;
		                     while((cnt=in.read(b)) != -1){
			                        out.write(b, 0, cnt);
		                     }
		                  }catch(Exception e) {
		                     e.printStackTrace();
		                  }finally{
		                     in.close();
		                     out.close();
		                  }
			            }
			         }
			      }
			      
			      //압축파일 생성
			      String OUTPUT_ZIP_FILE = curriculumVO.getCrclNm()+".zip";
			      String SOURCE_FOLDER = tempPath; // SourceFolder path
			      ZipUtils appZip = new ZipUtils();
			      appZip.zipProcess(OUTPUT_ZIP_FILE, SOURCE_FOLDER);
			      
			      String fileNameOrg ="";
			      String header = request.getHeader("User-Agent");
			      response.setContentType("application/zip");
			      if(header.contains("MSIE") || header.contains("Trident")) {
			            fileNameOrg = URLEncoder.encode(curriculumVO.getCrclNm(),"UTF-8").replaceAll("\\+", "%20");
			            response.setHeader("Content-Disposition", "attachment;filename=" + fileNameOrg + ".zip;");
			      }else{
			           fileNameOrg = new String(curriculumVO.getCrclNm().getBytes("UTF-8"), "ISO-8859-1");
			           response.addHeader("Content-Disposition", "attachment; filename="+ fileNameOrg +".zip");
			      }

			      FileInputStream fis=new FileInputStream(OUTPUT_ZIP_FILE);
			      BufferedInputStream bis=new BufferedInputStream(fis);
			      ServletOutputStream so=response.getOutputStream();
			      BufferedOutputStream bos=new BufferedOutputStream(so);

			      byte[] data=new byte[2048];
			      int input=0;

			      while((input=bis.read(data))!=-1){
			          bos.write(data,0,input);
			          bos.flush();
			      }

			      if(bos!=null) bos.close();
			      if(bis!=null) bis.close();
			      if(so!=null) so.close();
			      if(fis!=null) fis.close();
			      appZip.deleteDir(tempPath);
			      File zipFile = new File(OUTPUT_ZIP_FILE);
			      zipFile.delete();
			      
			} catch (java.io.IOException e) {
				e.printStackTrace();
				ZipUtils appZip = new ZipUtils();
				appZip.deleteDir(tempPath);
			}
		}catch (Exception e) {
			e.printStackTrace();
			ZipUtils appZip = new ZipUtils();
			appZip.deleteDir(tempPath);
		} 
	}
  	
	//수강신청관리 > 수강대상자 확정 > 조배정 뷰 페이지
  	@RequestMapping(value = "/lms/crm/curriculumGroupView.do")
  	public String selectCurriculumGroupView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//로그인 회원정보
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("USER_INFO", user);
  		
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);
  		
      	//수강 그룹 조회(반,조)
  		List<?> selectGroupList = curriculumMemberService.selectGroupList(searchVO);
  		model.addAttribute("selectGroupList", selectGroupList);

  		//선택된 수강 학생 조회
  		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
  		model.addAttribute("pickStudentList", selectPickStudentList);
  		
  		//조의 개수 리스트를 조회
  		List<?>  groupCntList = curriculumMemberService.selectCurriculumGroupCntList(searchVO);
  		model.addAttribute("groupCntList", groupCntList);

  		//수강 대상자 조회
		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
		model.addAttribute("selectStudentList", selectStudentList);
		
		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);

        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crm/curriculumGroupView";
  	}
  	
  	//수강신청관리 > 수강대상자 확정 > 조배정 확정
  	@RequestMapping(value = "/lms/crm/updateCurriculumGroup.do")
  	public String updateCurriculumGroup(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
  		String retStr = "forward:/lms/crm/curriculumGroupView.do";
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}
      	
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	if(user != null){
      		searchVO.setLastUpdusrId(user.getId());
      	}else{
      		return "forward:/index.do";
      	}
      	
      	curriculumMemberService.updateCurriculumGroup(searchVO);
      	status.setComplete();
          
          request.getSession().removeAttribute("sessionVO");
          
          return retStr;
  	}

  	//수강신청관리 > 수강대상자 확정 > 반배정 뷰 페이지
  	@RequestMapping(value = "/lms/crm/curriculumClassView.do")
  	public String selectCurriculumClassView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//로그인 회원정보
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		model.addAttribute("USER_INFO", user);

  		//책임교원
    	CurriculumVO curriculum = new CurriculumVO();
    	curriculum.setCrclId(searchVO.getCrclId());

    	//책임교원 코드 10
    	curriculum.setManageCode("10");
    	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

    	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
    	model.addAttribute("curriculumVO", curriculumVO);
    	
    	//과정에 등록된 교원
    	model.addAttribute("facList", curriculumService.selectCurriculumFacultyDp(searchVO));

    	//반 정보 조회
		List<?> selectCurriculumClassList = curriculumMemberService.selectCurriculumClassList(searchVO);
		model.addAttribute("curriculumClassList", selectCurriculumClassList);

    	//반 개수 조회
		int classCnt = curriculumMemberService.selectCurriculumClassTotCnt(searchVO);
		model.addAttribute("classCnt", classCnt);
		
		//선택된 수강 학생 조회
		List<?> selectPickStudentList = curriculumMemberService.selectPickStudentList(searchVO);
		model.addAttribute("pickStudentList", selectPickStudentList);
		
		//과정 담당자 인지 확인
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
		
      	request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crm/curriculumClassView";
  	}

  	//수강신청관리 > 수강대상자 확정 > 반배정 확정
  	@RequestMapping(value = "/lms/crm/updateCurriculumClass.do")
  	public String updateCurriculumClass(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
  		String retStr = "forward:/lms/crm/curriculumClassView.do";
      	if(request.getSession().getAttribute("sessionVO") == null) {
  			return retStr;
  		}
      	
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	if(user != null){
      		searchVO.setLastUpdusrId(user.getId());
      	}else{
      		return "forward:/index.do";
      	}
      	
      	curriculumMemberService.updateCurriculumClass(searchVO);
      	status.setComplete();
          
          request.getSession().removeAttribute("sessionVO");
          
          return retStr;
  	}
  	
  	//수강대상자 확정 승인
  	@RequestMapping(value = "/lms/crm/curseregManageConfirmList.do")
  	public String curseregManageConfirmList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
  		Ctgry ctgry = new Ctgry();
  		
  		//언어코드
		ctgry.setCtgrymasterId("CTGMST_0000000000002");
		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	//과정진행상태 - 공통코드(LMS30)
  		ComDefaultCodeVO voComCode = new ComDefaultCodeVO();
  	   	voComCode = new ComDefaultCodeVO();
      	voComCode.setCodeId("LMS30");
      	List<CmmnDetailCode> statusComCode = cmmUseService.selectCmmCodeDetail(voComCode);
      	model.addAttribute("statusComCode", statusComCode);

      	//학기
      	ctgry.setCtgrymasterId("CTGMST_0000000000016");
  		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));

      	//언어
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("langList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
  		//주관기관
  		ctgry.setCtgrymasterId("CTGMST_0000000000009");
  		model.addAttribute("insList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
      	
      	//년도
      	model.addAttribute("yearList", new LmsCommonUtil().getYear());
      	
      	/** EgovPropertyService.sample */
      	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
      	searchVO.setPageSize(propertiesService.getInt("pageSize"));

      	/** pageing */
      	PaginationInfo paginationInfo = new PaginationInfo();
  		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
  		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
  		paginationInfo.setPageSize(searchVO.getPageSize());
  		
  		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
  		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
  		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

  		//PROCESS_STTUS_CODE 0(관리자 등록 확정 대기) 상태 사용 안함
  		searchVO.setProcessSttusCodeZeroAt("N");
  		
  		//주관기관 가져오기 
  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if("08".equals(user.getUserSeCode())) {
			//searchVO.setSearchHostCode(user.getMngDeptCode());
			searchVO.setSearchCrclLang(user.getMajor());
		}

      	List<?> curriculumList = curriculumService.selectCurriculumList(searchVO, request, response);
        model.addAttribute("resultList", curriculumList);
          
        int totCnt = curriculumService.selectCurriculumListTotCnt(searchVO, request, response);
  		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);

  		return "/lms/crm/curseregManageConfirmList";
  	}
  	
  	//교육과정 수강료 납부 확인/환불
  	@RequestMapping(value = "/lms/crm/curseregManageConfirmView.do")
  	public String curseregManageConfirmView(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response, SessionStatus status) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);
      	
      	/** EgovPropertyService.sample */
      	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
      	searchVO.setPageSize(propertiesService.getInt("pageSize"));

      	/** pageing */
      	PaginationInfo paginationInfo = new PaginationInfo();
  		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
  		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
  		paginationInfo.setPageSize(searchVO.getPageSize());
  		
  		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
  		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
  		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
  		
      	//교육과정 수강료 납부 확인/환불 리스트 조회
  		List<?> curriculumMemberList = curriculumMemberService.selectCurriculumMemberList(searchVO);
  		model.addAttribute("curriculumMemberList", curriculumMemberList);

  		//교육과정 수강료 납부 확인/환불 총개수
  		int totCnt = curriculumMemberService.selectCurriculumMemberTotCnt(searchVO);
  		model.addAttribute("totCnt", totCnt);

  		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
  		
  		//학생 상태별 개수 조회
  		List<?> memberSttusCntList = curriculumMemberService.selectMemberSttusCntList(searchVO);
        model.addAttribute("memberSttusCntList", memberSttusCntList);
  		
        request.getSession().setAttribute("sessionVO", searchVO);

  		return "/lms/crm/curseregManageConfirmView";
  	}

  	//최근 접수가 시작된 수강신청
  	@RequestMapping(value = "/lms/crm/latestCurriculumList.do")
  	public String latestCurriculumList(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		return null;
  	}
  	
  	//교육과정 카드 형 Ajax
  	@RequestMapping(value = "/lms/crm/curriculumCardListAjax.do")
  	public String selectCurriculumCardListAjax(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		
  		CurriculumVO currVO = new CurriculumVO();
		//currVO.setStudentPageAt("Y");
  		currVO.setStudentPageAt("curriculumAllList");
		currVO.setFirstIndex(0);
		currVO.setLastIndex(8);
		currVO.setRecordCountPerPage(8);
		currVO.setSearchCrclLang(searchVO.getSearchCrclLang());
		List<?> curriculumList = curriculumService.selectCurriculumList(currVO, request, response);
		List<EgovMap> crclList = (List<EgovMap>) curriculumList;
		for(int i = 0; i < curriculumList.size(); i++){
			EgovMap map = (EgovMap) curriculumList.get(i);
			String crclId = map.get("crclId").toString();
			currVO.setCrclId(crclId);
			
			crclList.get(i).put("facList", curriculumService.selectCurriculumFacultyDp(currVO));
		}
        model.addAttribute("resultList", crclList);
  		
  		return "/lms/crm/curriculumCardListAjax";
  	}
  	
    //오늘의 수업 카드 형 Ajax
  	@RequestMapping(value = "/lms/crm/todayCardListAjax.do")
  	public String selectTodayCardListAjax(@ModelAttribute("searchVO") CurriculumVO searchVO,ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

  		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
  		 CurriculumVO myCurriculumVO = new CurriculumVO();
		  myCurriculumVO.setFacId(user.getId());
		  myCurriculumVO.setStartDate(searchVO.getStartDate());
		  myCurriculumVO.setIsMainFlag("Y");
		//학생, 교원  페이지 분기 처리
	  		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
	  			myCurriculumVO.setMyCurriculumPageFlag("student");
	  		} else { //교원
	  			myCurriculumVO.setMyCurriculumPageFlag("teacher");
	  		}
		
		  List<?> tmpTodayCrclList = curriculumService.selectTodayCrclList(myCurriculumVO);
		  List<EgovMap> todayCrclList = (List<EgovMap>) tmpTodayCrclList;
			for(int i = 0; i < tmpTodayCrclList.size(); i++){
				EgovMap map = (EgovMap) tmpTodayCrclList.get(i);
				String crclId = map.get("crclId").toString();
				String plId = map.get("plId").toString();
				ScheduleMngVO sVO = new ScheduleMngVO();
				sVO.setCrclId(crclId);
				sVO.setPlId(plId);
				
				todayCrclList.get(i).put("facPlList", scheduleMngService.selectFacultyList(sVO));
			}
  		
		  
		  model.addAttribute("myCurriculumList", todayCrclList);

		  model.addAttribute("yesterDate", EgovDateUtil.convertDate(EgovDateUtil.addDay(searchVO.getStartDate(), -1), "0000", "yyyy-MM-dd"));
		  model.addAttribute("tomorrowDate", EgovDateUtil.convertDate(EgovDateUtil.addDay(searchVO.getStartDate(), 1), "0000", "yyyy-MM-dd"));
		  model.addAttribute("today", EgovDateUtil.convertDate(searchVO.getStartDate().replaceAll("-", ""), "0000", "yyyy년 MM월 dd일"));
	      String dayNm = EgovDateUtil.getDateDay(myCurriculumVO.getStartDate().replaceAll("-", ""));
	      model.addAttribute("dayNm", dayNm); // 요일
	      
	      SimpleDateFormat sDate = new SimpleDateFormat("yyyy-MM-dd");
		  String realToday = sDate.format(new Date());
		  if(realToday.equals(searchVO.getStartDate())){
			  model.addAttribute("dayFlag", "TODAY");
		  }else if(realToday.compareTo(searchVO.getStartDate()) > 0){
			  model.addAttribute("dayFlag", "YESTERDAY");
		  }else if(realToday.compareTo(searchVO.getStartDate()) < 0){
			  model.addAttribute("dayFlag", "TOMORROW");
		  }
		  
		model.addAttribute("crclDay", searchVO.getStartDate());
		  
		//캠퍼스
        Ctgry ctgry = new Ctgry();
        ctgry.setCtgrymasterId("CTGMST_0000000000017");
        model.addAttribute("campusList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
        
        model.addAttribute("userSeCode", user.getUserSeCode());
  		
  		return "/lms/crm/todayCardListAjax";
  	}
  	
  	//과정관리 > 과제 > 과제  > 과제 등록
  	@RequestMapping(value = "/mng/lms/crcl/homeworkRegister.do")
  	public String selectMngCurriculumRegister(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
  		//책임교원
      	CurriculumVO curriculum = new CurriculumVO();
      	curriculum.setCrclId(searchVO.getCrclId());

      	//책임교원 코드 10
      	curriculum.setManageCode("10");
      	model.addAttribute("subUserList", curriculumService.selectCurriculumMng(curriculum));

      	CurriculumVO curriculumVO = curriculumService.selectCurriculum(searchVO);
      	model.addAttribute("curriculumVO", curriculumVO);

      	if(!EgovStringUtil.isEmpty(searchVO.getHwId())) {
      		CurriculumVO homeworkVO = curriculumService.selectHomeworkArticle(searchVO);
          	model.addAttribute("homeworkVO", homeworkVO);
      	}

      	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map = (EgovMap) subUserList.get(i);
          		String userId = map.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
      	model.addAttribute("curriculum", curriculum);
      	model.addAttribute("BbsFileStoreWebPathByWebFile", propertyService.getString("publish.tmplat.bbs.fileStoreWebPathByWebFile"));

      	//조별과제 분기를 위한 반 개수 조회
      	model.addAttribute("groupCnt", curriculumService.selectCurriculumMemberGroupCnt(searchVO));

      	//회원 이미지 경로
      	model.addAttribute("MembersFileStoreWebPath", propertiesService.getString("Members.fileStoreWebPath"));

      	request.getSession().setAttribute("sessionVO", searchVO);

        return "/mng/lms/crcl/HomeworkRegister";
  	}
}
