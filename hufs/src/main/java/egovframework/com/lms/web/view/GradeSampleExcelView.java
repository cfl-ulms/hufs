package egovframework.com.lms.web.view;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import egovframework.com.cmm.view.ExcelView;
import egovframework.rte.psl.dataaccess.util.EgovMap;

public class GradeSampleExcelView extends ExcelView {
	@Override
	protected void buildExcelDocument(final Map<String, Object> map, final HSSFWorkbook hssfWorkbook, final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws Exception {
		String excelDownType = map.get("excelDownType").toString();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String today = formatter.format(new java.util.Date());
		
		this.setContentType("application/msexcel");
		httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\""+ today+"_"+excelDownType +".xls\"");

		HSSFSheet sheet = null;
		HSSFRow row_1 = null;
		HSSFRow row_2 = null;
		HSSFRow row_3 = null;
		HSSFRow dataRow = null;
		HSSFCell cell_1 = null;
		HSSFCell cell_2 = null;
		HSSFCell cell_3 = null;
		
		this.documentTitleStyle = this.getStyleDocumentTitle(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());
		this.titleStyle = this.getStyleTitle(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());
		this.titleTextStyle = this.getStyleTitleText(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());
		
		HSSFPalette palette = hssfWorkbook.getCustomPalette();
		HSSFColor titleColor = palette.findSimilarColor(211, 211, 211);
		short palIndex = titleColor.getIndex();
		this.titleStyle.setFillForegroundColor(palIndex);				
		
		this.columnTextStyle = this.getStyleColumnText(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());
		this.columnTextCenterStyle = this.getStyleColumnTextCenter(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());
		this.columnTextRightStyle = this.getStyleColumnTextRight(hssfWorkbook.createCellStyle(), hssfWorkbook.createFont());

		if(excelDownType.equals("gradeSample")){
			gradeSampleGenerator(hssfWorkbook,sheet,map,row_1,cell_1,dataRow);
		}
	}
	
	//로그 현황
	public void gradeSampleGenerator(HSSFWorkbook hssfWorkbook, HSSFSheet sheet, Map<String, Object> map, HSSFRow row_1, HSSFCell cell_1, HSSFRow dataRow){
		
		@SuppressWarnings("unchecked")
		List<EgovMap> list = (List<EgovMap>) map.get("resultList");
		
		for(int i = 0; i < list.size(); i++){
			int mergeSize = 1;
			int startRowSize = 1;
			short defaultHeight = 300;
			int loopCount = 1;
			
			sheet = hssfWorkbook.createSheet();
			hssfWorkbook.setSheetName(i, list.get(i).get("title").toString());
			
			@SuppressWarnings("unchecked")
			List<EgovMap> listData = (List<EgovMap>) list.get(i).get("resultList");

			row_1 = sheet.createRow(loopCount++);
			row_1.setHeight(defaultHeight);
			
			//상단 타이틀 메뉴
			sheet.addMergedRegion(new CellRangeAddress(startRowSize, mergeSize, 0, 0));
			cell_1 = row_1.createCell(0);
			sheet.setColumnWidth(0, 3000);
			cell_1.setCellValue(new HSSFRichTextString("순번"));
			cell_1.setCellStyle(this.titleStyle);
			
			sheet.addMergedRegion(new CellRangeAddress(startRowSize, mergeSize, 1, 1));
			cell_1 = row_1.createCell(1);
			sheet.setColumnWidth(1, 3000);
			cell_1.setCellValue(new HSSFRichTextString("날짜"));
			cell_1.setCellStyle(this.titleStyle);
			
			sheet.addMergedRegion(new CellRangeAddress(startRowSize, mergeSize, 2, 2));
			cell_1 = row_1.createCell(2);
			sheet.setColumnWidth(2, 3000);
			cell_1.setCellValue(new HSSFRichTextString("이용 수"));
			cell_1.setCellStyle(this.titleStyle);
			
			int index = 1;
			//데이터 등록
			/*
			for (EgovMap EgovMap : listData) {
				dataRow = sheet.createRow(loopCount++);
				dataRow.setHeight(defaultHeight);

				cell_1 = dataRow.createCell(0);
				cell_1.setCellValue(new HSSFRichTextString(String.valueOf(index)));
				cell_1.setCellStyle(this.columnTextStyle);

				cell_1 = dataRow.createCell(1);
				cell_1.setCellValue(new HSSFRichTextString(MapUtil.isEgovMapNullCheck(EgovMap, "tdate")));
				cell_1.setCellStyle(this.columnTextStyle);
				
				cell_1 = dataRow.createCell(2);
				cell_1.setCellValue(new HSSFRichTextString(MapUtil.isEgovMapNullCheck(EgovMap, "logcnt") + " 건"));
				cell_1.setCellStyle(this.columnTextStyle);
				
				index++;
			}
			*/
		}
	}
	
}
