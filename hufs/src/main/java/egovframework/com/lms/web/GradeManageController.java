package egovframework.com.lms.web;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.View;

import egovframework.com.cmm.ComDefaultCodeVO;
import egovframework.com.cmm.service.EgovCmmUseService;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.com.cop.bbs.service.BoardMasterVO;
import egovframework.com.cop.bbs.service.Ctgry;
import egovframework.com.cop.bbs.service.CtgryMaster;
import egovframework.com.cop.bbs.service.EgovBBSAttributeManageService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryMasterService;
import egovframework.com.cop.bbs.service.EgovBBSCtgryService;
import egovframework.com.lms.service.AttendService;
import egovframework.com.lms.service.AttendVO;
import egovframework.com.lms.service.CurriculumMemberService;
import egovframework.com.lms.service.CurriculumService;
import egovframework.com.lms.service.CurriculumVO;
import egovframework.com.lms.service.GradeService;
import egovframework.com.lms.service.LmsCommonUtil;
import egovframework.com.lms.service.LmsMngVO;
import egovframework.com.lms.web.view.GradeSampleExcelView;
import egovframework.com.sch.service.ScheduleMngService;
import egovframework.com.sch.service.ScheduleMngVO;
import egovframework.com.sec.ram.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.com.sym.ccm.cde.service.CmmnDetailCode;
import egovframework.com.sym.sit.service.EgovSiteManageService;
import egovframework.com.sym.sit.service.SiteManageVO;
import egovframework.com.uat.uia.service.LoginVO;
import egovframework.com.utl.fcc.service.EgovDateUtil;
import egovframework.com.utl.fcc.service.EgovStringUtil;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.psl.dataaccess.util.EgovMap;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import net.sf.json.JSONObject;


@Controller
public class GradeManageController {

	 @Resource(name = "EgovBBSAttributeManageService")
	 private EgovBBSAttributeManageService bbsAttrbService;
	 
	 @Resource(name = "EgovBBSCtgryMasterService")
	 private EgovBBSCtgryMasterService egovBBSCtgryMasterService;
	 
	 @Resource(name = "EgovBBSCtgryService")
	 private EgovBBSCtgryService egovBBSCtgryService;
	 
	 @Resource(name = "curriculumService")
	 private CurriculumService curriculumService;
	 
	 @Resource(name="EgovCmmUseService")
	 private EgovCmmUseService cmmUseService;
	 
	 /** EgovPropertyService */
    @Resource(name = "propertiesService")
    protected EgovPropertyService propertiesService;
    
    @Resource(name = "ScheduleMngService")
	private ScheduleMngService scheduleMngService;
    
    @Resource(name = "SiteManageService")
	EgovSiteManageService siteManageService;
    
    @Resource(name = "curriculumMemberService")
    private CurriculumMemberService curriculumMemberService;

    @Resource(name = "EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

    @Resource(name = "EgovFileMngService")
	private EgovFileMngService fileMngService;

    @Resource(name = "gradeService")
	private GradeService gradeService;
    
    @Resource(name = "attendService")
    private AttendService attendService;
    
    //총괄평가
    @RequestMapping(value="/lms/manage/gradeTotal.do")
    public String gradeTotal(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
		//과정정보
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(searchVO));
		
    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		searchVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map1 = (EgovMap) subUserList.get(i);
          		String userId = map1.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
    	
      	//성적기준
      	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000011");
		model.addAttribute("gradeTypeList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
      	
      	//성적 목록
  		List gradeList = gradeService.selectGradeList(searchVO);
  		model.addAttribute("gradeList", gradeList);
  		
      	//수강 대상자 조회
  		if(gradeList == null || gradeList.size() == 0){
  			searchVO.setOrderByType("userName");
  			List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
  			model.addAttribute("selectStudentList", selectStudentList);
  		}else{
  			//과정 전체 학생 성적 목록 - summary용
  			searchVO.setSearchUserNm("");
  	  		List gradeTotList = gradeService.selectGradeList(searchVO);
  	  		model.addAttribute("gradeTotList", gradeTotList);
  		}

      	//총괄평가
		List evaluationList = curriculumService.selectTotalEvaluation(searchVO);
		model.addAttribute("evaluationList", evaluationList);

		//수업참여도 게시판
		List attendBbsList = curriculumService.selectAttendbbs(searchVO);
		model.addAttribute("attendBbsList", attendBbsList);
		
        return "/lms/manage/gradeTotal";
    }
    
    //총괄평가(성적) 저장
    @RequestMapping(value="/lms/manage/gradeTotalUpdate.do")
    public String gradeTotalUpdate(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
    	if(user != null){
    		searchVO.setLastUpdusrId(user.getId());
    		gradeService.gradeTotalUpdate(searchVO);
    	}
    	
        return "forward:/lms/manage/gradeTotal.do";
    }
    
    //총괄평가
    @RequestMapping(value="/lms/manage/completeStand.do")
    public String completeStand(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
		//과정정보
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(searchVO));
		
    	//과정 담당자 인지 확인
      	LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
      	String managerAt = "N";
      	if(user != null){
      		searchVO.setManageCode("");
      		List subUserList = curriculumService.selectCurriculumMng(searchVO);
          	for(int i = 0; i < subUserList.size(); i ++){
          		EgovMap map1 = (EgovMap) subUserList.get(i);
          		String userId = map1.get("userId").toString();
          		if(user.getId().equals(userId)){
          			managerAt = "Y";
          		}
          	}
      	}
      	model.addAttribute("managerAt", managerAt);
    	
      	//수료기준
      	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000010");
		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
      	//성적 목록
  		List gradeList = gradeService.selectGradeList(searchVO);
  		model.addAttribute("gradeList", gradeList);
  		
  		//수료 정보 취합
  		EgovMap gradeSummary = gradeService.selectGradeSummary(searchVO);
		model.addAttribute("gradeSummary", gradeSummary);
  		
      	//수강 대상자 조회
  		if(gradeSummary == null){
  			searchVO.setOrderByType("userName");
  			List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
  			model.addAttribute("selectStudentList", selectStudentList);
  		}

        return "/lms/manage/completeStand";
    }
    
    //수료/성적 확인 목록
    @RequestMapping(value="/lms/grade/gradeList.do")
    public String gradeList(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	//언어코드
  		Ctgry ctgry = new Ctgry();
  		ctgry.setCtgrymasterId("CTGMST_0000000000002");
  		model.addAttribute("languageList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
  		
    	//년도
		model.addAttribute("yearList", new LmsCommonUtil().getYear());
		
		//학기
		ctgry.setCtgrymasterId("CTGMST_0000000000016");
		model.addAttribute("crclTermList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
    	
      	//수료기준
		ctgry.setCtgrymasterId("CTGMST_0000000000010");
		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
		/** EgovPropertyService.sample */
    	searchVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	searchVO.setPageSize(propertiesService.getInt("pageSize"));

    	/** pageing */
    	PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(searchVO.getPageUnit());
		paginationInfo.setPageSize(searchVO.getPageSize());

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
      	//성적 목록
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setSearchUserId(user.getId());
  		}
		
		List<EgovMap> curriculumList = curriculumService.selectMyCurriculumList(searchVO);
        model.addAttribute("resultList", curriculumList);
		
		int totCnt = curriculumService.selectMyCurriculumListCnt(searchVO);
		paginationInfo.setTotalRecordCount(totCnt);
        model.addAttribute("paginationInfo", paginationInfo);
        
        return "/lms/grade/gradeList";
    }
    
    //수료/성적 확인 상세
    @RequestMapping(value="/lms/grade/selectGrade.do")
    public String selectGrade(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	//과정정보
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(searchVO));
    			
    	//수료기준
      	Ctgry ctgry = new Ctgry();
		ctgry.setCtgrymasterId("CTGMST_0000000000010");
		model.addAttribute("finishList", egovBBSCtgryService.selectComtnbbsctgryList(ctgry));
		
      	//성적(수료)
		LoginVO user = EgovUserDetailsHelper.getAuthenticatedUser(request, response);
		if("02".equals(user.getUserSeCode()) || "04".equals(user.getUserSeCode()) || "06".equals(user.getUserSeCode())) {
  			searchVO.setSearchUserId(user.getId());
  		}
		
  		List gradeList = gradeService.selectGradeList(searchVO);
  		model.addAttribute("gradeList", gradeList);
		
        return "/lms/grade/gradeView";
    }
    
    //엑셀 샘플 다운로드
    @RequestMapping(value="/lms/manage/gradeSpExcel.do")
    public String gradeSpExcel(@ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
		//과정정보
		model.addAttribute("curriculumVO", curriculumService.selectCurriculum(searchVO));
		
      	//총괄평가
		List evaluationList = curriculumService.selectTotalEvaluation(searchVO);
		model.addAttribute("evaluationList", evaluationList);

		//수강 대상자 조회
		searchVO.setOrderByType("userName");
  		List<?> selectStudentList = curriculumMemberService.selectStudentList(searchVO);
  		model.addAttribute("selectStudentList", selectStudentList);
		
        return "/lms/manage/gradeSpExcel";
    }
    
    //성적 엑셀 업로드
    @RequestMapping(value="/lms/manage/gradeSpExcelUpload.do")
    public String gradeSpExcelUpload(final MultipartHttpServletRequest multiRequest, @ModelAttribute("searchVO") CurriculumVO searchVO, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	try {
			List<FileVO> fileList = null;		      
			final Map<String, MultipartFile> files = multiRequest.getFileMap();
			if(!files.isEmpty()) {
				fileList = fileUtil.directParseFileInf(files, "GD_", 0, "Grade.fileStorePath", searchVO.getCrclId());
			}
			if(fileList != null && fileList.size() > 0) {
				Map<String, Object> resultList = gradeService.parseExcel(searchVO, fileList.get(0));
				String err_message = (String) resultList.get("message");
				if(err_message == null) {
					int applyCnt = 0;
					List<CurriculumVO> dataList = (List<CurriculumVO>)resultList.get("dataList");
					try {
						//applyCnt = comtnlrncntntsService.insertComtnlrncntntsExcel(dataList);
					} catch(DataAccessException ex) {
						System.out.println(ex.getMessage());
					} catch(Exception ex) {
						System.out.println(ex.getMessage());
					}
					/*
					if(EgovStringUtil.isEmpty(err_message)) {
						res.setSuccess(true); 
						res.setData(applyCnt);
					} else {
						 res.setCode("902");
						 res.setMessage(err_message);
					}
					*/
				} else {
					//업로드 실패
				}
			}
		} catch(DataAccessException ex) {
			System.out.println(ex.getMessage());
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
    	
    	return "forward:/lms/manage/gradeTotal.do";
    }
}
